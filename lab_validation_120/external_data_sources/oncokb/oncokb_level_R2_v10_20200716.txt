ALK
C1156Y
Non-Small Cell Lung Cancer
Crizotinib
ALK
G1202R
Non-Small Cell Lung Cancer
Alectinib
ALK
G1269A
Non-Small Cell Lung Cancer
Crizotinib
ALK
I1171N
Non-Small Cell Lung Cancer
Alectinib
ALK
L1196M
Non-Small Cell Lung Cancer
Crizotinib
EGFR
C797S
Non-Small Cell Lung Cancer
Osimertinib
EGFR
L718V
Non-Small Cell Lung Cancer
Osimertinib
EGFR
C797G
Non-Small Cell Lung Cancer
Osimertinib
EGFR
D761Y
Non-Small Cell Lung Cancer
Gefitinib
KIT
D816
Gastrointestinal Stromal Tumor
Imatinib
KIT
D816
Gastrointestinal Stromal Tumor
Sunitinib
KIT
D820
Gastrointestinal Stromal Tumor
Imatinib
KIT
D820
Gastrointestinal Stromal Tumor
Sunitinib
KIT
N822
Gastrointestinal Stromal Tumor
Imatinib
KIT
A829P
Gastrointestinal Stromal Tumor
Imatinib
KIT
N822
Gastrointestinal Stromal Tumor
Sunitinib
KIT
A829P
Gastrointestinal Stromal Tumor
Sunitinib
KIT
Y823D
Gastrointestinal Stromal Tumor
Sunitinib
KIT
Y823D
Gastrointestinal Stromal Tumor
Imatinib
KIT
C809G
Gastrointestinal Stromal Tumor
Sunitinib
KIT
C809G
Gastrointestinal Stromal Tumor
Imatinib
KIT
V654A
Gastrointestinal Stromal Tumor
Imatinib
KIT
T670I
Gastrointestinal Stromal Tumor
Imatinib
MET
D1228N
Non-Small Cell Lung Cancer
Crizotinib, Capmatinib, Cabozantinib
MET
Y1230H
Non-Small Cell Lung Cancer
Capmatinib, Crizotinib
MET
Amplification
Non-Small Cell Lung Cancer
Erlotinib, Gefitinib, Osimertinib
NTRK1
G595R
All Solid Tumors
Larotrectinib
NTRK3
G623R
All Solid Tumors
Larotrectinib