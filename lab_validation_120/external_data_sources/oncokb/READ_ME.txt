# open the following page: https://www.oncokb.org/actionableGenes#levels=1
# cut and paste all values into oncokb_level_1_v10_20200716.txt
cat  lab_validation_395/external_data_sources/oncokb/oncokb_level_1_v10_20200716.txt | awk 'BEGIN {FS="\t"} ORS=NR%4?FS:RS' >lab_validation_395/external_data_sources/oncokb/oncokb_level_1_v10_20200716_parsed.txt

# the did this to get list almost ready to place into lab_validation_395/utils/genomic_metadata_utilities.py
 cut -f 1 lab_validation_395/external_data_sources/oncokb/oncokb_level_1_v10_20200716_parsed.txt | sort -u | perl -ne 'chomp $_; print("\"$_\",\n");'