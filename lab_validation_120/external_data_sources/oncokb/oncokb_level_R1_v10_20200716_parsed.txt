ABL1	T315I	Chronic Myelogenous Leukemia	Imatinib, Bosutinib, Nilotinib, Dasatinib
ABL1	T315I	B-Lymphoblastic Leukemia/Lymphoma	Nilotinib, Imatinib, Bosutinib, Dasatinib
EGFR	Exon 20 insertion	Non-Small Cell Lung Cancer	Gefitinib, Afatinib, Erlotinib
EGFR	T790M	Non-Small Cell Lung Cancer	Gefitinib, Erlotinib, Afatinib
KRAS	Oncogenic Mutations	Colorectal Cancer	Cetuximab, Panitumumab
NRAS	Oncogenic Mutations	Colorectal Cancer	Panitumumab, Cetuximab
PDGFRA	D842V	Gastrointestinal Stromal Tumor	Imatinib
