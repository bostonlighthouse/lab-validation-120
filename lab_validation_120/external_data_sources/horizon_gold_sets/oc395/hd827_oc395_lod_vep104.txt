Chromosome	Gene	Variant	VAF	Variant_Type
# Removed b/c it's unannotated
#chr.2	ALK	N/A (Ins)	10.00	INS
# changed T1493T -> c.4479G>A(%3D)
chr.5	APC	T1493%3D	35.00	SNV
chr.7	BRAF	V600E	10.50	SNV
# changed K1691fs*15 -> K1691Nfs*15
chr.13	BRCA2	K1691Nfs*15	32.50	DEL
chr.3	CTNNB1	S33Y	32.50	SNV
chr.3	CTNNB1	S45del	10.00	DEL
chr.7	EGFR	G719S	24.50	SNV
chr.7	EGFR	T790M	1.00	SNV
chr.7	EGFR	L858R	3.00	SNV
# changed E746_A750delELREA -> E746_A750del
chr.7	EGFR	E746_A750del	2.00	DEL
# chaged Q787Q -> c.2202G>A(%3D)
chr.7	EGFR	Q734%3D	15.00	SNV
# changed S668fs*39 -> S550Vfs*39
chr.4	FBXW7	S550Vfs*39	32.50	DEL
# changed P986fs*>8 -> P945Afs*27
chr.13	FLT3	P945Afs*27	10.00	DEL
chr.4	KIT	D816V	10.00	SNV
# changed L862L -> c.2586G>C(%3D)
chr.4	KIT	L862%3D	7.50	SNV
chr.12	KRAS	G13D	15.00	SNV
chr.12	KRAS	G12D	6.00	SNV
# changed A1357A -> c.4017G>A(%3D)
chr.7	MET	A1339%3D	7.00	SNV
# changed L238fs*25 -> L238Yfs*25
chr.7	MET	L238Yfs*25	7.00	DEL
chr.9	NOTCH1	P668S	30.00	SNV
chr.1	NRAS	Q61K	12.50	SNV
chr.3	PIK3CA	E545K	9.00	SNV
chr.3	PIK3CA	H1047R	17.50	SNV
# changed  L769L -> c.2307G>T(%3D)
chr.10	RET	L769%3D	60.00	SNV
chr.17	TP53	P72R	92.50	SNV
# Below are all Presence Confirmed in parental cell lines