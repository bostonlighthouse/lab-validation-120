"""Need small module to do return metadata about the genome"""
from lab_validation_120.utils.configs.lab_validation_config import get_oc120_targets_bed_file, \
    get_oc120archer_targets_bed_file, get_oc120plus_targets_bed_file, get_oc395_targets_bed_file, \
    get_oc120plus_oc395_overlap_targets_bed_file, get_oc120_oc120plus_overlap_targets_bed_file, \
    get_agilent700_oc395_overlap_targets_bed_file, get_agilent700_oc180_overlap_targets_bed_file
from lab_validation_120.utils.pandas_dataframes_utilities import read_dataset_with_no_header_and_get_pandas_df, \
    get_columns_from_bed_with_no_header, get_columns_from_oc120plus_bed_file, \
    get_columns_from_bed_with_no_header_from_bedtools_intersect


def get_list_chromosomes():
    # must use string to represent the chromosomes
    #return list(map(str, range(1, 23))) + ['X', 'Y']
    # our assays really don't call on they Y chromosome, so remove
    return list(map(str, range(1, 23))) + ['X']


def get_list_oncokb_genes(file=None):
    """
    Used to return a list of e.g. L1 genes from OncoKB based on the file passed in
    See lab_validation_120/external_data_sources/oncokb/READ_ME.txt
    :param file: File to open a get the list of genes
    """
    genes = set()
    with open(file, mode='r') as fh:
        for line in fh:
            genes.add(line.split("\t")[0])
    return sorted(list(genes))


def is_overlapping(x1=None, x2=None, y1=None, y2=None):
    """Simple function to see if there was overlap between a variant and a BED interval
    :param x1: start in interval 1
    :param x2: stop in interval 1
    :param y1: start in interval 2
    :param y2: stop in interval 2
    :return bool:  True/False if there was overlap

    """
    return max(x1, y1) <= min(x2, y2)


def variant_position_overlapped_bed_file(chrom=None, position=None, list_of_dict_df_bed=None):
    """
    Go over all intervals in the BED file, once the chromosomes maps to the BED file,
    see if there was overlap using the is_overlapping function
    :param chrom: String of the chromosome
    :param position: Position in the genome
    :param list_of_dict_df_bed: A list of dictionaries, where each entry is a row in a BED file
    :return bool:  True/False if there was overlap (uses is_overlap function)

    """
    overlap = False
    for entry in list_of_dict_df_bed:
        # do the chromosomes match
        if entry['CHROM'] == chrom:
            # remember BED file is 0 based, so y1 + 1
            overlap = is_overlapping(x1=position, x2=position, y1=entry['START_ZERO_BASED'] + 1, y2=entry['END'])
            if overlap is True:
                return overlap
    return overlap


def get_df_bed_intervals_by_assay(which_oc_assay=None):
    """
    e.g. read the oc120/oc120Archer/oc120plus BED file into a data frame.  The oc120Archer is just the archer bed file
    @param which_oc_assay:  This will be used to open which BED file.
    @return: Return a pandas df of the intervals
    """
    if which_oc_assay == 'oc120':
        df_assay_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_bed_with_no_header(),
                                                                     file_name=get_oc120_targets_bed_file())

    elif which_oc_assay == 'oc120archer':
        df_assay_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_bed_with_no_header(),
                                                                     file_name=get_oc120archer_targets_bed_file(),
                                                                     ignore_line_starts_with=
                                                                     'track name="43233_targets"')
    elif which_oc_assay == 'oc120plus':
        df_assay_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_oc120plus_bed_file(),
                                                                     file_name=get_oc120plus_targets_bed_file(),
                                                                     ignore_line_starts_with=
                                                                     'track name="16897_targets"')
    elif which_oc_assay == 'oc395boosted_primers':
        df_assay_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_bed_with_no_header(),
                                                                     file_name=get_oc395_targets_bed_file())
    elif which_oc_assay == 'oc120plus_oc395_overlap':
        df_assay_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_bed_with_no_header_from_bedtools_intersect(),
                                                                     file_name=get_oc120plus_oc395_overlap_targets_bed_file())
    elif which_oc_assay == 'oc120_oc120plus_overlap':
        df_assay_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_bed_with_no_header(),
                                                                     file_name=get_oc120_oc120plus_overlap_targets_bed_file())

    elif which_oc_assay == 'agilent700_vs_oc180':
        df_assay_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_bed_with_no_header_from_bedtools_intersect(),
                                                                     file_name=get_agilent700_oc180_overlap_targets_bed_file())

    elif which_oc_assay == 'agilent700_vs_oc395':
        df_assay_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_bed_with_no_header(),
                                                                     file_name=get_agilent700_oc395_overlap_targets_bed_file())

    else:
        raise ValueError(f"Did not implement this which_oc_assay type: {which_oc_assay}")

    # replace the chr1 -> 1, chr2 -> 2, etc
    df_assay_bed = df_assay_bed.replace(to_replace='chr', value='', regex=True)

    return df_assay_bed
