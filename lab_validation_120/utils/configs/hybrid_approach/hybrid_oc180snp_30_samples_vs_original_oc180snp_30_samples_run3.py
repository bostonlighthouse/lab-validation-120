"""
Get the VCF / QC files for the GS180snps and GS395 hybrid approach (RUN 3)
Here we used 30 GS180 Snps and 6 GS395 samples (RUN 3)
"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run3():
    """
    Get the VCF files for GS180snps and GS395 hybrid approach
    30 GS180snps files (RUN2)
    """

    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run3",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc180snps/hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                    "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc180snps/hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt"
                ],
                "indel": [
                    "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc180snps/hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt",
                    "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc180snps/hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt"
                ]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file":  [
                        "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc180snps/hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                        "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc180snps/hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt"
                    ],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc180snps/hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt",
                        "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc180snps/hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt"
                    ],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS180 SNPs Hybrid run3",
                "y_axis_label": "GS180 SNPs Original",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS180 SNPs Hybrid run3",
                "y_axis_label": "GS180 SNPs Original",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS180 SNPs Hybrid run3",
                "y_axis_label": "GS180 SNPs Original",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            #"vaf_threshold": 10.0,
            #"depth_threshold": 100,

            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid23-584',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-584-snapshot/2023fev17-corridahib-b23-36-cid23-584-snapshot.B23-36.CID23-584.C529_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '1b-cid23-4',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2023jan13-gs180-run0-b23-4-cid23-4-snapshot/2023jan13-gs180-run0-b23-4-cid23-4-snapshot.B23-4.CID23-4.C529_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample2': [{'sample_id': '2a-cid23-585',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-585-snapshot/2023fev17-corridahib-b23-36-cid23-585-snapshot.B23-36.CID23-585.C539_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '2b-cid23-14',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2023jan13-gs180-run0-b23-4-cid23-14-snapshot/2023jan13-gs180-run0-b23-4-cid23-14-snapshot.B23-4.CID23-14.C539_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample3': [{'sample_id': '3a-cid23-586',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-586-snapshot/2023fev17-corridahib-b23-36-cid23-586-snapshot.B23-36.CID23-586.B509_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '3b-cid22-2859',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2023jan06-gs180-run0-b23-2-cid22-2859-snapshot/2023jan06-gs180-run0-b23-2-cid22-2859-snapshot.B23-2.CID22-2859.B509_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                # CID23-587 failed MCC (20x) http://multiqc.cgs.bostonlighthouse.us/batches/B23-36#samtools-stats
                #'inter_Sample4': [{'sample_id': '4a-cid23-587',
                #                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-587-snapshot/2023fev17-corridahib-b23-36-cid23-587-snapshot.B23-36.CID23-587.C537_N705.merged_concatenated.vcf',
                #                   'variant_type': 'snvs'},
                #                  {'sample_id': '4b-cid22-2232',
                #                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022out14-gs180-run3-b22-130-cid22-2232-snapshot/2022out14-gs180-run3-b22-130-cid22-2232-snapshot.B22-130.CID22-2232.C537_N705.merged_concatenated.vcf',
                #                   'variant_type': 'snvs'},
                #                  {'experiment_type': 'inter-run'}],
                'inter_Sample5': [{'sample_id': '5a-cid23-588',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-588-snapshot/2023fev17-corridahib-b23-36-cid23-588-snapshot.B23-36.CID23-588.B501_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '5b-cid21-1515',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-08-25-oc180-run-b21-112-cid21-1515-snapshot/2021-08-25-oc180-run-b21-112-cid21-1515-snapshot.B21-112.CID21-1515.C528_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample6': [{'sample_id': '6a-cid23-589',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-589-snapshot/2023fev17-corridahib-b23-36-cid23-589-snapshot.B23-36.CID23-589.B502_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '6b-cid21-1665',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample7': [{'sample_id': '7a-cid23-590',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-590-snapshot/2023fev17-corridahib-b23-36-cid23-590-snapshot.B23-36.CID23-590.B503_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '7b-cid21-484',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-23-gs180-run-b21-49-cid21-484-snapshot/2021-04-23-gs180-run-b21-49-cid21-484-snapshot.B21-49.CID21-484.B524_N708.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample8': [{'sample_id': '8a-cid23-591',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-591-snapshot/2023fev17-corridahib-b23-36-cid23-591-snapshot.B23-36.CID23-591.B504_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '8b-cid21-486',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-23-gs180-run-b21-49-cid21-486-snapshot/2021-04-23-gs180-run-b21-49-cid21-486-snapshot.B21-49.CID21-486.B539_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample9': [{'sample_id': '9a-cid23-592',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-592-snapshot/2023fev17-corridahib-b23-36-cid23-592-snapshot.B23-36.CID23-592.B505_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '9b-cid21-494',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-23-gs180-run-b21-51-cid21-494-snapshot/2021-04-23-gs180-run-b21-51-cid21-494-snapshot.B21-51.CID21-494.B533_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample10': [{'sample_id': '10a-cid23-593',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-593-snapshot/2023fev17-corridahib-b23-36-cid23-593-snapshot.B23-36.CID23-593.B506_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '10b-cid21-555',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-30-oc180-run-b21-55-cid21-555-snapshot/2021-04-30-oc180-run-b21-55-cid21-555-snapshot.B21-55.CID21-555.B539_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample11': [{'sample_id': '11a-cid23-594',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-594-snapshot/2023fev17-corridahib-b23-36-cid23-594-snapshot.B23-36.CID23-594.B507_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '11b-cid21-775',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-05-31-oc180-run-b21-73-cid21-775-snapshot/2021-05-31-oc180-run-b21-73-cid21-775-snapshot.B21-73.CID21-775.A537_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample12': [{'sample_id': '12a-cid23-595',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-595-snapshot/2023fev17-corridahib-b23-36-cid23-595-snapshot.B23-36.CID23-595.B508_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '12b-cid21-2645',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021dez11-oc180-r60--b21-175-cid21-2645-snapshot/2021dez11-oc180-r60--b21-175-cid21-2645-snapshot.B21-175.CID21-2645.C541_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample13': [{'sample_id': '13a-cid23-596',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-596-snapshot/2023fev17-corridahib-b23-36-cid23-596-snapshot.B23-36.CID23-596.B542_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '13b-cid21-1970',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021out13-oc180-run5-b21-145-cid21-1970-snapshot/2021out13-oc180-run5-b21-145-cid21-1970-snapshot.B21-145.CID21-1970.B504_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample14': [{'sample_id': '14a-cid23-597',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-597-snapshot/2023fev17-corridahib-b23-36-cid23-597-snapshot.B23-36.CID23-597.B526_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '14b-cid22-1083',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1083-snapshot/2022mai11-oc180-run0-b22-60-cid22-1083-snapshot.B22-60.CID22-1083.B526_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample15': [{'sample_id': '15a-cid23-598',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-598-snapshot/2023fev17-corridahib-b23-36-cid23-598-snapshot.B23-36.CID23-598.B525_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '15b-cid22-1082',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1082-snapshot/2022mai11-oc180-run0-b22-60-cid22-1082-snapshot.B22-60.CID22-1082.B525_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample16': [{'sample_id': '16a-cid23-599',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-599-snapshot/2023fev17-corridahib-b23-36-cid23-599-snapshot.B23-36.CID23-599.B527_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '16b-cid22-1084',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1084-snapshot/2022mai11-oc180-run0-b22-60-cid22-1084-snapshot.B22-60.CID22-1084.B527_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample17': [{'sample_id': '17a-cid23-600',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-600-snapshot/2023fev17-corridahib-b23-36-cid23-600-snapshot.B23-36.CID23-600.B528_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '17b-cid22-1085',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1085-snapshot/2022mai11-oc180-run0-b22-60-cid22-1085-snapshot.B22-60.CID22-1085.B528_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample18': [{'sample_id': '18a-cid23-601',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-601-snapshot/2023fev17-corridahib-b23-36-cid23-601-snapshot.B23-36.CID23-601.B529_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '18b-cid22-1086',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1086-snapshot/2022mai11-oc180-run0-b22-60-cid22-1086-snapshot.B22-60.CID22-1086.B529_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample19': [{'sample_id': '19a-cid23-602',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-602-snapshot/2023fev17-corridahib-b23-36-cid23-602-snapshot.B23-36.CID23-602.B531_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '19b-cid22-1088',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1088-snapshot/2022mai11-oc180-run0-b22-60-cid22-1088-snapshot.B22-60.CID22-1088.B531_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample20': [{'sample_id': '20a-cid23-603',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-603-snapshot/2023fev17-corridahib-b23-36-cid23-603-snapshot.B23-36.CID23-603.B530_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '20b-cid21-1226',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021jul15-oc180-run0-b21-92-cid21-1226-snapshot/2021jul15-oc180-run0-b21-92-cid21-1226-snapshot.B21-92.CID21-1226.A531_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample21': [{'sample_id': '21a-cid23-604',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-604-snapshot/2023fev17-corridahib-b23-36-cid23-604-snapshot.B23-36.CID23-604.B532_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '21b-cid22-1089',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1089-snapshot/2022mai11-oc180-run0-b22-60-cid22-1089-snapshot.B22-60.CID22-1089.B532_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample22': [{'sample_id': '22a-cid23-605',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-605-snapshot/2023fev17-corridahib-b23-36-cid23-605-snapshot.B23-36.CID23-605.B534_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '22b-cid22-1091',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1091-snapshot/2022mai11-oc180-run0-b22-60-cid22-1091-snapshot.B22-60.CID22-1091.B534_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample23': [{'sample_id': '23a-cid23-606',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-606-snapshot/2023fev17-corridahib-b23-36-cid23-606-snapshot.B23-36.CID23-606.B533_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '23b-cid22-1090',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1090-snapshot/2022mai11-oc180-run0-b22-60-cid22-1090-snapshot.B22-60.CID22-1090.B533_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample24': [{'sample_id': '24a-cid23-607',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-607-snapshot/2023fev17-corridahib-b23-36-cid23-607-snapshot.B23-36.CID23-607.B535_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '24b-cid22-1092',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1092-snapshot/2022mai11-oc180-run0-b22-60-cid22-1092-snapshot.B22-60.CID22-1092.B535_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample25': [{'sample_id': '25a-cid23-608',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-608-snapshot/2023fev17-corridahib-b23-36-cid23-608-snapshot.B23-36.CID23-608.B536_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '25b-cid22-1093',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1093-snapshot/2022mai11-oc180-run0-b22-60-cid22-1093-snapshot.B22-60.CID22-1093.B536_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample26': [{'sample_id': '26a-cid23-609',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-609-snapshot/2023fev17-corridahib-b23-36-cid23-609-snapshot.B23-36.CID23-609.B537_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '26b-cid22-1094',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1094-snapshot/2022mai11-oc180-run0-b22-60-cid22-1094-snapshot.B22-60.CID22-1094.B537_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample27': [{'sample_id': '27a-cid23-610',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-610-snapshot/2023fev17-corridahib-b23-36-cid23-610-snapshot.B23-36.CID23-610.B539_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '27b-cid22-1096',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1096-snapshot/2022mai11-oc180-run0-b22-60-cid22-1096-snapshot.B22-60.CID22-1096.B539_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample28': [{'sample_id': '28a-cid23-611',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-611-snapshot/2023fev17-corridahib-b23-36-cid23-611-snapshot.B23-36.CID23-611.B538_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '28b-cid22-1095',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1095-snapshot/2022mai11-oc180-run0-b22-60-cid22-1095-snapshot.B22-60.CID22-1095.B538_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample29': [{'sample_id': '29a-cid23-612',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-612-snapshot/2023fev17-corridahib-b23-36-cid23-612-snapshot.B23-36.CID23-612.B540_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '29b-cid22-1097',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1097-snapshot/2022mai11-oc180-run0-b22-60-cid22-1097-snapshot.B22-60.CID22-1097.B540_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample30': [{'sample_id': '30a-cid23-613',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-613-snapshot/2023fev17-corridahib-b23-36-cid23-613-snapshot.B23-36.CID23-613.B543_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '30b-cid22-1100',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1100-snapshot/2022mai11-oc180-run0-b22-60-cid22-1100-snapshot.B22-60.CID22-1100.B543_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}]
            }
        }
    }


def get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_htqc_plots_run3():
    """
    Get the QC files for GS180snps and GS395 hybrid approach (run3)
    30 GS180snps files (RUN 3)
    """

    key_2_use = 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run3'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS180 SNPs Hybrid (a) vs GS180 SNPs Hybrid (b) 29/30 samples passed MCC - Run3",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-584-snapshot/2023fev17-corridahib-b23-36-cid23-584-snapshot.B23-36.CID23-584.C529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-584-snapshot/2023fev17-corridahib-b23-36-cid23-584-snapshot.B23-36.CID23-584.C529_N705.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid23-584',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2023jan13-gs180-run0-b23-4-cid23-4-snapshot/2023jan13-gs180-run0-b23-4-cid23-4-snapshot.B23-4.CID23-4.C529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2023jan13-gs180-run0-b23-4-cid23-4-snapshot/2023jan13-gs180-run0-b23-4-cid23-4-snapshot.B23-4.CID23-4.C529_N705.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid23-4',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-585-snapshot/2023fev17-corridahib-b23-36-cid23-585-snapshot.B23-36.CID23-585.C539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-585-snapshot/2023fev17-corridahib-b23-36-cid23-585-snapshot.B23-36.CID23-585.C539_N707.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid23-585',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2023jan13-gs180-run0-b23-4-cid23-14-snapshot/2023jan13-gs180-run0-b23-4-cid23-14-snapshot.B23-4.CID23-14.C539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2023jan13-gs180-run0-b23-4-cid23-14-snapshot/2023jan13-gs180-run0-b23-4-cid23-14-snapshot.B23-4.CID23-14.C539_N707.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid23-14',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-586-snapshot/2023fev17-corridahib-b23-36-cid23-586-snapshot.B23-36.CID23-586.B509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-586-snapshot/2023fev17-corridahib-b23-36-cid23-586-snapshot.B23-36.CID23-586.B509_N701.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid23-586',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2023jan06-gs180-run0-b23-2-cid22-2859-snapshot/2023jan06-gs180-run0-b23-2-cid22-2859-snapshot.B23-2.CID22-2859.B509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2023jan06-gs180-run0-b23-2-cid22-2859-snapshot/2023jan06-gs180-run0-b23-2-cid22-2859-snapshot.B23-2.CID22-2859.B509_N701.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid22-2859',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-587-snapshot/2023fev17-corridahib-b23-36-cid23-587-snapshot.B23-36.CID23-587.C537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-587-snapshot/2023fev17-corridahib-b23-36-cid23-587-snapshot.B23-36.CID23-587.C537_N705.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid23-587',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022out14-gs180-run3-b22-130-cid22-2232-snapshot/2022out14-gs180-run3-b22-130-cid22-2232-snapshot.B22-130.CID22-2232.C537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022out14-gs180-run3-b22-130-cid22-2232-snapshot/2022out14-gs180-run3-b22-130-cid22-2232-snapshot.B22-130.CID22-2232.C537_N705.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid22-2232',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-588-snapshot/2023fev17-corridahib-b23-36-cid23-588-snapshot.B23-36.CID23-588.B501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-588-snapshot/2023fev17-corridahib-b23-36-cid23-588-snapshot.B23-36.CID23-588.B501_N701.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid23-588',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-08-25-oc180-run-b21-112-cid21-1515-snapshot/2021-08-25-oc180-run-b21-112-cid21-1515-snapshot.B21-112.CID21-1515.C528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-08-25-oc180-run-b21-112-cid21-1515-snapshot/2021-08-25-oc180-run-b21-112-cid21-1515-snapshot.B21-112.CID21-1515.C528_N704.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid21-1515',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-589-snapshot/2023fev17-corridahib-b23-36-cid23-589-snapshot.B23-36.CID23-589.B502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-589-snapshot/2023fev17-corridahib-b23-36-cid23-589-snapshot.B23-36.CID23-589.B502_N702.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid23-589',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid21-1665',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-590-snapshot/2023fev17-corridahib-b23-36-cid23-590-snapshot.B23-36.CID23-590.B503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-590-snapshot/2023fev17-corridahib-b23-36-cid23-590-snapshot.B23-36.CID23-590.B503_N703.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid23-590',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-23-gs180-run-b21-49-cid21-484-snapshot/2021-04-23-gs180-run-b21-49-cid21-484-snapshot.B21-49.CID21-484.B524_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-23-gs180-run-b21-49-cid21-484-snapshot/2021-04-23-gs180-run-b21-49-cid21-484-snapshot.B21-49.CID21-484.B524_N708.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid21-484',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-591-snapshot/2023fev17-corridahib-b23-36-cid23-591-snapshot.B23-36.CID23-591.B504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-591-snapshot/2023fev17-corridahib-b23-36-cid23-591-snapshot.B23-36.CID23-591.B504_N704.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid23-591',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-23-gs180-run-b21-49-cid21-486-snapshot/2021-04-23-gs180-run-b21-49-cid21-486-snapshot.B21-49.CID21-486.B539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-23-gs180-run-b21-49-cid21-486-snapshot/2021-04-23-gs180-run-b21-49-cid21-486-snapshot.B21-49.CID21-486.B539_N707.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid21-486',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-592-snapshot/2023fev17-corridahib-b23-36-cid23-592-snapshot.B23-36.CID23-592.B505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-592-snapshot/2023fev17-corridahib-b23-36-cid23-592-snapshot.B23-36.CID23-592.B505_N705.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid23-592',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-23-gs180-run-b21-51-cid21-494-snapshot/2021-04-23-gs180-run-b21-51-cid21-494-snapshot.B21-51.CID21-494.B533_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-23-gs180-run-b21-51-cid21-494-snapshot/2021-04-23-gs180-run-b21-51-cid21-494-snapshot.B21-51.CID21-494.B533_N704.coverage.summary.htqc.txt',
                        'sample_id': '9b-cid21-494',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-593-snapshot/2023fev17-corridahib-b23-36-cid23-593-snapshot.B23-36.CID23-593.B506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-593-snapshot/2023fev17-corridahib-b23-36-cid23-593-snapshot.B23-36.CID23-593.B506_N706.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid23-593',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-30-oc180-run-b21-55-cid21-555-snapshot/2021-04-30-oc180-run-b21-55-cid21-555-snapshot.B21-55.CID21-555.B539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-04-30-oc180-run-b21-55-cid21-555-snapshot/2021-04-30-oc180-run-b21-55-cid21-555-snapshot.B21-55.CID21-555.B539_N707.coverage.summary.htqc.txt',
                        'sample_id': '10b-cid21-555',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-594-snapshot/2023fev17-corridahib-b23-36-cid23-594-snapshot.B23-36.CID23-594.B507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-594-snapshot/2023fev17-corridahib-b23-36-cid23-594-snapshot.B23-36.CID23-594.B507_N707.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid23-594',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-05-31-oc180-run-b21-73-cid21-775-snapshot/2021-05-31-oc180-run-b21-73-cid21-775-snapshot.B21-73.CID21-775.A537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021-05-31-oc180-run-b21-73-cid21-775-snapshot/2021-05-31-oc180-run-b21-73-cid21-775-snapshot.B21-73.CID21-775.A537_N705.coverage.summary.htqc.txt',
                        'sample_id': '11b-cid21-775',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-595-snapshot/2023fev17-corridahib-b23-36-cid23-595-snapshot.B23-36.CID23-595.B508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-595-snapshot/2023fev17-corridahib-b23-36-cid23-595-snapshot.B23-36.CID23-595.B508_N708.coverage.summary.htqc.txt',
                        'sample_id': '12a-cid23-595',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021dez11-oc180-r60--b21-175-cid21-2645-snapshot/2021dez11-oc180-r60--b21-175-cid21-2645-snapshot.B21-175.CID21-2645.C541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021dez11-oc180-r60--b21-175-cid21-2645-snapshot/2021dez11-oc180-r60--b21-175-cid21-2645-snapshot.B21-175.CID21-2645.C541_N701.coverage.summary.htqc.txt',
                        'sample_id': '12b-cid21-2645',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-596-snapshot/2023fev17-corridahib-b23-36-cid23-596-snapshot.B23-36.CID23-596.B542_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-596-snapshot/2023fev17-corridahib-b23-36-cid23-596-snapshot.B23-36.CID23-596.B542_N704.coverage.summary.htqc.txt',
                        'sample_id': '13a-cid23-596',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021out13-oc180-run5-b21-145-cid21-1970-snapshot/2021out13-oc180-run5-b21-145-cid21-1970-snapshot.B21-145.CID21-1970.B504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021out13-oc180-run5-b21-145-cid21-1970-snapshot/2021out13-oc180-run5-b21-145-cid21-1970-snapshot.B21-145.CID21-1970.B504_N704.coverage.summary.htqc.txt',
                        'sample_id': '13b-cid21-1970',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-597-snapshot/2023fev17-corridahib-b23-36-cid23-597-snapshot.B23-36.CID23-597.B526_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-597-snapshot/2023fev17-corridahib-b23-36-cid23-597-snapshot.B23-36.CID23-597.B526_N702.coverage.summary.htqc.txt',
                        'sample_id': '14a-cid23-597',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1083-snapshot/2022mai11-oc180-run0-b22-60-cid22-1083-snapshot.B22-60.CID22-1083.B526_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1083-snapshot/2022mai11-oc180-run0-b22-60-cid22-1083-snapshot.B22-60.CID22-1083.B526_N702.coverage.summary.htqc.txt',
                        'sample_id': '14b-cid22-1083',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-598-snapshot/2023fev17-corridahib-b23-36-cid23-598-snapshot.B23-36.CID23-598.B525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-598-snapshot/2023fev17-corridahib-b23-36-cid23-598-snapshot.B23-36.CID23-598.B525_N701.coverage.summary.htqc.txt',
                        'sample_id': '15a-cid23-598',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1082-snapshot/2022mai11-oc180-run0-b22-60-cid22-1082-snapshot.B22-60.CID22-1082.B525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1082-snapshot/2022mai11-oc180-run0-b22-60-cid22-1082-snapshot.B22-60.CID22-1082.B525_N701.coverage.summary.htqc.txt',
                        'sample_id': '15b-cid22-1082',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-599-snapshot/2023fev17-corridahib-b23-36-cid23-599-snapshot.B23-36.CID23-599.B527_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-599-snapshot/2023fev17-corridahib-b23-36-cid23-599-snapshot.B23-36.CID23-599.B527_N703.coverage.summary.htqc.txt',
                        'sample_id': '16a-cid23-599',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1084-snapshot/2022mai11-oc180-run0-b22-60-cid22-1084-snapshot.B22-60.CID22-1084.B527_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1084-snapshot/2022mai11-oc180-run0-b22-60-cid22-1084-snapshot.B22-60.CID22-1084.B527_N703.coverage.summary.htqc.txt',
                        'sample_id': '16b-cid22-1084',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-600-snapshot/2023fev17-corridahib-b23-36-cid23-600-snapshot.B23-36.CID23-600.B528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-600-snapshot/2023fev17-corridahib-b23-36-cid23-600-snapshot.B23-36.CID23-600.B528_N704.coverage.summary.htqc.txt',
                        'sample_id': '17a-cid23-600',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1085-snapshot/2022mai11-oc180-run0-b22-60-cid22-1085-snapshot.B22-60.CID22-1085.B528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1085-snapshot/2022mai11-oc180-run0-b22-60-cid22-1085-snapshot.B22-60.CID22-1085.B528_N704.coverage.summary.htqc.txt',
                        'sample_id': '17b-cid22-1085',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-601-snapshot/2023fev17-corridahib-b23-36-cid23-601-snapshot.B23-36.CID23-601.B529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-601-snapshot/2023fev17-corridahib-b23-36-cid23-601-snapshot.B23-36.CID23-601.B529_N705.coverage.summary.htqc.txt',
                        'sample_id': '18a-cid23-601',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1086-snapshot/2022mai11-oc180-run0-b22-60-cid22-1086-snapshot.B22-60.CID22-1086.B529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1086-snapshot/2022mai11-oc180-run0-b22-60-cid22-1086-snapshot.B22-60.CID22-1086.B529_N705.coverage.summary.htqc.txt',
                        'sample_id': '18b-cid22-1086',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-602-snapshot/2023fev17-corridahib-b23-36-cid23-602-snapshot.B23-36.CID23-602.B531_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-602-snapshot/2023fev17-corridahib-b23-36-cid23-602-snapshot.B23-36.CID23-602.B531_N707.coverage.summary.htqc.txt',
                        'sample_id': '19a-cid23-602',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1088-snapshot/2022mai11-oc180-run0-b22-60-cid22-1088-snapshot.B22-60.CID22-1088.B531_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1088-snapshot/2022mai11-oc180-run0-b22-60-cid22-1088-snapshot.B22-60.CID22-1088.B531_N707.coverage.summary.htqc.txt',
                        'sample_id': '19b-cid22-1088',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-603-snapshot/2023fev17-corridahib-b23-36-cid23-603-snapshot.B23-36.CID23-603.B530_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-603-snapshot/2023fev17-corridahib-b23-36-cid23-603-snapshot.B23-36.CID23-603.B530_N706.coverage.summary.htqc.txt',
                        'sample_id': '20a-cid23-603',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021jul15-oc180-run0-b21-92-cid21-1226-snapshot/2021jul15-oc180-run0-b21-92-cid21-1226-snapshot.B21-92.CID21-1226.A531_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2021jul15-oc180-run0-b21-92-cid21-1226-snapshot/2021jul15-oc180-run0-b21-92-cid21-1226-snapshot.B21-92.CID21-1226.A531_N707.coverage.summary.htqc.txt',
                        'sample_id': '20b-cid21-1226',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-604-snapshot/2023fev17-corridahib-b23-36-cid23-604-snapshot.B23-36.CID23-604.B532_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-604-snapshot/2023fev17-corridahib-b23-36-cid23-604-snapshot.B23-36.CID23-604.B532_N708.coverage.summary.htqc.txt',
                        'sample_id': '21a-cid23-604',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1089-snapshot/2022mai11-oc180-run0-b22-60-cid22-1089-snapshot.B22-60.CID22-1089.B532_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1089-snapshot/2022mai11-oc180-run0-b22-60-cid22-1089-snapshot.B22-60.CID22-1089.B532_N708.coverage.summary.htqc.txt',
                        'sample_id': '21b-cid22-1089',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-605-snapshot/2023fev17-corridahib-b23-36-cid23-605-snapshot.B23-36.CID23-605.B534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-605-snapshot/2023fev17-corridahib-b23-36-cid23-605-snapshot.B23-36.CID23-605.B534_N702.coverage.summary.htqc.txt',
                        'sample_id': '22a-cid23-605',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1091-snapshot/2022mai11-oc180-run0-b22-60-cid22-1091-snapshot.B22-60.CID22-1091.B534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1091-snapshot/2022mai11-oc180-run0-b22-60-cid22-1091-snapshot.B22-60.CID22-1091.B534_N702.coverage.summary.htqc.txt',
                        'sample_id': '22b-cid22-1091',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-606-snapshot/2023fev17-corridahib-b23-36-cid23-606-snapshot.B23-36.CID23-606.B533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-606-snapshot/2023fev17-corridahib-b23-36-cid23-606-snapshot.B23-36.CID23-606.B533_N701.coverage.summary.htqc.txt',
                        'sample_id': '23a-cid23-606',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1090-snapshot/2022mai11-oc180-run0-b22-60-cid22-1090-snapshot.B22-60.CID22-1090.B533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1090-snapshot/2022mai11-oc180-run0-b22-60-cid22-1090-snapshot.B22-60.CID22-1090.B533_N701.coverage.summary.htqc.txt',
                        'sample_id': '23b-cid22-1090',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-607-snapshot/2023fev17-corridahib-b23-36-cid23-607-snapshot.B23-36.CID23-607.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-607-snapshot/2023fev17-corridahib-b23-36-cid23-607-snapshot.B23-36.CID23-607.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '24a-cid23-607',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1092-snapshot/2022mai11-oc180-run0-b22-60-cid22-1092-snapshot.B22-60.CID22-1092.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1092-snapshot/2022mai11-oc180-run0-b22-60-cid22-1092-snapshot.B22-60.CID22-1092.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '24b-cid22-1092',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-608-snapshot/2023fev17-corridahib-b23-36-cid23-608-snapshot.B23-36.CID23-608.B536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-608-snapshot/2023fev17-corridahib-b23-36-cid23-608-snapshot.B23-36.CID23-608.B536_N704.coverage.summary.htqc.txt',
                        'sample_id': '25a-cid23-608',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1093-snapshot/2022mai11-oc180-run0-b22-60-cid22-1093-snapshot.B22-60.CID22-1093.B536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1093-snapshot/2022mai11-oc180-run0-b22-60-cid22-1093-snapshot.B22-60.CID22-1093.B536_N704.coverage.summary.htqc.txt',
                        'sample_id': '25b-cid22-1093',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-609-snapshot/2023fev17-corridahib-b23-36-cid23-609-snapshot.B23-36.CID23-609.B537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-609-snapshot/2023fev17-corridahib-b23-36-cid23-609-snapshot.B23-36.CID23-609.B537_N705.coverage.summary.htqc.txt',
                        'sample_id': '26a-cid23-609',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1094-snapshot/2022mai11-oc180-run0-b22-60-cid22-1094-snapshot.B22-60.CID22-1094.B537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1094-snapshot/2022mai11-oc180-run0-b22-60-cid22-1094-snapshot.B22-60.CID22-1094.B537_N705.coverage.summary.htqc.txt',
                        'sample_id': '26b-cid22-1094',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-610-snapshot/2023fev17-corridahib-b23-36-cid23-610-snapshot.B23-36.CID23-610.B539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-610-snapshot/2023fev17-corridahib-b23-36-cid23-610-snapshot.B23-36.CID23-610.B539_N707.coverage.summary.htqc.txt',
                        'sample_id': '27a-cid23-610',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1096-snapshot/2022mai11-oc180-run0-b22-60-cid22-1096-snapshot.B22-60.CID22-1096.B539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1096-snapshot/2022mai11-oc180-run0-b22-60-cid22-1096-snapshot.B22-60.CID22-1096.B539_N707.coverage.summary.htqc.txt',
                        'sample_id': '27b-cid22-1096',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-611-snapshot/2023fev17-corridahib-b23-36-cid23-611-snapshot.B23-36.CID23-611.B538_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-611-snapshot/2023fev17-corridahib-b23-36-cid23-611-snapshot.B23-36.CID23-611.B538_N706.coverage.summary.htqc.txt',
                        'sample_id': '28a-cid23-611',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1095-snapshot/2022mai11-oc180-run0-b22-60-cid22-1095-snapshot.B22-60.CID22-1095.B538_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1095-snapshot/2022mai11-oc180-run0-b22-60-cid22-1095-snapshot.B22-60.CID22-1095.B538_N706.coverage.summary.htqc.txt',
                        'sample_id': '28b-cid22-1095',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-612-snapshot/2023fev17-corridahib-b23-36-cid23-612-snapshot.B23-36.CID23-612.B540_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-612-snapshot/2023fev17-corridahib-b23-36-cid23-612-snapshot.B23-36.CID23-612.B540_N708.coverage.summary.htqc.txt',
                        'sample_id': '29a-cid23-612',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1097-snapshot/2022mai11-oc180-run0-b22-60-cid22-1097-snapshot.B22-60.CID22-1097.B540_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1097-snapshot/2022mai11-oc180-run0-b22-60-cid22-1097-snapshot.B22-60.CID22-1097.B540_N708.coverage.summary.htqc.txt',
                        'sample_id': '29b-cid22-1097',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-613-snapshot/2023fev17-corridahib-b23-36-cid23-613-snapshot.B23-36.CID23-613.B543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc180snps//2023fev17-corridahib-b23-36-cid23-613-snapshot/2023fev17-corridahib-b23-36-cid23-613-snapshot.B23-36.CID23-613.B543_N703.coverage.summary.htqc.txt',
                        'sample_id': '30a-cid23-613',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1100-snapshot/2022mai11-oc180-run0-b22-60-cid22-1100-snapshot.B22-60.CID22-1100.B543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395/oc180snps//2022mai11-oc180-run0-b22-60-cid22-1100-snapshot/2022mai11-oc180-run0-b22-60-cid22-1100-snapshot.B22-60.CID22-1100.B543_N703.coverage.summary.htqc.txt',
                        'sample_id': '30b-cid22-1100',
                        'sample_type': ''}
                ]

            }
        }
    }
