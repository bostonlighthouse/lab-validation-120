"""
Get the VCF / QC files for the GS180snps and GS395 hybrid approach
Here we used 36 GS180 Snps and 4 GS395 samples
"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_hybrid_oc395_4_samples_vs_original_oc395_4_samples_reproducibility():
    """
    Get the VCF files for GS180snps and GS395 hybrid approach
    4 GS395 files
    """

    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/hybrid_oc395_4_samples_vs_original_oc395_4_samples_reproducibility",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    'scratch/hybrid_approach_oc180snps_and_oc395/run1/36_samples_oc180snp_4_samples_oc395/oc395/hybrid_oc395_4_samples_vs_original_oc395_4_samples_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                    'scratch/hybrid_approach_oc180snps_and_oc395/run1/36_samples_oc180snp_4_samples_oc395/oc395/hybrid_oc395_4_samples_vs_original_oc395_4_samples_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                ],
                "indel": [
                    'scratch/hybrid_approach_oc180snps_and_oc395/run1/36_samples_oc180snp_4_samples_oc395/oc395/hybrid_oc395_4_samples_vs_original_oc395_4_samples_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                    'scratch/hybrid_approach_oc180snps_and_oc395/run1/36_samples_oc180snp_4_samples_oc395/oc395/hybrid_oc395_4_samples_vs_original_oc395_4_samples_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                ]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file":  [
                        'scratch/hybrid_approach_oc180snps_and_oc395/run1/36_samples_oc180snp_4_samples_oc395/oc395/hybrid_oc395_4_samples_vs_original_oc395_4_samples_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                        'scratch/hybrid_approach_oc180snps_and_oc395/run1/36_samples_oc180snp_4_samples_oc395/oc395/hybrid_oc395_4_samples_vs_original_oc395_4_samples_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        'scratch/hybrid_approach_oc180snps_and_oc395/run1/36_samples_oc180snp_4_samples_oc395/oc395/hybrid_oc395_4_samples_vs_original_oc395_4_samples_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                        'scratch/hybrid_approach_oc180snps_and_oc395/run1/36_samples_oc180snp_4_samples_oc395/oc395/hybrid_oc395_4_samples_vs_original_oc395_4_samples_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                    ],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS395 Hybrid",
                "y_axis_label": "GS395 Original",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS395 Hybrid",
                "y_axis_label": "GS395 Original",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS395 Hybrid",
                "y_axis_label": "GS395 Original",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            #"vaf_threshold": 10.0,
            #"depth_threshold": 100,

            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid23-137',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-137-snapshot/2023jan28-oc395-oc18-b23-17-cid23-137-snapshot.B23-17.CID23-137.C515_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '1b-cid22-2634',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022dez06-run04-gsin-b22-150-cid22-2634-snapshot/2022dez06-run04-gsin-b22-150-cid22-2634-snapshot.B22-150.CID22-2634.C515_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample2': [{'sample_id': '2a-cid23-138',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-138-snapshot/2023jan28-oc395-oc18-b23-17-cid23-138-snapshot.B23-17.CID23-138.B541_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '2b-cid22-2558',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022nov21-run03-gsin-b22-144-cid22-2558-snapshot/2022nov21-run03-gsin-b22-144-cid22-2558-snapshot.B22-144.CID22-2558.B518_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample3': [{'sample_id': '3a-cid23-184',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-184-snapshot/2023jan28-oc395-oc18-b23-17-cid23-184-snapshot.B23-17.CID23-184.B518_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '3b-cid22-2749',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022dez30-run06-gsin-b22-162-cid22-2749-snapshot/2022dez30-run06-gsin-b22-162-cid22-2749-snapshot.B22-162.CID22-2749.B518_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample4': [{'sample_id': '4a-cid23-340',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-340-snapshot/2023jan28-oc395-oc18-b23-17-cid23-340-snapshot.B23-17.CID23-340.A521_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '4b-cid22-1239',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022jun02-oc395-run0-b22-71-cid22-1239-snapshot/2022jun02-oc395-run0-b22-71-cid22-1239-snapshot.B22-71.CID22-1239.A521_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}]
            }
        }
    }


def get_hybrid_oc395_4_samples_vs_original_oc395_4_samples_htqc_plots():
    """
    Get the QC files for GS180snps and GS395 hybrid approach
    4 GS395 files
    """

    key_2_use = 'hybrid_oc395_4_samples_vs_original_oc395_4_samples_reproducibility'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS395 Hybrid (a) vs GS395 Original (b) 4/4 samples passed MCC",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-137-snapshot/2023jan28-oc395-oc18-b23-17-cid23-137-snapshot.B23-17.CID23-137.C515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-137-snapshot/2023jan28-oc395-oc18-b23-17-cid23-137-snapshot.B23-17.CID23-137.C515_N707.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid23-137',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022dez06-run04-gsin-b22-150-cid22-2634-snapshot/2022dez06-run04-gsin-b22-150-cid22-2634-snapshot.B22-150.CID22-2634.C515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022dez06-run04-gsin-b22-150-cid22-2634-snapshot/2022dez06-run04-gsin-b22-150-cid22-2634-snapshot.B22-150.CID22-2634.C515_N707.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid22-2634',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-138-snapshot/2023jan28-oc395-oc18-b23-17-cid23-138-snapshot.B23-17.CID23-138.B541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-138-snapshot/2023jan28-oc395-oc18-b23-17-cid23-138-snapshot.B23-17.CID23-138.B541_N701.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid23-138',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022nov21-run03-gsin-b22-144-cid22-2558-snapshot/2022nov21-run03-gsin-b22-144-cid22-2558-snapshot.B22-144.CID22-2558.B518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022nov21-run03-gsin-b22-144-cid22-2558-snapshot/2022nov21-run03-gsin-b22-144-cid22-2558-snapshot.B22-144.CID22-2558.B518_N702.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid22-2558',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-184-snapshot/2023jan28-oc395-oc18-b23-17-cid23-184-snapshot.B23-17.CID23-184.B518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-184-snapshot/2023jan28-oc395-oc18-b23-17-cid23-184-snapshot.B23-17.CID23-184.B518_N702.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid23-184',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022dez30-run06-gsin-b22-162-cid22-2749-snapshot/2022dez30-run06-gsin-b22-162-cid22-2749-snapshot.B22-162.CID22-2749.B518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022dez30-run06-gsin-b22-162-cid22-2749-snapshot/2022dez30-run06-gsin-b22-162-cid22-2749-snapshot.B22-162.CID22-2749.B518_N702.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid22-2749',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-340-snapshot/2023jan28-oc395-oc18-b23-17-cid23-340-snapshot.B23-17.CID23-340.A521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2023jan28-oc395-oc18-b23-17-cid23-340-snapshot/2023jan28-oc395-oc18-b23-17-cid23-340-snapshot.B23-17.CID23-340.A521_N705.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid23-340',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022jun02-oc395-run0-b22-71-cid22-1239-snapshot/2022jun02-oc395-run0-b22-71-cid22-1239-snapshot.B22-71.CID22-1239.A521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/36_samples_oc180snp_4_samples_oc395/oc395//2022jun02-oc395-run0-b22-71-cid22-1239-snapshot/2022jun02-oc395-run0-b22-71-cid22-1239-snapshot.B22-71.CID22-1239.A521_N705.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid22-1239',
                        'sample_type': ''}
                ]
            }
        }
    }
