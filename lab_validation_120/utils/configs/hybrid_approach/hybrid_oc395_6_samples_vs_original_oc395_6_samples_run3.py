"""
Get the VCF / QC files for the GS180snps and GS395 hybrid approach (RUN 3)
Here we used 30 GS180 Snps and 6 GS395 samples (RUN 3)
"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility_run3():
    """
    Get the VCF files for GS180snps and GS395 hybrid approach
    6 GS395 files (RUN 3)
    """

    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility_run3",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc395/hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter.txt_and_intra.txt",
                    "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc395/hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3_vaf_5.0_depth_100_all_impacts_snvs_both_inter.txt_and_intra.txt"
                ],
                "indel": [
                    "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc395/hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt",
                    "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc395/hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt"
                ]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file":  [
                        "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc395/hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter.txt_and_intra.txt",
                        "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc395/hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3_vaf_5.0_depth_100_all_impacts_snvs_both_inter.txt_and_intra.txt"
                    ],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc395/hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt",
                        "scratch/hybrid_approach_oc180snps_and_oc395/run3/30_samples_oc180snp_6_samples_oc395/oc395/hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt"
                    ],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS395 Hybrid run3",
                "y_axis_label": "GS395 Original",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS395 Hybrid run3",
                "y_axis_label": "GS395 Original",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS395 Hybrid run3",
                "y_axis_label": "GS395 Original",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            #"vaf_threshold": 10.0,
            #"depth_threshold": 100,

            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid23-578',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-578-snapshot/2023fev17-corridahib-b23-34-cid23-578-snapshot.B23-34.CID23-578.A529_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '1b-cid22-1249',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1249-snapshot/2022jun02-oc395-run0-b22-71-cid22-1249-snapshot.B22-71.CID22-1249.A529_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample2': [{'sample_id': '2a-cid23-579',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-579-snapshot/2023fev17-corridahib-b23-34-cid23-579-snapshot.B23-34.CID23-579.A530_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '2b-cid22-1251',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1251-snapshot/2022jun02-oc395-run0-b22-71-cid22-1251-snapshot.B22-71.CID22-1251.A530_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample3': [{'sample_id': '3a-cid23-580',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-580-snapshot/2023fev17-corridahib-b23-34-cid23-580-snapshot.B23-34.CID23-580.A518_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '3b-cid22-1233',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1233-snapshot/2022jun02-oc395-run0-b22-71-cid22-1233-snapshot.B22-71.CID22-1233.A518_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample4': [{'sample_id': '4a-cid23-581',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-581-snapshot/2023fev17-corridahib-b23-34-cid23-581-snapshot.B23-34.CID23-581.A520_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '4b-cid22-1237',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1237-snapshot/2022jun02-oc395-run0-b22-71-cid22-1237-snapshot.B22-71.CID22-1237.A520_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample5': [{'sample_id': '5a-cid23-582',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-582-snapshot/2023fev17-corridahib-b23-34-cid23-582-snapshot.B23-34.CID23-582.A519_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '5b-cid22-1235',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1235-snapshot/2022jun02-oc395-run0-b22-71-cid22-1235-snapshot.B22-71.CID22-1235.A519_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                # GS180 sample
                #'inter_Sample6': [{'sample_id': '6a-cid23-583',
                #                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-583-snapshot/2023fev17-corridahib-b23-34-cid23-583-snapshot.B23-34.CID23-583.A521_N705.merged_concatenated.vcf',
                #                   'variant_type': 'snvs'},
                #                  {'sample_id': '6b-cid22-865',
                #                   'vcf_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022abr22-gs180-run1-b22-51-cid22-865-snapshot/2022abr22-gs180-run1-b22-51-cid22-865-snapshot.B22-51.CID22-865.C505_N705.merged_concatenated.vcf',
                #                   'variant_type': 'snvs'},
                #                  {'experiment_type': 'inter-run'}]
            }
        }
    }


def get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_htqc_plots_run3():
    """
    Get the QC files for GS180snps and GS395 hybrid approach
    6 GS395 files (RUN 3)
    """

    key_2_use = 'hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility_run3'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS395 Hybrid (a) vs GS395 Original (b) 5/6 samples passed MCC, three original were GS180,"
                               " so they were removed, Total 2 - RUN2",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-578-snapshot/2023fev17-corridahib-b23-34-cid23-578-snapshot.B23-34.CID23-578.A529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-578-snapshot/2023fev17-corridahib-b23-34-cid23-578-snapshot.B23-34.CID23-578.A529_N705.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid23-578',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1249-snapshot/2022jun02-oc395-run0-b22-71-cid22-1249-snapshot.B22-71.CID22-1249.A529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1249-snapshot/2022jun02-oc395-run0-b22-71-cid22-1249-snapshot.B22-71.CID22-1249.A529_N705.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid22-1249',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-579-snapshot/2023fev17-corridahib-b23-34-cid23-579-snapshot.B23-34.CID23-579.A530_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-579-snapshot/2023fev17-corridahib-b23-34-cid23-579-snapshot.B23-34.CID23-579.A530_N706.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid23-579',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1251-snapshot/2022jun02-oc395-run0-b22-71-cid22-1251-snapshot.B22-71.CID22-1251.A530_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1251-snapshot/2022jun02-oc395-run0-b22-71-cid22-1251-snapshot.B22-71.CID22-1251.A530_N706.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid22-1251',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-580-snapshot/2023fev17-corridahib-b23-34-cid23-580-snapshot.B23-34.CID23-580.A518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-580-snapshot/2023fev17-corridahib-b23-34-cid23-580-snapshot.B23-34.CID23-580.A518_N702.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid23-580',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1233-snapshot/2022jun02-oc395-run0-b22-71-cid22-1233-snapshot.B22-71.CID22-1233.A518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1233-snapshot/2022jun02-oc395-run0-b22-71-cid22-1233-snapshot.B22-71.CID22-1233.A518_N702.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid22-1233',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-581-snapshot/2023fev17-corridahib-b23-34-cid23-581-snapshot.B23-34.CID23-581.A520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-581-snapshot/2023fev17-corridahib-b23-34-cid23-581-snapshot.B23-34.CID23-581.A520_N704.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid23-581',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1237-snapshot/2022jun02-oc395-run0-b22-71-cid22-1237-snapshot.B22-71.CID22-1237.A520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1237-snapshot/2022jun02-oc395-run0-b22-71-cid22-1237-snapshot.B22-71.CID22-1237.A520_N704.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid22-1237',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-582-snapshot/2023fev17-corridahib-b23-34-cid23-582-snapshot.B23-34.CID23-582.A519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-582-snapshot/2023fev17-corridahib-b23-34-cid23-582-snapshot.B23-34.CID23-582.A519_N703.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid23-582',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1235-snapshot/2022jun02-oc395-run0-b22-71-cid22-1235-snapshot.B22-71.CID22-1235.A519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022jun02-oc395-run0-b22-71-cid22-1235-snapshot/2022jun02-oc395-run0-b22-71-cid22-1235-snapshot.B22-71.CID22-1235.A519_N703.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid22-1235',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-583-snapshot/2023fev17-corridahib-b23-34-cid23-583-snapshot.B23-34.CID23-583.A521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2023fev17-corridahib-b23-34-cid23-583-snapshot/2023fev17-corridahib-b23-34-cid23-583-snapshot.B23-34.CID23-583.A521_N705.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid23-583',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022abr22-gs180-run1-b22-51-cid22-865-snapshot/2022abr22-gs180-run1-b22-51-cid22-865-snapshot.B22-51.CID22-865.C505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/hybrid_approach_oc180snps_and_oc395/30_samples_oc180snp_6_samples_oc395_run3/oc395//2022abr22-gs180-run1-b22-51-cid22-865-snapshot/2022abr22-gs180-run1-b22-51-cid22-865-snapshot.B22-51.CID22-865.C505_N705.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid22-865',
                        'sample_type': ''}
                ]
            }
        }
    }
