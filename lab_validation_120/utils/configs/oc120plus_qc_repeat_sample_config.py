"""Simple config for the OC120plus repeat samples analysis"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_oc120plus_qc_repeat_sample_reproducibility():
    """
    Get the VCF files for the repeat samples used for QC
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/oc120plus_qc_repeat_samples",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": ['scratch/oc120plus/QC/oc120plus_qc_repeat_samples_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                        'scratch/oc120plus/QC/oc120plus_qc_repeat_samples_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'],
                "indel": ['scratch/oc120plus/QC/oc120plus_qc_repeat_samples_vaf_8.0_depth_100_consequential_impacts_only_indels_size_5_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                          'scratch/oc120plus/QC/oc120plus_qc_repeat_samples_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'],
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file":  ['scratch/oc120plus/QC/oc120plus_qc_repeat_samples_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                              'scratch/oc120plus/QC/oc120plus_qc_repeat_samples_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": ['scratch/oc120plus/QC/oc120plus_qc_repeat_samples_vaf_8.0_depth_100_consequential_impacts_only_indels_size_5_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                             'scratch/oc120plus/QC/oc120plus_qc_repeat_samples_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            #"vaf_threshold": 10.0,
            #"depth_threshold": 100,
            # Go over and look at the samples below, in the concordance_files
            # the oc120plus original sample is the first VCF in each entry
            # oc120plus repeat sample is the second VCF  in each entry
            "concordance_files": {
                'inter_Sample1': [    {    'sample_id': '1a-cid21-448',
                                           'variant_type': 'snvs',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-04-08-oc120-run-b21-45-cid21-448-snapshot/2021-04-08-oc120-run-b21-45-cid21-448-snapshot.B21-45.CID21-448.B512_N704.merged_concatenated.vcf'},
                                      {    'sample_id': '1b-cid21-547',
                                           'variant_type': 'snvs',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-04-23-gs180-run-b21-49-cid21-547-snapshot/2021-04-23-gs180-run-b21-49-cid21-547-snapshot.B21-49.CID21-547.B532_N703.merged_concatenated.vcf'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample2': [    {    'sample_id': '2a-cid21-500',
                                           'variant_type': 'snvs',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-04-23-gs180-run-b21-51-cid21-500-snapshot/2021-04-23-gs180-run-b21-51-cid21-500-snapshot.B21-51.CID21-500.B537_N708.merged_concatenated.vcf'},
                                      {    'sample_id': '2b-cid21-600',
                                           'variant_type': 'snvs',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-04-30-oc180-run-b21-53-cid21-600-snapshot/2021-04-30-oc180-run-b21-53-cid21-600-snapshot.B21-53.CID21-600.B501_N701.merged_concatenated.vcf'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample3': [    {    'sample_id': '3a-cid21-579',
                                           'variant_type': 'snvs',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-04-30-oc180-run-b21-53-cid21-579-snapshot/2021-04-30-oc180-run-b21-53-cid21-579-snapshot.B21-53.CID21-579.B503_N703.merged_concatenated.vcf'},
                                      {    'sample_id': '3b-cid21-602',
                                           'variant_type': 'snvs',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-05-07-oc180-run-b21-57-cid21-602-snapshot/2021-05-07-oc180-run-b21-57-cid21-602-snapshot.B21-57.CID21-602.B525_N701.merged_concatenated.vcf'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample4': [    {    'sample_id': '4a-cid21-624',
                                           'variant_type': 'snvs',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-05-15-oc180-run-b21-65-cid21-624-snapshot/2021-05-15-oc180-run-b21-65-cid21-624-snapshot.B21-65.CID21-624.B536_N704.merged_concatenated.vcf'},
                                      {    'sample_id': '4b-cid21-763',
                                           'variant_type': 'snvs',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-05-21-oc180-run-b21-67-cid21-763-snapshot/2021-05-21-oc180-run-b21-67-cid21-763-snapshot.B21-67.CID21-763.B533_N701.merged_concatenated.vcf'},
                                      {'experiment_type': 'inter-run'}]
            }
        }
    }




def get_oc120plus_qc_repeat_sample_htqc_plots():
    """
    Get the coverage files
    """
    key_2_use = 'oc120plus_qc_repeat_samples'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "oc120plus QC Repeat Samples",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-04-08-oc120-run-b21-45-cid21-448-snapshot/2021-04-08-oc120-run-b21-45-cid21-448-snapshot.B21-45.CID21-448.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-04-08-oc120-run-b21-45-cid21-448-snapshot/2021-04-08-oc120-run-b21-45-cid21-448-snapshot.B21-45.CID21-448.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid21-448',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-04-23-gs180-run-b21-49-cid21-547-snapshot/2021-04-23-gs180-run-b21-49-cid21-547-snapshot.B21-49.CID21-547.B532_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-04-23-gs180-run-b21-49-cid21-547-snapshot/2021-04-23-gs180-run-b21-49-cid21-547-snapshot.B21-49.CID21-547.B532_N703.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid21-547',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-04-23-gs180-run-b21-51-cid21-500-snapshot/2021-04-23-gs180-run-b21-51-cid21-500-snapshot.B21-51.CID21-500.B537_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-04-23-gs180-run-b21-51-cid21-500-snapshot/2021-04-23-gs180-run-b21-51-cid21-500-snapshot.B21-51.CID21-500.B537_N708.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid21-500',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-04-30-oc180-run-b21-53-cid21-600-snapshot/2021-04-30-oc180-run-b21-53-cid21-600-snapshot.B21-53.CID21-600.B501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-04-30-oc180-run-b21-53-cid21-600-snapshot/2021-04-30-oc180-run-b21-53-cid21-600-snapshot.B21-53.CID21-600.B501_N701.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid21-600',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-04-30-oc180-run-b21-53-cid21-579-snapshot/2021-04-30-oc180-run-b21-53-cid21-579-snapshot.B21-53.CID21-579.B503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-04-30-oc180-run-b21-53-cid21-579-snapshot/2021-04-30-oc180-run-b21-53-cid21-579-snapshot.B21-53.CID21-579.B503_N703.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid21-579',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-05-07-oc180-run-b21-57-cid21-602-snapshot/2021-05-07-oc180-run-b21-57-cid21-602-snapshot.B21-57.CID21-602.B525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-05-07-oc180-run-b21-57-cid21-602-snapshot/2021-05-07-oc180-run-b21-57-cid21-602-snapshot.B21-57.CID21-602.B525_N701.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid21-602',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-05-15-oc180-run-b21-65-cid21-624-snapshot/2021-05-15-oc180-run-b21-65-cid21-624-snapshot.B21-65.CID21-624.B536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples/2021-05-15-oc180-run-b21-65-cid21-624-snapshot/2021-05-15-oc180-run-b21-65-cid21-624-snapshot.B21-65.CID21-624.B536_N704.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid21-624',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-05-21-oc180-run-b21-67-cid21-763-snapshot/2021-05-21-oc180-run-b21-67-cid21-763-snapshot.B21-67.CID21-763.B533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Clinical_qc/Repeat_samples//2021-05-21-oc180-run-b21-67-cid21-763-snapshot/2021-05-21-oc180-run-b21-67-cid21-763-snapshot.B21-67.CID21-763.B533_N701.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid21-763',
                        'sample_type': ''}
                ]
            }
        }
    }