"""The configuration for Genosity comparison"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_genosity_vs_oc120plus_sample_recall():
    """
    Only function in the module
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "genosity_vs_oc120plus_sample_recall": {
            "run": "run1",
            "images_dir": "images/genosity_vs_oc120plus_sample_recall",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            #"final_overall_reproducibility_file": "lab_validation_120/performance_characteristics/reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_120plus_all_values.txt",
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                #"snv": f"clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_1.0_depth_50_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                "snv": None,
                #"indel": f"clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_1.0_depth_50_consequential_impacts_only_indels_size_1_1_both_inter_and_intra.txt"
                "indel": None,
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    #"file": f"clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_snv}_depth_{depth_threshold_snv}_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                    "file": None,
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    #"file": f"clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_indel}_depth_{depth_threshold_indel}_consequential_impacts_only_indels_size_{indel_thresholds[0][0]}_{indel_thresholds[0][1]}_both_inter_and_intra.txt",
                    "file": None,
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            #"vaf_threshold": 10.0,
            #"depth_threshold": 100,
            # Go over and look at the samples below, in the concordance_files
            "recall_files": {
                'Sample1': [{'sample_id': '1a-cid20-1205',
                             'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.merged_concatenated.vcf',
                             'variant_type': 'snvs'},
                            {'sample_id': '1b-POE20_138',
                             'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_138_GER2016612_D1_N1_LP201009248_xGen_UDI_Index_8_HP200587_PL200327_SEQ_2010220277.14885.variants.summary.tumor.comparison.tsv',
                             'variant_type': 'snvs'},
                            {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample2': [{'sample_id': '2a-cid22-1082',
                             'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1082-snapshot/2022mai11-oc180-run0-b22-60-cid22-1082-snapshot.B22-60.CID22-1082.B525_N701.merged_concatenated.vcf',
                             'variant_type': 'snvs'},
                            {'sample_id': '2b-POE20_167',
                             'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_167_GER2016613_D1_N1_LP201009248_xGen_UDI_Index_9_HP200587_PL200327_SEQ_2010220277.14896.variants.summary.tumor.comparison.tsv',
                             'variant_type': 'snvs'},
                            {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample3': [{'sample_id': '3a-cid22-1083',
                             'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1083-snapshot/2022mai11-oc180-run0-b22-60-cid22-1083-snapshot.B22-60.CID22-1083.B526_N702.merged_concatenated.vcf',
                             'variant_type': 'snvs'},
                            {'sample_id': '3b-POE20_107',
                             'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_107_GER2016614_D1_N1_LP201009248_xGen_UDI_Index_10_HP200587_PL200327_SEQ_2010220277.14886.variants.summary.tumor.comparison.tsv',
                             'variant_type': 'snvs'},
                            {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample4': [{'sample_id': '4a-cid22-1084',
                             'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1084-snapshot/2022mai11-oc180-run0-b22-60-cid22-1084-snapshot.B22-60.CID22-1084.B527_N703.merged_concatenated.vcf',
                             'variant_type': 'snvs'},
                            {'sample_id': '4b-POE20_148',
                             'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_148_GER2016617_D1_N1_LP201009248_xGen_UDI_Index_13_HP200588_PL200327_SEQ_2010220277.14898.variants.summary.tumor.comparison.tsv',
                             'variant_type': 'snvs'},
                            {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample5': [{'sample_id': '5a-cid22-1085',
                             'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1085-snapshot/2022mai11-oc180-run0-b22-60-cid22-1085-snapshot.B22-60.CID22-1085.B528_N704.merged_concatenated.vcf',
                             'variant_type': 'snvs'},
                            {'sample_id': '5b-POE20_195',
                             'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_195_GER2016620_D1_N1_LP201009248_xGen_UDI_Index_16_HP200589_PL200327_SEQ_2010220277.14887.variants.summary.tumor.comparison.tsv',
                             'variant_type': 'snvs'},
                            {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample6': [{'sample_id': '6a-cid22-1086',
                             'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1086-snapshot/2022mai11-oc180-run0-b22-60-cid22-1086-snapshot.B22-60.CID22-1086.B529_N705.merged_concatenated.vcf',
                             'variant_type': 'snvs'},
                            {'sample_id': '6b-POE20_200',
                             'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_200_GER2016621_D1_N1_LP201009248_xGen_UDI_Index_17_HP200589_PL200327_SEQ_2010220277.14890.variants.summary.tumor.comparison.tsv',
                             'variant_type': 'snvs'},
                            {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample7': [{'sample_id': '7a-cid20-1083',
                             'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.merged_concatenated.vcf',
                             'variant_type': 'snvs'},
                            {'sample_id': '7b-POE20_114',
                             'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_114_GER2016624_D1_N1_LP201009248_xGen_UDI_Index_20_HP200590_PL200327_SEQ_2010220277.14895.variants.summary.tumor.comparison.tsv',
                             'variant_type': 'snvs'},
                            {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample8': [{'sample_id': '8a-cid20-1213',
                             'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.merged_concatenated.vcf',
                             'variant_type': 'snvs'},
                            {'sample_id': '8b-POE20_203',
                             'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_203_GER2016628_D1_N1_LP201009248_xGen_UDI_Index_24_HP200591_PL200327_SEQ_2010220277.14897.variants.summary.tumor.comparison.tsv',
                             'variant_type': 'snvs'},
                            {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample9': [{'sample_id': '9a-cid20-1105',
                             'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.merged_concatenated.vcf',
                             'variant_type': 'snvs'},
                            {'sample_id': '9b-POE20_183',
                             'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_183_GER2016629_D1_N1_LP201009248_xGen_UDI_Index_25_HP200591_PL200327_SEQ_2010220277.14906.variants.summary.tumor.comparison.tsv',
                             'variant_type': 'snvs'},
                            {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample10': [{'sample_id': '10a-cid22-1091',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1091-snapshot/2022mai11-oc180-run0-b22-60-cid22-1091-snapshot.B22-60.CID22-1091.B534_N702.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '10b-POE20_216',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_216_GER2016632_D1_N1_LP201009248_xGen_UDI_Index_28_HP200592_PL200327_SEQ_2010220277.15095.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample11': [{'sample_id': '11a-cid20-1167',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2020nov12-oc120-run6-b20-126-cid20-1167-snapshot/2020nov12-oc120-run6-b20-126-cid20-1167-snapshot.B20-126.CID20-1167.B520_N704.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '11b-POE20_238',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_238_GER2020579_D1_N1_LP210118026_xGen_UDI_Index_1_HP210789_PL210515_SEQ_2101220026.17596.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample12': [{'sample_id': '12a-cid22-1092',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1092-snapshot/2022mai11-oc180-run0-b22-60-cid22-1092-snapshot.B22-60.CID22-1092.B535_N703.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '12b-POE20_291',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_291_GER2020580_D1_N1_LP210118026_xGen_UDI_Index_2_HP210789_PL210515_SEQ_2101220026.17628.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample13': [{'sample_id': '13a-cid22-1093',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1093-snapshot/2022mai11-oc180-run0-b22-60-cid22-1093-snapshot.B22-60.CID22-1093.B536_N704.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '13b-POE20_278',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_278_GER2020582_D1_N1_LP210118026_xGen_UDI_Index_4_HP210789_PL210515_SEQ_2101220026.17631.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample14': [{'sample_id': '14a-cid22-1094',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1094-snapshot/2022mai11-oc180-run0-b22-60-cid22-1094-snapshot.B22-60.CID22-1094.B537_N705.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '14b-POE20_175',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_175_GER2020583_D1_N1_LP210118026_xGen_UDI_Index_5_HP210790_PL210515_SEQ_2101220026.17632.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample15': [{'sample_id': '15a-cid22-1095',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1095-snapshot/2022mai11-oc180-run0-b22-60-cid22-1095-snapshot.B22-60.CID22-1095.B538_N706.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '15b-POE20_352',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_352_GER2020584_D1_N1_LP210118026_xGen_UDI_Index_6_HP210790_PL210515_SEQ_2101220026.17610.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample16': [{'sample_id': '16a-cid22-1096',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1096-snapshot/2022mai11-oc180-run0-b22-60-cid22-1096-snapshot.B22-60.CID22-1096.B539_N707.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '16b-POE20_239',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_239_GER2020585_D1_N1_LP210118026_xGen_UDI_Index_7_HP210790_PL210515_SEQ_2101220026.17594.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample17': [{'sample_id': '17a-cid22-1099',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1099-snapshot/2022mai11-oc180-run0-b22-60-cid22-1099-snapshot.B22-60.CID22-1099.B542_N702.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '17b-POE20_353',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_353_GER2020590_D1_N1_LP210118026_xGen_UDI_Index_11_HP210791_PL210515_SEQ_2101220026.17606.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample18': [{'sample_id': '18a-cid22-1100',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1100-snapshot/2022mai11-oc180-run0-b22-60-cid22-1100-snapshot.B22-60.CID22-1100.B543_N703.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '18b-POE20_83',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_83_GER2020591_D1_N1_LP210118026_xGen_UDI_Index_12_HP210791_PL210515_SEQ_2101220026.17614.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample19': [{'sample_id': '19a-cid22-1101',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1101-snapshot/2022mai11-oc180-run0-b22-60-cid22-1101-snapshot.B22-60.CID22-1101.B544_N704.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '19b-POE20_111',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_111_GER2020592_D1_N1_LP210118026_xGen_UDI_Index_13_HP210795_PL210515_SEQ_2101220026.17611.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample20': [{'sample_id': '20a-cid22-1102',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1102-snapshot/2022mai11-oc180-run0-b22-60-cid22-1102-snapshot.B22-60.CID22-1102.B545_N705.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '20b-POE20_282',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_282_GER2020593_D1_N1_LP210118026_xGen_UDI_Index_14_HP210795_PL210515_SEQ_2101220026.17624.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample21': [{'sample_id': '21a-cid22-1103',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1103-snapshot/2022mai11-oc180-run0-b22-60-cid22-1103-snapshot.B22-60.CID22-1103.B546_N706.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '21b-POE20_344',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_344_GER2020596_D1_N1_LP210118026_xGen_UDI_Index_17_HP210796_PL210515_SEQ_2101220026.17621.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample22': [{'sample_id': '22a-cid22-1104',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1104-snapshot/2022mai11-oc180-run0-b22-60-cid22-1104-snapshot.B22-60.CID22-1104.B547_N707.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '22b-POE20_149',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_149_GER2020597_D1_N1_LP210118026_xGen_UDI_Index_18_HP210796_PL210515_SEQ_2101220026.17629.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample23': [{'sample_id': '23a-cid22-1105',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai11-oc180-run0-b22-60-cid22-1105-snapshot/2022mai11-oc180-run0-b22-60-cid22-1105-snapshot.B22-60.CID22-1105.B548_N708.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '23b-POE20_247',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_247_GER2020598_D1_N1_LP210118026_xGen_UDI_Index_19_HP210796_PL210515_SEQ_2101220026.17630.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample24': [{'sample_id': '24a-cid22-1106',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2022mai13-gs180-run1-b22-63-cid22-1106-snapshot/2022mai13-gs180-run1-b22-63-cid22-1106-snapshot.B22-63.CID22-1106.B517_N705.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '24b-POE20_271',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_271_GER2020599_D1_N1_LP210118026_xGen_UDI_Index_20_HP210796_PL210515_SEQ_2101220026.17633.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample25': [{'sample_id': '25a-cid20-1110',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '25b-POE20_222',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_222_GER2020600_D1_N1_LP210118026_xGen_UDI_Index_21_HP210797_PL210515_SEQ_2101220026.17650.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}],
                'Sample26': [{'sample_id': '26a-cid21-1552',
                              'oc_vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples//2021ago23-oc180-run0-b21-110-cid21-1552-snapshot/2021ago23-oc180-run0-b21-110-cid21-1552-snapshot.B21-110.CID21-1552.A541_N701.merged_concatenated.vcf',
                              'variant_type': 'snvs'},
                             {'sample_id': '26b-POE20_338',
                              'genosity_overlap_variant_file': 'lab_validation_120/external_data_sources/genosity/POE20_338_GER2020604_D1_N1_LP210118026_xGen_UDI_Index_25_HP210798_PL210515_SEQ_2101220026.17623.variants.summary.tumor.comparison.tsv',
                              'variant_type': 'snvs'},
                             {'experiment_type': 'oc120plus_recall_vs_genosity'}]
            }
        }
    }
