def get_concordance_horizon_samples():
    """
    Get the horizon samples concordance configuration for concordance analysis
    """
    return {
        "concordance_horizon_samples": {
            "run": "run1",
            "images_dir": "images/concordance_horizon",
            "vaf_regression_plot": {
                "x_axis_label": "OC120",
                "y_axis_label": "Horizon",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "concordance_files":
                [
                    {
                        # http://vv.cgs.bostonlighthouse.us/varvetter/prod/snapshot2/tumor/clinical/2020-04-28-oc120-run-b20-30-cid20-115-snapshot.B20-30.CID20-115.A508_N703
                        "sample_id": "hd798a",
                        "horizon_file": "lab_validation_120/external_data_sources/horizon_gold_sets/hd798_oc120.txt",
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Concordance/2020-04-28-oc120-run-b20-30-cid20-115-snapshot/2020-04-28-oc120-run-b20-30-cid20-115-snapshot.B20-30.CID20-115.A508_N703.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # http://vv.cgs.bostonlighthouse.us/varvetter/prod/snapshot2/tumor/clinical/2020-05-08-oc120-run-b20-33-cid20-129-snapshot.B20-33.CID20-129.A517_N703
                    {
                        "sample_id": "hd798a",
                        "horizon_file": "lab_validation_120/external_data_sources/horizon_gold_sets/hd798_oc120.txt",
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Concordance/2020-05-08-oc120-run-b20-33-cid20-129-snapshot/2020-05-08-oc120-run-b20-33-cid20-129-snapshot.B20-33.CID20-129.A517_N703.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # http://vv.cgs.bostonlighthouse.us/varvetter/prod/snapshot2/tumor/clinical/2020-05-29-oc120-run-b20-38-cid20-186-snapshot.B20-38.CID20-186.C523_N707
                    {
                        "sample_id": "hd798b",
                        "horizon_file": "lab_validation_120/external_data_sources/horizon_gold_sets/hd798_oc120.txt",
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Concordance/2020-05-29-oc120-run-b20-38-cid20-186-snapshot/2020-05-29-oc120-run-b20-38-cid20-186-snapshot.B20-38.CID20-186.C523_N707.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                ]
        }
    }


def get_concordance_horizon_samples_qc_plots():
    """
    Get the horizon samples concordance configuration for HTQc plotting
    """

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "concordance_horizon_samples_qc_plots": {
            "run": "run1",
            "images_dir": "images/concordance_horizon",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 800,
                    'y_upper_lim': 1200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 6000,
                    'y_upper_lim': 1200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 250,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1100,
                    'y_upper_lim': 1300,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "input_files": {
                "group_id": "Horizon Samples HTQC RUN1",
                #"chart_title": None,  # not using this here
                "chart_title": "Horizon Samples RUN1 (XXXXXX BP Covered)",
                "data":
                    [
                        # Ended up removing this sample after speaking with Bruno, since it failed
                        #{
                        #    "sample_id": "hd798a",
                        #    "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Concordance/2020-04-28-oc120-run-b20-30-cid20-115-snapshot/2020-04-28-oc120-run-b20-30-cid20-115-snapshot.B20-30.CID20-115.A508_N703.coverage.summary.htqc.txt",
                        #    "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Concordance/2020-04-28-oc120-run-b20-30-cid20-115-snapshot/2020-04-28-oc120-run-b20-30-cid20-115-snapshot.B20-30.CID20-115.A508_N703.coverage.htqc.txt",
                        #    "sample_type": "horizon",  # intentionally left blank throughout this list of dictionaries below
                        #},
                        {
                            "sample_id": "hd798a",
                            "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Concordance/2020-05-08-oc120-run-b20-33-cid20-129-snapshot/2020-05-08-oc120-run-b20-33-cid20-129-snapshot.B20-33.CID20-129.A517_N703.coverage.summary.htqc.txt",
                            "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Concordance/2020-05-08-oc120-run-b20-33-cid20-129-snapshot/2020-05-08-oc120-run-b20-33-cid20-129-snapshot.B20-33.CID20-129.A517_N703.coverage.htqc.txt",
                            "sample_type": "horizon",  # intentionally left blank throughout this list of dictionaries below
                        },
                        {
                            "sample_id": "hd798b",
                            "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Concordance/2020-05-29-oc120-run-b20-38-cid20-186-snapshot/2020-05-29-oc120-run-b20-38-cid20-186-snapshot.B20-38.CID20-186.C523_N707.coverage.summary.htqc.txt",
                            "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Concordance/2020-05-29-oc120-run-b20-38-cid20-186-snapshot/2020-05-29-oc120-run-b20-38-cid20-186-snapshot.B20-38.CID20-186.C523_N707.coverage.htqc.txt",
                            "sample_type": "horizon",
                        },
                    ]
            }
        }
    }


def get_list_hd827_genes():
    """HD827 covers this list of genes

    OncoSpan is a well-characterised, cell line-derived Reference Standard containing 386 variants across 152 key cancer
    genes. This includes 249 variants with a COSMIC ID and 30 INDELs (24 deletions and 6 insertions, ranging from 2-16
    base pairs). Variants are present between 1-100% allelic frequency (AF), with 52 variants present at ≤ 20% AF for
    LOD determination of your assay.

    https://horizondiscovery.com/en/products/diagnostic-reference-standards/PIFs/OncoSpan-gDNA
    https://horizondiscovery.com/en/products/diagnostic-reference-standards/PIFs/-/media/C0411632E83F48A3B7EE5F488112F42F.ashx

    return list 152 genes
    """
    genes = ['ABL1', 'AKT1', 'AKT2', 'ALK', 'APC', 'AR', 'ARID1A', 'ATR', 'ATRX', 'AXL', 'BARD1', 'BCL6', 'BLM',
             'BMPR1A', 'BRAF', 'BRCA1', 'BRCA2', 'BTK', 'BUB1B', 'CARD11', 'CCND1', 'CCND3', 'CCNE1', 'CD79B', 'CDH1',
             'CDK12', 'CDK4', 'CEP57', 'CFH', 'CREBBP', 'CSF1R', 'CTNNB1', 'DDR2', 'DIS3L2', 'DNMT3A', 'EGFR', 'EML4',
             'EP300', 'EPCAM', 'ERBB2', 'ERBB3', 'ERCC1', 'ERCC2', 'ERCC4', 'ERCC5', 'ERG', 'ETS1', 'ETV4', 'EWSR1',
             'EXT1', 'FANCA', 'FANCD2', 'FANCE', 'FANCG', 'FANCI', 'FANCM', 'FBXW7', 'FGF10', 'FGF2', 'FGF3', 'FGF6',
             'FGFR1', 'FGFR3', 'FLCN', 'FLI1', 'FLT1', 'FLT3', 'FZR1', 'GATA2', 'GATA3', 'GEN1', 'GNA11', 'GNAS',
             'HNF1A', 'HRAS', 'IDH1', 'IDH2', 'JAK1', 'JAK2', 'JAK3', 'KDR', 'KIT', 'KRAS', 'LDLR', 'MAGI1', 'MAP2K1',
             'MAP2K2', 'MAX', 'MDM4', 'MED12', 'MET', 'MLH1', 'MLLT3', 'MMAB', 'MRE11', 'MSH2', 'MSH3', 'MSH6', 'MTOR',
             'NBN', 'NF1', 'NFE2L2', 'NOTCH1', 'NOTCH2', 'NOTCH3', 'NRAS', 'NRG1', 'NTRK1', 'NTRK3', 'PDGFRA', 'PDGFRB',
             'PIK3CA', 'PIK3CD', 'PIK3CG', 'PIK3R1', 'PMS2', 'PPARG', 'PPP2R2A', 'PRKAR1A', 'PROC', 'PTCH1', 'PTPN11',
             'RAD51B', 'RAD54L', 'RAF1', 'RB1', 'RBM45', 'RECQL4', 'RET', 'RHBDF2', 'ROS1', 'RPS6KB1', 'SDHB', 'SF3B1',
             'SF3B2', 'SLTM', 'SLX4', 'SMARCB1', 'SMO', 'SMOX', 'STK11', 'TERT', 'TET2', 'TFRC', 'TP53', 'TP53BP1',
             'TSC1', 'TSC2', 'WRN', 'XPA', 'XPC', 'ZNF395']

    assert len(genes) == 152
    return genes
