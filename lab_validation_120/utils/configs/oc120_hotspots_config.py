"""Config files for the amplicon metrics analysis"""

def get_tumor_amplicon_hotspot_metrics_oc120():

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "amplicon_metrics": {
            "run": "oc120_analysis",
            "images_dir": "images/amplicon_metrics/oc120",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data":[
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-435',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.hotspots.htqc.txt',
                        'sample_id': 'cid-440',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.hotspots.htqc.txt',
                        'sample_id': 'cid-594',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.hotspots.htqc.txt',
                        'sample_id': 'cid-4257',
                        'sample_type': 'tumor'
                    },

                    # the second 16 samples, we only can do 14 b/c of missing samples

                    # MCC was too low in original, keeping for data provenance
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-516-snapshot/2020jul29-oc120-run5-b20-69-cid20-516-snapshot.B20-69.CID20-516.C506_N706.hotspots.htqc.txt",
                    #     'sample_id': 'cid-516',
                    #     'sample_type': 'tumor'
                    # },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.hotspots.htqc.txt",
                        'sample_id': 'cid-576',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.hotspots.htqc.txt",
                        'sample_id': 'cid-334',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.hotspots.htqc.txt",
                        'sample_id': 'cid-640',
                        'sample_type': 'tumor'
                    },
                    # MCC was too low in original, keeping for data provenance
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-632-snapshot/2020-09-03-oc120-run-b20-86-cid20-632-snapshot.B20-86.CID20-632.B510_N702.hotspots.htqc.txt",
                    #     'sample_id': 'cid-632',
                    #     'sample_type': 'tumor'
                    # },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.hotspots.htqc.txt",
                        'sample_id': 'cid-620',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.hotspots.htqc.txt",
                        'sample_id': 'cid-692',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.hotspots.htqc.txt",
                        'sample_id': 'cid-829',
                        'sample_type': 'tumor'
                    },
                    # MCC was too low in original, keeping for data provenance
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-819-snapshot/2020set15-oc120-run5-b20-92-cid20-819-snapshot.B20-92.CID20-819.C504_N704.hotspots.htqc.txt",
                    #     'sample_id': 'cid-819',
                    #     'sample_type': 'tumor'
                    # },
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-893-snapshot/2020set15-oc120-run5-b20-92-cid20-893-snapshot.B20-92.CID20-893.C507_N707.hotspots.htqc.txt",
                    #     'sample_id': 'cid-893',
                    #     'sample_type': 'tumor'
                    # },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.hotspots.htqc.txt",
                        'sample_id': 'cid-841',
                        'sample_type': 'tumor'
                    },
                    # MCC was too low in original, keeping for data provenance
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-94-cid20-843-snapshot/2020set15-oc120-run5-b20-94-cid20-843-snapshot.B20-94.CID20-843.C509_N701.hotspots.htqc.txt",
                    #     'sample_id': 'cid-843',
                    #     'sample_type': 'tumor'
                    # },

                    #### NEW FILES Created by tools/create_oc120_vs_oc120plus_reproducibility_config_ds.py
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.hotspots.htqc.txt',
                        'sample_id': 'cid19-4524',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.hotspots.htqc.txt',
                        'sample_id': 'cid20-541',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid20-815',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid20-754',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-994',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.hotspots.htqc.txt',
                        'sample_id': 'cid20-624',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-1022',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid20-395',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-469',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid20-481',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid20-524',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid20-653',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-657',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid20-705',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid20-1024',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-936',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid20-946',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid20-1015',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-1000',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-972',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid20-974',
                        'sample_type': 'tumor'}
                ]
            }
        }
    }

def get_tumor_amplicon_hotspot_metrics_oc120plus():

    # Do not change yyyyyy his data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "amplicon_metrics": {
            "run": "oc120_analysis",
            "images_dir": "images/amplicon_metrics/oc120",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data":[
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.hotspots.htqc.txt",
                        'sample_id': 'cid20-1081',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.hotspots.htqc.txt",
                        'sample_id': 'cid20-1082',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.hotspots.htqc.txt",
                        'sample_id': 'cid20-1083',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.hotspots.htqc.txt",
                        'sample_id': 'cid20-1079',
                        'sample_type': 'tumor'
                    },

                    # the second 16 samples, we only can do 14 b/c of missing samples

                    # MCC was too low in original, keeping for the analysis, b/c OC120Plus does a better job
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1098-snapshot/2020out29-oc120-run6-b20-117-cid20-1098-snapshot.B20-117.CID20-1098.C501_N701.hotspots.htqc.txt",
                    #     'sample_id': 'cid20-1098',
                    #     'sample_type': 'tumor'
                    # },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1099-snapshot/2020out29-oc120-run6-b20-117-cid20-1099-snapshot.B20-117.CID20-1099.C502_N702.hotspots.htqc.txt",
                        'sample_id': 'cid20-1099',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.hotspots.htqc.txt",
                        'sample_id': 'cid20-1100',
                        'sample_type': 'tumor'
                    },
                    # didn't have original oc120 pair, but can use this
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1101-snapshot/2020out29-oc120-run6-b20-117-cid20-1101-snapshot.B20-117.CID20-1101.C504_N704.hotspots.htqc.txt",
                        'sample_id': 'cid20-1101',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.hotspots.htqc.txt",
                        'sample_id': 'cid20-1102',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.hotspots.htqc.txt",
                        'sample_id': 'cid20-1103',
                        'sample_type': 'tumor'
                    },
                    # MCC was too low in original, keeping for the analysis, b/c OC120Plus does a better job
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1104-snapshot/2020out29-oc120-run6-b20-117-cid20-1104-snapshot.B20-117.CID20-1104.C507_N707.hotspots.htqc.txt",
                    #     'sample_id': 'cid20-1104',
                    #     'sample_type': 'tumor'
                    # },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.hotspots.htqc.txt",
                        'sample_id': 'cid20-1105',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.hotspots.htqc.txt",
                        'sample_id': 'cid20-1106',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.hotspots.htqc.txt",
                        'sample_id': 'cid20-1107',
                        'sample_type': 'tumor'
                    },
                    # MCC was too low in original, keeping for the analysis, b/c OC120Plus does a better job
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1108-snapshot/2020out29-oc120-run6-b20-117-cid20-1108-snapshot.B20-117.CID20-1108.C511_N703.hotspots.htqc.txt",
                    #     'sample_id': 'cid20-1108',
                    #     'sample_type': 'tumor'
                    # },
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1109-snapshot/2020out29-oc120-run6-b20-117-cid20-1109-snapshot.B20-117.CID20-1109.C512_N707.hotspots.htqc.txt",
                    #     'sample_id': 'cid20-1109',
                    #     'sample_type': 'tumor'
                    # },
                    {
                        'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.hotspots.htqc.txt",
                        'sample_id': 'cid20-1110',
                        'sample_type': 'tumor'
                    },
                    # MCC was too low in original, keeping for the analysis, b/c OC120Plus does a better job
                    # {
                    #     'qc_htqc_file': "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1111-snapshot/2020out29-oc120-run6-b20-117-cid20-1111-snapshot.B20-117.CID20-1111.C514_N706.hotspots.htqc.txt",
                    #     'sample_id': 'cid20-1111',
                    #     'sample_type': 'tumor'
                    # },

                    #### NEW FILES Created by tools/create_oc120_vs_oc120plus_reproducibility_config_ds.py
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-1203',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid20-1204',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-1213',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid20-1214',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.hotspots.htqc.txt',
                        'sample_id': 'cid20-1215',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid20-1216',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid20-1217',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-1206',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid20-1205',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid20-1207',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid20-1208',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid20-1210',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-1211',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid20-1212',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid20-1163',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid20-1165',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid20-1168',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-1172',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-1174',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.hotspots.htqc.txt',
                        'sample_id': 'cid20-1241',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.hotspots.htqc.txt',
                        'sample_id': 'cid20-1240',
                        'sample_type': 'tumor'}
                ]
            }
        }
    }


