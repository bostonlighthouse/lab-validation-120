from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_clinical_sample_reproducibility():
    """
    Only function in the module
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 2.0
    depth_threshold_snv = 150
    vaf_threshold_indel = 4.0
    depth_threshold_indel = 200
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/reproducibility_clinical",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": "lab_validation_395/performance_characteristics/reproducibility/clinical_samples/clinical_sample_reproducibility_all_values.txt",
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": f"clinical_sample_reproducibility/clinical_sample_reproducibility_vaf_1.0_depth_50_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                "indel": f"clinical_sample_reproducibility/clinical_sample_reproducibility_vaf_1.0_depth_50_consequential_impacts_only_indels_size_1_1_both_inter_and_intra.txt"
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file": f"clinical_sample_reproducibility/clinical_sample_reproducibility_vaf_{vaf_threshold_snv}_depth_{depth_threshold_snv}_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": f"clinical_sample_reproducibility/clinical_sample_reproducibility_vaf_{vaf_threshold_indel}_depth_{depth_threshold_indel}_consequential_impacts_only_indels_size_{indel_thresholds[0][0]}_{indel_thresholds[0][1]}_both_inter_and_intra.txt",
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
    },
            "allele_count_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            #"vaf_threshold": 10.0,
            #"depth_threshold": 100,
            # Go over and look at the samples below, in the concordance_files
            "concordance_files": {
                "intra_RJA19-21876_F1": [
                    {
                        # CID20-199
                        "sample_id": "RJA19-21876_F1_1",
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120",
                    },
                    {
                        # CID20-201
                        "sample_id": "RJA19-21876_F1_2",
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "intra-run"
                    }
                ],
                "inter_RJA19-21876_F1": [
                    {
                        # CID20-199
                        "sample_id": "RJA19-21876_F1_1",
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-231
                        "sample_id": "RJA19-21876_F1_2",
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
            }
        }
    }


def get_concordance_clinical_samples_reproducibility_htqc_plots():
    """
    Get the clinical samples RAP concordance configuration for HTQc plotting
    """

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "concordance_clinical_samples_reproducibility_qc_plots": {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": "images/reproducibility_clinical",
            "input_files": {
                "group_id": "Clinical Sample from RAP HTQC RUN1&2",  # eventually used for a file name
                "chart_title": "Clinical Samples in Reproducibility Studies (RUN1&2)",
                "data": [
                    {
                        "sample_id": "28467-19_A_1-intra",  # used the sample name here not the CID, b.c. sample ID
                                                            # is not unique (can be sequenced more than once,
                                                            # but CID would have different values
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-205
                        "sample_id": "28467-19_A_2-intra",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120",
                        "sample_type": "",
                    },
                ]
            }
        }
    }