"""Config for the limit of detection experiment

HD827 - NA20-106
CID21-2009	HD827 - 100% positiva
CID21-2010	HD827 - 50% positiva + NA12878 - 50% negativa
CID21-2011	HD827 - 25% positiva + NA12878 - 75% negativa
CID21-2012	HD827 - 12,5% positiva + NA12878 - 87,5% negativa
CID21-2013	HD827 - 6,25% positiva + NA12878 - 93,75% negativa
CID21-2014	HD827 - 3,12% positiva + NA12878 - 96,88% negativa

POE22-31 - 100% positiva	CID22-538
POE22-31 - 50% positiva + POE21-945 - negativa	CID22-539
POE22-31 - 25% positiva + POE21-945 - negativa	CID22-540
POE22-31 - 12,5% positiva + POE21-945 - negativa	CID22-541
POE22-31 - 6,25% positiva + POE21-945 - negativa	CID22-542
POE22-31 - 3,12% positiva + POE21-945 - negativa	CID22-543
POE22-31 - 100% positiva	CID22-544
POE22-31 - 50% positiva + POE21-945 - negativa	CID22-545
POE22-31 - 25% positiva + POE21-945 - negativa	CID22-546
POE22-31 - 12,5% positiva + POE21-945 - negativa	CID22-547
POE22-31 - 6,25% positiva + POE21-945 - negativa	CID22-548
POE22-31 - 3,12% positiva + POE21-945 - negativa	CID22-549

"""


def _get_oc395primer_boost_horizon_snp_file():
    """Just return the string location of this file for oc395"""
    #return "lab_validation_120/external_data_sources/horizon_gold_sets/oc395/hd827_oc395_lod_vep104.txt"
    return "lab_validation_120/external_data_sources/horizon_gold_sets/hd827_for_lod.txt"


def _get_oc395primer_boost_horizon_indel_file():
    """Just return the string location of this file for oc395"""
    #return "lab_validation_120/external_data_sources/horizon_gold_sets/oc395/hd827_oc395_indels_only_for_lod_vep104.txt"
    return "lab_validation_120/external_data_sources/horizon_gold_sets/hd827_indels_only_for_lod.txt"


def _get_oc395primer_boost_image_dir(replicate=None):
    """
    Just return the string of where to store the images for oc395
    :param replicate: String to add in the directory
    """

    return '/'.join(("images/limit_of_detection_oc395primer_boost", replicate))


def _get_oc395primer_boost_clinical_snp_file():  # TODO update this for clinical LOD
    """Just return the string location of this file for oc395"""
    return "lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_lod_variants.txt"


def _get_oc395primer_boost_clinical_indel_file():   # TODO update this for clinical LOD
    """Just return the string location of this file for oc395"""
    return "lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_lod_variants.txt"


def get_horizon_concordance_in_lod_oc395primer_boost_rep1(snp_type=None):
    """
    Get the clinical sample for LOD analysis, Rep1
    :param snp_type:  Either snp or indels
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        horizon_file = _get_oc395primer_boost_horizon_snp_file()
    elif snp_type == 'indels':
        horizon_file = _get_oc395primer_boost_horizon_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc395primer_boost_image_dir(replicate='rep1'),
            "vaf_regression_plot": {
                "x_axis_label": "GS395",
                "y_axis_label": "Horizon HD798",
                "x_value_key": "395_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": None,
            "concordance_files":
                [
                    {'horizon_file': horizon_file,
                     'sample_id': '1_oc395primer_boost_lod_hd827_100%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon/2021out14-oc395-lod--b21-147-cid21-2009-snapshot/2021out14-oc395-lod--b21-147-cid21-2009-snapshot.B21-147.CID21-2009.B541_N701.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '2_oc395primer_boost_lod_hd827_50%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2010-snapshot/2021out14-oc395-lod--b21-147-cid21-2010-snapshot.B21-147.CID21-2010.B542_N702.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '3_oc395primer_boost_lod_hd827_25%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2011-snapshot/2021out14-oc395-lod--b21-147-cid21-2011-snapshot.B21-147.CID21-2011.B543_N703.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '4_oc395primer_boost_lod_hd827_12.5%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2012-snapshot/2021out14-oc395-lod--b21-147-cid21-2012-snapshot.B21-147.CID21-2012.B544_N704.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '5_oc395primer_boost_lod_hd827_6.25%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2013-snapshot/2021out14-oc395-lod--b21-147-cid21-2013-snapshot.B21-147.CID21-2013.B545_N705.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '6_oc395primer_boost_lod_hd827_3.125%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2014-snapshot/2021out14-oc395-lod--b21-147-cid21-2014-snapshot.B21-147.CID21-2014.B546_N706.merged_concatenated.vcf'}
                ]
        }
    }


def get_clinical_sample_concordance_in_lod_oc395primer_boost_rep1(snp_type=None):
    """
    Get the clinical sample for LOD analysis for OC120plus for rep1
    :param snp_type:  Either snp or both
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        clinical_variant_file = _get_oc395primer_boost_clinical_snp_file()
    elif snp_type == 'indels':
        clinical_variant_file = _get_oc395primer_boost_clinical_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc395primer_boost_image_dir(replicate='rep1'),
            "vaf_regression_plot": {
                "x_axis_label": "OC120plus",
                "y_axis_label": "Clinical Sample",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": "lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_dilution_sample/2021nov29-oc180-r58--b21-171-cid21-2581-snapshot/2021nov29-oc180-r58--b21-171-cid21-2581-snapshot.B21-171.CID21-2581.C510_N702.merged_concatenated.vcf",
            "concordance_files":
                [
                    {'horizon_file': clinical_variant_file,
                      'sample_id': '1_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_100%',
                      'variant_type': 'snvs',
                      'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-538-snapshot/2022mar08-oc395-run0-b22-31-cid22-538-snapshot.B22-31.CID22-538.B541_N701.merged_concatenated.vcf'},
                     {'horizon_file': clinical_variant_file,
                      'sample_id': '2_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_50%',
                      'variant_type': 'snvs',
                      'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-539-snapshot/2022mar08-oc395-run0-b22-31-cid22-539-snapshot.B22-31.CID22-539.B542_N702.merged_concatenated.vcf'},
                     {'horizon_file': clinical_variant_file,
                      'sample_id': '3_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_25%',
                      'variant_type': 'snvs',
                      'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-540-snapshot/2022mar08-oc395-run0-b22-31-cid22-540-snapshot.B22-31.CID22-540.B543_N703.merged_concatenated.vcf'},
                     {'horizon_file': clinical_variant_file,
                      'sample_id': '4_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_12.5%',
                      'variant_type': 'snvs',
                      'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-541-snapshot/2022mar08-oc395-run0-b22-31-cid22-541-snapshot.B22-31.CID22-541.B544_N704.merged_concatenated.vcf'},
                     {'horizon_file': clinical_variant_file,
                      'sample_id': '5_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_6.25%',
                      'variant_type': 'snvs',
                      'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-542-snapshot/2022mar08-oc395-run0-b22-31-cid22-542-snapshot.B22-31.CID22-542.B545_N705.merged_concatenated.vcf'},
                     {'horizon_file': clinical_variant_file,
                      'sample_id': '6_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_3.125%',
                      'variant_type': 'snvs',
                      'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-543-snapshot/2022mar08-oc395-run0-b22-31-cid22-543-snapshot.B22-31.CID22-543.B546_N706.merged_concatenated.vcf'}
                ]
        }
    }


def get_clinical_sample_concordance_in_lod_oc395primer_boost_rep2(snp_type=None):
    """
    Get the clinical sample for LOD analysis for OC120plus for rep2
    :param snp_type:  Either snp or both
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        clinical_variant_file = _get_oc395primer_boost_clinical_snp_file()
    elif snp_type == 'indels':
        clinical_variant_file = _get_oc395primer_boost_clinical_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run2",
            "images_dir": _get_oc395primer_boost_image_dir(replicate='rep2'),
            "vaf_regression_plot": {
                "x_axis_label": "OC120",
                "y_axis_label": "Horizon",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": "lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_dilution_sample/2021nov29-oc180-r58--b21-171-cid21-2581-snapshot/2021nov29-oc180-r58--b21-171-cid21-2581-snapshot.B21-171.CID21-2581.C510_N702.merged_concatenated.vcf",
            "concordance_files":
                [
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '1_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_100%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-544-snapshot/2022mar08-oc395-run0-b22-31-cid22-544-snapshot.B22-31.CID22-544.B547_N707.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '2_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_50%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-545-snapshot/2022mar08-oc395-run0-b22-31-cid22-545-snapshot.B22-31.CID22-545.B548_N708.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '3_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_25%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-546-snapshot/2022mar08-oc395-run0-b22-31-cid22-546-snapshot.B22-31.CID22-546.C506_N705.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '4_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_12.5%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-547-snapshot/2022mar08-oc395-run0-b22-31-cid22-547-snapshot.B22-31.CID22-547.C507_N706.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '5_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_6.25%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-548-snapshot/2022mar08-oc395-run0-b22-31-cid22-548-snapshot.B22-31.CID22-548.C508_N707.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '6_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_3.125%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-549-snapshot/2022mar08-oc395-run0-b22-31-cid22-549-snapshot.B22-31.CID22-549.B515_N708.merged_concatenated.vcf'}
                ]
        }
    }

####### HTQC


def get_limit_of_detection_for_horizon_htqc_oc395primer_boost_rep1():
    """The horizon samples LOD HTQC stats oc395 boosted rep1"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc395primer_boost_image_dir(replicate='rep1'),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep1_horizon_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2009-snapshot/2021out14-oc395-lod--b21-147-cid21-2009-snapshot.B21-147.CID21-2009.B541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2009-snapshot/2021out14-oc395-lod--b21-147-cid21-2009-snapshot.B21-147.CID21-2009.B541_N701.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid21-2009',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2010-snapshot/2021out14-oc395-lod--b21-147-cid21-2010-snapshot.B21-147.CID21-2010.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2010-snapshot/2021out14-oc395-lod--b21-147-cid21-2010-snapshot.B21-147.CID21-2010.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid21-2010',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2011-snapshot/2021out14-oc395-lod--b21-147-cid21-2011-snapshot.B21-147.CID21-2011.B543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2011-snapshot/2021out14-oc395-lod--b21-147-cid21-2011-snapshot.B21-147.CID21-2011.B543_N703.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid21-2011',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2012-snapshot/2021out14-oc395-lod--b21-147-cid21-2012-snapshot.B21-147.CID21-2012.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2012-snapshot/2021out14-oc395-lod--b21-147-cid21-2012-snapshot.B21-147.CID21-2012.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid21-2012',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2013-snapshot/2021out14-oc395-lod--b21-147-cid21-2013-snapshot.B21-147.CID21-2013.B545_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2013-snapshot/2021out14-oc395-lod--b21-147-cid21-2013-snapshot.B21-147.CID21-2013.B545_N705.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid21-2013',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2014-snapshot/2021out14-oc395-lod--b21-147-cid21-2014-snapshot.B21-147.CID21-2014.B546_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/horizon//2021out14-oc395-lod--b21-147-cid21-2014-snapshot/2021out14-oc395-lod--b21-147-cid21-2014-snapshot.B21-147.CID21-2014.B546_N706.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid21-2014',
                        'sample_type': ''}
                ]
            }
        }
    }


def get_limit_of_detection_for_clinical_sample_htqc_oc395primer_boost_rep1():
    """The clinical samples oc395 boosted LOD HTQC stats for rep1"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc395primer_boost_image_dir(replicate='rep1'),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep1_clinical_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-538-snapshot/2022mar08-oc395-run0-b22-31-cid22-538-snapshot.B22-31.CID22-538.B541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-538-snapshot/2022mar08-oc395-run0-b22-31-cid22-538-snapshot.B22-31.CID22-538.B541_N701.coverage.summary.htqc.txt',
                        'sample_id': '1_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_100%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-539-snapshot/2022mar08-oc395-run0-b22-31-cid22-539-snapshot.B22-31.CID22-539.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-539-snapshot/2022mar08-oc395-run0-b22-31-cid22-539-snapshot.B22-31.CID22-539.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '2_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_50%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-540-snapshot/2022mar08-oc395-run0-b22-31-cid22-540-snapshot.B22-31.CID22-540.B543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-540-snapshot/2022mar08-oc395-run0-b22-31-cid22-540-snapshot.B22-31.CID22-540.B543_N703.coverage.summary.htqc.txt',
                        'sample_id': '3_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-541-snapshot/2022mar08-oc395-run0-b22-31-cid22-541-snapshot.B22-31.CID22-541.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-541-snapshot/2022mar08-oc395-run0-b22-31-cid22-541-snapshot.B22-31.CID22-541.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '4_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_12.5%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-542-snapshot/2022mar08-oc395-run0-b22-31-cid22-542-snapshot.B22-31.CID22-542.B545_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-542-snapshot/2022mar08-oc395-run0-b22-31-cid22-542-snapshot.B22-31.CID22-542.B545_N705.coverage.summary.htqc.txt',
                        'sample_id': '5_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_6.25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-543-snapshot/2022mar08-oc395-run0-b22-31-cid22-543-snapshot.B22-31.CID22-543.B546_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-543-snapshot/2022mar08-oc395-run0-b22-31-cid22-543-snapshot.B22-31.CID22-543.B546_N706.coverage.summary.htqc.txt',
                        'sample_id': '6_oc395primer_boost_lod_clinical_rep1_oc395primer_boost_3.125%',
                        'sample_type': 'Tumor'}
                ]

            }
        }
    }


def get_limit_of_detection_for_clinical_sample_htqc_oc395primer_boost_rep2():
    """The clinical samples oc395 boosted LOD HTQC stats for rep2"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc395primer_boost_image_dir(replicate='rep2'),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep2_clinical_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-544-snapshot/2022mar08-oc395-run0-b22-31-cid22-544-snapshot.B22-31.CID22-544.B547_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-544-snapshot/2022mar08-oc395-run0-b22-31-cid22-544-snapshot.B22-31.CID22-544.B547_N707.coverage.summary.htqc.txt',
                        'sample_id': '1_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_100%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-545-snapshot/2022mar08-oc395-run0-b22-31-cid22-545-snapshot.B22-31.CID22-545.B548_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-545-snapshot/2022mar08-oc395-run0-b22-31-cid22-545-snapshot.B22-31.CID22-545.B548_N708.coverage.summary.htqc.txt',
                        'sample_id': '2_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_50%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-546-snapshot/2022mar08-oc395-run0-b22-31-cid22-546-snapshot.B22-31.CID22-546.C506_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-546-snapshot/2022mar08-oc395-run0-b22-31-cid22-546-snapshot.B22-31.CID22-546.C506_N705.coverage.summary.htqc.txt',
                        'sample_id': '3_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-547-snapshot/2022mar08-oc395-run0-b22-31-cid22-547-snapshot.B22-31.CID22-547.C507_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-547-snapshot/2022mar08-oc395-run0-b22-31-cid22-547-snapshot.B22-31.CID22-547.C507_N706.coverage.summary.htqc.txt',
                        'sample_id': '4_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_12.5%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-548-snapshot/2022mar08-oc395-run0-b22-31-cid22-548-snapshot.B22-31.CID22-548.C508_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-548-snapshot/2022mar08-oc395-run0-b22-31-cid22-548-snapshot.B22-31.CID22-548.C508_N707.coverage.summary.htqc.txt',
                        'sample_id': '5_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_6.25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-549-snapshot/2022mar08-oc395-run0-b22-31-cid22-549-snapshot.B22-31.CID22-549.B515_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_lod/clinical_samples/2022mar08-oc395-run0-b22-31-cid22-549-snapshot/2022mar08-oc395-run0-b22-31-cid22-549-snapshot.B22-31.CID22-549.B515_N708.coverage.summary.htqc.txt',
                        'sample_id': '6_oc395primer_boost_lod_clinical_rep2_oc395primer_boost_3.125%',
                        'sample_type': 'Tumor'}
                ]

            }
        }
    }
