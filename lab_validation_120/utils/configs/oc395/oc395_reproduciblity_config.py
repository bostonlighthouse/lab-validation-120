"""Get the VCF / QC files for the GS395 vs GS180"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_oc395_reproducibility():
    """
    Get the VCF files for the GS395 vs GS180
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/oc395_reproducibility",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    'scratch/oc395/oc395_primer_boost/oc395_reproducibility/oc395_primer_boost_reproducibility_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                    'scratch/oc395/oc395_primer_boost/oc395_reproducibility/oc395_primer_boost_reproducibility_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                ],
                "indel": [
                    'scratch/oc395/oc395_primer_boost/oc395_reproducibility/oc395_primer_boost_reproducibility_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                    'scratch/oc395/oc395_primer_boost/oc395_reproducibility/oc395_primer_boost_reproducibility_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                ]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file": [
                        'scratch/oc395/oc395_primer_boost/oc395_reproducibility/oc395_primer_boost_reproducibility_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                        'scratch/oc395/oc395_primer_boost/oc395_reproducibility/oc395_primer_boost_reproducibility_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        'scratch/oc395/oc395_primer_boost/oc395_reproducibility/oc395_primer_boost_reproducibility_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                        'scratch/oc395/oc395_primer_boost/oc395_reproducibility/oc395_primer_boost_reproducibility_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS395 Rep1",
                "y_axis_label": "GS395 Rep2",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS395 Rep1",
                "y_axis_label": "GS395 Re2",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS395 Rep1",
                "y_axis_label": "GS395 Rep2",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid22-784',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-784-snapshot/2022abr01-oc395-run0-b22-43-cid22-784-snapshot.B22-43.CID22-784.B537_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '1b-cid22-807',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-807-snapshot/2022abr05-oc395-run0-b22-44-cid22-807-snapshot.B22-44.CID22-807.B537_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample2': [{'sample_id': '2a-cid22-782',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-782-snapshot/2022abr01-oc395-run0-b22-43-cid22-782-snapshot.B22-43.CID22-782.B535_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '2b-cid22-808',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-808-snapshot/2022abr05-oc395-run0-b22-44-cid22-808-snapshot.B22-44.CID22-808.B535_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample3': [{'sample_id': '3a-cid22-780',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-780-snapshot/2022abr01-oc395-run0-b22-43-cid22-780-snapshot.B22-43.CID22-780.B534_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '3b-cid22-809',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-809-snapshot/2022abr05-oc395-run0-b22-44-cid22-809-snapshot.B22-44.CID22-809.B534_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample4': [{'sample_id': '4a-cid22-781',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-781-snapshot/2022abr01-oc395-run0-b22-43-cid22-781-snapshot.B22-43.CID22-781.B533_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '4b-cid22-810',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-810-snapshot/2022abr05-oc395-run0-b22-44-cid22-810-snapshot.B22-44.CID22-810.B533_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample5': [{'sample_id': '5a-cid22-776',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-776-snapshot/2022abr01-oc395-run0-b22-42-cid22-776-snapshot.B22-42.CID22-776.B530_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '5b-cid22-811',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-811-snapshot/2022abr05-oc395-run0-b22-44-cid22-811-snapshot.B22-44.CID22-811.B530_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample6': [{'sample_id': '6a-cid22-777',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-777-snapshot/2022abr01-oc395-run0-b22-42-cid22-777-snapshot.B22-42.CID22-777.B528_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '6b-cid22-814',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-814-snapshot/2022abr05-oc395-run0-b22-44-cid22-814-snapshot.B22-44.CID22-814.B528_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample7': [{'sample_id': '7a-cid22-774',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-774-snapshot/2022abr01-oc395-run0-b22-42-cid22-774-snapshot.B22-42.CID22-774.B527_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '7b-cid22-812',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-812-snapshot/2022abr05-oc395-run0-b22-44-cid22-812-snapshot.B22-44.CID22-812.B527_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample8': [{'sample_id': '8a-cid22-766',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-766-snapshot/2022abr01-oc395-run0-b22-42-cid22-766-snapshot.B22-42.CID22-766.B525_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '8b-cid22-813',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-813-snapshot/2022abr05-oc395-run0-b22-44-cid22-813-snapshot.B22-44.CID22-813.B525_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}]
            }

        }
    }



def get_oc395_reproducibility_htqc_plots():
    """
    Get the QC files for the GS395 vs GS180
    """

    key_2_use = 'oc395_reproducibility'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS395 Rep1 vs GS395 Rep2",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-784-snapshot/2022abr01-oc395-run0-b22-43-cid22-784-snapshot.B22-43.CID22-784.B537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-784-snapshot/2022abr01-oc395-run0-b22-43-cid22-784-snapshot.B22-43.CID22-784.B537_N705.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid22-784',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-807-snapshot/2022abr05-oc395-run0-b22-44-cid22-807-snapshot.B22-44.CID22-807.B537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-807-snapshot/2022abr05-oc395-run0-b22-44-cid22-807-snapshot.B22-44.CID22-807.B537_N705.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid22-807',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-782-snapshot/2022abr01-oc395-run0-b22-43-cid22-782-snapshot.B22-43.CID22-782.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-782-snapshot/2022abr01-oc395-run0-b22-43-cid22-782-snapshot.B22-43.CID22-782.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid22-782',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-808-snapshot/2022abr05-oc395-run0-b22-44-cid22-808-snapshot.B22-44.CID22-808.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-808-snapshot/2022abr05-oc395-run0-b22-44-cid22-808-snapshot.B22-44.CID22-808.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid22-808',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-780-snapshot/2022abr01-oc395-run0-b22-43-cid22-780-snapshot.B22-43.CID22-780.B534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-780-snapshot/2022abr01-oc395-run0-b22-43-cid22-780-snapshot.B22-43.CID22-780.B534_N702.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid22-780',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-809-snapshot/2022abr05-oc395-run0-b22-44-cid22-809-snapshot.B22-44.CID22-809.B534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-809-snapshot/2022abr05-oc395-run0-b22-44-cid22-809-snapshot.B22-44.CID22-809.B534_N702.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid22-809',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-781-snapshot/2022abr01-oc395-run0-b22-43-cid22-781-snapshot.B22-43.CID22-781.B533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-781-snapshot/2022abr01-oc395-run0-b22-43-cid22-781-snapshot.B22-43.CID22-781.B533_N701.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid22-781',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-810-snapshot/2022abr05-oc395-run0-b22-44-cid22-810-snapshot.B22-44.CID22-810.B533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-810-snapshot/2022abr05-oc395-run0-b22-44-cid22-810-snapshot.B22-44.CID22-810.B533_N701.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid22-810',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-776-snapshot/2022abr01-oc395-run0-b22-42-cid22-776-snapshot.B22-42.CID22-776.B530_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-776-snapshot/2022abr01-oc395-run0-b22-42-cid22-776-snapshot.B22-42.CID22-776.B530_N706.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid22-776',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-811-snapshot/2022abr05-oc395-run0-b22-44-cid22-811-snapshot.B22-44.CID22-811.B530_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-811-snapshot/2022abr05-oc395-run0-b22-44-cid22-811-snapshot.B22-44.CID22-811.B530_N706.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid22-811',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-777-snapshot/2022abr01-oc395-run0-b22-42-cid22-777-snapshot.B22-42.CID22-777.B528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-777-snapshot/2022abr01-oc395-run0-b22-42-cid22-777-snapshot.B22-42.CID22-777.B528_N704.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid22-777',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-814-snapshot/2022abr05-oc395-run0-b22-44-cid22-814-snapshot.B22-44.CID22-814.B528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-814-snapshot/2022abr05-oc395-run0-b22-44-cid22-814-snapshot.B22-44.CID22-814.B528_N704.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid22-814',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-774-snapshot/2022abr01-oc395-run0-b22-42-cid22-774-snapshot.B22-42.CID22-774.B527_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-774-snapshot/2022abr01-oc395-run0-b22-42-cid22-774-snapshot.B22-42.CID22-774.B527_N703.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid22-774',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-812-snapshot/2022abr05-oc395-run0-b22-44-cid22-812-snapshot.B22-44.CID22-812.B527_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-812-snapshot/2022abr05-oc395-run0-b22-44-cid22-812-snapshot.B22-44.CID22-812.B527_N703.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid22-812',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-766-snapshot/2022abr01-oc395-run0-b22-42-cid22-766-snapshot.B22-42.CID22-766.B525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-766-snapshot/2022abr01-oc395-run0-b22-42-cid22-766-snapshot.B22-42.CID22-766.B525_N701.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid22-766',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-813-snapshot/2022abr05-oc395-run0-b22-44-cid22-813-snapshot.B22-44.CID22-813.B525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-813-snapshot/2022abr05-oc395-run0-b22-44-cid22-813-snapshot.B22-44.CID22-813.B525_N701.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid22-813',
                        'sample_type': ''}
                ]
            }
        }
    }
