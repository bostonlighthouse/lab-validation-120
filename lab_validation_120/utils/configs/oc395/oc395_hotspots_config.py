"""Config files for the hotspots metrics analysis for OC395 (GS395)"""

def get_tumor_hotspot_metrics_oc395():

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    # these 11 samples were also  sequenced with oc395 boosted primers concentrations
    return {
        "amplicon_metrics": {
            "run": "oc395original_analysis",
            "images_dir": "images/amplicon_metrics/oc395",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data":[
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-782-snapshot/2020-08-28-oc395-run-b20-80-cid20-782-snapshot.B20-80.CID20-782.C504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid20-782',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-640-snapshot/2020-08-21-oc395-run-b20-77-cid20-640-snapshot.B20-77.CID20-640.A514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid20-640',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-607-snapshot/2020-07-31-oc395-run-b20-70-cid20-607-snapshot.B20-70.CID20-607.A503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-607',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-724-snapshot/2020-08-21-oc395-run-b20-77-cid20-724-snapshot.B20-77.CID20-724.A516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid20-724',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-612-snapshot/2020-07-31-oc395-run-b20-70-cid20-612-snapshot.B20-70.CID20-612.A536_N704.hotspots.htqc.txt',
                        'sample_id': 'cid20-612',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-605-snapshot/2020-07-31-oc395-run-b20-70-cid20-605-snapshot.B20-70.CID20-605.A501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-605',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-799-snapshot/2020-08-28-oc395-run-b20-80-cid20-799-snapshot.B20-80.CID20-799.C511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-799',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-779-snapshot/2020-08-28-oc395-run-b20-80-cid20-779-snapshot.B20-80.CID20-779.C503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-779',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-713-snapshot/2020-08-21-oc395-run-b20-77-cid20-713-snapshot.B20-77.CID20-713.A510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid20-713',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-717-snapshot/2020-08-21-oc395-run-b20-77-cid20-717-snapshot.B20-77.CID20-717.A511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid20-717',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-603-snapshot/2020-07-31-oc395-run-b20-70-cid20-603-snapshot.B20-70.CID20-603.A525_N701.hotspots.htqc.txt',
                        'sample_id': 'cid20-603',
                        'sample_type': 'tumor'}
                ]
            }
        }
    }

def get_tumor_hotspot_metrics_oc395primer_boosted():

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    # these 11 samples were also originally sequenced with oc395 original (no primer boosting)
    return {
        "amplicon_metrics": {
            "run": "oc395primer_boosted_analysis",
            "images_dir": "images/amplicon_metrics/oc395",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data":[
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot.B21-90.CID21-1050.C504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1050',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot.B21-91.CID21-1053.B517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1053',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot.B21-90.CID21-1058.C512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1058',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot.B21-91.CID21-1171.B521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1171',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot.B21-91.CID21-1172.B522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1172',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot.B21-91.CID21-1176.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1176',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot.B21-91.CID21-1178.B544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1178',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot.B21-91.CID21-1180.B546_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1180',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot.B21-90.CID21-1183.C534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1183',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot.B21-90.CID21-1185.C537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1185',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot.B21-90.CID21-1190.A532_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1190',
                        'sample_type': 'tumor'}
                ]
            }
        }
    }

def get_tumor_hotspot_metrics_oc395primer_boosted_all_33_sammples():

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    # these 11 samples were also originally sequenced with oc395 original (no primer boosting)
    return {
        "amplicon_metrics": {
            "run": "oc395primer_boosted_all_samples_analysis",
            "images_dir": "images/amplicon_metrics/oc395",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data":[
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1047-snapshot/2021jul08-oc395-run0-b21-90-cid21-1047-snapshot.B21-90.CID21-1047.C501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1047',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1048-snapshot/2021jul08-oc395-run0-b21-90-cid21-1048-snapshot.B21-90.CID21-1048.C502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1048',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1049-snapshot/2021jul08-oc395-run0-b21-90-cid21-1049-snapshot.B21-90.CID21-1049.C503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1049',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot.B21-90.CID21-1050.C504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1050',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1051-snapshot/2021jul08-oc395-run0-b21-90-cid21-1051-snapshot.B21-90.CID21-1051.C505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1051',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1052-snapshot/2021jul08-oc395-run0-b21-90-cid21-1052-snapshot.B21-90.CID21-1052.C506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1052',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot.B21-91.CID21-1053.B517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1053',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1054-snapshot/2021jul08-oc395-run0-b21-91-cid21-1054-snapshot.B21-91.CID21-1054.B518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1054',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1055-snapshot/2021jul08-oc395-run0-b21-91-cid21-1055-snapshot.B21-91.CID21-1055.B519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1055',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1057-snapshot/2021jul08-oc395-run0-b21-90-cid21-1057-snapshot.B21-90.CID21-1057.C511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1057',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot.B21-90.CID21-1058.C512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1058',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1063-snapshot/2021jul08-oc395-run0-b21-91-cid21-1063-snapshot.B21-91.CID21-1063.B520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1063',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot.B21-91.CID21-1171.B521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1171',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot.B21-91.CID21-1172.B522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1172',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1173-snapshot/2021jul08-oc395-run0-b21-91-cid21-1173-snapshot.B21-91.CID21-1173.B523_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1173',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1174-snapshot/2021jul08-oc395-run0-b21-91-cid21-1174-snapshot.B21-91.CID21-1174.B524_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1174',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1175-snapshot/2021jul08-oc395-run0-b21-91-cid21-1175-snapshot.B21-91.CID21-1175.B541_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1175',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot.B21-91.CID21-1176.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1176',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1177-snapshot/2021jul08-oc395-run0-b21-91-cid21-1177-snapshot.B21-91.CID21-1177.B543_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1177',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot.B21-91.CID21-1178.B544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1178',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1179-snapshot/2021jul08-oc395-run0-b21-91-cid21-1179-snapshot.B21-91.CID21-1179.B545_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1179',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot.B21-91.CID21-1180.B546_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1180',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1181-snapshot/2021jul08-oc395-run0-b21-91-cid21-1181-snapshot.B21-91.CID21-1181.B547_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1181',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1182-snapshot/2021jul08-oc395-run0-b21-91-cid21-1182-snapshot.B21-91.CID21-1182.B548_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1182',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1184-snapshot/2021jul08-oc395-run0-b21-91-cid21-1184-snapshot.B21-91.CID21-1184.C533_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1184',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot.B21-90.CID21-1183.C534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1183',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1191-snapshot/2021jul08-oc395-run0-b21-90-cid21-1191-snapshot.B21-90.CID21-1191.C535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1191',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1186-snapshot/2021jul08-oc395-run0-b21-90-cid21-1186-snapshot.B21-90.CID21-1186.C536_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1186',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot.B21-90.CID21-1185.C537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1185',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1187-snapshot/2021jul08-oc395-run0-b21-90-cid21-1187-snapshot.B21-90.CID21-1187.C538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1187',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1188-snapshot/2021jul08-oc395-run0-b21-90-cid21-1188-snapshot.B21-90.CID21-1188.C539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1188',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1189-snapshot/2021jul08-oc395-run0-b21-90-cid21-1189-snapshot.B21-90.CID21-1189.C540_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1189',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot.B21-90.CID21-1190.A532_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1190',
                        'sample_type': 'tumor'}
                ]
            }
        }
    }

def get_final_oc395_hotspots_validation_data_for_oc180():

    # The gs180 samples that were done as the concordance to gs395 - 160 samples
    return {
        "amplicon_metrics": {
            "run": "oc395primer_boosted_analysis_final_oc180",
            "images_dir": "images/oc395primer_boosted_analysis_final_oc180/",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai13-gs180-run1-b22-63-cid22-1108-snapshot/2022mai13-gs180-run1-b22-63-cid22-1108-snapshot.B22-63.CID22-1108.B519_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-1108',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022fev25-oc180-run0-b22-23-cid22-441-snapshot/2022fev25-oc180-run0-b22-23-cid22-441-snapshot.B22-23.CID22-441.B519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-441',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-29-oc180-run-b21-98-cid21-1304-snapshot/2021-07-29-oc180-run-b21-98-cid21-1304-snapshot.B21-98.CID21-1304.C535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1304',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021jul15-oc180-run0-b21-92-cid21-1222-snapshot/2021jul15-oc180-run0-b21-92-cid21-1222-snapshot.B21-92.CID21-1222.A527_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1222',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-23-gs180-run-b21-49-cid21-545-snapshot/2021-04-23-gs180-run-b21-49-cid21-545-snapshot.B21-49.CID21-545.B530_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-545',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-23-gs180-run-b21-51-cid21-498-snapshot/2021-04-23-gs180-run-b21-51-cid21-498-snapshot.B21-51.CID21-498.B534_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-498',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-23-gs180-run-b21-49-cid21-474-snapshot/2021-04-23-gs180-run-b21-49-cid21-474-snapshot.B21-49.CID21-474.B526_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-474',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-30-oc180-run-b21-55-cid21-519-snapshot/2021-04-30-oc180-run-b21-55-cid21-519-snapshot.B21-55.CID21-519.B548_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-519',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-30-oc180-run-b21-55-cid21-555-snapshot/2021-04-30-oc180-run-b21-55-cid21-555-snapshot.B21-55.CID21-555.B539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-555',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-07-oc180-run-b21-57-cid21-585-snapshot/2021-05-07-oc180-run-b21-57-cid21-585-snapshot.B21-57.CID21-585.B534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-585',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-21-oc180-run-b21-67-cid21-769-snapshot/2021-05-21-oc180-run-b21-67-cid21-769-snapshot.B21-67.CID21-769.B538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-769',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-21-oc180-run-b21-67-cid21-747-snapshot/2021-05-21-oc180-run-b21-67-cid21-747-snapshot.B21-67.CID21-747.B547_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-747',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1083-snapshot/2022mai11-oc180-run0-b22-60-cid22-1083-snapshot.B22-60.CID22-1083.B526_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-1083',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-26-oc180-run-b21-71-cid21-749-snapshot/2021-05-26-oc180-run-b21-71-cid21-749-snapshot.B21-71.CID21-749.B528_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-749',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-26-oc180-run-b21-71-cid21-753-snapshot/2021-05-26-oc180-run-b21-71-cid21-753-snapshot.B21-71.CID21-753.B529_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-753',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-26-oc180-run-b21-71-cid21-799-snapshot/2021-05-26-oc180-run-b21-71-cid21-799-snapshot.B21-71.CID21-799.B530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-799',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-26-oc180-run-b21-71-cid21-801-snapshot/2021-05-26-oc180-run-b21-71-cid21-801-snapshot.B21-71.CID21-801.B531_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-801',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-26-oc180-run-b21-71-cid21-759-snapshot/2021-05-26-oc180-run-b21-71-cid21-759-snapshot.B21-71.CID21-759.B534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-759',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-26-oc180-run-b21-69-cid21-797-snapshot/2021-05-26-oc180-run-b21-69-cid21-797-snapshot.B21-69.CID21-797.B526_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-797',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-29-oc180-run-b21-87-cid21-1017-snapshot/2021-06-29-oc180-run-b21-87-cid21-1017-snapshot.B21-87.CID21-1017.A537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1017',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-31-oc180-run-b21-73-cid21-775-snapshot/2021-05-31-oc180-run-b21-73-cid21-775-snapshot.B21-73.CID21-775.A537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-775',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-11-oc180-run-b21-80-cid21-847-snapshot/2021-06-11-oc180-run-b21-80-cid21-847-snapshot.B21-80.CID21-847.A509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-847',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-17-oc180-run-b21-94-cid21-1166-snapshot/2021-07-17-oc180-run-b21-94-cid21-1166-snapshot.B21-94.CID21-1166.A511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1166',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-29-oc180-run-b21-98-cid21-1310-snapshot/2021-07-29-oc180-run-b21-98-cid21-1310-snapshot.B21-98.CID21-1310.C536_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1310',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-18-oc180-run-b21-84-cid21-991-snapshot/2021-06-18-oc180-run-b21-84-cid21-991-snapshot.B21-84.CID21-991.A510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-991',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-29-oc180-run-b21-87-cid21-1089-snapshot/2021-06-29-oc180-run-b21-87-cid21-1089-snapshot.B21-87.CID21-1089.A547_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1089',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-29-oc180-run-b21-87-cid21-1082-snapshot/2021-06-29-oc180-run-b21-87-cid21-1082-snapshot.B21-87.CID21-1082.A539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1082',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-29-oc180-run-b21-87-cid21-1038-snapshot/2021-06-29-oc180-run-b21-87-cid21-1038-snapshot.B21-87.CID21-1038.A501_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1038',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1106-snapshot/2021-07-07-oc180-run-b21-89-cid21-1106-snapshot.B21-89.CID21-1106.A508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1106',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1288-snapshot/2021-08-06-oc180-run-b21-102-cid21-1288-snapshot.B21-102.CID21-1288.C501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1288',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1136-snapshot/2021-07-07-oc180-run-b21-89-cid21-1136-snapshot.B21-89.CID21-1136.A519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1136',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-07-oc180-run-b21-100-cid21-1388-snapshot/2021-08-07-oc180-run-b21-100-cid21-1388-snapshot.B21-100.CID21-1388.C526_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1388',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1213-snapshot/2021-08-06-oc180-run-b21-102-cid21-1213-snapshot.B21-102.CID21-1213.C524_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1213',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-07-oc180-run-b21-100-cid21-1400-snapshot/2021-08-07-oc180-run-b21-100-cid21-1400-snapshot.B21-100.CID21-1400.C531_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1400',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1328-snapshot/2021-08-06-oc180-run-b21-102-cid21-1328-snapshot.B21-102.CID21-1328.C518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1328',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-07-oc180-run-b21-100-cid21-1394-snapshot/2021-08-07-oc180-run-b21-100-cid21-1394-snapshot.B21-100.CID21-1394.C530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1394',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1332-snapshot/2021-08-06-oc180-run-b21-102-cid21-1332-snapshot.B21-102.CID21-1332.C508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1332',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1366-snapshot/2021-08-06-oc180-run-b21-102-cid21-1366-snapshot.B21-102.CID21-1366.C505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1366',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1322-snapshot/2021-08-06-oc180-run-b21-102-cid21-1322-snapshot.B21-102.CID21-1322.C521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1322',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1364-snapshot/2021-08-06-oc180-run-b21-102-cid21-1364-snapshot.B21-102.CID21-1364.C503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1364',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1348-snapshot/2021-08-06-oc180-run-b21-102-cid21-1348-snapshot.B21-102.CID21-1348.C522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1348',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1336-snapshot/2021-08-06-oc180-run-b21-102-cid21-1336-snapshot.B21-102.CID21-1336.C506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1336',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1324-snapshot/2021-08-06-oc180-run-b21-102-cid21-1324-snapshot.B21-102.CID21-1324.C519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1324',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-29-oc180-run-b21-98-cid21-1316-snapshot/2021-07-29-oc180-run-b21-98-cid21-1316-snapshot.B21-98.CID21-1316.C538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1316',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-06-oc180-run-b21-102-cid21-1334-snapshot/2021-08-06-oc180-run-b21-102-cid21-1334-snapshot.B21-102.CID21-1334.C507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1334',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-13-oc180-run-b21-104-cid21-1360-snapshot/2021-08-13-oc180-run-b21-104-cid21-1360-snapshot.B21-104.CID21-1360.A538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1360',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021jul15-oc180-run0-b21-92-cid21-1221-snapshot/2021jul15-oc180-run0-b21-92-cid21-1221-snapshot.B21-92.CID21-1221.A526_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1221',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021jul15-oc180-run0-b21-92-cid21-1220-snapshot/2021jul15-oc180-run0-b21-92-cid21-1220-snapshot.B21-92.CID21-1220.A525_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1220',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1094-snapshot/2022mai11-oc180-run0-b22-60-cid22-1094-snapshot.B22-60.CID22-1094.B537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-1094',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1093-snapshot/2022mai11-oc180-run0-b22-60-cid22-1093-snapshot.B22-60.CID22-1093.B536_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-1093',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1092-snapshot/2022mai11-oc180-run0-b22-60-cid22-1092-snapshot.B22-60.CID22-1092.B535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-1092',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1095-snapshot/2022mai11-oc180-run0-b22-60-cid22-1095-snapshot.B22-60.CID22-1095.B538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-1095',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-16-gs180-rerun-b21-47-cid21-505-snapshot/2021-04-16-gs180-rerun-b21-47-cid21-505-snapshot.B21-47.CID21-505.B536_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-505',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-16-gs180-rerun-b21-47-cid21-526-snapshot/2021-04-16-gs180-rerun-b21-47-cid21-526-snapshot.B21-47.CID21-526.B537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-526',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-30-oc180-run-b21-53-cid21-600-snapshot/2021-04-30-oc180-run-b21-53-cid21-600-snapshot.B21-53.CID21-600.B501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-600',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-30-oc180-run-b21-55-cid21-551-snapshot/2021-04-30-oc180-run-b21-55-cid21-551-snapshot.B21-55.CID21-551.B544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-551',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-30-oc180-run-b21-53-cid21-581-snapshot/2021-04-30-oc180-run-b21-53-cid21-581-snapshot.B21-53.CID21-581.B509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-581',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-30-oc180-run-b21-55-cid21-573-snapshot/2021-04-30-oc180-run-b21-55-cid21-573-snapshot.B21-55.CID21-573.B512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-573',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-30-oc180-run-b21-53-cid21-557-snapshot/2021-04-30-oc180-run-b21-53-cid21-557-snapshot.B21-53.CID21-557.B507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-557',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-05-07-oc180-run-b21-57-cid21-618-snapshot/2021-05-07-oc180-run-b21-57-cid21-618-snapshot.B21-57.CID21-618.B528_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-618',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-04-30-oc180-run-b21-53-cid21-583-snapshot/2021-04-30-oc180-run-b21-53-cid21-583-snapshot.B21-53.CID21-583.B508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-583',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-11-oc180-run-b21-80-cid21-951-snapshot/2021-06-11-oc180-run-b21-80-cid21-951-snapshot.B21-80.CID21-951.A502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-951',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-11-oc180-run-b21-80-cid21-851-snapshot/2021-06-11-oc180-run-b21-80-cid21-851-snapshot.B21-80.CID21-851.A508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-851',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-11-oc180-run-b21-78-cid21-833-snapshot/2021-06-11-oc180-run-b21-78-cid21-833-snapshot.B21-78.CID21-833.A514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-833',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-18-oc180-run-b21-84-cid21-989-snapshot/2021-06-18-oc180-run-b21-84-cid21-989-snapshot.B21-84.CID21-989.A508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-989',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-06-18-oc180-run-b21-84-cid21-983-snapshot/2021-06-18-oc180-run-b21-84-cid21-983-snapshot.B21-84.CID21-983.A512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-983',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1120-snapshot/2021-07-07-oc180-run-b21-89-cid21-1120-snapshot.B21-89.CID21-1120.A512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1120',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1062-snapshot/2021-07-07-oc180-run-b21-89-cid21-1062-snapshot.B21-89.CID21-1062.A504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1062',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1108-snapshot/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot.B21-89.CID21-1108.A509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1108',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1126-snapshot/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot.B21-89.CID21-1126.A515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1126',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1112-snapshot/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot.B21-89.CID21-1112.A510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1112',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1104-snapshot/2021-07-07-oc180-run-b21-89-cid21-1104-snapshot.B21-89.CID21-1104.A506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1104',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1114-snapshot/2021-07-07-oc180-run-b21-89-cid21-1114-snapshot.B21-89.CID21-1114.A511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1114',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-17-oc180-run-b21-94-cid21-1152-snapshot/2021-07-17-oc180-run-b21-94-cid21-1152-snapshot.B21-94.CID21-1152.A510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1152',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-07-oc180-run-b21-89-cid21-1156-snapshot/2021-07-07-oc180-run-b21-89-cid21-1156-snapshot.B21-89.CID21-1156.A523_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1156',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-17-oc180-run-b21-94-cid21-1236-snapshot/2021-07-17-oc180-run-b21-94-cid21-1236-snapshot.B21-94.CID21-1236.A516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1236',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-17-oc180-run-b21-94-cid21-1238-snapshot/2021-07-17-oc180-run-b21-94-cid21-1238-snapshot.B21-94.CID21-1238.A523_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1238',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-17-oc180-run-b21-94-cid21-1243-snapshot/2021-07-17-oc180-run-b21-94-cid21-1243-snapshot.B21-94.CID21-1243.A508_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1243',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-07-29-oc180-run-b21-98-cid21-1302-snapshot/2021-07-29-oc180-run-b21-98-cid21-1302-snapshot.B21-98.CID21-1302.C528_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1302',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-18-oc180-run-b21-108-cid21-1429-snapshot/2021-08-18-oc180-run-b21-108-cid21-1429-snapshot.B21-108.CID21-1429.C505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1429',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021set02-oc180-run4-b21-120-cid21-1602-snapshot/2021set02-oc180-run4-b21-120-cid21-1602-snapshot.B21-120.CID21-1602.C541_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1602',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-08-25-oc180-run-b21-112-cid21-1641-snapshot/2021-08-25-oc180-run-b21-112-cid21-1641-snapshot.B21-112.CID21-1641.C531_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1641',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1665',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021set02-oc180-run4-b21-120-cid21-1590-snapshot/2021set02-oc180-run4-b21-120-cid21-1590-snapshot.B21-120.CID21-1590.C542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1590',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021set02-oc180-run4-b21-120-cid21-1645-snapshot/2021set02-oc180-run4-b21-120-cid21-1645-snapshot.B21-120.CID21-1645.C512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1645',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021set02-oc180-run4-b21-120-cid21-1651-snapshot/2021set02-oc180-run4-b21-120-cid21-1651-snapshot.B21-120.CID21-1651.C510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1651',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out13-oc180-run5-b21-145-cid21-1972-snapshot/2021out13-oc180-run5-b21-145-cid21-1972-snapshot.B21-145.CID21-1972.B503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1972',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out13-oc180-run5-b21-145-cid21-2038-snapshot/2021out13-oc180-run5-b21-145-cid21-2038-snapshot.B21-145.CID21-2038.B511_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-2038',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out13-oc180-run5-b21-145-cid21-1970-snapshot/2021out13-oc180-run5-b21-145-cid21-1970-snapshot.B21-145.CID21-1970.B504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1970',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out13-oc180-run5-b21-145-cid21-1974-snapshot/2021out13-oc180-run5-b21-145-cid21-1974-snapshot.B21-145.CID21-1974.B502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1974',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out23-oc180-r53--b21-151-cid21-2149-snapshot/2021out23-oc180-r53--b21-151-cid21-2149-snapshot.B21-151.CID21-2149.B511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-2149',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out16-oc180-run5-b21-149-cid21-2069-snapshot/2021out16-oc180-run5-b21-149-cid21-2069-snapshot.B21-149.CID21-2069.B534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-2069',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out16-oc180-run5-b21-149-cid21-2071-snapshot/2021out16-oc180-run5-b21-149-cid21-2071-snapshot.B21-149.CID21-2071.B533_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-2071',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out16-oc180-run5-b21-149-cid21-2051-snapshot/2021out16-oc180-run5-b21-149-cid21-2051-snapshot.B21-149.CID21-2051.B525_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-2051',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-167-cid21-2330-snapshot/2021nov25-oc180-r57--b21-167-cid21-2330-snapshot.B21-167.CID21-2330.B506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-2330',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out16-oc180-run5-b21-149-cid21-2191-snapshot/2021out16-oc180-run5-b21-149-cid21-2191-snapshot.B21-149.CID21-2191.B539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-2191',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out16-oc180-run5-b21-149-cid21-2193-snapshot/2021out16-oc180-run5-b21-149-cid21-2193-snapshot.B21-149.CID21-2193.B538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-2193',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out23-oc180-r53--b21-151-cid21-2133-snapshot/2021out23-oc180-r53--b21-151-cid21-2133-snapshot.B21-151.CID21-2133.B512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-2133',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov13-r56-2-ns55-b21-162-cid21-2429-snapshot/2021nov13-r56-2-ns55-b21-162-cid21-2429-snapshot.B21-162.CID21-2429.B539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-2429',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov13-oc180-r56--b21-160-cid21-2322-snapshot/2021nov13-oc180-r56--b21-160-cid21-2322-snapshot.B21-160.CID21-2322.B539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-2322',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out29-oc180-r54--b21-155-cid21-2223-snapshot/2021out29-oc180-r54--b21-155-cid21-2223-snapshot.B21-155.CID21-2223.B543_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-2223',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov13-r56-2-ns55-b21-162-cid21-2426-snapshot/2021nov13-r56-2-ns55-b21-162-cid21-2426-snapshot.B21-162.CID21-2426.B537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-2426',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-167-cid21-2400-snapshot/2021nov25-oc180-r57--b21-167-cid21-2400-snapshot.B21-167.CID21-2400.B510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-2400',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-167-cid21-2276-snapshot/2021nov25-oc180-r57--b21-167-cid21-2276-snapshot.B21-167.CID21-2276.B504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-2276',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov13-oc180-r56--b21-160-cid21-2296-snapshot/2021nov13-oc180-r56--b21-160-cid21-2296-snapshot.B21-160.CID21-2296.B548_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-2296',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out29-oc180-r54--b21-155-cid21-2292-snapshot/2021out29-oc180-r54--b21-155-cid21-2292-snapshot.B21-155.CID21-2292.B546_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-2292',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021out30-gs180-run5-b21-153-cid21-2274-snapshot/2021out30-gs180-run5-b21-153-cid21-2274-snapshot.B21-153.CID21-2274.B503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-2274',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov13-oc180-r56--b21-160-cid21-2436-snapshot/2021nov13-oc180-r56--b21-160-cid21-2436-snapshot.B21-160.CID21-2436.B509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-2436',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov13-oc180-r56--b21-160-cid21-2318-snapshot/2021nov13-oc180-r56--b21-160-cid21-2318-snapshot.B21-160.CID21-2318.B538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-2318',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-167-cid21-2373-snapshot/2021nov25-oc180-r57--b21-167-cid21-2373-snapshot.B21-167.CID21-2373.B509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-2373',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-167-cid21-2363-snapshot/2021nov25-oc180-r57--b21-167-cid21-2363-snapshot.B21-167.CID21-2363.B513_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-2363',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-169-cid21-2454-snapshot/2021nov25-oc180-r57--b21-169-cid21-2454-snapshot.B21-169.CID21-2454.B525_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-2454',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-167-cid21-2408-snapshot/2021nov25-oc180-r57--b21-167-cid21-2408-snapshot.B21-167.CID21-2408.B519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-2408',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-167-cid21-2396-snapshot/2021nov25-oc180-r57--b21-167-cid21-2396-snapshot.B21-167.CID21-2396.B520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-2396',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez07-oc180-r59--b21-173-cid21-2517-snapshot/2021dez07-oc180-r59--b21-173-cid21-2517-snapshot.B21-173.CID21-2517.C540_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-2517',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-169-cid21-2465-snapshot/2021nov25-oc180-r57--b21-169-cid21-2465-snapshot.B21-169.CID21-2465.B534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-2465',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov29-oc180-r58--b21-171-cid21-2609-snapshot/2021nov29-oc180-r58--b21-171-cid21-2609-snapshot.B21-171.CID21-2609.B530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-2609',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-169-cid21-2471-snapshot/2021nov25-oc180-r57--b21-169-cid21-2471-snapshot.B21-169.CID21-2471.B526_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-2471',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov25-oc180-r57--b21-169-cid21-2461-snapshot/2021nov25-oc180-r57--b21-169-cid21-2461-snapshot.B21-169.CID21-2461.B529_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-2461',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez07-oc180-r59--b21-173-cid21-2511-snapshot/2021dez07-oc180-r59--b21-173-cid21-2511-snapshot.B21-173.CID21-2511.C522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-2511',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021nov29-oc180-r58--b21-171-cid21-2483-snapshot/2021nov29-oc180-r58--b21-171-cid21-2483-snapshot.B21-171.CID21-2483.C512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-2483',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez07-oc180-r59--b21-173-cid21-2551-snapshot/2021dez07-oc180-r59--b21-173-cid21-2551-snapshot.B21-173.CID21-2551.B501_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-2551',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez07-oc180-r59--b21-173-cid21-2641-snapshot/2021dez07-oc180-r59--b21-173-cid21-2641-snapshot.B21-173.CID21-2641.C535_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-2641',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez07-oc180-r59--b21-173-cid21-2561-snapshot/2021dez07-oc180-r59--b21-173-cid21-2561-snapshot.B21-173.CID21-2561.B548_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-2561',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez22-oc180-run6-b21-180-cid21-2563-snapshot/2021dez22-oc180-run6-b21-180-cid21-2563-snapshot.B21-180.CID21-2563.B510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-2563',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez22-oc180-run6-b21-180-cid21-2657-snapshot/2021dez22-oc180-run6-b21-180-cid21-2657-snapshot.B21-180.CID21-2657.B514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-2657',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez11-oc180-r60--b21-175-cid21-2599-snapshot/2021dez11-oc180-r60--b21-175-cid21-2599-snapshot.B21-175.CID21-2599.A502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-2599',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez11-oc180-r60--b21-175-cid21-2645-snapshot/2021dez11-oc180-r60--b21-175-cid21-2645-snapshot.B21-175.CID21-2645.C541_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-2645',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez22-oc180-run6-b21-180-cid21-2676-snapshot/2021dez22-oc180-run6-b21-180-cid21-2676-snapshot.B21-180.CID21-2676.B515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-2676',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez11-oc180-r60--b21-175-cid21-2625-snapshot/2021dez11-oc180-r60--b21-175-cid21-2625-snapshot.B21-175.CID21-2625.C534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-2625',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez22-oc180-run6-b21-180-cid21-2670-snapshot/2021dez22-oc180-run6-b21-180-cid21-2670-snapshot.B21-180.CID21-2670.B511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-2670',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021dez30-run63-gs18-b21-184-cid21-2715-snapshot/2021dez30-run63-gs18-b21-184-cid21-2715-snapshot.B21-184.CID21-2715.B530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-2715',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan13-gs180-run0-b22-7-cid22-72-snapshot/2022jan13-gs180-run0-b22-7-cid22-72-snapshot.B22-7.CID22-72.B531_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-72',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan06-gs180-run0-b22-4-cid21-2811-snapshot/2022jan06-gs180-run0-b22-4-cid21-2811-snapshot.B22-4.CID21-2811.B502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-2811',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan08-gs180-run0-b22-5-cid21-2825-snapshot/2022jan08-gs180-run0-b22-5-cid21-2825-snapshot.B22-5.CID21-2825.B510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-2825',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan13-gs180-run0-b22-7-cid22-62-snapshot/2022jan13-gs180-run0-b22-7-cid22-62-snapshot.B22-7.CID22-62.B527_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-62',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan13-gs180-run0-b22-7-cid22-46-snapshot/2022jan13-gs180-run0-b22-7-cid22-46-snapshot.B22-7.CID22-46.B530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-46',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan21-gs180-run0-b22-11-cid22-88-snapshot/2022jan21-gs180-run0-b22-11-cid22-88-snapshot.B22-11.CID22-88.B524_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-88',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan28-gs180-run0-b22-15-cid22-122-snapshot/2022jan28-gs180-run0-b22-15-cid22-122-snapshot.B22-15.CID22-122.B547_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-122',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan21-gs180-run0-b22-11-cid22-86-snapshot/2022jan21-gs180-run0-b22-11-cid22-86-snapshot.B22-11.CID22-86.B508_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-86',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan28-gs180-run0-b22-15-cid22-168-snapshot/2022jan28-gs180-run0-b22-15-cid22-168-snapshot.B22-15.CID22-168.B526_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-168',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan28-gs180-run0-b22-15-cid22-181-snapshot/2022jan28-gs180-run0-b22-15-cid22-181-snapshot.B22-15.CID22-181.B527_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-181',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022jan28-gs180-run0-b22-15-cid22-179-snapshot/2022jan28-gs180-run0-b22-15-cid22-179-snapshot.B22-15.CID22-179.B531_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-179',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021jul15-oc180-run0-b21-92-cid21-1230-snapshot/2021jul15-oc180-run0-b21-92-cid21-1230-snapshot.B21-92.CID21-1230.A535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1230',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1101-snapshot/2022mai11-oc180-run0-b22-60-cid22-1101-snapshot.B22-60.CID22-1101.B544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-1101',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021jul15-oc180-run0-b21-92-cid21-1225-snapshot/2021jul15-oc180-run0-b21-92-cid21-1225-snapshot.B21-92.CID21-1225.A529_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1225',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1087-snapshot/2022mai11-oc180-run0-b22-60-cid22-1087-snapshot.B22-60.CID22-1087.B530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-1087',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1084-snapshot/2022mai11-oc180-run0-b22-60-cid22-1084-snapshot.B22-60.CID22-1084.B527_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-1084',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1104-snapshot/2022mai11-oc180-run0-b22-60-cid22-1104-snapshot.B22-60.CID22-1104.B547_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-1104',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1082-snapshot/2022mai11-oc180-run0-b22-60-cid22-1082-snapshot.B22-60.CID22-1082.B525_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-1082',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021jul15-oc180-run0-b21-92-cid21-1223-snapshot/2021jul15-oc180-run0-b21-92-cid21-1223-snapshot.B21-92.CID21-1223.A528_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1223',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1086-snapshot/2022mai11-oc180-run0-b22-60-cid22-1086-snapshot.B22-60.CID22-1086.B529_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-1086',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1091-snapshot/2022mai11-oc180-run0-b22-60-cid22-1091-snapshot.B22-60.CID22-1091.B534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-1091',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021jul15-oc180-run0-b21-92-cid21-1229-snapshot/2021jul15-oc180-run0-b21-92-cid21-1229-snapshot.B21-92.CID21-1229.A534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1229',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1105-snapshot/2022mai11-oc180-run0-b22-60-cid22-1105-snapshot.B22-60.CID22-1105.B548_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-1105',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai13-gs180-run1-b22-63-cid22-1106-snapshot/2022mai13-gs180-run1-b22-63-cid22-1106-snapshot.B22-63.CID22-1106.B517_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-1106',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1102-snapshot/2022mai11-oc180-run0-b22-60-cid22-1102-snapshot.B22-60.CID22-1102.B545_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-1102',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2021ago23-oc180-run0-b21-110-cid21-1552-snapshot/2021ago23-oc180-run0-b21-110-cid21-1552-snapshot.B21-110.CID21-1552.A541_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1552',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1103-snapshot/2022mai11-oc180-run0-b22-60-cid22-1103-snapshot.B22-60.CID22-1103.B546_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-1103',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/tmb/oc180_samples_with_gs395//2022mai11-oc180-run0-b22-60-cid22-1099-snapshot/2022mai11-oc180-run0-b22-60-cid22-1099-snapshot.B22-60.CID22-1099.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-1099',
                        'sample_type': 'tumor'}
                ]
            }
        }
    }
def get_final_oc395_hotspots_validation_data_for_oc395():

    # The gs395 samples that were done as the concordance to gs180 - 160 samples - gs395 Validation

    return {
        "amplicon_metrics": {
            "run": "oc395primer_boosted_analysis_final_oc395",
            "images_dir": "images/oc395primer_boosted_analysis_final_oc395/",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1705-snapshot/2021set01-oc395-run0-b21-116-cid21-1705-snapshot.B21-116.CID21-1705.B507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1705',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1697-snapshot/2021set01-oc395-run0-b21-116-cid21-1697-snapshot.B21-116.CID21-1697.B503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1697',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-150-snapshot/2022jan17-oc395-run0-b22-8-cid22-150-snapshot.B22-8.CID22-150.B540_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-150',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1058-snapshot/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot.B21-90.CID21-1058.C512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1058',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-127-snapshot/2022jan17-oc395-run0-b22-9-cid22-127-snapshot.B22-9.CID22-127.B517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-127',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-130-snapshot/2022jan17-oc395-run0-b22-9-cid22-130-snapshot.B22-9.CID22-130.B520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-130',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-254-snapshot/2022fev18-oc395-run0-b22-13-cid22-254-snapshot.B22-13.CID22-254.C522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-254',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-258-snapshot/2022fev18-oc395-run0-b22-13-cid22-258-snapshot.B22-13.CID22-258.C526_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-258',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-251-snapshot/2022fev18-oc395-run0-b22-13-cid22-251-snapshot.B22-13.CID22-251.C519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-251',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-137-snapshot/2022jan17-oc395-run0-b22-9-cid22-137-snapshot.B22-9.CID22-137.B527_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-137',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-259-snapshot/2022fev18-oc395-run0-b22-13-cid22-259-snapshot.B22-13.CID22-259.C527_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-259',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-252-snapshot/2022fev18-oc395-run0-b22-13-cid22-252-snapshot.B22-13.CID22-252.C520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-252',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1049-snapshot/2021jul08-oc395-run0-b21-90-cid21-1049-snapshot.B21-90.CID21-1049.C503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1049',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-154-snapshot/2022jan17-oc395-run0-b22-8-cid22-154-snapshot.B22-8.CID22-154.B544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-154',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-132-snapshot/2022jan17-oc395-run0-b22-9-cid22-132-snapshot.B22-9.CID22-132.B522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-132',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-144-snapshot/2022jan17-oc395-run0-b22-8-cid22-144-snapshot.B22-8.CID22-144.B534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-144',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-146-snapshot/2022jan17-oc395-run0-b22-8-cid22-146-snapshot.B22-8.CID22-146.B536_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-146',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-169-snapshot/2022jan17-oc395-run0-b22-9-cid22-169-snapshot.B22-9.CID22-169.B526_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-169',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-260-snapshot/2022fev18-oc395-run0-b22-13-cid22-260-snapshot.B22-13.CID22-260.C528_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-260',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-131-snapshot/2022jan17-oc395-run0-b22-9-cid22-131-snapshot.B22-9.CID22-131.B521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-131',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-134-snapshot/2022jan17-oc395-run0-b22-9-cid22-134-snapshot.B22-9.CID22-134.B524_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-134',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-135-snapshot/2022jan17-oc395-run0-b22-9-cid22-135-snapshot.B22-9.CID22-135.B525_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-135',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev15-oc395-run0-b22-12-cid22-238-snapshot/2022fev15-oc395-run0-b22-12-cid22-238-snapshot.B22-12.CID22-238.C501_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-238',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev15-oc395-run0-b22-12-cid22-245-snapshot/2022fev15-oc395-run0-b22-12-cid22-245-snapshot.B22-12.CID22-245.C508_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-245',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-128-snapshot/2022jan17-oc395-run0-b22-9-cid22-128-snapshot.B22-9.CID22-128.B518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-128',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-140-snapshot/2022jan17-oc395-run0-b22-9-cid22-140-snapshot.B22-9.CID22-140.B530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-140',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-139-snapshot/2022jan17-oc395-run0-b22-9-cid22-139-snapshot.B22-9.CID22-139.B529_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-139',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-143-snapshot/2022jan17-oc395-run0-b22-8-cid22-143-snapshot.B22-8.CID22-143.B533_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-143',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-250-snapshot/2022fev18-oc395-run0-b22-13-cid22-250-snapshot.B22-13.CID22-250.C518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-250',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev15-oc395-run0-b22-12-cid22-242-snapshot/2022fev15-oc395-run0-b22-12-cid22-242-snapshot.B22-12.CID22-242.C505_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-242',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-141-snapshot/2022jan17-oc395-run0-b22-9-cid22-141-snapshot.B22-9.CID22-141.B531_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-141',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev15-oc395-run0-b22-12-cid22-239-snapshot/2022fev15-oc395-run0-b22-12-cid22-239-snapshot.B22-12.CID22-239.C502_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-239',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev15-oc395-run0-b22-12-cid22-243-snapshot/2022fev15-oc395-run0-b22-12-cid22-243-snapshot.B22-12.CID22-243.C506_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-243',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev15-oc395-run0-b22-12-cid22-246-snapshot/2022fev15-oc395-run0-b22-12-cid22-246-snapshot.B22-12.CID22-246.C509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-246',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev15-oc395-run0-b22-12-cid22-244-snapshot/2022fev15-oc395-run0-b22-12-cid22-244-snapshot.B22-12.CID22-244.C507_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-244',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev15-oc395-run0-b22-12-cid22-241-snapshot/2022fev15-oc395-run0-b22-12-cid22-241-snapshot.B22-12.CID22-241.C504_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-241',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-148-snapshot/2022jan17-oc395-run0-b22-8-cid22-148-snapshot.B22-8.CID22-148.B538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-148',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-9-cid22-138-snapshot/2022jan17-oc395-run0-b22-9-cid22-138-snapshot.B22-9.CID22-138.B528_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-138',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-152-snapshot/2022jan17-oc395-run0-b22-8-cid22-152-snapshot.B22-8.CID22-152.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-152',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-156-snapshot/2022jan17-oc395-run0-b22-8-cid22-156-snapshot.B22-8.CID22-156.B546_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-156',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev15-oc395-run0-b22-12-cid22-240-snapshot/2022fev15-oc395-run0-b22-12-cid22-240-snapshot.B22-12.CID22-240.C503_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-240',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-155-snapshot/2022jan17-oc395-run0-b22-8-cid22-155-snapshot.B22-8.CID22-155.B545_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-155',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-151-snapshot/2022jan17-oc395-run0-b22-8-cid22-151-snapshot.B22-8.CID22-151.B541_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-151',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-145-snapshot/2022jan17-oc395-run0-b22-8-cid22-145-snapshot.B22-8.CID22-145.B535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-145',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-153-snapshot/2022jan17-oc395-run0-b22-8-cid22-153-snapshot.B22-8.CID22-153.B543_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-153',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022jan17-oc395-run0-b22-8-cid22-147-snapshot/2022jan17-oc395-run0-b22-8-cid22-147-snapshot.B22-8.CID22-147.B537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-147',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1057-snapshot/2021jul08-oc395-run0-b21-90-cid21-1057-snapshot.B21-90.CID21-1057.C511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1057',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1052-snapshot/2021jul08-oc395-run0-b21-90-cid21-1052-snapshot.B21-90.CID21-1052.C506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1052',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1053-snapshot/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot.B21-91.CID21-1053.B517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1053',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1051-snapshot/2021jul08-oc395-run0-b21-90-cid21-1051-snapshot.B21-90.CID21-1051.C505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1051',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1048-snapshot/2021jul08-oc395-run0-b21-90-cid21-1048-snapshot.B21-90.CID21-1048.C502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1048',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1054-snapshot/2021jul08-oc395-run0-b21-91-cid21-1054-snapshot.B21-91.CID21-1054.B518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1054',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1707-snapshot/2021set01-oc395-run0-b21-116-cid21-1707-snapshot.B21-116.CID21-1707.B508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1707',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1727-snapshot/2021set01-oc395-run0-b21-116-cid21-1727-snapshot.B21-116.CID21-1727.B515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1727',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1767-snapshot/2021set02-oc395-run0-b21-118-cid21-1767-snapshot.B21-118.CID21-1767.B530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1767',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1711-snapshot/2021set01-oc395-run0-b21-116-cid21-1711-snapshot.B21-116.CID21-1711.B509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1711',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1715-snapshot/2021set01-oc395-run0-b21-116-cid21-1715-snapshot.B21-116.CID21-1715.B511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1715',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1721-snapshot/2021set01-oc395-run0-b21-116-cid21-1721-snapshot.B21-116.CID21-1721.B512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1721',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1695-snapshot/2021set01-oc395-run0-b21-116-cid21-1695-snapshot.B21-116.CID21-1695.B502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1695',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1723-snapshot/2021set01-oc395-run0-b21-116-cid21-1723-snapshot.B21-116.CID21-1723.B513_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1723',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1713-snapshot/2021set01-oc395-run0-b21-116-cid21-1713-snapshot.B21-116.CID21-1713.B510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1713',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1693-snapshot/2021set01-oc395-run0-b21-116-cid21-1693-snapshot.B21-116.CID21-1693.B501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1693',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1763-snapshot/2021set02-oc395-run0-b21-118-cid21-1763-snapshot.B21-118.CID21-1763.B528_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1763',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1725-snapshot/2021set01-oc395-run0-b21-116-cid21-1725-snapshot.B21-116.CID21-1725.B514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1725',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1765-snapshot/2021set02-oc395-run0-b21-118-cid21-1765-snapshot.B21-118.CID21-1765.B529_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1765',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1699-snapshot/2021set01-oc395-run0-b21-116-cid21-1699-snapshot.B21-116.CID21-1699.B504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1699',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set01-oc395-run0-b21-116-cid21-1729-snapshot/2021set01-oc395-run0-b21-116-cid21-1729-snapshot.B21-116.CID21-1729.B516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1729',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1741-snapshot/2021set02-oc395-run0-b21-118-cid21-1741-snapshot.B21-118.CID21-1741.B517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1741',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1743-snapshot/2021set02-oc395-run0-b21-118-cid21-1743-snapshot.B21-118.CID21-1743.B518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1743',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1745-snapshot/2021set02-oc395-run0-b21-118-cid21-1745-snapshot.B21-118.CID21-1745.B519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1745',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1747-snapshot/2021set02-oc395-run0-b21-118-cid21-1747-snapshot.B21-118.CID21-1747.B520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1747',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1749-snapshot/2021set02-oc395-run0-b21-118-cid21-1749-snapshot.B21-118.CID21-1749.B521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1749',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1751-snapshot/2021set02-oc395-run0-b21-118-cid21-1751-snapshot.B21-118.CID21-1751.B522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1751',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1771-snapshot/2021set02-oc395-run0-b21-118-cid21-1771-snapshot.B21-118.CID21-1771.B532_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1771',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1753-snapshot/2021set02-oc395-run0-b21-118-cid21-1753-snapshot.B21-118.CID21-1753.B523_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1753',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1757-snapshot/2021set02-oc395-run0-b21-118-cid21-1757-snapshot.B21-118.CID21-1757.B525_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1757',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1759-snapshot/2021set02-oc395-run0-b21-118-cid21-1759-snapshot.B21-118.CID21-1759.B526_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1759',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1755-snapshot/2021set02-oc395-run0-b21-118-cid21-1755-snapshot.B21-118.CID21-1755.B524_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1755',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021set02-oc395-run0-b21-118-cid21-1761-snapshot/2021set02-oc395-run0-b21-118-cid21-1761-snapshot.B21-118.CID21-1761.B527_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1761',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-255-snapshot/2022fev18-oc395-run0-b22-13-cid22-255-snapshot.B22-13.CID22-255.C523_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-255',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-524-snapshot/2022mar08-oc395-run0-b22-32-cid22-524-snapshot.B22-32.CID22-524.C534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-524',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-256-snapshot/2022fev18-oc395-run0-b22-13-cid22-256-snapshot.B22-13.CID22-256.C524_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-256',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-523-snapshot/2022mar08-oc395-run0-b22-32-cid22-523-snapshot.B22-32.CID22-523.C533_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-523',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-253-snapshot/2022fev18-oc395-run0-b22-13-cid22-253-snapshot.B22-13.CID22-253.C521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-253',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-249-snapshot/2022fev18-oc395-run0-b22-13-cid22-249-snapshot.B22-13.CID22-249.C517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-249',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-257-snapshot/2022fev18-oc395-run0-b22-13-cid22-257-snapshot.B22-13.CID22-257.C525_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-257',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-782-snapshot/2022abr01-oc395-run0-b22-43-cid22-782-snapshot.B22-43.CID22-782.B535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-782',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-799-snapshot/2022abr05-oc395-run0-b22-44-cid22-799-snapshot.B22-44.CID22-799.C523_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-799',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-785-snapshot/2022abr01-oc395-run0-b22-43-cid22-785-snapshot.B22-43.CID22-785.B538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-785',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-774-snapshot/2022abr01-oc395-run0-b22-42-cid22-774-snapshot.B22-42.CID22-774.B527_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-774',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-533-snapshot/2022mar08-oc395-run0-b22-32-cid22-533-snapshot.B22-32.CID22-533.C543_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-533',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-775-snapshot/2022abr01-oc395-run0-b22-42-cid22-775-snapshot.B22-42.CID22-775.B529_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-775',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-779-snapshot/2022abr01-oc395-run0-b22-43-cid22-779-snapshot.B22-43.CID22-779.B532_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-779',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-792-snapshot/2022abr01-oc395-run0-b22-43-cid22-792-snapshot.B22-43.CID22-792.B545_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-792',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-532-snapshot/2022mar08-oc395-run0-b22-32-cid22-532-snapshot.B22-32.CID22-532.C542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-532',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-778-snapshot/2022abr01-oc395-run0-b22-42-cid22-778-snapshot.B22-42.CID22-778.B531_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-778',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-786-snapshot/2022abr01-oc395-run0-b22-43-cid22-786-snapshot.B22-43.CID22-786.B539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-786',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-701-snapshot/2022mar25-oc395-run0-b22-37-cid22-701-snapshot.B22-37.CID22-701.B503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-701',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-531-snapshot/2022mar08-oc395-run0-b22-32-cid22-531-snapshot.B22-32.CID22-531.C541_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-531',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-526-snapshot/2022mar08-oc395-run0-b22-32-cid22-526-snapshot.B22-32.CID22-526.C536_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-526',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-529-snapshot/2022mar08-oc395-run0-b22-32-cid22-529-snapshot.B22-32.CID22-529.C539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-529',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-797-snapshot/2022abr05-oc395-run0-b22-44-cid22-797-snapshot.B22-44.CID22-797.C521_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-797',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-711-snapshot/2022mar25-oc395-run0-b22-37-cid22-711-snapshot.B22-37.CID22-711.B545_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-711',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-528-snapshot/2022mar08-oc395-run0-b22-32-cid22-528-snapshot.B22-32.CID22-528.C538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-528',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-248-snapshot/2022fev18-oc395-run0-b22-13-cid22-248-snapshot.B22-13.CID22-248.C516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-248',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-530-snapshot/2022mar08-oc395-run0-b22-32-cid22-530-snapshot.B22-32.CID22-530.C540_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-530',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-703-snapshot/2022mar25-oc395-run0-b22-37-cid22-703-snapshot.B22-37.CID22-703.B502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-703',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-527-snapshot/2022mar08-oc395-run0-b22-32-cid22-527-snapshot.B22-32.CID22-527.C537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-527',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-525-snapshot/2022mar08-oc395-run0-b22-32-cid22-525-snapshot.B22-32.CID22-525.C535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-525',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-712-snapshot/2022mar25-oc395-run0-b22-37-cid22-712-snapshot.B22-37.CID22-712.B544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-712',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-715-snapshot/2022mar25-oc395-run0-b22-37-cid22-715-snapshot.B22-37.CID22-715.B546_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-715',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-800-snapshot/2022abr05-oc395-run0-b22-44-cid22-800-snapshot.B22-44.CID22-800.C524_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-800',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-537-snapshot/2022mar08-oc395-run0-b22-32-cid22-537-snapshot.B22-32.CID22-537.C547_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-537',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-713-snapshot/2022mar25-oc395-run0-b22-37-cid22-713-snapshot.B22-37.CID22-713.B543_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-713',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-705-snapshot/2022mar25-oc395-run0-b22-37-cid22-705-snapshot.B22-37.CID22-705.B505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-705',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-708-snapshot/2022mar25-oc395-run0-b22-37-cid22-708-snapshot.B22-37.CID22-708.B541_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-708',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-709-snapshot/2022mar25-oc395-run0-b22-37-cid22-709-snapshot.B22-37.CID22-709.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-709',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-535-snapshot/2022mar08-oc395-run0-b22-32-cid22-535-snapshot.B22-32.CID22-535.C545_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-535',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-536-snapshot/2022mar08-oc395-run0-b22-32-cid22-536-snapshot.B22-32.CID22-536.C546_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-536',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar08-oc395-run0-b22-32-cid22-534-snapshot/2022mar08-oc395-run0-b22-32-cid22-534-snapshot.B22-32.CID22-534.C544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-534',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-710-snapshot/2022mar25-oc395-run0-b22-37-cid22-710-snapshot.B22-37.CID22-710.B508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-710',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-707-snapshot/2022mar25-oc395-run0-b22-37-cid22-707-snapshot.B22-37.CID22-707.B507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-707',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-704-snapshot/2022mar25-oc395-run0-b22-37-cid22-704-snapshot.B22-37.CID22-704.B504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-704',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-706-snapshot/2022mar25-oc395-run0-b22-37-cid22-706-snapshot.B22-37.CID22-706.B506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-706',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-798-snapshot/2022abr05-oc395-run0-b22-44-cid22-798-snapshot.B22-44.CID22-798.C522_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-798',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-702-snapshot/2022mar25-oc395-run0-b22-37-cid22-702-snapshot.B22-37.CID22-702.B501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-702',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022fev18-oc395-run0-b22-13-cid22-261-snapshot/2022fev18-oc395-run0-b22-13-cid22-261-snapshot.B22-13.CID22-261.C529_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-261',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-780-snapshot/2022abr01-oc395-run0-b22-43-cid22-780-snapshot.B22-43.CID22-780.B534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-780',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-781-snapshot/2022abr01-oc395-run0-b22-43-cid22-781-snapshot.B22-43.CID22-781.B533_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-781',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-783-snapshot/2022abr01-oc395-run0-b22-43-cid22-783-snapshot.B22-43.CID22-783.B536_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-783',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022mar25-oc395-run0-b22-37-cid22-714-snapshot/2022mar25-oc395-run0-b22-37-cid22-714-snapshot.B22-37.CID22-714.B547_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-714',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-784-snapshot/2022abr01-oc395-run0-b22-43-cid22-784-snapshot.B22-43.CID22-784.B537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-784',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-795-snapshot/2022abr05-oc395-run0-b22-44-cid22-795-snapshot.B22-44.CID22-795.B548_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-795',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-790-snapshot/2022abr01-oc395-run0-b22-43-cid22-790-snapshot.B22-43.CID22-790.B543_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-790',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-791-snapshot/2022abr01-oc395-run0-b22-43-cid22-791-snapshot.B22-43.CID22-791.B544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-791',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-776-snapshot/2022abr01-oc395-run0-b22-42-cid22-776-snapshot.B22-42.CID22-776.B530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-776',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-794-snapshot/2022abr05-oc395-run0-b22-44-cid22-794-snapshot.B22-44.CID22-794.C520_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-794',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-787-snapshot/2022abr01-oc395-run0-b22-43-cid22-787-snapshot.B22-43.CID22-787.B540_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-787',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr05-oc395-run0-b22-44-cid22-796-snapshot/2022abr05-oc395-run0-b22-44-cid22-796-snapshot.B22-44.CID22-796.B547_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-796',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-788-snapshot/2022abr01-oc395-run0-b22-43-cid22-788-snapshot.B22-43.CID22-788.B541_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-788',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-793-snapshot/2022abr01-oc395-run0-b22-43-cid22-793-snapshot.B22-43.CID22-793.B546_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-793',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-43-cid22-789-snapshot/2022abr01-oc395-run0-b22-43-cid22-789-snapshot.B22-43.CID22-789.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-789',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2022abr01-oc395-run0-b22-42-cid22-777-snapshot/2022abr01-oc395-run0-b22-42-cid22-777-snapshot.B22-42.CID22-777.B528_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-777',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1190-snapshot/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot.B21-90.CID21-1190.A532_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1190',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1176-snapshot/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot.B21-91.CID21-1176.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1176',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1172-snapshot/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot.B21-91.CID21-1172.B522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1172',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1183-snapshot/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot.B21-90.CID21-1183.C534_N702.hotspots.htqc.txt',
                        'sample_id': 'cid21-1183',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1063-snapshot/2021jul08-oc395-run0-b21-91-cid21-1063-snapshot.B21-91.CID21-1063.B520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1063',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1180-snapshot/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot.B21-91.CID21-1180.B546_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1180',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1178-snapshot/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot.B21-91.CID21-1178.B544_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-1178',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1171-snapshot/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot.B21-91.CID21-1171.B521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1171',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1184-snapshot/2021jul08-oc395-run0-b21-91-cid21-1184-snapshot.B21-91.CID21-1184.C533_N701.hotspots.htqc.txt',
                        'sample_id': 'cid21-1184',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1189-snapshot/2021jul08-oc395-run0-b21-90-cid21-1189-snapshot.B21-90.CID21-1189.C540_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1189',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1187-snapshot/2021jul08-oc395-run0-b21-90-cid21-1187-snapshot.B21-90.CID21-1187.C538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid21-1187',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1181-snapshot/2021jul08-oc395-run0-b21-91-cid21-1181-snapshot.B21-91.CID21-1181.B547_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1181',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1182-snapshot/2021jul08-oc395-run0-b21-91-cid21-1182-snapshot.B21-91.CID21-1182.B548_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-1182',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1177-snapshot/2021jul08-oc395-run0-b21-91-cid21-1177-snapshot.B21-91.CID21-1177.B543_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1177',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-90-cid21-1188-snapshot/2021jul08-oc395-run0-b21-90-cid21-1188-snapshot.B21-90.CID21-1188.C539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1188',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1179-snapshot/2021jul08-oc395-run0-b21-91-cid21-1179-snapshot.B21-91.CID21-1179.B545_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-1179',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost//2021jul08-oc395-run0-b21-91-cid21-1173-snapshot/2021jul08-oc395-run0-b21-91-cid21-1173-snapshot.B21-91.CID21-1173.B523_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-1173',
                        'sample_type': 'tumor'}
                ]
            }
        }
    }
