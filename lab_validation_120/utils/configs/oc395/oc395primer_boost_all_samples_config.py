"""Simple config for the oc395 (GS396) udpated primer concentration of primers - All samples"""


def get_oc395primer_boost_htqc_plots_all_33_samples():
    """
        Get the QC files for oc395 (GS396) oudpated primer concentration of primers - All samples
        """

    key_2_use = 'oc395primer_boost_htqc_plots_all_33_samples'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GSP Primer Boost  - 33 Samples - Across 2 Runs",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1047-snapshot/2021jul08-oc395-run0-b21-90-cid21-1047-snapshot.B21-90.CID21-1047.C501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1047-snapshot/2021jul08-oc395-run0-b21-90-cid21-1047-snapshot.B21-90.CID21-1047.C501_N701.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid21-1047',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1048-snapshot/2021jul08-oc395-run0-b21-90-cid21-1048-snapshot.B21-90.CID21-1048.C502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1048-snapshot/2021jul08-oc395-run0-b21-90-cid21-1048-snapshot.B21-90.CID21-1048.C502_N702.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid21-1048',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1049-snapshot/2021jul08-oc395-run0-b21-90-cid21-1049-snapshot.B21-90.CID21-1049.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1049-snapshot/2021jul08-oc395-run0-b21-90-cid21-1049-snapshot.B21-90.CID21-1049.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid21-1049',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot.B21-90.CID21-1050.C504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot.B21-90.CID21-1050.C504_N704.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid21-1050',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1051-snapshot/2021jul08-oc395-run0-b21-90-cid21-1051-snapshot.B21-90.CID21-1051.C505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1051-snapshot/2021jul08-oc395-run0-b21-90-cid21-1051-snapshot.B21-90.CID21-1051.C505_N705.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid21-1051',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1052-snapshot/2021jul08-oc395-run0-b21-90-cid21-1052-snapshot.B21-90.CID21-1052.C506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1052-snapshot/2021jul08-oc395-run0-b21-90-cid21-1052-snapshot.B21-90.CID21-1052.C506_N706.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid21-1052',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot.B21-91.CID21-1053.B517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot.B21-91.CID21-1053.B517_N701.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid21-1053',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1054-snapshot/2021jul08-oc395-run0-b21-91-cid21-1054-snapshot.B21-91.CID21-1054.B518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1054-snapshot/2021jul08-oc395-run0-b21-91-cid21-1054-snapshot.B21-91.CID21-1054.B518_N702.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid21-1054',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1055-snapshot/2021jul08-oc395-run0-b21-91-cid21-1055-snapshot.B21-91.CID21-1055.B519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1055-snapshot/2021jul08-oc395-run0-b21-91-cid21-1055-snapshot.B21-91.CID21-1055.B519_N703.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid21-1055',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1057-snapshot/2021jul08-oc395-run0-b21-90-cid21-1057-snapshot.B21-90.CID21-1057.C511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1057-snapshot/2021jul08-oc395-run0-b21-90-cid21-1057-snapshot.B21-90.CID21-1057.C511_N703.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid21-1057',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot.B21-90.CID21-1058.C512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot.B21-90.CID21-1058.C512_N704.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid21-1058',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1063-snapshot/2021jul08-oc395-run0-b21-91-cid21-1063-snapshot.B21-91.CID21-1063.B520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1063-snapshot/2021jul08-oc395-run0-b21-91-cid21-1063-snapshot.B21-91.CID21-1063.B520_N704.coverage.summary.htqc.txt',
                        'sample_id': '12a-cid21-1063',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot.B21-91.CID21-1171.B521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot.B21-91.CID21-1171.B521_N705.coverage.summary.htqc.txt',
                        'sample_id': '13a-cid21-1171',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot.B21-91.CID21-1172.B522_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot.B21-91.CID21-1172.B522_N706.coverage.summary.htqc.txt',
                        'sample_id': '14a-cid21-1172',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1173-snapshot/2021jul08-oc395-run0-b21-91-cid21-1173-snapshot.B21-91.CID21-1173.B523_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1173-snapshot/2021jul08-oc395-run0-b21-91-cid21-1173-snapshot.B21-91.CID21-1173.B523_N707.coverage.summary.htqc.txt',
                        'sample_id': '15a-cid21-1173',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1174-snapshot/2021jul08-oc395-run0-b21-91-cid21-1174-snapshot.B21-91.CID21-1174.B524_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1174-snapshot/2021jul08-oc395-run0-b21-91-cid21-1174-snapshot.B21-91.CID21-1174.B524_N708.coverage.summary.htqc.txt',
                        'sample_id': '16a-cid21-1174',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1175-snapshot/2021jul08-oc395-run0-b21-91-cid21-1175-snapshot.B21-91.CID21-1175.B541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1175-snapshot/2021jul08-oc395-run0-b21-91-cid21-1175-snapshot.B21-91.CID21-1175.B541_N701.coverage.summary.htqc.txt',
                        'sample_id': '17a-cid21-1175',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot.B21-91.CID21-1176.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot.B21-91.CID21-1176.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '18a-cid21-1176',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1177-snapshot/2021jul08-oc395-run0-b21-91-cid21-1177-snapshot.B21-91.CID21-1177.B543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1177-snapshot/2021jul08-oc395-run0-b21-91-cid21-1177-snapshot.B21-91.CID21-1177.B543_N703.coverage.summary.htqc.txt',
                        'sample_id': '19a-cid21-1177',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot.B21-91.CID21-1178.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot.B21-91.CID21-1178.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '20a-cid21-1178',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1179-snapshot/2021jul08-oc395-run0-b21-91-cid21-1179-snapshot.B21-91.CID21-1179.B545_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1179-snapshot/2021jul08-oc395-run0-b21-91-cid21-1179-snapshot.B21-91.CID21-1179.B545_N705.coverage.summary.htqc.txt',
                        'sample_id': '21a-cid21-1179',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot.B21-91.CID21-1180.B546_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot.B21-91.CID21-1180.B546_N706.coverage.summary.htqc.txt',
                        'sample_id': '22a-cid21-1180',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1181-snapshot/2021jul08-oc395-run0-b21-91-cid21-1181-snapshot.B21-91.CID21-1181.B547_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1181-snapshot/2021jul08-oc395-run0-b21-91-cid21-1181-snapshot.B21-91.CID21-1181.B547_N707.coverage.summary.htqc.txt',
                        'sample_id': '23a-cid21-1181',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1182-snapshot/2021jul08-oc395-run0-b21-91-cid21-1182-snapshot.B21-91.CID21-1182.B548_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1182-snapshot/2021jul08-oc395-run0-b21-91-cid21-1182-snapshot.B21-91.CID21-1182.B548_N708.coverage.summary.htqc.txt',
                        'sample_id': '24a-cid21-1182',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1184-snapshot/2021jul08-oc395-run0-b21-91-cid21-1184-snapshot.B21-91.CID21-1184.C533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1184-snapshot/2021jul08-oc395-run0-b21-91-cid21-1184-snapshot.B21-91.CID21-1184.C533_N701.coverage.summary.htqc.txt',
                        'sample_id': '25a-cid21-1184',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot.B21-90.CID21-1183.C534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot.B21-90.CID21-1183.C534_N702.coverage.summary.htqc.txt',
                        'sample_id': '26a-cid21-1183',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1191-snapshot/2021jul08-oc395-run0-b21-90-cid21-1191-snapshot.B21-90.CID21-1191.C535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1191-snapshot/2021jul08-oc395-run0-b21-90-cid21-1191-snapshot.B21-90.CID21-1191.C535_N703.coverage.summary.htqc.txt',
                        'sample_id': '27a-cid21-1191',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1186-snapshot/2021jul08-oc395-run0-b21-90-cid21-1186-snapshot.B21-90.CID21-1186.C536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1186-snapshot/2021jul08-oc395-run0-b21-90-cid21-1186-snapshot.B21-90.CID21-1186.C536_N704.coverage.summary.htqc.txt',
                        'sample_id': '28a-cid21-1186',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot.B21-90.CID21-1185.C537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot.B21-90.CID21-1185.C537_N705.coverage.summary.htqc.txt',
                        'sample_id': '29a-cid21-1185',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1187-snapshot/2021jul08-oc395-run0-b21-90-cid21-1187-snapshot.B21-90.CID21-1187.C538_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1187-snapshot/2021jul08-oc395-run0-b21-90-cid21-1187-snapshot.B21-90.CID21-1187.C538_N706.coverage.summary.htqc.txt',
                        'sample_id': '30a-cid21-1187',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1188-snapshot/2021jul08-oc395-run0-b21-90-cid21-1188-snapshot.B21-90.CID21-1188.C539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1188-snapshot/2021jul08-oc395-run0-b21-90-cid21-1188-snapshot.B21-90.CID21-1188.C539_N707.coverage.summary.htqc.txt',
                        'sample_id': '31a-cid21-1188',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1189-snapshot/2021jul08-oc395-run0-b21-90-cid21-1189-snapshot.B21-90.CID21-1189.C540_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1189-snapshot/2021jul08-oc395-run0-b21-90-cid21-1189-snapshot.B21-90.CID21-1189.C540_N708.coverage.summary.htqc.txt',
                        'sample_id': '32a-cid21-1189',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot.B21-90.CID21-1190.A532_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot.B21-90.CID21-1190.A532_N708.coverage.summary.htqc.txt',
                        'sample_id': '33a-cid21-1190',
                        'sample_type': ''}
                ]
            }
        }
    }