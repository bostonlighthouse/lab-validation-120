"""Simple config for the oc395 (GS396) original vs udpated primer concentration of primers"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds

def get_oc395_oc395primer_boost_reproducibility_11_samples():
    """
    Get the VCF files for the midoutput vs high output flow cell - 24 samples
     python3.8 -m tools.create_oc120_vs_oc120plus_reproducibility_config_ds \
     --pair_file tools/input_files/oc395/oc395_oc395primer_boost_equiv_samples.txt \
     --path1 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/' \
     --path2 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/' \
     --sample_num 1 --ds_type reproducibility_vcf
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/oc395_oc395primer_boost_equiv_htqc_plots_11_samples",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    'scratch/oc395/oc395_primer_boost/oc395_primer_boost_comparison_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                    'scratch/oc395/oc395_primer_boost/oc395_primer_boost_comparison_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'],
                "indel": [
                    'scratch/oc395/oc395_primer_boost/oc395_primer_boost_comparison_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                    'scratch/oc395/oc395_primer_boost/oc395_primer_boost_comparison_vaf_8.0_depth_100_all_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'],
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file": [
                        'scratch/oc395/oc395_primer_boost/oc395_primer_boost_comparison_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                        'scratch/oc395/oc395_primer_boost/oc395_primer_boost_comparison_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        'scratch/oc395/oc395_primer_boost/oc395_primer_boost_comparison_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                        'scratch/oc395/oc395_primer_boost/oc395_primer_boost_comparison_vaf_8.0_depth_100_all_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS395 no primer boost",
                "y_axis_label": "GS395 primer boost",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS395 no primer boost",
                "y_axis_label": "GS395 primer boost",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS395 no primer boost",
                "y_axis_label": "GS395 primer boost",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            # "vaf_threshold": 10.0,
            # "depth_threshold": 100,
            # Go over and look at the samples below, in the concordance_files
            # the oc120 is the first VCF in each entry
            # oc120plus is the second VCF  in each entry
            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid20-782',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-782-snapshot/2020-08-28-oc395-run-b20-80-cid20-782-snapshot.B20-80.CID20-782.C504_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '1b-cid21-1050',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot.B21-90.CID21-1050.C504_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample2': [{'sample_id': '2a-cid20-640',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-640-snapshot/2020-08-21-oc395-run-b20-77-cid20-640-snapshot.B20-77.CID20-640.A514_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '2b-cid21-1053',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot.B21-91.CID21-1053.B517_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample3': [{'sample_id': '3a-cid20-607',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-607-snapshot/2020-07-31-oc395-run-b20-70-cid20-607-snapshot.B20-70.CID20-607.A503_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '3b-cid21-1058',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot.B21-90.CID21-1058.C512_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample4': [{'sample_id': '4a-cid20-724',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-724-snapshot/2020-08-21-oc395-run-b20-77-cid20-724-snapshot.B20-77.CID20-724.A516_N708.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '4b-cid21-1171',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot.B21-91.CID21-1171.B521_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample5': [{'sample_id': '5a-cid20-612',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-612-snapshot/2020-07-31-oc395-run-b20-70-cid20-612-snapshot.B20-70.CID20-612.A536_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '5b-cid21-1172',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot.B21-91.CID21-1172.B522_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample6': [{'sample_id': '6a-cid20-605',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-605-snapshot/2020-07-31-oc395-run-b20-70-cid20-605-snapshot.B20-70.CID20-605.A501_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '6b-cid21-1176',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot.B21-91.CID21-1176.B542_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample7': [{'sample_id': '7a-cid20-799',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-799-snapshot/2020-08-28-oc395-run-b20-80-cid20-799-snapshot.B20-80.CID20-799.C511_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '7b-cid21-1178',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot.B21-91.CID21-1178.B544_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample8': [{'sample_id': '8a-cid20-779',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-779-snapshot/2020-08-28-oc395-run-b20-80-cid20-779-snapshot.B20-80.CID20-779.C503_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '8b-cid21-1180',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot.B21-91.CID21-1180.B546_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample9': [{'sample_id': '9a-cid20-713',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-713-snapshot/2020-08-21-oc395-run-b20-77-cid20-713-snapshot.B20-77.CID20-713.A510_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '9b-cid21-1183',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot.B21-90.CID21-1183.C534_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample10': [{'sample_id': '10a-cid20-717',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-717-snapshot/2020-08-21-oc395-run-b20-77-cid20-717-snapshot.B20-77.CID20-717.A511_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '10b-cid21-1185',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot.B21-90.CID21-1185.C537_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample11': [{'sample_id': '11a-cid20-603',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-603-snapshot/2020-07-31-oc395-run-b20-70-cid20-603-snapshot.B20-70.CID20-603.A525_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '11b-cid21-1190',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot.B21-90.CID21-1190.A532_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}]
            }
        }
    }


def get_oc395_oc395primer_boost_equiv_htqc_plots_11_samples():
    """
        Get the QC files for oc395 (GS396) original vs udpated primer concentration of primers
        """

    key_2_use = 'oc395_oc395primer_boost_equiv_htqc_plots_11_samples'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS395 Original vs. GSP Primer Boost  - 11 Samples",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-782-snapshot/2020-08-28-oc395-run-b20-80-cid20-782-snapshot.B20-80.CID20-782.C504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-782-snapshot/2020-08-28-oc395-run-b20-80-cid20-782-snapshot.B20-80.CID20-782.C504_N704.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid20-782',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot.B21-90.CID21-1050.C504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot/2021jul08-oc395-run0-b21-90-cid21-1050-snapshot.B21-90.CID21-1050.C504_N704.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid21-1050',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-640-snapshot/2020-08-21-oc395-run-b20-77-cid20-640-snapshot.B20-77.CID20-640.A514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-640-snapshot/2020-08-21-oc395-run-b20-77-cid20-640-snapshot.B20-77.CID20-640.A514_N706.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid20-640',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot.B21-91.CID21-1053.B517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot/2021jul08-oc395-run0-b21-91-cid21-1053-snapshot.B21-91.CID21-1053.B517_N701.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid21-1053',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-607-snapshot/2020-07-31-oc395-run-b20-70-cid20-607-snapshot.B20-70.CID20-607.A503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-607-snapshot/2020-07-31-oc395-run-b20-70-cid20-607-snapshot.B20-70.CID20-607.A503_N703.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid20-607',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot.B21-90.CID21-1058.C512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot/2021jul08-oc395-run0-b21-90-cid21-1058-snapshot.B21-90.CID21-1058.C512_N704.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid21-1058',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-724-snapshot/2020-08-21-oc395-run-b20-77-cid20-724-snapshot.B20-77.CID20-724.A516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-724-snapshot/2020-08-21-oc395-run-b20-77-cid20-724-snapshot.B20-77.CID20-724.A516_N708.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid20-724',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot.B21-91.CID21-1171.B521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot/2021jul08-oc395-run0-b21-91-cid21-1171-snapshot.B21-91.CID21-1171.B521_N705.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid21-1171',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-612-snapshot/2020-07-31-oc395-run-b20-70-cid20-612-snapshot.B20-70.CID20-612.A536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-612-snapshot/2020-07-31-oc395-run-b20-70-cid20-612-snapshot.B20-70.CID20-612.A536_N704.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid20-612',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot.B21-91.CID21-1172.B522_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot/2021jul08-oc395-run0-b21-91-cid21-1172-snapshot.B21-91.CID21-1172.B522_N706.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid21-1172',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-605-snapshot/2020-07-31-oc395-run-b20-70-cid20-605-snapshot.B20-70.CID20-605.A501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-605-snapshot/2020-07-31-oc395-run-b20-70-cid20-605-snapshot.B20-70.CID20-605.A501_N701.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid20-605',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot.B21-91.CID21-1176.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot/2021jul08-oc395-run0-b21-91-cid21-1176-snapshot.B21-91.CID21-1176.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid21-1176',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-799-snapshot/2020-08-28-oc395-run-b20-80-cid20-799-snapshot.B20-80.CID20-799.C511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-799-snapshot/2020-08-28-oc395-run-b20-80-cid20-799-snapshot.B20-80.CID20-799.C511_N703.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid20-799',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot.B21-91.CID21-1178.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot/2021jul08-oc395-run0-b21-91-cid21-1178-snapshot.B21-91.CID21-1178.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid21-1178',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-779-snapshot/2020-08-28-oc395-run-b20-80-cid20-779-snapshot.B20-80.CID20-779.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-28-oc395-run-b20-80-cid20-779-snapshot/2020-08-28-oc395-run-b20-80-cid20-779-snapshot.B20-80.CID20-779.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid20-779',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot.B21-91.CID21-1180.B546_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot/2021jul08-oc395-run0-b21-91-cid21-1180-snapshot.B21-91.CID21-1180.B546_N706.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid21-1180',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-713-snapshot/2020-08-21-oc395-run-b20-77-cid20-713-snapshot.B20-77.CID20-713.A510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-713-snapshot/2020-08-21-oc395-run-b20-77-cid20-713-snapshot.B20-77.CID20-713.A510_N702.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid20-713',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot.B21-90.CID21-1183.C534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot/2021jul08-oc395-run0-b21-90-cid21-1183-snapshot.B21-90.CID21-1183.C534_N702.coverage.summary.htqc.txt',
                        'sample_id': '9b-cid21-1183',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-717-snapshot/2020-08-21-oc395-run-b20-77-cid20-717-snapshot.B20-77.CID20-717.A511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-08-21-oc395-run-b20-77-cid20-717-snapshot/2020-08-21-oc395-run-b20-77-cid20-717-snapshot.B20-77.CID20-717.A511_N703.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid20-717',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot.B21-90.CID21-1185.C537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot/2021jul08-oc395-run0-b21-90-cid21-1185-snapshot.B21-90.CID21-1185.C537_N705.coverage.summary.htqc.txt',
                        'sample_id': '10b-cid21-1185',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-603-snapshot/2020-07-31-oc395-run-b20-70-cid20-603-snapshot.B20-70.CID20-603.A525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2020-07-31-oc395-run-b20-70-cid20-603-snapshot/2020-07-31-oc395-run-b20-70-cid20-603-snapshot.B20-70.CID20-603.A525_N701.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid20-603',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot.B21-90.CID21-1190.A532_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot/2021jul08-oc395-run0-b21-90-cid21-1190-snapshot.B21-90.CID21-1190.A532_N708.coverage.summary.htqc.txt',
                        'sample_id': '11b-cid21-1190',
                        'sample_type': ''}
                ]
            }
        }
    }