from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_oc120_vs_oc120plus_sample_reproducibility_oc120plus_bed_file():
    """
    Get the oc120 vs oc120plus concordance configuration for variant level data
    This was from data using the new oc120plus bed file on the oc120plus samples
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/reproducibility_clinical_oc120_vs_oc120plus_oc120plus_bed_file",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                #"snv": f"clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_1.0_depth_50_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                "snv": None,
                #"indel": f"clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_1.0_depth_50_consequential_impacts_only_indels_size_1_1_both_inter_and_intra.txt"
                "indel": None,
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    #"file": f"clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_snv}_depth_{depth_threshold_snv}_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                    "file": None,
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    #"file": f"clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_indel}_depth_{depth_threshold_indel}_consequential_impacts_only_indels_size_{indel_thresholds[0][0]}_{indel_thresholds[0][1]}_both_inter_and_intra.txt",
                    "file": None,
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
    },
            "allele_count_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            #"vaf_threshold": 10.0,
            #"depth_threshold": 100,
            # Go over and look at the samples below, in the concordance_files
            # the oc120 is the first VCF in each entry
            # oc120plus is the second VCF  in each entry
            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid20-435',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '1b-cid20-1081',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample2': [{'sample_id': '2a-cid20-440',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '2b-cid20-1082',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample3': [{'sample_id': '3a-cid20-594',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '3b-cid20-1083',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample4': [{'sample_id': '4a-cid19-4257',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '4b-cid20-1079',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample5': [{'sample_id': '5a-cid20-576',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '5b-cid20-1100',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample6': [{'sample_id': '6a-cid20-334',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '6b-cid20-1102',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample7': [{'sample_id': '7a-cid20-640',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '7b-cid20-1103',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample8': [{'sample_id': '8a-cid20-620',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '8b-cid20-1105',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample9': [{'sample_id': '9a-cid20-692',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '9b-cid20-1106',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample10': [{'sample_id': '10a-cid20-829',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '10b-cid20-1107',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample11': [{'sample_id': '11a-cid20-841',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '11b-cid20-1110',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample12': [{'sample_id': '12a-cid19-4524',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '12b-cid20-1203',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample13': [{'sample_id': '13a-cid20-541',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '13b-cid20-1204',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample14': [{'sample_id': '14a-cid20-815',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '14b-cid20-1213',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample15': [{'sample_id': '15a-cid20-754',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '15b-cid20-1214',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample16': [{'sample_id': '16a-cid20-994',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '16b-cid20-1215',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample17': [{'sample_id': '17a-cid20-624',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '17b-cid20-1216',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample18': [{'sample_id': '18a-cid20-1022',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '18b-cid20-1217',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample19': [{'sample_id': '19a-cid20-395',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '19b-cid20-1206',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample20': [{'sample_id': '20a-cid20-469',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '20b-cid20-1205',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample21': [{'sample_id': '21a-cid20-481',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '21b-cid20-1207',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample22': [{'sample_id': '22a-cid20-524',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '22b-cid20-1208',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample23': [{'sample_id': '23a-cid20-653',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '23b-cid20-1210',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample24': [{'sample_id': '24a-cid20-657',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '24b-cid20-1211',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample25': [{'sample_id': '25a-cid20-705',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '25b-cid20-1212',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample26': [{'sample_id': '26a-cid20-1024',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '26b-cid20-1163',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample27': [{'sample_id': '27a-cid20-936',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '27b-cid20-1165',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample28': [{'sample_id': '28a-cid20-946',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '28b-cid20-1168',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample29': [{'sample_id': '29a-cid20-1015',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '29b-cid20-1172',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample30': [{'sample_id': '30a-cid20-1000',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '30b-cid20-1174',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample31': [{'sample_id': '31a-cid20-972',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '31b-cid20-1241',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample32': [{'sample_id': '32a-cid20-974',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '32b-cid20-1240',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}]
            }
        }
    }


def get_oc120_vs_oc120plus_sample_reproducibility_oc120_bed_file():
    """
    Get the oc120 vs oc120plus concordance configuration for variant level data
    This was from data using the original oc120 bed file on the oc120plus samples
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/reproducibility_clinical_oc120_vs_oc120plus_oc120_bed_file",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": "lab_validation_120/performance_characteristics/reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_120plus_all_values.txt",
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [f"scratch/clinical_sample_reproducibility/oc120_vs_oc120plus/oc120_bed_file/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_snv}_depth_{depth_threshold_snv}_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                        f"scratch/clinical_sample_reproducibility/oc120_vs_oc120plus/oc120_bed_file/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_snv}_depth_{depth_threshold_snv}_all_impacts_snvs_both_inter_and_intra.txt"],
                # files were created by combinine length 1_1, 2_4, and 5_or_more:
                # cat clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_8.0_depth_100_consequential_impacts_only_*indels*both* \
                # >clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt
                # all impacts
                # cat clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_8.0_depth_100_all_impacts_*indels*both* \
                # >clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt
                # and then removing removing header rows
                "indel": [f"scratch/clinical_sample_reproducibility/oc120_vs_oc120plus/oc120_bed_file/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_indel}_depth_{depth_threshold_snv}_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt",
                          f"scratch/clinical_sample_reproducibility/oc120_vs_oc120plus/oc120_bed_file/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_indel}_depth_{depth_threshold_snv}_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt"]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file": [f"scratch/clinical_sample_reproducibility/oc120_vs_oc120plus/oc120_bed_file/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_snv}_depth_{depth_threshold_snv}_consequential_impacts_only_snvs_both_inter_and_intra.txt",
                             f"scratch/clinical_sample_reproducibility/oc120_vs_oc120plus/oc120_bed_file/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_snv}_depth_{depth_threshold_indel}_all_impacts_snvs_both_inter_and_intra.txt"],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [f"scratch/clinical_sample_reproducibility/oc120_vs_oc120plus/oc120_bed_file/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_indel}_depth_{depth_threshold_indel}_consequential_impacts_only_indels_size_{indel_thresholds[0][0]}_{indel_thresholds[0][1]}_both_inter_and_intra.txt",
                             f"scratch/clinical_sample_reproducibility/oc120_vs_oc120plus/oc120_bed_file/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_{vaf_threshold_indel}_depth_{depth_threshold_indel}_all_impacts_indels_size_{indel_thresholds[0][0]}_{indel_thresholds[0][1]}_both_inter_and_intra.txt"],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
    },
            "allele_count_regression_plot": {
                "x_axis_label": "REP1",
                "y_axis_label": "REP2",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            #"vaf_threshold": 10.0,
            #"depth_threshold": 100,
            # Go over and look at the samples below, in the concordance_files
            # the oc120 is the first VCF in each entry
            # oc120plus is the second VCF  in each entry
            "concordance_files": {
                "inter_Sample1": [
                    {
                        # CID20-435
                        "sample_id": "1a-cid20-435",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.merged_concatenated.vcf",
                    },
                    {
                        # CID20-1081
                        "sample_id": "1b-cid20-1081",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                "inter_Sample2": [
                    {
                        # CID20-440
                        "sample_id": "2a-cid20-440",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1082
                        "sample_id": "2b-cid20-1082",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                "inter_Sample3": [
                    {
                        # CID20-594
                        "sample_id": "3a-cid20-594",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1083
                        "sample_id": "3b-cid20-1083",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }

                ],
                "inter_Sample4": [
                    {
                        # CID19-4257
                        "sample_id": "4a-cid19-4257",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1079
                        "sample_id": "4b-cid20-1079",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                # MCC was too low in original, keeping for data provenance
                #"inter_Sample5": [
                #    {
                #        # CID19-516
                #        "sample_id": "5a-cid19-516",
                #        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-516-snapshot/2020jul29-oc120-run5-b20-69-cid20-516-snapshot.B20-69.CID20-516.C506_N706.merged_concatenated.vcf",
                #        "variant_type": "snvs"
                #    },
                #    {
                #        # CID20-1098
                #        "sample_id": "5b-cid20-1098",
                #        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1098-snapshot/2020out29-oc120-run6-b20-117-cid20-1098-snapshot.B20-117.CID20-1098.C501_N701.merged_concatenated.vcf",
                #        "variant_type": "snvs"
                #    },
                #    # must have this last element
                #    {
                #        "experiment_type": "inter-run"
                #    }
                #],
                "inter_Sample6": [
                    {
                        # CID19-576
                        "sample_id": "6a-cid19-576",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1100
                        "sample_id": "6b-cid20-1100",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                "inter_Sample7": [
                    {
                        # CID19-334
                        "sample_id": "7a-cid19-334",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1102
                        "sample_id": "7b-cid20-1102",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                "inter_Sample8": [
                    {
                        # CID19-640
                        "sample_id": "8a-cid19-640",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1103
                        "sample_id": "8b-cid20-1103",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                # MCC was too low in original, keeping for data provenance
                # "inter_Sample9": [
                #     {
                #         # CID19-632
                #         "sample_id": "9a-cid19-632",
                #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-632-snapshot/2020-09-03-oc120-run-b20-86-cid20-632-snapshot.B20-86.CID20-632.B510_N702.merged_concatenated.vcf",
                #         "variant_type": "snvs"
                #     },
                #     {
                #         # CID20-1104
                #         "sample_id": "9b-cid20-1104",
                #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1104-snapshot/2020out29-oc120-run6-b20-117-cid20-1104-snapshot.B20-117.CID20-1104.C507_N707.merged_concatenated.vcf",
                #         "variant_type": "snvs"
                #     },
                #     # must have this last element
                #     {
                #         "experiment_type": "inter-run"
                #     }
                # ],
                "inter_Sample10": [
                    {
                        # CID19-620
                        "sample_id": "10a-cid19-620",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1105
                        "sample_id": "10b-cid20-1105",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                "inter_Sample11": [
                    {
                        # CID19-692
                        "sample_id": "11a-cid19-692",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1106
                        "sample_id": "11b-cid20-1106",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                "inter_Sample12": [
                    {
                        # CID19-829
                        "sample_id": "12a-cid19-829",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1107
                        "sample_id": "12b-cid20-1107",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                # MCC was too low in original, keeping for data provenance
                # "inter_Sample13": [
                #     {
                #         # CID19-819
                #         "sample_id": "13a-cid19-819",
                #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-819-snapshot/2020set15-oc120-run5-b20-92-cid20-819-snapshot.B20-92.CID20-819.C504_N704.merged_concatenated.vcf",
                #         "variant_type": "snvs"
                #     },
                #     {
                #         # CID20-1108
                #         "sample_id": "13b-cid20-1108",
                #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1108-snapshot/2020out29-oc120-run6-b20-117-cid20-1108-snapshot.B20-117.CID20-1108.C511_N703.merged_concatenated.vcf",
                #         "variant_type": "snvs"
                #     },
                #     # must have this last element
                #     {
                #         "experiment_type": "inter-run"
                #     }
                # ],
                # "inter_Sample14": [
                #     {
                #         # CID19-893
                #         "sample_id": "14a-cid19-893",
                #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-893-snapshot/2020set15-oc120-run5-b20-92-cid20-893-snapshot.B20-92.CID20-893.C507_N707.merged_concatenated.vcf",
                #         "variant_type": "snvs"
                #     },
                #     {
                #         # CID20-1109
                #         "sample_id": "14b-cid20-1109",
                #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1109-snapshot/2020out29-oc120-run6-b20-117-cid20-1109-snapshot.B20-117.CID20-1109.C512_N707.merged_concatenated.vcf",
                #         "variant_type": "snvs"
                #     },
                #     # must have this last element
                #     {
                #         "experiment_type": "inter-run"
                #     }
                # ],
                "inter_Sample15": [
                    {
                        # CID19-841
                        "sample_id": "15a-cid19-841",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID20-1110
                        "sample_id": "15b-cid20-1110",
                        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    # must have this last element
                    {
                        "experiment_type": "inter-run"
                    }
                ],
                # MCC was too low in original, keeping for data provenance
                # "inter_Sample16": [
                #     {
                #         # CID19-843
                #         "sample_id": "16a-cid19-843",
                #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-94-cid20-843-snapshot/2020set15-oc120-run5-b20-94-cid20-843-snapshot.B20-94.CID20-843.C509_N701.merged_concatenated.vcf",
                #         "variant_type": "snvs"
                #     },
                #     {
                #         # CID20-1111
                #         "sample_id": "16b-cid20-1111",
                #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1111-snapshot/2020out29-oc120-run6-b20-117-cid20-1111-snapshot.B20-117.CID20-1111.C514_N706.merged_concatenated.vcf",
                #         "variant_type": "snvs"
                #     },
                #     # must have this last element
                #     {
                #         "experiment_type": "inter-run"
                #     }
                # ],

                #### NEW FILES Created by tools/create_oc120_vs_oc120plus_reproducibility_config_ds.py
                'inter_Sample17': [{'sample_id': '17a-cid19-4524',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.merged_concatenated.vcf'},
                                   {'sample_id': '17b-cid20-1203',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample18': [{'sample_id': '18a-cid20-541',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.merged_concatenated.vcf'},
                                   {'sample_id': '18b-cid20-1204',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample19': [{'sample_id': '19a-cid20-815',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.merged_concatenated.vcf'},
                                   {'sample_id': '19b-cid20-1213',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample20': [{'sample_id': '20a-cid20-754',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.merged_concatenated.vcf'},
                                   {'sample_id': '20b-cid20-1214',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample21': [{'sample_id': '21a-cid20-994',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.merged_concatenated.vcf'},
                                   {'sample_id': '21b-cid20-1215',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample22': [{'sample_id': '22a-cid20-624',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.merged_concatenated.vcf'},
                                   {'sample_id': '22b-cid20-1216',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample23': [{'sample_id': '23a-cid20-1022',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.merged_concatenated.vcf'},
                                   {'sample_id': '23b-cid20-1217',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample24': [{'sample_id': '24a-cid20-395',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.merged_concatenated.vcf'},
                                   {'sample_id': '24b-cid20-1206',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample25': [{'sample_id': '25a-cid20-469',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.merged_concatenated.vcf'},
                                   {'sample_id': '25b-cid20-1205',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample26': [{'sample_id': '26a-cid20-481',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.merged_concatenated.vcf'},
                                   {'sample_id': '26b-cid20-1207',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample27': [{'sample_id': '27a-cid20-524',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.merged_concatenated.vcf'},
                                   {'sample_id': '27b-cid20-1208',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample28': [{'sample_id': '28a-cid20-653',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.merged_concatenated.vcf'},
                                   {'sample_id': '28b-cid20-1210',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample29': [{'sample_id': '29a-cid20-657',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.merged_concatenated.vcf'},
                                   {'sample_id': '29b-cid20-1211',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample30': [{'sample_id': '30a-cid20-705',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.merged_concatenated.vcf'},
                                   {'sample_id': '30b-cid20-1212',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample31': [{'sample_id': '31a-cid20-1024',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.merged_concatenated.vcf'},
                                   {'sample_id': '31b-cid20-1163',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample32': [{'sample_id': '32a-cid20-936',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.merged_concatenated.vcf'},
                                   {'sample_id': '32b-cid20-1165',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample33': [{'sample_id': '33a-cid20-946',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.merged_concatenated.vcf'},
                                   {'sample_id': '33b-cid20-1168',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample34': [{'sample_id': '34a-cid20-1015',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.merged_concatenated.vcf'},
                                   {'sample_id': '34b-cid20-1172',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample35': [{'sample_id': '35a-cid20-1000',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.merged_concatenated.vcf'},
                                   {'sample_id': '35b-cid20-1174',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample36': [{'sample_id': '36a-cid20-972',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.merged_concatenated.vcf'},
                                   {'sample_id': '36b-cid20-1241',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample37': [{'sample_id': '37a-cid20-974',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.merged_concatenated.vcf'},
                                   {'sample_id': '37b-cid20-1240',
                                    'variant_type': 'snvs',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.merged_concatenated.vcf'},
                                   {'experiment_type': 'inter-run'}]
            }
        }
    }


def get_oc120_vs_oc120plus_sample_reproducibility_htqc_plots_oc120plus_bed_file():
    """
    Get the oc120 vs oc120plus concordance configuration for HTQc plotting
    This was from data using the  oc120plus bed file on the oc120plus samples
    """

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "oc120_vs_oc120plus_samples_reproducibility_qc_plots": {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": "images/reproducibility_clinical_oc120_vs_oc120plus_oc120plus_bed_file",
            "input_files": {
                "group_id": "oc120_vs_oc120plus_oc120plus_bed_reproducibility_qc_plots",  # eventually used for a file name
                "chart_title": "Reproducibility GS120 vs. GS120plus (GS120plus Bed file on GS120plus Sample)",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid20-435',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid20-1081',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid20-440',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid20-1082',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid20-594',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid20-1083',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid19-4257',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid20-1079',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid20-576',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid20-1100',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid20-334',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid20-1102',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid20-640',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid20-1103',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid20-620',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid20-1105',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid20-692',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.coverage.summary.htqc.txt',
                        'sample_id': '9b-cid20-1106',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid20-829',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.coverage.summary.htqc.txt',
                        'sample_id': '10b-cid20-1107',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid20-841',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.coverage.summary.htqc.txt',
                        'sample_id': '11b-cid20-1110',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.coverage.summary.htqc.txt',
                        'sample_id': '12a-cid19-4524',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.coverage.summary.htqc.txt',
                        'sample_id': '12b-cid20-1203',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.coverage.summary.htqc.txt',
                        'sample_id': '13a-cid20-541',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.coverage.summary.htqc.txt',
                        'sample_id': '13b-cid20-1204',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.coverage.summary.htqc.txt',
                        'sample_id': '14a-cid20-815',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.coverage.summary.htqc.txt',
                        'sample_id': '14b-cid20-1213',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.coverage.summary.htqc.txt',
                        'sample_id': '15a-cid20-754',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '15b-cid20-1214',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.coverage.summary.htqc.txt',
                        'sample_id': '16a-cid20-994',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.coverage.summary.htqc.txt',
                        'sample_id': '16b-cid20-1215',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.coverage.summary.htqc.txt',
                        'sample_id': '17a-cid20-624',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.coverage.summary.htqc.txt',
                        'sample_id': '17b-cid20-1216',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.coverage.summary.htqc.txt',
                        'sample_id': '18a-cid20-1022',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.coverage.summary.htqc.txt',
                        'sample_id': '18b-cid20-1217',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.coverage.summary.htqc.txt',
                        'sample_id': '19a-cid20-395',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.coverage.summary.htqc.txt',
                        'sample_id': '19b-cid20-1206',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.coverage.summary.htqc.txt',
                        'sample_id': '20a-cid20-469',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.coverage.summary.htqc.txt',
                        'sample_id': '20b-cid20-1205',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.coverage.summary.htqc.txt',
                        'sample_id': '21a-cid20-481',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.coverage.summary.htqc.txt',
                        'sample_id': '21b-cid20-1207',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.coverage.summary.htqc.txt',
                        'sample_id': '22a-cid20-524',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.coverage.summary.htqc.txt',
                        'sample_id': '22b-cid20-1208',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.coverage.summary.htqc.txt',
                        'sample_id': '23a-cid20-653',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.coverage.summary.htqc.txt',
                        'sample_id': '23b-cid20-1210',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.coverage.summary.htqc.txt',
                        'sample_id': '24a-cid20-657',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.coverage.summary.htqc.txt',
                        'sample_id': '24b-cid20-1211',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.coverage.summary.htqc.txt',
                        'sample_id': '25a-cid20-705',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.coverage.summary.htqc.txt',
                        'sample_id': '25b-cid20-1212',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '26a-cid20-1024',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.coverage.summary.htqc.txt',
                        'sample_id': '26b-cid20-1163',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.coverage.summary.htqc.txt',
                        'sample_id': '27a-cid20-936',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.coverage.summary.htqc.txt',
                        'sample_id': '27b-cid20-1165',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.coverage.summary.htqc.txt',
                        'sample_id': '28a-cid20-946',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.coverage.summary.htqc.txt',
                        'sample_id': '28b-cid20-1168',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '29a-cid20-1015',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.coverage.summary.htqc.txt',
                        'sample_id': '29b-cid20-1172',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.coverage.summary.htqc.txt',
                        'sample_id': '30a-cid20-1000',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.coverage.summary.htqc.txt',
                        'sample_id': '30b-cid20-1174',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.coverage.summary.htqc.txt',
                        'sample_id': '31a-cid20-972',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.coverage.summary.htqc.txt',
                        'sample_id': '31b-cid20-1241',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file//2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.coverage.summary.htqc.txt',
                        'sample_id': '32a-cid20-974',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.coverage.summary.htqc.txt',
                        'sample_id': '32b-cid20-1240',
                        'sample_type': ''}
                ]
            }
        }
    }


def get_oc120_vs_oc120plus_sample_reproducibility_htqc_plots_oc120_bed_file():
    """
    Get the oc120 vs oc120plus concordance configuration for HTQc plotting
    This was from data using the original oc120 bed file on the oc120plus samples
    """

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "oc120_vs_oc120plus_samples_reproducibility_qc_plots": {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": "images/reproducibility_clinical_oc120_vs_oc120plus_oc120_bed_file",
            "input_files": {
                "group_id": "oc120_vs_oc120plus_oc120_bed_reproducibility_qc_plots",  # eventually used for a file name
                "chart_title": "Reproducibility GS120 vs. GS120plus (GS120 Bed file on GS120plus Sample)",
                "data": [
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-435
                        "sample_id": "1a-cid20-435",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1081
                        "sample_id": "1b-cid20-1081",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-440
                        "sample_id": "2a-cid20-440",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1082
                        "sample_id": "2b-cid20-1082",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-594
                        "sample_id": "3a-cid20-594",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1083
                        "sample_id": "3b-cid20-1083",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-4257
                        "sample_id": "4a-cid20-4257",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1079
                        "sample_id": "4b-cid20-1079",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    # MCC was too low in original, keeping for data provenance
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-516
                    #     "sample_id": "5a-cid20-516",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-516-snapshot/2020jul29-oc120-run5-b20-69-cid20-516-snapshot.B20-69.CID20-516.C506_N706.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-516-snapshot/2020jul29-oc120-run5-b20-69-cid20-516-snapshot.B20-69.CID20-516.C506_N706.coverage.htqc.txt",
                    #     "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    # },
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-1098
                    #     "sample_id": "5b-cid20-1098",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1098-snapshot/2020out29-oc120-run6-b20-117-cid20-1098-snapshot.B20-117.CID20-1098.C501_N701.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1098-snapshot/2020out29-oc120-run6-b20-117-cid20-1098-snapshot.B20-117.CID20-1098.C501_N701.coverage.htqc.txt",
                    #     "sample_type": "",
                    # },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-576
                        "sample_id": "6a-cid20-576",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1100
                        "sample_id": "6b-cid20-1100",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-334
                        "sample_id": "7a-cid20-334",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1102
                        "sample_id": "7b-cid20-1102",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-640
                        "sample_id": "8a-cid20-640",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1103
                        "sample_id": "8b-cid20-1103",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    # MCC was too low in original, keeping for data provenance
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-632
                    #     "sample_id": "9a-cid20-532",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-632-snapshot/2020-09-03-oc120-run-b20-86-cid20-632-snapshot.B20-86.CID20-632.B510_N702.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-632-snapshot/2020-09-03-oc120-run-b20-86-cid20-632-snapshot.B20-86.CID20-632.B510_N702.coverage.htqc.txt",
                    #     "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    # },
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-1104
                    #     "sample_id": "9b-cid20-1104",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1104-snapshot/2020out29-oc120-run6-b20-117-cid20-1104-snapshot.B20-117.CID20-1104.C507_N707.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1104-snapshot/2020out29-oc120-run6-b20-117-cid20-1104-snapshot.B20-117.CID20-1104.C507_N707.coverage.htqc.txt",
                    #     "sample_type": "",
                    # },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-620
                        "sample_id": "10a-cid20-620",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1105
                        "sample_id": "10b-cid20-1105",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-692
                        "sample_id": "11a-cid20-692",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1106
                        "sample_id": "11b-cid20-1106",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-829
                        "sample_id": "12a-cid20-829",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1107
                        "sample_id": "12b-cid20-1107",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    # MCC was too low in original, keeping for data provenance
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-819
                    #     "sample_id": "13a-cid20-819",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-819-snapshot/2020set15-oc120-run5-b20-92-cid20-819-snapshot.B20-92.CID20-819.C504_N704.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-819-snapshot/2020set15-oc120-run5-b20-92-cid20-819-snapshot.B20-92.CID20-819.C504_N704.coverage.htqc.txt",
                    #     "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    # },
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-1108
                    #     "sample_id": "13b-cid20-1108",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1108-snapshot/2020out29-oc120-run6-b20-117-cid20-1108-snapshot.B20-117.CID20-1108.C511_N703.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1108-snapshot/2020out29-oc120-run6-b20-117-cid20-1108-snapshot.B20-117.CID20-1108.C511_N703.coverage.htqc.txt",
                    #     "sample_type": "",
                    # },
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-893
                    #     "sample_id": "14a-cid20-893",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-893-snapshot/2020set15-oc120-run5-b20-92-cid20-893-snapshot.B20-92.CID20-893.C507_N707.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-893-snapshot/2020set15-oc120-run5-b20-92-cid20-893-snapshot.B20-92.CID20-893.C507_N707.coverage.htqc.txt",
                    #     "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    # },
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-1109
                    #     "sample_id": "14b-cid20-1109",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1109-snapshot/2020out29-oc120-run6-b20-117-cid20-1109-snapshot.B20-117.CID20-1109.C512_N707.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1109-snapshot/2020out29-oc120-run6-b20-117-cid20-1109-snapshot.B20-117.CID20-1109.C512_N707.coverage.htqc.txt",
                    #     "sample_type": "",
                    # },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-841
                        "sample_id": "15a-cid20-841",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.coverage.htqc.txt",
                        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1110
                        "sample_id": "15b-cid20-1110",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.coverage.htqc.txt",
                        "sample_type": "",
                    },
                    # MCC was too low in original, keeping for data provenance
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-843
                    #     "sample_id": "16a-cid20-843",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-94-cid20-843-snapshot/2020set15-oc120-run5-b20-94-cid20-843-snapshot.B20-94.CID20-843.C509_N701.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-94-cid20-843-snapshot/2020set15-oc120-run5-b20-94-cid20-843-snapshot.B20-94.CID20-843.C509_N701.coverage.htqc.txt",
                    #     "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
                    # },
                    # {
                    #     # http://lims.demo.bostonlighthouse.us/index.php/CID20-1111
                    #     "sample_id": "16b-cid20--1111",
                    #     "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1111-snapshot/2020out29-oc120-run6-b20-117-cid20-1111-snapshot.B20-117.CID20-1111.C514_N706.coverage.summary.htqc.txt",
                    #     "qc_htqc_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out29-oc120-run6-b20-117-cid20-1111-snapshot/2020out29-oc120-run6-b20-117-cid20-1111-snapshot.B20-117.CID20-1111.C514_N706.coverage.htqc.txt",
                    #     "sample_type": "",
                    # },

                    #### NEW FILES Created by tools/create_oc120_vs_oc120plus_reproducibility_config_ds.py
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.coverage.summary.htqc.txt',
                        'sample_id': '17a-cid19-4524',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.coverage.summary.htqc.txt',
                        'sample_id': '17b-cid20-1203',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.coverage.summary.htqc.txt',
                        'sample_id': '18a-cid20-541',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.coverage.summary.htqc.txt',
                        'sample_id': '18b-cid20-1204',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.coverage.summary.htqc.txt',
                        'sample_id': '19a-cid20-815',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.coverage.summary.htqc.txt',
                        'sample_id': '19b-cid20-1213',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.coverage.summary.htqc.txt',
                        'sample_id': '20a-cid20-754',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '20b-cid20-1214',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.coverage.summary.htqc.txt',
                        'sample_id': '21a-cid20-994',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.coverage.summary.htqc.txt',
                        'sample_id': '21b-cid20-1215',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.coverage.summary.htqc.txt',
                        'sample_id': '22a-cid20-624',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.coverage.summary.htqc.txt',
                        'sample_id': '22b-cid20-1216',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.coverage.summary.htqc.txt',
                        'sample_id': '23a-cid20-1022',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.coverage.summary.htqc.txt',
                        'sample_id': '23b-cid20-1217',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.coverage.summary.htqc.txt',
                        'sample_id': '24a-cid20-395',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.coverage.summary.htqc.txt',
                        'sample_id': '24b-cid20-1206',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.coverage.summary.htqc.txt',
                        'sample_id': '25a-cid20-469',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.coverage.summary.htqc.txt',
                        'sample_id': '25b-cid20-1205',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.coverage.summary.htqc.txt',
                        'sample_id': '26a-cid20-481',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.coverage.summary.htqc.txt',
                        'sample_id': '26b-cid20-1207',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.coverage.summary.htqc.txt',
                        'sample_id': '27a-cid20-524',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.coverage.summary.htqc.txt',
                        'sample_id': '27b-cid20-1208',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.coverage.summary.htqc.txt',
                        'sample_id': '28a-cid20-653',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.coverage.summary.htqc.txt',
                        'sample_id': '28b-cid20-1210',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.coverage.summary.htqc.txt',
                        'sample_id': '29a-cid20-657',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.coverage.summary.htqc.txt',
                        'sample_id': '29b-cid20-1211',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.coverage.summary.htqc.txt',
                        'sample_id': '30a-cid20-705',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.coverage.summary.htqc.txt',
                        'sample_id': '30b-cid20-1212',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '31a-cid20-1024',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.coverage.summary.htqc.txt',
                        'sample_id': '31b-cid20-1163',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.coverage.summary.htqc.txt',
                        'sample_id': '32a-cid20-936',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.coverage.summary.htqc.txt',
                        'sample_id': '32b-cid20-1165',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.coverage.summary.htqc.txt',
                        'sample_id': '33a-cid20-946',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.coverage.summary.htqc.txt',
                        'sample_id': '33b-cid20-1168',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '34a-cid20-1015',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.coverage.summary.htqc.txt',
                        'sample_id': '34b-cid20-1172',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.coverage.summary.htqc.txt',
                        'sample_id': '35a-cid20-1000',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.coverage.summary.htqc.txt',
                        'sample_id': '35b-cid20-1174',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.coverage.summary.htqc.txt',
                        'sample_id': '36a-cid20-972',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.coverage.summary.htqc.txt',
                        'sample_id': '36b-cid20-1241',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.coverage.summary.htqc.txt',
                        'sample_id': '37a-cid20-974',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.coverage.summary.htqc.txt',
                        'sample_id': '37b-cid20-1240',
                        'sample_type': ''}
                ]
            }
        }
    }


def get_oc120plus_equivalency_samples_for_htqc_plots_using_oc120plus_bed_file():
    """
    Get the oc120plus samples from the equivalency study for HTQc plotting
    This was from data using the oc120plus bed file on the oc120plus samples
    These were the 32 samples used in the equivalency study
    """
    key_2_use = 'oc120plus_equivalency_qc_plots'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": "oc120plus_bed_qc_plots",  # eventually used for a file name
                "chart_title": "GS120plus (GS120plus Bed file)",
                "data": [
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.coverage.summary.htqc.txt',
                         'sample_id': '1a-cid20-1081',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.coverage.summary.htqc.txt',
                         'sample_id': '2a-cid20-1082',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.coverage.summary.htqc.txt',
                         'sample_id': '3a-cid20-1083',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.coverage.summary.htqc.txt',
                         'sample_id': '4a-cid20-1079',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.coverage.summary.htqc.txt',
                         'sample_id': '5a-cid20-1100',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.coverage.summary.htqc.txt',
                         'sample_id': '6a-cid20-1102',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.coverage.summary.htqc.txt',
                         'sample_id': '7a-cid20-1103',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.coverage.summary.htqc.txt',
                         'sample_id': '8a-cid20-1105',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.coverage.summary.htqc.txt',
                         'sample_id': '9a-cid20-1106',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.coverage.summary.htqc.txt',
                         'sample_id': '10a-cid20-1107',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.coverage.summary.htqc.txt',
                         'sample_id': '11a-cid20-1110',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.coverage.summary.htqc.txt',
                         'sample_id': '12a-cid20-1203',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.coverage.summary.htqc.txt',
                         'sample_id': '13a-cid20-1204',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.coverage.summary.htqc.txt',
                         'sample_id': '14a-cid20-1213',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.coverage.summary.htqc.txt',
                         'sample_id': '15a-cid20-1214',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.coverage.summary.htqc.txt',
                         'sample_id': '16a-cid20-1215',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.coverage.summary.htqc.txt',
                         'sample_id': '17a-cid20-1216',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.coverage.summary.htqc.txt',
                         'sample_id': '18a-cid20-1217',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.coverage.summary.htqc.txt',
                         'sample_id': '19a-cid20-1206',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.coverage.summary.htqc.txt',
                         'sample_id': '20a-cid20-1205',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.coverage.summary.htqc.txt',
                         'sample_id': '21a-cid20-1207',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.coverage.summary.htqc.txt',
                         'sample_id': '22a-cid20-1208',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.coverage.summary.htqc.txt',
                         'sample_id': '23a-cid20-1210',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.coverage.summary.htqc.txt',
                         'sample_id': '24a-cid20-1211',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.coverage.summary.htqc.txt',
                         'sample_id': '25a-cid20-1212',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.coverage.summary.htqc.txt',
                         'sample_id': '26a-cid20-1163',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.coverage.summary.htqc.txt',
                         'sample_id': '27a-cid20-1165',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.coverage.summary.htqc.txt',
                         'sample_id': '28a-cid20-1168',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.coverage.summary.htqc.txt',
                         'sample_id': '29a-cid20-1172',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.coverage.summary.htqc.txt',
                         'sample_id': '30a-cid20-1174',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.coverage.summary.htqc.txt',
                         'sample_id': '31a-cid20-1241',
                         'sample_type': ''},
                     {
                         'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.coverage.summary.htqc.txt',
                         'sample_id': '32a-cid20-1240',
                         'sample_type': ''}

                ]
            }
        }
    }


def get_oc120_equivalency_samples_for_htqc_plots_using_oc120_bed_file():
    """
    Get the  oc120 samples from the equivalency study for   HTQc plotting
    This was from data using the oc120 bed file on the oc120 samples
    These were the 32 samples used in the equivalency study
    """

    key_2_use = 'oc120_equivalency_qc_plots'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": "oc120_bed_qc_plots",  # eventually used for a file name
                "chart_title": "GS120 (GS120 Bed file)",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid20-435',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid20-440',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid20-594',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid19-4257',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid20-576',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid20-334',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid20-640',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid20-620',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid20-692',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid20-829',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid20-841',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.coverage.summary.htqc.txt',
                        'sample_id': '12a-cid19-4524',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.coverage.summary.htqc.txt',
                        'sample_id': '13a-cid20-541',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.coverage.summary.htqc.txt',
                        'sample_id': '14a-cid20-815',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.coverage.summary.htqc.txt',
                        'sample_id': '15a-cid20-754',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.coverage.summary.htqc.txt',
                        'sample_id': '16a-cid20-994',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.coverage.summary.htqc.txt',
                        'sample_id': '17a-cid20-624',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.coverage.summary.htqc.txt',
                        'sample_id': '18a-cid20-1022',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.coverage.summary.htqc.txt',
                        'sample_id': '19a-cid20-395',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.coverage.summary.htqc.txt',
                        'sample_id': '20a-cid20-469',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.coverage.summary.htqc.txt',
                        'sample_id': '21a-cid20-481',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.coverage.summary.htqc.txt',
                        'sample_id': '22a-cid20-524',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.coverage.summary.htqc.txt',
                        'sample_id': '23a-cid20-653',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.coverage.summary.htqc.txt',
                        'sample_id': '24a-cid20-657',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.coverage.summary.htqc.txt',
                        'sample_id': '25a-cid20-705',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '26a-cid20-1024',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.coverage.summary.htqc.txt',
                        'sample_id': '27a-cid20-936',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.coverage.summary.htqc.txt',
                        'sample_id': '28a-cid20-946',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '29a-cid20-1015',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.coverage.summary.htqc.txt',
                        'sample_id': '30a-cid20-1000',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.coverage.summary.htqc.txt',
                        'sample_id': '31a-cid20-972',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.coverage.summary.htqc.txt',
                        'sample_id': '32a-cid20-974',
                        'sample_type': ''}
                ]
            }
        }
    }