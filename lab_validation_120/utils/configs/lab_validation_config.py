"""Simple configuration module for the validation """
import json
import pandas as pd
import re
import os


def get_oncokb_gene_list():
    """
    Return a dictionary of the oncoKB gene list, name : file locations
    """
    return {
        'oncokb_L1': 'lab_validation_120/external_data_sources/oncokb/oncokb_level_1_v10_20200716_parsed.txt',
        'oncokb_L2': 'lab_validation_120/external_data_sources/oncokb/oncokb_level_2_v10_20200716_parsed.txt',
        'oncokb_R1': 'lab_validation_120/external_data_sources/oncokb/oncokb_level_R1_v10_20200716_parsed.txt',
        'oncokb_R2': 'lab_validation_120/external_data_sources/oncokb/oncokb_level_R2_v10_20200716_parsed.txt',
    }


def get_oc395_long_and_rodrigo_hotspot_bed():
    """The combined hotspot file from Long and Rodrigo"""
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/snapshot_hotspots_combined_long_and_actionable_resistence_biomarkers_v14.bed'


def get_oc395_targets_bed_file():
    """just return the OC395 BED file"""
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/oc395/TargetsRefSeqExonsMap_OC395_updated_gene_names_updated_MET_exon14.v1.3.bed'


def get_oc120_targets_bed_file():
    """just return the OC120 BED file"""
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/variantplex_03012017_coverage_OC120.bed'


def get_oc120archer_targets_bed_file():
    """just return the OC120 BED file"""
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/VariantPlex_OC_v2_cSA5075_primers_targets.bed'


def get_oc120_targets_bed_gc_content_file():
    """just return the OC395 BED file that has the gc content data residing in it location"""
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/variantplex_03012017_coverage_OC120.gc.bed'


def get_oc120plus_targets_bed_file():
    """
    just return the OC120Plus (GS v2.0) BED file
    This file was from Elina Wegner (ArcherDx)
    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/' \
           'VariantPlex_OC_v3_16897_primers_targets.sorted.bed'

def get_oc120plus_snps_targets_bed_file():
    """
    just return the OC120Plus (GS v2.0) PLUS SNPS (but not the SNPs, just the three new genes: CDK12, FANCL, PPP2R2A)
    BED file
    This file was from get_oc120plus_targets_bed_file, and updated to include only the CDK12, FANCL, PPP2R2A
    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/oc180_snps/VariantPlex_OC_v3_16897_primers_targets_19638_VariantPlex_GS180v3_Additions_CDK12_FANCL_PPP2R2A_AKT1_TERT_only.noChr.sorted.gene_map.v1.1.bed'


def get_oc120plus_snps_targets_bed_file_from_archer():
    """
    just return the OC120Plus (GS v2.0) PLUS SNPS (but not the SNPs, just the three new genes: CDK12, FANCL, PPP2R2A)
    BED file
    This file was from Archer:
    https://mail.google.com/mail/u/0/#search/Dear/KtbxLwgdgfXxPDhWjbJZgchJFTHlvFvtGB
    And was updated to have all the exon level information like the data we got for oc395
    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/oc180_snps/VariantPlex_GS180_dSA16897081_2_AllExonsTargeted.sorted.bed'


def get_oc120plus_oc395_overlap_targets_bed_file():
    """
    just return the # overalp b/t GS395 and GS180
    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/' \
           'VariantPlex_OC_v3_16897_primers_targets.sorted.noChr_intersect_TargetsRefSeqExonsMap_OC395.bed'


def get_agilent_targets_bed_file():
    """just return the Agilent BED file"""
    return 'lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg19lft_intervname.updated_chr.bed'


def get_oc120_oc120plus_overlap_targets_bed_file():
    """
    just return the # overlap b/t OC120 and OC120plus
    bedtools intersect -a lab_validation_120/internal_data_sources/bed_files_for_assays/variantplex_03012017_coverage_OC120.bed \
    -b lab_validation_120/internal_data_sources/bed_files_for_assays/VariantPlex_OC_v3_16897_primers_targets.sorted.bed~ \
    >lab_validation_120/internal_data_sources/bed_files_for_assays/variantplex_03012017_coverage_OC120_overlap_VariantPlex_OC_v3_16897_primers_targets.sorted.bed

    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/variantplex_03012017_coverage_OC120_overlap_VariantPlex_OC_v3_16897_primers_targets.sorted.bed'

def get_oc395_gtf_file():
    """
    just return the 395 gtf file
    This file was from Fernanda
    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/oc395/cGSP5075_VariantPlex_OncoClinicas_Solid_Tumor-v2_updated_gene_names.gtf'


def get_oc120plus_snps_cnv_bed_file():
    """
    just return the oc120plus (oc180) snps BED CNV file
    This file created for the CNV coefficients calculation you have to use this one, b/c we reduced the number of
    primers so we could calculate the coefficients file
    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/oc180_snps/cnv_mean_mcc_lower_100_upper_mean_mcc_500_gs180_snp_final_v.1.2_updated_gene_names.bed'


def get_oc395_cnv_bed_file():
    """
    just return the oc395 BED CNV file
    This file created for the CNV coefficients calculation you have to use this one, b/c we reduced the number of
    primers so we could calculate the coefficients file
    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/oc395/cnv_mean_mcc_lower_100_upper_mean_mcc_600_gs395_final_v.1.2_updated_gene_names.bed'

def get_oc120_gtf_file():
    """
    just return the OC120 gtf file
    This file was from Fernanda
    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/VariantPlex_OncoClinicas_SolidTumor-v1.0.gtf'


def get_oc120plus_gtf_file():
    """
    just return the OC120Plus (GS v2.0) GTF file
    This file was from Elina Wegner (ArcherDx)
    """
    return 'lab_validation_120/internal_data_sources/bed_files_for_assays/VariantPlex_OncoClinicas_Solid_Tumor_v3-v1.0.gtf'


def get_oc395_archer_exons_excel_file(assay_name=None):
    """
    just return  VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx, and worksheet name dSA16897_v3
    This file was from Elina Wegner (ArcherDx)
    """
    worksheet = '15491_15478_OC_ADtgtsUpdate2'
    spreadsheet = \
        'lab_validation_120/internal_data_sources/bed_files_for_assays/oc395/VariantPlex_GS395_AD15491_Target_List.xlsx'
    return spreadsheet, worksheet


def get_oc120plus_archer_exons_excel_file(assay_name=None):
    """
    just return  VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx, and worksheet name dSA16897_v3
    This file was from Elina Wegner (ArcherDx)
    """
    worksheet = 'dSA16897_v3'
    spreadsheet = \
        'lab_validation_120/internal_data_sources/bed_files_for_assays/VariantPlex_OncoclinicasSolidTumor_v1_v3.xlsx'
    if assay_name == 'oc120plus_snps':  # get the updated workseeht with CDK12, FANCL, PPP2R2A
        worksheet = 'dSA16897_v3_snps'

    # return the spreadsheet and the name of the spreadsheet
    return spreadsheet, worksheet

def get_oc395_gene_list_for_variant_plex():
    """
    List of gene from Archer from the spreadsheet 15491_15478_OC_ADtgtsUpdate2 column A found in:
    VariantPlex_GS395_AD15491_Target_List.xlsx
    """
    # there were duplicates in this data, so set it first and then list
    genes = list({
        'ABL1', 'ACVR1', 'ACVR2A', 'AJUBA', 'AKT1', 'AKT2', 'AKT3', 'ALB',
        'ALK', 'APC', 'AR', 'ARHGAP35', 'ARID1A', 'ARID1B', 'ARID5B', 'ATF7IP',
        'ATM', 'ATR', 'ATRX', 'ATXN3', 'AURKA', 'AURKB', 'AXIN1', 'AXIN2',
        'B2M', 'BAP1', 'BARD1', 'BCL2', 'BCL2L11', 'BLM', 'BRAF', 'BRCA1',
        'BRCA2', 'BRD7', 'BRIP1', 'BTG2', 'CACNA1A', 'CARD11', 'CASP8', 'CBFB',
        'CCND1', 'CCND2', 'CCND3', 'CCNE1', 'CD70', 'CD79B', 'CDC73', 'CDH1',
        'CDK12', 'CDK4', 'CDK6', 'CDKN1A', 'CDKN1B', 'CDKN2A', 'CDKN2B', 'CDKN2C',
        'CEBPA', 'CHD1', 'CHD3', 'CHD4', 'CHD8', 'CHEK1', 'CHEK2', 'CIC',
        'CNBD1', 'CNOT9', 'COL5A1', 'CREB3L3', 'CREBBP', 'CSDE1', 'CSF1R', 'CTCF', 'CTLA4',
        'CTNNA1', 'CTNNB1', 'CTNND1', 'CUL1', 'CUL3', 'CYSLTR2', 'DAXX', 'DAZAP1',
        'DDR2', 'DDX3X', 'DHX9', 'DIAPH2', 'DMD', 'DNMT3A', 'EEF1A1', 'EEF2',
        'EGFR', 'EGR3', 'ELF3', 'ELOC', 'EP300', 'EPAS1', 'EPCAM', 'EPHA2', 'EPHA3',
        'ERBB2', 'ERBB3', 'ERBB4', 'ERCC1', 'ERCC2', 'ERCC4', 'ESR1', 'EWSR1',
        'EZH2', 'FAM175A', 'FAM46C', 'FAM46D', 'FANCA', 'FANCC', 'FANCD2', 'FANCE',
        'FANCF', 'FANCG', 'FANCI', 'FANCL', 'FAT1', 'FBXW7', 'FGF1', 'FGF19',
        'FGF2', 'FGF3', 'FGF4', 'FGF7', 'FGF8', 'FGF9', 'FGFR1', 'FGFR2',
        'FGFR3', 'FGFR4', 'FH', 'FLCN', 'FLNA', 'FLT1', 'FLT3', 'FOXA1',
        'FOXA2', 'FOXL2', 'FOXO1', 'FOXQ1', 'FUBP1', 'GATA3', 'GEN1', 'GLI1',
        'GNA11', 'GNA13', 'GNAQ', 'GNAS', 'GPS2', 'GRIN2D', 'H3-5', 'HGF',
        'HIST1H1C', 'HIST1H1E', 'HIST1H2BD', 'HIST1H3B', 'HLA-A', 'HLA-B', 'HLA-C', 'HNF1A',
        'HRAS', 'HUWE1', 'IDH1', 'IDH2', 'IL6ST', 'IL7R', 'INPPL1', 'IRF2',
        'IRF6', 'JAK1', 'JAK2', 'JAK3', 'KANSL1', 'KDM5A', 'KDM5C', 'KDM6A',
        'KDR', 'KEAP1', 'KEL', 'KIF1A', 'KIT', 'KLF4', 'KLF5', 'KMT2A',
        'KMT2B', 'KMT2C', 'KMT2D', 'KRAS', 'KRT222', 'LAMP1', 'LATS1', 'LATS2',
        'LEMD2', 'LZTR1', 'MACF1', 'MAP2K1', 'MAP2K2', 'MAP2K4', 'MAP3K1', 'MAP3K13',
        'MAP3K4', 'MAPK1', 'MAX', 'MDM2', 'MDM4', 'MECOM', 'MED12', 'MEN1',
        'MET', 'MGA', 'MGMT', 'MITF', 'MLH1', 'MLLT3', 'MPL', 'MRE11',
        'MSH2', 'MSH3', 'MSH6', 'MTOR', 'MUC6', 'MUTYH', 'MYC', 'MYCL',
        'MYCN', 'MYD88', 'MYH9', 'NBN', 'NCOR1', 'NF1', 'NF2', 'NFE2L2',
        'NIPBL', 'NKX2-1', 'NOTCH1', 'NOTCH2', 'NOTCH3', 'NOTCH4', 'NPM1', 'NRAS',
        'NSD1', 'NTRK1', 'NTRK2', 'NTRK3', 'NUP133', 'NUP93', 'PALB2', 'PAX5',
        'PAX8', 'PBRM1', 'PCBP1', 'PDGFRA', 'PDS5B', 'PGR', 'PHF6', 'PIK3CA',
        'PIK3CB', 'PIK3CD', 'PIK3CG', 'PIK3R1', 'PIK3R2', 'PIK3R3', 'PIM1', 'PLCB4',
        'PLCG1', 'PLXNB2', 'PMS1', 'PMS2', 'POLD1', 'POLE', 'POLQ', 'POLRMT',
        'PPM1D', 'PPP2R1A', 'PPP2R2A', 'PPP6C', 'PRKAR1A', 'PRKD1', 'PSIP1', 'PTCH1',
        'PTEN', 'PTMA', 'PTPDC1', 'PTPN11', 'PTPRC', 'PTPRD', 'RAC1', 'RAD21',
        'RAD50', 'RAD51', 'RAD51B', 'RAD51C', 'RAD51D', 'RAD52', 'RAD54L', 'RAF1',
        'RARA', 'RASA1', 'RB1', 'REL', 'RET', 'RFC1', 'RHEB', 'RHOA',
        'RHOB', 'RICTOR', 'RIT1', 'RNF111', 'RNF43', 'RPL22', 'RPL5', 'RPS6KA3',
        'RPS6KB1', 'RPTOR',  'RRAS2', 'RUNX1', 'RXRA', 'SCAF4', 'SDHB',
        'SETBP1', 'SETD2', 'SF1', 'SIN3A', 'SLX4', 'SMAD2', 'SMAD3', 'SMAD4',
        'SMARCA1', 'SMARCA4', 'SMARCB1', 'SMC1A', 'SMC3', 'SMO', 'SOCS1', 'SOX17',
        'SOX2', 'SOX9', 'SPOP', 'SPTA1', 'SPTAN1', 'SRC', 'SRSF2', 'STAG1',
        'STAG2', 'STK11', 'SUFU', 'TAF1', 'TBL1XR1', 'TBX3', 'TCF7L2',
        'TERT', 'TET1', 'TET2', 'TFRC', 'TGFBR1', 'TGFBR2', 'TGIF1', 'THRAP3',
        'TLR4', 'TMSB4X', 'TP53', 'TP63', 'TRAF3', 'TRAF7', 'TSC1', 'TSC2',
        'TSHR', 'TXNIP', 'U2AF1', 'UNCX', 'USP9X', 'VHL', 'WT1', 'XPO1',
        'XRCC2', 'XRCC3', 'ZBTB20', 'ZBTB7B', 'ZC3H12A', 'ZCCHC12', 'ZFHX3', 'ZFP36L1',
        'ZFP36L2', 'ZMYM2', 'ZMYM3', 'ZNF133', 'ZNF750',
    })
    assert len(genes) == 389, f"List of genes is 389, found {len(genes)}"
    return genes


def get_oc120plus_gene_list_for_variant_plex(assay_name=None):
    """
    List of gene from Archer from the spreadsheet dSA16897_v3 column A found in:
    VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx
    """
    # there were duplicates in this data, so set it first and then list
    genes = list(
        {
        'ABL1', 'AKT1', 'AKT2', 'AKT3', 'AKT3', 'ALK', 'APC', 'AR', 'AR', 'ARID1A',
        'ATM', 'ATM', 'ATR', 'ATRX', 'AURKA', 'B2M', 'BAP1', 'BARD1', 'BRAF', 'BRCA1',
        'BRCA1', 'BRCA2', 'BRIP1', 'CCND1', 'CCND2', 'CCNE1', 'CD274', 'CDH1', 'CDK4', 'CDKN2A',
        'CDKN2B', 'CHD1', 'CHEK1', 'CHEK2', 'CHEK2', 'CIC', 'CSF1R', 'CTNNB1', 'DAXX', 'DAXX',
        'DDR2', 'DDX3X', 'EGFR', 'EGFR', 'EGFR', 'EGFR', 'EP300', 'ERBB2', 'ERBB3', 'ERBB3',
        'ERBB4', 'ERCC2', 'ERCC2', 'ESR1', 'EZH2', 'FBXW7', 'FBXW7', 'FGF19', 'FGFR1', 'FGFR1',
        'FGFR2', 'FGFR3', 'FLT3', 'FOXA1', 'FOXL2', 'FUBP1', 'FUBP1', 'GNA11', 'GNAQ', 'GNAS',
        'GNAS', 'H3-3B', 'HNF1A', 'HRAS', 'IDH1', 'IDH2', 'IGF1R', 'JAK1', 'JAK2', 'JAK3',
        'KDM6A', 'KDR', 'KEAP1', 'KIT', 'KLF4', 'KMT2C', 'KRAS', 'LZTR1', 'MAP2K1', 'MAP3K1',
        'MDM2', 'MDM4', 'MED12', 'MEN1', 'MET', 'MLH1', 'MPL', 'MRE11', 'MRE11', 'MSH2',
        'MSH6', 'MSH6', 'MYC', 'MYCL', 'MYCL', 'MYCN', 'NBN', 'NF1', 'NF1', 'NF1',
        'NF2', 'NF2', 'NKX2-1', 'NOTCH1', 'NOTCH2', 'NPM1', 'NRAS', 'PALB2', 'PBRM1', 'PDGFRA',
        'PIK3CA', 'PIK3R1', 'PIK3R1', 'PMS2', 'POLE', 'PPP2R1A', 'PRKCA', 'PRKD1', 'PTCH1', 'PTEN',
        'PTPN11', 'RAD50', 'RAD51B', 'RAD51B', 'RAD51B', 'RAD51C', 'RAD51C', 'RAD51D', 'RAD51D', 'RAD54L',
        'RB1', 'RET', 'RHOA', 'RNF43', 'ROS1', 'SDHB', 'SETD2', 'SMAD2', 'SMAD4', 'SMARCA4',
        'SMARCA4', 'SMARCB1', 'SMO', 'SRC', 'STAG2', 'STAG2', 'STK11', 'SUFU', 'SUZ12', 'TERT',
        'TGFBR2', 'TP53', 'TP53', 'TP63', 'TRAF7', 'TSC1', 'TSC2', 'TSC2', 'TSHR', 'VHL',
        'XRCC2', 'XRCC3'}
    )
    if assay_name == 'oc120plus_snps':  # add these three genes onto the this list
        genes = genes + ['CDK12', 'FANCL', 'PPP2R2A']
        assert len(genes) == 145, f"List of genes is 145, found {len(genes)}"
    else:
        assert len(genes) == 142, f"List of genes is 142, found {len(genes)}"
    return genes


def get_oc120plus_new_gene_list_for_variant_plex(assay_name=None):
    """"
    List of new 52 genes that are in this panel and not in GS v1:  Found this by looking at file:
    VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx
    spreadsheet: dSA12795_v1, columns A
    spreadsheet: dSA16897_v3, columns A, then taking the new genes found here, and not in dSA12795_v1
    """
    genes = [
        'AKT2', 'AKT3', 'AR', 'ATR', 'B2M', 'BAP1', 'BARD1', 'BRIP1', 'CCND1',
        'CCND2', 'CD274', 'CDKN2B', 'CHD1', 'CHEK1', 'CHEK2', 'EP300', 'ERCC2', 'FGF19',
        'FOXA1', 'FUBP1', 'H3-3B', 'IGF1R', 'JAK1', 'KDM6A', 'KLF4', 'KMT2C', 'LZTR1',
        'MDM4', 'MED12', 'MRE11', 'MSH2', 'MYCL', 'NBN', 'NKX2-1', 'NOTCH2', 'PALB2',
        'PBRM1', 'PMS2', 'PPP2R1A', 'PRKCA', 'PRKD1', 'RAD50', 'RAD51B', 'RAD51C', 'RAD51D',
        'RAD54L', 'SETD2', 'SUZ12', 'TGFBR2', 'TRAF7', 'XRCC2', 'XRCC3',
    ]
    if assay_name == 'oc120plus_snps':  # add these three genes onto the this list
        genes = genes + ['CDK12', 'FANCL', 'PPP2R2A']
        assert len(genes) == 55, "The size was not 55 genes"
    else:
        assert len(genes) == 52, "The size was not 52 genes"
    return genes


def get_oc120plus_new_gene_list_to_keep_for_gs_v2(assay_name=None):
    """
    Rodrigo sent me a list of 15 genes that are a subset of the genes found in get_oc120plus_new_gene_list_for_variant_plex.
    Only these genes will be kept in the interval file for GS v2.0
    """
    genes = ["AR", "ATR", "BARD1", "BRIP1", "CD274", "CHEK1", "CHEK2", "MSH2", "PALB2", "PMS2", "RAD50", "RAD51B",
              "RAD51C", "RAD51D", "RAD54L"]

    if assay_name == 'oc120plus_snps':  # add these three genes onto the this list
        genes = genes + ['CDK12', 'FANCL', 'PPP2R2A']
        assert len(genes) == 18, "The size was not 55 genes"
    else:
        assert len(genes) == 15, "The size was not 15 genes"

    return genes

def get_oc395_cnv_annotation_dict_from_gtf_file():
    cnv_gene_map = {}
    with open(get_oc395_gtf_file(), 'r') as infh:
        for line in infh:
            gene = None
            match = re.search(r'gene_id "(\S+)"', line)
            if match:  # there was a gene id match
                # function "SNV"; designer_version "3.9.1"; gtf_date "31JAN2020"; name "None_chr7_97793230_22_-_A1_GSP2"; cache_version "3.0"; bfactor "1.0"; git_tag "624e93c"; transcript_id "NM_014916"; gene_id "LMTK2";
                gene = match.group(1)
            else:  # if not we can still match by the name attribute
                # function "SNV"; designer_version "3.9.1"; gtf_date "31JAN2020"; name "CIC_chr19_42778272_20_-_A1_GSP2"; cache_version "3.0"; bfactor "1.0"; git_tag "624e93c";
                # function "SNV"; designer_version "3.9.1"; gtf_date "31JAN2020"; name "None_chr7_97097087_33_-_A1_GSP2"; cache_version "3.0"; bfactor "1.0"; git_tag "624e93c";
                match = re.search(f'name "(\S+)";', line)
                if match:
                    gene = match.group(1).split('_')[0]
            if gene == "None":  # skip these
                # function "SNV"; designer_version "3.9.1"; gtf_date "31JAN2020"; name "None_chr7_97097087_33_-_A1_GSP2"; cache_version "3.0"; bfactor "1.0"; git_tag "624e93c";
                continue
            # update if needed or init
            if gene in cnv_gene_map:
                cnv_gene_map[gene]['num_primers'] += 1
            else:
                cnv_gene_map[gene] = {}
                cnv_gene_map[gene]['num_primers'] = 1
    return cnv_gene_map


def get_cnv_annotation_dict_from_cnv_bed_file(assay_name=None):
    """
    Get the cnv annotation dictionary from the assay name
    :param assay_name: String of the assay name
    """
    file_name = None
    if 'oc395' in assay_name :
        file_name = get_oc395_cnv_bed_file()
    elif assay_name == 'oc120plus_snps':
        file_name = get_oc120plus_snps_cnv_bed_file()
    else:
        raise ValueError(f"Do not know the cnv bed file for this assay: {assay_name}")

    cnv_gene_map = {}
    with open(file_name, 'r') as infh:
        for line in infh:
            # 1       6246874 6246883 RPL22_chr1_6246883_27_-
            values = line.rstrip().split('\t')
            gene = values[-1].split('_')[0]  # get the last value and then split on '_'
            if gene in cnv_gene_map:
                cnv_gene_map[gene]['num_primers'] += 1
            else:
                cnv_gene_map[gene] = {}
                cnv_gene_map[gene]['num_primers'] = 1
    return cnv_gene_map


def get_oc120plus_cnv_annotation_dict_from_excel_file(assay_name=None):
    """
    Return the excel spreadsheet file and sheet name with the CNV that will be used.  The spreadsheet looks like the
    following:

    Num_primers	    Gene	    Clinically_relevant
    179	            KMT2C	    FALSE
    176	            ATM	        FALSE
    134	            NF1	        FALSE
    ..
    ..

    Clinically_relevant was listed by Rodriogo for oc120plus go-live
    """
    file, sheet_name = (
    "lab_validation_120/internal_data_sources/bed_files_for_assays/VariantPlex_OncoClinicas_Solid_Tumor_v13-v1.0.cnv-v13-12102020.xlsx",
    'VariantPlex_OncoClinicas_Solid')
    if assay_name == 'oc120plus_snps':
        file, sheet_name = ("lab_validation_120/internal_data_sources/bed_files_for_assays/oc180_snps/VariantPlex_OncoClinicas_Solid_Tumor_v13-v1.0.cnv-v14-03212022.xlsx",
                'VariantPlex_OncoClinicas_Solid')

    df = pd.read_excel(file, sheet_name=sheet_name)
    # covert the data frame to a dictionary
    # {
    #    'KMT2C': {'num_primers': 179, 'clinically_relevant': False},
    #    'ATM': {'num_primers': 176, 'clinically_relevant': False},
    # }v
    return {gene_name: {
        'num_primers': num_primers,
        'clinically_relevant': clinically_relevant,
    } for gene_name, num_primers, clinically_relevant in zip(df.Gene, df.Num_primers, df.Clinically_relevant)}


def get_v2_fusion_annotation_list():
    """
    Return the Fusion GFT parsed file
    """
    file = "lab_validation_120/internal_data_sources/bed_files_for_assays/FusionPlex_OncoClinicas_Solid_Tumor_v2-v1.0_updated_exon_information.gtf"
    list_fusions = _helper_fusion_parser(file)
    return list_fusions

def get_oc120plus_fusion_annotation_list():
    """
    Return the Fusion GFT parsed file
    """
    file = "lab_validation_120/internal_data_sources/bed_files_for_assays/FusionPlex_OncoClinicas_SolidTumor_cSK5058-v1.1.gtf"
    list_fusions = _helper_fusion_parser(file)
    return list_fusions

def _helper_fusion_parser(file=None):
    with open(file) as infh:
        # go over an create a dictionary of data
        fusion_list = []
        for line in infh:
            # get rid of all ' and "", and split the values on tab
            all_values = line.rstrip().replace("'", '').replace('"', '').split("\t")

            # function "SNV,FUSION"; designer_version "3.9.1"; gtf_date "13JUN2017";
            # name "CSF1_chr1_110471478_21 2_+_A1_GSP2"; cache_version "3.0"; bfactor "1.0"; git_tag "624e93c";
            # transcript_id "NM_000757"; gene_id "CSF1";
            annotation_values = all_values[8].split(';')
            annotation_values.pop() # there was a trialing ;
            annotation_dict = {}
            for ele in annotation_values:
                # can't just split on ' ' , gene_id GPI_chr19_34890194_20_+_A1_GSP2; transcript_id ; function CONTROL;
                # b/c in this example transcript_id has no value to split into two values
                ele_vals = ele.split()
                key = ele_vals[0].replace(" ", "") # remove spaces around the key
                annotation_dict[key] = ele_vals[1] if len(ele_vals) > 1 else None # make sure there is a [1] value
            # some entries like this:  gene_id "MSMB"' or gene_id "GPI_chr19_34890194_20_+_A1_GSP2"',
            gene_name = annotation_dict['gene_id'].split('_')[0]
            # update the fusion dictionary.  This is a dictionary of all genes in the fusion panel
            fusion_list.append((gene_name, annotation_dict))

    final_list_fusion = []
    for gene, dict_ in fusion_list:
        if 'exon_number' in dict_:  # only store if an exon is available, since in FusionPlex®_OncoclinicasSolidTumor_v1_v2.xlsx
            # there are entries like:  Where this is not a fusion partner
            # NRAS	NM_002524	2,3,4	Mutation
            final_list_fusion.append((gene, dict_))
        elif dict_['function'] == 'CONTROL':
            # no exon FusionPlex_OncoClinicas_SolidTumor_cSK5058-v1.1.gtf
            # gene_id "RAB7A_chr3_128516850_29_+_A1_GSP2"; transcript_id ""; function "CONTROL";
            final_list_fusion.append((gene, dict_))

    return final_list_fusion


def get_gene_to_esemble_mapping_from_vv():
    """
    Open the json file and return a python data structure
    """
    file = 'lab_validation_120/internal_data_sources/bed_files_for_assays/snapshot2_map.json'
    with open(file) as infh:
        data = json.load(infh)
    return data


def get_oc120plus_msi_list():
    # this came from cut -f 4 dSA16897_cSA5075_MSI_Targets.bed
    # removed two real gene names, b/c the rest are not gene names but are in the bed file:
    # VariantPlex_OC_v3_16897_primers_targets.sorted.bed, so just removed TGFBR2 and MYCL_TTTC
    # "D18S363_1", "D18S363_2", "D18S363_3", "D18S363_4", "D18S1156_1" these all become D18S363
    # D18S1156_1 -> D18S1156
    # BAT34c4_BROAD_RANGE - BAT34c4
    return ["BAT25", "BAT26", "D2S123", "D5S346", "D17S250", "BAT34c4", "D18S55",  "D18S363", "D18S1156", "BAT40",
            "D10S197"]


def get_gc_bias_config_for_htqc_metric():
    # the y_upper_lim values for the GC Bias these values were set via trial and error.  Do not
    # change
    return [
        {
            'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
            'acronym_of_metric_to_plot': 'MCC',
            'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
            'y_upper_lim': 3,  # coverage is plotted on this axis
        },
        {
            'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
            'acronym_of_metric_to_plot': 'MAC',
            'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
            'y_upper_lim': 5,  # coverage is plotted on this axis
        },
        {
            'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
            'acronym_of_metric_to_plot': 'MFC',
            'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
            'y_upper_lim': 4,  # coverage is plotted on this axis
        }
    ]


def get_archer_fusion_direction_file():
    """just return archer fusion direction file"""
    return 'lab_validation_120/internal_data_sources/archer_fusion/fusion_direction.csv'


def get_agilent_hg38_hybrid_capture_dna_targets_bed_file():
    """just return Agilent hg38 DNA targets bed file file"""
    # TODO Remove commented string, just here for posterity now...
    #return 'lab_validation_120/external_data_sources/agilent/hg38_A3416642_Covered_DNA.bed'
    return 'lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg38.bed'


def get_agilent_hg38_hybrid_capture_rna_targets_bed_file():
    """just return Agilent hg38 DNA targets bed file """
    return 'lab_validation_120/external_data_sources/agilent/hg38_A3370051_Covered_RNA.bed'


def get_agilent_hg19_hybrid_capture_dna_targets_bed_file():
    """just return Agilent hg19 DNA targets bed file file"""
    # TODO Remove commented string, just here for posterity now...
    #return 'lab_validation_120/external_data_sources/agilent/hg19_A3416642_Covered_DNA.bed'
    return 'lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg19lft_intervname.bed'


def get_agilent_hg19_hybrid_capture_dna_targets_bed_file_for_ifcnv_bed_file_generation():
    """
    just return Agilent hg19 DNA targets bed file required to be parsed into the ifCNV BED file format
    The BED file returned here is generated via:
    \time pipenv run python3 -m tools.gene_maps.agilent.create_agilent_bed_file_for_cider \
    --json_file_name_version 13  2>log_i24

    The BED returned here is used for gs700 and ifCNV via (Command below creates the BED files test.bed
    pipenv run python3 -m tools.cnv.ifCNV.get_ifCNV_bed_format --print_simple_bed_file True --assay_name gs700 >test.bed
    """
    return 'lab_validation_120/external_data_sources/agilent/hg19_A3416642_Covered_DNA.ifCNV.bed'


def get_agilent_hg19_hybrid_capture_rna_targets_bed_file():
    """just return Agilent hg19 DNA targets bed file"""
    return 'lab_validation_120/external_data_sources/agilent/hg19_A3416642_Covered_RNA.bed'


def get_agilent_hybrid_capture_dna_gene_list_from_brochure():
    """
    This file was prepared by table 1 of the agilent file
    brochure-cancer-cgp-assay-5994-5801EN-agilent.pdf
    """
    return 'lab_validation_120/external_data_sources/agilent/genes-dna-table1-brochure-cancer-cgp-assay-5994-5801EN-agilent.txt'


def get_agilent_hybrid_capture_rna_gene_list_from_brochure():
    """
    This file was prepared by table 2 of the agilent file
    brochure-cancer-cgp-assay-5994-5801EN-agilent.pdf
    """
    return 'lab_validation_120/external_data_sources/agilent/genes-rna-table1-brochure-cancer-cgp-assay-5994-5801EN-agilent.txt'


def get_agilent_hybrid_capture_rna_gene_directionality():
    """
    This file has the updated genes and the directionality for fusions
    """
    return 'lab_validation_120/external_data_sources/agilent/fusion-direction-v10-20230713.xlsx', \
           'fusion-direction-update'


def get_agilent_hybrid_capture_dna_cnv_gene_list_from_brochure():
    """
    This file was prepared by table 1 of the agilent file
    brochure-cancer-cgp-assay-5994-5801EN-agilent.pdf
    """
    return 'lab_validation_120/external_data_sources/agilent/genes-dna-cnv-table1-brochure-cancer-cgp-assay-5994-5801EN-agilent.txt'


def get_agilent700_oc395_overlap_targets_bed_file():
    """
    just return the # overlap b/t Agilent700 and GS395
    bedtools intersect -a \
    ~/BLI-repo/lab-validation-120/lab_validation_120/internal_data_sources/bed_files_for_assays/oc395/TargetsRefSeqExonsMap_OC395_updated_gene_names_updated_MET_exon14.v1.3.bed \
    -b ~/BLI-repo/lab-validation-120/lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg19lft_50bppadded_intervname.updated_chr.bed  \
    >~/BLI-repo/lab-validation-120/lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg19lft_intervname.updated_chr.intersect_gs395_agilent.bed

    """
    return 'lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg19lft_intervname.updated_chr.intersect_gs395_agilent.bed'


def get_agilent700_oc180_overlap_targets_bed_file():
    """
    just return the # overalp b/t Agilent700 and GS180
    bedtools intersect -a \
    ~/BLI-repo/lab-validation-120/lab_validation_120/internal_data_sources/bed_files_for_assays/oc180_snps/VariantPlex_OC_v3_16897_primers_targets_19638_VariantPlex_GS180v3_Additions_CDK12_FANCL_PPP2R2A_AKT1_TERT_only.noChr.sorted.gene_map.v1.1.bed \
    -b ~/BLI-repo/lab-validation-120/lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg19lft_50bppadded_intervname.updated_chr.bed  \
    >~/BLI-repo/lab-validation-120/lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg19lft_intervname.updated_chr.intersect_gs180_agilent.bed

    """
    return 'lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg19lft_intervname.updated_chr.intersect_gs180_agilent.bed'


def get_agilent700_interval_annotation_excel():
    """
    Output from
    \time pipenv run python3 -m tools.gene_maps.agilent.create_agilent_bed_file_for_cider \
    --json_file_name_version 13
    :return: string of the excel file
    """
    return 'lab_validation_120/external_data_sources/agilent/hg19_and_hg38_dna_and_rna_v13.xlsx'


def get_agilent_700_interval_100bp_gc_content():
    """

    :return:
    """
    return 'lab_validation_120/external_data_sources/agilent/A3416642_Regions_hg19lft_intervname_100bp_window.bedtools_nuc_gc.txt'


def get_hg19_agilent_700_probe_interval_100bp():
    """
    This is the hg19 file that's been broken up into 100bp overlap
    :return:
    """
    return "lab_validation_120/external_data_sources/agilent/probe_bed_files/hg19_A3416642_Covered_DNA_100bp_window.upadated_chr.bed"


def get_hg19_agilent_700_probe_interval_100bp_facets_bed_file():
    """
    This is the hg19 file that's been broken up into 100bp overlap
    :return:
    """
    file = "_".join((os.path.splitext(get_hg19_agilent_700_probe_interval_100bp())[0],
                     "cnv_facets_annotation_file.bed"))

    return file

def get_hg38_agilent_700_probe_interval():
    """
    This is the hg38 file that's been broken up into 100bp overlap
    :return:
    """
    return "lab_validation_120/external_data_sources/agilent/probe_bed_files/hg38_A3416642_Covered_DNA.bed"
