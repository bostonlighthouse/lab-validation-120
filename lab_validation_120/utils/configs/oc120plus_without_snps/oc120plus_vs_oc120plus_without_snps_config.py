"""Simple config for GS180 without SNPs verification study we performed"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_oc120plus_vs_oc120plus_without_snps_reproducibility_11_samples():
    """
    Get the VCF files for the GS180 without SNPs verification study we performed - 11 samples
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/oc120plus_vs_oc120plus_without_snps",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    'scratch/oc120plus_vs_oc120plus_without_snps/oc120plus_vs_oc120plus_without_snps_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                    'scratch/oc120plus_vs_oc120plus_without_snps/oc120plus_vs_oc120plus_without_snps_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                ],
                "indel": [
                    'scratch/oc120plus_vs_oc120plus_without_snps/oc120plus_vs_oc120plus_without_snps_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                    'scratch/oc120plus_vs_oc120plus_without_snps/oc120plus_vs_oc120plus_without_snps_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                ]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file":  [
                        'scratch/oc120plus_vs_oc120plus_without_snps/oc120plus_vs_oc120plus_without_snps_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                        'scratch/oc120plus_vs_oc120plus_without_snps/oc120plus_vs_oc120plus_without_snps_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        'scratch/oc120plus_vs_oc120plus_without_snps/oc120plus_vs_oc120plus_without_snps_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                        'scratch/oc120plus_vs_oc120plus_without_snps/oc120plus_vs_oc120plus_without_snps_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "GS180 new Primers (no SNPs)",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "GS180 new Primers (no SNPs)",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "GS180 new Primers (no SNPs)",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            "concordance_files": {
                'inter_Sample1': [    {    'sample_id': '1a-cid21-1555',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot.B21-110.CID21-1555.A544_N704.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {    'sample_id': '1b-cid22-441',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-441-snapshot/2022fev25-oc180-run0-b22-23-cid22-441-snapshot.B22-23.CID22-441.B519_N703.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample2': [    {    'sample_id': '2a-cid21-1558',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot.B21-110.CID21-1558.A546_N706.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {    'sample_id': '2b-cid22-443',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-443-snapshot/2022fev25-oc180-run0-b22-23-cid22-443-snapshot.B22-23.CID22-443.B521_N705.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample3': [    {    'sample_id': '3a-cid21-448',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-08-oc120-run-b21-45-cid21-448-snapshot/2021-04-08-oc120-run-b21-45-cid21-448-snapshot.B21-45.CID21-448.B512_N704.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {    'sample_id': '3b-cid22-444',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-444-snapshot/2022fev25-oc180-run0-b22-23-cid22-444-snapshot.B22-23.CID22-444.B522_N706.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample4': [    {    'sample_id': '4a-cid21-505',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-16-gs180-run-b21-47-cid21-505-snapshot/2021-04-16-gs180-run-b21-47-cid21-505-snapshot.B21-47.CID21-505.B536_N704.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {    'sample_id': '4b-cid22-452',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-452-snapshot/2022fev25-oc180-run0-b22-23-cid22-452-snapshot.B22-23.CID22-452.B524_N708.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample5': [    {    'sample_id': '5a-cid21-551',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-30-oc180-run-b21-55-cid21-551-snapshot/2021-04-30-oc180-run-b21-55-cid21-551-snapshot.B21-55.CID21-551.B544_N704.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {    'sample_id': '5b-cid22-453',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-453-snapshot/2022fev25-oc180-run0-b22-23-cid22-453-snapshot.B22-23.CID22-453.B525_N701.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {'experiment_type': 'inter-run'}],
                # removed b/c of differences in Fraction Greater than Upper DP
                #'inter_Sample6': [    {    'sample_id': '6a-cid21-765',
                #                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-05-21-oc180-run-b21-67-cid21-765-snapshot/2021-05-21-oc180-run-b21-67-cid21-765-snapshot.B21-67.CID21-765.B534_N702.merged_concatenated.vcf',
                #                           'variant_type': 'snvs'},
                #                      {    'sample_id': '6b-cid22-454',
                #                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-454-snapshot/2022fev25-oc180-run0-b22-23-cid22-454-snapshot.B22-23.CID22-454.B526_N702.merged_concatenated.vcf',
                #                           'variant_type': 'snvs'},
                #                      {'experiment_type': 'inter-run'}],
                'inter_Sample7': [    {    'sample_id': '7a-cid21-557',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-30-oc180-run-b21-53-cid21-557-snapshot/2021-04-30-oc180-run-b21-53-cid21-557-snapshot.B21-53.CID21-557.B507_N707.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {    'sample_id': '7b-cid22-440',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-440-snapshot/2022fev25-oc180-run0-b22-23-cid22-440-snapshot.B22-23.CID22-440.B518_N702.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample8': [    {    'sample_id': '8a-cid21-951',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-06-11-oc180-run-b21-80-cid21-951-snapshot/2021-06-11-oc180-run-b21-80-cid21-951-snapshot.B21-80.CID21-951.A502_N702.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {    'sample_id': '8b-cid22-439',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-439-snapshot/2022fev25-oc180-run0-b22-23-cid22-439-snapshot.B22-23.CID22-439.B517_N701.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample9': [    {    'sample_id': '9a-cid21-1108',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot.B21-89.CID21-1108.A509_N701.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {    'sample_id': '9b-cid22-458',
                                           'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-458-snapshot/2022fev25-oc180-run0-b22-23-cid22-458-snapshot.B22-23.CID22-458.B528_N704.merged_concatenated.vcf',
                                           'variant_type': 'snvs'},
                                      {'experiment_type': 'inter-run'}],
                'inter_Sample10': [    {    'sample_id': '10a-cid21-1126',
                                            'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot.B21-89.CID21-1126.A515_N707.merged_concatenated.vcf',
                                            'variant_type': 'snvs'},
                                       {    'sample_id': '10b-cid22-445',
                                            'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-445-snapshot/2022fev25-oc180-run0-b22-23-cid22-445-snapshot.B22-23.CID22-445.B523_N707.merged_concatenated.vcf',
                                            'variant_type': 'snvs'},
                                       {'experiment_type': 'inter-run'}],
                'inter_Sample11': [    {    'sample_id': '11a-cid21-1112',
                                            'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot.B21-89.CID21-1112.A510_N702.merged_concatenated.vcf',
                                            'variant_type': 'snvs'},
                                       {    'sample_id': '11b-cid22-442',
                                            'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-442-snapshot/2022fev25-oc180-run0-b22-23-cid22-442-snapshot.B22-23.CID22-442.B520_N704.merged_concatenated.vcf',
                                            'variant_type': 'snvs'},
                                       {'experiment_type': 'inter-run'}]
            }

        }
    }


def get_oc120plus_vs_oc120plus_without_snps_htqc_plots_11_samples():
    """
    Get the VCF files for the GS180 without SNPs verification study we performed - 11 samples
    """

    key_2_use = 'oc120plus_vs_oc120plus_without_snps'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS180 vs GS180 withou SNPs - 11 Samples",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot.B21-110.CID21-1555.A544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot.B21-110.CID21-1555.A544_N704.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid21-1555',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-441-snapshot/2022fev25-oc180-run0-b22-23-cid22-441-snapshot.B22-23.CID22-441.B519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-441-snapshot/2022fev25-oc180-run0-b22-23-cid22-441-snapshot.B22-23.CID22-441.B519_N703.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid22-441',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot.B21-110.CID21-1558.A546_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot.B21-110.CID21-1558.A546_N706.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid21-1558',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-443-snapshot/2022fev25-oc180-run0-b22-23-cid22-443-snapshot.B22-23.CID22-443.B521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-443-snapshot/2022fev25-oc180-run0-b22-23-cid22-443-snapshot.B22-23.CID22-443.B521_N705.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid22-443',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-08-oc120-run-b21-45-cid21-448-snapshot/2021-04-08-oc120-run-b21-45-cid21-448-snapshot.B21-45.CID21-448.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-08-oc120-run-b21-45-cid21-448-snapshot/2021-04-08-oc120-run-b21-45-cid21-448-snapshot.B21-45.CID21-448.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid21-448',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-444-snapshot/2022fev25-oc180-run0-b22-23-cid22-444-snapshot.B22-23.CID22-444.B522_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-444-snapshot/2022fev25-oc180-run0-b22-23-cid22-444-snapshot.B22-23.CID22-444.B522_N706.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid22-444',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-16-gs180-run-b21-47-cid21-505-snapshot/2021-04-16-gs180-run-b21-47-cid21-505-snapshot.B21-47.CID21-505.B536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-16-gs180-run-b21-47-cid21-505-snapshot/2021-04-16-gs180-run-b21-47-cid21-505-snapshot.B21-47.CID21-505.B536_N704.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid21-505',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-452-snapshot/2022fev25-oc180-run0-b22-23-cid22-452-snapshot.B22-23.CID22-452.B524_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-452-snapshot/2022fev25-oc180-run0-b22-23-cid22-452-snapshot.B22-23.CID22-452.B524_N708.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid22-452',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-30-oc180-run-b21-55-cid21-551-snapshot/2021-04-30-oc180-run-b21-55-cid21-551-snapshot.B21-55.CID21-551.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-30-oc180-run-b21-55-cid21-551-snapshot/2021-04-30-oc180-run-b21-55-cid21-551-snapshot.B21-55.CID21-551.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid21-551',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-453-snapshot/2022fev25-oc180-run0-b22-23-cid22-453-snapshot.B22-23.CID22-453.B525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-453-snapshot/2022fev25-oc180-run0-b22-23-cid22-453-snapshot.B22-23.CID22-453.B525_N701.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid22-453',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-05-21-oc180-run-b21-67-cid21-765-snapshot/2021-05-21-oc180-run-b21-67-cid21-765-snapshot.B21-67.CID21-765.B534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-05-21-oc180-run-b21-67-cid21-765-snapshot/2021-05-21-oc180-run-b21-67-cid21-765-snapshot.B21-67.CID21-765.B534_N702.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid21-765',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-454-snapshot/2022fev25-oc180-run0-b22-23-cid22-454-snapshot.B22-23.CID22-454.B526_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-454-snapshot/2022fev25-oc180-run0-b22-23-cid22-454-snapshot.B22-23.CID22-454.B526_N702.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid22-454',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-30-oc180-run-b21-53-cid21-557-snapshot/2021-04-30-oc180-run-b21-53-cid21-557-snapshot.B21-53.CID21-557.B507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-04-30-oc180-run-b21-53-cid21-557-snapshot/2021-04-30-oc180-run-b21-53-cid21-557-snapshot.B21-53.CID21-557.B507_N707.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid21-557',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-440-snapshot/2022fev25-oc180-run0-b22-23-cid22-440-snapshot.B22-23.CID22-440.B518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-440-snapshot/2022fev25-oc180-run0-b22-23-cid22-440-snapshot.B22-23.CID22-440.B518_N702.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid22-440',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-06-11-oc180-run-b21-80-cid21-951-snapshot/2021-06-11-oc180-run-b21-80-cid21-951-snapshot.B21-80.CID21-951.A502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-06-11-oc180-run-b21-80-cid21-951-snapshot/2021-06-11-oc180-run-b21-80-cid21-951-snapshot.B21-80.CID21-951.A502_N702.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid21-951',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-439-snapshot/2022fev25-oc180-run0-b22-23-cid22-439-snapshot.B22-23.CID22-439.B517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-439-snapshot/2022fev25-oc180-run0-b22-23-cid22-439-snapshot.B22-23.CID22-439.B517_N701.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid22-439',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot.B21-89.CID21-1108.A509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot.B21-89.CID21-1108.A509_N701.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid21-1108',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-458-snapshot/2022fev25-oc180-run0-b22-23-cid22-458-snapshot.B22-23.CID22-458.B528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-458-snapshot/2022fev25-oc180-run0-b22-23-cid22-458-snapshot.B22-23.CID22-458.B528_N704.coverage.summary.htqc.txt',
                        'sample_id': '9b-cid22-458',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot.B21-89.CID21-1126.A515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot.B21-89.CID21-1126.A515_N707.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid21-1126',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-445-snapshot/2022fev25-oc180-run0-b22-23-cid22-445-snapshot.B22-23.CID22-445.B523_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-445-snapshot/2022fev25-oc180-run0-b22-23-cid22-445-snapshot.B22-23.CID22-445.B523_N707.coverage.summary.htqc.txt',
                        'sample_id': '10b-cid22-445',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot.B21-89.CID21-1112.A510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot.B21-89.CID21-1112.A510_N702.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid21-1112',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-442-snapshot/2022fev25-oc180-run0-b22-23-cid22-442-snapshot.B22-23.CID22-442.B520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_without_snps/2022fev25-oc180-run0-b22-23-cid22-442-snapshot/2022fev25-oc180-run0-b22-23-cid22-442-snapshot.B22-23.CID22-442.B520_N704.coverage.summary.htqc.txt',
                        'sample_id': '11b-cid22-442',
                        'sample_type': ''}
                ]
            }
        }
    }
