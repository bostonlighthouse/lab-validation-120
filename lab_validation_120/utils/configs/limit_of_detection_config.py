"""Config for the limit of detection experiment

HD827 - NA20-106
CID20-952 100% Horizon
CID20-953 50% Horizon
CID20-954 25% Horizon
CID20-955 12.5% Horizon
CID20-956 6% Horizon

PERLA - NA20-401 (PERLA is just the name of the sample)
CID20-957 100% Clinical
CID20-958 50% Clinical
CID20-959 25% Clinical
CID20-960 - 12.5% Clinical
CID20-961 - 6% Clinical

OC12plus here:

"""


def _get_oc120plus_horizon_snp_file():
    """Just return the string location of this file for oc120plus"""
    #return "lab_validation_120/external_data_sources/horizon_gold_sets/hd798_oc120plus.txt"
    return "lab_validation_120/external_data_sources/horizon_gold_sets/hd798_oc120.txt"


def _get_oc120plus_horizon_indel_file():
    """Just return the string location of this file for oc120plus"""
    return "lab_validation_120/external_data_sources/horizon_gold_sets/hd798_oc120plus_indels_only_for_lod.txt"


def _get_oc120plus_image_dir(replicate=None):
    """
    Just return the string of where to store the images for oc120plus
    :param replicate: String to add in the directory
    """

    return '/'.join(("images/limit_of_detection_oc120plus", replicate))


def _get_oc120_clinical_snp_file():
    """Just return the string location of this file for oc120"""
    return "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/perla_clinical_sample.txt"


def _get_oc120_clinical_indel_file():
    """Just return the string location of this file for oc120"""
    return "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/perla_clinical_sample.txt"


def _get_oc120plus_clinical_snp_file():
    """Just return the string location of this file for oc120plus"""
    return "lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/clinical_sample.txt"


def _get_oc120plus_clinical_indel_file():
    """Just return the string location of this file for oc120plus"""
    return "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/TODO"


def _get_oc120_image_dir():
    """Just return the string of where to store the images for oc120"""
    return "images/limit_of_detection"


def get_horizon_concordance_in_lod_oc120(snp_type=None):
    """
    Get the clinical sample for LOD analysis
    :param snp_type:  Either snp or indels
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        horizon_file = "lab_validation_120/external_data_sources/horizon_gold_sets/hd827_for_lod.txt"
    elif snp_type == 'indels':
        horizon_file = "lab_validation_120/external_data_sources/horizon_gold_sets/hd827_indels_only_for_lod.txt"
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc120_image_dir(),
            "vaf_regression_plot": {
                "x_axis_label": "OC120",
                "y_axis_label": "Horizon",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "concordance_files":
                [
                    {
                        # CID CID20-952 = V_HD827
                        "sample_id": "1st_HD827",
                        "horizon_file": horizon_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-952-snapshot/2020set28-oc120-run5-b20-99-cid20-952-snapshot.B20-99.CID20-952.C536_N704.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-953 = V_HD827 1/2
                        "sample_id": "2nd_50%",
                        "horizon_file": horizon_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-953-snapshot/2020set28-oc120-run5-b20-99-cid20-953-snapshot.B20-99.CID20-953.C537_N705.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-954 = V_HD827 1/4
                        "sample_id": "3rd_25%",
                        "horizon_file": horizon_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-954-snapshot/2020set28-oc120-run5-b20-99-cid20-954-snapshot.B20-99.CID20-954.C538_N706.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-955 = V_HD827 1/8
                        "sample_id": "4th_12.5%",
                        "horizon_file": horizon_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-955-snapshot/2020set28-oc120-run5-b20-99-cid20-955-snapshot.B20-99.CID20-955.C539_N707.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-956 = V_HD827 1/16
                        "sample_id": "5th_6.25%",
                        "horizon_file": horizon_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-956-snapshot/2020set28-oc120-run5-b20-99-cid20-956-snapshot.B20-99.CID20-956.C540_N708.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },

                ]
        }
    }


def get_horizon_concordance_in_lod_oc120plus_rep1(snp_type=None):
    """
    Get the clinical sample for LOD analysis, Rep1
    :param snp_type:  Either snp or indels
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        horizon_file = _get_oc120plus_horizon_snp_file()
    elif snp_type == 'indels':
        horizon_file = _get_oc120plus_horizon_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc120plus_image_dir(replicate='rep1'),
            "vaf_regression_plot": {
                "x_axis_label": "GS v2.0",
                "y_axis_label": "Horizon HD798",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": None,
            "concordance_files":
                [
                    {'horizon_file': horizon_file,
                     'sample_id': '1_oc120plus_horizon_lod_rep1_100%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1445-snapshot/2020dez23-oc120-run7-b20-148-cid20-1445-snapshot.B20-148.CID20-1445.B511_N705.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '2_oc120plus_horizon_lod_rep1_50%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1446-snapshot/2020dez23-oc120-run7-b20-148-cid20-1446-snapshot.B20-148.CID20-1446.B512_N706.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '3_oc120plus_horizon_lod_rep1_25%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1447-snapshot/2020dez23-oc120-run7-b20-148-cid20-1447-snapshot.B20-148.CID20-1447.B513_N707.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '4_oc120plus_horizon_lod_rep1_12.5%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1448-snapshot/2020dez23-oc120-run7-b20-148-cid20-1448-snapshot.B20-148.CID20-1448.B514_N708.merged_concatenated.vcf'}
                ]
        }
    }


def get_horizon_concordance_in_lod_oc120plus_rep2(snp_type=None):
    """
    Get the clinical sample for LOD analysis, Rep2
    :param snp_type:  Either snp or indels
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        horizon_file = _get_oc120plus_horizon_snp_file()
    elif snp_type == 'indels':
        horizon_file = _get_oc120plus_horizon_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc120plus_image_dir(replicate='rep2'),
            "vaf_regression_plot": {
                "x_axis_label": "GS v2.0",
                "y_axis_label": "Horizon HD798",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": None,
            "concordance_files":
                [
                    {'horizon_file': horizon_file,
                     'sample_id': '1_oc120plus_horizon_lod_rep2_100%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1449-snapshot/2020dez23-oc120-run7-b20-148-cid20-1449-snapshot.B20-148.CID20-1449.C537_N701.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '2_oc120plus_horizon_lod_rep2_50%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1450-snapshot/2020dez23-oc120-run7-b20-148-cid20-1450-snapshot.B20-148.CID20-1450.C538_N702.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '3_oc120plus_horizon_lod_rep2_25%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1451-snapshot/2020dez23-oc120-run7-b20-148-cid20-1451-snapshot.B20-148.CID20-1451.C539_N703.merged_concatenated.vcf'},
                    {'horizon_file': horizon_file,
                     'sample_id': '4_oc120plus_horizon_lod_rep2_12.5%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1452-snapshot/2020dez23-oc120-run7-b20-148-cid20-1452-snapshot.B20-148.CID20-1452.C540_N704.merged_concatenated.vcf'}
                ]
        }
    }


def get_clinical_sample_concordance_in_lod_oc120_rep1(snp_type=None):
    """
    Get the clinical sample for LOD analysis for OC120 for rep1
    :param snp_type:  Either snp or both
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        clinical_variant_file = _get_oc120_clinical_snp_file()
    elif snp_type == 'indels':
        clinical_variant_file = _get_oc120_clinical_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc120_image_dir(),
            "vaf_regression_plot": {
                "x_axis_label": "OC120",
                "y_axis_label": "Horizon",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/dilution_sample/2019out17-archer-run-b19-33-cid19-4631-snapshot/2019out17-archer-run-b19-33-cid19-4631-snapshot.B19-33.CID19-4631.C505_N705.merged_concatenated.vcf",
            "concordance_files":
                [
                    {
                        # CID CID20-1054 = V_PERLA
                        "sample_id": "1st_Perla",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1054-snapshot/2020out23-oc120-run6-b20-116-cid20-1054-snapshot.B20-116.CID20-1054.C542_N707.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-1056 = V_PERLA 1/2
                        "sample_id": "2nd_50%",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1056-snapshot/2020out23-oc120-run6-b20-116-cid20-1056-snapshot.B20-116.CID20-1056.C543_N708.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-1058 = V_PERLA 1/4
                        "sample_id": "3rd_25%",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1058-snapshot/2020out23-oc120-run6-b20-116-cid20-1058-snapshot.B20-116.CID20-1058.A533_N701.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-1060 = V_PERLA 1/8
                        "sample_id": "4th_12.5%",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1060-snapshot/2020out23-oc120-run6-b20-116-cid20-1060-snapshot.B20-116.CID20-1060.A534_N702.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-1062 = V_PERLA 1/16
                        "sample_id": "5th_6.25%",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1062-snapshot/2020out23-oc120-run6-b20-116-cid20-1062-snapshot.B20-116.CID20-1062.A535_N703.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },

                ]
        }
    }


def get_clinical_sample_concordance_in_lod_oc120_rep2(snp_type=None):
    """
    Get the clinical sample for LOD analysis for OC120 for rep2
    :param snp_type:  Either snp or both
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        clinical_variant_file = _get_oc120_clinical_snp_file()
    elif snp_type == 'indels':
        clinical_variant_file = _get_oc120_clinical_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc120_image_dir(),
            "vaf_regression_plot": {
                "x_axis_label": "OC120",
                "y_axis_label": "Horizon",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/dilution_sample/2019out17-archer-run-b19-33-cid19-4631-snapshot/2019out17-archer-run-b19-33-cid19-4631-snapshot.B19-33.CID19-4631.C505_N705.merged_concatenated.vcf",
            "concordance_files":
                [
                    {
                        # CID CID20-1055 = V_PERLA
                        "sample_id": "1st_Perla",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1055-snapshot/2020out23-oc120-run6-b20-116-cid20-1055-snapshot.B20-116.CID20-1055.A536_N704.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-1057 = V_PERLA 1/2
                        "sample_id": "2nd_50%",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1057-snapshot/2020out23-oc120-run6-b20-116-cid20-1057-snapshot.B20-116.CID20-1057.A537_N705.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-1059 = V_PERLA 1/4
                        "sample_id": "3rd_25%",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1059-snapshot/2020out23-oc120-run6-b20-116-cid20-1059-snapshot.B20-116.CID20-1059.A538_N706.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-1061 = V_PERLA 1/8
                        "sample_id": "4th_12.5%",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1061-snapshot/2020out23-oc120-run6-b20-116-cid20-1061-snapshot.B20-116.CID20-1061.A539_N707.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },
                    {
                        # CID CID20-1063 = V_PERLA 1/16
                        "sample_id": "5th_6.25%",
                        "horizon_file": clinical_variant_file,
                        "vcf_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1063-snapshot/2020out23-oc120-run6-b20-116-cid20-1063-snapshot.B20-116.CID20-1063.A540_N708.merged_concatenated.vcf",
                        "variant_type": "snvs"
                    },

                ]
        }
    }


def get_clinical_sample_concordance_in_lod_oc120plus_rep1(snp_type=None):
    """
    Get the clinical sample for LOD analysis for OC120plus for rep1
    :param snp_type:  Either snp or both
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        clinical_variant_file = _get_oc120plus_clinical_snp_file()
    elif snp_type == 'indels':
        clinical_variant_file = _get_oc120plus_clinical_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc120plus_image_dir(replicate='rep1'),
            "vaf_regression_plot": {
                "x_axis_label": "OC120plus",
                "y_axis_label": "Clinical Sample",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": "lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/clinical_dilution_sample/2020dez11-oc120-b20-142-cid20-1370-snapshot-gsv2/2020dez11-oc120-b20-142-cid20-1370-snapshot-gsv2.B20-142.CID20-1370.C522_N706.merged_concatenated.vcf",
            "concordance_files":
                [

                    {'horizon_file': clinical_variant_file,
                     'sample_id': '1_oc120plus_clinical_lod_rep1_100%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-291-snapshot/2021mar08-oc120-run1-b21-25-cid21-291-snapshot.B21-25.CID21-291.B541_N701.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '2_oc120plus_clinical_lod_rep1_50%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-292-snapshot/2021mar08-oc120-run1-b21-25-cid21-292-snapshot.B21-25.CID21-292.B542_N702.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '3_oc120plus_clinical_lod_rep1_25%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-293-snapshot/2021mar08-oc120-run1-b21-25-cid21-293-snapshot.B21-25.CID21-293.B543_N703.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '4_oc120plus_clinical_lod_rep1_12.5%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-294-snapshot/2021mar08-oc120-run1-b21-25-cid21-294-snapshot.B21-25.CID21-294.B544_N704.merged_concatenated.vcf'}
                ]
        }
    }


def get_clinical_sample_concordance_in_lod_oc120plus_rep2(snp_type=None):
    """
    Get the clinical sample for LOD analysis for OC120plus for rep2
    :param snp_type:  Either snp or both
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        clinical_variant_file = _get_oc120plus_clinical_snp_file()
    elif snp_type == 'indels':
        clinical_variant_file = _get_oc120plus_clinical_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run2",
            "images_dir": _get_oc120plus_image_dir(replicate='rep2'),
            "vaf_regression_plot": {
                "x_axis_label": "OC120",
                "y_axis_label": "Horizon",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": "lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/clinical_dilution_sample/2020dez11-oc120-b20-142-cid20-1370-snapshot-gsv2/2020dez11-oc120-b20-142-cid20-1370-snapshot-gsv2.B20-142.CID20-1370.C522_N706.merged_concatenated.vcf",
            "concordance_files":
                [
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '1_oc120plus_clinical_lod_rep2_100%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-295-snapshot/2021mar08-oc120-run1-b21-25-cid21-295-snapshot.B21-25.CID21-295.B545_N705.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '2_oc120plus_clinical_lod_rep2_50%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-296-snapshot/2021mar08-oc120-run1-b21-25-cid21-296-snapshot.B21-25.CID21-296.B546_N706.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '3_oc120plus_clinical_lod_rep2_25%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-297-snapshot/2021mar08-oc120-run1-b21-25-cid21-297-snapshot.B21-25.CID21-297.B547_N707.merged_concatenated.vcf'},
                    {'horizon_file': clinical_variant_file,
                     'sample_id': '4_oc120plus_clinical_lod_rep2_12.5%',
                     'variant_type': 'snvs',
                     'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-298-snapshot/2021mar08-oc120-run1-b21-25-cid21-298-snapshot.B21-25.CID21-298.B548_N708.merged_concatenated.vcf'}
                ]
        }
    }

####### HTQC

def get_limit_of_detection_for_horizon_htqc_oc120():
    """The horizon samples LOD HTQC stats oc120, was not run in replicate"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": "images/limit_of_detection",
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-952
                        "sample_id": "1st_HD827",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-952-snapshot/2020set28-oc120-run5-b20-99-cid20-952-snapshot.B20-99.CID20-952.C536_N704.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-952-snapshot/2020set28-oc120-run5-b20-99-cid20-952-snapshot.B20-99.CID20-952.C536_N704.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-953
                        "sample_id": "2nd_50%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-953-snapshot/2020set28-oc120-run5-b20-99-cid20-953-snapshot.B20-99.CID20-953.C537_N705.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-953-snapshot/2020set28-oc120-run5-b20-99-cid20-953-snapshot.B20-99.CID20-953.C537_N705.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-954
                        "sample_id": "3rd_25%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-954-snapshot/2020set28-oc120-run5-b20-99-cid20-954-snapshot.B20-99.CID20-954.C538_N706.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-954-snapshot/2020set28-oc120-run5-b20-99-cid20-954-snapshot.B20-99.CID20-954.C538_N706.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-955
                        "sample_id": "4th_12.5%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-955-snapshot/2020set28-oc120-run5-b20-99-cid20-955-snapshot.B20-99.CID20-955.C539_N707.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-955-snapshot/2020set28-oc120-run5-b20-99-cid20-955-snapshot.B20-99.CID20-955.C539_N707.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-956
                        "sample_id": "5th_6.25%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-956-snapshot/2020set28-oc120-run5-b20-99-cid20-956-snapshot.B20-99.CID20-956.C540_N708.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020set28-oc120-run5-b20-99-cid20-956-snapshot/2020set28-oc120-run5-b20-99-cid20-956-snapshot.B20-99.CID20-956.C540_N708.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                ]
            }
        }
    }


def get_limit_of_detection_for_horizon_htqc_oc12plus_rep1():
    """The horizon samples LOD HTQC stats oc120plus rep1"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc120plus_image_dir(replicate='rep1'),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep1_horizon_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1445-snapshot/2020dez23-oc120-run7-b20-148-cid20-1445-snapshot.B20-148.CID20-1445.B511_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1445-snapshot/2020dez23-oc120-run7-b20-148-cid20-1445-snapshot.B20-148.CID20-1445.B511_N705.coverage.summary.htqc.txt',
                        'sample_id': '1_oc120plus_horizon_lod_rep1_100%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1446-snapshot/2020dez23-oc120-run7-b20-148-cid20-1446-snapshot.B20-148.CID20-1446.B512_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1446-snapshot/2020dez23-oc120-run7-b20-148-cid20-1446-snapshot.B20-148.CID20-1446.B512_N706.coverage.summary.htqc.txt',
                        'sample_id': '2_oc120plus_horizon_lod_rep1_50%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1447-snapshot/2020dez23-oc120-run7-b20-148-cid20-1447-snapshot.B20-148.CID20-1447.B513_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1447-snapshot/2020dez23-oc120-run7-b20-148-cid20-1447-snapshot.B20-148.CID20-1447.B513_N707.coverage.summary.htqc.txt',
                        'sample_id': '3_oc120plus_horizon_lod_rep1_25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1448-snapshot/2020dez23-oc120-run7-b20-148-cid20-1448-snapshot.B20-148.CID20-1448.B514_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1448-snapshot/2020dez23-oc120-run7-b20-148-cid20-1448-snapshot.B20-148.CID20-1448.B514_N708.coverage.summary.htqc.txt',
                        'sample_id': '4_oc120plus_horizon_lod_rep1_12.5%',
                        'sample_type': 'Tumor'}
                ]
            }
        }
    }


def get_limit_of_detection_for_horizon_htqc_oc12plus_rep2():
    """The horizon samples LOD HTQC stats oc120plus rep2"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc120plus_image_dir(replicate='rep2'),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep2_horizon_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1449-snapshot/2020dez23-oc120-run7-b20-148-cid20-1449-snapshot.B20-148.CID20-1449.C537_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1449-snapshot/2020dez23-oc120-run7-b20-148-cid20-1449-snapshot.B20-148.CID20-1449.C537_N701.coverage.summary.htqc.txt',
                        'sample_id': '1_oc120plus_horizon_lod_rep2_100%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1450-snapshot/2020dez23-oc120-run7-b20-148-cid20-1450-snapshot.B20-148.CID20-1450.C538_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1450-snapshot/2020dez23-oc120-run7-b20-148-cid20-1450-snapshot.B20-148.CID20-1450.C538_N702.coverage.summary.htqc.txt',
                        'sample_id': '2_oc120plus_horizon_lod_rep2_50%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1451-snapshot/2020dez23-oc120-run7-b20-148-cid20-1451-snapshot.B20-148.CID20-1451.C539_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1451-snapshot/2020dez23-oc120-run7-b20-148-cid20-1451-snapshot.B20-148.CID20-1451.C539_N703.coverage.summary.htqc.txt',
                        'sample_id': '3_oc120plus_horizon_lod_rep2_25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1452-snapshot/2020dez23-oc120-run7-b20-148-cid20-1452-snapshot.B20-148.CID20-1452.C540_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2020dez23-oc120-run7-b20-148-cid20-1452-snapshot/2020dez23-oc120-run7-b20-148-cid20-1452-snapshot.B20-148.CID20-1452.C540_N704.coverage.summary.htqc.txt',
                        'sample_id': '4_oc120plus_horizon_lod_rep2_12.5%',
                        'sample_type': 'Tumor'}
                ]
            }
        }
    }


def get_limit_of_detection_for_clinical_sample_htqc_oc120_rep1():
    """The clinical samples oc120 LOD HTQC stats for REP2"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc120_image_dir(),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep1_clinical_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1054
                        "sample_id": "1st_Perla",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1054-snapshot/2020out23-oc120-run6-b20-116-cid20-1054-snapshot.B20-116.CID20-1054.C542_N707.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1054-snapshot/2020out23-oc120-run6-b20-116-cid20-1054-snapshot.B20-116.CID20-1054.C542_N707.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1056
                        "sample_id": "2nd_50%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1056-snapshot/2020out23-oc120-run6-b20-116-cid20-1056-snapshot.B20-116.CID20-1056.C543_N708.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1056-snapshot/2020out23-oc120-run6-b20-116-cid20-1056-snapshot.B20-116.CID20-1056.C543_N708.coverage.htqc.tx",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1058
                        "sample_id": "3rd_25%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1058-snapshot/2020out23-oc120-run6-b20-116-cid20-1058-snapshot.B20-116.CID20-1058.A533_N701.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1058-snapshot/2020out23-oc120-run6-b20-116-cid20-1058-snapshot.B20-116.CID20-1058.A533_N701.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1060
                        "sample_id": "4th_12.5%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1060-snapshot/2020out23-oc120-run6-b20-116-cid20-1060-snapshot.B20-116.CID20-1060.A534_N702.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1060-snapshot/2020out23-oc120-run6-b20-116-cid20-1060-snapshot.B20-116.CID20-1060.A534_N702.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1062
                        "sample_id": "5th_6.25%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1062-snapshot/2020out23-oc120-run6-b20-116-cid20-1062-snapshot.B20-116.CID20-1062.A535_N703.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1062-snapshot/2020out23-oc120-run6-b20-116-cid20-1062-snapshot.B20-116.CID20-1062.A535_N703.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                ]
            }
        }
    }


def get_limit_of_detection_for_clinical_sample_htqc_oc120_rep2():
    """The clinical samples oc120 LOD HTQC stats for REP2"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc120_image_dir(),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep2_clinical_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1055
                        "sample_id": "1st_Perla",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1055-snapshot/2020out23-oc120-run6-b20-116-cid20-1055-snapshot.B20-116.CID20-1055.A536_N704.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1055-snapshot/2020out23-oc120-run6-b20-116-cid20-1055-snapshot.B20-116.CID20-1055.A536_N704.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1057
                        "sample_id": "2nd_50%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1057-snapshot/2020out23-oc120-run6-b20-116-cid20-1057-snapshot.B20-116.CID20-1057.A537_N705.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1057-snapshot/2020out23-oc120-run6-b20-116-cid20-1057-snapshot.B20-116.CID20-1057.A537_N705.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1059
                        "sample_id": "3rd_25%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1059-snapshot/2020out23-oc120-run6-b20-116-cid20-1059-snapshot.B20-116.CID20-1059.A538_N706.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1059-snapshot/2020out23-oc120-run6-b20-116-cid20-1059-snapshot.B20-116.CID20-1059.A538_N706.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1061
                        "sample_id": "4th_12.5%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1061-snapshot/2020out23-oc120-run6-b20-116-cid20-1061-snapshot.B20-116.CID20-1061.A539_N707.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1061-snapshot/2020out23-oc120-run6-b20-116-cid20-1061-snapshot.B20-116.CID20-1061.A539_N707.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                    {
                        # http://lims.demo.bostonlighthouse.us/index.php/CID20-1063
                        "sample_id": "5th_6.25%",
                        "qc_htqc_summary_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1063-snapshot/2020out23-oc120-run6-b20-116-cid20-1063-snapshot.B20-116.CID20-1063.A540_N708.coverage.summary.htqc.txt",
                        "qc_htqc_file": "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/2020out23-oc120-run6-b20-116-cid20-1063-snapshot/2020out23-oc120-run6-b20-116-cid20-1063-snapshot.B20-116.CID20-1063.A540_N708.coverage.htqc.txt",
                        "sample_type": "Tumor"
                    },
                ]
            }
        }
    }


def get_limit_of_detection_for_clinical_sample_htqc_oc120plus_rep1():
    """The clinical samples oc120plus LOD HTQC stats for REP1"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc120plus_image_dir(replicate='rep1'),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep1_clinical_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-291-snapshot/2021mar08-oc120-run1-b21-25-cid21-291-snapshot.B21-25.CID21-291.B541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-291-snapshot/2021mar08-oc120-run1-b21-25-cid21-291-snapshot.B21-25.CID21-291.B541_N701.coverage.summary.htqc.txt',
                        'sample_id': '1_oc120plus_clinical_lod_rep1_100%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-292-snapshot/2021mar08-oc120-run1-b21-25-cid21-292-snapshot.B21-25.CID21-292.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-292-snapshot/2021mar08-oc120-run1-b21-25-cid21-292-snapshot.B21-25.CID21-292.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '2_oc120plus_clinical_lod_rep1_50%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-293-snapshot/2021mar08-oc120-run1-b21-25-cid21-293-snapshot.B21-25.CID21-293.B543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-293-snapshot/2021mar08-oc120-run1-b21-25-cid21-293-snapshot.B21-25.CID21-293.B543_N703.coverage.summary.htqc.txt',
                        'sample_id': '3_oc120plus_clinical_lod_rep1_25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-294-snapshot/2021mar08-oc120-run1-b21-25-cid21-294-snapshot.B21-25.CID21-294.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-294-snapshot/2021mar08-oc120-run1-b21-25-cid21-294-snapshot.B21-25.CID21-294.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '4_oc120plus_clinical_lod_rep1_12.5%',
                        'sample_type': 'Tumor'}
                ]
            }
        }
    }


def get_limit_of_detection_for_clinical_sample_htqc_oc120plus_rep2():
    """The clinical samples oc120plus LOD HTQC stats for REP2"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc120plus_image_dir(replicate='rep2'),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep2_clinical_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-295-snapshot/2021mar08-oc120-run1-b21-25-cid21-295-snapshot.B21-25.CID21-295.B545_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-295-snapshot/2021mar08-oc120-run1-b21-25-cid21-295-snapshot.B21-25.CID21-295.B545_N705.coverage.summary.htqc.txt',
                        'sample_id': '1_oc120plus_clinical_lod_rep2_100%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-296-snapshot/2021mar08-oc120-run1-b21-25-cid21-296-snapshot.B21-25.CID21-296.B546_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-296-snapshot/2021mar08-oc120-run1-b21-25-cid21-296-snapshot.B21-25.CID21-296.B546_N706.coverage.summary.htqc.txt',
                        'sample_id': '2_oc120plus_clinical_lod_rep2_50%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-297-snapshot/2021mar08-oc120-run1-b21-25-cid21-297-snapshot.B21-25.CID21-297.B547_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-297-snapshot/2021mar08-oc120-run1-b21-25-cid21-297-snapshot.B21-25.CID21-297.B547_N707.coverage.summary.htqc.txt',
                        'sample_id': '3_oc120plus_clinical_lod_rep2_25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-298-snapshot/2021mar08-oc120-run1-b21-25-cid21-298-snapshot.B21-25.CID21-298.B548_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus/Limit_of_detection/2021mar08-oc120-run1-b21-25-cid21-298-snapshot/2021mar08-oc120-run1-b21-25-cid21-298-snapshot.B21-25.CID21-298.B548_N708.coverage.summary.htqc.txt',
                        'sample_id': '4_oc120plus_clinical_lod_rep2_12.5%',
                        'sample_type': 'Tumor'}
                ]
            }
        }
    }
