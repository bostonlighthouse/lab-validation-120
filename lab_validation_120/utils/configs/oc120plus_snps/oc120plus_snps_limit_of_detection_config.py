"""Config for the limit of detection experiment

HD827 - 100% positiva	POE20-67	POE20-78	CID21-2794
HD827 - 50% positiva + NA12878 - 50% negativa	POE20-67	POE20-78	CID21-2795
HD827 - 25% positiva + NA12878 - 75% negativa	POE20-67	POE20-78	CID21-2796
HD827 - 12,5% positiva + NA12878 - 87,5% negativa	POE20-67	POE20-78	CID21-2797
HD827 - 6,25% positiva + NA12878 - 93,75% negativa	POE20-67	POE20-78	CID21-2798
HD827 - 3,12% positiva + NA12878 - 96,88% negativa	POE20-67	POE20-78	CID21-2799

POE21-305 - 100% positiva	POE21-305	POE21-868	CID21-2800
POE21-305 - 50% positiva + POE21-868 - 50% negativa	POE21-305	POE21-868	CID21-2801
POE21-305 - 25% positiva + POE21-868 - 75% negativa	POE21-305	POE21-868	CID21-2802
POE21-305 - 12,5% positiva + POE21-868 - 87,5% negativa	POE21-305	POE21-868	CID21-2803
POE21-305 - 6,25% positiva + POE21-868 - 93,75% negativa	POE21-305	POE21-868	CID21-2804
POE21-305 - 3,12% positiva + POE21-868 - 96,88% negativa	POE21-305	POE21-868	CID21-2805

"""

def _get_oc120_clinical_snp_file():  # TODO FIX
    """Just return the string location of this file for oc120"""
    return "lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical/clinical_sample.txt"


def _get_oc120_clinical_indel_file(): # TODO FIX
    """Just return the string location of this file for oc120"""
    assert "Did not implement the file for insertions and deletions, just use both"
    #return "" # "lab_validation_120/internal_data_sources/validation_120/Limit_of_detection/perla_clinical_sample.txt"


def _get_oc120plus_snps_image_dir():
    """Just return the string of where to store the images for oc120"""
    return "images/limit_of_detection_oc120plus_snps"


def get_horizon_concordance_in_lod_oc120plus_snps(snp_type=None):
    """
    Get the clinical sample for LOD analysis for Horizon GS120plus + SNPs
    :param snp_type:  Either snp or indels
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        horizon_file = "lab_validation_120/external_data_sources/horizon_gold_sets/hd827_for_lod.txt"
    elif snp_type == 'indels':
        horizon_file = "lab_validation_120/external_data_sources/horizon_gold_sets/hd827_indels_only_for_lod.txt"
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc120plus_snps_image_dir(),
            "vaf_regression_plot": {
                "x_axis_label": "OC120plus_snps",
                "y_axis_label": "Horizon",
                "x_value_key": "120plus_snps_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "concordance_files":
                [{'horizon_file': horizon_file,
                  'sample_id': '1_gs180_snps/gs180_snps_hd_lod_100%',
                  'variant_type': 'snvs',
                  'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2794-snapshot/2021dez23-oc180-run6-b21-181-cid21-2794-snapshot.B21-181.CID21-2794.B525_N701.merged_concatenated.vcf'},
                 {'horizon_file': horizon_file,
                  'sample_id': '2_gs180_snps/gs180_snps_hd_lod_50%',
                  'variant_type': 'snvs',
                  'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2795-snapshot/2021dez23-oc180-run6-b21-181-cid21-2795-snapshot.B21-181.CID21-2795.B526_N702.merged_concatenated.vcf'},
                 {'horizon_file': horizon_file,
                  'sample_id': '3_gs180_snps/gs180_snps_hd_lod_25%',
                  'variant_type': 'snvs',
                  'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2796-snapshot/2021dez23-oc180-run6-b21-181-cid21-2796-snapshot.B21-181.CID21-2796.B527_N703.merged_concatenated.vcf'},
                 {'horizon_file': horizon_file,
                  'sample_id': '4_gs180_snps/gs180_snps_hd_lod_12.5%',
                  'variant_type': 'snvs',
                  'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2797-snapshot/2021dez23-oc180-run6-b21-181-cid21-2797-snapshot.B21-181.CID21-2797.B528_N704.merged_concatenated.vcf'},
                 {'horizon_file': horizon_file,
                  'sample_id': '5_gs180_snps/gs180_snps_hd_lod_6.25%',
                  'variant_type': 'snvs',
                  'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2798-snapshot/2021dez23-oc180-run6-b21-181-cid21-2798-snapshot.B21-181.CID21-2798.B529_N705.merged_concatenated.vcf'},
                 {'horizon_file': horizon_file,
                  'sample_id': '6_gs180_snps/gs180_snps_hd_lod_3.125%',
                  'variant_type': 'snvs',
                  'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2799-snapshot/2021dez23-oc180-run6-b21-181-cid21-2799-snapshot.B21-181.CID21-2799.B530_N706.merged_concatenated.vcf'}]

        }
    }


def get_clinical_sample_concordance_in_lod_oc120plus_snps_rep1(snp_type=None):
    """
    Get the clinical sample for LOD analysis for OC120plus + SNPs for rep1
    :param snp_type:  Either snp or both
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        clinical_variant_file = _get_oc120_clinical_snp_file()
    elif snp_type == 'indels':
        clinical_variant_file = _get_oc120_clinical_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc120plus_snps_image_dir(),
            "vaf_regression_plot": {
                "x_axis_label": "OC120",
                "y_axis_label": "Horizon",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": "lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical/dilution_sample/2021dez07-oc180-r59--b21-173-cid21-2641-snapshot/2021dez07-oc180-r59--b21-173-cid21-2641-snapshot.B21-173.CID21-2641.C535_N708.merged_concatenated.vcf",
            "concordance_files": [
                {'horizon_file': clinical_variant_file,
                 'sample_id': '1_gs180_snps_clinical_lod_rep1_100%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-305-snapshot/2022fev08-oc180-run0-b22-18-cid22-305-snapshot.B22-18.CID22-305.B533_N701.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '2_gs180_snps_clinical_lod_rep1_50%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-306-snapshot/2022fev08-oc180-run0-b22-18-cid22-306-snapshot.B22-18.CID22-306.B534_N702.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '3_gs180_snps_clinical_lod_rep1_25%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-307-snapshot/2022fev08-oc180-run0-b22-18-cid22-307-snapshot.B22-18.CID22-307.B535_N703.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '4_gs180_snps_clinical_lod_rep1_12.5%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-308-snapshot/2022fev08-oc180-run0-b22-18-cid22-308-snapshot.B22-18.CID22-308.B536_N704.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '5_gs180_snps_clinical_lod_rep1_6.25%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-309-snapshot/2022fev08-oc180-run0-b22-18-cid22-309-snapshot.B22-18.CID22-309.B537_N705.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '6_gs180_snps_clinical_lod_rep1_3.125%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-310-snapshot/2022fev08-oc180-run0-b22-18-cid22-310-snapshot.B22-18.CID22-310.B538_N706.merged_concatenated.vcf'}]

        }
    }


def get_clinical_sample_concordance_in_lod_oc120plus_snps_rep2(snp_type=None):
    """
    Get the clinical sample for LOD analysis for OC120plus + SNPs for rep1
    :param snp_type:  Either snp or both
    """
    snp_type = snp_type.lower()
    if snp_type == 'both':
        clinical_variant_file = _get_oc120_clinical_snp_file()
    elif snp_type == 'indels':
        clinical_variant_file = _get_oc120_clinical_indel_file()
    else:
        raise Exception(f"Did not implement snp_type = {snp_type}")

    return {
        "concordance_lod_samples": {
            "run": "run1",
            "images_dir": _get_oc120plus_snps_image_dir(),
            "vaf_regression_plot": {
                "x_axis_label": "OC120",
                "y_axis_label": "Horizon",
                "x_value_key": "120_vaf",
                "y_value_key": "horizon_vaf",
                "df_value1": "ALT_AF",
                "df_value2": "VAF",
                "value_plotted": "VAF"
            },
            "vaf_threshold": 0.0,
            "depth_threshold": 0,
            "dilution_sample_vcf": "lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical/dilution_sample/2021dez07-oc180-r59--b21-173-cid21-2641-snapshot/2021dez07-oc180-r59--b21-173-cid21-2641-snapshot.B21-173.CID21-2641.C535_N708.merged_concatenated.vcf",
            "concordance_files": [

                {'horizon_file': clinical_variant_file,
                 'sample_id': '1_gs180_snps_clinical_lod_rep2_100%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-311-snapshot/2022fev08-oc180-run0-b22-18-cid22-311-snapshot.B22-18.CID22-311.B539_N707.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '2_gs180_snps_clinical_lod_rep2_50%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-312-snapshot/2022fev08-oc180-run0-b22-18-cid22-312-snapshot.B22-18.CID22-312.B540_N708.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '3_gs180_snps_clinical_lod_rep2_25%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-313-snapshot/2022fev08-oc180-run0-b22-18-cid22-313-snapshot.B22-18.CID22-313.B541_N701.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '4_gs180_snps_clinical_lod_rep2_12.5%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-314-snapshot/2022fev08-oc180-run0-b22-18-cid22-314-snapshot.B22-18.CID22-314.B542_N702.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '5_gs180_snps_clinical_lod_rep2_6.25%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-315-snapshot/2022fev08-oc180-run0-b22-18-cid22-315-snapshot.B22-18.CID22-315.B543_N703.merged_concatenated.vcf'},
                {'horizon_file': clinical_variant_file,
                 'sample_id': '6_gs180_snps_clinical_lod_rep2_3.125%',
                 'variant_type': 'snvs',
                 'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-316-snapshot/2022fev08-oc180-run0-b22-18-cid22-316-snapshot.B22-18.CID22-316.B544_N704.merged_concatenated.vcf'}

            ]

        }
    }



####### HTQC

def get_limit_of_detection_for_horizon_htqc_oc120plus_snps():
    """The horizon samples LOD HTQC stats oc120plus + SNPs, was not run in replicate"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc120plus_snps_image_dir(),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2794-snapshot/2021dez23-oc180-run6-b21-181-cid21-2794-snapshot.B21-181.CID21-2794.B525_N701.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2794-snapshot/2021dez23-oc180-run6-b21-181-cid21-2794-snapshot.B21-181.CID21-2794.B525_N701.coverage.summary.htqc.txt',
                         'sample_id': '1_gs180_snps/gs180_snps_hd_lod_100%',
                         'sample_type': 'Tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2795-snapshot/2021dez23-oc180-run6-b21-181-cid21-2795-snapshot.B21-181.CID21-2795.B526_N702.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2795-snapshot/2021dez23-oc180-run6-b21-181-cid21-2795-snapshot.B21-181.CID21-2795.B526_N702.coverage.summary.htqc.txt',
                         'sample_id': '2_gs180_snps/gs180_snps_hd_lod_50%',
                         'sample_type': 'Tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2796-snapshot/2021dez23-oc180-run6-b21-181-cid21-2796-snapshot.B21-181.CID21-2796.B527_N703.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2796-snapshot/2021dez23-oc180-run6-b21-181-cid21-2796-snapshot.B21-181.CID21-2796.B527_N703.coverage.summary.htqc.txt',
                         'sample_id': '3_gs180_snps/gs180_snps_hd_lod_25%',
                         'sample_type': 'Tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2797-snapshot/2021dez23-oc180-run6-b21-181-cid21-2797-snapshot.B21-181.CID21-2797.B528_N704.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2797-snapshot/2021dez23-oc180-run6-b21-181-cid21-2797-snapshot.B21-181.CID21-2797.B528_N704.coverage.summary.htqc.txt',
                         'sample_id': '4_gs180_snps/gs180_snps_hd_lod_12.5%',
                         'sample_type': 'Tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2798-snapshot/2021dez23-oc180-run6-b21-181-cid21-2798-snapshot.B21-181.CID21-2798.B529_N705.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2798-snapshot/2021dez23-oc180-run6-b21-181-cid21-2798-snapshot.B21-181.CID21-2798.B529_N705.coverage.summary.htqc.txt',
                         'sample_id': '5_gs180_snps/gs180_snps_hd_lod_6.25%',
                         'sample_type': 'Tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2799-snapshot/2021dez23-oc180-run6-b21-181-cid21-2799-snapshot.B21-181.CID21-2799.B530_N706.coverage.htqc.txt',
                         'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/horizon/2021dez23-oc180-run6-b21-181-cid21-2799-snapshot/2021dez23-oc180-run6-b21-181-cid21-2799-snapshot.B21-181.CID21-2799.B530_N706.coverage.summary.htqc.txt',
                         'sample_id': '6_gs180_snps/gs180_snps_hd_lod_3.125%',
                         'sample_type': 'Tumor'}
                ]
            }
        }
    }


def get_limit_of_detection_for_clinical_sample_htqc_oc120plus_snps_rep1():
    """The clinical samples oc120plus + SNPs LOD HTQC stats for REP1.. Only did on REP"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc120plus_snps_image_dir(),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep1_clinical_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-305-snapshot/2022fev08-oc180-run0-b22-18-cid22-305-snapshot.B22-18.CID22-305.B533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-305-snapshot/2022fev08-oc180-run0-b22-18-cid22-305-snapshot.B22-18.CID22-305.B533_N701.coverage.summary.htqc.txt',
                        'sample_id': '1_gs180_snps_clinical_lod_rep1_100%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-306-snapshot/2022fev08-oc180-run0-b22-18-cid22-306-snapshot.B22-18.CID22-306.B534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-306-snapshot/2022fev08-oc180-run0-b22-18-cid22-306-snapshot.B22-18.CID22-306.B534_N702.coverage.summary.htqc.txt',
                        'sample_id': '2_gs180_snps_clinical_lod_rep1_50%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-307-snapshot/2022fev08-oc180-run0-b22-18-cid22-307-snapshot.B22-18.CID22-307.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-307-snapshot/2022fev08-oc180-run0-b22-18-cid22-307-snapshot.B22-18.CID22-307.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '3_gs180_snps_clinical_lod_rep1_25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-308-snapshot/2022fev08-oc180-run0-b22-18-cid22-308-snapshot.B22-18.CID22-308.B536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-308-snapshot/2022fev08-oc180-run0-b22-18-cid22-308-snapshot.B22-18.CID22-308.B536_N704.coverage.summary.htqc.txt',
                        'sample_id': '4_gs180_snps_clinical_lod_rep1_12.5%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-309-snapshot/2022fev08-oc180-run0-b22-18-cid22-309-snapshot.B22-18.CID22-309.B537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-309-snapshot/2022fev08-oc180-run0-b22-18-cid22-309-snapshot.B22-18.CID22-309.B537_N705.coverage.summary.htqc.txt',
                        'sample_id': '5_gs180_snps_clinical_lod_rep1_6.25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-310-snapshot/2022fev08-oc180-run0-b22-18-cid22-310-snapshot.B22-18.CID22-310.B538_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-310-snapshot/2022fev08-oc180-run0-b22-18-cid22-310-snapshot.B22-18.CID22-310.B538_N706.coverage.summary.htqc.txt',
                        'sample_id': '6_gs180_snps_clinical_lod_rep1_3.125%',
                        'sample_type': 'Tumor'}
                ]
            }
        }
    }

def get_limit_of_detection_for_clinical_sample_htqc_oc120plus_snps_rep2():
    """The clinical samples oc120plus + SNPs LOD HTQC stats for REP1.. Only did on REP"""

    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        "limit_of_detection": {
            "run": "run_unk",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": _get_oc120plus_snps_image_dir(),
            "input_files": {
                # TODO update when ready with the correct samples
                "group_id": "rep1_clinical_lod",
                "chart_title": "Limit of Detection",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-311-snapshot/2022fev08-oc180-run0-b22-18-cid22-311-snapshot.B22-18.CID22-311.B539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-311-snapshot/2022fev08-oc180-run0-b22-18-cid22-311-snapshot.B22-18.CID22-311.B539_N707.coverage.summary.htqc.txt',
                        'sample_id': '1_gs180_snps_clinical_lod_rep2_100%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-312-snapshot/2022fev08-oc180-run0-b22-18-cid22-312-snapshot.B22-18.CID22-312.B540_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-312-snapshot/2022fev08-oc180-run0-b22-18-cid22-312-snapshot.B22-18.CID22-312.B540_N708.coverage.summary.htqc.txt',
                        'sample_id': '2_gs180_snps_clinical_lod_rep2_50%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-313-snapshot/2022fev08-oc180-run0-b22-18-cid22-313-snapshot.B22-18.CID22-313.B541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-313-snapshot/2022fev08-oc180-run0-b22-18-cid22-313-snapshot.B22-18.CID22-313.B541_N701.coverage.summary.htqc.txt',
                        'sample_id': '3_gs180_snps_clinical_lod_rep2_25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-314-snapshot/2022fev08-oc180-run0-b22-18-cid22-314-snapshot.B22-18.CID22-314.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-314-snapshot/2022fev08-oc180-run0-b22-18-cid22-314-snapshot.B22-18.CID22-314.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '4_gs180_snps_clinical_lod_rep2_12.5%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-315-snapshot/2022fev08-oc180-run0-b22-18-cid22-315-snapshot.B22-18.CID22-315.B543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-315-snapshot/2022fev08-oc180-run0-b22-18-cid22-315-snapshot.B22-18.CID22-315.B543_N703.coverage.summary.htqc.txt',
                        'sample_id': '5_gs180_snps_clinical_lod_rep2_6.25%',
                        'sample_type': 'Tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-316-snapshot/2022fev08-oc180-run0-b22-18-cid22-316-snapshot.B22-18.CID22-316.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/lod/clinical//2022fev08-oc180-run0-b22-18-cid22-316-snapshot/2022fev08-oc180-run0-b22-18-cid22-316-snapshot.B22-18.CID22-316.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '6_gs180_snps_clinical_lod_rep2_3.125%',
                        'sample_type': 'Tumor'}
                ]
            }
        }
    }



