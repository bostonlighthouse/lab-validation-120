"""Get the VCF / QC files for the GS180 vs GS180 + SNPs - 11 re-sequenced samples"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_oc120plus_vs_oc120plus_snps_reproducibility_11_resequenced_samples():
    """
    Get the VCF files for the GS180 vs GS180 + SNPs - 11 resequenced samples
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/oc120plus_vs_oc120plus_snps_11_resequenced_samples",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    'scratch/oc120plus_vs_oc120plus_snps_11_resequenced_samples/oc120plus_vs_oc120plus_snps_11_resequenced_samples_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                    'scratch/oc120plus_vs_oc120plus_snps_11_resequenced_samples/oc120plus_vs_oc120plus_snps_11_resequenced_samples_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                ],
                "indel": [
                    'scratch/oc120plus_vs_oc120plus_snps_11_resequenced_samples/oc120plus_vs_oc120plus_snps_11_resequenced_samples_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                    'scratch/oc120plus_vs_oc120plus_snps_11_resequenced_samples/oc120plus_vs_oc120plus_snps_11_resequenced_samples_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                ]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file":  [
                        'scratch/oc120plus_vs_oc120plus_snps_11_resequenced_samples/oc120plus_vs_oc120plus_snps_11_resequenced_samples_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                        'scratch/oc120plus_vs_oc120plus_snps_11_resequenced_samples/oc120plus_vs_oc120plus_snps_11_resequenced_samples_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        'scratch/oc120plus_vs_oc120plus_snps_11_resequenced_samples/oc120plus_vs_oc120plus_snps_11_resequenced_samples_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                        'scratch/oc120plus_vs_oc120plus_snps_11_resequenced_samples/oc120plus_vs_oc120plus_snps_11_resequenced_samples_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "GS180 SNPs",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "GS180 SNPs",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "GS180 SNPs",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            # TODO remove these, and have the program be called with those parameters
            #"vaf_threshold": 10.0,
            #"depth_threshold": 100,
            # Go over and look at the samples below, in the concordance_files
            # the oc120plus is the first VCF in each entry
            # oc120plus + SNPs is the second VCF  in each entry
            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid21-557',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-30-oc180-run-b21-53-cid21-557-snapshot/2021-04-30-oc180-run-b21-53-cid21-557-snapshot.B21-53.CID21-557.B507_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '1b-cid21-2374',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2374-snapshot/2021nov04-gs180-r04--b21-158-cid21-2374-snapshot.B21-158.CID21-2374.B530_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample2': [{'sample_id': '2a-cid21-1555',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot.B21-110.CID21-1555.A544_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '2b-cid21-2375',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2375-snapshot/2021nov04-gs180-r04--b21-158-cid21-2375-snapshot.B21-158.CID21-2375.B531_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample3': [{'sample_id': '3a-cid21-1112',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot.B21-89.CID21-1112.A510_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '3b-cid21-2376',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2376-snapshot/2021nov04-gs180-r04--b21-158-cid21-2376-snapshot.B21-158.CID21-2376.B532_N708.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample4': [{'sample_id': '4a-cid21-1558',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot.B21-110.CID21-1558.A546_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '4b-cid21-2377',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2377-snapshot/2021nov04-gs180-r04--b21-158-cid21-2377-snapshot.B21-158.CID21-2377.B533_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample5': [{'sample_id': '5a-cid21-448',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-08-oc120-run-b21-45-cid21-448-snapshot/2021-04-08-oc120-run-b21-45-cid21-448-snapshot.B21-45.CID21-448.B512_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '5b-cid21-2378',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2378-snapshot/2021nov04-gs180-r04--b21-158-cid21-2378-snapshot.B21-158.CID21-2378.B534_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample6': [{'sample_id': '6a-cid21-1126',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot.B21-89.CID21-1126.A515_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '6b-cid21-2379',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2379-snapshot/2021nov04-gs180-r04--b21-158-cid21-2379-snapshot.B21-158.CID21-2379.B535_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample7': [{'sample_id': '7a-cid21-505',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-16-gs180-run-b21-47-cid21-505-snapshot/2021-04-16-gs180-run-b21-47-cid21-505-snapshot.B21-47.CID21-505.B536_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '7b-cid21-2380',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2380-snapshot/2021nov04-gs180-r04--b21-158-cid21-2380-snapshot.B21-158.CID21-2380.B536_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample8': [{'sample_id': '8a-cid21-551',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-30-oc180-run-b21-55-cid21-551-snapshot/2021-04-30-oc180-run-b21-55-cid21-551-snapshot.B21-55.CID21-551.B544_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '8b-cid21-2381',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2381-snapshot/2021nov04-gs180-r04--b21-158-cid21-2381-snapshot.B21-158.CID21-2381.B537_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample9': [{'sample_id': '9a-cid21-765',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-05-21-oc180-run-b21-67-cid21-765-snapshot/2021-05-21-oc180-run-b21-67-cid21-765-snapshot.B21-67.CID21-765.B534_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '9b-cid21-2382',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2382-snapshot/2021nov04-gs180-r04--b21-158-cid21-2382-snapshot.B21-158.CID21-2382.B538_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample10': [{'sample_id': '10a-cid21-581',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-30-oc180-run-b21-53-cid21-581-snapshot/2021-04-30-oc180-run-b21-53-cid21-581-snapshot.B21-53.CID21-581.B509_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '10b-cid21-2383',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2383-snapshot/2021nov04-gs180-r04--b21-158-cid21-2383-snapshot.B21-158.CID21-2383.B539_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample11': [{'sample_id': '11a-cid21-1108',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot.B21-89.CID21-1108.A509_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '11b-cid21-2384',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2384-snapshot/2021nov04-gs180-r04--b21-158-cid21-2384-snapshot.B21-158.CID21-2384.B540_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}]
            }
        }
    }


def get_oc120plus_vs_oc120plus_snps_htqc_plots_11_resequenced_samples():
    """
    Get the QC files for the GS180 vs GS180 + SNPs - 11 re-sequenced samples
    """

    key_2_use = 'oc120plus_vs_oc120plus_snps_11_resequenced_samples'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS180 vs GS180 SNPs - 11 re-sequenced Samples",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-30-oc180-run-b21-53-cid21-557-snapshot/2021-04-30-oc180-run-b21-53-cid21-557-snapshot.B21-53.CID21-557.B507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-30-oc180-run-b21-53-cid21-557-snapshot/2021-04-30-oc180-run-b21-53-cid21-557-snapshot.B21-53.CID21-557.B507_N707.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid21-557',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2374-snapshot/2021nov04-gs180-r04--b21-158-cid21-2374-snapshot.B21-158.CID21-2374.B530_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2374-snapshot/2021nov04-gs180-r04--b21-158-cid21-2374-snapshot.B21-158.CID21-2374.B530_N706.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid21-2374',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot.B21-110.CID21-1555.A544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot/2021ago23-oc180-run0-b21-110-cid21-1555-snapshot.B21-110.CID21-1555.A544_N704.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid21-1555',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2375-snapshot/2021nov04-gs180-r04--b21-158-cid21-2375-snapshot.B21-158.CID21-2375.B531_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2375-snapshot/2021nov04-gs180-r04--b21-158-cid21-2375-snapshot.B21-158.CID21-2375.B531_N707.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid21-2375',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot.B21-89.CID21-1112.A510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot/2021-07-07-oc180-run-b21-89-cid21-1112-snapshot.B21-89.CID21-1112.A510_N702.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid21-1112',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2376-snapshot/2021nov04-gs180-r04--b21-158-cid21-2376-snapshot.B21-158.CID21-2376.B532_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2376-snapshot/2021nov04-gs180-r04--b21-158-cid21-2376-snapshot.B21-158.CID21-2376.B532_N708.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid21-2376',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot.B21-110.CID21-1558.A546_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot/2021ago23-oc180-run0-b21-110-cid21-1558-snapshot.B21-110.CID21-1558.A546_N706.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid21-1558',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2377-snapshot/2021nov04-gs180-r04--b21-158-cid21-2377-snapshot.B21-158.CID21-2377.B533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2377-snapshot/2021nov04-gs180-r04--b21-158-cid21-2377-snapshot.B21-158.CID21-2377.B533_N701.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid21-2377',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-08-oc120-run-b21-45-cid21-448-snapshot/2021-04-08-oc120-run-b21-45-cid21-448-snapshot.B21-45.CID21-448.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-08-oc120-run-b21-45-cid21-448-snapshot/2021-04-08-oc120-run-b21-45-cid21-448-snapshot.B21-45.CID21-448.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid21-448',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2378-snapshot/2021nov04-gs180-r04--b21-158-cid21-2378-snapshot.B21-158.CID21-2378.B534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2378-snapshot/2021nov04-gs180-r04--b21-158-cid21-2378-snapshot.B21-158.CID21-2378.B534_N702.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid21-2378',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot.B21-89.CID21-1126.A515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot/2021-07-07-oc180-run-b21-89-cid21-1126-snapshot.B21-89.CID21-1126.A515_N707.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid21-1126',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2379-snapshot/2021nov04-gs180-r04--b21-158-cid21-2379-snapshot.B21-158.CID21-2379.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2379-snapshot/2021nov04-gs180-r04--b21-158-cid21-2379-snapshot.B21-158.CID21-2379.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid21-2379',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-16-gs180-run-b21-47-cid21-505-snapshot/2021-04-16-gs180-run-b21-47-cid21-505-snapshot.B21-47.CID21-505.B536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-16-gs180-run-b21-47-cid21-505-snapshot/2021-04-16-gs180-run-b21-47-cid21-505-snapshot.B21-47.CID21-505.B536_N704.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid21-505',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2380-snapshot/2021nov04-gs180-r04--b21-158-cid21-2380-snapshot.B21-158.CID21-2380.B536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2380-snapshot/2021nov04-gs180-r04--b21-158-cid21-2380-snapshot.B21-158.CID21-2380.B536_N704.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid21-2380',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-30-oc180-run-b21-55-cid21-551-snapshot/2021-04-30-oc180-run-b21-55-cid21-551-snapshot.B21-55.CID21-551.B544_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-30-oc180-run-b21-55-cid21-551-snapshot/2021-04-30-oc180-run-b21-55-cid21-551-snapshot.B21-55.CID21-551.B544_N704.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid21-551',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2381-snapshot/2021nov04-gs180-r04--b21-158-cid21-2381-snapshot.B21-158.CID21-2381.B537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2381-snapshot/2021nov04-gs180-r04--b21-158-cid21-2381-snapshot.B21-158.CID21-2381.B537_N705.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid21-2381',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-05-21-oc180-run-b21-67-cid21-765-snapshot/2021-05-21-oc180-run-b21-67-cid21-765-snapshot.B21-67.CID21-765.B534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-05-21-oc180-run-b21-67-cid21-765-snapshot/2021-05-21-oc180-run-b21-67-cid21-765-snapshot.B21-67.CID21-765.B534_N702.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid21-765',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2382-snapshot/2021nov04-gs180-r04--b21-158-cid21-2382-snapshot.B21-158.CID21-2382.B538_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2382-snapshot/2021nov04-gs180-r04--b21-158-cid21-2382-snapshot.B21-158.CID21-2382.B538_N706.coverage.summary.htqc.txt',
                        'sample_id': '9b-cid21-2382',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-30-oc180-run-b21-53-cid21-581-snapshot/2021-04-30-oc180-run-b21-53-cid21-581-snapshot.B21-53.CID21-581.B509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-04-30-oc180-run-b21-53-cid21-581-snapshot/2021-04-30-oc180-run-b21-53-cid21-581-snapshot.B21-53.CID21-581.B509_N701.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid21-581',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2383-snapshot/2021nov04-gs180-r04--b21-158-cid21-2383-snapshot.B21-158.CID21-2383.B539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2383-snapshot/2021nov04-gs180-r04--b21-158-cid21-2383-snapshot.B21-158.CID21-2383.B539_N707.coverage.summary.htqc.txt',
                        'sample_id': '10b-cid21-2383',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot.B21-89.CID21-1108.A509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot/2021-07-07-oc180-run-b21-89-cid21-1108-snapshot.B21-89.CID21-1108.A509_N701.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid21-1108',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2384-snapshot/2021nov04-gs180-r04--b21-158-cid21-2384-snapshot.B21-158.CID21-2384.B540_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/validation_120plus_with_snps/validation/sequenced_with_oc180snps/2021nov04-gs180-r04--b21-158-cid21-2384-snapshot/2021nov04-gs180-r04--b21-158-cid21-2384-snapshot.B21-158.CID21-2384.B540_N708.coverage.summary.htqc.txt',
                        'sample_id': '11b-cid21-2384',
                        'sample_type': ''}
                ]
            }
        }
    }
