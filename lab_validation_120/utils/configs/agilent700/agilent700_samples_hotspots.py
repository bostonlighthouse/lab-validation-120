"""Agilent 700 validation - comparison of the Agilent vs GS395 vs G180 for the hotspots"""


def get_agilent700_vs_oc180_samples_hotspots():

    # The Agilent 700 samples used vs the GS180
    return {
        "amplicon_metrics": {
            "run": "Agilent700_samples_hotspots",
            "images_dir": "images/agilent700_vs_gs180_samples_hotspots/",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot.B23-115.CID23-1680.A501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1680',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot.B23-115.CID23-1704.A502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1704',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot.B23-115.CID23-1705.A503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1705',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot.B23-115.CID23-1706.A504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1706',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot.B23-115.CID23-1683.A508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1683',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot.B23-115.CID23-1685.A510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1685',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot.B23-115.CID23-1686.A511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1686',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot.B23-115.CID23-1687.A512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1687',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot.B23-115.CID23-1689.A514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1689',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot.B23-115.CID23-1690.A515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1690',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot.B23-115.CID23-1684.A509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1684',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot.B23-115.CID23-1691.A516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1691',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot.B23-115.CID23-1692.A517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1692',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot.B23-115.CID23-1708.A518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1708',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot.B23-115.CID23-1694.A521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1694',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot.B23-115.CID23-1696.A522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1696',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot.B23-115.CID23-1695.A523_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1695',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot.B23-115.CID23-1682.A505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1682',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot.B23-121.CID23-1833.A514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1833',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot.B23-121.CID23-1832.A515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1832',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot.B23-121.CID23-1834.A516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1834',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot.B23-121.CID23-1838.A519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1838',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot.B23-121.CID23-1837.A520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1837',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot.B23-121.CID23-1839.A521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1839',
                        'sample_type': 'tumor'
                    }


                ]
            }
        }
    }


def get_oc180_samples_used_with_agilent700_samples_hotspots():

    # The GS180 samples samples used vs the Agilent800
    return {
        "amplicon_metrics": {
            "run": "gs180_samples_hotspots",
            "images_dir": "images/agilent700_vs_gs180_samples_hotspots/",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2023jun02-gs180-run1-b23-95-cid23-1351-snapshot/2023jun02-gs180-run1-b23-95-cid23-1351-snapshot.B23-95.CID23-1351.B502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1351',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2023maio22-gs180-run-b23-87-cid23-1215-snapshot/2023maio22-gs180-run-b23-87-cid23-1215-snapshot.B23-87.CID23-1215.B505_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1215',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2023jun02-gs180-run1-b23-95-cid23-1313-snapshot/2023jun02-gs180-run1-b23-95-cid23-1313-snapshot.B23-95.CID23-1313.C543_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1313',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2023jun02-gs180-run1-b23-95-cid23-1319-snapshot/2023jun02-gs180-run1-b23-95-cid23-1319-snapshot.B23-95.CID23-1319.C545_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1319',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2023jun02-gs180-run1-b23-95-cid23-1375-snapshot/2023jun02-gs180-run1-b23-95-cid23-1375-snapshot.B23-95.CID23-1375.B504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1375',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2022abr15-gs180-run1-b23-70-cid23-989-snapshot/2022abr15-gs180-run1-b23-70-cid23-989-snapshot.B23-70.CID23-989.B538_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-989',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2022abr15-gs180-run1-b23-70-cid23-1020-snapshot/2022abr15-gs180-run1-b23-70-cid23-1020-snapshot.B23-70.CID23-1020.B537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1020',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2023mar27-gs180-run1-b23-58-cid23-825-snapshot/2023mar27-gs180-run1-b23-58-cid23-825-snapshot.B23-58.CID23-825.B513_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-825',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2023mar17-gs180-run1-b23-53-cid23-757-snapshot/2023mar17-gs180-run1-b23-53-cid23-757-snapshot.B23-53.CID23-757.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-757',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2022mai09-gs180-run1-b22-59-cid22-947-snapshot/2022mai09-gs180-run1-b22-59-cid22-947-snapshot.B22-59.CID22-947.B530_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-947',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2021-04-23-gs180-run-b21-51-cid21-494-snapshot/2021-04-23-gs180-run-b21-51-cid21-494-snapshot.B21-51.CID21-494.B533_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-494',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2022abr08-gs180-run1-b22-46-cid22-840-snapshot/2022abr08-gs180-run1-b22-46-cid22-840-snapshot.B22-46.CID22-840.B542_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-840',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2022fev02-gs180-run0-b22-17-cid22-283-snapshot/2022fev02-gs180-run0-b22-17-cid22-283-snapshot.B22-17.CID22-283.B547_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-283',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2023maio26-gs180-run-b23-91-cid23-1281-snapshot/2023maio26-gs180-run-b23-91-cid23-1281-snapshot.B23-91.CID23-1281.B539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1281',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2022jan21-gs180-run0-b22-11-cid22-189-snapshot/2022jan21-gs180-run0-b22-11-cid22-189-snapshot.B22-11.CID22-189.B507_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-189',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2021out23-oc180-r53--b21-151-cid21-2159-snapshot/2021out23-oc180-r53--b21-151-cid21-2159-snapshot.B21-151.CID21-2159.B520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-2159',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2021set29-oc180-run4-b21-136-cid21-1978-snapshot/2021set29-oc180-run4-b21-136-cid21-1978-snapshot.B21-136.CID21-1978.C535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1978',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run//2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.hotspots.htqc.txt',
                        'sample_id': 'cid21-1665',
                        'sample_type': 'tumor'},
                    {

                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-05-31-oc180-run-b21-73-cid21-775-snapshot/2021-05-31-oc180-run-b21-73-cid21-775-snapshot.B21-73.CID21-775.A537_N705.hotspots.htqc.txt',
                        'sample_id': 'cid21-775',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-04-23-gs180-run-b21-49-cid21-484-snapshot/2021-04-23-gs180-run-b21-49-cid21-484-snapshot.B21-49.CID21-484.B524_N708.hotspots.htqc.txt',
                        'sample_id': 'cid21-484',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-04-23-gs180-run-b21-49-cid21-486-snapshot/2021-04-23-gs180-run-b21-49-cid21-486-snapshot.B21-49.CID21-486.B539_N707.hotspots.htqc.txt',
                        'sample_id': 'cid21-486',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2022mai13-gs180-run1-b22-62-cid22-1120-snapshot/2022mai13-gs180-run1-b22-62-cid22-1120-snapshot.B22-62.CID22-1120.C528_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-1120',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2022jan21-gs180-run0-b22-11-cid22-82-snapshot/2022jan21-gs180-run0-b22-11-cid22-82-snapshot.B22-11.CID22-82.B510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-82',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-05-26-oc180-run-b21-69-cid21-745-snapshot/2021-05-26-oc180-run-b21-69-cid21-745-snapshot.B21-69.CID21-745.B520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid21-745',
                        'sample_type': 'tumor'
                    }
                ]
            }
        }
    }

def get_agilent700_vs_oc395_samples_hotspots():

    # The Agilent 700 samples used vs the GS395
    return {
        "amplicon_metrics": {
            "run": "Agilent700_samples_hotspots",
            "images_dir": "images/agilent700_vs_gs395_samples_hotspots/",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot.B23-115.CID23-1681.A506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1681',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot.B23-115.CID23-1707.A507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1707',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot.B23-115.CID23-1688.A513_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1688',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot.B23-115.CID23-1709.A519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1709',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot.B23-115.CID23-1693.A520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1693',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot.B23-121.CID23-1820.A501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1820',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot.B23-121.CID23-1821.A502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1821',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot.B23-121.CID23-1864.A503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1864',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot.B23-121.CID23-1822.A504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1822',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot.B23-121.CID23-1823.A505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1823',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot.B23-121.CID23-1824.A507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1824',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot.B23-121.CID23-1825.A508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1825',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot.B23-121.CID23-1826.A509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1826',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot.B23-121.CID23-1827.A510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1827',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot.B23-121.CID23-1828.A511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1828',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot.B23-121.CID23-1829.A512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1829',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot.B23-121.CID23-1830.A513_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1830',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot.B23-121.CID23-1835.A517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1835',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot.B23-121.CID23-1836.A518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1836',
                        'sample_type': 'tumor'
                    }
                ]
            }
        }
    }

def get_oc395_samples_used_with_agilent700_samples_hotspots():

    # The GS395 samples used vs the Agilent700
    return {
        "amplicon_metrics": {
            "run": "gs395_samples_hotspots",
            "images_dir": "images/agilent700_vs_gs395_samples_hotspots/",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run//2023abr17-gsinfinity-b23-72-cid23-1026-snapshot/2023abr17-gsinfinity-b23-72-cid23-1026-snapshot.B23-72.CID23-1026.B544_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1026',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run//2023jun02-gsinfinity-b23-93-cid23-1305-snapshot/2023jun02-gsinfinity-b23-93-cid23-1305-snapshot.B23-93.CID23-1305.C529_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1305',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run//2023mar22-gsinfinity-b23-56-cid23-863-snapshot/2023mar22-gsinfinity-b23-56-cid23-863-snapshot.B23-56.CID23-863.C516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-863',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run//2023maio22-gsinfinit-b23-89-cid23-1251-snapshot/2023maio22-gsinfinit-b23-89-cid23-1251-snapshot.B23-89.CID23-1251.B521_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1251',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run//2023mar28-gsinfinity-b23-60-cid23-881-snapshot/2023mar28-gsinfinity-b23-60-cid23-881-snapshot.B23-60.CID23-881.B543_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-881',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023jun02-gsinfinity-b23-93-cid23-1321-snapshot/2023jun02-gsinfinity-b23-93-cid23-1321-snapshot.B23-93.CID23-1321.C531_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1321',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023jun02-gsinfinity-b23-93-cid23-1341-snapshot/2023jun02-gsinfinity-b23-93-cid23-1341-snapshot.B23-93.CID23-1341.C514_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1341',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1217-snapshot/2023maio12-gsinfinit-b23-84-cid23-1217-snapshot.B23-84.CID23-1217.C516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1217',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio22-gsinfinit-b23-89-cid23-1245-snapshot/2023maio22-gsinfinit-b23-89-cid23-1245-snapshot.B23-89.CID23-1245.B523_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1245',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio22-gsinfinit-b23-89-cid23-1255-snapshot/2023maio22-gsinfinit-b23-89-cid23-1255-snapshot.B23-89.CID23-1255.B522_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1255',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1158-snapshot/2023maio12-gsinfinit-b23-84-cid23-1158-snapshot.B23-84.CID23-1158.C514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1158',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1172-snapshot/2023maio12-gsinfinit-b23-84-cid23-1172-snapshot.B23-84.CID23-1172.C515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1172',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1108-snapshot/2023maio05-gsinfinit-b23-80-cid23-1108-snapshot.B23-80.CID23-1108.C505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1108',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1131-snapshot/2023maio05-gsinfinit-b23-80-cid23-1131-snapshot.B23-80.CID23-1131.C504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1131',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1124-snapshot/2023maio05-gsinfinit-b23-80-cid23-1124-snapshot.B23-80.CID23-1124.C502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1124',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1092-snapshot/2023maio05-gsinfinit-b23-80-cid23-1092-snapshot.B23-80.CID23-1092.C503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1092',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023abr28-gsinfinity-b23-76-cid23-1102-snapshot/2023abr28-gsinfinity-b23-76-cid23-1102-snapshot.B23-76.CID23-1102.B522_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1102',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023mar09-gsinfinity-b23-49-cid23-703-snapshot/2023mar09-gsinfinity-b23-49-cid23-703-snapshot.B23-49.CID23-703.C502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-703',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023abr28-gsinfinity-b23-76-cid23-1100-snapshot/2023abr28-gsinfinity-b23-76-cid23-1100-snapshot.B23-76.CID23-1100.B517_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1100',
                        'sample_type': 'tumor'
                    }
                ]
            }
        }
    }


def get_agilent700_samples_hotspots():

    # The Agilent 700 samples used vs the GS180
    return {
        "amplicon_metrics": {
            "run": "Agilent700_samples_hotspots",
            "images_dir": "images/agilent700_samples_hotspots/",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot.B23-115.CID23-1680.A501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1680',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot.B23-115.CID23-1704.A502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1704',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot.B23-115.CID23-1705.A503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1705',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot.B23-115.CID23-1706.A504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1706',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot.B23-115.CID23-1683.A508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1683',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot.B23-115.CID23-1685.A510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1685',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot.B23-115.CID23-1686.A511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1686',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot.B23-115.CID23-1687.A512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1687',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot.B23-115.CID23-1689.A514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1689',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot.B23-115.CID23-1690.A515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1690',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot.B23-115.CID23-1684.A509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1684',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot.B23-115.CID23-1691.A516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1691',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot.B23-115.CID23-1692.A517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1692',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot.B23-115.CID23-1708.A518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1708',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot.B23-115.CID23-1694.A521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1694',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot.B23-115.CID23-1696.A522_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1696',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot.B23-115.CID23-1695.A523_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1695',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot.B23-115.CID23-1682.A505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1682',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot.B23-121.CID23-1833.A514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1833',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot.B23-121.CID23-1832.A515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1832',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot.B23-121.CID23-1834.A516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1834',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot.B23-121.CID23-1838.A519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1838',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot.B23-121.CID23-1837.A520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1837',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot.B23-121.CID23-1839.A521_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1839',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot.B23-115.CID23-1681.A506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid23-1681',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot.B23-115.CID23-1707.A507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1707',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot.B23-115.CID23-1688.A513_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1688',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot.B23-115.CID23-1709.A519_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1709',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot.B23-115.CID23-1693.A520_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1693',
                        'sample_type': 'tumor'
                    },
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot.B23-121.CID23-1820.A501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1820',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot.B23-121.CID23-1821.A502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1821',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot.B23-121.CID23-1864.A503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1864',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot.B23-121.CID23-1822.A504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1822',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot.B23-121.CID23-1823.A505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1823',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot.B23-121.CID23-1824.A507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid23-1824',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot.B23-121.CID23-1825.A508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid23-1825',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot.B23-121.CID23-1826.A509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1826',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot.B23-121.CID23-1827.A510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1827',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot.B23-121.CID23-1828.A511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid23-1828',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot.B23-121.CID23-1829.A512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid23-1829',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot.B23-121.CID23-1830.A513_N705.hotspots.htqc.txt',
                        'sample_id': 'cid23-1830',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot.B23-121.CID23-1835.A517_N701.hotspots.htqc.txt',
                        'sample_id': 'cid23-1835',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot.B23-121.CID23-1836.A518_N702.hotspots.htqc.txt',
                        'sample_id': 'cid23-1836',
                        'sample_type': 'tumor'
                    }


                ]
            }
        }
    }
