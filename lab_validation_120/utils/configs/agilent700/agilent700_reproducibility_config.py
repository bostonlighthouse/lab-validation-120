"""Get the VCF / QC files for the Agilent vs GS180 and Agilent vs GS395"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_agilent700_vs_oc180_reproducibility():
    """
    Get the VCF files for the Agilent700 vs GS180
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/agilent700_vs_oc180_reproducibility_run1_run2",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    #'scratch/agilent700/agilent700_vs_gs180/agilent700_vs_oc180_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                    #'scratch/agilent700/agilent700_vs_gs180/agilent700_vs_oc180_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                ],
                "indel": [
                    #'scratch/agilent700/agilent700_vs_gs180/agilent700_vs_oc180_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                    #'scratch/agilent700/agilent700_vs_gs180/agilent700_vs_oc180_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                ]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file": [
                        #'scratch/agilent700/agilent700_vs_gs180/agilent700_vs_oc180_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                        #'scratch/agilent700/agilent700_vs_gs180/agilent700_vs_oc180_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        #'scratch/agilent700/agilent700_vs_gs180/agilent700_vs_oc180_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                        #'scratch/agilent700/agilent700_vs_gs180/agilent700_vs_oc180_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "Agilent 700",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "Agilent 700",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "Agilent 700",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid23-1351',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1351-snapshot/2023jun02-gs180-run1-b23-95-cid23-1351-snapshot.B23-95.CID23-1351.B502_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '1b-cid23-1680',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot.B23-115.CID23-1680.A501_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample2': [{'sample_id': '2a-cid23-1215',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023maio22-gs180-run-b23-87-cid23-1215-snapshot/2023maio22-gs180-run-b23-87-cid23-1215-snapshot.B23-87.CID23-1215.B505_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '2b-cid23-1704',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot.B23-115.CID23-1704.A502_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample3': [{'sample_id': '3a-cid23-1313',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1313-snapshot/2023jun02-gs180-run1-b23-95-cid23-1313-snapshot.B23-95.CID23-1313.C543_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '3b-cid23-1705',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot.B23-115.CID23-1705.A503_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample4': [{'sample_id': '4a-cid23-1319',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1319-snapshot/2023jun02-gs180-run1-b23-95-cid23-1319-snapshot.B23-95.CID23-1319.C545_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '4b-cid23-1706',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot.B23-115.CID23-1706.A504_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample5': [{'sample_id': '5a-cid23-1375',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1375-snapshot/2023jun02-gs180-run1-b23-95-cid23-1375-snapshot.B23-95.CID23-1375.B504_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '5b-cid23-1683',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot.B23-115.CID23-1683.A508_N708.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample6': [{'sample_id': '6a-cid23-989',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022abr15-gs180-run1-b23-70-cid23-989-snapshot/2022abr15-gs180-run1-b23-70-cid23-989-snapshot.B23-70.CID23-989.B538_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '6b-cid23-1685',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot.B23-115.CID23-1685.A510_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample7': [{'sample_id': '7a-cid23-1020',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022abr15-gs180-run1-b23-70-cid23-1020-snapshot/2022abr15-gs180-run1-b23-70-cid23-1020-snapshot.B23-70.CID23-1020.B537_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '7b-cid23-1686',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot.B23-115.CID23-1686.A511_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample8': [{'sample_id': '8a-cid23-825',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023mar27-gs180-run1-b23-58-cid23-825-snapshot/2023mar27-gs180-run1-b23-58-cid23-825-snapshot.B23-58.CID23-825.B513_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '8b-cid23-1687',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot.B23-115.CID23-1687.A512_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample9': [{'sample_id': '9a-cid23-757',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023mar17-gs180-run1-b23-53-cid23-757-snapshot/2023mar17-gs180-run1-b23-53-cid23-757-snapshot.B23-53.CID23-757.B542_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '9b-cid23-1689',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot.B23-115.CID23-1689.A514_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample10': [{'sample_id': '10a-cid22-947',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022mai09-gs180-run1-b22-59-cid22-947-snapshot/2022mai09-gs180-run1-b22-59-cid22-947-snapshot.B22-59.CID22-947.B530_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '10b-cid23-1690',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot.B23-115.CID23-1690.A515_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample11': [{'sample_id': '11a-cid21-494',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021-04-23-gs180-run-b21-51-cid21-494-snapshot/2021-04-23-gs180-run-b21-51-cid21-494-snapshot.B21-51.CID21-494.B533_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '11b-cid23-1684',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot.B23-115.CID23-1684.A509_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample12': [{'sample_id': '12a-cid22-840',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022abr08-gs180-run1-b22-46-cid22-840-snapshot/2022abr08-gs180-run1-b22-46-cid22-840-snapshot.B22-46.CID22-840.B542_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '12b-cid23-1691',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot.B23-115.CID23-1691.A516_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample13': [{'sample_id': '13a-cid22-283',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022fev02-gs180-run0-b22-17-cid22-283-snapshot/2022fev02-gs180-run0-b22-17-cid22-283-snapshot.B22-17.CID22-283.B547_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '13b-cid23-1692',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot.B23-115.CID23-1692.A517_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample14': [{'sample_id': '14a-cid23-1281',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023maio26-gs180-run-b23-91-cid23-1281-snapshot/2023maio26-gs180-run-b23-91-cid23-1281-snapshot.B23-91.CID23-1281.B539_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '14b-cid23-1708',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot.B23-115.CID23-1708.A518_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample15': [{'sample_id': '15a-cid22-189',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022jan21-gs180-run0-b22-11-cid22-189-snapshot/2022jan21-gs180-run0-b22-11-cid22-189-snapshot.B22-11.CID22-189.B507_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '15b-cid23-1694',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot.B23-115.CID23-1694.A521_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample16': [{'sample_id': '16a-cid21-2159',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021out23-oc180-r53--b21-151-cid21-2159-snapshot/2021out23-oc180-r53--b21-151-cid21-2159-snapshot.B21-151.CID21-2159.B520_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '16b-cid23-1696',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot.B23-115.CID23-1696.A522_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample17': [{'sample_id': '17a-cid21-1978',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021set29-oc180-run4-b21-136-cid21-1978-snapshot/2021set29-oc180-run4-b21-136-cid21-1978-snapshot.B21-136.CID21-1978.C535_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '17b-cid23-1695',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot.B23-115.CID23-1695.A523_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample18': [{'sample_id': '18a-cid21-1665',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '18b-cid23-1682',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot.B23-115.CID23-1682.A505_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                # RUN #2
                'inter_Sample19': [{'sample_id': '19a-cid21-775',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-05-31-oc180-run-b21-73-cid21-775-snapshot/2021-05-31-oc180-run-b21-73-cid21-775-snapshot.B21-73.CID21-775.A537_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '19b-cid23-1833',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot.B23-121.CID23-1833.A514_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample20': [{'sample_id': '20a-cid21-484',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-04-23-gs180-run-b21-49-cid21-484-snapshot/2021-04-23-gs180-run-b21-49-cid21-484-snapshot.B21-49.CID21-484.B524_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '20b-cid23-1832',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot.B23-121.CID23-1832.A515_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample21': [{'sample_id': '21a-cid21-486',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-04-23-gs180-run-b21-49-cid21-486-snapshot/2021-04-23-gs180-run-b21-49-cid21-486-snapshot.B21-49.CID21-486.B539_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '21b-cid23-1834',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot.B23-121.CID23-1834.A516_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample22': [{'sample_id': '22a-cid22-1120',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2022mai13-gs180-run1-b22-62-cid22-1120-snapshot/2022mai13-gs180-run1-b22-62-cid22-1120-snapshot.B22-62.CID22-1120.C528_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '22b-cid23-1838',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot.B23-121.CID23-1838.A519_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample23': [{'sample_id': '23a-cid22-82',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2022jan21-gs180-run0-b22-11-cid22-82-snapshot/2022jan21-gs180-run0-b22-11-cid22-82-snapshot.B22-11.CID22-82.B510_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '23b-cid23-1837',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot.B23-121.CID23-1837.A520_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample24': [{'sample_id': '24a-cid21-745',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-05-26-oc180-run-b21-69-cid21-745-snapshot/2021-05-26-oc180-run-b21-69-cid21-745-snapshot.B21-69.CID21-745.B520_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '24b-cid23-1839',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot.B23-121.CID23-1839.A521_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}]
            }

        }
    }


def get_agilent700_vs_oc395_reproducibility():
    """
    Get the VCF files for the Agilent700 vs GS395
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/agilent700_vs_oc395_reproducibility_run1_run2",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    #'scratch/agilent700/agilent700_vs_gs395/agilent700_vs_oc395_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                    #'scratch/agilent700/agilent700_vs_gs395/agilent700_vs_oc395_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt',
                ],
                "indel": [
                    #'scratch/agilent700/agilent700_vs_gs395/agilent700_vs_oc395_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                    #'scratch/agilent700/agilent700_vs_gs395/agilent700_vs_oc395_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                ]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file": [
                        #'scratch/agilent700/agilent700_vs_gs395/agilent700_vs_oc395_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                        #'scratch/agilent700/agilent700_vs_gs395/agilent700_vs_oc395_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt',
                    ],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        #'scratch/agilent700/agilent700_vs_gs395/agilent700_vs_oc395_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                        #'scratch/agilent700/agilent700_vs_gs395/agilent700_vs_oc395_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS395",
                "y_axis_label": "Agilent 700",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS395",
                "y_axis_label": "Agilent 700",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS395",
                "y_axis_label": "Agilent 700",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid23-1026',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023abr17-gsinfinity-b23-72-cid23-1026-snapshot/2023abr17-gsinfinity-b23-72-cid23-1026-snapshot.B23-72.CID23-1026.B544_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '1b-cid23-1681',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot.B23-115.CID23-1681.A506_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                 'inter_Sample2': [{'sample_id': '2a-cid23-1305',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023jun02-gsinfinity-b23-93-cid23-1305-snapshot/2023jun02-gsinfinity-b23-93-cid23-1305-snapshot.B23-93.CID23-1305.C529_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '2b-cid23-1707',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot.B23-115.CID23-1707.A507_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                 'inter_Sample3': [{'sample_id': '3a-cid23-863',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023mar22-gsinfinity-b23-56-cid23-863-snapshot/2023mar22-gsinfinity-b23-56-cid23-863-snapshot.B23-56.CID23-863.C516_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '3b-cid23-1688',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot.B23-115.CID23-1688.A513_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                 'inter_Sample4': [{'sample_id': '4a-cid23-1251',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023maio22-gsinfinit-b23-89-cid23-1251-snapshot/2023maio22-gsinfinit-b23-89-cid23-1251-snapshot.B23-89.CID23-1251.B521_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '4b-cid23-1709',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot.B23-115.CID23-1709.A519_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample5': [{'sample_id': '5a-cid23-881',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023mar28-gsinfinity-b23-60-cid23-881-snapshot/2023mar28-gsinfinity-b23-60-cid23-881-snapshot.B23-60.CID23-881.B543_N705.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '5b-cid23-1693',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot.B23-115.CID23-1693.A520_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                ### Second RUN
                'inter_Sample6': [{'sample_id': '6a-cid23-1321',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023jun02-gsinfinity-b23-93-cid23-1321-snapshot/2023jun02-gsinfinity-b23-93-cid23-1321-snapshot.B23-93.CID23-1321.C531_N708.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '6b-cid23-1820',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot.B23-121.CID23-1820.A501_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample7': [{'sample_id': '7a-cid23-1341',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023jun02-gsinfinity-b23-93-cid23-1341-snapshot/2023jun02-gsinfinity-b23-93-cid23-1341-snapshot.B23-93.CID23-1341.C514_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '7b-cid23-1821',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot.B23-121.CID23-1821.A502_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample8': [{'sample_id': '8a-cid23-1217',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1217-snapshot/2023maio12-gsinfinit-b23-84-cid23-1217-snapshot.B23-84.CID23-1217.C516_N708.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '8b-cid23-1864',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot.B23-121.CID23-1864.A503_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample9': [{'sample_id': '9a-cid23-1245',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio22-gsinfinit-b23-89-cid23-1245-snapshot/2023maio22-gsinfinit-b23-89-cid23-1245-snapshot.B23-89.CID23-1245.B523_N708.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '9b-cid23-1822',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot.B23-121.CID23-1822.A504_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample10': [{'sample_id': '10a-cid23-1255',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio22-gsinfinit-b23-89-cid23-1255-snapshot/2023maio22-gsinfinit-b23-89-cid23-1255-snapshot.B23-89.CID23-1255.B522_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '10b-cid23-1823',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot.B23-121.CID23-1823.A505_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample11': [{'sample_id': '11a-cid23-1158',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1158-snapshot/2023maio12-gsinfinit-b23-84-cid23-1158-snapshot.B23-84.CID23-1158.C514_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '11b-cid23-1824',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot.B23-121.CID23-1824.A507_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample12': [{'sample_id': '12a-cid23-1172',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1172-snapshot/2023maio12-gsinfinit-b23-84-cid23-1172-snapshot.B23-84.CID23-1172.C515_N707.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '12b-cid23-1825',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot.B23-121.CID23-1825.A508_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample13': [{'sample_id': '13a-cid23-1108',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1108-snapshot/2023maio05-gsinfinit-b23-80-cid23-1108-snapshot.B23-80.CID23-1108.C505_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '13b-cid23-1826',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot.B23-121.CID23-1826.A509_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample14': [{'sample_id': '14a-cid23-1131',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1131-snapshot/2023maio05-gsinfinit-b23-80-cid23-1131-snapshot.B23-80.CID23-1131.C504_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '14b-cid23-1827',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot.B23-121.CID23-1827.A510_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample15': [{'sample_id': '15a-cid23-1124',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1124-snapshot/2023maio05-gsinfinit-b23-80-cid23-1124-snapshot.B23-80.CID23-1124.C502_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '15b-cid23-1828',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot.B23-121.CID23-1828.A511_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample16': [{'sample_id': '16a-cid23-1092',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1092-snapshot/2023maio05-gsinfinit-b23-80-cid23-1092-snapshot.B23-80.CID23-1092.C503_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '16b-cid23-1829',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot.B23-121.CID23-1829.A512_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample17': [{'sample_id': '17a-cid23-1102',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023abr28-gsinfinity-b23-76-cid23-1102-snapshot/2023abr28-gsinfinity-b23-76-cid23-1102-snapshot.B23-76.CID23-1102.B522_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '17b-cid23-1830',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot.B23-121.CID23-1830.A513_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample18': [{'sample_id': '18a-cid23-703',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023mar09-gsinfinity-b23-49-cid23-703-snapshot/2023mar09-gsinfinity-b23-49-cid23-703-snapshot.B23-49.CID23-703.C502_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '18b-cid23-1835',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot.B23-121.CID23-1835.A517_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample19': [{'sample_id': '19a-cid23-1100',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023abr28-gsinfinity-b23-76-cid23-1100-snapshot/2023abr28-gsinfinity-b23-76-cid23-1100-snapshot.B23-76.CID23-1100.B517_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '19b-cid23-1836',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot.B23-121.CID23-1836.A518_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}]
            }

        }
    }


def get_agilent700_vs_oc180_reproducibility_htqc_plots():
    """
    Get the QC files for the Agilent700 vs GS180
    """

    key_2_use = 'agilent700_vs_oc180_reproducibility_run1_run2'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "Agilent700 vs GS180",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1351-snapshot/2023jun02-gs180-run1-b23-95-cid23-1351-snapshot.B23-95.CID23-1351.B502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1351-snapshot/2023jun02-gs180-run1-b23-95-cid23-1351-snapshot.B23-95.CID23-1351.B502_N702.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid23-1351',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot.B23-115.CID23-1680.A501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1680-snapshot.B23-115.CID23-1680.A501_N701.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid23-1680',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023maio22-gs180-run-b23-87-cid23-1215-snapshot/2023maio22-gs180-run-b23-87-cid23-1215-snapshot.B23-87.CID23-1215.B505_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023maio22-gs180-run-b23-87-cid23-1215-snapshot/2023maio22-gs180-run-b23-87-cid23-1215-snapshot.B23-87.CID23-1215.B505_N703.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid23-1215',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot.B23-115.CID23-1704.A502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1704-snapshot.B23-115.CID23-1704.A502_N702.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid23-1704',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1313-snapshot/2023jun02-gs180-run1-b23-95-cid23-1313-snapshot.B23-95.CID23-1313.C543_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1313-snapshot/2023jun02-gs180-run1-b23-95-cid23-1313-snapshot.B23-95.CID23-1313.C543_N703.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid23-1313',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot.B23-115.CID23-1705.A503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1705-snapshot.B23-115.CID23-1705.A503_N703.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid23-1705',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1319-snapshot/2023jun02-gs180-run1-b23-95-cid23-1319-snapshot.B23-95.CID23-1319.C545_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1319-snapshot/2023jun02-gs180-run1-b23-95-cid23-1319-snapshot.B23-95.CID23-1319.C545_N705.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid23-1319',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot.B23-115.CID23-1706.A504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1706-snapshot.B23-115.CID23-1706.A504_N704.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid23-1706',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1375-snapshot/2023jun02-gs180-run1-b23-95-cid23-1375-snapshot.B23-95.CID23-1375.B504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023jun02-gs180-run1-b23-95-cid23-1375-snapshot/2023jun02-gs180-run1-b23-95-cid23-1375-snapshot.B23-95.CID23-1375.B504_N704.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid23-1375',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot.B23-115.CID23-1683.A508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1683-snapshot.B23-115.CID23-1683.A508_N708.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid23-1683',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022abr15-gs180-run1-b23-70-cid23-989-snapshot/2022abr15-gs180-run1-b23-70-cid23-989-snapshot.B23-70.CID23-989.B538_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022abr15-gs180-run1-b23-70-cid23-989-snapshot/2022abr15-gs180-run1-b23-70-cid23-989-snapshot.B23-70.CID23-989.B538_N706.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid23-989',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot.B23-115.CID23-1685.A510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1685-snapshot.B23-115.CID23-1685.A510_N702.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid23-1685',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022abr15-gs180-run1-b23-70-cid23-1020-snapshot/2022abr15-gs180-run1-b23-70-cid23-1020-snapshot.B23-70.CID23-1020.B537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022abr15-gs180-run1-b23-70-cid23-1020-snapshot/2022abr15-gs180-run1-b23-70-cid23-1020-snapshot.B23-70.CID23-1020.B537_N705.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid23-1020',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot.B23-115.CID23-1686.A511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1686-snapshot.B23-115.CID23-1686.A511_N703.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid23-1686',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023mar27-gs180-run1-b23-58-cid23-825-snapshot/2023mar27-gs180-run1-b23-58-cid23-825-snapshot.B23-58.CID23-825.B513_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023mar27-gs180-run1-b23-58-cid23-825-snapshot/2023mar27-gs180-run1-b23-58-cid23-825-snapshot.B23-58.CID23-825.B513_N702.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid23-825',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot.B23-115.CID23-1687.A512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1687-snapshot.B23-115.CID23-1687.A512_N704.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid23-1687',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023mar17-gs180-run1-b23-53-cid23-757-snapshot/2023mar17-gs180-run1-b23-53-cid23-757-snapshot.B23-53.CID23-757.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023mar17-gs180-run1-b23-53-cid23-757-snapshot/2023mar17-gs180-run1-b23-53-cid23-757-snapshot.B23-53.CID23-757.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid23-757',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot.B23-115.CID23-1689.A514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1689-snapshot.B23-115.CID23-1689.A514_N706.coverage.summary.htqc.txt',
                        'sample_id': '9b-cid23-1689',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022mai09-gs180-run1-b22-59-cid22-947-snapshot/2022mai09-gs180-run1-b22-59-cid22-947-snapshot.B22-59.CID22-947.B530_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022mai09-gs180-run1-b22-59-cid22-947-snapshot/2022mai09-gs180-run1-b22-59-cid22-947-snapshot.B22-59.CID22-947.B530_N706.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid22-947',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot.B23-115.CID23-1690.A515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1690-snapshot.B23-115.CID23-1690.A515_N707.coverage.summary.htqc.txt',
                        'sample_id': '10b-cid23-1690',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021-04-23-gs180-run-b21-51-cid21-494-snapshot/2021-04-23-gs180-run-b21-51-cid21-494-snapshot.B21-51.CID21-494.B533_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021-04-23-gs180-run-b21-51-cid21-494-snapshot/2021-04-23-gs180-run-b21-51-cid21-494-snapshot.B21-51.CID21-494.B533_N704.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid21-494',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot.B23-115.CID23-1684.A509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1684-snapshot.B23-115.CID23-1684.A509_N701.coverage.summary.htqc.txt',
                        'sample_id': '11b-cid23-1684',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022abr08-gs180-run1-b22-46-cid22-840-snapshot/2022abr08-gs180-run1-b22-46-cid22-840-snapshot.B22-46.CID22-840.B542_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022abr08-gs180-run1-b22-46-cid22-840-snapshot/2022abr08-gs180-run1-b22-46-cid22-840-snapshot.B22-46.CID22-840.B542_N702.coverage.summary.htqc.txt',
                        'sample_id': '12a-cid22-840',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot.B23-115.CID23-1691.A516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1691-snapshot.B23-115.CID23-1691.A516_N708.coverage.summary.htqc.txt',
                        'sample_id': '12b-cid23-1691',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022fev02-gs180-run0-b22-17-cid22-283-snapshot/2022fev02-gs180-run0-b22-17-cid22-283-snapshot.B22-17.CID22-283.B547_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022fev02-gs180-run0-b22-17-cid22-283-snapshot/2022fev02-gs180-run0-b22-17-cid22-283-snapshot.B22-17.CID22-283.B547_N707.coverage.summary.htqc.txt',
                        'sample_id': '13a-cid22-283',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot.B23-115.CID23-1692.A517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1692-snapshot.B23-115.CID23-1692.A517_N701.coverage.summary.htqc.txt',
                        'sample_id': '13b-cid23-1692',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023maio26-gs180-run-b23-91-cid23-1281-snapshot/2023maio26-gs180-run-b23-91-cid23-1281-snapshot.B23-91.CID23-1281.B539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2023maio26-gs180-run-b23-91-cid23-1281-snapshot/2023maio26-gs180-run-b23-91-cid23-1281-snapshot.B23-91.CID23-1281.B539_N707.coverage.summary.htqc.txt',
                        'sample_id': '14a-cid23-1281',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot.B23-115.CID23-1708.A518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1708-snapshot.B23-115.CID23-1708.A518_N702.coverage.summary.htqc.txt',
                        'sample_id': '14b-cid23-1708',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022jan21-gs180-run0-b22-11-cid22-189-snapshot/2022jan21-gs180-run0-b22-11-cid22-189-snapshot.B22-11.CID22-189.B507_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2022jan21-gs180-run0-b22-11-cid22-189-snapshot/2022jan21-gs180-run0-b22-11-cid22-189-snapshot.B22-11.CID22-189.B507_N706.coverage.summary.htqc.txt',
                        'sample_id': '15a-cid22-189',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot.B23-115.CID23-1694.A521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1694-snapshot.B23-115.CID23-1694.A521_N705.coverage.summary.htqc.txt',
                        'sample_id': '15b-cid23-1694',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021out23-oc180-r53--b21-151-cid21-2159-snapshot/2021out23-oc180-r53--b21-151-cid21-2159-snapshot.B21-151.CID21-2159.B520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021out23-oc180-r53--b21-151-cid21-2159-snapshot/2021out23-oc180-r53--b21-151-cid21-2159-snapshot.B21-151.CID21-2159.B520_N704.coverage.summary.htqc.txt',
                        'sample_id': '16a-cid21-2159',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot.B23-115.CID23-1696.A522_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1696-snapshot.B23-115.CID23-1696.A522_N706.coverage.summary.htqc.txt',
                        'sample_id': '16b-cid23-1696',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021set29-oc180-run4-b21-136-cid21-1978-snapshot/2021set29-oc180-run4-b21-136-cid21-1978-snapshot.B21-136.CID21-1978.C535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021set29-oc180-run4-b21-136-cid21-1978-snapshot/2021set29-oc180-run4-b21-136-cid21-1978-snapshot.B21-136.CID21-1978.C535_N703.coverage.summary.htqc.txt',
                        'sample_id': '17a-cid21-1978',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot.B23-115.CID23-1695.A523_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1695-snapshot.B23-115.CID23-1695.A523_N707.coverage.summary.htqc.txt',
                        'sample_id': '17b-cid23-1695',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/first_run/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '18a-cid21-1665',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot.B23-115.CID23-1682.A505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1682-snapshot.B23-115.CID23-1682.A505_N705.coverage.summary.htqc.txt',
                        'sample_id': '18b-cid23-1682',
                        'sample_type': ''},
                    # RUN #2
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-05-31-oc180-run-b21-73-cid21-775-snapshot/2021-05-31-oc180-run-b21-73-cid21-775-snapshot.B21-73.CID21-775.A537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-05-31-oc180-run-b21-73-cid21-775-snapshot/2021-05-31-oc180-run-b21-73-cid21-775-snapshot.B21-73.CID21-775.A537_N705.coverage.summary.htqc.txt',
                        'sample_id': '19a-cid21-775',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot.B23-121.CID23-1833.A514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1833-snapshot.B23-121.CID23-1833.A514_N706.coverage.summary.htqc.txt',
                        'sample_id': '19b-cid23-1833',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-04-23-gs180-run-b21-49-cid21-484-snapshot/2021-04-23-gs180-run-b21-49-cid21-484-snapshot.B21-49.CID21-484.B524_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-04-23-gs180-run-b21-49-cid21-484-snapshot/2021-04-23-gs180-run-b21-49-cid21-484-snapshot.B21-49.CID21-484.B524_N708.coverage.summary.htqc.txt',
                        'sample_id': '20a-cid21-484',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot.B23-121.CID23-1832.A515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1832-snapshot.B23-121.CID23-1832.A515_N707.coverage.summary.htqc.txt',
                        'sample_id': '20b-cid23-1832',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-04-23-gs180-run-b21-49-cid21-486-snapshot/2021-04-23-gs180-run-b21-49-cid21-486-snapshot.B21-49.CID21-486.B539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-04-23-gs180-run-b21-49-cid21-486-snapshot/2021-04-23-gs180-run-b21-49-cid21-486-snapshot.B21-49.CID21-486.B539_N707.coverage.summary.htqc.txt',
                        'sample_id': '21a-cid21-486',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot.B23-121.CID23-1834.A516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1834-snapshot.B23-121.CID23-1834.A516_N708.coverage.summary.htqc.txt',
                        'sample_id': '21b-cid23-1834',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2022mai13-gs180-run1-b22-62-cid22-1120-snapshot/2022mai13-gs180-run1-b22-62-cid22-1120-snapshot.B22-62.CID22-1120.C528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2022mai13-gs180-run1-b22-62-cid22-1120-snapshot/2022mai13-gs180-run1-b22-62-cid22-1120-snapshot.B22-62.CID22-1120.C528_N704.coverage.summary.htqc.txt',
                        'sample_id': '22a-cid22-1120',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot.B23-121.CID23-1838.A519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1838-snapshot.B23-121.CID23-1838.A519_N703.coverage.summary.htqc.txt',
                        'sample_id': '22b-cid23-1838',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2022jan21-gs180-run0-b22-11-cid22-82-snapshot/2022jan21-gs180-run0-b22-11-cid22-82-snapshot.B22-11.CID22-82.B510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2022jan21-gs180-run0-b22-11-cid22-82-snapshot/2022jan21-gs180-run0-b22-11-cid22-82-snapshot.B22-11.CID22-82.B510_N702.coverage.summary.htqc.txt',
                        'sample_id': '23a-cid22-82',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot.B23-121.CID23-1837.A520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1837-snapshot.B23-121.CID23-1837.A520_N704.coverage.summary.htqc.txt',
                        'sample_id': '23b-cid23-1837',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-05-26-oc180-run-b21-69-cid21-745-snapshot/2021-05-26-oc180-run-b21-69-cid21-745-snapshot.B21-69.CID21-745.B520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/oc180_data/second_run//2021-05-26-oc180-run-b21-69-cid21-745-snapshot/2021-05-26-oc180-run-b21-69-cid21-745-snapshot.B21-69.CID21-745.B520_N704.coverage.summary.htqc.txt',
                        'sample_id': '24a-cid21-745',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot.B23-121.CID23-1839.A521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc180_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1839-snapshot.B23-121.CID23-1839.A521_N705.coverage.summary.htqc.txt',
                        'sample_id': '24b-cid23-1839',
                        'sample_type': ''}
                ]
            }
        }
    }


def get_agilent700_vs_oc395_reproducibility_htqc_plots():
    """
    Get the QC files for the Agilent700 vs GS395
    """

    key_2_use = 'agilent700_vs_oc395_reproducibility_run1_run2'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "Agilent700 vs GS395",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023abr17-gsinfinity-b23-72-cid23-1026-snapshot/2023abr17-gsinfinity-b23-72-cid23-1026-snapshot.B23-72.CID23-1026.B544_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023abr17-gsinfinity-b23-72-cid23-1026-snapshot/2023abr17-gsinfinity-b23-72-cid23-1026-snapshot.B23-72.CID23-1026.B544_N705.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid23-1026',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot.B23-115.CID23-1681.A506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1681-snapshot.B23-115.CID23-1681.A506_N706.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid23-1681',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023jun02-gsinfinity-b23-93-cid23-1305-snapshot/2023jun02-gsinfinity-b23-93-cid23-1305-snapshot.B23-93.CID23-1305.C529_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023jun02-gsinfinity-b23-93-cid23-1305-snapshot/2023jun02-gsinfinity-b23-93-cid23-1305-snapshot.B23-93.CID23-1305.C529_N706.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid23-1305',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot.B23-115.CID23-1707.A507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1707-snapshot.B23-115.CID23-1707.A507_N707.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid23-1707',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023mar22-gsinfinity-b23-56-cid23-863-snapshot/2023mar22-gsinfinity-b23-56-cid23-863-snapshot.B23-56.CID23-863.C516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023mar22-gsinfinity-b23-56-cid23-863-snapshot/2023mar22-gsinfinity-b23-56-cid23-863-snapshot.B23-56.CID23-863.C516_N708.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid23-863',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot.B23-115.CID23-1688.A513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1688-snapshot.B23-115.CID23-1688.A513_N705.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid23-1688',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023maio22-gsinfinit-b23-89-cid23-1251-snapshot/2023maio22-gsinfinit-b23-89-cid23-1251-snapshot.B23-89.CID23-1251.B521_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023maio22-gsinfinit-b23-89-cid23-1251-snapshot/2023maio22-gsinfinit-b23-89-cid23-1251-snapshot.B23-89.CID23-1251.B521_N706.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid23-1251',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot.B23-115.CID23-1709.A519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1709-snapshot.B23-115.CID23-1709.A519_N703.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid23-1709',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023mar28-gsinfinity-b23-60-cid23-881-snapshot/2023mar28-gsinfinity-b23-60-cid23-881-snapshot.B23-60.CID23-881.B543_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/first_run/2023mar28-gsinfinity-b23-60-cid23-881-snapshot/2023mar28-gsinfinity-b23-60-cid23-881-snapshot.B23-60.CID23-881.B543_N705.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid23-881',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot.B23-115.CID23-1693.A520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/first_run//2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot/2023jul13-agcgp-v2-run0-b23-115-cid23-1693-snapshot.B23-115.CID23-1693.A520_N704.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid23-1693',
                        'sample_type': ''},
                    # RUN #2
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023jun02-gsinfinity-b23-93-cid23-1321-snapshot/2023jun02-gsinfinity-b23-93-cid23-1321-snapshot.B23-93.CID23-1321.C531_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023jun02-gsinfinity-b23-93-cid23-1321-snapshot/2023jun02-gsinfinity-b23-93-cid23-1321-snapshot.B23-93.CID23-1321.C531_N708.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid23-1321',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot.B23-121.CID23-1820.A501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1820-snapshot.B23-121.CID23-1820.A501_N701.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid23-1820',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023jun02-gsinfinity-b23-93-cid23-1341-snapshot/2023jun02-gsinfinity-b23-93-cid23-1341-snapshot.B23-93.CID23-1341.C514_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023jun02-gsinfinity-b23-93-cid23-1341-snapshot/2023jun02-gsinfinity-b23-93-cid23-1341-snapshot.B23-93.CID23-1341.C514_N702.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid23-1341',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot.B23-121.CID23-1821.A502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1821-snapshot.B23-121.CID23-1821.A502_N702.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid23-1821',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1217-snapshot/2023maio12-gsinfinit-b23-84-cid23-1217-snapshot.B23-84.CID23-1217.C516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1217-snapshot/2023maio12-gsinfinit-b23-84-cid23-1217-snapshot.B23-84.CID23-1217.C516_N708.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid23-1217',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot.B23-121.CID23-1864.A503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1864-snapshot.B23-121.CID23-1864.A503_N703.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid23-1864',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio22-gsinfinit-b23-89-cid23-1245-snapshot/2023maio22-gsinfinit-b23-89-cid23-1245-snapshot.B23-89.CID23-1245.B523_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio22-gsinfinit-b23-89-cid23-1245-snapshot/2023maio22-gsinfinit-b23-89-cid23-1245-snapshot.B23-89.CID23-1245.B523_N708.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid23-1245',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot.B23-121.CID23-1822.A504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1822-snapshot.B23-121.CID23-1822.A504_N704.coverage.summary.htqc.txt',
                        'sample_id': '9b-cid23-1822',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio22-gsinfinit-b23-89-cid23-1255-snapshot/2023maio22-gsinfinit-b23-89-cid23-1255-snapshot.B23-89.CID23-1255.B522_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio22-gsinfinit-b23-89-cid23-1255-snapshot/2023maio22-gsinfinit-b23-89-cid23-1255-snapshot.B23-89.CID23-1255.B522_N707.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid23-1255',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot.B23-121.CID23-1823.A505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1823-snapshot.B23-121.CID23-1823.A505_N705.coverage.summary.htqc.txt',
                        'sample_id': '10b-cid23-1823',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1158-snapshot/2023maio12-gsinfinit-b23-84-cid23-1158-snapshot.B23-84.CID23-1158.C514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1158-snapshot/2023maio12-gsinfinit-b23-84-cid23-1158-snapshot.B23-84.CID23-1158.C514_N706.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid23-1158',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot.B23-121.CID23-1824.A507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1824-snapshot.B23-121.CID23-1824.A507_N707.coverage.summary.htqc.txt',
                        'sample_id': '11b-cid23-1824',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1172-snapshot/2023maio12-gsinfinit-b23-84-cid23-1172-snapshot.B23-84.CID23-1172.C515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio12-gsinfinit-b23-84-cid23-1172-snapshot/2023maio12-gsinfinit-b23-84-cid23-1172-snapshot.B23-84.CID23-1172.C515_N707.coverage.summary.htqc.txt',
                        'sample_id': '12a-cid23-1172',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot.B23-121.CID23-1825.A508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1825-snapshot.B23-121.CID23-1825.A508_N708.coverage.summary.htqc.txt',
                        'sample_id': '12b-cid23-1825',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1108-snapshot/2023maio05-gsinfinit-b23-80-cid23-1108-snapshot.B23-80.CID23-1108.C505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1108-snapshot/2023maio05-gsinfinit-b23-80-cid23-1108-snapshot.B23-80.CID23-1108.C505_N705.coverage.summary.htqc.txt',
                        'sample_id': '13a-cid23-1108',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot.B23-121.CID23-1826.A509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1826-snapshot.B23-121.CID23-1826.A509_N701.coverage.summary.htqc.txt',
                        'sample_id': '13b-cid23-1826',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1131-snapshot/2023maio05-gsinfinit-b23-80-cid23-1131-snapshot.B23-80.CID23-1131.C504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1131-snapshot/2023maio05-gsinfinit-b23-80-cid23-1131-snapshot.B23-80.CID23-1131.C504_N704.coverage.summary.htqc.txt',
                        'sample_id': '14a-cid23-1131',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot.B23-121.CID23-1827.A510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1827-snapshot.B23-121.CID23-1827.A510_N702.coverage.summary.htqc.txt',
                        'sample_id': '14b-cid23-1827',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1124-snapshot/2023maio05-gsinfinit-b23-80-cid23-1124-snapshot.B23-80.CID23-1124.C502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1124-snapshot/2023maio05-gsinfinit-b23-80-cid23-1124-snapshot.B23-80.CID23-1124.C502_N702.coverage.summary.htqc.txt',
                        'sample_id': '15a-cid23-1124',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot.B23-121.CID23-1828.A511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1828-snapshot.B23-121.CID23-1828.A511_N703.coverage.summary.htqc.txt',
                        'sample_id': '15b-cid23-1828',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1092-snapshot/2023maio05-gsinfinit-b23-80-cid23-1092-snapshot.B23-80.CID23-1092.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023maio05-gsinfinit-b23-80-cid23-1092-snapshot/2023maio05-gsinfinit-b23-80-cid23-1092-snapshot.B23-80.CID23-1092.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '16a-cid23-1092',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot.B23-121.CID23-1829.A512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1829-snapshot.B23-121.CID23-1829.A512_N704.coverage.summary.htqc.txt',
                        'sample_id': '16b-cid23-1829',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023abr28-gsinfinity-b23-76-cid23-1102-snapshot/2023abr28-gsinfinity-b23-76-cid23-1102-snapshot.B23-76.CID23-1102.B522_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023abr28-gsinfinity-b23-76-cid23-1102-snapshot/2023abr28-gsinfinity-b23-76-cid23-1102-snapshot.B23-76.CID23-1102.B522_N704.coverage.summary.htqc.txt',
                        'sample_id': '17a-cid23-1102',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot.B23-121.CID23-1830.A513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1830-snapshot.B23-121.CID23-1830.A513_N705.coverage.summary.htqc.txt',
                        'sample_id': '17b-cid23-1830',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023mar09-gsinfinity-b23-49-cid23-703-snapshot/2023mar09-gsinfinity-b23-49-cid23-703-snapshot.B23-49.CID23-703.C502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023mar09-gsinfinity-b23-49-cid23-703-snapshot/2023mar09-gsinfinity-b23-49-cid23-703-snapshot.B23-49.CID23-703.C502_N702.coverage.summary.htqc.txt',
                        'sample_id': '18a-cid23-703',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot.B23-121.CID23-1835.A517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1835-snapshot.B23-121.CID23-1835.A517_N701.coverage.summary.htqc.txt',
                        'sample_id': '18b-cid23-1835',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023abr28-gsinfinity-b23-76-cid23-1100-snapshot/2023abr28-gsinfinity-b23-76-cid23-1100-snapshot.B23-76.CID23-1100.B517_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/oc395_data/second_run//2023abr28-gsinfinity-b23-76-cid23-1100-snapshot/2023abr28-gsinfinity-b23-76-cid23-1100-snapshot.B23-76.CID23-1100.B517_N705.coverage.summary.htqc.txt',
                        'sample_id': '19a-cid23-1100',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot.B23-121.CID23-1836.A518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/agilent_700/oc395_vs_agilent/agilent_data/second_run//2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot/2023jul27-agcgp-run0-b23-121-cid23-1836-snapshot.B23-121.CID23-1836.A518_N702.coverage.summary.htqc.txt',
                        'sample_id': '19b-cid23-1836',
                        'sample_type': ''}
                ]
            }
        }
    }
