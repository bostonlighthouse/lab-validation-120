"""Get the VCF / QC files for the GS395 SNPs vs GS180"""
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds


def get_oc395snps_reproducibility():
    """
    Get the VCF files for the GS395 SNPs vs GS180
    """
    # TODO update with what final threshold did we come up with from the stem plots
    vaf_threshold_snv = 5.0
    depth_threshold_snv = 100
    vaf_threshold_indel = 8.0
    depth_threshold_indel = 100
    indel_thresholds = indel_concordance_size_thresholds()
    return {
        "clinical_sample_reproducibility": {
            "run": "run1",
            "images_dir": "images/oc395_snps_reproducibility",
            "consequential_impacts_only": True,  # This can be overwritten in argparse in some cases
            "final_overall_reproducibility_file": None,
            # only need to open these files, b/c we want the lower limit, and look at all from there
            "files_for_boxplot": {
                "snv": [
                    'scratch/gs395_snps/equivalency_to_gs180/consequential/oc120plus_vs_gs395sns_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                    'scratch/gs395_snps/equivalency_to_gs180/all/oc120plus_vs_gs395sns_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                ],
                "indel": [
                    'scratch/gs395_snps/equivalency_to_gs180/consequential/oc120plus_vs_gs395sns_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                    'scratch/gs395_snps/equivalency_to_gs180/all/oc120plus_vs_gs395sns_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                ]
            },
            # here we want to control what were th final values we opened
            "files_for_regression": {
                "snv": {
                    "file": [
                        'scratch/gs395_snps/equivalency_to_gs180/consequential/oc120plus_vs_gs395sns_vaf_5.0_depth_100_consequential_impacts_only_snvs_both_inter_and_intra.txt',
                        'scratch/gs395_snps/equivalency_to_gs180/all/oc120plus_vs_gs395sns_vaf_5.0_depth_100_all_impacts_snvs_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_snv,
                    "depth_threshold": depth_threshold_snv
                },
                "indel1": {
                    # get the indel size thresholds from the call to indel_concordance_size_thresholds() returns a list
                    # of tuples
                    "file": [
                        'scratch/gs395_snps/equivalency_to_gs180/consequential/oc120plus_vs_gs395sns_vaf_8.0_depth_100_consequential_impacts_only_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt',
                        'scratch/gs395_snps/equivalency_to_gs180/all/oc120plus_vs_gs395sns_vaf_8.0_depth_100_all_impacts_indels_size_1_1_and_2_4_and_5_or_more_both_inter_and_intra.txt'
                    ],
                    "vaf_threshold": vaf_threshold_indel,
                    "depth_threshold": depth_threshold_indel,
                    # indels need to have their corresponding indel min and max indel length
                    "min_length_variant": indel_thresholds[0][0],
                    "max_length_variant": indel_thresholds[0][1],
                }
            },
            "vaf_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "GS395 SNPs",
                "x_value_key": "VAF1",
                "y_value_key": "VAF2",
                "df_value1": "VAF1",
                "df_value2": "VAF2",
                "value_plotted": "VAF",
            },
            "depth_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "GS395 SNPs",
                "x_value_key": "Depth1",
                "y_value_key": "Depth2",
                "df_value1": "Depth1",
                "df_value2": "Depth2",
                "value_plotted": "Depth",
            },
            "allele_count_regression_plot": {
                "x_axis_label": "GS180",
                "y_axis_label": "GS395 SNPs",
                "x_value_key": "Allele_count1",
                "y_value_key": "Allele_count2",
                "df_value1": "Allele_count1",
                "df_value2": "Allele_count2",
                "value_plotted": "Allele Count",
            },
            "concordance_files": {
                'inter_Sample1': [{'sample_id': '1a-cid21-2645',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez11-oc180-r60--b21-175-cid21-2645-snapshot/2021dez11-oc180-r60--b21-175-cid21-2645-snapshot.B21-175.CID21-2645.C541_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '1b-cid22-2291',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2291-snapshot/2022out10-oc395-run1-b22-128-cid22-2291-snapshot.B22-128.CID22-2291.B511_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample2': [{'sample_id': '2a-cid21-2625',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez11-oc180-r60--b21-175-cid21-2625-snapshot/2021dez11-oc180-r60--b21-175-cid21-2625-snapshot.B21-175.CID21-2625.C534_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '2b-cid22-2300',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2300-snapshot/2022out10-oc395-run1-b22-128-cid22-2300-snapshot.B22-128.CID22-2300.B510_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample3': [{'sample_id': '3a-cid21-2723',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez30-run63-gs18-b21-184-cid21-2723-snapshot/2021dez30-run63-gs18-b21-184-cid21-2723-snapshot.B21-184.CID21-2723.B522_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '3b-cid22-2294',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2294-snapshot/2022out10-oc395-run1-b22-128-cid22-2294-snapshot.B22-128.CID22-2294.B514_N706.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample4': [{'sample_id': '4a-cid21-2819',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan06-gs180-run0-b22-4-cid21-2819-snapshot/2022jan06-gs180-run0-b22-4-cid21-2819-snapshot.B22-4.CID21-2819.B507_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '4b-cid22-2295',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2295-snapshot/2022out10-oc395-run1-b22-128-cid22-2295-snapshot.B22-128.CID22-2295.B515_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample5': [{'sample_id': '5a-cid21-1665',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '5b-cid22-2290',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2290-snapshot/2022out10-oc395-run1-b22-128-cid22-2290-snapshot.B22-128.CID22-2290.B509_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample6': [{'sample_id': '6a-cid21-1970',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021out13-oc180-run5-b21-145-cid21-1970-snapshot/2021out13-oc180-run5-b21-145-cid21-1970-snapshot.B21-145.CID21-1970.B504_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '6b-cid22-2292',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2292-snapshot/2022out10-oc395-run1-b22-128-cid22-2292-snapshot.B22-128.CID22-2292.B512_N704.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample7': [{'sample_id': '7a-cid22-497',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022fev28-gs180-run0-b22-28-cid22-497-snapshot/2022fev28-gs180-run0-b22-28-cid22-497-snapshot.B22-28.CID22-497.C539_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '7b-cid22-2283',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2283-snapshot/2022out10-oc395-run1-b22-128-cid22-2283-snapshot.B22-128.CID22-2283.B501_N701.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample8': [{'sample_id': '8a-cid22-460',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mar04-gs180-run0-b22-30-cid22-460-snapshot/2022mar04-gs180-run0-b22-30-cid22-460-snapshot.B22-30.CID22-460.B526_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '8b-cid22-2287',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2287-snapshot/2022out10-oc395-run1-b22-128-cid22-2287-snapshot.B22-128.CID22-2287.B507_N707.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample9': [{'sample_id': '9a-cid22-834',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022abr14-gs180-run1-b22-48-cid22-834-snapshot/2022abr14-gs180-run1-b22-48-cid22-834-snapshot.B22-48.CID22-834.B526_N702.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'sample_id': '9b-cid22-2285',
                                   'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2285-snapshot/2022out10-oc395-run1-b22-128-cid22-2285-snapshot.B22-128.CID22-2285.B503_N703.merged_concatenated.vcf',
                                   'variant_type': 'snvs'},
                                  {'experiment_type': 'inter-run'}],
                'inter_Sample10': [{'sample_id': '10a-cid22-889',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022abr14-gs180-run1-b22-48-cid22-889-snapshot/2022abr14-gs180-run1-b22-48-cid22-889-snapshot.B22-48.CID22-889.B528_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '10b-cid22-2284',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2284-snapshot/2022out10-oc395-run1-b22-128-cid22-2284-snapshot.B22-128.CID22-2284.B502_N702.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample11': [{'sample_id': '11a-cid22-965',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mai09-gs180-run1-b22-59-cid22-965-snapshot/2022mai09-gs180-run1-b22-59-cid22-965-snapshot.B22-59.CID22-965.B533_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '11b-cid22-2289',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2289-snapshot/2022out10-oc395-run1-b22-128-cid22-2289-snapshot.B22-128.CID22-2289.B508_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample12': [{'sample_id': '12a-cid22-15',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan13-gs180-run0-b22-7-cid22-15-snapshot/2022jan13-gs180-run0-b22-7-cid22-15-snapshot.B22-7.CID22-15.B520_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '12b-cid22-2296',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2296-snapshot/2022out10-oc395-run1-b22-128-cid22-2296-snapshot.B22-128.CID22-2296.B516_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample13': [{'sample_id': '13a-cid22-1180',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mai27-gs180-run2-b22-67-cid22-1180-snapshot/2022mai27-gs180-run2-b22-67-cid22-1180-snapshot.B22-67.CID22-1180.B505_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '13b-cid22-2286',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2286-snapshot/2022out10-oc395-run1-b22-128-cid22-2286-snapshot.B22-128.CID22-2286.B504_N704.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample14': [{'sample_id': '14a-cid22-1292',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jun03-gs180-run2-b22-73-cid22-1292-snapshot/2022jun03-gs180-run2-b22-73-cid22-1292-snapshot.B22-73.CID22-1292.B517_N701.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '14b-cid22-2297',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2297-snapshot/2022out10-oc395-run1-b22-128-cid22-2297-snapshot.B22-128.CID22-2297.B505_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample15': [{'sample_id': '15a-cid22-1757',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022ago12-gs180-run3-b22-101-cid22-1757-snapshot/2022ago12-gs180-run3-b22-101-cid22-1757-snapshot.B22-101.CID22-1757.C535_N703.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '15b-cid22-2293',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2293-snapshot/2022out10-oc395-run1-b22-128-cid22-2293-snapshot.B22-128.CID22-2293.B513_N705.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}],
                'inter_Sample16': [{'sample_id': '16a-cid22-88',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan21-gs180-run0-b22-11-cid22-88-snapshot/2022jan21-gs180-run0-b22-11-cid22-88-snapshot.B22-11.CID22-88.B524_N708.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'sample_id': '16b-cid22-2288',
                                    'vcf_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2288-snapshot/2022out10-oc395-run1-b22-128-cid22-2288-snapshot.B22-128.CID22-2288.B506_N706.merged_concatenated.vcf',
                                    'variant_type': 'snvs'},
                                   {'experiment_type': 'inter-run'}]
            }

        }
    }



def get_oc395snps_reproducibility_htqc_plots():
    """
    Get the QC files for the GS395 vs GS180
    """

    key_2_use = 'oc395_snps_reproducibility'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS395 SNPs vs GS180",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez11-oc180-r60--b21-175-cid21-2645-snapshot/2021dez11-oc180-r60--b21-175-cid21-2645-snapshot.B21-175.CID21-2645.C541_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez11-oc180-r60--b21-175-cid21-2645-snapshot/2021dez11-oc180-r60--b21-175-cid21-2645-snapshot.B21-175.CID21-2645.C541_N701.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid21-2645',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2291-snapshot/2022out10-oc395-run1-b22-128-cid22-2291-snapshot.B22-128.CID22-2291.B511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2291-snapshot/2022out10-oc395-run1-b22-128-cid22-2291-snapshot.B22-128.CID22-2291.B511_N703.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid22-2291',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez11-oc180-r60--b21-175-cid21-2625-snapshot/2021dez11-oc180-r60--b21-175-cid21-2625-snapshot.B21-175.CID21-2625.C534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez11-oc180-r60--b21-175-cid21-2625-snapshot/2021dez11-oc180-r60--b21-175-cid21-2625-snapshot.B21-175.CID21-2625.C534_N702.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid21-2625',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2300-snapshot/2022out10-oc395-run1-b22-128-cid22-2300-snapshot.B22-128.CID22-2300.B510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2300-snapshot/2022out10-oc395-run1-b22-128-cid22-2300-snapshot.B22-128.CID22-2300.B510_N702.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid22-2300',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez30-run63-gs18-b21-184-cid21-2723-snapshot/2021dez30-run63-gs18-b21-184-cid21-2723-snapshot.B21-184.CID21-2723.B522_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez30-run63-gs18-b21-184-cid21-2723-snapshot/2021dez30-run63-gs18-b21-184-cid21-2723-snapshot.B21-184.CID21-2723.B522_N701.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid21-2723',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2294-snapshot/2022out10-oc395-run1-b22-128-cid22-2294-snapshot.B22-128.CID22-2294.B514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2294-snapshot/2022out10-oc395-run1-b22-128-cid22-2294-snapshot.B22-128.CID22-2294.B514_N706.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid22-2294',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan06-gs180-run0-b22-4-cid21-2819-snapshot/2022jan06-gs180-run0-b22-4-cid21-2819-snapshot.B22-4.CID21-2819.B507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan06-gs180-run0-b22-4-cid21-2819-snapshot/2022jan06-gs180-run0-b22-4-cid21-2819-snapshot.B22-4.CID21-2819.B507_N707.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid21-2819',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2295-snapshot/2022out10-oc395-run1-b22-128-cid22-2295-snapshot.B22-128.CID22-2295.B515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2295-snapshot/2022out10-oc395-run1-b22-128-cid22-2295-snapshot.B22-128.CID22-2295.B515_N707.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid22-2295',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid21-1665',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2290-snapshot/2022out10-oc395-run1-b22-128-cid22-2290-snapshot.B22-128.CID22-2290.B509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2290-snapshot/2022out10-oc395-run1-b22-128-cid22-2290-snapshot.B22-128.CID22-2290.B509_N701.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid22-2290',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021out13-oc180-run5-b21-145-cid21-1970-snapshot/2021out13-oc180-run5-b21-145-cid21-1970-snapshot.B21-145.CID21-1970.B504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021out13-oc180-run5-b21-145-cid21-1970-snapshot/2021out13-oc180-run5-b21-145-cid21-1970-snapshot.B21-145.CID21-1970.B504_N704.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid21-1970',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2292-snapshot/2022out10-oc395-run1-b22-128-cid22-2292-snapshot.B22-128.CID22-2292.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2292-snapshot/2022out10-oc395-run1-b22-128-cid22-2292-snapshot.B22-128.CID22-2292.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid22-2292',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022fev28-gs180-run0-b22-28-cid22-497-snapshot/2022fev28-gs180-run0-b22-28-cid22-497-snapshot.B22-28.CID22-497.C539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022fev28-gs180-run0-b22-28-cid22-497-snapshot/2022fev28-gs180-run0-b22-28-cid22-497-snapshot.B22-28.CID22-497.C539_N707.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid22-497',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2283-snapshot/2022out10-oc395-run1-b22-128-cid22-2283-snapshot.B22-128.CID22-2283.B501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2283-snapshot/2022out10-oc395-run1-b22-128-cid22-2283-snapshot.B22-128.CID22-2283.B501_N701.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid22-2283',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mar04-gs180-run0-b22-30-cid22-460-snapshot/2022mar04-gs180-run0-b22-30-cid22-460-snapshot.B22-30.CID22-460.B526_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mar04-gs180-run0-b22-30-cid22-460-snapshot/2022mar04-gs180-run0-b22-30-cid22-460-snapshot.B22-30.CID22-460.B526_N702.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid22-460',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2287-snapshot/2022out10-oc395-run1-b22-128-cid22-2287-snapshot.B22-128.CID22-2287.B507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2287-snapshot/2022out10-oc395-run1-b22-128-cid22-2287-snapshot.B22-128.CID22-2287.B507_N707.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid22-2287',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022abr14-gs180-run1-b22-48-cid22-834-snapshot/2022abr14-gs180-run1-b22-48-cid22-834-snapshot.B22-48.CID22-834.B526_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022abr14-gs180-run1-b22-48-cid22-834-snapshot/2022abr14-gs180-run1-b22-48-cid22-834-snapshot.B22-48.CID22-834.B526_N702.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid22-834',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2285-snapshot/2022out10-oc395-run1-b22-128-cid22-2285-snapshot.B22-128.CID22-2285.B503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2285-snapshot/2022out10-oc395-run1-b22-128-cid22-2285-snapshot.B22-128.CID22-2285.B503_N703.coverage.summary.htqc.txt',
                        'sample_id': '9b-cid22-2285',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022abr14-gs180-run1-b22-48-cid22-889-snapshot/2022abr14-gs180-run1-b22-48-cid22-889-snapshot.B22-48.CID22-889.B528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022abr14-gs180-run1-b22-48-cid22-889-snapshot/2022abr14-gs180-run1-b22-48-cid22-889-snapshot.B22-48.CID22-889.B528_N704.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid22-889',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2284-snapshot/2022out10-oc395-run1-b22-128-cid22-2284-snapshot.B22-128.CID22-2284.B502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2284-snapshot/2022out10-oc395-run1-b22-128-cid22-2284-snapshot.B22-128.CID22-2284.B502_N702.coverage.summary.htqc.txt',
                        'sample_id': '10b-cid22-2284',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mai09-gs180-run1-b22-59-cid22-965-snapshot/2022mai09-gs180-run1-b22-59-cid22-965-snapshot.B22-59.CID22-965.B533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mai09-gs180-run1-b22-59-cid22-965-snapshot/2022mai09-gs180-run1-b22-59-cid22-965-snapshot.B22-59.CID22-965.B533_N701.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid22-965',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2289-snapshot/2022out10-oc395-run1-b22-128-cid22-2289-snapshot.B22-128.CID22-2289.B508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2289-snapshot/2022out10-oc395-run1-b22-128-cid22-2289-snapshot.B22-128.CID22-2289.B508_N708.coverage.summary.htqc.txt',
                        'sample_id': '11b-cid22-2289',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan13-gs180-run0-b22-7-cid22-15-snapshot/2022jan13-gs180-run0-b22-7-cid22-15-snapshot.B22-7.CID22-15.B520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan13-gs180-run0-b22-7-cid22-15-snapshot/2022jan13-gs180-run0-b22-7-cid22-15-snapshot.B22-7.CID22-15.B520_N704.coverage.summary.htqc.txt',
                        'sample_id': '12a-cid22-15',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2296-snapshot/2022out10-oc395-run1-b22-128-cid22-2296-snapshot.B22-128.CID22-2296.B516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2296-snapshot/2022out10-oc395-run1-b22-128-cid22-2296-snapshot.B22-128.CID22-2296.B516_N708.coverage.summary.htqc.txt',
                        'sample_id': '12b-cid22-2296',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mai27-gs180-run2-b22-67-cid22-1180-snapshot/2022mai27-gs180-run2-b22-67-cid22-1180-snapshot.B22-67.CID22-1180.B505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mai27-gs180-run2-b22-67-cid22-1180-snapshot/2022mai27-gs180-run2-b22-67-cid22-1180-snapshot.B22-67.CID22-1180.B505_N705.coverage.summary.htqc.txt',
                        'sample_id': '13a-cid22-1180',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2286-snapshot/2022out10-oc395-run1-b22-128-cid22-2286-snapshot.B22-128.CID22-2286.B504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2286-snapshot/2022out10-oc395-run1-b22-128-cid22-2286-snapshot.B22-128.CID22-2286.B504_N704.coverage.summary.htqc.txt',
                        'sample_id': '13b-cid22-2286',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jun03-gs180-run2-b22-73-cid22-1292-snapshot/2022jun03-gs180-run2-b22-73-cid22-1292-snapshot.B22-73.CID22-1292.B517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jun03-gs180-run2-b22-73-cid22-1292-snapshot/2022jun03-gs180-run2-b22-73-cid22-1292-snapshot.B22-73.CID22-1292.B517_N701.coverage.summary.htqc.txt',
                        'sample_id': '14a-cid22-1292',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2297-snapshot/2022out10-oc395-run1-b22-128-cid22-2297-snapshot.B22-128.CID22-2297.B505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2297-snapshot/2022out10-oc395-run1-b22-128-cid22-2297-snapshot.B22-128.CID22-2297.B505_N705.coverage.summary.htqc.txt',
                        'sample_id': '14b-cid22-2297',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022ago12-gs180-run3-b22-101-cid22-1757-snapshot/2022ago12-gs180-run3-b22-101-cid22-1757-snapshot.B22-101.CID22-1757.C535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022ago12-gs180-run3-b22-101-cid22-1757-snapshot/2022ago12-gs180-run3-b22-101-cid22-1757-snapshot.B22-101.CID22-1757.C535_N703.coverage.summary.htqc.txt',
                        'sample_id': '15a-cid22-1757',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2293-snapshot/2022out10-oc395-run1-b22-128-cid22-2293-snapshot.B22-128.CID22-2293.B513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2293-snapshot/2022out10-oc395-run1-b22-128-cid22-2293-snapshot.B22-128.CID22-2293.B513_N705.coverage.summary.htqc.txt',
                        'sample_id': '15b-cid22-2293',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan21-gs180-run0-b22-11-cid22-88-snapshot/2022jan21-gs180-run0-b22-11-cid22-88-snapshot.B22-11.CID22-88.B524_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan21-gs180-run0-b22-11-cid22-88-snapshot/2022jan21-gs180-run0-b22-11-cid22-88-snapshot.B22-11.CID22-88.B524_N708.coverage.summary.htqc.txt',
                        'sample_id': '16a-cid22-88',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2288-snapshot/2022out10-oc395-run1-b22-128-cid22-2288-snapshot.B22-128.CID22-2288.B506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2288-snapshot/2022out10-oc395-run1-b22-128-cid22-2288-snapshot.B22-128.CID22-2288.B506_N706.coverage.summary.htqc.txt',
                        'sample_id': '16b-cid22-2288',
                        'sample_type': ''}
                ]
            }
        }
    }
