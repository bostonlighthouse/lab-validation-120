
def get_oc395_snps_samples_hotspots():

    # The GS395 snps samples samples
    return {
        "amplicon_metrics": {
            "run": "oc395_snps_samples_hotspots",
            "images_dir": "images/oc395_snps_samples_hotspots/",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2291-snapshot/2022out10-oc395-run1-b22-128-cid22-2291-snapshot.B22-128.CID22-2291.B511_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-2291',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2300-snapshot/2022out10-oc395-run1-b22-128-cid22-2300-snapshot.B22-128.CID22-2300.B510_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-2300',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2294-snapshot/2022out10-oc395-run1-b22-128-cid22-2294-snapshot.B22-128.CID22-2294.B514_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-2294',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2295-snapshot/2022out10-oc395-run1-b22-128-cid22-2295-snapshot.B22-128.CID22-2295.B515_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-2295',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2290-snapshot/2022out10-oc395-run1-b22-128-cid22-2290-snapshot.B22-128.CID22-2290.B509_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-2290',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2292-snapshot/2022out10-oc395-run1-b22-128-cid22-2292-snapshot.B22-128.CID22-2292.B512_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-2292',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2283-snapshot/2022out10-oc395-run1-b22-128-cid22-2283-snapshot.B22-128.CID22-2283.B501_N701.hotspots.htqc.txt',
                        'sample_id': 'cid22-2283',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2287-snapshot/2022out10-oc395-run1-b22-128-cid22-2287-snapshot.B22-128.CID22-2287.B507_N707.hotspots.htqc.txt',
                        'sample_id': 'cid22-2287',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2285-snapshot/2022out10-oc395-run1-b22-128-cid22-2285-snapshot.B22-128.CID22-2285.B503_N703.hotspots.htqc.txt',
                        'sample_id': 'cid22-2285',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2284-snapshot/2022out10-oc395-run1-b22-128-cid22-2284-snapshot.B22-128.CID22-2284.B502_N702.hotspots.htqc.txt',
                        'sample_id': 'cid22-2284',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2289-snapshot/2022out10-oc395-run1-b22-128-cid22-2289-snapshot.B22-128.CID22-2289.B508_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-2289',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2296-snapshot/2022out10-oc395-run1-b22-128-cid22-2296-snapshot.B22-128.CID22-2296.B516_N708.hotspots.htqc.txt',
                        'sample_id': 'cid22-2296',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2286-snapshot/2022out10-oc395-run1-b22-128-cid22-2286-snapshot.B22-128.CID22-2286.B504_N704.hotspots.htqc.txt',
                        'sample_id': 'cid22-2286',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2297-snapshot/2022out10-oc395-run1-b22-128-cid22-2297-snapshot.B22-128.CID22-2297.B505_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-2297',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2293-snapshot/2022out10-oc395-run1-b22-128-cid22-2293-snapshot.B22-128.CID22-2293.B513_N705.hotspots.htqc.txt',
                        'sample_id': 'cid22-2293',
                        'sample_type': 'tumor'},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set//2022out10-oc395-run1-b22-128-cid22-2288-snapshot/2022out10-oc395-run1-b22-128-cid22-2288-snapshot.B22-128.CID22-2288.B506_N706.hotspots.htqc.txt',
                        'sample_id': 'cid22-2288',
                        'sample_type': 'tumor'}
                ]
            }
        }
    }


def get_oc180_samples_used_with_oc395_snps_samples_hotspots():

    # The GS395 snps samples samples
    return {
        "amplicon_metrics": {
            "run": "oc395_snps_samples_hotspots",
            "images_dir": "images/oc395_snps_samples_hotspots/",
            "input_files": {
                "group_id": "amplicon_metrics",
                "data": [
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez11-oc180-r60--b21-175-cid21-2645-snapshot/2021dez11-oc180-r60--b21-175-cid21-2645-snapshot.B21-175.CID21-2645.C541_N701.hotspots.htqc.txt',
                         'sample_id': 'cid21-2645',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez11-oc180-r60--b21-175-cid21-2625-snapshot/2021dez11-oc180-r60--b21-175-cid21-2625-snapshot.B21-175.CID21-2625.C534_N702.hotspots.htqc.txt',
                         'sample_id': 'cid21-2625',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021dez30-run63-gs18-b21-184-cid21-2723-snapshot/2021dez30-run63-gs18-b21-184-cid21-2723-snapshot.B21-184.CID21-2723.B522_N701.hotspots.htqc.txt',
                         'sample_id': 'cid21-2723',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan06-gs180-run0-b22-4-cid21-2819-snapshot/2022jan06-gs180-run0-b22-4-cid21-2819-snapshot.B22-4.CID21-2819.B507_N707.hotspots.htqc.txt',
                         'sample_id': 'cid21-2819',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot/2021-09-04-oc180-r44-b21-122-cid21-1665-snapshot.B21-122.CID21-1665.B535_N703.hotspots.htqc.txt',
                         'sample_id': 'cid21-1665',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2021out13-oc180-run5-b21-145-cid21-1970-snapshot/2021out13-oc180-run5-b21-145-cid21-1970-snapshot.B21-145.CID21-1970.B504_N704.hotspots.htqc.txt',
                         'sample_id': 'cid21-1970',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022fev28-gs180-run0-b22-28-cid22-497-snapshot/2022fev28-gs180-run0-b22-28-cid22-497-snapshot.B22-28.CID22-497.C539_N707.hotspots.htqc.txt',
                         'sample_id': 'cid22-497',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mar04-gs180-run0-b22-30-cid22-460-snapshot/2022mar04-gs180-run0-b22-30-cid22-460-snapshot.B22-30.CID22-460.B526_N702.hotspots.htqc.txt',
                         'sample_id': 'cid22-460',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022abr14-gs180-run1-b22-48-cid22-834-snapshot/2022abr14-gs180-run1-b22-48-cid22-834-snapshot.B22-48.CID22-834.B526_N702.hotspots.htqc.txt',
                         'sample_id': 'cid22-834',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022abr14-gs180-run1-b22-48-cid22-889-snapshot/2022abr14-gs180-run1-b22-48-cid22-889-snapshot.B22-48.CID22-889.B528_N704.hotspots.htqc.txt',
                         'sample_id': 'cid22-889',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mai09-gs180-run1-b22-59-cid22-965-snapshot/2022mai09-gs180-run1-b22-59-cid22-965-snapshot.B22-59.CID22-965.B533_N701.hotspots.htqc.txt',
                         'sample_id': 'cid22-965',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan13-gs180-run0-b22-7-cid22-15-snapshot/2022jan13-gs180-run0-b22-7-cid22-15-snapshot.B22-7.CID22-15.B520_N704.hotspots.htqc.txt',
                         'sample_id': 'cid22-15',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022mai27-gs180-run2-b22-67-cid22-1180-snapshot/2022mai27-gs180-run2-b22-67-cid22-1180-snapshot.B22-67.CID22-1180.B505_N705.hotspots.htqc.txt',
                         'sample_id': 'cid22-1180',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jun03-gs180-run2-b22-73-cid22-1292-snapshot/2022jun03-gs180-run2-b22-73-cid22-1292-snapshot.B22-73.CID22-1292.B517_N701.hotspots.htqc.txt',
                         'sample_id': 'cid22-1292',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022ago12-gs180-run3-b22-101-cid22-1757-snapshot/2022ago12-gs180-run3-b22-101-cid22-1757-snapshot.B22-101.CID22-1757.C535_N703.hotspots.htqc.txt',
                         'sample_id': 'cid22-1757',
                         'sample_type': 'tumor'},
                    {    'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/validation_set/gs180//2022jan21-gs180-run0-b22-11-cid22-88-snapshot/2022jan21-gs180-run0-b22-11-cid22-88-snapshot.B22-11.CID22-88.B524_N708.hotspots.htqc.txt',
                         'sample_id': 'cid22-88',
                         'sample_type': 'tumor'}
                ]
            }
        }
    }
