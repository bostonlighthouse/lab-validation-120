"""    Get the QC files for the GS395 pon vs GS395 + SNPs pon """
from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds



def get_oc395_pon_vs_oc395snps_pon_htqc_plots():
    """
    Get the QC files for the GS395 pon vs GS395 + SNPs pon
    """

    key_2_use = 'oc395_pon_vs_oc395_snps_pon'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS395 PON vs. GS395 SNPs PON",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid21-1484-snapshot/2021dez13-oc395-r07--b22-2-cid21-1484-snapshot.B22-2.CID21-1484.B527_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid21-1484-snapshot/2021dez13-oc395-r07--b22-2-cid21-1484-snapshot.B22-2.CID21-1484.B527_N703.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid21-1484',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2047-snapshot/2022set10-oc395-run0-b22-118-cid22-2047-snapshot.B22-118.CID22-2047.C501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2047-snapshot/2022set10-oc395-run0-b22-118-cid22-2047-snapshot.B22-118.CID22-2047.C501_N701.coverage.summary.htqc.txt',
                        'sample_id': '1b-cid22-2047',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2742-snapshot/2021dez16-oc395-run0-b21-178-cid21-2742-snapshot.B21-178.CID21-2742.B509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2742-snapshot/2021dez16-oc395-run0-b21-178-cid21-2742-snapshot.B21-178.CID21-2742.B509_N701.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid21-2742',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2048-snapshot/2022set10-oc395-run0-b22-118-cid22-2048-snapshot.B22-118.CID22-2048.C502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2048-snapshot/2022set10-oc395-run0-b22-118-cid22-2048-snapshot.B22-118.CID22-2048.C502_N702.coverage.summary.htqc.txt',
                        'sample_id': '2b-cid22-2048',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2743-snapshot/2021dez16-oc395-run0-b21-178-cid21-2743-snapshot.B21-178.CID21-2743.B510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2743-snapshot/2021dez16-oc395-run0-b21-178-cid21-2743-snapshot.B21-178.CID21-2743.B510_N702.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid21-2743',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2049-snapshot/2022set10-oc395-run0-b22-118-cid22-2049-snapshot.B22-118.CID22-2049.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2049-snapshot/2022set10-oc395-run0-b22-118-cid22-2049-snapshot.B22-118.CID22-2049.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '3b-cid22-2049',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2744-snapshot/2021dez16-oc395-run0-b21-178-cid21-2744-snapshot.B21-178.CID21-2744.B511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2744-snapshot/2021dez16-oc395-run0-b21-178-cid21-2744-snapshot.B21-178.CID21-2744.B511_N703.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid21-2744',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2050-snapshot/2022set10-oc395-run0-b22-118-cid22-2050-snapshot.B22-118.CID22-2050.C505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2050-snapshot/2022set10-oc395-run0-b22-118-cid22-2050-snapshot.B22-118.CID22-2050.C505_N705.coverage.summary.htqc.txt',
                        'sample_id': '4b-cid22-2050',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2745-snapshot/2021dez16-oc395-run0-b21-178-cid21-2745-snapshot.B21-178.CID21-2745.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2745-snapshot/2021dez16-oc395-run0-b21-178-cid21-2745-snapshot.B21-178.CID21-2745.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid21-2745',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2051-snapshot/2022set10-oc395-run0-b22-118-cid22-2051-snapshot.B22-118.CID22-2051.C504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2051-snapshot/2022set10-oc395-run0-b22-118-cid22-2051-snapshot.B22-118.CID22-2051.C504_N704.coverage.summary.htqc.txt',
                        'sample_id': '5b-cid22-2051',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2746-snapshot/2021dez16-oc395-run0-b21-178-cid21-2746-snapshot.B21-178.CID21-2746.B513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2746-snapshot/2021dez16-oc395-run0-b21-178-cid21-2746-snapshot.B21-178.CID21-2746.B513_N705.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid21-2746',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2052-snapshot/2022set10-oc395-run0-b22-118-cid22-2052-snapshot.B22-118.CID22-2052.C507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2052-snapshot/2022set10-oc395-run0-b22-118-cid22-2052-snapshot.B22-118.CID22-2052.C507_N707.coverage.summary.htqc.txt',
                        'sample_id': '6b-cid22-2052',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2755-snapshot/2021dez16-oc395-run0-b21-178-cid21-2755-snapshot.B21-178.CID21-2755.B514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2755-snapshot/2021dez16-oc395-run0-b21-178-cid21-2755-snapshot.B21-178.CID21-2755.B514_N706.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid21-2755',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2053-snapshot/2022set10-oc395-run0-b22-118-cid22-2053-snapshot.B22-118.CID22-2053.C506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2053-snapshot/2022set10-oc395-run0-b22-118-cid22-2053-snapshot.B22-118.CID22-2053.C506_N706.coverage.summary.htqc.txt',
                        'sample_id': '7b-cid22-2053',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2756-snapshot/2021dez16-oc395-run0-b21-178-cid21-2756-snapshot.B21-178.CID21-2756.B515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2756-snapshot/2021dez16-oc395-run0-b21-178-cid21-2756-snapshot.B21-178.CID21-2756.B515_N707.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid21-2756',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2054-snapshot/2022set10-oc395-run0-b22-118-cid22-2054-snapshot.B22-118.CID22-2054.C508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2054-snapshot/2022set10-oc395-run0-b22-118-cid22-2054-snapshot.B22-118.CID22-2054.C508_N708.coverage.summary.htqc.txt',
                        'sample_id': '8b-cid22-2054',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2757-snapshot/2021dez16-oc395-run0-b21-178-cid21-2757-snapshot.B21-178.CID21-2757.B516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2757-snapshot/2021dez16-oc395-run0-b21-178-cid21-2757-snapshot.B21-178.CID21-2757.B516_N708.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid21-2757',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2055-snapshot/2022set10-oc395-run0-b22-118-cid22-2055-snapshot.B22-118.CID22-2055.C509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2055-snapshot/2022set10-oc395-run0-b22-118-cid22-2055-snapshot.B22-118.CID22-2055.C509_N701.coverage.summary.htqc.txt',
                        'sample_id': '9b-cid22-2055',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2758-snapshot/2021dez16-oc395-run0-b21-178-cid21-2758-snapshot.B21-178.CID21-2758.B517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2758-snapshot/2021dez16-oc395-run0-b21-178-cid21-2758-snapshot.B21-178.CID21-2758.B517_N701.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid21-2758',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2056-snapshot/2022set10-oc395-run0-b22-118-cid22-2056-snapshot.B22-118.CID22-2056.C510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2056-snapshot/2022set10-oc395-run0-b22-118-cid22-2056-snapshot.B22-118.CID22-2056.C510_N702.coverage.summary.htqc.txt',
                        'sample_id': '10b-cid22-2056',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2759-snapshot/2021dez16-oc395-run0-b21-178-cid21-2759-snapshot.B21-178.CID21-2759.B518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2759-snapshot/2021dez16-oc395-run0-b21-178-cid21-2759-snapshot.B21-178.CID21-2759.B518_N702.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid21-2759',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2057-snapshot/2022set10-oc395-run0-b22-118-cid22-2057-snapshot.B22-118.CID22-2057.C511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2057-snapshot/2022set10-oc395-run0-b22-118-cid22-2057-snapshot.B22-118.CID22-2057.C511_N703.coverage.summary.htqc.txt',
                        'sample_id': '11b-cid22-2057',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2760-snapshot/2021dez16-oc395-run0-b21-178-cid21-2760-snapshot.B21-178.CID21-2760.B519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2760-snapshot/2021dez16-oc395-run0-b21-178-cid21-2760-snapshot.B21-178.CID21-2760.B519_N703.coverage.summary.htqc.txt',
                        'sample_id': '12a-cid21-2760',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2058-snapshot/2022set10-oc395-run0-b22-118-cid22-2058-snapshot.B22-118.CID22-2058.C512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2058-snapshot/2022set10-oc395-run0-b22-118-cid22-2058-snapshot.B22-118.CID22-2058.C512_N704.coverage.summary.htqc.txt',
                        'sample_id': '12b-cid22-2058',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2761-snapshot/2021dez16-oc395-run0-b21-178-cid21-2761-snapshot.B21-178.CID21-2761.B520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2761-snapshot/2021dez16-oc395-run0-b21-178-cid21-2761-snapshot.B21-178.CID21-2761.B520_N704.coverage.summary.htqc.txt',
                        'sample_id': '13a-cid21-2761',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2059-snapshot/2022set10-oc395-run0-b22-118-cid22-2059-snapshot.B22-118.CID22-2059.C513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2059-snapshot/2022set10-oc395-run0-b22-118-cid22-2059-snapshot.B22-118.CID22-2059.C513_N705.coverage.summary.htqc.txt',
                        'sample_id': '13b-cid22-2059',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2762-snapshot/2021dez16-oc395-run0-b21-178-cid21-2762-snapshot.B21-178.CID21-2762.B521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2762-snapshot/2021dez16-oc395-run0-b21-178-cid21-2762-snapshot.B21-178.CID21-2762.B521_N705.coverage.summary.htqc.txt',
                        'sample_id': '14a-cid21-2762',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2060-snapshot/2022set10-oc395-run0-b22-118-cid22-2060-snapshot.B22-118.CID22-2060.C516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2060-snapshot/2022set10-oc395-run0-b22-118-cid22-2060-snapshot.B22-118.CID22-2060.C516_N708.coverage.summary.htqc.txt',
                        'sample_id': '14b-cid22-2060',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2763-snapshot/2021dez16-oc395-run0-b21-178-cid21-2763-snapshot.B21-178.CID21-2763.B522_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2763-snapshot/2021dez16-oc395-run0-b21-178-cid21-2763-snapshot.B21-178.CID21-2763.B522_N706.coverage.summary.htqc.txt',
                        'sample_id': '15a-cid21-2763',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2061-snapshot/2022set10-oc395-run0-b22-118-cid22-2061-snapshot.B22-118.CID22-2061.C518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2061-snapshot/2022set10-oc395-run0-b22-118-cid22-2061-snapshot.B22-118.CID22-2061.C518_N702.coverage.summary.htqc.txt',
                        'sample_id': '15b-cid22-2061',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2764-snapshot/2021dez16-oc395-run0-b21-178-cid21-2764-snapshot.B21-178.CID21-2764.B523_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2764-snapshot/2021dez16-oc395-run0-b21-178-cid21-2764-snapshot.B21-178.CID21-2764.B523_N707.coverage.summary.htqc.txt',
                        'sample_id': '16a-cid21-2764',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2062-snapshot/2022set10-oc395-run0-b22-118-cid22-2062-snapshot.B22-118.CID22-2062.C517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2062-snapshot/2022set10-oc395-run0-b22-118-cid22-2062-snapshot.B22-118.CID22-2062.C517_N701.coverage.summary.htqc.txt',
                        'sample_id': '16b-cid22-2062',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2765-snapshot/2021dez16-oc395-run0-b21-178-cid21-2765-snapshot.B21-178.CID21-2765.B524_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez16-oc395-run0-b21-178-cid21-2765-snapshot/2021dez16-oc395-run0-b21-178-cid21-2765-snapshot.B21-178.CID21-2765.B524_N708.coverage.summary.htqc.txt',
                        'sample_id': '17a-cid21-2765',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2063-snapshot/2022set10-oc395-run0-b22-119-cid22-2063-snapshot.B22-119.CID22-2063.C519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2063-snapshot/2022set10-oc395-run0-b22-119-cid22-2063-snapshot.B22-119.CID22-2063.C519_N703.coverage.summary.htqc.txt',
                        'sample_id': '17b-cid22-2063',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-24-snapshot/2021dez13-oc395-r07--b22-2-cid22-24-snapshot.B22-2.CID22-24.B529_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-24-snapshot/2021dez13-oc395-r07--b22-2-cid22-24-snapshot.B22-2.CID22-24.B529_N705.coverage.summary.htqc.txt',
                        'sample_id': '18a-cid22-24',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2064-snapshot/2022set10-oc395-run0-b22-119-cid22-2064-snapshot.B22-119.CID22-2064.C520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2064-snapshot/2022set10-oc395-run0-b22-119-cid22-2064-snapshot.B22-119.CID22-2064.C520_N704.coverage.summary.htqc.txt',
                        'sample_id': '18b-cid22-2064',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-26-snapshot/2021dez13-oc395-r07--b22-2-cid22-26-snapshot.B22-2.CID22-26.B530_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-26-snapshot/2021dez13-oc395-r07--b22-2-cid22-26-snapshot.B22-2.CID22-26.B530_N706.coverage.summary.htqc.txt',
                        'sample_id': '19a-cid22-26',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2065-snapshot/2022set10-oc395-run0-b22-119-cid22-2065-snapshot.B22-119.CID22-2065.C521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2065-snapshot/2022set10-oc395-run0-b22-119-cid22-2065-snapshot.B22-119.CID22-2065.C521_N705.coverage.summary.htqc.txt',
                        'sample_id': '19b-cid22-2065',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-27-snapshot/2021dez13-oc395-r07--b22-2-cid22-27-snapshot.B22-2.CID22-27.B531_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-27-snapshot/2021dez13-oc395-r07--b22-2-cid22-27-snapshot.B22-2.CID22-27.B531_N707.coverage.summary.htqc.txt',
                        'sample_id': '20a-cid22-27',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2066-snapshot/2022set10-oc395-run0-b22-119-cid22-2066-snapshot.B22-119.CID22-2066.C522_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2066-snapshot/2022set10-oc395-run0-b22-119-cid22-2066-snapshot.B22-119.CID22-2066.C522_N706.coverage.summary.htqc.txt',
                        'sample_id': '20b-cid22-2066',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-28-snapshot/2021dez13-oc395-r07--b22-2-cid22-28-snapshot.B22-2.CID22-28.B532_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-28-snapshot/2021dez13-oc395-r07--b22-2-cid22-28-snapshot.B22-2.CID22-28.B532_N708.coverage.summary.htqc.txt',
                        'sample_id': '21a-cid22-28',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2067-snapshot/2022set10-oc395-run0-b22-119-cid22-2067-snapshot.B22-119.CID22-2067.C523_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2067-snapshot/2022set10-oc395-run0-b22-119-cid22-2067-snapshot.B22-119.CID22-2067.C523_N707.coverage.summary.htqc.txt',
                        'sample_id': '21b-cid22-2067',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-29-snapshot/2021dez13-oc395-r07--b22-2-cid22-29-snapshot.B22-2.CID22-29.B533_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-29-snapshot/2021dez13-oc395-r07--b22-2-cid22-29-snapshot.B22-2.CID22-29.B533_N701.coverage.summary.htqc.txt',
                        'sample_id': '22a-cid22-29',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2068-snapshot/2022set10-oc395-run0-b22-119-cid22-2068-snapshot.B22-119.CID22-2068.C524_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2068-snapshot/2022set10-oc395-run0-b22-119-cid22-2068-snapshot.B22-119.CID22-2068.C524_N708.coverage.summary.htqc.txt',
                        'sample_id': '22b-cid22-2068',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-30-snapshot/2021dez13-oc395-r07--b22-2-cid22-30-snapshot.B22-2.CID22-30.B534_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-30-snapshot/2021dez13-oc395-r07--b22-2-cid22-30-snapshot.B22-2.CID22-30.B534_N702.coverage.summary.htqc.txt',
                        'sample_id': '23a-cid22-30',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2069-snapshot/2022set10-oc395-run0-b22-119-cid22-2069-snapshot.B22-119.CID22-2069.C525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2069-snapshot/2022set10-oc395-run0-b22-119-cid22-2069-snapshot.B22-119.CID22-2069.C525_N701.coverage.summary.htqc.txt',
                        'sample_id': '23b-cid22-2069',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-31-snapshot/2021dez13-oc395-r07--b22-2-cid22-31-snapshot.B22-2.CID22-31.B535_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-31-snapshot/2021dez13-oc395-r07--b22-2-cid22-31-snapshot.B22-2.CID22-31.B535_N703.coverage.summary.htqc.txt',
                        'sample_id': '24a-cid22-31',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2070-snapshot/2022set10-oc395-run0-b22-119-cid22-2070-snapshot.B22-119.CID22-2070.C526_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2070-snapshot/2022set10-oc395-run0-b22-119-cid22-2070-snapshot.B22-119.CID22-2070.C526_N702.coverage.summary.htqc.txt',
                        'sample_id': '24b-cid22-2070',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-32-snapshot/2021dez13-oc395-r07--b22-2-cid22-32-snapshot.B22-2.CID22-32.B536_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-32-snapshot/2021dez13-oc395-r07--b22-2-cid22-32-snapshot.B22-2.CID22-32.B536_N704.coverage.summary.htqc.txt',
                        'sample_id': '25a-cid22-32',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2071-snapshot/2022set10-oc395-run0-b22-119-cid22-2071-snapshot.B22-119.CID22-2071.C527_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2071-snapshot/2022set10-oc395-run0-b22-119-cid22-2071-snapshot.B22-119.CID22-2071.C527_N703.coverage.summary.htqc.txt',
                        'sample_id': '25b-cid22-2071',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-33-snapshot/2021dez13-oc395-r07--b22-2-cid22-33-snapshot.B22-2.CID22-33.B537_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-33-snapshot/2021dez13-oc395-r07--b22-2-cid22-33-snapshot.B22-2.CID22-33.B537_N705.coverage.summary.htqc.txt',
                        'sample_id': '26a-cid22-33',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2072-snapshot/2022set10-oc395-run0-b22-119-cid22-2072-snapshot.B22-119.CID22-2072.C515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2072-snapshot/2022set10-oc395-run0-b22-119-cid22-2072-snapshot.B22-119.CID22-2072.C515_N707.coverage.summary.htqc.txt',
                        'sample_id': '26b-cid22-2072',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-34-snapshot/2021dez13-oc395-r07--b22-2-cid22-34-snapshot.B22-2.CID22-34.B538_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-34-snapshot/2021dez13-oc395-r07--b22-2-cid22-34-snapshot.B22-2.CID22-34.B538_N706.coverage.summary.htqc.txt',
                        'sample_id': '27a-cid22-34',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2073-snapshot/2022set10-oc395-run0-b22-119-cid22-2073-snapshot.B22-119.CID22-2073.C514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2073-snapshot/2022set10-oc395-run0-b22-119-cid22-2073-snapshot.B22-119.CID22-2073.C514_N706.coverage.summary.htqc.txt',
                        'sample_id': '27b-cid22-2073',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-35-snapshot/2021dez13-oc395-r07--b22-2-cid22-35-snapshot.B22-2.CID22-35.B539_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-35-snapshot/2021dez13-oc395-r07--b22-2-cid22-35-snapshot.B22-2.CID22-35.B539_N707.coverage.summary.htqc.txt',
                        'sample_id': '28a-cid22-35',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2074-snapshot/2022set10-oc395-run0-b22-119-cid22-2074-snapshot.B22-119.CID22-2074.C528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2074-snapshot/2022set10-oc395-run0-b22-119-cid22-2074-snapshot.B22-119.CID22-2074.C528_N704.coverage.summary.htqc.txt',
                        'sample_id': '28b-cid22-2074',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-36-snapshot/2021dez13-oc395-r07--b22-2-cid22-36-snapshot.B22-2.CID22-36.B540_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2021dez13-oc395-r07--b22-2-cid22-36-snapshot/2021dez13-oc395-r07--b22-2-cid22-36-snapshot.B22-2.CID22-36.B540_N708.coverage.summary.htqc.txt',
                        'sample_id': '29a-cid22-36',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2283-snapshot/2022out10-oc395-run1-b22-128-cid22-2283-snapshot.B22-128.CID22-2283.B501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2283-snapshot/2022out10-oc395-run1-b22-128-cid22-2283-snapshot.B22-128.CID22-2283.B501_N701.coverage.summary.htqc.txt',
                        'sample_id': '29b-cid22-2283',
                        'sample_type': ''}
                ]
            }
        }
    }


def get_oc395snps_pon_htqc_plots():
    """
    Get the QC files for the GS395 + SNPs pon
    """

    key_2_use = 'oc395_snps_pon'
    # Do not change this data-structure format..., values can be changed, but overall structure cannot be modified
    return {
        key_2_use: {
            "run": "run1-2",
            # the distribution data will need some configuring, these values were set via trial and error.  Do not
            # change
            "distribution_config": [
                {
                    'df_col_metric_to_plot': 'MEAN_COLLAPSED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MCC',
                    'xlabel': 'Amplicon Mean Collapsed Coverage (MCC)',
                    'x_upper_lim': 700,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_ABS_COVERAGE',
                    'acronym_of_metric_to_plot': 'MAC',
                    'xlabel': 'Amplicon Mean Absolute Coverage (MAC)',
                    'x_upper_lim': 3000,
                    'y_upper_lim': 3200,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 100,
                    'normal_bins': 25,
                },
                {
                    'df_col_metric_to_plot': 'MEAN_FILTERED_COVERAGE',
                    'acronym_of_metric_to_plot': 'MFC',
                    'xlabel': 'Amplicon Mean Filtered Coverage (MFC)',
                    'x_upper_lim': 1200,
                    'y_upper_lim': 1600,
                    'sample_size_normal_dist': 7550,
                    'coverage_bins': 25,
                    'normal_bins': 25,
                }
            ],
            "images_dir": f"images/{key_2_use}",
            "input_files": {
                "group_id": key_2_use,  # eventually used for a file name
                "chart_title": "GS395 SNPs PON",
                "data": [
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2047-snapshot/2022set10-oc395-run0-b22-118-cid22-2047-snapshot.B22-118.CID22-2047.C501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2047-snapshot/2022set10-oc395-run0-b22-118-cid22-2047-snapshot.B22-118.CID22-2047.C501_N701.coverage.summary.htqc.txt',
                        'sample_id': '1a-cid22-2047',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2048-snapshot/2022set10-oc395-run0-b22-118-cid22-2048-snapshot.B22-118.CID22-2048.C502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2048-snapshot/2022set10-oc395-run0-b22-118-cid22-2048-snapshot.B22-118.CID22-2048.C502_N702.coverage.summary.htqc.txt',
                        'sample_id': '2a-cid22-2048',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2049-snapshot/2022set10-oc395-run0-b22-118-cid22-2049-snapshot.B22-118.CID22-2049.C503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2049-snapshot/2022set10-oc395-run0-b22-118-cid22-2049-snapshot.B22-118.CID22-2049.C503_N703.coverage.summary.htqc.txt',
                        'sample_id': '3a-cid22-2049',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2050-snapshot/2022set10-oc395-run0-b22-118-cid22-2050-snapshot.B22-118.CID22-2050.C505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2050-snapshot/2022set10-oc395-run0-b22-118-cid22-2050-snapshot.B22-118.CID22-2050.C505_N705.coverage.summary.htqc.txt',
                        'sample_id': '4a-cid22-2050',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2051-snapshot/2022set10-oc395-run0-b22-118-cid22-2051-snapshot.B22-118.CID22-2051.C504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2051-snapshot/2022set10-oc395-run0-b22-118-cid22-2051-snapshot.B22-118.CID22-2051.C504_N704.coverage.summary.htqc.txt',
                        'sample_id': '5a-cid22-2051',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2052-snapshot/2022set10-oc395-run0-b22-118-cid22-2052-snapshot.B22-118.CID22-2052.C507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2052-snapshot/2022set10-oc395-run0-b22-118-cid22-2052-snapshot.B22-118.CID22-2052.C507_N707.coverage.summary.htqc.txt',
                        'sample_id': '6a-cid22-2052',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2053-snapshot/2022set10-oc395-run0-b22-118-cid22-2053-snapshot.B22-118.CID22-2053.C506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2053-snapshot/2022set10-oc395-run0-b22-118-cid22-2053-snapshot.B22-118.CID22-2053.C506_N706.coverage.summary.htqc.txt',
                        'sample_id': '7a-cid22-2053',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2054-snapshot/2022set10-oc395-run0-b22-118-cid22-2054-snapshot.B22-118.CID22-2054.C508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2054-snapshot/2022set10-oc395-run0-b22-118-cid22-2054-snapshot.B22-118.CID22-2054.C508_N708.coverage.summary.htqc.txt',
                        'sample_id': '8a-cid22-2054',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2055-snapshot/2022set10-oc395-run0-b22-118-cid22-2055-snapshot.B22-118.CID22-2055.C509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2055-snapshot/2022set10-oc395-run0-b22-118-cid22-2055-snapshot.B22-118.CID22-2055.C509_N701.coverage.summary.htqc.txt',
                        'sample_id': '9a-cid22-2055',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2056-snapshot/2022set10-oc395-run0-b22-118-cid22-2056-snapshot.B22-118.CID22-2056.C510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2056-snapshot/2022set10-oc395-run0-b22-118-cid22-2056-snapshot.B22-118.CID22-2056.C510_N702.coverage.summary.htqc.txt',
                        'sample_id': '10a-cid22-2056',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2057-snapshot/2022set10-oc395-run0-b22-118-cid22-2057-snapshot.B22-118.CID22-2057.C511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2057-snapshot/2022set10-oc395-run0-b22-118-cid22-2057-snapshot.B22-118.CID22-2057.C511_N703.coverage.summary.htqc.txt',
                        'sample_id': '11a-cid22-2057',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2058-snapshot/2022set10-oc395-run0-b22-118-cid22-2058-snapshot.B22-118.CID22-2058.C512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2058-snapshot/2022set10-oc395-run0-b22-118-cid22-2058-snapshot.B22-118.CID22-2058.C512_N704.coverage.summary.htqc.txt',
                        'sample_id': '12a-cid22-2058',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2059-snapshot/2022set10-oc395-run0-b22-118-cid22-2059-snapshot.B22-118.CID22-2059.C513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2059-snapshot/2022set10-oc395-run0-b22-118-cid22-2059-snapshot.B22-118.CID22-2059.C513_N705.coverage.summary.htqc.txt',
                        'sample_id': '13a-cid22-2059',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2060-snapshot/2022set10-oc395-run0-b22-118-cid22-2060-snapshot.B22-118.CID22-2060.C516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2060-snapshot/2022set10-oc395-run0-b22-118-cid22-2060-snapshot.B22-118.CID22-2060.C516_N708.coverage.summary.htqc.txt',
                        'sample_id': '14a-cid22-2060',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2061-snapshot/2022set10-oc395-run0-b22-118-cid22-2061-snapshot.B22-118.CID22-2061.C518_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2061-snapshot/2022set10-oc395-run0-b22-118-cid22-2061-snapshot.B22-118.CID22-2061.C518_N702.coverage.summary.htqc.txt',
                        'sample_id': '15a-cid22-2061',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2062-snapshot/2022set10-oc395-run0-b22-118-cid22-2062-snapshot.B22-118.CID22-2062.C517_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-118-cid22-2062-snapshot/2022set10-oc395-run0-b22-118-cid22-2062-snapshot.B22-118.CID22-2062.C517_N701.coverage.summary.htqc.txt',
                        'sample_id': '16a-cid22-2062',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2063-snapshot/2022set10-oc395-run0-b22-119-cid22-2063-snapshot.B22-119.CID22-2063.C519_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2063-snapshot/2022set10-oc395-run0-b22-119-cid22-2063-snapshot.B22-119.CID22-2063.C519_N703.coverage.summary.htqc.txt',
                        'sample_id': '17a-cid22-2063',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2064-snapshot/2022set10-oc395-run0-b22-119-cid22-2064-snapshot.B22-119.CID22-2064.C520_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2064-snapshot/2022set10-oc395-run0-b22-119-cid22-2064-snapshot.B22-119.CID22-2064.C520_N704.coverage.summary.htqc.txt',
                        'sample_id': '18a-cid22-2064',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2065-snapshot/2022set10-oc395-run0-b22-119-cid22-2065-snapshot.B22-119.CID22-2065.C521_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2065-snapshot/2022set10-oc395-run0-b22-119-cid22-2065-snapshot.B22-119.CID22-2065.C521_N705.coverage.summary.htqc.txt',
                        'sample_id': '19a-cid22-2065',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2066-snapshot/2022set10-oc395-run0-b22-119-cid22-2066-snapshot.B22-119.CID22-2066.C522_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2066-snapshot/2022set10-oc395-run0-b22-119-cid22-2066-snapshot.B22-119.CID22-2066.C522_N706.coverage.summary.htqc.txt',
                        'sample_id': '20a-cid22-2066',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2067-snapshot/2022set10-oc395-run0-b22-119-cid22-2067-snapshot.B22-119.CID22-2067.C523_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2067-snapshot/2022set10-oc395-run0-b22-119-cid22-2067-snapshot.B22-119.CID22-2067.C523_N707.coverage.summary.htqc.txt',
                        'sample_id': '21a-cid22-2067',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2068-snapshot/2022set10-oc395-run0-b22-119-cid22-2068-snapshot.B22-119.CID22-2068.C524_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2068-snapshot/2022set10-oc395-run0-b22-119-cid22-2068-snapshot.B22-119.CID22-2068.C524_N708.coverage.summary.htqc.txt',
                        'sample_id': '22a-cid22-2068',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2069-snapshot/2022set10-oc395-run0-b22-119-cid22-2069-snapshot.B22-119.CID22-2069.C525_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2069-snapshot/2022set10-oc395-run0-b22-119-cid22-2069-snapshot.B22-119.CID22-2069.C525_N701.coverage.summary.htqc.txt',
                        'sample_id': '23a-cid22-2069',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2070-snapshot/2022set10-oc395-run0-b22-119-cid22-2070-snapshot.B22-119.CID22-2070.C526_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2070-snapshot/2022set10-oc395-run0-b22-119-cid22-2070-snapshot.B22-119.CID22-2070.C526_N702.coverage.summary.htqc.txt',
                        'sample_id': '24a-cid22-2070',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2071-snapshot/2022set10-oc395-run0-b22-119-cid22-2071-snapshot.B22-119.CID22-2071.C527_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2071-snapshot/2022set10-oc395-run0-b22-119-cid22-2071-snapshot.B22-119.CID22-2071.C527_N703.coverage.summary.htqc.txt',
                        'sample_id': '25a-cid22-2071',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2072-snapshot/2022set10-oc395-run0-b22-119-cid22-2072-snapshot.B22-119.CID22-2072.C515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2072-snapshot/2022set10-oc395-run0-b22-119-cid22-2072-snapshot.B22-119.CID22-2072.C515_N707.coverage.summary.htqc.txt',
                        'sample_id': '26a-cid22-2072',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2073-snapshot/2022set10-oc395-run0-b22-119-cid22-2073-snapshot.B22-119.CID22-2073.C514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2073-snapshot/2022set10-oc395-run0-b22-119-cid22-2073-snapshot.B22-119.CID22-2073.C514_N706.coverage.summary.htqc.txt',
                        'sample_id': '27a-cid22-2073',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2074-snapshot/2022set10-oc395-run0-b22-119-cid22-2074-snapshot.B22-119.CID22-2074.C528_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022set10-oc395-run0-b22-119-cid22-2074-snapshot/2022set10-oc395-run0-b22-119-cid22-2074-snapshot.B22-119.CID22-2074.C528_N704.coverage.summary.htqc.txt',
                        'sample_id': '28a-cid22-2074',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2283-snapshot/2022out10-oc395-run1-b22-128-cid22-2283-snapshot.B22-128.CID22-2283.B501_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2283-snapshot/2022out10-oc395-run1-b22-128-cid22-2283-snapshot.B22-128.CID22-2283.B501_N701.coverage.summary.htqc.txt',
                        'sample_id': '29a-cid22-2283',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2284-snapshot/2022out10-oc395-run1-b22-128-cid22-2284-snapshot.B22-128.CID22-2284.B502_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2284-snapshot/2022out10-oc395-run1-b22-128-cid22-2284-snapshot.B22-128.CID22-2284.B502_N702.coverage.summary.htqc.txt',
                        'sample_id': '30a-cid22-2284',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2285-snapshot/2022out10-oc395-run1-b22-128-cid22-2285-snapshot.B22-128.CID22-2285.B503_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2285-snapshot/2022out10-oc395-run1-b22-128-cid22-2285-snapshot.B22-128.CID22-2285.B503_N703.coverage.summary.htqc.txt',
                        'sample_id': '31a-cid22-2285',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2286-snapshot/2022out10-oc395-run1-b22-128-cid22-2286-snapshot.B22-128.CID22-2286.B504_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2286-snapshot/2022out10-oc395-run1-b22-128-cid22-2286-snapshot.B22-128.CID22-2286.B504_N704.coverage.summary.htqc.txt',
                        'sample_id': '32a-cid22-2286',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2287-snapshot/2022out10-oc395-run1-b22-128-cid22-2287-snapshot.B22-128.CID22-2287.B507_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2287-snapshot/2022out10-oc395-run1-b22-128-cid22-2287-snapshot.B22-128.CID22-2287.B507_N707.coverage.summary.htqc.txt',
                        'sample_id': '33a-cid22-2287',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2288-snapshot/2022out10-oc395-run1-b22-128-cid22-2288-snapshot.B22-128.CID22-2288.B506_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2288-snapshot/2022out10-oc395-run1-b22-128-cid22-2288-snapshot.B22-128.CID22-2288.B506_N706.coverage.summary.htqc.txt',
                        'sample_id': '34a-cid22-2288',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2289-snapshot/2022out10-oc395-run1-b22-128-cid22-2289-snapshot.B22-128.CID22-2289.B508_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2289-snapshot/2022out10-oc395-run1-b22-128-cid22-2289-snapshot.B22-128.CID22-2289.B508_N708.coverage.summary.htqc.txt',
                        'sample_id': '35a-cid22-2289',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2290-snapshot/2022out10-oc395-run1-b22-128-cid22-2290-snapshot.B22-128.CID22-2290.B509_N701.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2290-snapshot/2022out10-oc395-run1-b22-128-cid22-2290-snapshot.B22-128.CID22-2290.B509_N701.coverage.summary.htqc.txt',
                        'sample_id': '36a-cid22-2290',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2291-snapshot/2022out10-oc395-run1-b22-128-cid22-2291-snapshot.B22-128.CID22-2291.B511_N703.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2291-snapshot/2022out10-oc395-run1-b22-128-cid22-2291-snapshot.B22-128.CID22-2291.B511_N703.coverage.summary.htqc.txt',
                        'sample_id': '37a-cid22-2291',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2292-snapshot/2022out10-oc395-run1-b22-128-cid22-2292-snapshot.B22-128.CID22-2292.B512_N704.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2292-snapshot/2022out10-oc395-run1-b22-128-cid22-2292-snapshot.B22-128.CID22-2292.B512_N704.coverage.summary.htqc.txt',
                        'sample_id': '38a-cid22-2292',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2293-snapshot/2022out10-oc395-run1-b22-128-cid22-2293-snapshot.B22-128.CID22-2293.B513_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2293-snapshot/2022out10-oc395-run1-b22-128-cid22-2293-snapshot.B22-128.CID22-2293.B513_N705.coverage.summary.htqc.txt',
                        'sample_id': '39a-cid22-2293',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2294-snapshot/2022out10-oc395-run1-b22-128-cid22-2294-snapshot.B22-128.CID22-2294.B514_N706.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2294-snapshot/2022out10-oc395-run1-b22-128-cid22-2294-snapshot.B22-128.CID22-2294.B514_N706.coverage.summary.htqc.txt',
                        'sample_id': '40a-cid22-2294',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2295-snapshot/2022out10-oc395-run1-b22-128-cid22-2295-snapshot.B22-128.CID22-2295.B515_N707.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2295-snapshot/2022out10-oc395-run1-b22-128-cid22-2295-snapshot.B22-128.CID22-2295.B515_N707.coverage.summary.htqc.txt',
                        'sample_id': '41a-cid22-2295',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2296-snapshot/2022out10-oc395-run1-b22-128-cid22-2296-snapshot.B22-128.CID22-2296.B516_N708.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2296-snapshot/2022out10-oc395-run1-b22-128-cid22-2296-snapshot.B22-128.CID22-2296.B516_N708.coverage.summary.htqc.txt',
                        'sample_id': '42a-cid22-2296',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2297-snapshot/2022out10-oc395-run1-b22-128-cid22-2297-snapshot.B22-128.CID22-2297.B505_N705.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2297-snapshot/2022out10-oc395-run1-b22-128-cid22-2297-snapshot.B22-128.CID22-2297.B505_N705.coverage.summary.htqc.txt',
                        'sample_id': '43a-cid22-2297',
                        'sample_type': ''},
                    {
                        'qc_htqc_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2300-snapshot/2022out10-oc395-run1-b22-128-cid22-2300-snapshot.B22-128.CID22-2300.B510_N702.coverage.htqc.txt',
                        'qc_htqc_summary_file': 'lab_validation_120/internal_data_sources/oc395_snps/pon//2022out10-oc395-run1-b22-128-cid22-2300-snapshot/2022out10-oc395-run1-b22-128-cid22-2300-snapshot.B22-128.CID22-2300.B510_N702.coverage.summary.htqc.txt',
                        'sample_id': '44a-cid22-2300',
                        'sample_type': ''}
                ]
            }
        }
    }
