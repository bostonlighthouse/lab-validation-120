"""Module to use for VCF functions in the validation"""
import sys
from collections import defaultdict
import biomodels.models.vcf.vcf_container as vc

# TODO change the modules that call this module to use it from biomodels


def get_keys_for_vcf_dictionary():
    """
    Just function to return the keys of the vcf dictionary passed around, needed this when a key might not match
    so just added helper function here.  If you add more values from the vcf_entry, then give them a column name and
    adjust in the dictionary_all_aa_changes() function: values_for_dictionary and add it there
    """

    return ['AA_CHANGE', 'REF_DP', 'ALT_DP', 'TOTAL_DP', 'ALT_AF', 'PRIMARY_CALLER', 'POS']


def dictionary_all_aa_changes(vcf_file_name=None):
    """
    :param vcf_file_name: A file path and name to a vcf file
    :return: all chrom + gene + aa changes in a defaultdict
    """
    # new instance of a container with all vcf_entry instances for a given file
    variant_container = vc.VcfContainer(vcf_file_name)
    # print(variant_container)
    chrom_gene_aa_change = defaultdict(None)
    # go over all samples in the TOML config file
    # get all  VcfEntry instances

    for variant_entry in variant_container.dictionary_vcf_entries.values():
        # print(variant_entry)
        # get a all transcript consequences: InfoVep
        all_transcript_consequences = get_all_transcript_consequences(variant_entry_obj=variant_entry)
        for info_vep in all_transcript_consequences:
            # if there's not Gene name or HGVSp just ignore
            found = None
            aa_change = None
            # was there a protein change?
            if info_vep.SYMBOL and info_vep.HGVSp:
                aa_change = get_jax_protein_change(info_vep.HGVSp.split(':')[1])
                aa_change = aa_change.replace('p.', '')
                # print(f"chrom {variant_entry.CHROM} start {variant_entry.POS} aa change {aa_change}")
                found = 1
            # if no protein change, get the cdot change
            elif info_vep.SYMBOL and info_vep.HGVSc:
                aa_change = info_vep.HGVSc.split(':')[1]
                found = 1
            # was their either a protein or c.dot change?  If so store for searching
            if found == 1 and variant_entry.info_internal_2_vcf.ALT_DP is not None and \
                    variant_entry.info_internal_2_vcf.REF_DP is not None:
                # create a key
                key_store = "_".join((variant_entry.CHROM, info_vep.SYMBOL, aa_change))
                # store the depth, vaf, primary caller
                total_depth = variant_entry.info_internal_2_vcf.ALT_DP + variant_entry.info_internal_2_vcf.REF_DP
                vaf = variant_entry.info_internal_2_vcf.ALT_AF
                primary_caller = variant_entry.info_internal_2_vcf.PRIMARY_CALLER
                values_for_dictionary = (aa_change,
                                         variant_entry.info_internal_2_vcf.REF_DP,
                                         variant_entry.info_internal_2_vcf.ALT_DP,
                                         total_depth,
                                         vaf,
                                         primary_caller, variant_entry.POS)
                chrom_gene_aa_change[key_store] = dict(zip(get_keys_for_vcf_dictionary(), values_for_dictionary))

    return chrom_gene_aa_change


def return_most_severe_impact_transcript_obj(variant_entry_obj=None):
    """
    Method used to find the PICK=1 attribute, which is found by VEP
    :param variant_entry_obj: an object that stores all the variant information from a transcript from VEP
    """
    all_transcript_consequences = get_all_transcript_consequences(variant_entry_obj=variant_entry_obj)

    for info_vep in all_transcript_consequences:
        if info_vep.PICK == 1:
            return info_vep
    # all impacts should have one PICK=1
    raise Exception("Should always have a PICK == 1")


def is_a_consequential_impact(variant_consequence_list=None):
    """
    Test to make sure the variant was a protein impacting
    :param variant_consequence_list: a vep transcript object's list of Consequences
    """
    # all all_impacts see thus far via VEP
    all_impacts = _get_set_all_impacts()
    check = set(variant_consequence_list).issubset(all_impacts)
    # if it was not found, then just die so it can be added
    assert check is True, f"Found impact in {set(variant_consequence_list)} not in all_impacts set"

    severe_impacts = _get_set_severe_impacts()

    check = any(item in variant_consequence_list for item in list(severe_impacts))
    return check


def _get_set_all_impacts():
    """Return a set of severe impacts"""
    # all all_impacts see thus far via VEP
    all_impacts = {
        "3_prime_UTR_variant",
        "5_prime_UTR_variant",
        "coding_sequence_variant",
        "downstream_gene_variant",
        "frameshift_variant",
        "incomplete_terminal_codon_variant",
        "inframe_deletion",
        "inframe_insertion",
        "intergenic_variant",
        "intron_variant",
        "NMD_transcript_variant",
        "non_coding_transcript_variant",
        "non_coding_transcript_exon_variant",
        "missense_variant",
        "protein_altering_variant",
        "regulatory_region_variant",
        "TFBS_ablation",
        "TF_binding_site_variant",
        "splice_acceptor_variant",
        "splice_donor_variant",
        "splice_region_variant",
        "start_lost",
        "start_retained_variant", # new variant I came across on 07/29/2021
        "stop_gained",
        "stop_lost",
        "stop_retained_variant",
        "synonymous_variant",
        "upstream_gene_variant",
    }
    return all_impacts


def _get_set_severe_impacts():
    """Return a set of severe impacts"""
    # only care about these impacts, i.e. will just ignore non-consequential impacts
    severe_impacts = {
        "coding_sequence_variant",
        "frameshift_variant",
        "incomplete_terminal_codon_variant",
        "inframe_deletion",
        "inframe_insertion",
        "missense_variant",
        "protein_altering_variant",
        "regulatory_region_variant",
        "TFBS_ablation",
        "TF_binding_site_variant",
        "splice_acceptor_variant",
        "splice_donor_variant",
        "start_lost",
        "stop_gained",
        "stop_lost",
    }
    return severe_impacts

def get_vaf_from_vf_field(variant_entry_obj=None):
    """
    Take a variant_entry object and progress through till we have a vaf
    :param variant_entry_obj: A variant entry object
    return VAF (float)
    """
    if variant_entry_obj.info_internal_2_vcf.ALT_AF is not None:
        vaf_2_use =  variant_entry_obj.info_internal_2_vcf.ALT_AF
    elif variant_entry_obj.info_gatk.gatk_AF is not None:
        vaf_2_use = variant_entry_obj.info_gatk.gatk_AF
    elif variant_entry_obj.info_lofreq.lofreq_AF is not None:
        vaf_2_use = variant_entry_obj.info_lofreq.lofreq_AF
    elif variant_entry_obj.info_hotspotter.hotspotter_AF is not None:
        vaf_2_use = variant_entry_obj.info_hotspotter.hotspotter_AF
    elif variant_entry_obj.format_mutect2.mutect2_AF is not None:
        vaf_2_use = variant_entry_obj.format_mutect2.mutect2_AF
    else:
        #raise Exception(f"No AF found in model {variant_entry_obj}")
        print(f"No AF found in model {variant_entry_obj}", file=sys.stderr)
        print("Returning 0 for AF", file=sys.stderr)
        vaf_2_use = 0

    return vaf_2_use


def ignore_caller_because_of_caller_thresholds(variant_entry_obj=None, variant_transcript_obj=None,
                                               keep_gatk_calls=False, lofreq_sb=10,
                                               lofreq_hrun=5):
    """We want to ignore places where the PRIMARY_CALLER had a reason, or other caller like Mutect had a reason
    :param variant_entry_obj: first variant_entry object
    :param variant_transcript_obj: What is the VEP of the most consequential
    :param keep_gatk_calls: by default we'll remove gatk primary calls, but we can keep them when True
    :param lofreq_sb: Lofreq Strand bias filter, default to 10, which is very stringent
    :param lofreq_hrun: Lofreq homopolymer run, defualt is 5
    """
    ignore_call = False
    reason = None
    if variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER == 'mutect':
        if variant_entry_obj.info_mutect.mutect_FILTER is not None:
            #print("Mutect", variant_entry_obj.info_mutect.mutect_FILTER)
            ignore_call = True
            reason = "mutect_filter"
    elif variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER == 'mutect2':
        if variant_entry_obj.info_mutect2.mutect2_FILTER is not None:
            #print("Mutect2", variant_entry_obj.info_mutect.mutect2_FILTER)
            ignore_call = True
            reason = "mutect2_filter"
    elif variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER == 'lofreq':
        if variant_entry_obj.info_mutect2.mutect2_FILTER is not None:
            #print("lofreq", variant_entry_obj.info_mutect.mutect2_FILTER)
            ignore_call = True
            reason = "lofreq_mutect_filter"
        elif variant_entry_obj.info_mutect.mutect_FILTER is not None:
            #print("lofreq", variant_entry_obj.info_mutect.mutect_FILTER)
            ignore_call = True
            reason = "lofreq_mutect2_filter"
        # homopolymer run, TODO Add transcript impact here??
        elif variant_entry_obj.info_lofreq.lofreq_HRUN is not None and \
                variant_entry_obj.info_lofreq.lofreq_HRUN > lofreq_hrun:
            #print("lofreq lofreq_HRUN_filter", variant_entry_obj.info_lofreq.lofreq_HRUN)
            ignore_call = True
            reason = "lofreq_HRUN_filter"
        # strand bias
        elif variant_entry_obj.info_lofreq.lofreq_SB is not None and \
                variant_entry_obj.info_lofreq.lofreq_SB >= lofreq_sb:
            #print("lofreq SB", variant_entry_obj.info_lofreq.lofreq_SB)
            ignore_call = True
            reason = "lofreq_SB_filter"
    elif keep_gatk_calls is False and variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER == 'gatk':
        ignore_call = True
        reason = "gatk"
    elif variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER == 'vargrouper':
        ignore_call = True
        reason = "vargrouper"
    # ignore entires from hotspotter where all other callers did not fid it
    elif variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER == 'hotspotter' and \
            variant_entry_obj.info_gatk.gatk_DETECT == 'N' and \
            variant_entry_obj.info_lofreq.lofreq_DETECT == 'N' and \
            variant_entry_obj.info_mutect.mutect_DETECT == 'N' and \
            variant_entry_obj.info_mutect2.mutect2_DETECT == 'N':
        ignore_call = True
        reason = "hotspotter_other_caller"

    return ignore_call, reason


def get_all_transcript_consequences(variant_entry_obj=None):
    """
    Go over and get all transcripts
    """
    all_transcript_consequences = variant_entry_obj.intergenic_consequences + \
                                  variant_entry_obj.regulatory_feature_consequences + \
                                  variant_entry_obj.motif_feature_consequences + \
                                  variant_entry_obj.info_vep_transcript_consequences
    return all_transcript_consequences


def is_dbsnp_variant(variant_entry_obj=None, rescue_cosmic=None):
    """
    Return if it's a dbSNP variant.  If the variant is a COSMIC variant, and rescue_cosmic is True, then don't count
    this as a dbSNP variant
    :param variant_entry_obj: an VariantEntry object
    :param rescue_cosmic: Boolean on whether to rescue dnSNP that are also COSMIC
    """
    is_rs = False  # is is dbSNP id
    is_cos = False  # is it COSMIC id

    # was there a Mutect/Mutect2 variant found
    if variant_entry_obj.info_mutect2.mutect2_ID is not None and \
            variant_entry_obj.info_mutect2.mutect2_ID.startswith('rs'):
        is_rs = True
    elif variant_entry_obj.info_mutect.mutect_ID is not None and \
            variant_entry_obj.info_mutect.mutect_ID.startswith('rs'):
        is_rs = True

    # get all transcripts
    all_transcript_consequences = get_all_transcript_consequences(variant_entry_obj=variant_entry_obj)

    # go over all transcript consequences
    for info_vep in all_transcript_consequences:
        # the attribute Existing_variation returns a list
        for existing_variation in info_vep.Existing_variation:
            # did it have the dbSNP id type
            if existing_variation.startswith('rs'):
                is_rs = True
            # did the COSMIC id type
            elif rescue_cosmic is True and existing_variation.startswith('COSM'):
                is_cos = True

    # was not found to be a COSMIC ID, then return as a dbSNP if it was found to be
    if is_cos is False and is_rs is True:
        return True
    else:
        return False


def is_population_variant(variant_entry_obj=None, af_threshold=0.01):
    """
    Go through and see if the variant was circulating in the population via ExAC_MAF or GMAF
    :param variant_entry_obj: an VariantEntry object
    :param af_threshold: The allele threshold to use a circulating, default = 0.01
    Exmample
        GMAF                     : ['C', 0.4525]                                 (list)
        AFR_MAF                  : ['T', 0.0635]                                 (list)
        AMR_MAF                  : ['T', 0.7017]                                 (list)
        EAS_MAF                  : ['T', 0.8165]                                 (list)
        EUR_MAF                  : ['T', 0.7356]                                 (list)
        SAS_MAF                  : ['T', 0.6217]                                 (list)
        AA_MAF                   : ['T', 0.1768]                                 (list)
        EA_MAF                   : ['T', 0.735]                                  (list)
        ExAC_MAF                 : ['T', 0.678]                                  (list)
        ExAC_Adj_MAF             : ['T', 0.6779]                                 (list)
        ExAC_AFR_MAF             : ['T', 0.1588]                                 (list)
        ExAC_AMR_MAF             : ['T', 0.7357]                                 (list)
        ExAC_EAS_MAF             : ['T', 0.8203]                                 (list)
        ExAC_FIN_MAF             : ['T', 0.7245]                                 (list)
        ExAC_NFE_MAF             : ['T', 0.7321]                                 (list)
        ExAC_OTH_MAF             : ['T', 0.7004]                                 (list)
        ExAC_SAS_MAF             : ['T', 0.6512]
    """
    allele  = variant_entry_obj.ALT
    # get all transcripts
    all_transcript_consequences = get_all_transcript_consequences(variant_entry_obj=variant_entry_obj)

    # go over all transcript consequences
    for info_vep in all_transcript_consequences:
        # the attribute Existing_variation returns a list
        if info_vep.GMAF is not None and allele == info_vep.GMAF[0] and info_vep.GMAF[1] >= af_threshold:
            return True
        elif info_vep.ExAC_MAF is not None and allele == info_vep.ExAC_MAF[0] and info_vep.ExAC_MAF[1] >= af_threshold:
            return True

# TODO: to maybe put this in a central tool repo, like nucleus
# see: dlims/dlims/report_app/gene_variants/base_snv_indel_variant.py, Note I did not use BaseSNVIndelGeneVariant

__AA_MAP = {
    "Ala": "A",
    "Arg": "R",
    "Asn": "N",
    "Asp": "D",
    "Asx": "B",
    "Cys": "C",
    "Gln": "Q",
    "Glu": "E",
    "Glx": "Z",
    "Gly": "G",
    "His": "H",
    "Ile": "I",
    "Leu": "L",
    "Lys": "K",
    "Met": "M",
    "Phe": "F",
    "Pro": "P",
    "Pyl": "O",
    "Sec": "U",
    "Ser": "S",
    "Thr": "T",
    "Trp": "W",
    "Tyr": "Y",
    "Val": "V",
    "Xaa": "X",
    "Xle": "J",
    "Ter": "*",
}


def get_jax_protein_change(hgvsp):
    """ Parse HGVSP value and get protein change for JAX lookup """
    if hgvsp.startswith("c.") and "%" in hgvsp:  # pylint: disable=no-else-return
        # in VV synonymous variants have a c. for HGVSP values
        # return None  # VV ENST00000456483.2:c.1317G>A(p.%3D)  %3D HEX ->  (p.=)
        return hgvsp  # VV ENST00000456483.2:c.1317G>A(p.%3D)  %3D HEX ->  (p.=)
    elif hgvsp.strip() == 'p.=':  # pylint: disable=no-else-return
        # at this point if hgvsp comes in as raw p.=
        return None  # "ENST0000:p.=",
    #elif hgvsp.endswith("%3D"):  # pylint: disable=no-else-return
    #    # in VV synonymous variants
    #    return None  # VV ENST0000:p.Val600%3D

    # see dlims/dlims/report_app/gene_variants/base_snv_indel_variant.py
    # protein = BaseSNVIndelGeneVariant._validate_hgvs_str(hgvsp)
    protein = hgvsp
    for key in __AA_MAP:  # TODO: improve this replace method...
        protein = protein.replace(key, __AA_MAP[key])  # replace full AA with abbreviations
    is_valid = len(hgvsp) > 3  # p.G12R  p. first two characters + one more p.A...
    # TODO: if "%" in protein? check VV
    if is_valid:  # valid protein
        return protein
    return None  # None