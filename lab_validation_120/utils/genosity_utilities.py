"""Module for genosity data"""

import biomodels.models.vcf.vcf_container as vc
import pandas as pd

from lab_validation_120.utils.pandas_dataframes_utilities import read_dataset_and_get_pandas_df, \
    get_columns_from_genosity_variants_summary_tumor_comparison_tsv
from lab_validation_120.utils.concordance_utilites import get_dictionary_of_concordance, get_dictionary_of_recall
from lab_validation_120.utils.vcf_utilities import return_most_severe_impact_transcript_obj, get_vaf_from_vf_field, \
    is_a_consequential_impact, ignore_caller_because_of_caller_thresholds
from tools.utils.tools_utils import get_binomial_probability


class UnknownVariantType(Exception):
    """Class for the VariantType seen in Genosity"""
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f"found unknown VariantType: {self.value}"


class UnknownNanVariantTYpe(Exception):
    """Class for when nan Variant Types comes in for ratio finding"""
    def __init__(self, ref_len, alt_len, genosity_dict):
        self.alt_len = alt_len
        self.ref_len = ref_len
        self.genosity_dict = genosity_dict

    def __str__(self):
        return f"found unknown variant lengths ref_len {self.ref_len}, alt_len {self.alt_len}\n{self.genosity_dict}"


class UnknownNucleotide(Exception):
    """Class for when an unknown nucleotide is found"""
    def __init__(self, nt, dict_):
        self.nt = nt
        self.dict_ = dict_

    def __str__(self):
        return f"found unknown nucleotide:  {self.nt}\n\n{self.dict_}"


class UnknownVariantLocation(Exception):
    """Class for when an unknown genosity variant_location is found"""
    def __init__(self, variant_location):
        self.variant_location = variant_location

    def __str__(self):
        return f"found unknown variant_location:  {self.variant_location}"


def process_genosity_samples_for_recall(config=None, vaf_threshold=None, depth_threshold=None, key_to_search=None):
    """
    Go over all  Genosity samples and look for matches in OC data (e.g. OC120plus)
    :param config: simple dictionary of values from the JSON passed in
    :param vaf_threshold: The VAF to use as a threshold
    :param depth_threshold: The Depth to use as a threshold
    :param key_to_search: What key to pull out of the config dictionary to get the values needed, e.g.:
    "concordance_clinical_samples_reproducibility"
    """

    final_matched_recall_results = []  # store all data that matched or did not in the vcf, 2-D list
    final_matched_specificity_results = []  # store all data that matched or did not in the vcf, 2-D list
    reasons_for_skipping_or_keeping_recall_variants = {}  # why did we skip the variant
    reasons_for_skipping_or_keeping_specificity_variants = {}  # why did we skip the variant
    total_num_samples = 0  # total samples analyzed
    num_recall_concordant = 0  # total concordant recalled variants (called in genosity and found in oc120)
    num_recall_discordant = 0
    num_specificity_concordant = 0  # total concordant specificity variants (called in oc120 and found in genosity)
    num_specificity_discordant = 0
    # go over all clinical samples found in the JSON config file and look for matches in their respective VCF file found
    # in the JSON config file
    for sample_id, clinical_sample_list in config[key_to_search]['recall_files'].items():
        experiment_type = clinical_sample_list.pop(2)['experiment_type']
        cid_sample = clinical_sample_list[0]['sample_id']
        genosity_sample = clinical_sample_list[1]['sample_id']
        # variables to store the respective data structures
        cid_sample_data, genosity_sample_data = None, None
        genosity_file = None  # output file created by genosity for variant calling
        # go over each sample file and store
        if 'oc_vcf_file' in clinical_sample_list[0]:
            cid_sample_data = vc.VcfContainer(clinical_sample_list[0]['oc_vcf_file'])
        else:
            exit("The OC file should be the first in the 2 element list in this config type, not the first")
        if 'genosity_overlap_variant_file' in clinical_sample_list[1]:
            genosity_file = clinical_sample_list[1]['genosity_overlap_variant_file']
            genosity_sample_data = get_genosity_variant_data(genosity_file)
        else:
            exit("The Genosity file should be the second in the 2 element list in this config type")
        # only take consequential impacts into account?
        consequential_impacts_only = config[key_to_search]['consequential_impacts_only']
        # go over and get numbers on recall: did we find what genosity found
        num_recall_concordant_temp, num_recall_discordant_temp = \
            _process_recall_matches_in_vcf_from_config(
                depth_threshold=depth_threshold,
                vaf_threshold=vaf_threshold,
                final_matched_recall_results=final_matched_recall_results,
                consequential_impacts_only=consequential_impacts_only,
                experiment_type=experiment_type,
                cid_sample=cid_sample,
                cid_sample_data=cid_sample_data,
                genosity_sample=genosity_sample,
                genosity_sample_data=genosity_sample_data,
                config_sample_id=sample_id,
                genosity_file=genosity_file,
                reasons_for_skipping_or_keeping=reasons_for_skipping_or_keeping_recall_variants)
        # go over and get numbers on whether genosity found what we found
        num_specificity_concordant_temp, num_specificity_discordant_temp = \
            _process_specificity_matches_in_vcf_from_config(
                depth_threshold=depth_threshold,
                vaf_threshold=vaf_threshold,
                final_specificity_recall_results=final_matched_specificity_results,
                consequential_impacts_only=consequential_impacts_only,
                experiment_type=experiment_type,
                cid_sample=cid_sample, cid_sample_data=cid_sample_data,
                genosity_sample=genosity_sample,
                genosity_sample_data=genosity_sample_data,
                config_sample_id=sample_id,
                genosity_file=genosity_file,
                reasons_for_skipping_or_keeping=reasons_for_skipping_or_keeping_specificity_variants)

        total_num_samples += 1
        num_recall_concordant += num_recall_concordant_temp
        num_recall_discordant += num_recall_discordant_temp
        num_specificity_concordant += num_specificity_concordant_temp
        num_specificity_discordant += num_specificity_discordant_temp
        #final_matched_recall_results = final_matched_recall_results + final_matched_specificity_results

    #print("here")
    #print(final_matched_specificity_results)
    final_matched_results = final_matched_recall_results + final_matched_specificity_results
    return final_matched_results, reasons_for_skipping_or_keeping_recall_variants, total_num_samples, \
           num_recall_concordant, num_recall_discordant


def _process_specificity_matches_in_vcf_from_config(depth_threshold=None, vaf_threshold=None,
                                                    final_specificity_recall_results=None,
                                                    consequential_impacts_only=None, experiment_type=None,
                                                    cid_sample=None, cid_sample_data=None,
                                                    genosity_sample=None, genosity_sample_data=None,
                                                    config_sample_id=None,
                                                    genosity_file=None,
                                                    reasons_for_skipping_or_keeping=None):
    """
    Go over the oc120plus sample data and process variants that should be looked at, and see if they are in Genosity
    :param vaf_threshold: The VAF to use as a threshold
    :param depth_threshold: The Depth to use as a threshold
    :param final_specificity_recall_results:  The list of data to store, this is a mutable list
    :param consequential_impacts_only: boolean, only take into account consequential impacts?
    :param experiment type: Just a string from the config file
    :param cid_sample: The string name for the cid
    :param cid_sample_data: The VcfContainer for the data
    :param genosity_sample: The string name for the genosity sample
    :param genosity_sample_data: The variant data from genosity
    :param config_sample_id: The config sample name
    :param genosity_file: The file for the genosity variants
    :param reasons_for_skipping_or_keeping: Dictionary that gets filled corresponding to why the variant was skipped or
    used
    """
    # 1_17380497_._G_T_0 have an int at the end b/c genosity could have multiple lines with the same variant key,
    # so remove for this scenario
    updated_genosity_dict = {}
    # how many of each did we find?
    num_specificity_concordant = 0
    num_specificity_discordant = 0
    for key, value in genosity_sample_data.items():
        updated_key = "_".join(key.split('_')[0:5])
        updated_genosity_dict[updated_key] = value

    for variant_entry_key, variant_entry_obj in cid_sample_data.dictionary_vcf_entries.items():
        variant_transcript_obj = return_most_severe_impact_transcript_obj(variant_entry_obj=variant_entry_obj)
        # was the argparse passed in to only look at transcript impacts?
        if consequential_impacts_only:
            # do not look a non consequential impact, then skip
            if is_a_consequential_impact(variant_consequence_list=variant_transcript_obj.Consequence) is False:
                continue
        if variant_transcript_obj.GMAF and  variant_transcript_obj.GMAF[1] >= 0.01:
            continue
        if variant_transcript_obj.ExAC_MAF and  variant_transcript_obj.ExAC_MAF[1] >= 0.01:
            continue
        # was there something to ignore in the oc120plus
        ignore, _ = ignore_caller_because_of_caller_thresholds(variant_entry_obj=variant_entry_obj,
                                                               variant_transcript_obj=variant_transcript_obj)
        if ignore is True:
            continue

        sig_rounding = 3
        cid_total_depth = variant_entry_obj.info_internal_2_vcf.REF_DP + variant_entry_obj.info_internal_2_vcf.ALT_DP
        vaf_2_use = get_vaf_from_vf_field(variant_entry_obj=variant_entry_obj)
        cid_vaf_2_use = round(vaf_2_use * 100, sig_rounding)

        # compare thresholds here, and do not consider if they are lower
        if cid_vaf_2_use < vaf_threshold:
            continue
        elif cid_total_depth < depth_threshold:
            continue

        if variant_entry_key in updated_genosity_dict:
            num_specificity_concordant += 1
            genosity_variant_data = updated_genosity_dict[variant_entry_key]
            entry_dict = \
                get_dictionary_of_concordance(
                    sample_id=config_sample_id,
                    variant_entry1_obj=variant_entry_obj,  # use the same variant_entry_obj here b/c it's found in both
                    variant_entry2_obj=variant_entry_obj,
                    variant_transcript_obj=variant_transcript_obj,
                    allele_count1=genosity_variant_data['FAD'],
                    allele_count2=variant_entry_obj.info_internal_2_vcf.ALT_DP,
                    vaf1=genosity_variant_data['FAF'],
                    vaf2=cid_vaf_2_use,
                    total_depth1=genosity_variant_data['FDP'],
                    total_depth2=cid_total_depth,
                    file1=genosity_file,
                    file2=cid_sample_data.file,
                    experiment_type=experiment_type,
                    sample_cid1=genosity_sample,
                    sample_cid2=cid_sample,
                    sample_pbinom1=get_binomial_probability(total_depth=genosity_variant_data['FDP'],
                                                            vaf=genosity_variant_data['FAF'],
                                                            sig_rounding=sig_rounding),
                    sample_pbinom2=get_binomial_probability(total_depth=cid_total_depth,
                                                            vaf=cid_vaf_2_use,
                                                            sig_rounding=sig_rounding)
                )
        else:
            # this was called by OC assay by not by Genosity (potential false positive)
            cid_pbinom = get_binomial_probability(total_depth=cid_total_depth,
                                                  vaf=cid_vaf_2_use,
                                                  sig_rounding=sig_rounding)
            # keep for debugging
            print(f"missed {variant_entry_key}")
            print(f"genosity file {genosity_file}")
            print(f"oc120 plus {cid_sample_data.file}")
            # print(f"variant entry {variant_entry_obj}")
            num_specificity_discordant += 1
            entry_dict = \
                get_dictionary_of_concordance(
                    sample_id=config_sample_id,
                    variant_entry1_obj=None,
                    variant_entry2_obj=variant_entry_obj,
                    variant_transcript_obj=variant_transcript_obj,
                    allele_count1=None,
                    allele_count2=variant_entry_obj.info_internal_2_vcf.ALT_DP,
                    vaf1=None,
                    vaf2=cid_vaf_2_use,
                    total_depth1=None,
                    total_depth2=cid_total_depth,
                    file1=genosity_file,
                    file2=cid_sample_data.file,
                    experiment_type=experiment_type,
                    sample_cid1=genosity_sample,
                    sample_cid2=cid_sample,
                    sample_pbinom1=None,
                    sample_pbinom2=cid_pbinom
                )
        # update the list past in to this function
        final_specificity_recall_results.append(entry_dict)
    return num_specificity_concordant, num_specificity_discordant


def _process_recall_matches_in_vcf_from_config(depth_threshold=None, vaf_threshold=None,
                                               final_matched_recall_results=None,
                                               consequential_impacts_only=None, experiment_type=None,
                                               cid_sample=None, cid_sample_data=None,
                                               genosity_sample=None, genosity_sample_data=None,
                                               config_sample_id=None,
                                               genosity_file=None,
                                               reasons_for_skipping_or_keeping=None):
    """
    Go over the Genosity sample data and process variants that should be looked at, and see if they are in the VCF file
    :param vaf_threshold: The VAF to use as a threshold
    :param depth_threshold: The Depth to use as a threshold
    :param final_matched_recall_results:  The list of data to store, this is a mutable list
    :param consequential_impacts_only: boolean, only take into account consequential impacts?
    :param experiment type: Just a string from the config file
    :param cid_sample: The string name for the cid
    :param cid_sample_data: The VcfContainer for the data
    :param genosity_sample: The string name for the genosity sample
    :param genosity_sample_data: The variant data from genosity
    :param config_sample_id: The config sample name
    :param genosity_file: The file for the genosity variants
    :param reasons_for_skipping_or_keeping: Dictionary that gets filled corresponding to why the variant was skipped or
    used
    """
    # how many of each did we find?
    num_recall_concordant = 0
    num_recall_discordant = 0
    variant_was_seen = {}  # genosity data has duplicate entries when more than one gene aligns to the region, so ignore
    # if we see the same key again..

    # need to use a tuple here from the dict.items, b/c we'll be removing some of the keys
    for genosity_variant_key, genosity_variant_data in tuple(genosity_sample_data.items()):
        # have to use this b/c the key for genosity included an int at the end of the key b/c genosity has multiple rows
        genosity_variant_key_for_our_data = '_'.join(genosity_variant_key.split('_')[0:5])

        # since all Genosity need pbinom, get it i.e. both concordant and discordant, all genosity were found
        sig_rounding = 3
        sample_pbinom1 = get_binomial_probability(total_depth=genosity_variant_data['FDP'],
                                                  vaf=genosity_variant_data['FAF'],
                                                  sig_rounding=sig_rounding)

        # was this a variant to skip based on data from genosity's row of information in the variant file
        skip_variant, reason = _is_a_variant_2_skip(genosity_dict=genosity_sample_data[genosity_variant_key],
                                                    consequential_impacts_only=consequential_impacts_only,
                                                    genosity_variant_key=genosity_variant_key,
                                                    depth_threshold=depth_threshold,
                                                    vaf_threshold=vaf_threshold,
                                                    genosity_pbinom=sample_pbinom1)
        # if the variant was seen before we can skip it if skip_variant is true then go get next variant
        # again this happens b/c genosity puts multiple entries of the same variant when there are two or more genes
        # involved at that loci
        if genosity_variant_key_for_our_data in variant_was_seen and skip_variant is True:
            continue
        else:
            variant_was_seen[genosity_variant_key_for_our_data] = []
        # update the reason we skipped
        reasons_for_skipping_or_keeping[reason] = reasons_for_skipping_or_keeping.get(reason, 0) + 1

        #print(f"All genosity keys: {genosity_variant_key_for_our_data}, should we skip {skip_variant}, reason {reason}")

        # if we are to skip b/c _is_a_variant_2_skip returned true, then just continue
        if skip_variant is True:
            # if we skipped b/c of genosity, then remove it from OC analysis when we OC assay vs Genosity
            #if cid_sample_data.get_key(genosity_variant_key_for_our_data):
            #    del cid_sample_data.dictionary_vcf_entries[genosity_variant_key_for_our_data]
            continue

        # if this key from Genosity is found in the VC container, it was a match
        if cid_sample_data.get_key(genosity_variant_key_for_our_data):
            num_recall_concordant += 1
            # get the variant entry object, b/c at this point we know it was in the cid_sample_data object
            variant_entry_obj = cid_sample_data.dictionary_vcf_entries[genosity_variant_key_for_our_data]
            cid_total_depth = variant_entry_obj.info_internal_2_vcf.REF_DP + variant_entry_obj.info_internal_2_vcf.ALT_DP
            cid_vaf_2_use = get_vaf_from_vf_field(variant_entry_obj=variant_entry_obj)
            cid_vaf_2_use = round(cid_vaf_2_use * 100, sig_rounding)
            # find the most severe transcript object from the OC assay
            severe_impact = return_most_severe_impact_transcript_obj(variant_entry_obj=variant_entry_obj)
            # here Genosity is the first sample, and the matched OC variant is the second
            entry_dict = \
                get_dictionary_of_concordance(
                    sample_id=config_sample_id,
                    variant_entry1_obj=variant_entry_obj,  # use the same variant_entry_obj here b/c it's found in both
                    variant_entry2_obj=variant_entry_obj,
                    variant_transcript_obj=severe_impact,
                    allele_count1=genosity_variant_data['FAD'],
                    allele_count2=variant_entry_obj.info_internal_2_vcf.ALT_DP,
                    vaf1=genosity_variant_data['FAF'],
                    vaf2=cid_vaf_2_use,
                    total_depth1=genosity_variant_data['FDP'],
                    total_depth2=cid_total_depth,
                    file1=genosity_file,
                    file2=cid_sample_data.file,
                    experiment_type=experiment_type,
                    sample_cid1=genosity_sample,
                    sample_cid2=cid_sample,
                    sample_pbinom1=sample_pbinom1,
                    sample_pbinom2=get_binomial_probability(total_depth=cid_total_depth,
                                                            vaf=cid_vaf_2_use,
                                                            sig_rounding=sig_rounding)
                )
            # remove the key, so when I test oc120 plus vs. genosity it's not there...
            #print(f"found {genosity_variant_key_for_our_data} {genosity_variant_key}")
            del cid_sample_data.dictionary_vcf_entries[genosity_variant_key_for_our_data]
        else:
            num_recall_discordant += 1
            # here Genosity is the first sample, and there was not match to OC sample so many values are None
            # as will pass in the genosity_variant_data data b/c get_dictionary_of_recall is looking for it
            entry_dict = \
                get_dictionary_of_recall(
                    samples_id=config_sample_id,
                    data_dict1=genosity_variant_data,
                    data_dict2=None,  # was missed by this gs assay version
                    allele_count1=genosity_variant_data['FAD'],
                    allele_count2=None,
                    vaf1=genosity_variant_data['FAF'],
                    vaf2=None, # was missed by this gs assay version
                    total_depth1=genosity_variant_data['FDP'],
                    total_depth2=None,  # was missed by this gs assay version
                    file1=genosity_file,
                    file2=None, # was missed by this gs assay version
                    experiment_type=experiment_type,
                    sample_id1=genosity_sample,
                    sample_id2=cid_sample,
                    sample_pbinom1=sample_pbinom1,
                    sample_pbinom2=None
                )
        # delete this because we're not going to check this the other way, i.e. oc assay vs genosity
        #del genosity_sample_data[genosity_variant_key]
        # update the list past in to this function
        final_matched_recall_results.append(entry_dict)
        # keep for debugging purposes
        """
        print(cid_sample, genosity_variant_key, genosity_sample_data[genosity_variant_key])
        print("\t".join(('Comparison summary', genosity_variant_data['Comparison summary'], 'VariantEffect',
                         str(genosity_variant_data['VariantEffect']),
                         'FDP', str(genosity_variant_data['FDP']),
                         'FRD', str(genosity_variant_data['FRD']),
                         'FRF', str(genosity_variant_data['FRF']),
                         'FAF', str(genosity_variant_data['FAF']),
                         'FAD', str(genosity_variant_data['FAD']),
                         'Gene', genosity_variant_data['Gene'], "\n")))
        """
    # return the number of of concordant and discordant results
    return num_recall_concordant, num_recall_discordant


def _is_a_variant_2_skip(genosity_dict=None, consequential_impacts_only=None, genosity_variant_key=None,
                         depth_threshold=None, vaf_threshold=None, genosity_pbinom=None):
    """
    Go through evaluate whether this variant should be kept by looking at:
    1. strand bias ratio >= .20
    2. Was it a "Germline" variant
    3. Was it a synonymous variant
    4. Was the VariantEffect == 'nan'
    5. Was the key 19_11098422_._G_C
    6. Was the indel length >= 30
    :param genosity_dict: The genosity df that was converted to a dictionary with key's like:
    key 18_61873521_._CTGTGTGTGTG_C
    :param genosity_pbinom: Pbinom for this variant in genosity
    :return ret_val: boolean
    """
    ret_val = False  # the variant did not match something below to be removed
    reason = 'keep'  # the variant should be kept
    # find the bias ratio
    if get_genosity_strand_bias_ratio(genosity_dict=genosity_dict) < .20:
        ret_val, reason = True, 'strand_bias'
    # We do not do official Germline calling so remove
    elif genosity_dict['Comparison summary'] == 'Germline':
        ret_val, reason = True, 'germline'
    # Ignore Variant Effect (synonymous), ignore with consequential_impacts_only being run
    elif consequential_impacts_only is True and genosity_dict['VariantEffect'] == 'synonymous':
        ret_val, reason = True, 'synonymous'
    # many variants had VariantLocation we will ignore with consequential_impacts_only being run
    elif consequential_impacts_only is True and _is_a_variant_location_2_skip(genosity_dict=genosity_dict):
        ret_val, reason = True, 'variant_location'
    # this variant was found on 4 samples (VAF), cid20-1082 (6.26), cid20-1213 (6.82), cid20-1100 (7.95),
    # cid20-1105 (6.3)
    elif genosity_variant_key == '19_11098422_._G_C':
        ret_val, reason = True, '19_11098422_._G_'
    # length of the indel should be larger thant 30 (use 31 b/c of leading base
    elif not len(genosity_dict['Ref']) <= 31 or not len(genosity_dict['Alt']) <= 31:
        ret_val, reason = True, 'indel_length'
    # these have a negative cDNA, so it was intronic so just remove.
    # genosity_dict['HGVS_cDNA'] can be nan so test for string first
    elif isinstance(genosity_dict['HGVS_cDNA'], str) and '-' in genosity_dict['HGVS_cDNA']:
        ret_val, reason = True, 'negative_cdot'
    elif genosity_dict['FDP'] < depth_threshold:
        ret_val, reason = True, 'depth_threshold'
    elif genosity_dict['FAF'] < vaf_threshold:
        ret_val, reason = True, 'vaf_threshold'
    elif genosity_pbinom < 0.99:
        ret_val, reason = True, 'genosity_binom'
    return ret_val, reason


def _is_a_variant_location_2_skip(genosity_dict: dict) -> bool:
    """
    Go through and skip things that are VariantLocation's that are not important
    :param genosity_dict: genosity dictionary of data for a variant
    :return ret_val: boolean
    """
    ret_value = False
    variant_location = genosity_dict['VariantLocation']
    # we will not compare to these variants, since they are not protein coding, use str here b/c some can be nan
    if str(variant_location) in ('downstream', 'intron', 'upstream', "5'UTR", "3'UTR", 'nan'):
        ret_value = True
    elif variant_location in ('exon', 'splice site'):
        pass
    else:
        raise UnknownVariantLocation(variant_location)

    return ret_value


def get_genosity_strand_bias_ratio(genosity_dict: dict) -> float:
    """
    Go through strand bias ratio by calling
    _get_low_ratio_of_2_numbers
    :param genosity_dict: genosity dictionary of data for a variant
    :return ratio: float
    """
    variant_type = genosity_dict['VariantType']
    alt_len = len(genosity_dict['Alt'])
    ref_len = len(genosity_dict['Ref'])
    if variant_type == 'substitution':
        if alt_len == 1 and ref_len == 1:
            ratio = _process_substitution_for_stand_bias(genosity_dict=genosity_dict)
        else:  # delins, e.g. 'Ref': 'AG', 'Alt': 'GC',
            ratio = \
                _get_low_ratio_of_2_numbers(
                    num1=genosity_dict['A_CountForward'] + genosity_dict['C_CountForward'] + genosity_dict[
                        'G_CountForward'] + genosity_dict['T_CountForward'],
                    num2=genosity_dict['A_CountReverse'] + genosity_dict['C_CountReverse'] + genosity_dict[
                        'G_CountReverse'] + genosity_dict['T_CountReverse'])
    # was it an deletion
    elif variant_type == 'deletion':
        ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['Del_CountForward'],
                                            num2=genosity_dict['Del_CountReverse'])
    # was it in insertion
    elif variant_type == 'insertion':
        ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['Ins_CountForward'],
                                            num2=genosity_dict['Ins_CountReverse'])
    # a duplication is an duplication
    elif variant_type == 'duplication':
        ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['Ins_CountForward'],
                                            num2=genosity_dict['Ins_CountReverse'])
    # a duplication is an delins, e.g. Ref CA, Alt TG will have T_CountForward and T_CountReverse
    elif variant_type == 'delins':
        #print("\n\nTake a look at this dict since it was a delins, do the *_CountForward and *_CountReverse "
        #      "make sense given the Alt, i.e. if Alt->TA, do you see T_CountForward and T_CountReverse\n\n",
        #      genosity_dict, "\n")
        if genosity_dict['Alt'].startswith('A'):
            ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['A_CountForward'],
                                                num2=genosity_dict['A_CountReverse'])
        elif genosity_dict['Alt'].startswith('T'):
            ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['T_CountForward'],
                                                num2=genosity_dict['T_CountReverse'])
        elif genosity_dict['Alt'].startswith('C'):
            ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['C_CountForward'],
                                                num2=genosity_dict['C_CountReverse'])
        elif genosity_dict['Alt'].startswith('G'):
            ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['G_CountForward'],
                                                num2=genosity_dict['G_CountReverse'])
        else:
            raise UnknownNucleotide(genosity_dict['Alt'], genosity_dict)
    elif str(variant_type) == 'nan':
        # simple substitution
        if alt_len == 1 and ref_len == 1:
            ratio = _process_substitution_for_stand_bias(genosity_dict=genosity_dict)
        elif alt_len > ref_len:  # insertion
            ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['Ins_CountForward'],
                                                num2=genosity_dict['Ins_CountReverse'])
        elif ref_len > alt_len:  # deletion
            ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['Del_CountForward'],
                                                num2=genosity_dict['Del_CountReverse'])
        elif alt_len == ref_len:  # delins, e.g 'Ref': 'AG', 'Alt': 'GC',
            ratio = \
                _get_low_ratio_of_2_numbers(
                    num1=genosity_dict['A_CountForward'] + genosity_dict['C_CountForward'] + genosity_dict[
                        'G_CountForward'] + genosity_dict['T_CountForward'],
                    num2=genosity_dict['A_CountReverse'] + genosity_dict['C_CountReverse'] + genosity_dict[
                        'G_CountReverse'] + genosity_dict['T_CountReverse'])
        else:
            raise UnknownNanVariantTYpe(ref_len, alt_len, genosity_dict)

    else:
        raise UnknownVariantType(variant_type)

    return ratio


def _process_substitution_for_stand_bias(genosity_dict: dict) -> float:
    """
    GO through and process a single substitution for and find the strand bias ratio by calling
    _get_low_ratio_of_2_numbers
    :param genosity_dict: genosity dictionary of data for a variant
    :return ratio: float
    """
    alt_allele = genosity_dict['Alt']
    if alt_allele == 'A':
        ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['A_CountForward'], num2=genosity_dict['A_CountReverse'])
    elif alt_allele == 'G':
        ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['G_CountForward'], num2=genosity_dict['G_CountReverse'])
    elif alt_allele == 'C':
        ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['C_CountForward'], num2=genosity_dict['C_CountReverse'])
    elif alt_allele == 'T':
        ratio = _get_low_ratio_of_2_numbers(num1=genosity_dict['T_CountForward'], num2=genosity_dict['T_CountReverse'])
    else:
        raise UnknownNucleotide(alt_allele, genosity_dict)
    return ratio


def _get_low_ratio_of_2_numbers(num1: int, num2: int) -> float:
    """
    Find the low ratio of two numbers e.g. 1 and 2  1/2 and 2/1, return .5
    :param num1: The first numbers
    :param num2: The second numbers
    :return ratio: float
    """

    try:
        ratio1 = num1 / num2
    except ZeroDivisionError:
        ratio1 = 0

    try:
        ratio2 = num2 / num1
    except ZeroDivisionError:
        ratio2 = 0

    if ratio1 > ratio2:
        ret = ratio2
    else:
        ret = ratio1
    return ret


def get_genosity_variant_data(file: str, filter_germline_variants: bool = False) -> dict:
    """Open the genosity file, get a data frame and then convert to a dictionary with key like:
    The keys will look: 1_877831_._T_C_INCREMENTER have the incrementer name b/c genosity provides multiple
    :param file:  The Genosity file to open
    :param filter_germline_variants: Boolean on whether to filter out Germline calls in the Genosity Data
    """
    # get the genosity data
    df_genosity = \
        read_dataset_and_get_pandas_df(
            file_name=file,
            col_vals=get_columns_from_genosity_variants_summary_tumor_comparison_tsv())
    if filter_germline_variants is True:
        """
        # got the columns from original overlapping, before adding filter_germline_variants 
        less *comparison.overlapping_oc395boosted_primers.tsv  | cut -f 51 | sort | uniq -c 
        31 Comparison summary
        23033 Germline
        226 Possibly germline
        3381 Tumor
        125 Unknown tumor-normal
        """
        df_genosity = df_genosity[df_genosity['Comparison summary'] == 'Tumor'].copy()
    # get a dictionary from the genosity data
    dict_of_dict_genosity_set_variants = _get_dict_list_of_dict_from_genosity(chrom="Chrom", df=df_genosity)
    return dict_of_dict_genosity_set_variants


def _get_dict_list_of_dict_from_genosity(chrom: str, df: pd.DataFrame) -> dict:
    """
    :param chrom: Chromosome value in the df from genosity
    :param df: Data frame from Genosity data
    The keys     following: 1_877831_._T_C_INCREMENTER have the incrementer name b/c genosity provides multiple
    annoations for a variant: eg.
    1	878314	.	G	C	899	181	20.13	715	79.53	.	.	.	SAMD11
    1	878314	.	G	C	899	181	20.13	715	79.53	.	.	.	NOC2L
    """
    # create them here Key that look like: 1_4849384_._T_C_GENE
    # get a list of dictionaries from the data frame
    list_of_dictionaries = df.to_dict('records')
    dict_list_of_dict_variants = {}
    for i, dict_ in enumerate(list_of_dictionaries):
        # create a key that looks like it does in vcf_container
        key = "_".join((dict_[chrom], str(dict_['Pos']), '.', dict_['Ref'], dict_['Alt'], str(i)))
        dict_list_of_dict_variants[key] = dict_

    return dict_list_of_dict_variants
