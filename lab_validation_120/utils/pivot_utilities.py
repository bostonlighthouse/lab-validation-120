import os

import numpy as np
import pandas as pd

from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame


def print_data_frame_pivot_outputs(df=None, args=None, group_by_key=None):

    # need to replace NaNs, b/c when a variant has all NaNs, e.g. T790M, the pivot will not include the sample
    df.fillna(-1, inplace=True)
    # round this columns
    df["ALT_AF"] = df["ALT_AF"].round(decimals=3)
    # covert this column
    df['TOTAL_DP'] = df['TOTAL_DP'].astype(float)

    # No longer need this since I combined it below.  Leave here for now
    # print_data_frame(df=df, tabular_output_file=args.tabular_output_file)

    # get two pivots tables that will be combined
    pivot_df_detph = _pivot_lod_tables(df=df, group_by_key=group_by_key, values_key="TOTAL_DP")
    pivot_df_vaf = _pivot_lod_tables(df=df, group_by_key=group_by_key, values_key="ALT_AF")

    final_df = pd.concat([pivot_df_vaf, pivot_df_detph], axis=1)
    # change the value back to NaN,that will get
    final_df.replace(to_replace=-1, value=np.nan, inplace=True)
    pivot_filename, ext = os.path.splitext(args.tabular_output_file)
    pivot_filename = "_".join((pivot_filename, "word_document_table")) + ext
    print_data_frame(df=final_df, tabular_output_file=pivot_filename)


def _pivot_lod_tables(df=None, group_by_key=None, values_key=None):

    # pivot so that horizon names are columns and values are the values_key passed in
    pivot_df = pd.pivot_table(
        df, values=values_key, index=["Gene", "Variant", "Variant_Type", "Chromosome_no_p_q", "VAF", "Key",
                                      "Matched_dilution_sample"],
        columns=[group_by_key])

    pivot_df.reset_index(inplace=True)
    pivot_df.columns.name = ""
    #pivot_df.sort_values(["Gene", "Variant"], inplace=True)

    return pivot_df