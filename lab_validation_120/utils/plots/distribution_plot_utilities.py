import os
import collections
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from scipy import stats
from lab_validation_120.utils.plots.plot_utilities import get_plot_file_type, global_rc_set, \
    get_subplot_bounding_box_parameters
from scipy.stats import shapiro, ks_2samp


def process_distribution_analysis_for_htqc_intervals(df=None, config=None, key_to_config_usage=None,
                                                     col_name_4_sample_names=None):
    """
    Go over the three key metrics, MEAN_COLLAPSED_COVERAGE, MEAN_ABS_COVERAGE, MEAN_FILTERED_COVERAGE and plot out
    their distributions
    :param df: data frame for the analysis
    :param config: simple dictionary of values from the dict passed in
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What to name the new column in the data frame
    """
    # get the configuration on how to plot
    images_dir = config[key_to_config_usage]['images_dir']
    chart_title = config[key_to_config_usage]['input_files']['chart_title']
    # go over all values above
    for dict_ in config[key_to_config_usage]['distribution_config']:
        cols = len(df.groupby(col_name_4_sample_names))
        acronym_of_metric_to_plot = dict_['acronym_of_metric_to_plot']
        df_col_metric_to_plot = dict_['df_col_metric_to_plot']
        width = cols * 4  # each plot is 4 (got this from Horizon data)
        plt.rcParams['figure.figsize'] = (width, 15.0)
        global_rc_set()
        # rows x cols, 1st row here is distribution only, 2nd sorted by overall sum
        fig, ax = plt.subplots(3, cols)
        # get a dictionary of dictionaries that then can be sorted by sum of metric (sum) or by
        # sample name (sample_name) right below
        sample_names_dict = _sort_sample_names_by_sum_metric(df=df, df_col_metric_to_plot=df_col_metric_to_plot,
                                                             col_name_4_sample_names=col_name_4_sample_names)

        # plot two rows.  The first is grouped by name, the second row is sorted by the sum of the metric
        for i, (dict_sort_value, is_log_plot) in enumerate([('sample_name', False),
                                                            ('sum', False),
                                                            ('sum', True)]):
            plot_htqc_coverage_distribution(sample_names_dict=sample_names_dict, ax=ax[i],
                                            # values from the dict_ here:
                                            df_col_metric_to_plot=df_col_metric_to_plot,
                                            acronym_of_metric_to_plot=acronym_of_metric_to_plot,
                                            x_upper_lim=dict_['x_upper_lim'], y_upper_lim=dict_['y_upper_lim'],
                                            xlabel=dict_['xlabel'],
                                            sample_size_normal_dist=dict_['sample_size_normal_dist'],
                                            normal_bins=dict_['normal_bins'], coverage_bins=dict_['coverage_bins'],
                                            dict_sort_value=dict_sort_value,
                                            log_y_plot=is_log_plot)
        if chart_title:
            fig.suptitle(f'{chart_title}', fontsize="x-large")  # or plt.suptitle('Main title')
        # automatically add and adjust padding between subplots
        # only considers ticklabels, axis labels, and titles. Thus, other artists may be clipped and also may overlap.
        # So you can adjust the subplot geometry in the very tight_layout call as follows:
        fig.tight_layout(rect=get_subplot_bounding_box_parameters())
        group_name = config[key_to_config_usage]['input_files']['group_id']
        plt.savefig(os.path.join(images_dir, f"{group_name}_htqc_metric_distributions_{acronym_of_metric_to_plot}"
                                 + get_plot_file_type()))
        plt.close(fig)  # have to close


def _sort_sample_names_by_sum_metric(df=None, df_col_metric_to_plot=None, col_name_4_sample_names=None):
    value_2_return = {}
    for (sample, df2) in df.groupby(col_name_4_sample_names):
        value_2_return[sample] = {}
        value_2_return[sample]['df'] = df2
        value_2_return[sample]['sample_name'] = sample
        value_2_return[sample]['sum'] = round(sum(df2[df_col_metric_to_plot].tolist()))
    return value_2_return


def plot_htqc_coverage_distribution(sample_names_dict=None, ax=None, df_col_metric_to_plot=None,
                                    acronym_of_metric_to_plot=None,
                                    x_upper_lim=None, y_upper_lim=None,
                                    xlabel=None,
                                    sample_size_normal_dist=None,
                                    normal_bins=None, coverage_bins=None, dict_sort_value=None,
                                    vertical_threshold_line_value=None,
                                    log_y_plot=None):
    """
    :param ax: an instance of the class plt.Axes
    :param df_col_metric_to_plot: The column in the df to plot
    :param acronym_of_metric_to_plot: Abbreviation of the metric to plot, e.g. MCC = MEAN_COLLAPSED_COVERAGE
    :param x_upper_lim: Upper limit on the x-axis to plot
    :param y_upper_lim: Upper limit on the y-axis to plot
    :param xlabel: X-axis label of the x-axis
    :param sample_size_normal_dist: Size of the normal distribution to sample from
    :param coverage_bins: How many bins to use of the coverage metric
    :param dict_sort_value: What key to sort on
    """

    col_num = 0
    # sort the dictionary of dictionaries that was passed in by the key (dict_sort_value)
    for sample_name in collections.OrderedDict(sorted(sample_names_dict.items(), key=lambda t:t[1][dict_sort_value])):
        df2 = sample_names_dict[sample_name]['df']

        values_2_plot = df2[df_col_metric_to_plot].tolist()  # get the column of metric values
        sample_size = len(values_2_plot)
        sum_value = round(sum(values_2_plot))
        assert sum_value == sample_names_dict[sample_name]['sum']
        # plot the distribution
        sns.distplot(values_2_plot, kde=False, hist=True, ax=ax[col_num],  color='red', bins=coverage_bins,
                     label=acronym_of_metric_to_plot, hist_kws={'log':log_y_plot})

        # what are the x and y thresholds
        # MAC has too large of an x-axis, but find the max x-axis show when it's log plottingo of the y-axsis
        max_x_value = df2[df_col_metric_to_plot].max()
        if df_col_metric_to_plot == 'MEAN_ABS_COVERAGE' and log_y_plot is True:
            ax[col_num].set_xlim(-100, max_x_value + 500)
        else:
            ax[col_num].set_xlim(-100, x_upper_lim)

        # do not set y_limit when using log y-axis
        if log_y_plot is False:
            ax[col_num].set_ylim(0, y_upper_lim)

        mu, std = stats.norm.fit(values_2_plot)
        if not (acronym_of_metric_to_plot == 'MAC' or log_y_plot):
            # Fit a normal distribution to the data:
            np.random.seed(4)  # seed random number generator with fixed value so we always get same values below

            # keep values continuous, since depth values are in the coverage
            # loc as the mean of the distribution
            # scale as the standard deviation of the distribution
            # size as number of samples
            normal_distr_values = list(np.random.normal(loc=mu, scale=std, size=sample_size_normal_dist))
            # Plotting of the normal
            sns.distplot(normal_distr_values, kde=False, hist=True, color='blue', ax=ax[col_num], bins=normal_bins,
                         hist_kws={"histtype": "step", "linewidth": 2, "alpha": 1, "color": "blue"}, label="Normal")
        # set title
        ax[col_num].set_title(sample_name + f" {acronym_of_metric_to_plot} Dist. " + f'(sum: {sum_value:,})\n' +
                        f"n={sample_size} mu={round(mu, 1)} std={round(std, 1)}", fontsize=9)
        # add an x-axis label
        ax[col_num].set(xlabel=xlabel)
        # add a legend
        ax[col_num].legend(loc='upper right', frameon=False)
        ax[col_num].axvline(x=mu, color='r', linestyle='--')
        ax[col_num].axvline(x=max_x_value, color='b', linestyle='--')
        col_num += 1  # new column, i.e. a new sample, so update the ax


def plot_distribution_of_data(ax=None, values=None, legend_value_for_values=None, plot_normal_distribution=None,
                              plot_legend=False, alpha=0.01, value_plotted=None, x_axis_label=None) -> list:

    """
    Simple function to plot a distribution, and then plot a normal distribution over it
    :param ax:
    :param values: Values to be plotted (can be a list)
    :param legend_value_for_values: String for the legend
    :param plot_legend: Boolean on whether to plot the legend, default False
    :param alpha: What alpha level to run the tests
    :param x_axis_label: x-axis label for the distribution
    :param plot_normal_distribution: Boolean on whether to plot a normal dist.  This is time consuming for large lists
    :param value_plotted: String to be added on the y-axis label
    """

    # plot the distribution of the data
    sns.kdeplot(values, ax=ax, color='red', label=legend_value_for_values)

    # plot a normal over it...
    normal_distr_values = None
    if plot_normal_distribution is True:
        fitted_norm_pop_mean, fitted_norm_std_dev = stats.norm.fit(values)
        # Fit a normal distribution to the data:
        #np.random.seed(4)  # seed random number generator with fixed value so we always get same values below
        # keep values continuous, since depth values are in the coverage
        # loc as the mean of the distribution
        # scale as the standard deviation of the distribution
        # size as number of samples
        normal_distr_values = list(np.random.normal(loc=fitted_norm_pop_mean, scale=fitted_norm_std_dev, size=len(values)*1000))
        # Plotting of the normal
        sns.kdeplot(normal_distr_values, color='blue', ax=ax)

        # The Shapiro-Wilk's test evaluates a data sample and quantifies how likely it is that the data was drawn from a
        # Gaussian distribution
        stat, p = shapiro(values)
        stat_line = f"Distribution of {legend_value_for_values}\nShapiro-Wilk's statistics={stat:.3f}, p={p:.3f}"
        # interpret
        if p > alpha:
            alpha_line = 'Sample looks Gaussian (fail to reject H0)'
        else:
            alpha_line = 'Sample does not look Gaussian (reject H0)'

        ax.set_title(f"{stat_line}\n{alpha_line}")

        #stat = ks_2samp(values, 'norm')
        #print(stat)

    ax.set_xlabel(x_axis_label)
    ax.set_ylabel(f"Frequency {value_plotted}")

    # might have function calling this take care of plotting the legend
    if plot_legend is True:
        ax.legend(loc='upper right', frameon=True)

    return normal_distr_values
