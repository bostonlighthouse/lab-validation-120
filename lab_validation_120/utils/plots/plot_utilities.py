"""Plotting module"""

import numpy as np
import seaborn as sns


def get_x_y_list_of_points(dict_values=None, x_value_key=None, y_value_key=None):
    """
    Used to take the dict of dictionaries and and get x , y lists of VAF values
    :param dict_values:  e.g. {0: {'horizon_vaf': 10.7, '395_vaf': 9.9}, 1: {'horizon_vaf': 10.0, '395_vaf': 10.7}
    :param x_value_key: e.g. above would be '395_vaf'
    :param y_value_key: e.g. above would be 'horizon_vaf'
    :return: x, y lists
    """
    final_tmb_dict = {k: v for k, v in dict_values.items()}  # pylint: disable=unnecessary-comprehension
    x = np.array([i[x_value_key] for i in final_tmb_dict.values()])
    y = np.array([i[y_value_key] for i in final_tmb_dict.values()])
    return x, y


def return_dictionary_x_y_values(row_from_df=None, x_value_key=None, y_value_key=None, df_value1=None, df_value2=None):
    """
    Helper function for df.apply lambda function
    :param row_from_df: Row from the dataframe
    :param x_value_key: Set the key for X, e.g. above would be '395_vaf'
    :param y_value_key: Set the key for Y, e.g. above would be '395_vaf'
    :param df_value1: What value to get from the df, e.g. "ALT_AF"
    :param df_value1: What value to get from the df,  e.g. "VAF"
    :return: Dictionary of vaf values for horizon data and 395
    """
    return {
        x_value_key: 0 if np.isnan(row_from_df[df_value1]) else float(row_from_df[df_value1]),
        y_value_key: 0 if np.isnan(row_from_df[df_value2]) else float(row_from_df[df_value2])
    }


def get_plot_file_type():
    """Just a function to control the type of images created across the project"""
    # Stay here...
    return '.svg'


def get_consequential_str(consequential_impacts_only=None):
    """If There's a need to print out consequential impacts do it.."""
    if consequential_impacts_only is None:
        return ""
    else:
        consequential_impacts_str = "All Variant Impacts"
        if consequential_impacts_only is True:
            consequential_impacts_str = "Consequential Variant Impacts Only"
        return consequential_impacts_str


def global_rc_set():
    #return {}

    rc = {'font.size': 9, 'axes.labelsize': 10, 'legend.fontsize': 8,
          'axes.titlesize': 12, 'xtick.labelsize': 6, 'ytick.labelsize': 6,
          'axes.grid': True,
          'figure.titlesize': 12,
          'axes.edgecolor': 'dimgray',  # the border around the plot
          'grid.color': 'lightgray',
          'axes.facecolor': 'white',
          # new
          'axes.spines.bottom': True,
          }

    sns.set(rc=rc)


def get_subplot_bounding_box_parameters():
    """
    You may provide an optional rect parameter, which specifies the bounding box that the subplots will be fit inside.
    The coordinates must be in normalized figure coordinates and the default is (0, 0, 1, 1).  I updated these
    """
    return [0, 0.03, 1, 0.95]


def get_mcc_threshold():
    """
    Return the MCC plotting threshold for red lines
    """
    return 100


def get_alternate_colors(df=None, alternate_colors=False, color1='lightsteelblue', color2='orange'):
    """
    Create a list of alternating colors for plotting, if alternate_color is false, then color1 will be used throughout
    :param df: The dataframe used in the analysis
    :param alternate_colors: Boolean on whether to alternate or not
    """
    colors_2_use = []
    for _ in df:
        if alternate_colors is True:
            colors_2_use.append(color1)
            colors_2_use.append(color2)
        else:
            colors_2_use.append(color1)
            colors_2_use.append(color1)
    return colors_2_use


