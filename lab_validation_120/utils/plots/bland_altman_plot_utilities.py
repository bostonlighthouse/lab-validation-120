import seaborn as sns
import numpy as np
from lab_validation_120.utils.plots.deming_regession_plot_utilities import deming_regression_plot
from scipy import stats


def bland_altman_plot(ax=None, x=None, y=None, sd_limit=1.96, str_x_name=None, str_y_name=None,
                      plot_deming_line=True, deming_plot_color='green', value_plotted=None,
                      log_transformation=False, plot_legend=True):
    """
    Construct a Tukey/Bland-Altman Mean Difference Plot. Tukey’s Mean Difference Plot (also known as a Bland-Altman
    plot) is a graphical method to analyze the differences between two methods of measurement. The mean of the measures
    is plotted against their difference.

    The resulting graph is a scatter plot XY, in which the Y axis shows the difference between the two paired
    measurements (A-B) and the X axis represents the average of these measures ((A+B)/2). In other words

    For more information see https://en.wikipedia.org/wiki/Bland-Altman_plot
    Also suggested for analysis in validations: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5800325/
    Also suggested for analysis in validations: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4470095/

    :param ax: an instance of the class plt.Axes
    :param x: Any sequence that can be interpreted as an ndarray
    :param y: Any sequence that can be interpreted as an ndarray
    :param sd_limit: The standard deviations to use, default is to use 1.96
    :param str_x_name: The string name of the first set of data, to be used in the title
    :param str_y_name: The string name of the second set of data, to be used in the title
    :param plot_deming_line: Boolean to
    :param deming_plot_color: Color of the deming regression line
    :param value_plotted: What type of value was plotted, e.g. VAF, Depth, etc
    :param log_transformation: Do a log 2 transformation of the data, booloean
    :param plot_legend: Plot the legend on the graph
    :return: np vector of differences
    """
    x = np.asarray(x)
    y = np.asarray(y)
    log_transformation_string = "No Log Transformation Present"
    if log_transformation is True:
        constant = abs(min(x)) + 1
        x = [value + constant for value in x]
        constant = abs(min(y)) + 1
        y = [value + constant for value in y]
        x = np.log2(x)
        y = np.log2(y)
        log_transformation_string = "Log Transformation Present"

    assert len(x) == len(y)
    mean = np.mean([x, y], axis=0)
    differences = x - y  # Difference between data2 and data1, note this is how the labels below are shown x - y
    mean_differences = np.mean(differences)   # Mean of the difference
    stand_dev_differences = np.std(differences, axis=0)  # Standard deviation of the difference

    differences_confidence_intervals = _mean_confidence_interval(differences)

    # find the limit of agreement lines to plot
    limit_of_agreement = sd_limit * stand_dev_differences
    lower = mean_differences - limit_of_agreement
    upper = mean_differences + limit_of_agreement

    deming_str = "No Deming Regression Line"
    if plot_deming_line is True:
        # Plot a deming regression line
        deming_regression_plot(ax=ax, x=mean, y=differences, deming_plot_color=deming_plot_color)
        deming_str = "with Deming Regression Line"

    # draw the scatter plot of the data
    sns.scatterplot(x=mean, y=differences, ax=ax)
    # draw the three lines in the Bland-Altman here mean
    labels_2_print = f'mean differences: {np.round(mean_differences, 2)}'
    ax.axhline(mean_differences, color='black', linestyle='--', label=labels_2_print)
    # draw the confidence interval around the mean
    x_fill_line = [0, max(mean)+max(mean)*.1]
    y_ci1 = [differences_confidence_intervals[1], differences_confidence_intervals[1]]
    y_ci2 = [differences_confidence_intervals[2], differences_confidence_intervals[2]]
    ax.fill_between(x_fill_line, y_ci1, y_ci2, alpha=0.3, facecolor='blue', edgecolor='none', label="95% CI mean")


    # draw a line a -
    labels_2_print = '0 line'
    ax.axhline(0, color='black', linestyle='-', label=labels_2_print)
    # draw the three lines in the Bland-Altman here +SD
    labels_2_print = f'+SD {sd_limit}: {np.round(upper, 2)}'
    ax.axhline(mean_differences + sd_limit*stand_dev_differences, color='red', linestyle='-', label=labels_2_print)
    # draw the three lines in the Bland-Altman here -SD
    labels_2_print = f'-SD {sd_limit}: {np.round(lower, 2)}'
    ax.axhline(mean_differences - sd_limit*stand_dev_differences, color='blue', linestyle='-', label=labels_2_print)

    # set title and labels
    ax.set_title(f"Bland-Altman Plot ({str_y_name} {value_plotted} vs. {str_x_name} {value_plotted}) N={len(x)}\n" +
                 f"{deming_str}\n{log_transformation_string}")
    y_label = f"Differences ({str_x_name} {value_plotted} - {str_y_name} {value_plotted})"
    ax.set_ylabel(y_label)
    ax.set_xlabel(f"Means ([{str_x_name} {value_plotted} + {str_y_name} {value_plotted}] / 2)")

    # might have function calling this take care of plotting the legend
    if plot_legend is True:
        ax.legend(loc='upper right', frameon=True)

    return differences, y_label


def _mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), stats.sem(a)
    h = se * stats.t.ppf((1 + confidence) / 2., n-1)
    return m, m-h, m+h