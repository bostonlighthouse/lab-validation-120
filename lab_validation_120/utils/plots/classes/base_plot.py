"""Base class for plotting"""
from matplotlib import pyplot as plt

class FileNameIssue(Exception):
    """Class for when a first instance is created and no file name is provided"""
    def __init__(self, type=None):
        self.err = None
        self.type = type
        super(FileNameIssue, self).__init__()

    def __str__(self):
        if self.type is None:
            self.err = "No file name given for the first instance of your Plot"
        else:
            self.err = f"{self.type}: File name was set in the first instance of your Plot, do not try and reset"
        return self.err


class BasePlot():
    """Simple class for the base plotting"""
    __file_name = None

    def __init__(self, ax=None, fig=None, x_data_list=None, y_data_list=None, title=None, xlabel=None, ylabel=None,
                 xticks=None, yticks=None, file_name=None, size_image=(10, 10), data_frame=None, ylim=None,
                 xlim=None):
        self.ax = ax
        self.fig = fig
        self.x_data_list = x_data_list
        self.y_data_list = y_data_list
        self.data_frame = data_frame
        self.size_image = size_image
        self.title = title

        self.xlabel = xlabel
        self.ylabel = ylabel
        self.xticks = xticks
        self.yticks = yticks
        self.ylim = ylim
        self.xlim = xlim

        self.title = title

        # set the class attributes
        # these should only be set once, but shared across all instances that inherit from this
        if BasePlot.__file_name is None and file_name is None:
            raise FileNameIssue()
        #elif BasePlot.__file_name is not None and file_name is not None:
        #    raise FileNameIssue(type="Resetting")
        elif file_name is not None:
            BasePlot.__file_name = file_name

        self.draw_plot()

    def __str__(self):
        print(f"x_data_list = {self.x_data_list}")

    def __set_title_and_labels(self):
        self.ax.title.set_text(self.title)
        # was there both a ylabel and xlabel?
        if self.xlabel is not None and self.ylabel is not None:
            self.ax.set(xlabel=self.xlabel, ylabel=self.ylabel)
        # only an xlabel
        elif self.xlabel is not None and self.ylabel is None:
            self.ax.set(xlabel=self.xlabel)
        # only a ylablel
        elif self.ylabel is not None:
            self.ax.set(ylabel=self.ylabel)

        # was there both a yticks and yticks?
        if self.xticks is not None and self.yticks is not None:
            self.ax.set(xticks=self.xticks, yticks=self.yticks)
        # only an xticks
        elif self.xticks is not None and self.yticks is None:
            self.ax.set(xticks=self.xticks)
        # only a yticks
        elif self.yticks is not None:
            self.ax.set(yticks=self.yticks)
        #self.ax.set(xticks=range(0, 105, 5))

    def draw_plot(self):
        if self.size_image is not None:
            self.fig.set_size_inches(self.size_image)
        self.__set_title_and_labels()

        if self.xlim is not None:
            self.ax.set_xlim(self.xlim)
        if self.ylim is not None:
            self.ax.set_ylim(self.ylim)

    def finalize_plot(self):
        """Used to store the final plot"""
        self.fig.tight_layout()
        plt.savefig(BasePlot.__file_name)
        plt.close(self.fig)  # have to close





