"""Seaborn histplot class"""

import seaborn as sns
from lab_validation_120.utils.plots.classes.base_plot import BasePlot


class HistplotClass(BasePlot):
    """specific class for histplot"""

    def __init__(self, ax=None, fig=None, x_data_list=None, y_data_list=None, title=None, xlabel=None, ylabel=None,
                 xticks=None, yticks=None, file_name=None, size_image=None, data_frame=None,
                 kde=False, color='red', bins=None, log_scale=(False, False), label_bins=False, xlim=None, ylim=None):
        self.kde = kde
        self.color = color
        self.bins = bins
        self.log_scale = log_scale
        self.label_bins = label_bins
        super().__init__(ax=ax, fig=fig, x_data_list=x_data_list, y_data_list=y_data_list, title=title, xlabel=xlabel,
                         ylabel=ylabel, xticks=xticks, yticks=yticks, file_name=file_name, size_image=size_image,
                         data_frame=data_frame, xlim=xlim, ylim=ylim)
        #self.draw_plot()

    def draw_plot(self):
        super().draw_plot()
        # plot the distribution of the data, if bins was None, just let the histplot take care of it
        if self.bins is None:
            sns.histplot(self.x_data_list, kde=self.kde, ax=self.ax, color=self.color,
                         log_scale=self.log_scale)
        else:
            sns.histplot(self.x_data_list, kde=self.kde, ax=self.ax, color=self.color, bins=self.bins,
                         log_scale=self.log_scale)

        if self.label_bins is True:
            # label each bar in histogram
            for p in self.ax.patches:
                height = p.get_height()  # get the height of each bar
                if height == 0:
                    continue
                # adding text to each bar
                self.ax.text(x=p.get_x() + (p.get_width() / 2),
                             # x-coordinate position of data label, padded to be in the middle of the bar
                             y=height + 0.2,  # y-coordinate position of data label, padded 0.2 above bar
                             s='{: .0f}'.format(height),  # data label, formatted to ignore decimals
                             ha='center')  # sets horizontal alignment (ha) to center
