import math
import os
import numpy as np
from matplotlib import pyplot as plt, patches as mpatches
from scipy import stats
import seaborn as sns

from lab_validation_120.utils.concordance_utilites import get_overall_sample_metrics_concordance_df
from lab_validation_120.utils.plots.distribution_plot_utilities import plot_distribution_of_data
from lab_validation_120.utils.plots.plot_utilities import return_dictionary_x_y_values, get_x_y_list_of_points, \
    get_plot_file_type, get_consequential_str, global_rc_set, get_subplot_bounding_box_parameters
from lab_validation_120.utils.plots.bland_altman_plot_utilities import bland_altman_plot
from lab_validation_120.utils.plots.deming_regession_plot_utilities import deming_regression_plot
from lab_validation_120.utils.plots.qq_plot_utilities import plot_qq_plot
from lab_validation_120.utils.pandas_dataframes_utilities import sort_df_by_outliers_in_place, print_data_frame


def process_regressions_from_reproducibility_output(config=None, key_to_search=None, consequential_impacts_only=None,
                                                    alpha=None,
                                                    df_file_prefix=None,
                                                    plot_regressions_for_individual_sample=False):
    """
    Open the file in the config and produce regression analysis
    :param config: The dictionary from the config
    :param consequential_impacts_only: Boolean testing if we only looked at consequential impacts or all impacts
    :param key_to_search: What key to pull out of the config dictionary to get the values needed, e.g.:
    :param alpha: Matplotlib allows you to adjust the transparency of a graph plot using the alpha attribute
    "clinical_sample_reproducibility"
    :param df_file_prefix: Some data frames get printed and will be printed with this file name prefix
    :param plot_regressions_for_individual_sample: Plot for individual samples?  Default False
    """

    # look at specific file from from clinical_sample_reproducibility.py, see config for the file
    files_to_boxplot = config[key_to_search]['files_for_regression']
    for variant_type, dict_values in files_to_boxplot.items():
        # at this point all data is in df_overall_concordance, so the actual regression analysis can be completed
        if consequential_impacts_only is True:
            df_overall_concordance = get_overall_sample_metrics_concordance_df(dict_values['file'][0])
        else:
            df_overall_concordance = get_overall_sample_metrics_concordance_df(dict_values['file'][1])

        # get some values form the config file, that were stored in dict_values
        vaf_threshold = dict_values['vaf_threshold']
        depth_threshold = dict_values['depth_threshold']
        min_length_variant = None  # for indels
        max_length_variant = None

        # call to get a string based on whether you are measuring consequential impacts or not (Bool)
        consequential_impacts_str = get_consequential_str(consequential_impacts_only)

        subtitle = f"Thresholds: VAF >= {vaf_threshold} Depth >= {depth_threshold} {consequential_impacts_str}"
        # if we're plotting indels, do something slightly different, note keys for indels might be like indel_1_1
        # for min max length so just adjust
        if variant_type.startswith("indel"):
            variant_type = 'indel'
            # there was not enough indels to plot individually, so just print all
            #df_overall_concordance['Sample_ID'] = "All"
            min_length_variant = dict_values['min_length_variant']
            max_length_variant = dict_values['max_length_variant']
        elif variant_type == 'snv':
            if plot_regressions_for_individual_sample is True:
                # plot before we changes the 'Sample_ID' column to all
                plot_regression_charts(df=df_overall_concordance, config=config, subtitle=subtitle,
                                       vaf_threshold=vaf_threshold, depth_threshold=depth_threshold,
                                       variant_type=variant_type, key_to_search=key_to_search, alpha=alpha,
                                       consequential_impacts_only=consequential_impacts_only)
            # Also print entire for snv
            # but store the original name
            #df_overall_concordance['Sample_ID_temp'] = df_overall_concordance['Sample_ID']
            #df_overall_concordance['Sample_ID'] = "All"

        df_overall_concordance['Sample_ID_temp'] = df_overall_concordance['Sample_ID']
        df_overall_concordance['Sample_ID'] = "All"
        df_overall_concordance = df_overall_concordance[(df_overall_concordance['REP1'] == '+') &
                                                        (df_overall_concordance['REP2'] == '+')]
        # do the plotting
        plot_regression_charts(df=df_overall_concordance, config=config, subtitle=subtitle,
                               vaf_threshold=vaf_threshold, depth_threshold=depth_threshold,
                               variant_type=variant_type, key_to_search=key_to_search,
                               min_length_variant=min_length_variant, max_length_variant=max_length_variant,
                               alpha=alpha,
                               consequential_impacts_only=consequential_impacts_only)
        # print out the intervals for the vcf regression
        regression_file = f'{variant_type}_vcf_regression_data.txt'
        if df_file_prefix is not None:
            print_data_frame(df=df_overall_concordance, tabular_output_file=regression_file)
        else:
            print_data_frame(df=df_overall_concordance,
                             tabular_output_file=f'{df_file_prefix}_{regression_file}')


def plot_regression_charts(df=None, config=None, subtitle=None, vaf_threshold=None, depth_threshold=None,
                           variant_type=None, key_to_search=None, min_length_variant=None, max_length_variant=None,
                           alpha=None, consequential_impacts_only=None):
    """
    This will call the plotting of regression over different values, by then calling: process_regression_and_bland_altman_analysis
    :param df: the data frame to be used for regression plotting
    :param config: dictionary of values used for configuration
    :param subtitle: Subtitle to be printed
    :param vaf_threshold: VAF threshold to be used
    :param depth_threshold: VAF depth_threshold to be used
    :param variant_type: snv or indel
    :param key_to_search: What key to pull out of the config dictionary to get the values needed, e.g.:
    "concordance_clinical_samples_reproducibility"
    :param min_length_variant: minimum length of the variant, not snv are a 0, b/c an indel is 1 or -1
    :param max_length_variant: maximum length of the variant, not snv are a 0, b/c an indel is 1 or -1
    :param alpha: Matplotlib allows you to adjust the transparency of a graph plot using the alpha attribute
    :param consequential_impacts_only: Boolean testing if we only looked at consequential impacts or all impacts
    """

    # get the name of the type we will be looking at
    # there are three different types of plots we will look at from a regression stand point
    for type_chart in ['vaf_regression_plot', 'depth_regression_plot', 'allele_count_regression_plot']:
        if min_length_variant is not None:
            file_name_add_on = f"{variant_type}_{type_chart}_vaf_{vaf_threshold}_depth_{depth_threshold}" \
                               f"_size_{min_length_variant}_{max_length_variant}"
        else:
            file_name_add_on = f"{variant_type}_{type_chart}_vaf_{vaf_threshold}_depth_{depth_threshold}"
        file_name_add_on = f"{file_name_add_on}_consequential_impacts_only_{str(consequential_impacts_only)}"
        # call the plot_utilities.py function
        process_regression_and_bland_altman_analysis(
            df=df, group_by_key="Sample_ID",
            images_dir=config[key_to_search]['images_dir'],
            config_plot=config[key_to_search][type_chart],
            subtitle=subtitle,
            file_name_add_on=file_name_add_on,
            alpha=alpha)


def process_regression_and_bland_altman_analysis(df=None, group_by_key=None, images_dir=None, config_plot=None, subtitle=None,
                                                 file_name_add_on=None, alpha=None):
    """
    Take in the two dimensional list of data in final_matched_results, and create a data frame, updating VAF values,
    and then call each horizon sample via matched_results_df.groupby(["Horizon_ID"]), or another key, group_by_key
    :param df: the pandas data frame with values to be used for plotting
    :param group_by_key: the column in the df to call groupby on
    :param images_dir: where to store the images that will be created
    :param config_plot: dictionary from the JSON config file for the entry using the key e.g. vaf_regression_plot
    :param subtitle: A subtitle to print on the regression plot
    :param file_name_add_on: Add on to the image file name
    :param alpha: Matplotlib allows you to adjust the transparency of a graph plot using the alpha attribute
    :return data frame
    """
    # TODO remove from the config?
    x_axis_label = config_plot['x_axis_label']  # e.g. 395 VAF'
    y_axis_label = config_plot['y_axis_label']  # e.g 'Horizon VAF'

    x_value_key = config_plot['x_value_key']  # e.g. '395_vaf'
    y_value_key = config_plot['y_value_key']  # e.g. 'horizon_vaf'

    df_value1 = config_plot['df_value1']  # e.g 'ALT_AF'
    df_value2 = config_plot['df_value2']  # e.g. 'VAF'

    value_plotted = config_plot["value_plotted"]  # e.g. VAF, or DEPTH

    #if value_plotted == 'VAF':
    df[df_value1] = df[df_value1].astype(float)
    df[df_value2] = df[df_value2].astype(float)

    # just a helper function
    # To stop the function from from printing set print_data_frame=False
    sort_df_by_outliers_in_place(df=df, column1=df_value1, column2=df_value2,
                                 string_2_print=f"Samples grouped by {group_by_key}", print_data_frame=False)

    for sample_name, data_grouped_by_horizon in df.groupby(group_by_key):
        # find the max to plot on the chars
        # can't just use df.max here, b/c there could be a NaN as the first number:
        # So if you start with nan as the first value in the array, every subsequent comparison will be check
        # whether some_other_value > nan. so use np.nanmax instead
        max_x = math.ceil(np.nanmax(data_grouped_by_horizon[df_value1]) / .05) * .05
        max_y = math.ceil(np.nanmax(data_grouped_by_horizon[df_value2]) / .05) * .05
        max_vaf_2_plot = max_x if max_x > max_y else max_y  # this will control the max x and y-axis
        # just give it a little cushion
        #max_vaf_2_plot += .05

        dict_horizon_vals = dict(data_grouped_by_horizon.apply(lambda row:
                                                               return_dictionary_x_y_values(row_from_df=row,
                                                                                            x_value_key=x_value_key,
                                                                                            y_value_key=y_value_key,
                                                                                            df_value1=df_value1,
                                                                                            df_value2=df_value2),
                                                               axis=1))
        # add the sample name to the x-axis and y-axis:
        x_axis_label_temp = " ".join((x_axis_label, f"[Sample(s): {sample_name.upper()}]"))
        y_axis_label_temp = " ".join((y_axis_label, f"[Sample(s): {sample_name.upper()}]"))
        # print out the plot
        plot_regression_and_bland_altman(dict_sample_vals=dict_horizon_vals, sample_name=sample_name, images_dir=images_dir,
                                         max_x_axis_2_plot=max_vaf_2_plot, max_y_axis_2_plot=max_vaf_2_plot,
                                         x_axis_label=x_axis_label_temp, y_axis_label=y_axis_label_temp,
                                         x_value_key=x_value_key, y_value_key=y_value_key, subtitle=subtitle,
                                         file_name_add_on=file_name_add_on, value_plotted=value_plotted,
                                         alpha=alpha)


def plot_regression_and_bland_altman(dict_sample_vals=None, sample_name=None, images_dir=None, max_x_axis_2_plot=None,
                                     max_y_axis_2_plot=None,
                                     x_axis_label=None, y_axis_label=None, x_value_key=None, y_value_key=None, subtitle=None,
                                     file_name_add_on=None, value_plotted=None, alpha=None):
    """
    Used to take the dict of dictionaries and and get x , y lists of TMP values
    :param dict_sample_vals:  e.g. {0: {'horizon_vaf': 10.7, '395_vaf': 9.9}, 1: {'horizon_vaf': 10.0, '395_vaf': 10.7}
    :param sample_name: A name to use in the tile and file name of of the plot
    :param images_dir: Where to store the image, if directory does not exist it will be made
    :param max_x_axis_2_plot: The max value for the VAF, or really whatever values
    :param max_y_axis_2_plot: The max value for the VAF, or really whatever values
    :param x_axis_label: Label for the x-axis
    :param y_axis_label: Label for the y-axis
    :param x_value_key what column name in the data frame to use for the x-axis
    :param y_value_key what column name in the data frame to use for the y-axis
    :param subtitle: Is there a subtitle you want to print?
    :param file_name_add_on: Is there something to added to the file names
    :param value_plotted: What type of value plotted, e.g. VAF, DEPTH pass into the function
    :param alpha: Matplotlib allows you to adjust the transparency of a graph plot using the alpha attribute
    """
    # size of the images
    global_rc_set()
    plt.rcParams["figure.figsize"] = [31, 9]  # 3 plots in a row, so 23 worked best to keep them each square
    fig, ax = plt.subplots(1, 4)  # rows x cols
    x, y = get_x_y_list_of_points(dict_values=dict_sample_vals, x_value_key=x_value_key, y_value_key=y_value_key)
    regression_plot(ax=ax[0], x=x, y=y,
                    max_x_axis_2_plot=max_x_axis_2_plot, max_y_axis_2_plot=max_y_axis_2_plot,
                    x_axis_label=x_axis_label, y_axis_label=y_axis_label,
                    value_plotted=value_plotted, alpha=alpha)
    # print out the bland - altman plot
    log_transformation = False
    differences, ba_y_axis_label = bland_altman_plot(ax=ax[1], x=x, y=y, str_x_name=x_axis_label,
                                                     str_y_name=y_axis_label,
                                                     value_plotted=value_plotted,
                                                     log_transformation=log_transformation)
    # update this label
    ba_y_axis_label = f"Distribution: {ba_y_axis_label}"

    # plot the distribution of data, and include a normal data plot using values from the differences
    fitted_normal_distr_values = plot_distribution_of_data(ax=ax[2], values=differences,
                                                           legend_value_for_values="Differences between X and Y",
                                                           plot_normal_distribution=True, plot_legend=True,
                                                           x_axis_label=ba_y_axis_label, value_plotted=value_plotted)
    #
    plot_qq_plot(ax=ax[3], sample1=list(differences), sample2=fitted_normal_distr_values,
                 x_axis_label="Normal Distribution", y_axis_label=ba_y_axis_label,
                 sample1_distribution_str="Differences",
                 sample2_distribution_str="Normal Distribution")

    fig.tight_layout(rect=get_subplot_bounding_box_parameters())

    # store the image
    if not os.path.exists(images_dir):
        os.makedirs(images_dir)
    if file_name_add_on is not None:
        # user sent in an add-on, assuming they will call it "_regression" something...
        plt.savefig(os.path.join(
            images_dir,
            "_".join(
                (file_name_add_on,
                 f"regression_ba_log_transformation_{str(log_transformation)}_verified_and_presence_confirmed_mutations",
                 sample_name)) + get_plot_file_type()))
    else:
        plt.savefig(os.path.join(
            images_dir,
            "_".join(
                (f"regression_ba_log_transformation_{str(log_transformation)}_verified_and_presence_confirmed_mutations",
                 sample_name)) + get_plot_file_type()))

    plt.close(fig)  # have to close


def plot_regression(dict_sample_vals=None, max_x_axis_2_plot=None, max_y_axis_2_plot=None,
                    x_axis_label=None, y_axis_label=None, x_value_key=None, y_value_key=None,
                    value_plotted=None, ax=None, plot_reference_line=True, size_point=None,
                    r_value_squared_threshold=0.60, alpha=None):
    """
    Used to take the dict of dictionaries and and get x , y lists of TMP values
    *Note, this can not be removed to just call regression_plot***
    :param max_x_axis_2_plot: The max value for the VAF, or really whatever values
    :param max_y_axis_2_plot: The max value for the VAF, or really whatever values
    :param x_axis_label: Label for the x-axis
    :param y_axis_label: Label for the y-axis
    :param x_value_key what column name in the data frame to use for the x-axis
    :param y_value_key what column name in the data frame to use for the y-axis
    :param value_plotted: What type of value plotted, e.g. VAF, DEPTH
    :param ax: an instance of the class plt.Axes
    :param alpha: Matplotlib allows you to adjust the transparency of a graph plot using the alpha attribute
    """
    x, y = get_x_y_list_of_points(dict_values=dict_sample_vals, x_value_key=x_value_key, y_value_key=y_value_key)
    regression_plot(ax=ax, x=x, y=y,
                    max_x_axis_2_plot=max_x_axis_2_plot, max_y_axis_2_plot=max_y_axis_2_plot,
                    x_axis_label=x_axis_label, y_axis_label=y_axis_label,
                    value_plotted=value_plotted, plot_reference_line=plot_reference_line,
                    size_point=size_point, r_value_squared_threshold=r_value_squared_threshold,
                    alpha=alpha)


def regression_plot(ax=None, x=None, y=None, max_x_axis_2_plot=None, max_y_axis_2_plot=None,
                    x_axis_label=None, y_axis_label=None,
                    value_plotted=None, plot_reference_line=True, size_point=35,
                    r_value_squared_threshold=0.60, horizontal_threshold_line_value=None,
                    horizontal_threshold_line_label = None,
                    add_2_x_axis=None, add_2_y_axis=None, alpha=None,
                    legend_location='upper left'):

    """
    Plot the regression line
    :param ax: an instance of the class plt.Axes
    :param x: list of values for x
    :param y: list of values for y
    :param max_x_axis_2_plot: max value to plot on the x axis
    :param max_y_axis_2_plot: max value to plot on the y axis
    :param x_axis_label: Label for the x axis
    :param y_axis_label: Label for the y axis
    :param value_plotted: Something to add to the x-axis and y-axis label
    :param plot_reference_line: For VAF and others, plot a perfect 45 degree line, boolean
    :param size_point: Size of the points to plot
    :param r_value_squared_threshold: What is the threshold to plot the regression line
    :param alpha: Matplotlib allows you to adjust the transparency of a graph plot using the alpha attribute
    """
    chart_rounding = 3
    alpha_for_stats = 0.05
    # calculate values needed for plotting or for return
    # Calculate a Spearman correlation coefficient with associated p-value.
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.spearmanr.html
    rho, pvalue_sp = stats.spearmanr(x, y)  # pylint: disable=unused-variable
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html
    slope, intercept, r_value, pvalue_lr, std_err = stats.linregress(x, y)  # pylint: disable=unused-variable
    r_value_squared = round(r_value ** 2, chart_rounding)
    number_points = len(x)

    # All plotting from here...
    if max_y_axis_2_plot is not None:
        ax.set_ylim(0, max_y_axis_2_plot)
    if max_x_axis_2_plot is not None:
        ax.set_xlim(0, max_x_axis_2_plot)
    # x axis
    if value_plotted is not None:
        ax.set_xlabel(f"{x_axis_label} {value_plotted}")
    elif add_2_x_axis is not None:
        ax.set_xlabel(f"{x_axis_label} {add_2_x_axis}")
    else:
        ax.set_xlabel(f"{x_axis_label}")

    # x axis
    if value_plotted is not None:
        ax.set_ylabel(f"{y_axis_label} {value_plotted}")
    elif add_2_y_axis is not None:
        ax.set_ylabel(f"{y_axis_label} {add_2_y_axis}")
    else:
        ax.set_ylabel(f"{y_axis_label}")

    # Draw a horizontal line
    if horizontal_threshold_line_value is not None:
        ax.axhline(y=horizontal_threshold_line_value, color='r', linestyle='--', label=horizontal_threshold_line_label)

    # print the actual vaf values
    sns.scatterplot(x=x, y=y, ax=ax, s=size_point, alpha=alpha)

    if plot_reference_line is True:
        # the reference red dotted line
        reference_line = [*range(0, int(max_x_axis_2_plot))]
        ax.plot(reference_line, reference_line, 'r--', label='45 degree line')

    labels_2_print = f'N = {number_points}  R^2 = {r_value_squared} \n' \
                     f'y = Std.E {round(std_err, chart_rounding)}\n' \
                     f'y = {round(slope, chart_rounding)}x + {round(intercept, chart_rounding)}\n' \
                     f'Spearman = {round(rho, chart_rounding)} '

    # if the r2 is really bad, don't plot the regression line
    if r_value_squared > r_value_squared_threshold:
        # fit the confidence interval line
        yl, yu, xd, ci = _linear_regression_ci(xd=x, yd=y, slope=slope, intercept=intercept, alpha=alpha_for_stats,
                                               chart_rounding=chart_rounding)
        ax.fill_between(xd, yl, yu, alpha=0.3, facecolor='blue', edgecolor='none', label="95% CI")
        ax.plot(x, intercept + slope * x, 'b', label=labels_2_print)  # plot the regression line

    # Title for the Chart
    if value_plotted is not None:
        title = f'{y_axis_label} {value_plotted} vs. {x_axis_label} {value_plotted}'
    else:
        title = f'{y_axis_label} vs. {x_axis_label}'

    ax.set_title(title, fontdict=None, loc='center', pad=None)

    ax.legend(loc=legend_location, frameon=True, fontsize=12)
    ax.grid(True)


def _linear_regression_ci(xd=None, yd=None, slope=None, intercept=None, alpha=0.5, chart_rounding=3):
    """
    :param xd: data arrays
    :param yd: data arrays
    :param slope: linear fit parameters as in y=ax+b (here x)
    :param intercept: linear fit parameters as in y=ax+b (here b)
    :param alpha: desired confidence level, by default 0.05 (2 sigma)
    :param chart_rounding: What decimal to round to
    :return: Sequence (yl, yu, x, ci_range) with the arrays holding the lower and upper confidence bands
    corresponding to the [input] x array, and the ci-range list returned from stats.t.interval
    """

    n = xd.size  # data sample size
    # Returns 100 evenly spaced samples, calculated over the interval [start, stop]
    x = np.linspace(xd.min(), xd.max(), 100)
    y = slope * x + intercept  # Predicted values (best-fit model)

    sd = 1.0 / (n - 2.0) * np.sum((yd - slope * xd - intercept) ** 2)  # Scatter of data about the model
    se = np.sqrt(sd)  # standard error
    sxd = np.sum((xd - xd.mean()) ** 2)
    sx = (x - xd.mean()) ** 2

    # quantile of student's t distribution for p=1-alpha/2
    q = stats.t.ppf(1. - alpha / 2, n - 2)
    # get the actual confidence interval
    ci_range = [round((slope + se * t), chart_rounding) for t in stats.t.interval(alpha / 2, len(x) - 2)]

    # get the upper and lower CI for the confidence band:
    ci = q * se * np.sqrt(1.0 / n + sx / sxd)
    yl = y - ci  # upper confidence band for y
    yu = y + ci  # lower confidence band for y

    return yl, yu, x, ci_range
