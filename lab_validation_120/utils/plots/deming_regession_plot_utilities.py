import numpy as np
import pandas as pd


def deming_regression_plot(ax=None, x=None, y=None, deming_plot_color='green', plot_legned=True):
    # deming regression: https://en.wikipedia.org/wiki/Deming_regression#Solution
    data = pd.DataFrame({'x': x, 'y': y})
    cov = data.cov()
    mean_x = data['x'].mean()
    mean_y = data['y'].mean()
    s_xx = cov['x']['x']
    s_yy = cov['y']['y']
    s_xy = cov['x']['y']
    delta = 2 ** 2
    slope = (s_yy - delta * s_xx + np.sqrt((s_yy - delta * s_xx) ** 2 + 4 * delta * s_xy ** 2)) / (2 * s_xy)
    intercept = mean_y - slope * mean_x

    z = [data['x'].min(), data['x'].max()]
    ax.plot(z, list(map(lambda x: intercept + slope * x, z)), label='Deming Regression\n(accounts for errors in $x$)',
            color=deming_plot_color, linestyle='--')

    if plot_legned is True:
        ax.legend(loc='upper right', frameon=True)