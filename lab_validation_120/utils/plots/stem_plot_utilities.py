import os

from matplotlib import pyplot as plt

from lab_validation_120.utils.concordance_utilites import indel_concordance_size_thresholds
from lab_validation_120.utils.plots.plot_utilities import get_plot_file_type, get_consequential_str


def reproducibility_stem_plot(df=None, variant_type=None, images_dir=None, equivalency_study=None):
    """
    :param df: the data frame of all values for reproducibility
    :param images_dir: Where to store the final image created
    :param equivalency_study: Boolean on if this was an equivalency_study, if so we will drop all discordance
    in the df for REP1, since we are only looking at recall. i.e. discordance only count in Sample 2
    :param variant_type: snv or indel
    """
    # values in the list of tuples: 0 index is the vaf lower limit to plot on the x-axis
    #                               1 index is the vaf upper limit to plot on the x-axis
    #                               2 y-axis min threshold
    #                               3 The variant type to look at, i.e. snv or indel
    #                               4 The experiment type (both - inter and intra run)
    values_to_run = []
    if variant_type == 'snv':
        if equivalency_study is True:
            values_to_run1 = [(1, 3, 80, "snv", experiment, 0, 0) for experiment in ("both",)]
            values_to_run2 = [(4, 6, 80, "snv", experiment, 0, 0) for experiment in ("both",)]
            values_to_run = values_to_run1 + values_to_run2
        else:
            values_to_run = [(1, 3, 94, "snv", experiment, 0, 0) for experiment in ("both", "intra-run", "inter-run")]
    else:
        # create a list of tuples to go over in the inner for loop below.  With indels we need to plot at different
        # vaf values, (1-3) (4-6) (8-10).  Start with the min and max length on the outer loop
        # then plot the experiment type in the inner loop, finally the vaf on the final inner loop on this comprehension
        # Don't change the order of these for loops
        if equivalency_study is True:
            values_to_run = [iii + ("indel", "both") + i for i in indel_concordance_size_thresholds()
                             for iii in [(1, 6, 0), (4, 9, 0), (5, 10, 0)]  # was orginall (1, 3, 0), (4, 6, 0), (8, 10, 0
                             ]
        else:
            values_to_run = [iii + ii +  i for i in indel_concordance_size_thresholds()
                             for ii in [("indel", "both"),
                                        ("indel", "intra-run",),
                                        ("indel", "inter-run")]
                             for iii in [(1, 3, 0), (4, 6, 0), (8, 10, 0)]
                             ]
    # plot stem plot for indels and snvs.  SNV will have 3 plots for each consequence type, and Indels will have 27
    # plots per each consequence.  This will generate a 4 plots.  2 for SNVs by consequence and 2 for indels by
    # consequence
    for consequential_impacts_only in [True, False]:
        # SNV only have three plots per consequential_impacts_only
        if variant_type == 'snv':
            plt.rcParams["figure.figsize"] = [10, len(values_to_run) * 4]  # width, height in inches, use 4 inches per plot
            fig, ax = plt.subplots(len(values_to_run), 1)  # rows x cols
            depth_threshold = 600
        else:
            plt.rcParams["figure.figsize"] = [10, len(values_to_run) * 4]  # width, height in inches, use 4 inches per plot (27 plots)
            fig, ax = plt.subplots(len(values_to_run), 1)  # rows x cols
            depth_threshold = 250

        for index, (vaf_lower_limit, vaf_upper_limit, y_lower_lim, variant_type, experiment_type,
                    min_length_variant, max_length_variant) in enumerate(values_to_run):
            process_stem_plot_reproducibility(df=df,
                                              ax=ax[index],
                                              consequential_impacts_only=consequential_impacts_only,
                                              vaf_lower_limit=vaf_lower_limit,
                                              vaf_upper_limit=vaf_upper_limit,
                                              y_lower_lim=y_lower_lim,
                                              variant_type=variant_type,
                                              experiment_type=experiment_type,
                                              min_length_variant=min_length_variant,
                                              max_length_variant=max_length_variant,
                                              equivalency_study=equivalency_study,
                                              depth_threshold=depth_threshold)

        fig.tight_layout()
        file_name_str = "_".join(("reproducibility_rates_for", variant_type))
        # indels have a size that is used.  FYI snvs do to, but the min and max is 0, 0
        if consequential_impacts_only is True:
            plt.savefig(os.path.join(images_dir,
                                     "_".join(
                                         (file_name_str, "consequential_variant_impacts_only",
                                          f"equivalency_study_{equivalency_study}")) + get_plot_file_type()))
        else:
            plt.savefig(os.path.join(images_dir,
                                     "_".join(
                                         (file_name_str, "all_variant_impacts",
                                          f"equivalency_study_{equivalency_study}")) + get_plot_file_type()))
        plt.close(fig)  # have to close


def process_stem_plot_reproducibility(df=None, consequential_impacts_only=None,
                                      depth_threshold=600, vaf_lower_limit=None,
                                      vaf_upper_limit=None, y_lower_lim=None, variant_type=None,
                                      experiment_type=None, min_length_variant=None,
                                      max_length_variant=None,
                                      ax=None, equivalency_study=None):
    """Main driver used to produce the stem plots
    :param df: the data frame of all values for reproducibility
    :param consequential_impacts_only: Boolean on whether to look at consequential impacts only
    :param depth_threshold:  We are not looking past this depth, b/c it's high correlation
    :param equivalency_study: Is this an equivalency study, e.g. oc120 vs oc120plus, then only compare the second sample
    """

    df_queried_to_plot = _return_data_frame_from_concordance_query(df=df,
                                                                   experiment_type=experiment_type,
                                                                   consequential_impacts_only=
                                                                   consequential_impacts_only,
                                                                   vaf_lower_limit=vaf_lower_limit,
                                                                   vaf_upper_limit=vaf_upper_limit,
                                                                   depth_threshold=depth_threshold,
                                                                   variant_type=variant_type,
                                                                   min_length_variant=min_length_variant,
                                                                   max_length_variant=max_length_variant)

    #print(f"variant_type {variant_type} vaf_lower_limit {vaf_lower_limit} vaf_upper_limit {vaf_upper_limit}")
    #print(df_queried_to_plot)

    plot_stem_chart(ax=ax, df=df_queried_to_plot, vaf_lower_limit=vaf_lower_limit, vaf_upper_limit=vaf_upper_limit,
                    y_lower_lim=y_lower_lim, variant_type=variant_type,
                    experiment_type=experiment_type,
                    consequential_impacts_only=consequential_impacts_only,
                    min_length_variant=min_length_variant,
                    max_length_variant=max_length_variant,
                    equivalency_study=equivalency_study)


# TODO: This right now has wording in it for "Reproducibility", but this can also be "Concordance", add an option
def plot_stem_chart(ax=None, df=None, vaf_lower_limit=None, vaf_upper_limit=None, y_lower_lim=None,
                    variant_type=None, experiment_type=None, consequential_impacts_only=None,
                    min_length_variant=None,
                    max_length_variant=None, equivalency_study=None):
    """
    Function is used for stem plotting... Actual stem plotting is done by: generic_stem_plot()
    :param ax: an instance of the class plt.Axes
    :param df: data frame to use for plotting
    :param vaf_lower_limit: VAF lower limit to look at, i.e. <= only plot values
    :param vaf_upper_limit: VAF lower limit to look at, i.e. >= only plot values
    :param y_lower_lim: Lower limit on the y-axis to plot
    :param variant_type: indel or snv
    :param experiment_type: What this an inter-run or inter-run or both TODO, need to add more to this so it's abstract
    :param consequential_impacts_only: Boolean testing if we only looked at consequential impacts or all impacts
    :param equivalency_study: Is this an equivalency study, e.g. oc120 vs oc120plus, then only compare the second sample
    """

    # set the horizontal threshold that should be plotted for the % concordance, i.e 98 = 98% reproducibility
    # keep these values for horizontal thresholds here, b/c all concordance and reproducibility studies should use them
    if variant_type == 'snv':
        if equivalency_study is True:
            horizontal_threshold_line_value = 95
        else:
            horizontal_threshold_line_value = 98  # was 98 for concordance study
        variant_string = f"{variant_type.upper()}s"
    elif variant_type == 'indel':
        horizontal_threshold_line_value = 95
        if min_length_variant == 1 and max_length_variant == 1:
            variant_string = f"{variant_type.upper()}s length {min_length_variant}bp"
        else:
            variant_string = f"{variant_type.upper()}s length {min_length_variant}-{max_length_variant}bp"
    else:
        raise Exception(f"Don't know this variant type{variant_type}")

    # call to get a string based on whether you are measuring consequential impacts or not (Bool)
    consequential_impacts_str = get_consequential_str(consequential_impacts_only)

    if experiment_type == 'both':
        #title = 'Both Intra/Inter-run'
        title = 'GS120 vs. GS12Plus'

    else:
        title = f'{experiment_type[0].upper()}{experiment_type[1:]}'

    title = f'{title} Reproducibility, {consequential_impacts_str}, {variant_string} ' \
            f'({int(vaf_lower_limit)}-{int(vaf_upper_limit)}% VAF)'

    generic_stem_plot(ax=ax, df=df, title=title, xkey='Composite_key', ykey='Percent_positive',
                      xlabel='VAF(%) Depth(x) Occurrence(#)', ylabel='Reproducibility Rate (%)',
                      y_lower_lim=y_lower_lim,
                      y_upper_lim=110, print_stem_point_value=True,
                      horizontal_threshold_line_value=horizontal_threshold_line_value)


def generic_stem_plot(ax=None, df=None, title=None, xkey=None, ykey=None, xlabel=None, ylabel=None,
                      y_lower_lim=None, y_upper_lim=None, print_stem_point_value=False,
                      horizontal_threshold_line_value=None):
    """
    Function that's tightly coupled to this program that will go over an call the stem plotting...
    :param ax: an instance of the class plt.Axes
    :param df: data frame to use for plotting
    :param title: Title for the plot
    :param xkey: Column to pull out of the df for the x variable
    :param ykey: Column to pull out of the df for the y variable
    :param xlabel: X axis label
    :param xlabel: Y axis label
    :param y_lower_lim: Lower limit on the y-axis to plot
    :param y_upper_lim: Lower limit on the y-axis to plot, 100 for not, i.e. 100%
    :param print_stem_point_value: Should the values of the stem points be plotted, do not print 0.0 or 100.0
    :param horizontal_threshold_line_value: Where to draw a threshold line on the y-axis


    """

    x = df[xkey].to_list()
    y = df[ykey].to_list()
    # plot the stem plot
    ax.stem(x, y, use_line_collection=True)

    # set some plot related information
    ax.set_ylim(y_lower_lim, y_upper_lim + .5)
    ax.set(xlabel=xlabel, ylabel=ylabel)
    #ax.set_xticklabels(x, rotation=90, ha='right')
    ax.set_xticklabels(x, rotation=90, ha='center')
    ax.set_title(title)
    ax.grid(True)
    if horizontal_threshold_line_value:
        ax.axhline(y=horizontal_threshold_line_value, color='r', linestyle='-')

    if print_stem_point_value is True:
        # label the points
        # zip joins x and y coordinates in pairs
        for x, y in zip(x, y):
            if y == 0 or y == 100:
                continue
            label = "{:.1f}".format(y)
            ax.annotate(label,  # this is the text
                        (x, y),  # this is the point to label
                        fontsize=7,
                        textcoords="offset points",  # how to position the text
                        xytext=(0, 10),  # distance from text to points (x,y)
                        ha='center')  # horizontal alignment can be left, right or center


def _return_data_frame_from_concordance_query(df=None, experiment_type=None, consequential_impacts_only=None,
                                              vaf_lower_limit=None,
                                              vaf_upper_limit=None,
                                              depth_threshold=None, variant_type=None,
                                              min_length_variant=None,
                                              max_length_variant=None):
    """
    Simple function to go over an asses whether there was concordance based on thresholds passed in
    :param df: data frame to use for filtering
    :param experiment_type: Filter based on experiment type.  See config, e..g intra-run or inter-run
    :param consequential_impacts_only: Boolean testing if we only looked at consequential impacts or all impacts
    :param vaf_lower_limit: VAF lower limit to look at, i.e. <= only plot values
    :param vaf_upper_limit: VAF lower limit to look at, i.e. >= only plot values
    :param depth_threshold: The Depth to plot too, ie. <= of this value
    :param variant_type: indel or snv
    """
    # at this point the data frame doesn't have actual events on whether a variant fell into a bin, that was produced by
    # the overall running of the program clinical_sample_reproducibility.py, and specifically in the
    # process_samples_for_concordance function, so here you just plot the bins.  THis is
    # why some of the values for threshold are both >= and <=, i.e. it's not binning just setting plot thresholds
    # depth only has one value, b/c its the upper limit of what to plot on the x-axis
    df_queried = df[(df['Experiment_type'] == experiment_type) &
                    (df['Consequential_impacts_only'] == consequential_impacts_only) &
                    (df['VAF_threshold'] >= vaf_lower_limit) &
                    (df['VAF_threshold'] <= vaf_upper_limit) &
                    (df['Depth_threshold'] <= depth_threshold) &
                    (df['Variant_type'] == variant_type) &
                    (df['Min_length_variant'] >= min_length_variant) &
                    (df['Max_length_variant'] <= max_length_variant)]

    return df_queried

