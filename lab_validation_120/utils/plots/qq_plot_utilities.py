import statsmodels.api as sm
import numpy as np


def plot_qq_plot(ax=None, sample1=None, sample2=None, x_axis_label=None, y_axis_label=None, value_plotted=None,
                 plot_legend=True, sample1_distribution_str=None, sample2_distribution_str=None,
                 fontsize=9):
    """
    A Q-Q plot is a scatterplot created by plotting two sets of quantiles against one another.
    If both sets of quantiles came from the same distribution, we should see the points forming a line that's roughly
    straight.
    :param ax: an instance of the class plt.Axes
    :param sample: Data to plot
    :param x_axis_label: x axis label
    :param x_axis_label: y axis label
    :param plot_legend: Plot the legend on the graph
    :param sample1_distribution_str: Just a string that will be used in the title
    :param sample2_distribution_str: Just a string that will be used in the title
    :param fontsize: Font size for the title
    """
    if isinstance(sample1, list):
        sample1 = np.array(sample1)
    if isinstance(sample2, list):
        sample2 = np.array(sample2)
    # plot out the qq and add a straight vertical line
    sm.qqplot_2samples(data1=sample1, data2=sample2, ax=ax, line='45', xlabel=x_axis_label, ylabel=y_axis_label)
    ax.set_title(f"Q-Q Plot sample1: {sample1_distribution_str} vs.\n sample2: {sample2_distribution_str}",
                 fontsize=fontsize)