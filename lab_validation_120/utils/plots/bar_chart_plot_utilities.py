import os

import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
from lab_validation_120.utils.htqc_utilites import get_columns_names_and_yaxis_values_2_plot_for_htqc_summary
from lab_validation_120.utils.pandas_dataframes_utilities import \
    read_dataset_and_get_pandas_df_from_key_value_each_line, get_column_types_summary_htqc, get_column_types_htqc, \
    read_dataset_and_get_pandas_df
from lab_validation_120.utils.plots.plot_utilities import get_plot_file_type, get_subplot_bounding_box_parameters, \
    global_rc_set, get_alternate_colors
from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame


def process_bar_chart_analysis_4_htqc_summary(config=None, key_to_config_usage=None, col_name_4_sample_names=None,
                                              figsize_width=None, sort_x_by_value=True,
                                              alternate_colors=False):
    """
    Print out the bar chart plots for the htqc summaries
    :param config: dictionary of values used for configuration
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What to name the new column in the data frame
    :param figsize_width: The width of the plot for HTQC metrics
    :param sort_x_by_value: Sort the x-axis values by the value of each for HTQC metrics, default is True
    :param alternate_colors: alternate the colors on the bar chart, default is False
    """
    images_dir = config[key_to_config_usage]['images_dir']
    # go over each group of DNA samples from a run: input_samples_list_of_dicts['id']
    # Then go over each sample in that group below derivative_dict['sample_id'])
    # e.g. (400ng, 200ng, 100ng, 50ng), input_samples_list_of_dicts contains all the data
    input_samples_dict = config[key_to_config_usage]['input_files']
    chart_title = None
    if 'chart_title' in input_samples_dict and input_samples_dict['chart_title']:
        chart_title = input_samples_dict['chart_title']
    # store a list of data frames for plotting
    list_df = []
    for sample_dict in input_samples_dict['data']:
        # open the file and get a df
        df = read_dataset_and_get_pandas_df(col_vals=get_column_types_htqc(), file_name=sample_dict['qc_htqc_file'])
        # create a new column that pulls from the two config values if they are both present, else the one
        if 'sample_type' in sample_dict and sample_dict['sample_type']:
            df[col_name_4_sample_names] = '-'.join((sample_dict['sample_type'], sample_dict['sample_id']))
        else:
            df[col_name_4_sample_names] = sample_dict['sample_id']
        list_df.append(df)
    #print(list_df)
    df = pd.concat(list_df)
    plot_bar_chart_4_htqc_summary(df=df, sample_name=input_samples_dict['group_id'],
                                  images_dir=images_dir, col_name_4_sample_names=col_name_4_sample_names,
                                  nrows=2, ncols=3, figsize_width=figsize_width, figsize_height=10,
                                  word_list_to_not_capitalize=["ABS", "BP", "GTE", "LTE", "DP"],
                                  chart_title=chart_title, sort_x_by_value=sort_x_by_value,
                                  alternate_colors=alternate_colors)
    return df


def print_htqc_summary_data(config=None, tabular_output_file=None, col_name_4_sample_names=None,
                            key_to_config_usage=None):
    """
    Function used to print out the HTQc summary of the data
    :param config: dictionary of values used for configuration
    :param tabular_output_file: Name of the file for the summary data
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What to name the new column in the data frame
    THIS CANNOT BE REFACTORED INTO process_bar_chart_analysis_4_htqc_summary()
    """
    # store a list of data frames for plotting
    input_samples_dict = config[key_to_config_usage]['input_files']
    list_df = []
    for sample_dict in input_samples_dict['data']:
        # open the file and get a df
        df = read_dataset_and_get_pandas_df_from_key_value_each_line(col_vals=get_column_types_summary_htqc(),
                                                                     file_name=sample_dict['qc_htqc_summary_file'],
                                                                     )
        # create a new column that pulls from the two config values if they are both present, else the one
        if 'sample_type' in sample_dict and sample_dict['sample_type']:
            df[col_name_4_sample_names] = '-'.join((sample_dict['sample_type'], sample_dict['sample_id']))
        else:
            df[col_name_4_sample_names] = sample_dict['sample_id']

        list_df.append(df)
    print_data_frame(df=pd.concat(list_df), tabular_output_file=tabular_output_file)


def plot_bar_chart_4_htqc_summary(df=None, sample_name=None, images_dir=None, col_name_4_sample_names=None,
                                  nrows=None, ncols=None, figsize_width=None, figsize_height=None,
                                  word_list_to_not_capitalize=None,
                                  chart_title=None, sort_x_by_value=True, alternate_colors=False):
    """
    Go through and print for example all the summary data found in a HTQC summary file
    :param df: The data frame for a set of samples, e.g. for all HTQC data
    :param sample_name: The sample name that was diluted down
    :param images_dir: Where to store the images
    :param col_name_4_sample_names: What's the column used for the sample names and printed on the x-axis
    :param nrows: # number of rows to print in the figure
    :param ncols: # number of cols to print in the figure
    :param figsize_width: width of the figure
    :param figsize_height: height of the figure
    :param word_list_to_not_capitalize: Simple list of words that the _capitalize_every_word_except_list will use
    :param chart_title:  The title for each fo the subplots
    :param figsize_width: The width of the plot for HTQC metrics
    :param sort_x_by_value: Sort the x-axis values by the value of each for HTQC metrics, default is True
    :param alternate_colors: alternate the colors on the bar chart, default is False
    """
    # set the size of the plots
    global_rc_set()
    plt.rcParams["figure.figsize"] = [figsize_width, figsize_height]
    # how many are we plotting
    fig, axes = plt.subplots(nrows, ncols)
    # Dictionary of column names (keys) and the y-axis labels (values)
    columns_names_and_yaxis_values_2_plot = get_columns_names_and_yaxis_values_2_plot_for_htqc_summary()
    # just get the column names for each metric
    # ['MEAN_ABS_COVERAGE', 'MEAN_FILTERED_COVERAGE', 'MEAN_COLLAPSED_COVERAGE', 'FRACTION_WITH_ZERO_DP',
    # 'FRACTION_LTE_LOWER_DP', 'FRACTION_GTE_UPPER_DP']
    column_names = list(columns_names_and_yaxis_values_2_plot.keys())
    num_plots = 0
    filename_order = "sorted_by_input_order"
    for row in axes:  # go over each row that was created
        # each column is an instance of the class plt.Axes
        for ax in row:  # a row will have columns in it based on the plt.subplots
            if len(column_names) > 0:  # possible that you need to get out b/c no more columns
                # get the data frames column name to be plotted
                column_name = column_names.pop(0)
            else:
                break

            # title for the plot
            title = column_name.replace("_", " ")
            capitalized_title = " ".join([_capitalize_every_word_except_list(word, word_list_to_not_capitalize)
                                          for word in title.split(" ")])
            # round the value based on the value, BP dont' need decimal rounding
            if columns_names_and_yaxis_values_2_plot[column_name] == 'BP':
                df[column_name] = df[column_name].round(decimals=0)
            else:
                df[column_name] = df[column_name].round(decimals=3)
            # sort x-axis by the values of each?
            sample_order = None
            if sort_x_by_value is True:
                # get the order of the names to print out in
                sample_order = \
                    list(df.groupby(by=[col_name_4_sample_names])[column_name].mean().sort_values(ascending=True).index)
                filename_order = "sorted_by_metric"

            ci = None  # only plot error bars for MEAN_* columns
            sig_digits = 3  # rounding for values on top of each bar
            height_multiplier = 1.005  # where to place the number on each bar vertically
            if column_name.startswith("MEAN"):
                ci = ('ci', 95)
                sig_digits = 1
                height_multiplier = 1.07

            # Create empty plot with blank marker containing the extra label
            #ax.plot([], [], ' ', label="|---| 95% CI over all intervals")


            # we can request to alternate the colors
            colors_2_use = get_alternate_colors(df=df, alternate_colors=alternate_colors)

            # actual plotting
            sns.barplot(x=col_name_4_sample_names, y=column_name, data=df, ax=ax, palette=colors_2_use, errorbar=ci,
                        errwidth=.5, capsize=.5, label=capitalized_title, order=sample_order)
            # print out the bar labels
            max_val = -10000
            for p in ax.patches:
                val = round(p.get_height(), sig_digits)
                if val > max_val:
                    max_val = val
                # the string to print, and then the x , y coordinate in a tuple
                ax.annotate(str(val),
                            (p.get_x() * .995, p.get_height() * height_multiplier))
            # needed to add more space for the values plotted on bars
            if column_name == 'MEAN_ABS_COVERAGE':
                ax.set_ylim(0, max_val + 600)
            elif column_name == 'MEAN_FILTERED_COVERAGE':
                ax.set_ylim(0, max_val + 100)
            elif column_name == 'MEAN_COLLAPSED_COVERAGE':
                ax.set_ylim(0, max_val + 100)

            # set values for x and y-axis
            ax.set(ylabel=columns_names_and_yaxis_values_2_plot[column_name], xlabel='Samples')

            #ax.legend(loc='upper left', frameon=True)

            # what labels to use for the x-axis
            if sample_order is not None:
                ax.set_xticklabels(sample_order, rotation=45, ha='right')
            else:
                ax.set_xticklabels(df[col_name_4_sample_names].unique().tolist(), rotation=45, ha='right')
            temp_chart_title = " ".join((chart_title, f"({column_name.replace('_', ' ')})"))
            ax.set_title(temp_chart_title)
            num_plots += 1

    # e.g. if there was 7 plots to create, and we created 8 via plt.subplots(nrows, ncols), then we should remove
    # one plot from the canvas
    num_to_delete = (nrows * ncols) - num_plots
    row_delete = nrows - 1
    for i in range(1, num_to_delete + 1):
        delete = i * -1
        fig.delaxes(ax=axes[row_delete, delete])

    # store the image
    if not os.path.exists(images_dir):
        os.makedirs(images_dir)

    #if chart_title:
    #    fig.suptitle(f'{chart_title}', fontsize="x-large")  # or plt.suptitle('Main title')

    # automatically add and adjust padding between subplots
    # only considers ticklabels, axis labels, and titles. Thus, other artists may be clipped and also may overlap.
    # So you can adjust the subplot geometry in the very tight_layout call as follows:
    fig.tight_layout(rect=get_subplot_bounding_box_parameters())

    sample_name = sample_name.replace(" ", "_")
    # keep _Summary in the filename
    plt.savefig(os.path.join(images_dir, sample_name + f"_barchart_htqc_metrics_all_samples_{filename_order}" +
                             get_plot_file_type()))
    plt.close(fig)  # have to close


def _capitalize_every_word_except_list(word, dont_capitalize):
    if word in dont_capitalize:
        return word
    return word.capitalize()