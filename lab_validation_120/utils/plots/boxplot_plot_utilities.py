import os
import sys

import seaborn as sns
from matplotlib import pyplot as plt

from lab_validation_120.utils.concordance_utilites import get_concordance_data, get_concordance_variables, \
    get_overall_sample_metrics_concordance_df
from lab_validation_120.utils.plots.plot_utilities import get_plot_file_type, get_consequential_str, global_rc_set, \
    get_mcc_threshold, get_alternate_colors
from lab_validation_120.utils.genomic_metadata_utilities import get_list_chromosomes
from lab_validation_120.utils.htqc_utilites import get_column_metric_names_for_htqc_intervals, \
    get_columns_names_and_yaxis_values_2_plot_for_htqc_summary
from lab_validation_120.utils.genomic_metadata_utilities import get_list_oncokb_genes
from lab_validation_120.utils.configs.lab_validation_config import get_oncokb_gene_list


def process_boxplots_from_reproducibility_output(config=None, key_to_search=None,
                                                 consequential_impacts_only=None,
                                                 equivalency_study=False):
    """
    Open the file in the config and produce boxplot analysis
    :param config: The dictionary from the config
    :param consequential_impacts_only: Boolean testing if we only looked at consequential impacts or all impacts
    :param key_to_search: What key to pull out of the config dictionary to get the values needed, e.g.:
    "clinical_sample_reproducibility"
    :param equivalency_study: Boolean on if this was an equivalency_study, if so we will drop all discordance
    in the df for REP1, since we are only looking at recall. i.e. discordance only count in Sample 2
    """

    # look at specific file from from clinical_sample_reproducibility.py, see config for the file
    files_to_boxplot = config[key_to_search]['files_for_boxplot']
    images_dir = config[key_to_search]['images_dir']
    for variant_type, file in files_to_boxplot.items():
        # only look at consequential impacts?  Both files are provided in the config, but which one is used is
        # determined here
        if consequential_impacts_only is True:
            file = file[0]
        else:
            file = file[1]
        df_overall_concordance = get_overall_sample_metrics_concordance_df(file=file)
        # we can drop REP1 = - if this was an equivalency_study
        if equivalency_study is True:
            df_overall_concordance.drop(df_overall_concordance.loc[df_overall_concordance['REP1'] == '-'].index,
                                        inplace=True)
        # process the overall all concordance metrics
        process_boxplots_overall_concordance_metrics(df=df_overall_concordance, variant_type=variant_type,
                                                     images_dir=images_dir,
                                                     consequential_impacts_only=consequential_impacts_only,
                                                     add_n_value_4_each_boxplot=True)

        for is_concordant in (True, False):
            process_boxplots_overall_concordance_metrics_bt_experiments(df=df_overall_concordance,
                                                                        variant_type=variant_type, images_dir=images_dir,
                                                                        is_concordant=is_concordant,
                                                                        experiment_type="inter_intra_run",
                                                                        consequential_impacts_only=consequential_impacts_only)
            process_boxplots_overall_sample_concordance_metrics(df=df_overall_concordance, variant_type=variant_type,
                                                                images_dir=images_dir,
                                                                is_concordant=is_concordant,
                                                                consequential_impacts_only=consequential_impacts_only)


def process_boxplots_overall_concordance_metrics(df=None, variant_type=None, images_dir=None,
                                                 consequential_impacts_only=None,
                                                 add_n_value_4_each_boxplot=False):
    """
    User for plotting overall concordance on the x-axis True vs False
    :param df: data frame to use for plotting
    :param variant_type: indel or snv
    :param images_dir: Where to print the image
    :param consequential_impacts_only: Boolean testing if we only looked at consequential impacts or all impacts
    :param add_n_value_4_each_boxplot:  Should we add in the number of observations for each boxplot
    """
    global_rc_set()
    plt.rcParams["figure.figsize"] = [10, 10]
    fig, ax = plt.subplots(2, 3)  # rows x cols

    # Draw these with both outliers and no-outliers
    for i, show_outliers in enumerate([False, True]):
        plot_boxplot_x_vs_y(ax=ax[i, 0], x="Is_concordant", y="Vaf_2_boxplot",
                            xlabel="Concordance", ylabel="VAF",
                            df=df, title="Variant Allele Frequency",
                            show_outliers=show_outliers, y_upper_lim=None,
                            add_n_value_4_each_boxplot=add_n_value_4_each_boxplot)
        plot_boxplot_x_vs_y(ax=ax[i, 1], x="Is_concordant", y="Depth_2_boxplot",
                            xlabel="Concordance", ylabel="Coverage Depth",
                            df=df, title="Coverage Depth",
                            show_outliers=show_outliers, y_upper_lim=None,  # used to be 1000
                            add_n_value_4_each_boxplot=add_n_value_4_each_boxplot)
        plot_boxplot_x_vs_y(ax=ax[i, 2], x="Is_concordant", y="Allele_count_2_boxplot",
                            xlabel="Concordance", ylabel="Allele Count",
                            df=df, title="Allele Count",
                            show_outliers=show_outliers, y_upper_lim=None,  # used to be 200
                            add_n_value_4_each_boxplot=add_n_value_4_each_boxplot)

    fig.tight_layout()
    plt.savefig(os.path.join(images_dir, "boxplot_concordance_metrics" +
                             f'_{variant_type}'
                             f'_consequential_impacts_only_{str(consequential_impacts_only)}' + get_plot_file_type()))
    plt.close(fig) # have to close


def process_boxplots_overall_concordance_metrics_bt_experiments(df=None, variant_type=None, images_dir=None,
                                                                is_concordant=None, experiment_type=None,
                                                                consequential_impacts_only=None):
    """
    Used for plotting between the two experiment types, e.g. inter vs intra run, or 400ng vs 100ng on the x-axis
    :param df: data frame to use for plotting
    :param variant_type: indel or snv
    :param images_dir: Where to print the image
    :param is_concordant: Boolean if the data is concordant or is discordant
    :param experiment_type:  Just a string that's added to the file name
    :param consequential_impacts_only: Boolean testing if we only looked at consequential impacts or all impacts
    """

    # call to get a string based on whether you are measuring consequential impacts or not (Bool)
    consequential_impacts_str = get_consequential_str(consequential_impacts_only)
    global_rc_set()
    plt.rcParams["figure.figsize"] = [10, 10]
    fig, ax = plt.subplots(2, 3)  # rows x cols

    # Set it None to display all rows in the data frame
    df_is_concordant = get_concordance_data(df=df, is_concordant=is_concordant)
    is_concordant_filename_str, xlabel = get_concordance_variables(is_concordant=is_concordant)

    # Draw these with both outliers and no-outliers
    for i, show_outliers in enumerate([False, True]):
        plot_boxplot_x_vs_y(ax=ax[i, 0], x="Experiment_type", y="Vaf_2_boxplot",
                            xlabel="Concordance", ylabel="VAF",
                            df=df_is_concordant, title=f"Variant Allele Frequency\n{consequential_impacts_str}",
                            show_outliers=show_outliers, y_upper_lim=None)
        plot_boxplot_x_vs_y(ax=ax[i, 1], x="Experiment_type", y="Depth_2_boxplot",
                            xlabel="Concordance", ylabel="Coverage Depth",
                            df=df_is_concordant, title=f"Coverage Depth\n{consequential_impacts_str}",
                            show_outliers=show_outliers, y_upper_lim=None)  # used to be 1000
        plot_boxplot_x_vs_y(ax=ax[i, 2], x="Experiment_type", y="Allele_count_2_boxplot",
                            xlabel="Concordance", ylabel="Allele Count",
                            df=df_is_concordant, title=f"Allele Count\n{consequential_impacts_str}",
                            show_outliers=show_outliers, y_upper_lim=None)  # used to be 200
    fig.tight_layout()
    plt.savefig(os.path.join(images_dir, experiment_type +
                             f'_boxplot_{variant_type}_{is_concordant_filename_str}'
                             f'_consequential_impacts_only_{str(consequential_impacts_only)}' +
                             get_plot_file_type()))
    plt.close(fig)  # have to close


def process_boxplots_overall_sample_concordance_metrics(df=None, variant_type=None,
                                                        images_dir=None, is_concordant=None,
                                                        consequential_impacts_only=None):
    """
    :param df: data frame to use for plotting
    :param variant_type: indel or snv
    :param images_dir: Where to print the image
    :param is_concordant: Boolean if the data is concordant or is discordant
    :param consequential_impacts_only: Boolean testing if we only looked at consequential impacts or all impacts
    """

    # call to get a string based on whether you are measuring consequential impacts or not (Bool)
    consequential_impacts_str = get_consequential_str(consequential_impacts_only)
    global_rc_set()
    plt.rcParams["figure.figsize"] = [10, 10]
    fig, ax = plt.subplots(2, 3)  # rows x cols

    # Set it None to display all rows in the data frame
    df_is_concordant = get_concordance_data(df=df, is_concordant=is_concordant)
    is_concordant_filename_str, xlabel = get_concordance_variables(is_concordant=is_concordant)

    # Draw these with both outliers and no-outliers
    for i, show_outliers in enumerate([False, True]):
        plot_boxplot_x_vs_y(ax=ax[i, 0], x="Sample_ID", y="Vaf_2_boxplot",
                            xlabel=f"{xlabel} Sample Calls", ylabel="VAF",
                            df=df_is_concordant, title=f"Variant Allele Frequency\n{consequential_impacts_str}",
                            tilted_x_names=45, show_outliers=show_outliers, y_upper_lim=None)
        plot_boxplot_x_vs_y(ax=ax[i, 1], x="Sample_ID", y="Depth_2_boxplot",
                            xlabel=f"{xlabel} Sample Calls", ylabel="Coverage Depth",
                            df=df_is_concordant, title=f"Coverage Depth\n{consequential_impacts_str}",
                            tilted_x_names=45, show_outliers=show_outliers, y_upper_lim=None)  # used to be 1000
        plot_boxplot_x_vs_y(ax=ax[i, 2], x="Sample_ID", y="Allele_count_2_boxplot",
                            xlabel=f"{xlabel} Sample Calls", ylabel="Allele Count",
                            df=df_is_concordant, title=f"Allele Count\n{consequential_impacts_str}",
                            tilted_x_names=45, show_outliers=show_outliers, y_upper_lim=None)  # used to 200

    fig.tight_layout()
    plt.savefig(os.path.join(images_dir, "sample_boxplot" +
                             f'_{variant_type}_{is_concordant_filename_str}'
                             f'_consequential_impacts_only_{str(consequential_impacts_only)}' +
                             get_plot_file_type()))
    plt.close(fig)  # have to close


def chart_box_plot_2_columns(ax=None, key1=None, key2=None, xlabel=None, ylabel=None, df=None, title=None,
                             tilted_x_names=None, show_outliers=False):
    """
    :param ax: an instance of the class plt.Axes
    :param key1: column in the data frame to be plotted
    :param key2: column in the data frame to be plotted
    :param xlabel: Xlabel of the chart
    :param ylabel: Y label of the chart
    :param df: data frame to use for plotting
    :param title: Title for the plot
    :param tilted_x_names: Boolean to rotate the text by 45 degrees
    :param show_outliers: Boolean to show outliers or not
    """
    # confidence interval for the median values displayed by a box plot: True as the value for the notch attribute
    sns.boxplot(data=df[[key1, key2]], ax=ax, notch=True, showfliers=show_outliers)
    ax.set(xlabel=xlabel, ylabel=ylabel)
    ax.grid(True)
    if show_outliers is False:
        title = " ".join((title, "\n(no outliers shown)"))
    if tilted_x_names is True:
        ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
    ax.set_title(title)


def plot_boxplot_x_vs_y(ax=None, x=None, y=None, xlabel=None, ylabel=None, df=None, title=None,
                        tilted_x_names=None, show_outliers=False, swarmplot=False, order_x_axis=None,
                        notch_box_plot=True, horizontal_threshold_line_value=None, y_upper_lim=None,
                        alternate_colors=False, add_n_value_4_each_boxplot=False):
    """
    :param ax: an instance of the class plt.Axes
    :param x: column in the data frame to be plotted
    :param y: column in the data frame to be plotted
    :param xlabel: Xlabel of the chart
    :param ylabel: Y label of the chart
    :param df: data frame to use for plotting
    :param title: Title for the plot
    :param tilted_x_names: Int to rotate the text by, e.g. 45 degrees
    :param show_outliers: Boolean to show outliers or not
    :param swarmplot: Add a swarmplot to the barchorts
    :param df: data frame to be used for box-plotting
    :param notch_box_plot: Add a notch to the box indicating a confidence interval for the median
    :param y_upper_lim: Upper limit on the y-axis to plot
    :param alternate_colors: alternate the colors on the bar chart, default is False
    :param add_n_value_4_each_boxplot:  Should we add in the number of observations for each boxplot
    """
    if show_outliers is False and swarmplot is True:
        print("Using swarmplot, and 'show_outliers' was set to False, so overriding and will show outliers",
              file=sys.stderr)
        show_outliers = True
    # we can request to alternate the colors
    colors_2_use = get_alternate_colors(df=df, alternate_colors=alternate_colors)

    # confidence interval for the median values displayed by a box plot: True as the value for the notch attribute
    if alternate_colors is True:
        sns.boxplot(x=x, y=y, data=df, ax=ax, notch=notch_box_plot, showfliers=show_outliers, order=order_x_axis,
                    fliersize=1, palette=colors_2_use)
    else:
        sns.boxplot(x=x, y=y, data=df, ax=ax, notch=notch_box_plot, showfliers=show_outliers, order=order_x_axis,
                    fliersize=1)
    ax.set(xlabel=xlabel.replace("_", " "), ylabel=ylabel.replace("_", " "))
    # TODO Fix this to pass in as parameter after the talk
    ax.set_ylim(0, y_upper_lim)
    ax.grid(True)
    # add a swarmplot to the boxplots
    if swarmplot is True:
        # add swarmplot
        sns.swarmplot(x=x, y=y,
                      data=df,
                      color='black',
                      alpha=0.35,
                      ax=ax, size=1)  # size >3 or more caused #  UserWarning: 10.0% of the points cannot be placed;
    # outliers can squish the overall plot, so we can remove them
    if show_outliers is False:
        title = " ".join((title.replace("_", " "), "\n(no outliers shown)"))
    if tilted_x_names is not None:
        ax.set_xticklabels(ax.get_xticklabels(), rotation=tilted_x_names, ha='right')
    if horizontal_threshold_line_value is not None:
        ax.axhline(y=horizontal_threshold_line_value, color='r', linestyle='-')
    # how many points shown?
    title = title + f" N = {len(df)}"
    ax.set_title(title)

    if add_n_value_4_each_boxplot is True:
        # Calculate number of obs per group & median to position labels
        medians = df.groupby([x])[y].median().values
        nobs = df[x].value_counts().values
        nobs = [str(x) for x in nobs.tolist()]
        nobs = ["N = " + i for i in reversed(nobs)]

        # Add it to the plot
        pos = range(len(nobs))
        for tick, label in zip(pos, ax.get_xticklabels()):
            ax.text(pos[tick],
                    medians[tick] + 30,
                    nobs[tick],
                    horizontalalignment='center',
                    size='medium',
                    color='black',
                    weight='semibold')



def process_boxplot_analysis_for_htqc_intervals_by_chromosome(df=None, config=None, key_to_config_usage=None,
                                                              col_name_4_sample_names=None):
    """
    Print out the bar boxplot for the htqc
    :param df: data frame to use for plotting
    :param config: dictionary of values used for configuration
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What to name the new column in the data frame
    """
    for sample, df_by_sample in df.groupby(col_name_4_sample_names):
        for metric in get_column_metric_names_for_htqc_intervals():
            # go through and get the df ready for plotting
            prepare_boxplot_subplots_by_metric_for_htqc_intervals_by_chromosome(
                df=df_by_sample,
                column_to_use_for_calculation=metric,
                sample_name=sample,
                images_dir=config[key_to_config_usage]['images_dir'],
                group_name=config[key_to_config_usage]['input_files']['group_id'])


def prepare_boxplot_subplots_by_metric_for_htqc_intervals_by_sample(df=None, images_dir=None,
                                                                    col_name_4_sample_names=None,
                                                                    config=None, key_to_config_usage=None,
                                                                    figsize_width=None,
                                                                    htqc_boxchart_sort_x_by_value=None,
                                                                    alternate_colors=False):
    """
    Go over an boxplot the htqc metrics by samples
    :param df: data frame to use for plotting
    :param images_dir: Where to print the image
    :param col_name_4_sample_names: What to name the new column in the data frame
    :param config: dictionary of values used for configuration
    :param figsize_width: Width of the size of the image
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param htqc_boxchath_sort_x_by_value: Sort the x-axis values by the value of each for HTQC metrics, default is True
    :param alternate_colors: alternate the colors on the bar chart, default is False
    """
    input_samples_dict = config[key_to_config_usage]['input_files']
    chart_title = None
    if 'chart_title' in input_samples_dict and input_samples_dict['chart_title']:
        chart_title = input_samples_dict['chart_title']
    global_rc_set()
    plt.rcParams['figure.figsize'] = (figsize_width, 10)
    fig, ax = plt.subplots(2, 3)  # rows x cols
    columns_names_and_yaxis_values_2_plot = get_columns_names_and_yaxis_values_2_plot_for_htqc_summary()
    # just get the column names for each metric
    # ['MEAN_ABS_COVERAGE', 'MEAN_FILTERED_COVERAGE', 'MEAN_COLLAPSED_COVERAGE', 'FRACTION_WITH_ZERO_DP',
    # 'FRACTION_LTE_LOWER_DP', 'FRACTION_GTE_UPPER_DP']
    column_names = list(columns_names_and_yaxis_values_2_plot.keys())
    filename_order = "sorted_by_input_order"
    for row, show_outliers in enumerate([False, True]):
        for col, column_to_use_for_calculation in enumerate(column_names):
            if not column_to_use_for_calculation.startswith("MEAN"):
                continue
            order_x_axis = None
            # create a temp dictionary sorted by median of the column_to_use_for_calculation, and then use list
            # comprehension to assign to order_x_axis
            if htqc_boxchart_sort_x_by_value is True:
                order_x_axis = [
                    k for k, v in sorted(
                        df.groupby([col_name_4_sample_names])[column_to_use_for_calculation].median().to_dict().items(),
                        key=lambda x: x[1])]
                filename_order = "sorted_by_metric"

            plot_boxplot_x_vs_y(ax=ax[row, col],
                                x=col_name_4_sample_names,
                                y=column_to_use_for_calculation,
                                xlabel=col_name_4_sample_names,
                                ylabel=column_to_use_for_calculation,
                                order_x_axis=order_x_axis,
                                df=df,
                                title=f"{chart_title}",
                                tilted_x_names=45,
                                show_outliers=show_outliers,
                                swarmplot=False, notch_box_plot=True,
                                horizontal_threshold_line_value=_get_horizontal_threshold_line_value(
                                column_to_use_for_calculation=column_to_use_for_calculation),
                                y_upper_lim=None,
                                alternate_colors=alternate_colors)

    # final charting
    fig.tight_layout()
    # if there was group id in the config add it here to the name of the file
    group_name = ""
    if 'group_id' in input_samples_dict and input_samples_dict['group_id']:
        group_name = input_samples_dict['group_id'].replace(" ", "_")
    if group_name:
        plt.savefig(os.path.join(images_dir, f"{group_name}_boxplot_htqc_metrics_all_samples_{filename_order}" +
                                 get_plot_file_type()))
    else:
        plt.savefig(os.path.join(images_dir, f"boxplot_htqc_metrics_all_samples_{filename_order}" +
                                 get_plot_file_type()))
    plt.close(fig)  # have to close



def prepare_boxplot_subplots_by_metric_for_htqc_intervals_by_chromosome(df=None, column_to_use_for_calculation=None,
                                                                        sample_name=None,
                                                                        images_dir=None, group_name=None):
    """
    Go over an boxplot the htqc metrics by sample and chromosome
    :param df: data frame to use for plotting
    :param column_to_use_for_calculation: What column in the df to use for plotting
    :param sample_name: Name of the sample being plotted, e.g. hd799
    :param images_dir: Where to print the image
    :param group_name: The group name of the samples (used in the final image name)

    """

    chromosomes = get_list_chromosomes()  # get a list of strings for the chromosomes
    global_rc_set()
    plt.rcParams['figure.figsize'] = (60.0, 30.0)
    cols = 6
    rows = 4
    fig, ax = plt.subplots(rows, cols)  # rows x cols
    chromosome_index = 0
    for row in range(0, rows):
        for col in range(0, cols):
            # we have 24 spots, but only use 23 chr, since Y is not being analyzed
            if chromosome_index < len(chromosomes):
                chromosome = chromosomes[chromosome_index]
            else:
                break
            df_chrom = df[df['CHROM'] == chromosome]
            if df_chrom.empty:
                continue
            df_chrom = df_chrom.sort_values("GENE")
            y_upper_lim = df_chrom[column_to_use_for_calculation].max()
            chromosome_index += 1
            plot_boxplot_x_vs_y(ax=ax[row, col], x="GENE", y=column_to_use_for_calculation,
                                xlabel="Gene", ylabel=column_to_use_for_calculation, df=df_chrom,
                                title=f"Genes on Chromosome {chromosome} ({column_to_use_for_calculation})",
                                tilted_x_names=90, show_outliers=True, swarmplot=True, notch_box_plot=False,
                                horizontal_threshold_line_value=_get_horizontal_threshold_line_value(
                                    column_to_use_for_calculation=column_to_use_for_calculation),
                                y_upper_lim=y_upper_lim)

    # final charting
    fig.tight_layout()
    plt.savefig(os.path.join(images_dir, f"{group_name}_boxplot_{column_to_use_for_calculation}_{sample_name}" +
                             get_plot_file_type()))
    plt.close(fig)  # have to close


def process_boxplot_analysis_for_htqc_intervals_for_gene_list(df=None, config=None, key_to_config_usage=None,
                                                              col_name_4_sample_names=None,
                                                              gene_list=None,
                                                              gene_list_name=None,
                                                              only_plot_mcc=False):
    """
    Go over an boxplot the htqc metrics any list of gene
    :param df: data frame to use for plotting
    :param config: dictionary of values used for configuration
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What column name to group by
    :param gene_list: List of genes to plot
    :param gene_list_name: Name of the list of genes
    :param only_plot_mcc: Boolean on whether to only print out MCC
    """

    # go over all the metrics to plot
    for metric in get_column_metric_names_for_htqc_intervals():
        # only print out MCC if told to do so
        if only_plot_mcc is True and metric != 'MEAN_COLLAPSED_COVERAGE':
            continue
        helper_process_boxplot_analysis_for_htqc_intervals_for_gene_list(
            df=df, config=config,
            key_to_config_usage=key_to_config_usage,
            col_name_4_sample_names=col_name_4_sample_names,
            metric=metric,
            group_name=config[key_to_config_usage]['input_files']['group_id'],
            gene_list=gene_list,
            gene_list_name=gene_list_name
        )


def helper_process_boxplot_analysis_for_htqc_intervals_for_gene_list(df=None, config=None, key_to_config_usage=None,
                                                                      col_name_4_sample_names=None, metric=None,
                                                                      group_name=None,
                                                                      gene_list=None,
                                                                      gene_list_name=None):
    """
    Go over data frame grouped by sample name, and then each metric from get_column_metric_names_for_htqc_intervals()
    :param df: data frame to use for plotting
    :param config: dictionary of values used for configuration
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What column name to group by
    :param gene_list_name: string of the Gene list to plot to put in the title and image file name
    :param group_name: The group name of the samples (used in the final image name)
    :param gene_list: List of genes to plot
    :param gene_list_name: Name of the list of genes
    """

    for sample, df_by_sample in df.groupby(col_name_4_sample_names):
        global_rc_set()
        plt.rcParams['figure.figsize'] = (10.0, 10.0)
        fig, ax = plt.subplots(1, 1)  # rows x cols
        df_genes = \
            df_by_sample[df_by_sample['GENE'].isin(gene_list)]
        # go through and get the df ready for plotting
        prepare_boxplot_subplots_by_metric_for_htqc_intervals_and_gene_list(df=df_genes,
                                                                            column_to_use_for_calculation=metric,
                                                                            gene_list_name=gene_list_name,
                                                                            ax=ax)
        # final charting
        fig.tight_layout()
        gene_list_name = gene_list_name.replace(" ", "_")
        plt.savefig(
            os.path.join(config[key_to_config_usage]['images_dir'],
                         f"{group_name}_boxplot_{gene_list_name}_{metric}_{sample}" +
                         get_plot_file_type()))
        plt.close(fig)  # have to close


def process_boxplot_analysis_for_htqc_intervals_for_oncokb_genes(df=None, config=None, key_to_config_usage=None,
                                                                 col_name_4_sample_names=None):

    """
    Go over an boxplot the htqc metrics for the oncokb genes, this function does the looping over the different list of
    files from oncokb, e.g. oncokb_L1.  see: get_oncokb_gene_list()
    :param df: data frame to use for plotting
    :param config: dictionary of values used for configuration
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What column name to group by
    """

    # go over all the metrics to plot
    for metric in get_column_metric_names_for_htqc_intervals():
        helper_process_boxplot_analysis_for_htqc_intervals_for_oncokb_genes(
            df=df, config=config,
            key_to_config_usage=key_to_config_usage,
            col_name_4_sample_names=col_name_4_sample_names,
            metric=metric,
            group_name=config[key_to_config_usage]['input_files']['group_id'])


def helper_process_boxplot_analysis_for_htqc_intervals_for_oncokb_genes(df=None, config=None, key_to_config_usage=None,
                                                                        col_name_4_sample_names=None, metric=None,
                                                                        group_name=None):
    """
    Go over data frame grouped by sample name, and then each metric from get_column_metric_names_for_htqc_intervals()
    :param df: data frame to use for plotting
    :param config: dictionary of values used for configuration
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What column name to group by
    :param gene_list_name: string of the Gene list to plot to put in the title and image file name
    :param group_name: The group name of the samples (used in the final image name)
    """
    for sample, df_by_sample in df.groupby(col_name_4_sample_names):
        global_rc_set()
        plt.rcParams['figure.figsize'] = (20.0, 10.0)
        fig, ax = plt.subplots(2, 2)  # rows x cols
        row_col = [(0, 0), (0, 1), (1, 0), (1, 1)]  # what ax subplots to plot
        for i, gene_list_name in enumerate(get_oncokb_gene_list().keys()):
            df_oncokb_l1_gene = \
                df_by_sample[df_by_sample['GENE'].isin(get_list_oncokb_genes(
                    file=get_oncokb_gene_list()[gene_list_name]
                ))]
            # go through and get the df ready for plotting
            prepare_boxplot_subplots_by_metric_for_htqc_intervals_and_gene_list(df=df_oncokb_l1_gene,
                                                                                column_to_use_for_calculation=metric,
                                                                                gene_list_name=gene_list_name,
                                                                                ax=ax[row_col[i]])
        # final charting
        fig.tight_layout()
        plt.savefig(
            os.path.join(config[key_to_config_usage]['images_dir'], f"{group_name}_boxplot_oncokb_levels_{metric}_{sample}" +
                         get_plot_file_type()))
        plt.close(fig)  # have to close


def prepare_boxplot_subplots_by_metric_for_htqc_intervals_and_gene_list(df=None, column_to_use_for_calculation=None,
                                                                        ax=None,
                                                                        gene_list_name=None):
    """
    Create the subplots for plotting, and actual calling of plot_boxplot_x_vs_y
    :param df: data frame to use for plotting
    :param column_to_use_for_calculation: What column in the df to use for plotting
    :param ax: an instance of the class plt.Axes
    :param gene_list_name: string of the Gene list to plot to put in the title and image file name
    """

    df = df.sort_values("GENE")
    y_upper_lim = df[column_to_use_for_calculation].max()
    plot_boxplot_x_vs_y(ax=ax, x="GENE", y=column_to_use_for_calculation,
                        xlabel="Gene", ylabel=column_to_use_for_calculation, df=df,
                        title=f"Genes in {gene_list_name} ({column_to_use_for_calculation})",
                        tilted_x_names=90, show_outliers=True, swarmplot=True,
                        notch_box_plot=False,
                        horizontal_threshold_line_value=_get_horizontal_threshold_line_value(
                        column_to_use_for_calculation=column_to_use_for_calculation),
                        y_upper_lim=y_upper_lim)


def _get_horizontal_threshold_line_value(column_to_use_for_calculation=None):
    horizontal_threshold_line_value = None
    if column_to_use_for_calculation == "MEAN_COLLAPSED_COVERAGE":
        horizontal_threshold_line_value = get_mcc_threshold()
    return horizontal_threshold_line_value
