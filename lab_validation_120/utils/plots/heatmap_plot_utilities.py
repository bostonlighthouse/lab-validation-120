import os
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

from lab_validation_120.utils.genomic_metadata_utilities import get_list_chromosomes
from lab_validation_120.utils.htqc_utilites import get_overall_gene_normalized_htqc_metric_df
from lab_validation_120.utils.plots.plot_utilities import get_plot_file_type, global_rc_set
from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame


def process_gene_heatmap_analysis_for_htqc_intervals(df=None, config=None, key_to_config_usage=None,
                                                     col_name_4_sample_names=None,
                                                     list_of_heatmap_values=None,
                                                     sequencing_run=None,
                                                     heatmap_df_filename=None):
    """
    Print out the bar heatmap for the htqc
    :param config: dictionary of values used for configuration
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What to name the new column in the data frame
    :param list_of_heatmap_values: List of tuples that contain the various HTQC metrics
            # metric: e.g. MEAN_TARGET_COVERAGE
            # vmax is how high to show results, i.e. 800 = 800 of the target
                # vmax here is the highest number on the heatmap
            # num_segments_on_cbar is the number of segments to break the heatmaps, e.g.
            # vmax and num_segments_on_cbar can be None
    :param sequencing_run: what Run was this from?  Could be a batch number as well
    :param heatmap_df_filename: Print out the gene heatmap data frame for analysis
    """
    for metric, vmax, num_segments_on_cbar in list_of_heatmap_values:
        # go through and get the df ready for plotting
        prepare_gene_heatmap_subplots_by_metric_for_htqc(df=df, column_to_use_for_calculation=metric,
                                                         col_name_4_sample_names=col_name_4_sample_names,
                                                         images_dir=config[key_to_config_usage]['images_dir'],
                                                         vmax=vmax, num_segments_on_cbar=num_segments_on_cbar,
                                                         sequencing_run=sequencing_run,
                                                         heatmap_df_filename=heatmap_df_filename,
                                                         group_name=
                                                         config[key_to_config_usage]['input_files']['group_id'])


def prepare_gene_heatmap_subplots_by_metric_for_htqc(df=None, column_to_use_for_calculation=None,
                                                     col_name_4_sample_names=None,
                                                     images_dir=None, vmax=None, num_segments_on_cbar=None,
                                                     sequencing_run=None,
                                                     heatmap_df_filename=None,
                                                     group_name=None):
    """
    :param df: data frame to use for plotting,
    :param column_to_use_for_calculation:  What column (metric) will be used, e.g. MCC
    :param col_name_4_sample_names: What to name the new column in the data frame
    :param images_dir: Where to store the images
    :param vmax: vmax is how high to show results, i.e. 800 = 800 of the target
    :param sequencing_run: what Run was this from?  Could be a batch number as well
    :param num_segments_on_cbar: is the number of segments to break the heatmaps, e.g.
    :param sequencing_run: If this was passed in, it just notes the sequencing run.  This is here b/c sometimes the
    calling script will process multiple runs, and heatmaps are high-level and take all samples on a run, and are not
    named individually by samples like some other plotting
    :param heatmap_df_filename: Print out the gene heatmap data frame for analysis
    """

    # Two new keys for the new data frame that will be created
    mean_metric_total_bp_key = '_'.join((column_to_use_for_calculation, 'TOTAL_BP'))
    overall_gene_metric_key = f'OVERALL_GENE_{column_to_use_for_calculation}_{column_to_use_for_calculation}'
    # get the overall metric
    df_overall_gene_metric = \
        get_overall_gene_normalized_htqc_metric_df(df=df,
                                                   mean_metric_total_bp_key=mean_metric_total_bp_key,
                                                   column_to_use_for_calculation=column_to_use_for_calculation,
                                                   overall_gene_metric_key=overall_gene_metric_key,
                                                   col_name_4_sample_names=col_name_4_sample_names)

    # TODO Create an output for this file, and store with the images
    if heatmap_df_filename:
        print_data_frame(df=df_overall_gene_metric,
                         tabular_output_file=f'{column_to_use_for_calculation}_{heatmap_df_filename}')

    chromosomes = get_list_chromosomes()  # get a list of strings for the chromosomes
    plt.rcParams['figure.figsize'] = (120.0, 30.0)
    global_rc_set()
    cols = 6
    rows = 4
    fig, ax = plt.subplots(rows, cols)  # rows x cols
    chromosome_index = 0

    if num_segments_on_cbar is None:
        cmap = sns.diverging_palette(240, 10) # coloring for the heamap
    else:
        cmap = sns.diverging_palette(240, 10, n=num_segments_on_cbar) # coloring for the heamap

    chart_title_addition = f'({column_to_use_for_calculation} Normalized to Gene)'
    for row in range(0, rows):
        for col in range(0, cols):
            # we have 24 spots, but only use 23 chr, since Y is not being analyzed
            if chromosome_index < len(chromosomes):
                chromosome = chromosomes[chromosome_index]
            else:
                break
            df_chrom = df_overall_gene_metric[df_overall_gene_metric['CHROM'] == chromosome]
            chromosome_index += 1
            plot_heatmap(ax=ax[row, col], df=df_chrom, key_for_values_in_df=overall_gene_metric_key,
                         columns_along_x_axis=col_name_4_sample_names,
                         columns_along_y_axis="GENE",
                         tilted_x_names=True,
                         title=f"Genes on Chromosome {chromosome} {chart_title_addition}",
                         vmin=0, vmax=vmax, cbar=True, cmap=cmap)
    # final charting
    fig.tight_layout()
    if sequencing_run is not None:
        plt.savefig(os.path.join(images_dir, "heatmap" + f'_{sequencing_run}_{column_to_use_for_calculation}' +
                                 get_plot_file_type()))
        # if both a sequencing run was passed in, and print_heatmap_df_filename is True then print out these metrics
        # was was printed b/c the lab wanted to look at this data
        if heatmap_df_filename is not None:
            print_data_frame(df=df_overall_gene_metric,
                             tabular_output_file="_".join((sequencing_run, column_to_use_for_calculation + ".txt")))

    else:
        plt.savefig(os.path.join(images_dir, '_'.join((f"{group_name}", "heatmap", f'{column_to_use_for_calculation}'))
                                 + get_plot_file_type()))
    plt.close(fig)  # have to close


def plot_heatmap(ax=None, df=None, key_for_values_in_df=None, columns_along_x_axis=None, columns_along_y_axis=None,
                 tilted_x_names=None, title=None, vmin=None, vmax=None, cbar=True, cmap=None):
    """
    :param ax: an instance of the class plt.Axes
    :param df: data frame to use for plotting
    :param key_for_values_in_df: The key that we created (This is a new normalized gene), will be plotted on the heatmap
    :param columns_along_x_axis: The column name in the df to place on the x-axis
    :param columns_along_y_axis: The column name in the df to place on the y-axis
    :param tilted_x_names: Boolean to rotate the text by 90 degrees
    :param title: Title for each individual plot
    :param vmin: Values to anchor the colormap, otherwise they are inferred from the data and other keyword arguments.
    :param vmax: Values to anchor the colormap, otherwise they are inferred from the data and other keyword arguments.
    :param cbar: Whether to draw a colorbar.
    :param cmap: colormap object that can map continuous values to colors, depending on the value of
    """
    df2 = pd.pivot_table(df, values=key_for_values_in_df, index=[columns_along_y_axis], columns=columns_along_x_axis,
                         fill_value=0)
    # font size needs to be small here b/c there 24 heatmaps we are plotting
    sns.heatmap(df2, annot=True, fmt="d", ax=ax, annot_kws={"size": 5}, vmin=vmin, vmax=vmax, cbar=cbar, cmap=cmap)
    ax.set_title(title)
    if tilted_x_names is True:
        ax.set_xticklabels(ax.get_xticklabels(), rotation=90, ha='right')

