from urllib import request
from urllib.error import URLError, HTTPError
import json
from os import path
import sys
import time


def get_annotation_from_dbsnp_id(rs_number=None, download_data=False):
    """
    :param rs_number: Just a string containing the rs number
    :param download_data: Boolean on whether to force a new download of the data
    :return snp_key, which is the rs_number w/o the 'rs' portion and the eutils de-serialized JSON
    """

    json_file_name = 'scratch/json/dbsnp/' + '.'.join((rs_number, 'json'))
    snp_id_temp = rs_number.replace('rs', '')
    eutils_json = None
    if path.exists(json_file_name) and download_data is False:
        #print("Opening file", file=sys.stderr)
        with open(json_file_name) as infh:
            eutils_json = json.load(infh)
    else:
        eutils_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?retmode=json&db=snp&id=" + snp_id_temp
        print(f"Downloading file: {eutils_url}", file=sys.stderr)
        try:
            f = request.urlopen(eutils_url)
        except HTTPError as e:
            raise('HTTPError code: ', e.code)
        except URLError as e:
            raise('URLError: ', e.reason)
        except OSError as e:
            raise('OSError: ', e)

        content = f.read()
        eutils_json = json.loads(content.decode("utf-8"))
        # save for later
        with open(json_file_name, 'w') as json_fh:
            print(json.dumps(eutils_json, indent=4), file=json_fh)
        time.sleep(1)

    if eutils_json and 'error' in eutils_json['result'][str(snp_id_temp)]:
        eutils_json = None

    return snp_id_temp, eutils_json


if __name__ == "__main__":
    snp_id, eutils = get_annotation_from_dbsnp_id(rs_number='rs560681')
    if eutils:
        snp = eutils["result"][snp_id]
        gene = snp["genes"][0]
        snp_docsum_list = snp["docsum"].split(";")
        print(eutils)
        print(gene['name'])

