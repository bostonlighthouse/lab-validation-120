""" Pandas dataframe utility package for the lab_validation_120 project """
import io
import re
import sys

import pandas as pd
# set these globally for nice printing of data frames in this project
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 2000)


class PandasColsUse(Exception):
    """Exception raised for errors when Pandas columns to not match up

    Attributes:
        file -- The file that was attempted to be opened in a pandas dataframe
        err - the error being thrown
        message - explanation of the error
    """

    def __init__(self, file, err=None, message=None):
        self.file = file
        self.err = err
        if message is None:
            self.message = f"ERROR:\n\n{self.err}\n\n**Issue with file: {self.file}\n\nOpening the correct file type?\n"
        else:
            self.message = message
        super().__init__(self.message)

class HorizonFile(Exception):
    """Exception raised when I forget to remove the quotes around the 'horizon_file' in the config

    Attributes:
        err - the error being thrown
        message - explanation of the error
    """

    def __init__(self, err=None, message=None, file_name=None):
        self.err = err
        self.file_name = file_name
        if message is None:
            self.message = "\n\nDid you update the data structure for concordance_files in the config module:\n" \
                           "\t'horizon_file': 'horizon_file'\n" \
                           "No quotes around the second horizon_file in the config module ....\n" \
                           f"Or missing file name {self.file_name}"
        else:
            self.message = message
        super().__init__(self.message)


def get_columns_from_vv_and_graphql_endpoints():
    """
    :return: columns to use for the Test endpoint script
    """

    col_vals = {
        'batch': str,
        'cid': str,
        'vv_url': str,
        'FRACTION_lower_than_UPPER_DP': str,
        'Fraction_GTE_Upper_DP': str,
        'MEAN_COLLAPSED_COVERAGE': float,
        'num_variants': int,
        'ID_1': str,
        'PrimarySite': str,
        'PrimarySiteDiagnosis': str,
        'TumorPercentage': str,
        'Metastatic': str,
        'ID_2': str,
        'Dateofbirth': str,
        'Firstname': str,
        'Middlename': str,
        'Lastname': str,
        'Gender': str,
        'InternalProviderNumber': str,
        'TestCategory': str,
        'ID_3': str,
        'Offset': str,
        'CreationDate': str,
        'ModificationDate': str,
        'PageNotFound': str,
        'URL': str,
        'Patient_ID': str,

    }
    return col_vals


def get_columns_from_cider_gene_cnv_file():
    """Open a gene cnv file from cider"""

    col_vals = {
        "chr": str,
        "start": int,
        "end": int,
        "gene": str,
        "log2cnr": float,
        "pvalue": float,
        "stouffer": float,
        "sd": float
    }
    return col_vals

def get_columns_from_cider_cnv_file():
    """Open a  cnv file from cider"""

    col_vals = {
        "chr": str,
        "start": int,
        "end": int,
        "gene_symbol": str,
        "exon": str,
        "pos": str,
        "log2cnr": float,
        "pvalue": float,
    }
    return col_vals

def get_columns_from_genosity_variants_summary_tumor_comparison_tsv():
    """Open the Genosity variants.summary.tumor.comparison.tsv file"""
    col_vals = {
        "OrderID": str,
        "SampleName": str,
        "Genome": str,
        "Chrom": str,
        "Pos": int,
        "Stop": str,
        "Ref": str,
        "Alt": str,
        "FDP": int,
        "FRD": int,   # should be an int
        "FRF": float,  # # should be an int
        "FAD": int,  # # should be an int
        "FAF": float,
        #"Batch_MeanFAF": str,   # appears to be all '.'
        #"Batch_PercentofSamples": str,  # appears to be all '.'
        #"Batch_NumOfCases": str,  # appears to be all '.'
        "Gene": str,
        "GeneID": str,
        "Transcript": str,
        "VariantType": str,
        "HGVS_cDNA": str,
        "HGVS_protein": str,
        "VariantID": str,
        "A_CountForward": int,
        "A_CountReverse": int,
        "C_CountForward": int,
        "C_CountReverse": int,
        "G_CountForward": int,
        "G_CountReverse": int,
        "T_CountForward": int,
        "T_CountReverse": int,
        "Ins_CountForward": int,
        "Ins_CountReverse": int,
        "Del_CountForward": int,
        "Del_CountReverse": int,
        "Genosity_QualityScore": str,
        "HomopolymerSpan": int,
        "VariantLocation": str,
        "VariantEffect": str,
        "GenotypeCall": str,
        "DepthQCStatus": str,
        "rsValidated": str,
        "1000G_MaxAF": float,
        "rsMAF": float,
        "Gnomad_MaxAltFreq": float,
        "ExAC_MaxAltAF": float,
        "strand": str,  # NA values here
        "exon": str,  # NA values here
        "gnomadAltFreq_all": float,
        "clinVarIds": str,
        "clinVarOrigins": str,
        "clinVarClinSignifs": str,
        "cosmicIds": str,
        "Comparison summary": str,
        "NormalFDP": float,
        "NormalFAF": float,
    }
    return col_vals


def get_columns_from_picard_collect_targeted_pcr_metrics():

    """
    https://broadinstitute.github.io/picard/javadoc/picard/index.html?picard/analysis/directed/TargetedPcrMetrics.html
    """
    col_vals = {
        "CUSTOM_AMPLICON_SET": str,
        "AMPLICON_TERRITORY": int,
        "ON_AMPLICON_BASES": int,
        "NEAR_AMPLICON_BASES": int,
        "OFF_AMPLICON_BASES": int,
        "PCT_AMPLIFIED_BASES": float,
        "PCT_OFF_AMPLICON": float,
        "ON_AMPLICON_VS_SELECTED": float,
        "MEAN_AMPLICON_COVERAGE": float,
        "FOLD_ENRICHMENT": float,
        "PF_SELECTED_PAIRS": int,
        "PF_SELECTED_UNIQUE_PAIRS": int,
        "ON_TARGET_FROM_PAIR_BASES": int,
        "TARGET_TERRITORY": int,
        "GENOME_SIZE": int,
        "TOTAL_READS": int,
        "PF_READS": int,
        "PF_BASES": int,
        "PF_UNIQUE_READS": int,
        "PF_UQ_READS_ALIGNED": int,
        "PF_BASES_ALIGNED": int,
        "PF_UQ_BASES_ALIGNED": int,
        "ON_TARGET_BASES": int,
        "PCT_PF_READS": float,
        "PCT_PF_UQ_READS": float,
        "PCT_PF_UQ_READS_ALIGNED": float,
        "MEAN_TARGET_COVERAGE": float,
        "MEDIAN_TARGET_COVERAGE": float,
        "MAX_TARGET_COVERAGE": int,
        "MIN_TARGET_COVERAGE": int,
        "ZERO_CVG_TARGETS_PCT": float,
        "PCT_EXC_DUPE": float,
        "PCT_EXC_ADAPTER": float,
        "PCT_EXC_MAPQ": float,
        "PCT_EXC_BASEQ": float,
        "PCT_EXC_OVERLAP": float,
        "PCT_EXC_OFF_TARGET": float,
        "FOLD_80_BASE_PENALTY": str,  # had this originally as a float, but sometimes can be ?
        "PCT_TARGET_BASES_1X": float,
        "PCT_TARGET_BASES_2X": float,
        "PCT_TARGET_BASES_10X": float,
        "PCT_TARGET_BASES_20X": float,
        "PCT_TARGET_BASES_30X": float,
        "PCT_TARGET_BASES_40X": float,
        "PCT_TARGET_BASES_50X": float,
        "PCT_TARGET_BASES_100X": float,
        "AT_DROPOUT": float,
        "GC_DROPOUT": float,
        "HET_SNP_SENSITIVITY": float,
        "HET_SNP_Q": float,
        "SAMPLE": str,
        "LIBRARY": str,
        "READ_GROUP": str,
    }
    return col_vals


def get_columns_from_picard_collect_targeted_pcr_metrics_per_base_coverage_output():
    """
    Return the columns for Picard's CollectTargetedPcrMetrics where the output is:
    --PER_BASEless_COVERAGE
    """
    col_vals = {
        "chrom": str,
        "pos": int,
        "target": str,
        "coverage": int,
    }
    return col_vals


def get_columns_from_picard_collect_targeted_pcr_metrics_per_target_coverage_output():
    """
    Return the columns for Picard's CollectTargetedPcrMetrics where the output is:
    --PER_TARGET_COVERAGE
    """
    col_vals = {
        "chrom": str,
        "start": int,
        "end": int,
        "length": int,
        "name": str,
        "%gc": float,
        "mean_coverage": float,
        "normalized_coverage": float,
        "min_normalized_coverage": float,
        "max_normalized_coverage": float,
        "min_coverage": int,
        "max_coverage": int,
        "pct_0x": float,
        "read_count": int,
    }
    return col_vals

def get_columns_from_hd827_variant_file():
    """
    :return: columns from a HD827 data source file that was created
    # hd827 has a set of known files that came from: HD827_HD832_HD833_variant_annotation_COSMICv85_dbSNPv151.xlsx,
    in the worksheet: Variants_4_analysis
    """

    col_vals = {
        "CHROM": str,
        "POS": int,
        "REF": str,
        "ALT": str,
        "GENE": str,
        "VARIANT_TYPE": str,
        "NGS_VAF": float,
        "ddPCR_VAF": float,
        "dbSNP_Amino_Acid": str,
    }
    return col_vals


def get_columns_from_vcf():
    """
    :return: columns from a VCF file
    """

    col_vals = {
        "#CHROM": str,
        "POS": int,
        "ID": str,
        "REF": str,
        "ALT": str,
        "QUAL": str,
        "FILTER": str,
        "INFO": str
    }
    return col_vals


def get_columns_from_bed_with_no_header():
    """
    :return: columns from a BED file (note this is from a non-header BED file)
    followed format from here: https://genome.ucsc.edu/FAQ/FAQformat.html
    """

    col_vals = {
        "CHROM": str,
        "START_ZERO_BASED": int,
        "END": int,
        "NAME": str,
        "SCORE": int,
        "STRAND": str,
    }
    return col_vals

def get_columns_from_bed_with_no_header_from_bedtools_intersect():
    """
    :return: columns from a BED file (note this is from a non-header BED file)
    followed format from here: https://genome.ucsc.edu/FAQ/FAQformat.html
    """

    col_vals = {
        "CHROM": str,
        "START_ZERO_BASED": int,
        "END": int,
        "NAME": str,
        "SCORE": int,
        "STRAND": str,
        "IGNORE1": int,
        "IGNORE2": int
    }
    return col_vals


def get_columns_from_oc120plus_bed_file():
    """
    :return: columns from a BED file (note this is from a non-header BED file)
    followed format from here: https://genome.ucsc.edu/FAQ/FAQformat.html
    """

    col_vals = {
        "CHROM": str,
        "START_ZERO_BASED": int,
        "END": int,
        "NAME": str,
        "SCORE": int,
        "STRAND": str,
        "_START1": int,
        "_STOP1": int,
    }
    return col_vals


def get_columns_from_bed_run_through_bedtools_nuc():
    """
    :return: columns from a BED file run through bedtools nuc.  NOte I updated the bed file to have
    CHROM, START_ZERO_BASED, END, NAME, SCORE, STRAND
    followed format from here: https://genome.ucsc.edu/FAQ/FAQformat.html
    """

    col_vals = {
        "CHROM": str,
        "START_ZERO_BASED": int,
        "END": int,
        "NAME": str,
        "SCORE": int,
        "STRAND": str,
        "pct_at": float,
        "pct_gc": float,
        "num_A": int,
        "num_C": int,
        "num_G": int,
        "num_T": int,
        "num_N": int,
        "num_oth": int,
        "seq_len": int,
    }
    return col_vals


def get_columns_from_horizon_bed_with_no_header():
    """
    :return: columns from a BED file (note this is from a non-header BED file)
    followed format from here: https://genome.ucsc.edu/FAQ/FAQformat.html
    """

    col_vals = {
        "CHROM": str,
        "START_ZERO_BASED": int,
        "END": int,
        "GENE": str,
    }
    return col_vals


def get_columns_from_hotspot_bed_with_no_header():
    """
    :return: columns from a Hotspot BED file (note this is from a non-header BED file)
    """

    col_vals = {
        "CHROM": int,  # kept as an int here, b/c hotpsot does not have X or Y, and makes sorting easier
        "START_ZERO_BASED": int,
        "END": int,
        "GENE": str,
        "HGNC": str,
    }
    return col_vals


def get_columns_from_hotspotter_bed_with_no_header():
    """
    :return: columns from a hotspot version BED file (note this is from a non-header BED file)
    """

    col_vals = {
        "CHROM": str,
        "START_ZERO_BASED": int,
        "END": int,
        "GENE": str,
        "ANNOTATION": str
    }
    return col_vals


def get_columns_from_horizon_gold_set_snvs():
    """
    :return: columns to use for snvs
    """

    col_vals = {
        "Chromosome": str,
        "Gene": str,
        "Variant": str,
        "VAF": float,
        "Variant_Type": str,
    }
    return col_vals


def get_columns_from_horizon_gold_set_snvs_cnvs_fusions():
    """
    :return: columns to use for snvs
    """

    col_vals = {
        "Chromosome": str,
        "Gene": str,
        "Variant": str,
        "VAF": str,
        "Variant_Type": str
    }
    return col_vals


def get_column_types_summary_htqc():
    """
    Return the column names an types for the summary.htqc files from the pipeline
    """

    values = {
        'SIZE (bp)': int,
        'MEAN_ABS_COVERAGE': float,
        'MEAN_FILTERED_COVERAGE': float,
        'MEAN_COLLAPSED_COVERAGE': float,
        'FRACTION_WITH_ZERO_DP': float,
        'FRACTION_LTE_LOWER_DP': float,
        'FRACTION_GTE_UPPER_DP': float,

    }
    return values


def get_column_types_htqc():
    """
    Return the column names an types for the .htqc (NOT the Summary file) files from the pipeline
    """

    values = {
        'CHROM': str,
        'START': int,
        'END': int,
        'NAME': str,
        'SIZE (bp)': int,
        'MEAN_ABS_COVERAGE': float,
        'MEAN_FILTERED_COVERAGE': float,
        'MEAN_COLLAPSED_COVERAGE': float,
        'FRACTION_WITH_ZERO_DP': float,
        'FRACTION_LTE_LOWER_DP': float,
        'FRACTION_GTE_UPPER_DP': float,
    }
    return values


def get_columns_from_clinical_sample_reproducibility_runs():
    """
    REP1_pos  REP1_neg  REP2_pos  REP2_neg  Num_pos_pos  Variant_type  VAF_threshold  Depth_threshold  Consequential_impacts_only  Experiment_type
    2427      5         2428      4         2423         snv           5.0            200              False                       both
    1209      2         1209      2         1207         snv           5.0            200              False                       intra-run
    1218      3         1219      2         1216         snv           5.0            200              False                       inter-run
    26        0         26        0         26           indel         5.0            200              False                       both
    12        0         12        0         12           indel         5.0            200              False                       intra-run
    14        0         14        0         14           indel         5.0            200              False                       inter-run

    :return: columns to use for run
    """
    col_vals = {
        "REP1_pos": int,
        "REP1_neg": int,
        "REP2_pos": int,
        "REP2_neg": int,
        "Num_pos_pos": int,  # total number of variant entries that was + + for both REP1 and REP2
        "Variant_type": str,  # snv or indel
        "VAF_threshold": float,
        "Depth_threshold": int,
        "Consequential_impacts_only": bool,
        "Experiment_type": str,
        "Min_length_variant": int,
        "Max_length_variant": int,
    }
    return col_vals


def get_columns_from_sigma_lite(tumor_type: str="breast"):
    """
    Get the sigMa output (lite_format)
    :return: columns to use
    """
    # base columns found to work when tumor_type was breast
    col_vals = {
        'tumor': str,
        'total_snvs': int,
        'Signature_3_ml': float,
        'Signature_8_ml': float,
        'Signature_17_ml': float,
        'Signature_clock_ml': float,
        'Signature_APOBEC_ml': float,
        'Signature_3_c': float,
        'exp_sig3': float,
        'Signature_3_l_rat': float,
        'Signature_3_mva': float,
        'pass_mva': bool,
        'pass_mva_strict': bool,
        'sigs_all': str,
        'exps_all': str,
        'pass_ml': bool,
        'categ': str
    }
    # Breast cancer was used to develop the code, but as additional SigMA cancer types were added there
    # were some fields in the dict above that needed to be removed, or additional fields were required to
    # be added
    if tumor_type == 'breast':
        pass
    elif tumor_type in ['ovary', 'prost']:
        del col_vals['Signature_8_ml']
        del col_vals['Signature_17_ml']
        del col_vals['Signature_APOBEC_ml']
        if tumor_type == 'ovary':
            col_vals['Signature_18_ml'] = float
    elif tumor_type == 'stomach':
        del col_vals['Signature_8_ml']
        del col_vals['Signature_APOBEC_ml']
    elif tumor_type in ['uterus', 'bladder']:
        del col_vals['Signature_8_ml']
        del col_vals['Signature_17_ml']
    elif tumor_type in ['eso', 'panc_ad']:  # Oesophageal carcinoma OR Pancreas Adenocarcinoma
        del col_vals['Signature_8_ml']
    elif tumor_type == 'crc':  # Colorectal Adenocarcinoma
        col_vals['Signature_18_ml'] = float
        col_vals['Signature_41_ml'] = float
        col_vals['Signature_P1_ml'] = float
        del col_vals['pass_mva_strict']
        del col_vals['Signature_3_mva']
        del col_vals['pass_mva']
        del col_vals['pass_ml']
    elif tumor_type == 'lung':
        col_vals['Signature_4_ml'] = float
        col_vals['Signature_18_ml'] = float
        col_vals['Signature_16_ml'] = float
        col_vals['Signature_28_ml'] = float
        del col_vals['pass_ml']
        del col_vals['Signature_8_ml']
        del col_vals['pass_mva_strict']
        del col_vals['Signature_17_ml']
        del col_vals['Signature_3_mva']
        del col_vals['pass_mva']

    else:
        raise ValueError(f"unknown tumor type {tumor_type} in Pandas Column Typing")

    return col_vals


def get_columns_from_individual_clinical_sample_reproducibility_run():
    """
    :return: columns to use from an individual reproducibility run from clinical_sample_reproducibility.py
    """

    col_vals = {
        "Sample_ID": str,
        "Chromosome": str,
        "Position": int,
        "Ref_allele": str,
        "Alt_allele": str,
        "Allele_count1": float,  # needs to be float b/c of NA values cannot be converted to int
        "Allele_count2": float,  # needs to be float b/c of NA values cannot be converted to int
        "VAF1": float,
        "VAF2": float,
        "Depth1": float,  # needs to be float b/c of NA values cannot be converted to int
        "Depth2": float,  # needs to be float b/c of NA values cannot be converted to int
        "HGVSc": str,
        "Consequence": str,
        "Variant_class": str,
        "Indel": bool,
        "Variant_length": int,
        "Min_length_variant": int,
        "Max_length_variant": int,
        "Primary_caller1": str,
        "Primary_caller2": str,
        "REP1": str,
        "REP2": str,
        "Experiment_type": str,
        "Sample_cid1": str,
        "Sample_cid2": str
    }
    return col_vals


def query_pandas_df(df, query_string):
    """
    Input:  Pandas data frame to be queried
    Output: a queried data frame
    """
    return df.query(query_string)


def read_dataset_and_get_pandas_df(col_vals=None, file_name=None, keep_default_na=True,
                                   ignore_line_starts_with="#", reduce_size_data=None,
                                   sep='\t'):
    """
    :param col_vals: Dictionary of column names and types
    :param file_name: File to open
    :param keep_default_na: Boolean to keep the NAs or covert them via Pandas
    :param ignore_line_starts_with: What character to ignore, default #
    :param reduce_size_data:  Needed a way to make large data sets smaller
    :param sep: Seperator for the file when creating the dataframe
    Input:  The column values as a dictionary with the key = to the column, and the value = type, e.g. int, str, etc
    Output: pandas data frame
    """
    df = None
    try:
        with open(file_name, 'r') as fh:
            lines = [lines for lines in fh if not lines.startswith(ignore_line_starts_with)]  # ignore comments
        # if a value comes in here, then reduce the size of the return dataframe
        if reduce_size_data is not None:
            lines = lines[:reduce_size_data]

        df = pd.read_table(
            io.StringIO(''.join(lines)),
            usecols=list(col_vals.keys()),
            dtype=col_vals, # pass in the dictionary of key values, key is the column, key is the type, e.g. int, float...
            keep_default_na=keep_default_na,
            sep=sep
        )
    except FileNotFoundError as err:
        raise HorizonFile(err=err, file_name=file_name)
    except ValueError as err:
        raise PandasColsUse(file_name, err=err)

    return df


def read_dataset_and_get_pandas_df_with_break(col_vals=None, file_name=None, keep_default_na=True,
                                   ignore_line_starts_with="#",
                                   break_when_line_starts_with=None):
    """
    :param col_vals: Dictionary of column names and types, but break out of the reading based on Startswith
    :param file_name: File to open
    :param keep_default_na: Boolean to keep the NAs or covert them via Pandas
    :param ignore_line_starts_with: What character to ignore, default #
    Input:  The column values as a dictionary with the key = to the column, and the value = type, e.g. int, str, etc
    Output: pandas data frame
    """
    df = None
    try:
        with open(file_name, 'r') as fh:
            lines = []
            for line in fh:
                # this must be first, and not after line.startswith(ignore_line_starts_with):  # ignore comments
                if break_when_line_starts_with is not None and line.startswith(break_when_line_starts_with):
                    break
                elif line.startswith(ignore_line_starts_with):  # ignore comments
                    continue
                elif not line.strip(): # ignore empty line
                    continue
                else:
                    lines.append(line)
        df = pd.read_table(
            io.StringIO(''.join(lines)),
            usecols=list(col_vals.keys()),
            dtype=col_vals, # pass in the dictionary of key values, key is the column, key is the type, e.g. int, float...
            keep_default_na=keep_default_na,
            sep='\t'
        )
    except ValueError as err:
        raise PandasColsUse(file_name, err=err)

    return df


def read_dataset_with_no_header_and_get_pandas_df(col_vals=None, file_name=None, keep_default_na=True,
                                   ignore_line_starts_with="#"):
    """
    :param col_vals: Dictionary of column names and types
    :param file_name: File to open
    :param keep_default_na: Boolean to keep the NAs or covert them via Pandas
    :param ignore_line_starts_with: What character to ignore, default #
    Input:  The column values as a dictionary with the key = to the column, and the value = type, e.g. int, str, etc
    Output: pandas data frame
    """

    df = None
    try:
        with open(file_name, 'r') as fh:
            lines = [lines for lines in fh if not lines.startswith(ignore_line_starts_with)]  # ignore comments
        df = pd.read_table(
            io.StringIO(''.join(lines)),
            names=list(col_vals.keys()),
            dtype=col_vals, # pass in the dictionary of key values, key is the column, key is the type, e.g. int, float...
            keep_default_na=keep_default_na,
            sep='\t'
        )
    except ValueError as err:
        raise PandasColsUse(file_name, err=err)

    return df


def read_dataset_and_get_pandas_df_from_key_value_each_line(col_vals=None, file_name=None, keep_default_na=True,
                                                            delimiter="="):
    """
    Input:  The column values as a dictionary with the key = to the column, and the value = type, e.g. int, str, etc
    This type of file has each line, e.g.:
        MEAN_ABS_COVERAGE=1357.544
        MEAN_FILTERED_COVERAGE=252.081
        MEAN_COLLAPSED_COVERAGE=150.232
    :param col_vals: the column values for the data frame
    :param file_name: File to open
    :param keep_default_na: keep NA values
    :param delimiter: what to split the key pairs as, e.g. "="
    Output: pandas data frame
    """
    df = None
    try:
        lines = list()
        # you must keep the \n for this too work below
        lines.append(("\t".join(get_column_types_summary_htqc().keys()) + "\n"))

        with open(file_name, 'r') as fh:
            inner = []
            for line in fh:
                _, value = line.rstrip().split(delimiter)  # e.g. split on "="
                inner.append(value)
            lines.append("\t".join(inner))

        df = pd.read_table(
            io.StringIO(''.join(lines)),
            usecols=list(col_vals.keys()),
            dtype=col_vals,
            keep_default_na=keep_default_na,
            sep='\t'
        )
    except ValueError as err:
        raise PandasColsUse(file_name, err=err)

    return df


def print_row_dataframe(data_frame):
    """
    Input:  pandas data frame to get the values.  This was done before usecols
    Output: pandas data frame that passed all the thresholds
    """

    for _, row in data_frame.iterrows():
        row_dict = row.to_dict()
        for i, (key, val) in enumerate(row_dict.items(), 1):
            print(f'{i}\t{key}\t{val}')
        return


def print_data_frame(df, tabular_output_file, index_label=None, configuration_string=None, na_rep='NA',
                     append_mode=False, header=True):
    """
    Function to print out the data frames for this project
    :param df:  The pandas data frame passed in
    :param tabular_output_file: Name of the file to print too
    :param index_label: The name of the index label if you want to print one, else it will not be printed
    :param configuration_string: a string to print at the top, to show parameters
    :param na_rep: Can override the standard 'NA'
    :param append_mode: By default this is False, but can be changed so that the call to this will append
    :param header:  By default this is True, so it will print out the header
    :return: None
    """
    mode = 'w'
    if append_mode is True:
        mode = 'a'

    # print out the table
    with open(tabular_output_file, mode) as outfh:
        if configuration_string is not None:
            print(f'{configuration_string}', file=outfh)
        # mode='a' is a must b/c we might print documentation for the data frame directly above
        if index_label:
            df.to_csv(outfh, sep='\t', index_label=index_label, na_rep=na_rep, mode='a', header=header)
        else:
            df.to_csv(outfh, index=False, sep='\t', na_rep=na_rep, mode='a', header=header)


def sort_df_by_outliers_in_place(df=None, column1=None, column2=None, turn_column1_to_fraction=False,
                                 turn_column2_to_fraction=False, string_2_print=None, print_data_frame=False):
    """
    Function to find the differences between two columns and then to sort by that difference
    :param df: Data frame to sort
    :param column1: What first column to look at
    :param column2: What other column to look at
    :param turn_column1_to_fraction: Update it, e.g. VAF might be 10.5 = turn it to 0.105
    :param turn_column2_to_fraction: Update it, e.g. VAF might be 10.5 = turn it to 0.105
    """
    df[column1] = pd.to_numeric(df[column1])
    if turn_column1_to_fraction is True:
        df[column1] = df[column1] / 100
    df[column2] = pd.to_numeric(df[column2])
    if turn_column2_to_fraction is True:
        df[column2] = df[column2] / 100

    df["Differences"] = abs(df[column1] - df[column2])
    df.sort_values(ascending=False, by=["Differences"], inplace=True)

    if print_data_frame is True:
        print(string_2_print)
        print(df)


def get_cid_from_data_frame_row(row=None, col_name='sample_name'):
    match = re.search(r'(cid\d+-\d+)', row[col_name])
    if match:
        return match.groups(0)[0]
    return return_unknown_sample()


def get_cid_from_file_name(file_name=None):
    match = re.search(r'(cid\d+-\d+)', file_name)
    if match:
        return match.groups(0)[0]
    return return_unknown_sample()


def return_unknown_sample():
    return "Not a CID sample"