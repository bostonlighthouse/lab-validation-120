import numpy as np
import pandas as pd
import sys
import re
from lab_validation_120.utils.pandas_dataframes_utilities import read_dataset_and_get_pandas_df, \
    get_columns_from_horizon_gold_set_snvs, get_columns_from_horizon_gold_set_snvs_cnvs_fusions, query_pandas_df, \
    sort_df_by_outliers_in_place
from lab_validation_120.utils.r_function_stats import pbinom
from lab_validation_120.utils.vcf_utilities import get_keys_for_vcf_dictionary, dictionary_all_aa_changes


def process_all_horizon_samples_for_variants(config, group_by_key, vaf_threshold, depth_threshold,
                                             key_to_config_usage=None):
    """
    Go over all horizon samples and look for matches in the related VCF.  This function really only works for horizon
    samples that have known variants in files found in the config
    :param config: simple dictionary of values from the dict passed in
    :param group_by_key: Use this to create a new column in the data frame for grouping
    :param vaf_threshold: The vaf threshold to use, anything less than will not be analyzed
    :param depth_threshold: The depth threshold to use, anything less than will not be analyzed
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    """

    final_matched_results = []  # store all data that matched or did not in the vcf, 2-D list
    final_header_names = None  # store all the column names for the pandas dataframe at the end
    # go over all horizon samples found in the config dictionary and look for matches in their respective VCF file
    # found in the config dictionary
    for i, horizon_sample in enumerate(config[key_to_config_usage]['concordance_files']):
        # each horizon sample will look for matches in teh VCF and fill in final_matched_results
        horizon_df = _process_horizon_sample_for_matches_in_vcf_from_config(horizon_sample, depth_threshold,
                                                                            vaf_threshold,
                                                                            final_matched_results)
        # get the header of values and store for later, only need to do once
        if i == 0:
            # get all values from the gold set
            gold_set_names = [str(k) for k, _ in horizon_df.items()]
            # get all values from the dictionary
            vcf_set_names = [str(k) for k in get_keys_for_vcf_dictionary()]
            # add two additional columns
            vcf_set_names.append(f'PASSED_TOTAL_DP({depth_threshold})')
            vcf_set_names.append(f'PASSED_ALT_AF({vaf_threshold})')
            # final column names
            final_header_names = [group_by_key] + gold_set_names + vcf_set_names

    # final_matched_results is a 2-D list
    matched_results_df = pd.DataFrame(final_matched_results, columns=final_header_names)
    # change the VAF to fractional
    # just a helper function, to stop the function from printing set print_data_frame=False
    sort_df_by_outliers_in_place(df=matched_results_df, column1="VAF", column2="ALT_AF",
                                 string_2_print="Horizon samples all together",
                                 turn_column1_to_fraction=True, print_data_frame=False)

    add_binomial_probabilities_to_df(df=matched_results_df)

    return matched_results_df


def _process_horizon_sample_for_matches_in_vcf_from_config(horizon_sample, depth_threshold, vaf_threshold,
                                                           final_matched_results):
    """
    Function used to find matches or non-matches to the horizon data
    :horizon_sample: dict of values from the horizon file that was read in
    :depth_threshold: The depth of coverage threshold
    :vaf_threshold: The variant allele frequency  threshold
    :final_matched_results: list of all data that getting appended to over each horizon sample
    """

    # get all the data we need to look at in a defaultdict with keys chrom:gene:hgvsp like: 7_BRAF_V600E
    chrom_gene_aa_change = dictionary_all_aa_changes(vcf_file_name=horizon_sample['vcf_file'])

    horizon_df = None
    # Open the horizon gold set text input files and get the data for it via the config
    if horizon_sample['variant_type'] == 'snvs':
        horizon_df = read_dataset_and_get_pandas_df(col_vals=get_columns_from_horizon_gold_set_snvs(),
                                                    file_name=horizon_sample['horizon_file'])
    elif horizon_sample['variant_type'] == 'snvs_cnvs_fusion':
        horizon_df = read_dataset_and_get_pandas_df(col_vals=get_columns_from_horizon_gold_set_snvs_cnvs_fusions(),
                                                    file_name=horizon_sample['horizon_file'])

    horizon_df.apply(lambda row: test_df_for_pdot(row=row, file=horizon_sample['horizon_file']), axis=1)

    # we are only looking at SNV, INS, DEL, DELINS (delins are something I could not tell b/c its framshift
    horizon_df = query_pandas_df(horizon_df, 'Variant_Type == "SNV" \
                                                     or Variant_Type == "INS" \
                                                     or Variant_Type == "DEL" \
                                                     or Variant_Type == "DELINS"')
    # update the horizon df with two new columns
    # sometimes there data that's not exactly a chr, e.g. 15q22.31 or chr.7
    horizon_df['Chromosome_no_p_q'] = horizon_df.apply(lambda row: _update_data_frame_chr(row), axis=1)
    # set the key that can be matched on below
    horizon_df['Key'] = horizon_df.apply(lambda row: _update_data_frame_key(row), axis=1)

    # was there a match between horizon and the vcf data, given the thresholds
    for index, df_row in horizon_df.iterrows():
        # these are the values from the VCF entry
        vcf_set_values = _match_changes(df_row['Key'], chrom_gene_aa_change, depth_threshold, vaf_threshold)
        # get all values from the gold set
        gold_set_values = [str(v) for _, v in df_row.items()]
        final_matched_results.append([horizon_sample['sample_id']] + gold_set_values + vcf_set_values)

    return horizon_df


def test_df_for_pdot(row=None, file=None):
    """Simple function to make sure I don't have, e.g. p.A456T, since should only have A456T
    :param row: from from teh data frame
    :param file: File that caused the issue
    """
    if re.search(r"^p.\S+", row.Variant):

        sys.exit(f"Found a p.dot in the input file {row.Variant}, please open the file an remove it from:\n" +
                 f"{file}")
    else:
        pass


def _match_changes(key_2_match, chrom_gene_aa_change, depth_threshold=0, vaf_threshold=0):
    """
    Function used to find matches or non-matches to the horizon data, and return the values for matching based
    on read depth and vaf
    :param key_2_match: key to match in the chrom_gene_aa_change defaultdict
    :param chrom_gene_aa_change: defaultdict of data from the VCF file
    :param depth_threshold: The depth of coverage threshold
    :param vaf_threshold: The variant allele frequency  threshold
    """

    # there was a match in the golden set
    if key_2_match in chrom_gene_aa_change:
        # get all values from the dictionary, i.e. VCF data
        vcf_set_values = [str(v) for v in chrom_gene_aa_change[key_2_match].values()]
        # was there an issue with coverage and vaf
        if chrom_gene_aa_change[key_2_match]['TOTAL_DP'] < depth_threshold and \
                chrom_gene_aa_change[key_2_match]['ALT_AF'] < vaf_threshold:
            vcf_set_values.append('INDETERMINATE_TOTAL_DP')
            vcf_set_values.append('INDETERMINATE_ALT_AF')
        # was there an issue with coverage
        elif chrom_gene_aa_change[key_2_match]['TOTAL_DP'] < depth_threshold:
            vcf_set_values.append('INDETERMINATE_TOTAL_DP')
            vcf_set_values.append('PASS_ALT_AF')
        # was there an issue with vaf
        elif chrom_gene_aa_change[key_2_match]['ALT_AF'] < vaf_threshold:
            vcf_set_values.append('INDETERMINATE_ALT_AF')
            vcf_set_values.append('PASS_TOTAL_DP')
        # passed the thresholds
        else:
            vcf_set_values.append('PASS_TOTAL_DP')
            vcf_set_values.append('PASS_ALT_AF')
    else:
        vcf_set_values = [np.NaN for _ in range(len(get_keys_for_vcf_dictionary()))]
        vcf_set_values.append(np.NaN)  # for TOTAL_DP
        vcf_set_values.append(np.NaN)  # for ALT_AF

    return vcf_set_values


def _update_data_frame_key(row):
    """
    Helper function to update the key in the data frame to match the dictionary of VCF entries
    :param row: Row from the dictionary
    """
    return '_'.join((row['Chromosome_no_p_q'], row['Gene'], row['Variant']))


def _update_data_frame_chr(row):
    """
    Helper function to update the chromsome b/c the data had p and q arms in the chromosome name
    :param row: Row from the dictionary
    """
    value_2_change = row['Chromosome']

    if value_2_change.startswith('chr.'):  # remove the chr. off chr.7
        return value_2_change.replace('chr.', '')
    elif 'p' in value_2_change:
        return value_2_change.split('p')[0]  # remove the p22.31 from 15p22.31
    elif 'q' in value_2_change:
        return value_2_change.split('q')[0]  # remove the q22.31 from 15q22.31
    else:  # nothing to change
        return value_2_change


def add_binomial_probabilities_to_df(df=None):
    """
    Add binomial probabilities to the df
    :param df: Data frame to add the probabilities to
    """
    # use binomial to calculate the probability using the oc395 VAF
    df["Prob_oc395"] = df.apply(lambda row:
                                _update_data_frame_with_probability(row,
                                                                    vaf_key='ALT_AF'),
                                axis=1)
    # use binomial to calculate the probability using the horizon VAF
    df["Prob_horizon"] = df.apply(lambda row:
                                  _update_data_frame_with_probability(row,
                                                                      vaf_key='VAF'),
                                  axis=1)


def _update_data_frame_with_probability(row, vaf_key=None):
    """
    Helper function to get the probability, based on VAF from Horizon (known vaf, not our measured VAF),
    a minimum of 5 reads, and total coverage at that site
    :param row: with the probability
    :param vaf_key: What key to use for VAF
    """
    # the minimum reads, i.e. >= min_variant_reads are used in the pbnom call
    min_variant_reads = 5

    # if oc395 was not concordant, then return np.NAN for the oc395 column
    if row['ALT_DP'] is np.NaN and vaf_key == 'ALT_AF':
        return np.NaN
    # if oc395 was not concordant, then return probability based on 250x and horizon vaf for the horizon column
    if row['ALT_DP'] is np.NaN and vaf_key == 'VAF':
        vaf = row[vaf_key]
        return 1 - pbinom(min_variant_reads - 1, 250, prob=float(vaf) / 100)
    # else there are values and calculate them based on the min_variant_reads, found depth, and the vaf
    else:
        # needed to create copies of the value, b/c need to type clase below
        total_dp = row['TOTAL_DP']
        vaf = row[vaf_key]
        return 1 - pbinom(min_variant_reads-1, int(total_dp), prob=float(vaf)/100)