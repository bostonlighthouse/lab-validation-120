"""Functions related to assay names, etc."""


def get_assay_names():
    # type: () -> list
    """
    Return a simple list of assay names.  Add assay names here
    """
    return ['gs120', 'gs180', 'gs180_snp', 'gs395', 'gs395_normals', 'gs700', 'gs700_normals']


def get_filename(filename=None, extension=None):
    return f"{filename}.{extension}"


def get_root_repo_dir():
    return '/Users/cleslin/BLI-repo/lab-validation-120'