"""Functions for HTQC files from the pipeline"""
import os

from lab_validation_120.utils.pandas_dataframes_utilities import read_dataset_and_get_pandas_df, \
    get_column_types_htqc, query_pandas_df, print_data_frame
import pandas as pd
import re

from tools.utils.tools_utils import open_directory_get_list_files_recursively


def get_columns_names_and_yaxis_values_2_plot_for_htqc_summary():
    "Simple function to return the Column names and the y-axis label for plotting"
    columns_names_and_yaxis_values = {
        # 'SIZE (bp)': 'BP',
        'MEAN_ABS_COVERAGE': 'BP',
        'MEAN_FILTERED_COVERAGE': 'BP',
        'MEAN_COLLAPSED_COVERAGE': 'BP',
        'FRACTION_WITH_ZERO_DP': 'Percentage (%)',
        'FRACTION_LTE_LOWER_DP': 'Percentage (%)',
        'FRACTION_GTE_UPPER_DP': 'Percentage (%)',
    }
    return columns_names_and_yaxis_values


def get_htqc_interval_data(config=None, key_to_config_usage=None, col_name_4_sample_names=None, sample_type=None,
                           reduce_size_data=None):
    """
    Print out the bar heatmap for the htqc
    :param config: dictionary of values used for configuration
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What to name the new column in the data frame
    :param sample_type: What sample type to look at... Mostly will be None, but certain types might come in as "Tumor"
    :param reduce_size_data: You can make this run faster for testing.  None avoids reducing the dataset size
    :return A list of final dataframes
    """
    # get file object (technically not an object, but is in JSON-speak
    input_samples_dict = config[key_to_config_usage]['input_files']
    # store a list of data frames for plotting
    list_df = []
    # loop over all files and store them in a list of data frames
    for sample_dict in input_samples_dict['data']:
        df = read_dataset_and_get_pandas_df(col_vals=get_column_types_htqc(),
                                            file_name=sample_dict['qc_htqc_file'], reduce_size_data=reduce_size_data)

        # there are times when I only want to look at a sample type, e.g. "Tumor"
        if sample_type is not None:
            if sample_type == sample_dict['sample_type']:
                pass
            else:
                continue

        # add a column with the name of the sample from the config
        if 'sample_type' in sample_dict and sample_dict['sample_type']:
            df[col_name_4_sample_names] = '-'.join((sample_dict['sample_type'], sample_dict['sample_id']))
        else:
            df[col_name_4_sample_names] = sample_dict['sample_id']
        list_df.append(df)
    # convert the list of data frames to a large data frame
    df_all_values = pd.concat(list_df)

    # Add a new column that has the gene name, this was necessary b/c we didn't have exact gene name, they were
    # long strings, e.g. PIK3CD;NM_005026.3_Exon_20
    df_all_values['GENE'] = df_all_values.apply(lambda row: _add_gene_name(row), axis=1)
    #df_all_values = _filter_set_genes(df=df_all_values)
    # keep this return, used by other function outside this module

    # oc120plus had a slightly different BED file, which resulted in HTQC having gene names that were differently
    # tokenized.  So just fix here
    to_remove = [';NM', ';NP', ';NR']
    for ele in to_remove:
        df_all_values['GENE'].replace(ele, '', regex=True, inplace=True)

    return df_all_values


def _add_gene_name(row):
    """
    Go through the row and update the GENE name column
    :param row: The row from the data frame
    :return string GENE Name
    """
    if row.NAME.startswith('rs'):
        return "SNP"
    elif "g.139390145T>C" in row.NAME:
        return "SNP"
    elif "ROS1" in row.NAME:
        return "ROS1"
    else:
        return row.NAME.split("_")[0]


def _filter_set_genes(df=None):
    """
    Remove some gene names, e.g. NM002944_ROS1_exon042 or g.139390145T>C
    :param df: The data frame to be searched
    """
    new_df = query_pandas_df(df, 'GENE != "g.139390145T>C" \
                                 and GENE != "NM002944_ROS1_exon042" \
                                 and GENE != "NM002944_ROS1_exon041" \
                                 and GENE != "NM002944_ROS1_exon040" \
                                 and GENE != "NM002944_ROS1_exon039" \
                                 and GENE != "NM002944__ROS1_exon039" \
                                 and GENE != "NM002944_ROS1_exon038" \
                                 and GENE != "NM002944_ROS1_exon037" \
                                 and GENE != "NM002944_ROS1_exon036"')
    return new_df


def get_column_metric_names_for_htqc_intervals():
    """
    Get the column names for the metrics in HTQC overall intervals
    """
    return ["MEAN_COLLAPSED_COVERAGE", "MEAN_FILTERED_COVERAGE",  "MEAN_ABS_COVERAGE"]


def get_overall_gene_normalized_htqc_metric_df(df=None, column_to_use_for_calculation=None,
                                               mean_metric_total_bp_key=None,
                                               overall_gene_metric_key=None, col_name_4_sample_names=None):
    """
    We have MEAN_COLLAPSED_COVERAGE for each interval, but wanted to assess at the gene level.  So this function will
    group by each chromosome and then the gene and come up with a new "overall gene mcc".  e.g. if an gene had 3
    interval:
    Name        Size (bp)   MCC   = MCC_TOTAL_BP
    interval1    10     *    100   =  1000
    interval2    20     *    200   =  4000
    interval3    30     *    150   =  4500
    Sum          60                   9500   =  overall gene mcc 9500/60 =  158.3 = round int 158
    This function could be used for MEAN_ABS_COVERAGE, or MEAN_FILTERED_COVERAGE, or MEAN_COLLAPSED_COVERAGE
    :param df: data frame to use for plotting
    :param column_to_use_for_calculation:  What column (metric) will be used, e.g. MCC
    :param mean_metric_total_bp_key: The key in the df to use to the calculated value
    :param overall_gene_metric_key: the key in the df to use for genes
    :param col_name_4_sample_names: What to name the new column in the data frame
    """
    # get the new total metric column show above in the example
    df[mean_metric_total_bp_key] = df.apply(
        lambda row: row['SIZE (bp)'] * row[column_to_use_for_calculation],  # e.g. 'MEAN_COLLAPSED_COVERAGE'
        axis=1)

    data = []  # store all data to conver to a df later
    for chrom_gene, df_chrom_gene in df.groupby(["CHROM", "GENE", col_name_4_sample_names]):  # also group by the sample
        # sum up the size of the intervals by gene
        result_size = df_chrom_gene['SIZE (bp)'].agg(['sum'])
        # sum up the MCC_TOTAL_BP by gene, MCC_TOTAL_BP = SIZE (bp) * MEAN_COLLAPSED_COVERAGE
        result_mcc = df_chrom_gene[mean_metric_total_bp_key].agg(['sum'])
        # store the data for later
        data.append([chrom_gene[0], chrom_gene[1], chrom_gene[2], result_size['sum'], result_mcc['sum'],
                     int(round(result_mcc['sum'] / result_size['sum']))])
    # create a new data frame of the data
    columns = ["CHROM", "GENE", col_name_4_sample_names, "SIZE_TOTAL", mean_metric_total_bp_key, overall_gene_metric_key]
    df_overall_gene_mean_metric = pd.DataFrame(data, columns=columns)
    return df_overall_gene_mean_metric


def output_normalized_gene_counts_by_sample(df=None, col_name_4_sample_names=None, df_ouput_filename=None):
    """
    Print out the normalized MEAN_COLLAPSED_COVERAGE for each gene for each sample
    :param df: The HTQC intervals data for all samples and all intervals
    :param col_name_4_sample_names: What's the column name in the df for the sample names

    """

    column_to_use_for_calculation = 'MEAN_COLLAPSED_COVERAGE'
    mean_metric_total_bp_key = '_'.join((column_to_use_for_calculation, 'TOTAL_BP'))
    overall_gene_metric_key = f'OVERALL_GENE_MEAN_{column_to_use_for_calculation}'
    # get a normalized metric for each gene and each sample
    df_genes = get_overall_gene_normalized_htqc_metric_df(
        df=df,
        column_to_use_for_calculation=column_to_use_for_calculation,
        mean_metric_total_bp_key=mean_metric_total_bp_key,
        overall_gene_metric_key=overall_gene_metric_key,
        col_name_4_sample_names=col_name_4_sample_names)
    # print the data
    print_data_frame(df=df_genes, tabular_output_file=df_ouput_filename)

    # get the averages of the genes over the samples
    df_gene_averages = df_genes.groupby(['GENE']).mean().reset_index()
    file, _ = os.path.splitext(df_ouput_filename)
    print_data_frame(df=df_gene_averages, tabular_output_file=f"{file}_genes_averaged_over_sample.txt")


def get_htqc_summary_df_from_directory_files(directory: str, sample_col: str, file_glob: str='*htqc_output_summary.txt',
                                             is_summary: bool=True) -> pd.DataFrame:
    """

    :param directory:  Where to open the files
    :param file_glob: glot to grab files from in the directory
    :param sample_col:  column name to store the sample in
    :return:
    """
    list_hsmetrics_files = open_directory_get_list_files_recursively(directory=directory,
                                                                     file_glob=file_glob)

    list_df = []
    for filename in list_hsmetrics_files:
        sample_file_name = os.path.basename(filename)
        if is_summary is True:
            with open(filename) as in_fh:
                # Create a dictionary from the data
                data_dict = {line.split("=")[0]: float(line.split("=")[1]) for line in in_fh}
                # Convert the dictionary to a pandas DataFrame
                df = pd.DataFrame([data_dict])
        else:
            df = pd.read_csv(filename, sep="\t")
        df[sample_col] = sample_file_name
        list_df.append(df)

    return pd.concat(list_df).reset_index(drop=True)
