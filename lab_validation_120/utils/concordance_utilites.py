"""Module used for concordance between VCF files"""
import os.path
import sys
from collections import defaultdict

import numpy as np

import biomodels.models.vcf.vcf_container as vc
import pandas as pd

from lab_validation_120.utils.vcf_utilities import return_most_severe_impact_transcript_obj, \
    is_a_consequential_impact,\
    get_vaf_from_vf_field, \
    ignore_caller_because_of_caller_thresholds

from lab_validation_120.utils.pandas_dataframes_utilities import \
    get_columns_from_clinical_sample_reproducibility_runs, \
    get_columns_from_individual_clinical_sample_reproducibility_run, \
    read_dataset_and_get_pandas_df, print_data_frame

from lab_validation_120.utils.genomic_metadata_utilities import get_df_bed_intervals_by_assay, \
    variant_position_overlapped_bed_file


def process_samples_for_concordance(config=None, vaf_threshold=None, depth_threshold=None, key_to_search=None,
                                    assay_intervals_for_overlap_analysis=None):
    """
    Go over all  samples and look for matches in reproduced samples (inter or intra run)
    :param config: simple dictionary of values from the JSON passed in
    :param vaf_threshold: The VAF to use as a threshold
    :param depth_threshold: The Depth to use as a threshold
    :param key_to_search: What key to pull out of the config dictionary to get the values needed, e.g.:
    :param assay_intervals_for_overlap_analysis:  THere are times when we'll need to find overlap b/t assays.. not typical but will be
    needed
    "concordance_clinical_samples_reproducibility"
    """
    list_of_dict_df_bed = None
    if assay_intervals_for_overlap_analysis is not None:
        df_assay_intervals = get_df_bed_intervals_by_assay(which_oc_assay=assay_intervals_for_overlap_analysis)
        # get the list of dictionaries from the BED file
        list_of_dict_df_bed = df_assay_intervals.to_dict('records')
    final_matched_results = []  # store all data that matched or did not in the vcf, 2-D list
    # go over all clinical samples found in the JSON config file and look for matches in their respective VCF file found
    # in the JSON config file
    print(f"Processing {len(config[key_to_search]['concordance_files'])} samples", file=sys.stderr)
    for i, (sample_id, clinical_sample_list) in \
            enumerate(config[key_to_search]['concordance_files'].items()):
        list_sample_vcf_containers = []
        experiment_type = clinical_sample_list.pop(2)
        sample_cid1 = clinical_sample_list[0]['sample_id']
        sample_cid2 = clinical_sample_list[1]['sample_id']
        experiment_type = experiment_type['experiment_type']
        # go over each sample file and store the VCF files
        for clinical_sample in clinical_sample_list:
            list_sample_vcf_containers.append(vc.VcfContainer(clinical_sample['vcf_file']))

        consequential_impacts_only = config[key_to_search]['consequential_impacts_only']
        _process_clinical_samples_for_matches_in_vcf_from_config(sample_id=sample_id,
                                                                 list_sample_vcf_containers=list_sample_vcf_containers,
                                                                 depth_threshold=depth_threshold,
                                                                 vaf_threshold=vaf_threshold,
                                                                 final_matched_results=final_matched_results,
                                                                 consequential_impacts_only=consequential_impacts_only,
                                                                 experiment_type=experiment_type,
                                                                 sample_cid1=sample_cid1, sample_cid2=sample_cid2,
                                                                 list_of_dict_df_bed=list_of_dict_df_bed)
        print(f"Finished sample # {i}, {sample_id}", file=sys.stderr)
        # leave break here b/c I can uncomment and run process much faster for debugging
        #break
    return final_matched_results


def get_dictionary_of_concordance(sample_id=None, variant_entry1_obj=None, variant_entry2_obj=None,
                                  variant_transcript_obj=None,
                                  allele_count1=None, allele_count2=None,
                                  vaf1=None, vaf2=None,
                                  total_depth1=None, total_depth2=None,
                                  file1=None, file2=None,
                                  experiment_type=None,
                                  sample_cid1=None, sample_cid2=None,
                                  sample_pbinom1=None, sample_pbinom2=None):
    """
    Go through and get the dictionary of values to pass back for final output
    :param sample_id: The Id of the sample that was passed in
    :param variant_entry1_obj: The variant_entry object
    :param variant_entry2_obj: The variant_entry object
    :param variant_transcript_obj: The VEP object for the most severe consequence
    :param allele_count1:  allele count for sample 1
    :param allele_count2: allele count for sample 2
    :param vaf1: VAF for sample 1
    :param vaf2: VAF for sample 2
    :param total_depth1: Depth for sample 1
    :param total_depth2: Depth for sample 2
    :param file1: VCF file for REP1
    :param file2: VCF file for REP2
    :param experiment_type: What type of experiment was run?  e.g. inter-run or intra-run or other (oc395 vs oc120)
    :param  sample_cid1: From the config json key sample_id
    :param  sample_cid2: From the config json key sample_id
    :param sample_pbinom1: Pbinom of sample1
    :param sample_pbinom2: Pbinom of sample2

    *** Keep dictionary below sync'd with get_dictionary_of_recall
    """

    if variant_entry1_obj:
        variant_entry_obj_2_use = variant_entry1_obj
    else:
        variant_entry_obj_2_use = variant_entry2_obj

    indel = True
    if len(variant_entry_obj_2_use.REF) == len(variant_entry_obj_2_use.ALT):
        indel = False

    return {
        "Sample_ID": sample_id,
        "Chromosome": variant_entry_obj_2_use.CHROM,
        "Position": variant_entry_obj_2_use.POS,
        "Ref_allele": variant_entry_obj_2_use.REF,
        "Alt_allele": variant_entry_obj_2_use.ALT,
        # values vaf1, vaf2, total_depth1 and total_depth2 all have np.nan if they were not found
        "Allele_count1": allele_count1 if allele_count1 else np.nan,
        "Allele_count2": allele_count2 if allele_count2 else np.nan,
        "VAF1": vaf1 if vaf1 else np.nan,
        "VAF2": vaf2 if vaf2 else np.nan,
        "Depth1": total_depth1 if total_depth1 else np.nan,
        "Depth2": total_depth2 if total_depth2 else np.nan,
        "SYMBOL": variant_transcript_obj.SYMBOL if variant_transcript_obj.SYMBOL else 'ND',
        "HGVSp": variant_transcript_obj.HGVSp if variant_transcript_obj.HGVSp else 'ND',
        "HGVSc": variant_transcript_obj.HGVSc if variant_transcript_obj.HGVSc else 'ND',
        "Consequence": variant_transcript_obj.Consequence,
        "Variant_class": variant_transcript_obj.VARIANT_CLASS,
        "Indel": indel,  # is this an indel?  True or False
        "Variant_length":  len(variant_entry_obj_2_use.ALT) - len(variant_entry_obj_2_use.REF),
        "Primary_caller1": variant_entry1_obj.info_internal_2_vcf.PRIMARY_CALLER if variant_entry1_obj else 'ND',
        "Primary_caller2": variant_entry2_obj.info_internal_2_vcf.PRIMARY_CALLER if variant_entry2_obj else 'ND',
        "REP1": '+' if variant_entry1_obj is not None else'-',
        "REP2": '+' if variant_entry2_obj is not None else '-',
        "Experiment_type": experiment_type,
        "File1": file1,
        "File2": file2,
        "Sample_cid1": sample_cid1,
        "Sample_cid2": sample_cid2,
        "Pbinom_cid1": sample_pbinom1,
        "Pbinom_cid2": sample_pbinom2
    }


def get_dictionary_of_recall(samples_id=None, data_dict1=None, data_dict2=None,
                             allele_count1=None, allele_count2=None,
                             vaf1=None, vaf2=None,
                             total_depth1=None, total_depth2=None,
                             file1=None, file2=None,
                             experiment_type=None,
                             sample_id1=None, sample_id2=None,
                             sample_pbinom1=None, sample_pbinom2=None):
    """
    Go through and get the dictionary of values to pass back for final output
    :param samples_id: The Id of the sample that was passed in
    :param data_dict1: Dictionary of genomic data
    :param data_dict2: Dictionary of genomic data
    :param allele_count1:  allele count for sample 1
    :param allele_count2: allele count for sample 2
    :param vaf1: VAF for sample 1
    :param vaf2: VAF for sample 2
    :param total_depth1: Depth for sample 1
    :param total_depth2: Depth for sample 2
    :param file1: VCF file for REP1
    :param file2: VCF file for REP2
    :param experiment_type: What type of experiment was run?  e.g. inter-run or intra-run or other (oc395 vs oc120)
    :param  sample_id1: From the config json key sample_id
    :param  sample_id2: From the config json key sample_id
    :param sample_pbinom1: Pbinom of sample1
    :param sample_pbinom2: Pbinom of sample2

    *** Keep dictionary below sync'd with get_dictionary_of_concordance
    """
    indel = True
    if len(data_dict1['Ref']) == len(data_dict1['Alt']):
        indel = False

    return {
        "Sample_ID": samples_id,
        "Chromosome": data_dict1['Chrom'],
        "Position": data_dict1['Pos'],
        "Ref_allele": data_dict1['Ref'],
        "Alt_allele": data_dict1['Alt'],
        # values vaf1, vaf2, total_depth1 and total_depth2 all have np.nan if they were not found
        "Allele_count1": allele_count1 if allele_count1 else np.nan,
        "Allele_count2": allele_count2 if allele_count2 else np.nan,
        "VAF1": vaf1 if vaf1 else np.nan,
        "VAF2": vaf2 if vaf2 else np.nan,
        "Depth1": total_depth1 if total_depth1 else np.nan,
        "Depth2": total_depth2 if total_depth2 else np.nan,
        "SYMBOL": data_dict1['Gene'],
        "HGVSp": data_dict1['HGVS_protein'],
        "HGVSc": data_dict1['HGVS_cDNA'],
        "Consequence": data_dict1['VariantEffect'],
        "Variant_class": data_dict1['VariantType'],
        "Indel": indel,  # is this an indel?  True or False
        "Variant_length":  len(data_dict1['Alt']) - len(data_dict1['Ref']) ,
        "Primary_caller1": data_dict1['Caller'] if data_dict1 is not None and 'Caller' in data_dict1 else 'ND',
        "Primary_caller2": data_dict2['Caller'] if data_dict2 is not None and 'Caller' in data_dict2 else 'ND',
        "REP1": '+' if data_dict1 is not None else'-',
        "REP2": '+' if data_dict2 is not None else '-',
        "Experiment_type": experiment_type,
        "File1": file1,
        "File2": file2,
        "Sample_cid1": sample_id1,
        "Sample_cid2": sample_id2,
        "Pbinom_cid1": sample_pbinom1,
        "Pbinom_cid2": sample_pbinom2,
    }


def _process_clinical_samples_for_matches_in_vcf_from_config(sample_id=None, list_sample_vcf_containers=None,
                                                             depth_threshold=None,
                                                             vaf_threshold=None,
                                                             final_matched_results=None,
                                                             consequential_impacts_only=None,
                                                             experiment_type=None,
                                                             sample_cid1=None,
                                                             sample_cid2=None,
                                                             list_of_dict_df_bed=None):
    """
    Function used to go through the list of VCF container (only designed o use a list of 2) passed in, and update the
    list final_matched_results passed in with entries that were found and passed depth_threshold and vaf_thresholds
    :param sample_id: Clinical Sample begin looked at
    :param list_sample_vcf_containers: list containing the two vcf containers
    :param depth_threshold: Depth Threshold
    :param vaf_threshold: VAF Threshold
    :param final_matched_results: A list of dictionaries that gets updated
    :param  sample_cid1: From the config json key sample_id
    :param  sample_cid2: From the config json key sample_id
    :param list_of_dict_df_bed: A list of dictionaries, where each entry is a row in a BED fil
    """

    # get variant entry containers
    vc_container1, vc_container2 = list_sample_vcf_containers[0], list_sample_vcf_containers[1]
    # get all the keys from the containers
    values1 = vc_container1.dictionary_vcf_entries.keys()
    values2 = vc_container2.dictionary_vcf_entries.keys()
    # find the shared values
    shared_values = set(values1).intersection(values2)
    # find the difference values between the two sets, i.e. where one sample did not get sequenced, discordance
    diff_values1 = set(values1).difference(values2)
    diff_values2 = set(values2).difference(values1)

    # go over all elements, where are keys that where found shared, or unique to one variant entry container
    # sorted to keep output consistent, since we're using sets, that get turned into lists
    # we'll get numbers like when summed like (not sums not done here, see function get_values_on_rep1_rep2
    # REP1_pos  REP1_neg  REP2_pos  REP2_neg
    # 11744     1313      11870     1187       # Note number 11744+1313  == 11870+1185 b,c we adding all three lists
    for key in sorted(list(shared_values) + list(diff_values1) + list(diff_values2)):
        variant_entry1_obj, variant_entry2_obj = vc_container1.get_key(key), vc_container2.get_key(key)
        # leave here for debugging
        # print(f'FILE = {vc_container1.file}')
        # ignore these callers
        if variant_entry1_obj is not None and variant_entry1_obj.info_internal_2_vcf.PRIMARY_CALLER in {"pindel", "platypus", "scalpel"}:
            continue
        if variant_entry2_obj is not None and variant_entry2_obj.info_internal_2_vcf.PRIMARY_CALLER in {"pindel", "platypus", "scalpel"}:
            continue

        # if we pass this in then we need to if there was overlap with the list_of_dict_df_bed, otherwise we pass
        variant_entry_to_test_overlap = variant_entry1_obj if variant_entry1_obj is not None else variant_entry2_obj
        if list_of_dict_df_bed is not None:
            # do the actual test to see if there was overlap
            if variant_position_overlapped_bed_file(chrom=variant_entry_to_test_overlap.CHROM,
                                                    position=variant_entry_to_test_overlap.POS,
                                                    list_of_dict_df_bed=list_of_dict_df_bed) is True:
                pass
            else:
                continue

        # Might have an object that did not match b/c we used sets above
        if variant_entry1_obj:
            variant_transcript_obj = return_most_severe_impact_transcript_obj(variant_entry_obj=variant_entry1_obj)
        # use the other sample that did exist
        else:
            variant_transcript_obj = return_most_severe_impact_transcript_obj(variant_entry_obj=variant_entry2_obj)
        # was the argparse passed in to only look at transcript impacts?
        if consequential_impacts_only:
            # do not look a non consequential impact, then skip
            if is_a_consequential_impact(variant_consequence_list=variant_transcript_obj.Consequence) is False:
                continue

        total_depth1, total_depth2, allele_count1, allele_count2, vaf1, vaf2, file1, file2 = \
            None, None, None, None, None, None, 'ND', 'ND'
        # TODO what causes ALT_AF to now have a value?
        # 2020-06-12-oc395-run-b20-40-cid20-203-snapshot/2020-06-12-oc395-run-b20-40-cid20-203-snapshot.
        # B20-40.CID20-203.C513_N705.merged_concatenated.vcf
        # positions 6       31238259        .       G       T

        # remember it's possible for one of the variant entry objects to be None, b/c there was no match
        # (i.e. discordant)
        sig_rounding = 3
        if variant_entry1_obj:
            try:
                total_depth1 = variant_entry1_obj.info_internal_2_vcf.REF_DP + variant_entry1_obj.info_internal_2_vcf.ALT_DP
                allele_count1 = variant_entry1_obj.info_internal_2_vcf.ALT_DP
                vaf_2_use = get_vaf_from_vf_field(variant_entry_obj=variant_entry1_obj)
                vaf1 = round(vaf_2_use * 100, sig_rounding)
                file1 = vc_container1.file
            except TypeError:
                print("Issue with variant_entry1_obj.info_internal_2_vcf", variant_entry1_obj.info_internal_2_vcf, file=sys.stderr)
                print("Entire variant_entry1_obj", variant_entry1_obj, file=sys.stderr)
                continue

        if variant_entry2_obj:
            try:
                total_depth2 = variant_entry2_obj.info_internal_2_vcf.REF_DP + variant_entry2_obj.info_internal_2_vcf.ALT_DP
                allele_count2 = variant_entry2_obj.info_internal_2_vcf.ALT_DP
                vaf_2_use = get_vaf_from_vf_field(variant_entry_obj=variant_entry2_obj)
                vaf2 = round(vaf_2_use * 100, sig_rounding)
                file2 = vc_container2.file
            except TypeError:
                print("Issue with variant_entry2_obj.info_internal_2_vcf", variant_entry2_obj.info_internal_2_vcf, file=sys.stderr)
                print("Entire variant_entry2_obj", variant_entry2_obj, file=sys.stderr)
                continue

        # ignore a certain type of call from the analysis of concordance, see vcf_utilites.ignore_caller_because_of_caller_thresholds
        # when in one but not the other
        if variant_entry1_obj and variant_entry2_obj is None:
            ignore, _ = ignore_caller_because_of_caller_thresholds(variant_entry_obj=variant_entry1_obj,
                                                          variant_transcript_obj=variant_transcript_obj)
            if ignore is True:
                continue
        elif variant_entry2_obj and variant_entry1_obj is None:
            ignore, _ = ignore_caller_because_of_caller_thresholds(variant_entry_obj=variant_entry2_obj,
                                                          variant_transcript_obj=variant_transcript_obj)
            if ignore is True:
                continue
        else:
            ignore1, _ = ignore_caller_because_of_caller_thresholds(variant_entry_obj=variant_entry1_obj,
                                                          variant_transcript_obj=variant_transcript_obj)
            ignore2, _ = ignore_caller_because_of_caller_thresholds(variant_entry_obj=variant_entry2_obj,
                                                               variant_transcript_obj=variant_transcript_obj)
            if ignore1 is True and ignore2 is True:
                continue

        # only look at variants that passed the thresholds when both samples were matched
        # need not None, b/c VAF and Depth below can be 0.0 or 0, which would test as False in if or elif
        if (vaf1 is not None and vaf2 is not None) and (vaf1 < vaf_threshold and vaf2 < vaf_threshold):
            continue
        # only look at sample1 data
        elif (vaf1 is not None and vaf2 is None) and vaf1 < vaf_threshold:
            continue
        # only look at sample2 data
        elif (vaf2 is not None and vaf1 is None) and vaf2 < vaf_threshold:
            continue

        # only look at variants that passed the thresholds when both samples were matched
        # need not None, b/c VAF and Depth below can be 0.0 or 0
        if (total_depth1 is not None and total_depth2 is not None) and \
                (total_depth1 < depth_threshold and total_depth2 < depth_threshold):
            continue
        # only look at sample1 data
        elif (total_depth1 is not None and total_depth2 is None) and (total_depth1 < depth_threshold):
            continue
        # only look at sample2 data
        elif (total_depth2 is not None and total_depth1 is None) and (total_depth2 < depth_threshold):
            continue

        # get the dictionary of values to return
        entry_dict = get_dictionary_of_concordance(sample_id=sample_id,
                                                   variant_entry1_obj=variant_entry1_obj,
                                                   variant_entry2_obj=variant_entry2_obj,
                                                   variant_transcript_obj=variant_transcript_obj,
                                                   allele_count1=allele_count1, allele_count2=allele_count2,
                                                   vaf1=vaf1, vaf2=vaf2,
                                                   total_depth1=total_depth1, total_depth2=total_depth2,
                                                   file1=file1, file2=file2,
                                                   experiment_type=experiment_type,
                                                   sample_cid1=sample_cid1, sample_cid2=sample_cid2)

        # keep here since I can print this out if needed for debugging
        # print(entry_dict)
        # if entry_dict['REP1'] == '-' or entry_dict['REP2'] == '-':
            # if variant_entry1_obj:
                # print(variant_entry1_obj)
                # print(variant_entry1_obj.POS)
            # if variant_entry2_obj:
                # print(variant_entry2_obj)
                # print(variant_entry2_obj.POS)

        # update the list passed in with the new dictionary
        final_matched_results.append(entry_dict)


def get_overall_stem_plot_concordance_df(file=None, equivalency_study=None):
    """Get to overall data frame of concordance, and then add some new columns
    :param file: the file to open using the read_dataset_and_get_pandas_df
    :param equivalency_study: Is this an equivalency study, e.g. oc120 vs oc120plus, then only compare the second sample
    :return df: A data frame from the file input, with new columns
    """

    # get the data frame of data from the input file
    df_overall_data = read_dataset_and_get_pandas_df(col_vals=get_columns_from_clinical_sample_reproducibility_runs(),
                                                     file_name=file)

    df_overall_data['Total_positives'] = df_overall_data.apply(lambda row: row.REP1_pos + row.REP2_pos, axis=1)
    df_overall_data['Total_negatives'] = df_overall_data.apply(lambda row: row.REP1_neg + row.REP2_neg, axis=1)

    # was this an equivalency study? Note: Total_calls must be calculated first
    if equivalency_study is False:
        df_overall_data['Total_calls'] = df_overall_data.apply(lambda row:
                                                               row.Num_pos_pos + row.REP1_neg + row.REP2_neg,
                                                               axis=1)
        df_overall_data['Percent_positive'] = df_overall_data.apply(lambda row: _get_concordance_rate(row), axis=1)
        # this composite key will be used int eh data frame, but is nice for plotting on the x-axis of the stem plot
        df_overall_data['Composite_key'] = df_overall_data.apply(lambda row: " ".join((str(row.VAF_threshold) + "%",
                                                                                       str(row.Depth_threshold) + 'x',
                                                                                       str(row.Total_calls) + '#')),
                                                                 axis=1)
    else:
        df_overall_data['Total_calls'] = df_overall_data.apply(lambda row: row.Num_pos_pos + row.REP2_neg, axis=1)
        df_overall_data['Percent_positive'] = df_overall_data.apply(lambda row: _get_equivalency_concordance_rate(row),
                                                                    axis=1)
        # this composite key will be used int eh data frame, but is nice for plotting on the x-axis of the stem plot
        df_overall_data['Composite_key'] = df_overall_data.apply(lambda row: " ".join((str(row.VAF_threshold) + "%",
                                                                                       str(row.Depth_threshold) + 'x',
                                                                                       str(row.Total_calls) + '#')),
                                                                 axis=1)


    return df_overall_data


def get_overall_sample_metrics_concordance_df(file=None):
    """Get to overall sample data frame of concordance, and then add some new columns for metrics
    :param file: the file to open using the read_dataset_and_get_pandas_df
    :return df: A data frame from the file input, with new columns
    """

    df_overall_concordance = \
        read_dataset_and_get_pandas_df(col_vals=get_columns_from_individual_clinical_sample_reproducibility_run(),
                                       file_name=file)
    # set a boolean here for whether the row was concordant
    df_overall_concordance['Is_concordant'] = df_overall_concordance.apply(lambda row: _get_concordance_boolean(row),
                                                                           axis=1)
    # find the average vaf between vaf1 and vaf2 when concordant, else use only the single value
    df_overall_concordance['Vaf_2_boxplot'] = \
        df_overall_concordance.apply(lambda row: _get_average_value(row, key1="VAF1", key2="VAF2"), axis=1)
    # find the average depth between depth1 and depth2 when concordant, else use only the single value
    df_overall_concordance['Depth_2_boxplot'] = \
        df_overall_concordance.apply(lambda row: _get_average_value(row, key1="Depth1", key2="Depth2"), axis=1)
    # find the average allele between depth1 and depth2 when concordant, else use only the single value
    df_overall_concordance['Allele_count_2_boxplot'] = \
        df_overall_concordance.apply(lambda row: _get_average_value(row, key1="Allele_count1", key2="Allele_count2"),
                                     axis=1)
    return df_overall_concordance


def get_concordance_data(df=None, is_concordant=None):
    """Get the concordance data
    :param df: the df that needs to be filtered
    :param is_concordant: boolean that says if it's concordant data or discordant data we're looking for
    """
    df_is_concordant = df[df['Is_concordant'] == is_concordant]
    return df_is_concordant


def get_concordance_variables(is_concordant=None):
    """Simple helper to return some variables use in plotting and naming a file"""
    is_concordant_filename_str = "discordant"
    xlabel = "Discordant"
    if is_concordant is True:
        is_concordant_filename_str = "concordant"
        xlabel = "Concordant"
    return is_concordant_filename_str, xlabel


def _get_average_value(row, key1=None, key2=None):
    """
    Return the average vaf between vaf1 and vaf2
    :param: row from the data frame called by the lambda function
    """
    # was it concordant call? Then take the average
    if row['REP1'] == '+' and row['REP2'] == '+':
        return (row[key1] + row[key2]) / 2
    # was REP1 positive?  Then just use that
    elif row['REP1'] == '+':
        return row[key1]
    # must use REP2 then...
    else:
        return row[key2]


def _get_concordance_boolean(row):
    """
    Return the Concordance boolean for the row + + = True, else False
    :param: row from the data frame called by the lambda function
    """
    if row['REP1'] == '+' and row['REP2'] == '+':
        return True
    else:
        return False


def _get_concordance_rate(row):
    """
    Return the Concordance rate calculation (see the paper below
    :param: row from the data frame called by the lambda function

    https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4489803/
    Concordance rate calculation
    The concordance rate, Rc, between duplicate samples was defined as where Nc was the number of concordant SNVs
    between a pair of replicate samples (i.e., variants that were detected in both samples), and N1 and N2 were the
    total number of SNVs detected in each of the duplicated sample (i.e., the sum of concordant and discordant NVs).
    """
    # See
    # https://www.sciencedirect.com/science/article/abs/pii/S1872497316301041
    # https://invivoscribe.com/Posters/2017/EHA/05172017_LMC_poster_EHA_small-panel-flt-3-across-panels_final.pdf

    # cannot calculate with Total calls were 0, this happens b/c we ran clinical_sample_reproducibility.py over many
    # thresholds, some which did not have any variants found
    if row.Total_calls == 0:
        return 0
    else:
        # when it was a REP1 positive it counts, and then if it caused a REP2 - then it was in REP1 as well
        #                                             REP1_pos   REP2_neg
        variants_rep1 = row.REP1_pos + row.REP2_neg   #  +          -
        # when it was a REP2 positive it counts, and then if it caused a REP1 - then it was in REP2 as well
        #                                             REP2_pos   REP1_neg
        variants_rep2 = row.REP2_pos + row.REP1_neg   #  +          -
        mean = (variants_rep1 + variants_rep2) / 2.0
        return (row['Num_pos_pos'] / mean) * 100


def _get_equivalency_concordance_rate(row):
    """
    Return the Concordance rate calculation for equivalency study, i.e only look at the second sample
    """
    # cannot calculate with Total calls were 0, this happens b/c we ran clinical_sample_reproducibility.py over many
    # thresholds, some which did not have any variants found
    if row.Total_calls == 0 or row.Num_pos_pos == 0:
        return 0
    else:
        return 100 - ((row.REP2_neg / (row.REP2_neg + row.Num_pos_pos)) * 100)


def indel_concordance_size_thresholds():
    """
    Just return the three thresholds that are used for indel concordance
    e.g. 1-1, 2-5, 5-30
    """
    return [(1, 1), (2, 4), (5, 30)]


def prepare_matched_results_for_printing(args=None, final_matched_results=None, vaf_threshold=None,
                                         depth_threshold=None, filter_hotspot_caller=True,
                                         only_print_intra_inter_tables=False):
    """
    :param args: argpase instance
    :param only_print_intra_inter_tables: Boolean as to only print out intra_inter tables. False default

    """

    final_matched_result_values = [list(entry.values()) for entry in final_matched_results]

    # if the length is 0, there was no matches, so we'll have to create one...
    if len(final_matched_results) == 0:
        final_header_names = [" Sample_ID", "Chromosome", "Position", "Ref_allele", "Alt_allele", "Allele_count1",
                              "Allele_count2", "VAF1", "VAF2", "Depth1", "Depth2", "HGVSp", "HGVSc",
                              "Consequence", "Variant_class", "Indel", "Variant_length", "Primary_caller1",
                              "Primary_caller2", "REP1", "REP2", "Experiment_type", "File1", "File2", "Sample_cid1",
                              "Sample_cid1"]

        matched_results_df = pd.DataFrame(columns=final_header_names)
    else:
        final_header_names = list(final_matched_results[0].keys())
        matched_results_df = pd.DataFrame(final_matched_result_values, columns=final_header_names)

    if filter_hotspot_caller is True:
        # filter out all hotspots
        matched_results_df = matched_results_df[(matched_results_df['Primary_caller1'] != 'hotspotter') &
                                                (matched_results_df['Primary_caller2'] != 'hotspotter')]

    _print_final_concordance_tables(vaf_threshold=vaf_threshold, depth_threshold=depth_threshold,
                                   is_consequential=args.consequential_impacts_only,
                                   df=matched_results_df, print_final_output_header=args.print_final_output_header,
                                   tabular_output_file=args.tabular_output_file,
                                   only_print_intra_inter_tables=only_print_intra_inter_tables)


def _print_final_concordance_tables(df=None, vaf_threshold=None, depth_threshold=None, is_consequential=None,
                                    print_final_output_header=None, tabular_output_file=None,
                                    only_print_intra_inter_tables=False):
    """
    :param df: The df that will be used (the matched data frame)
    :param vaf_threshold: The VAF to use as a threshold
    :param depth_threshold: The Depth to use as a threshold
    :param is_consequential: Are you to only look for consequential impacts
    :param print_final_output_header: Boolean to state whether to print the header line
    :param tabular_output_file: Where to print the final output file
    :param only_print_intra_inter_tables: Boolean as to only print out intra_inter tables. False default
    """
    # get the header line for this output, storing b/c I want to include this in the variant output
    header_str = "\t".join(list(get_columns_from_clinical_sample_reproducibility_runs().keys()))
    # print if argparse said too...
    if print_final_output_header:
        print(header_str)

    matched_snvs = _filter_matched_results(vaf_threshold=vaf_threshold, depth_threshold=depth_threshold,
                                           is_consequential=is_consequential,
                                           df=df, is_deletion=False)

    list_matched_indels = []
    # go over all thresholds and store them
    for threshold_tuple in indel_concordance_size_thresholds():
        matched_indels = _filter_matched_results(vaf_threshold=vaf_threshold, depth_threshold=depth_threshold,
                                                 is_consequential=is_consequential, df=df, is_deletion=True,
                                                 min_length_variant=threshold_tuple[0],
                                                 max_length_variant=threshold_tuple[1])
        list_matched_indels.append(matched_indels)
    # unpack the three threshold
    matched_indels_1st_threshold, matched_indels_2nd_threshold, matched_indels_3rd_threshold = list_matched_indels

    # get the strings to use as keys
    df_intra_inter, df_intra, df_inter, str_intra_inter, str_intra, str_inter = _get_concordance_keys()
    # for the actual data frame's file, print the configuration document string, not used anywhere else, could drop this
    configuration_str = f'# parameters vaf_threshold ({vaf_threshold})\t' \
                       f'depth_threshold ({depth_threshold})\t' \
                       f'consequential_impacts_only ({is_consequential})\n' \
                       f'# {header_str}\n' \
                       f'# {matched_snvs[str_intra_inter]}\n' \
                       f'# {matched_indels_1st_threshold[str_intra_inter]}\n' \
                       f'# {matched_indels_2nd_threshold[str_intra_inter]}\n' \
                       f'# {matched_indels_3rd_threshold[str_intra_inter]}\n'
    """
        configuration_str = f'# parameters vaf_threshold ({vaf_threshold})\t' \
                       f'depth_threshold ({depth_threshold})\t' \
                       f'consequential_impacts_only ({is_consequential})\n' \
                       f'# {header_str}\n' \
                       f'# {matched_snvs[str_intra_inter]}\n' \
                       f'# {matched_snvs[str_intra]}\n' \
                       f'# {matched_snvs[str_inter]}\n' \
                       f'# {matched_indels_1st_threshold[str_intra_inter]}\n' \
                       f'# {matched_indels_1st_threshold[str_intra]}\n' \
                       f'# {matched_indels_1st_threshold[str_inter]}\n' \
                       f'# {matched_indels_2nd_threshold[str_intra_inter]}\n' \
                       f'# {matched_indels_2nd_threshold[str_intra]}\n' \
                       f'# {matched_indels_2nd_threshold[str_inter]}\n' \
                       f'# {matched_indels_3rd_threshold[str_intra_inter]}\n' \
                       f'# {matched_indels_3rd_threshold[str_intra]}\n' \
                       f'# {matched_indels_3rd_threshold[str_inter]}'
    """
    configuration_str_to_print_stdout = configuration_str.lstrip().replace('# ', '')  # need both replaces here
    print(configuration_str_to_print_stdout)
    # get a new filename for the variant level concordance data
    filename, extension = os.path.splitext(tabular_output_file)
    filename_base = "".join((filename, "_", f"vaf_{float(vaf_threshold)}", "_",
                            f"depth_{depth_threshold}"))

    # looking at all values or consequential only?
    if is_consequential is True:
        # subtitle = f"Thresholds: VAF >= {vaf_threshold} Depth >= {depth_threshold} Consequential Variant Impacts Only"
        filename_base = "".join((filename_base, "_", "consequential_impacts_only"))
    else:
        # subtitle = f"Thresholds: VAF >= {vaf_threshold} Depth >= {depth_threshold} All Variant Impacts"
        filename_base = "".join((filename_base, "_", "all_impacts"))

    for df_key, addition_2_filename, df in ((f"{df_intra_inter}", "_snvs_both_inter_and_intra", matched_snvs),
                                            #(f"{df_intra}", "_snvs_intra", matched_snvs),
                                            #(f"{df_inter}", "_snvs_inter", matched_snvs),
                                            # indels of length 1
                                            (f"{df_intra_inter}", "_indels_size_1_1_both_inter_and_intra",
                                             matched_indels_1st_threshold),
                                            #(f"{df_intra}", "_indels_size_1_1_intra", matched_indels_1st_threshold),
                                            #(f"{df_inter}", "_indels_size_1_1_inter", matched_indels_1st_threshold),
                                            # indels of length 2-4
                                            (f"{df_intra_inter}", "_indels_size_2_4_both_inter_and_intra",
                                             matched_indels_2nd_threshold),
                                            #(f"{df_intra}", "_indels_size_2_4_intra", matched_indels_2nd_threshold),
                                            #(f"{df_inter}", "_indels_size_2_4_inter", matched_indels_2nd_threshold),
                                            # indels of length 5 or more
                                            (f"{df_intra_inter}", "_indels_size_5_or_more_both_inter_and_intra",
                                             matched_indels_3rd_threshold),
                                            #(f"{df_intra}", "_indels_size_5_or_more_intra",
                                            # matched_indels_3rd_threshold),
                                            #(f"{df_inter}", "_indels_size_5_or_more_inter",
                                            # matched_indels_3rd_threshold),
                                            ):
        # if we're only to print out the intra_inter tables
        if only_print_intra_inter_tables is True and df_intra_inter not in df_key:
            continue
        # print the actual snv tables out
        _print_variant_tables(df=df, df_key=df_key, filename_base=filename_base,
                              addition_2_filename=addition_2_filename, extension=extension,
                              configuration_str=configuration_str)


def _print_variant_tables(df=None, df_key=None, filename_base=None, extension=None, configuration_str=None,
                          addition_2_filename=None):

    filename_all = "".join((filename_base, addition_2_filename, extension))
    print_data_frame(df[df_key], filename_all, configuration_string=configuration_str)


def _filter_matched_results(df=None, vaf_threshold=None, depth_threshold=None, is_consequential=None,
                            is_deletion=None, min_length_variant=0, max_length_variant=0):
    """
    :param df: The df that will be used (the matched dataframe)
    :param vaf_threshold: The VAF to use as a threshold
    :param depth_threshold: The Depth to use as a threshold
    :param is_deletion: Bool if it's an deletion
    :param is_consequential: Are you to only look for consequential impacts
    :param min_length_variant: minimum length of the variant, not snv are a 0, b/c an indel is 1 or -1
    :param max_length_variant: maximum length of the variant, not snv are a 0, b/c an indel is 1 or -1
    """
    # column to tell if it's an insertion or deletion (Boolean),
    # e.g. if min_length_variant = 1 and max_length_variant = 1
    # e.g. if min_length_variant = 2 and max_length_variant = 4
    # e.g. if min_length_variant = 5 and max_length_variant = very large number
    df_intra_inter = df[(df['Indel'] == is_deletion)
                        & (abs(df['Variant_length']) >= min_length_variant)
                        & (abs(df['Variant_length']) <= max_length_variant)]

    df_intra_inter = df_intra_inter.copy()
    # set these in this new data frame "df_intra_inter", and not before
    df_intra_inter["Min_length_variant"] = min_length_variant
    df_intra_inter["Max_length_variant"] = max_length_variant
    # it's inter and intra run
    str_intra_inter = _get_configuration_string_for_final_analysis(vaf_threshold=vaf_threshold,
                                                                   depth_threshold=depth_threshold,
                                                                   df=df_intra_inter,
                                                                   is_consequential=is_consequential,
                                                                   experiment_type='both', is_deletion=is_deletion,
                                                                   min_length_variant=min_length_variant,
                                                                   max_length_variant=max_length_variant)
    # left for debugging
    #print_data_frame(df=df, tabular_output_file=f"{is_deletion}_str_intra_inter_{max_length_variant}_junk.txt")
    # second time look at both intra-run
    df_intra = df_intra_inter[(df_intra_inter['Experiment_type']) == 'intra-run']
    str_intra = _get_configuration_string_for_final_analysis(vaf_threshold=vaf_threshold,
                                                             depth_threshold=depth_threshold,
                                                             df=df_intra,
                                                             is_consequential=is_consequential,
                                                             experiment_type='intra-run', is_deletion=is_deletion,
                                                             min_length_variant=min_length_variant,
                                                             max_length_variant=max_length_variant)
    # left for debugging
    #print_data_frame(df=df, tabular_output_file=f"{is_deletion}_str_intra_{max_length_variant}_junk.txt")
    # third time look at both inter-run
    df_inter = df_intra_inter[(df_intra_inter['Experiment_type']) == 'inter-run']
    str_inter = _get_configuration_string_for_final_analysis(vaf_threshold=vaf_threshold,
                                                             depth_threshold=depth_threshold,
                                                             df=df_inter,
                                                             is_consequential=is_consequential,
                                                             experiment_type='inter-run', is_deletion=is_deletion,
                                                             min_length_variant=min_length_variant,
                                                             max_length_variant=max_length_variant)
    # left for debugging
    #print_data_frame(df=df, tabular_output_file=f"{is_deletion}_str_inter_{max_length_variant}_junk.txt")

    df_intra_inter_key, df_intra_key, df_inter_key, str_intra_inter_key, str_intra_key, str_inter_key = \
        _get_concordance_keys()

    return {
        df_intra_inter_key: df_intra_inter,
        df_intra_key: df_intra,
        df_inter_key: df_inter,
        str_intra_inter_key: str_intra_inter,
        str_intra_key: str_intra,
        str_inter_key: str_inter
    }


def _get_concordance_keys():
    """Simple function to return strings that will be used as keys in a dictionary from _filter_matched_results()
     Wanted to create this function b/c I was using the string in multiple places, not as just the keys
     """

    return "df_intra_inter", "df_intra", "df_inter", "str_intra_inter", "str_intra", "str_inter"


def _get_configuration_string_for_final_analysis(vaf_threshold=None, depth_threshold=None, df=None,
                                                 is_consequential=None, experiment_type=None,
                                                 is_deletion=None, min_length_variant=None,
                                                 max_length_variant=None):
    """
    Print out the final overall analysis
    :param vaf_threshold: The VAF to use as a threshold
    :param depth_threshold: The Depth to use as a threshold
    :param df: The data frame with all the matched data of concordance
    :param is_consequential: Boolean passed in from argparse
    :param experiment_type: str that tells the type of experiment, e.g. intra-run or inter-run
    :param is_deletion: Bool if it's an deletion
    :param min_length_variant: minimum length of the variant, not snv are a 0, b/c an indel is 1 or -1
    :param max_length_variant: maximum length of the variant, not snv are a 0, b/c an indel is 1 or -1
    """
    variant_type = "snv"
    if is_deletion:
        variant_type = "indel"

    final_values = _get_values_on_rep1_rep2(df=df)
    values = list(final_values.keys())
    df_pos_pos = df[(df[values[0]] == '+') &  # REP1
                    (df[values[1]] == '+')]   # REP2
    # get the output string containing how many values + and - were found in each bucket
    # storing b/c I want to include this in the variant output
    output_str = ("\t".join((str(final_values[values[0]]['+']), str(final_values[values[0]]['-']),
                             str(final_values[values[1]]['+']), str(final_values[values[1]]['-']),
                             str(len(df_pos_pos)),  # the Num_pos_pos
                             variant_type,
                             str(float(vaf_threshold)), str(depth_threshold),
                             str(is_consequential), experiment_type,
                             str(min_length_variant), str(max_length_variant))))

    #print(output_str)
    return output_str


def _get_values_on_rep1_rep2(df=None):
    """
    :param df: The data frame to search for the + and -
    :return dictionary of the values
    """
    # store the data for the final output
    values = ['REP1', 'REP2']
    # column names go over
    final_values = defaultdict(None)
    for col in values:
        # find the count for each REP1 or REP2 samples
        series = df[col].value_counts()
        final_values[col] = {}
        # + and - were match or no match between the two samples
        for sign in ['+', '-']:
            if series.get(sign, None) is not None:
                final_values[col][sign] = series[sign]
            else:
                final_values[col][sign] = 0
    return final_values
