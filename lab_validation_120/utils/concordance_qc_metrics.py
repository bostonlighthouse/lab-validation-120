import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
import os

from lab_validation_120.utils.htqc_utilites import get_htqc_interval_data, get_column_metric_names_for_htqc_intervals
from lab_validation_120.utils.plots.bar_chart_plot_utilities import process_bar_chart_analysis_4_htqc_summary, \
    print_htqc_summary_data
from lab_validation_120.utils.pandas_dataframes_utilities import read_dataset_and_get_pandas_df, \
    get_columns_from_bed_run_through_bedtools_nuc, print_data_frame
from lab_validation_120.utils.plots.plot_utilities import global_rc_set, return_dictionary_x_y_values, \
    get_subplot_bounding_box_parameters, get_plot_file_type, get_x_y_list_of_points
from lab_validation_120.utils.plots.regression_plot_utilities import regression_plot


from lab_validation_120.utils.plots.boxplot_plot_utilities import \
    process_boxplot_analysis_for_htqc_intervals_by_chromosome, \
    process_boxplot_analysis_for_htqc_intervals_for_oncokb_genes
from lab_validation_120.utils.plots.distribution_plot_utilities import process_distribution_analysis_for_htqc_intervals
from lab_validation_120.utils.plots.heatmap_plot_utilities import process_gene_heatmap_analysis_for_htqc_intervals
from lab_validation_120.utils.plots.boxplot_plot_utilities import \
    prepare_boxplot_subplots_by_metric_for_htqc_intervals_by_sample
from lab_validation_120.utils.configs.lab_validation_config import get_oc120_targets_bed_gc_content_file, \
    get_gc_bias_config_for_htqc_metric


def process_concordance_htqc_plots(config=None, key_to_config_usage=None, col_name_4_sample_names=None,
                                   list_of_heatmap_values=None, name_for_all_samples_combined=None,
                                   htqc_barchart_figsize_width=25,
                                   htqc_boxchart_figsize_width=25,
                                   htqc_barchart_sort_x_by_value=True,
                                   htqc_boxchart_sort_x_by_value=True,
                                   only_high_level_metrics=False,
                                   sample_type=None,
                                   sequencing_run=None,
                                   sort_gc_analysis_charts_by_metric=True,
                                   intervals_df_filename=None,
                                   htqc_summary_data_filename=None,
                                   heatmap_df_filename=None,
                                   alternate_colors=False):
    """
    This function will drive the plotting of multiple different types of HTQC plots: barcharts, heatmaps, and boxplots
    Will do this for Horizon data, clinical data, and min. DNA so just wanted this to be one function
    :param config: simple dictionary of values from the dict passed in
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What to name the new column in the data frame
    :param list_of_heatmap_values: List of tuples that contain the various HTQC metrics
            # metric: e.g. MEAN_TARGET_COVERAGE
            # vmax is how high to show results, i.e. 800 = 800 of the target
                # vmax here is the highest number on the heatmap
            # num_segments_on_cbar is the number of segments to break the heatmaps, e.g.
            # vmax and num_segments_on_cbar can be None
    :param name_for_all_samples_combined: A final data structure is created with All samples combined for plotting
    :param figsize_width: The width of the plot for HTQC metrics
    :param htqc_barchart_sort_x_by_value: Sort the x-axis values by the value of each for HTQC metrics, default is True
    :param htqc_boxchath_sort_x_by_value: Sort the x-axis values by the value of each for HTQC metrics, default is True
    :param only_high_level_metrics: Don't plot all the other boxplots and heatmaps
    :param sample_type: What sample type to look at... Mostly will be None, but certain types might come in as "Tumor"
    :param sequencing_run: what Run was this from?  Could be a batch number as well
    :param heatmap_df_filename: Print out the gene heatmap data frame for analysis
    :param htqc_summary_data_filename: File name for the summary metrics from HTQC
    :param intervals_df_filename: File name for the intervals dataframe
    :param sort_gc_analysis_charts_by_metric: Sort the charts by the metric or just by the Sample name:  Default is metric
    :param alternate_colors: alternate the colors on the bar chart, default is False
    """

    # Bar charts of QC metrics
    #"""
    process_bar_chart_analysis_4_htqc_summary(config=config,
                                              key_to_config_usage=key_to_config_usage,
                                              col_name_4_sample_names=col_name_4_sample_names,
                                              figsize_width=htqc_barchart_figsize_width,
                                              sort_x_by_value=htqc_barchart_sort_x_by_value,
                                              alternate_colors=alternate_colors)
    # plot out the summary file
    if htqc_summary_data_filename:
        print_htqc_summary_data(tabular_output_file=htqc_summary_data_filename,
                                col_name_4_sample_names=col_name_4_sample_names,
                                key_to_config_usage=key_to_config_usage,
                                config=config)
    #"""

    df_all_htqc_interval_values = get_htqc_interval_data(config=config,
                                                         key_to_config_usage=key_to_config_usage,
                                                         col_name_4_sample_names=col_name_4_sample_names,
                                                         sample_type=sample_type)

    #""""
    #plot_regression_gc_content_vs_coverage(df_htqc=df_all_htqc_interval_values,
    #                                       col_name_4_sample_names=col_name_4_sample_names, config=config,
    #                                       key_to_config_usage=key_to_config_usage,
    #                                       sort_gc_analysis_charts_by_metric=sort_gc_analysis_charts_by_metric)

    prepare_boxplot_subplots_by_metric_for_htqc_intervals_by_sample(
        df=df_all_htqc_interval_values,
        images_dir=config[key_to_config_usage]['images_dir'],
        col_name_4_sample_names=col_name_4_sample_names,
        config=config,
        key_to_config_usage=key_to_config_usage,
        figsize_width=htqc_boxchart_figsize_width,
        htqc_boxchart_sort_x_by_value=htqc_boxchart_sort_x_by_value,
        alternate_colors=alternate_colors
    )

    # print out the intervals for the heatamps
    if intervals_df_filename :
        print_data_frame(df=df_all_htqc_interval_values,
                         tabular_output_file=intervals_df_filename)

    if only_high_level_metrics is True:
        return df_all_htqc_interval_values

    process_gene_heatmap_analysis_for_htqc_intervals(df=df_all_htqc_interval_values, config=config,
                                                     key_to_config_usage=key_to_config_usage,
                                                     col_name_4_sample_names=col_name_4_sample_names,
                                                     list_of_heatmap_values=list_of_heatmap_values,
                                                     sequencing_run=sequencing_run,
                                                     heatmap_df_filename=heatmap_df_filename)

    # distributions
    process_distribution_analysis_for_htqc_intervals(df=df_all_htqc_interval_values, config=config,
                                                     key_to_config_usage=key_to_config_usage,
                                                     col_name_4_sample_names=col_name_4_sample_names)

    # boxplots of interval level gene data
    process_boxplot_analysis_for_htqc_intervals_by_chromosome(df=df_all_htqc_interval_values, config=config,
                                                              key_to_config_usage=key_to_config_usage,
                                                              col_name_4_sample_names=col_name_4_sample_names)
    # make a copy, so that I can run all samples in a final boxplot
    df_all_htqc_interval_values_temp = df_all_htqc_interval_values.copy(deep=True)
    # change the sample name to all, so it will be grouped by, i.e. all will be grouped by
    df_all_htqc_interval_values_temp[col_name_4_sample_names] = name_for_all_samples_combined
    process_boxplot_analysis_for_htqc_intervals_by_chromosome(df=df_all_htqc_interval_values_temp, config=config,
                                                              key_to_config_usage=key_to_config_usage,
                                                              col_name_4_sample_names=col_name_4_sample_names)

    # boxplots for interval level gene data of oncokb gene lists per sample (df_all_htqc_interval_values) and then
    # combined in all samples (df_all_htqc_interval_values_temp)
    for df in [df_all_htqc_interval_values, df_all_htqc_interval_values_temp]:
        process_boxplot_analysis_for_htqc_intervals_for_oncokb_genes(df=df, config=config,
                                                                     key_to_config_usage=key_to_config_usage,
                                                                     col_name_4_sample_names=col_name_4_sample_names)

    return df_all_htqc_interval_values

def plot_regression_gc_content_vs_coverage(df_htqc=None, col_name_4_sample_names=None, config=None,
                                           key_to_config_usage=None,
                                           sort_gc_analysis_charts_by_metric=True):
    """
    Go through and analyze the gc content vs. the metrics
    :param df_htqc: The data frame containing all samples being analyzed and the HTQC metrics
    :param col_name_4_sample_names: What to name the new column in the data frame
    :param sort_gc_analysis_charts_by_metric: Sort the charts by the metric or just by the Sample name:  Default is metric
    """
    # get the gc content by interval
    gc_oc120_df = read_dataset_and_get_pandas_df(file_name=get_oc120_targets_bed_gc_content_file(),
                                                 col_vals=get_columns_from_bed_run_through_bedtools_nuc())
    # create a copy, b/c I don't want to mess with this df outside this gc bias analysis
    df_htqc_temp = pd.DataFrame.copy(df_htqc, deep=True)
    # rename the column to match the gc df
    df_htqc_temp.rename(columns={"START": "START_ZERO_BASED"}, inplace=True)
    # left join on our data with the gc data.  This has many samples in it
    df_all_cols = pd.merge(df_htqc_temp, gc_oc120_df, on=["CHROM", "START_ZERO_BASED", "END"], how="left")
    # some very short intervals, and my just 1, so get rid of them for this analysis
    df_all_cols = df_all_cols[df_all_cols['seq_len'] > 10]
    process_gc_analysis_for_htqc_intervals_by_sample(df=df_all_cols, config=config,
                                                     key_to_config_usage=key_to_config_usage,
                                                     col_name_4_sample_names=col_name_4_sample_names,
                                                     sort_gc_analysis_charts_by_metric=sort_gc_analysis_charts_by_metric)


def process_gc_analysis_for_htqc_intervals_by_sample(df=None, config=None, key_to_config_usage=None,
                                                     col_name_4_sample_names=None,
                                                     sort_gc_analysis_charts_by_metric=True):
    """
    :param df: data frame to use for plotting
    :param config: dictionary of values used for configuration
    :param key_to_config_usage: What key will be used in the config to get the rest of the data needed below
    :param col_name_4_sample_names: What to name the new column in the data frame
    :param sort_gc_analysis_charts_by_metric: Sort the charts by the metric or just by the Sample name:  Default is metric
    """
    num_unique_samples = df[col_name_4_sample_names].nunique()  # need this to figure out how large the plot should be
    images_dir = config[key_to_config_usage]['images_dir']
    figsize_width = num_unique_samples * 5

    for dict_ in get_gc_bias_config_for_htqc_metric():
        # get the values from the config dictionary above
        acronym_of_metric_to_plot = dict_['acronym_of_metric_to_plot']
        metric = dict_['df_col_metric_to_plot']
        max_y_axis_2_plot = dict_['y_upper_lim']
        # new plot being created for the metric
        global_rc_set()
        plt.rcParams['figure.figsize'] = (figsize_width, 6)
        fig, ax = plt.subplots(1, num_unique_samples)  # rows x cols

        dict_values_2_sort = {}
        # go over and get mean, b/c I want to sort by this, so we can plot in ascending order of mean
        for i, (sample, df_by_sample) in enumerate(sorted(df.groupby(col_name_4_sample_names))):
            mean_value = df_by_sample[metric].mean()
            # Read coverage is normalized to the mean value so that the results would not scale with the amount of data
            df_by_sample[metric] = df_by_sample[metric] / mean_value
            dict_values_2_sort[i] = {
                "df": df_by_sample,
                "mean": mean_value,
                "sample": sample
            }
        # Sort the charts by the sample names or the mean from the dictionary dict_values_2_sort
        sort_by = 'sample'
        if sort_gc_analysis_charts_by_metric is True:
            sort_by = 'mean'

        # sort the dict_values_2_sort by the variable sort_by
        for i, sample_tuple in enumerate(sorted(dict_values_2_sort.items(), key=lambda k_v: k_v[1][sort_by])):
            ax[i].text(0.8, .9, f"{acronym_of_metric_to_plot} = {round(sample_tuple[1]['mean'], 0)}",
                       transform=ax[i].transAxes, fontweight="bold")
            # go through and get the df ready for plotting
            prepare_gc_subplots_by_metric_for_htqc_intervals_by_sample(ax=ax[i], df=sample_tuple[1]['df'],
                                                                       sample_name=sample_tuple[1]['sample'],
                                                                       metric=metric,
                                                                       max_y_axis_2_plot=max_y_axis_2_plot,
                                                                       y_axis_label=acronym_of_metric_to_plot,
                                                                       gc_col_name='pct_gc')  # key found by bedtools



        fig.tight_layout(rect=get_subplot_bounding_box_parameters())
        # store the image
        if not os.path.exists(images_dir):
            os.makedirs(images_dir)

        plt.savefig(os.path.join(images_dir, config[key_to_config_usage]['input_files']['group_id'] +
                                 f"_regression_GC_Coverage_{metric}_verified_and_presence_confirmed_mutations" +
                                 get_plot_file_type()))
        plt.close(fig)  # have to close


def prepare_gc_subplots_by_metric_for_htqc_intervals_by_sample(ax=None, df=None, sample_name=None, metric=None,
                                                               max_y_axis_2_plot=None, y_axis_label=None,
                                                               gc_col_name=None):
    """
    :param ax: an instance of the class plt.Axes
    :param df: data frame to use for plotting
    :param metric: The metric to be plotted
    :param sample_name: The Sample name of the gc plot
    """
    x_value_key = 'PCT_GC'
    y_value_key = metric

    dict_horizon_vals = dict(df.apply(lambda row:
                                      return_dictionary_x_y_values(row_from_df=row,
                                                                   x_value_key=x_value_key,
                                                                   y_value_key=y_value_key,
                                                                   df_value1=gc_col_name,
                                                                   df_value2=metric),
                                      axis=1))
    x, y = get_x_y_list_of_points(dict_values=dict_horizon_vals, x_value_key=x_value_key, y_value_key=y_value_key)
    regression_plot(ax=ax, x=x, y=y, max_x_axis_2_plot=None, max_y_axis_2_plot=max_y_axis_2_plot,
                    x_axis_label=f"% GC Content\n{sample_name}",
                    y_axis_label=f"{y_axis_label}", plot_reference_line=False,
                    size_point=10,  r_value_squared_threshold=0.00, horizontal_threshold_line_value=1.0,
                    horizontal_threshold_line_label=f"Normalized {y_axis_label}",
                    add_2_x_axis="", add_2_y_axis=f"(normalized to the mean {y_axis_label})")

