"""Module to complete the concordance with the Horizon samples at the LIMIT of Detection"""
import argparse
import sys
from lab_validation_120.utils.configs.limit_of_detection_config import get_limit_of_detection_for_horizon_htqc_oc120, \
    get_horizon_concordance_in_lod_oc120, get_limit_of_detection_for_clinical_sample_htqc_oc120_rep1, \
    get_clinical_sample_concordance_in_lod_oc120_rep1, get_clinical_sample_concordance_in_lod_oc120_rep2, \
    get_limit_of_detection_for_clinical_sample_htqc_oc120_rep2, \
    get_limit_of_detection_for_clinical_sample_htqc_oc120plus_rep1, \
    get_limit_of_detection_for_clinical_sample_htqc_oc120plus_rep2, \
    get_clinical_sample_concordance_in_lod_oc120plus_rep1, \
    get_clinical_sample_concordance_in_lod_oc120plus_rep2, \
    get_horizon_concordance_in_lod_oc120plus_rep1, get_horizon_concordance_in_lod_oc120plus_rep2, \
    get_limit_of_detection_for_horizon_htqc_oc12plus_rep1, \
    get_limit_of_detection_for_horizon_htqc_oc12plus_rep2

from lab_validation_120.utils.configs.oc395.oc395primer_boost_limit_of_detection_config import \
    get_horizon_concordance_in_lod_oc395primer_boost_rep1, \
    get_limit_of_detection_for_horizon_htqc_oc395primer_boost_rep1,\
    get_limit_of_detection_for_clinical_sample_htqc_oc395primer_boost_rep1, \
    get_limit_of_detection_for_clinical_sample_htqc_oc395primer_boost_rep2, \
    get_clinical_sample_concordance_in_lod_oc395primer_boost_rep1, \
    get_clinical_sample_concordance_in_lod_oc395primer_boost_rep2


from lab_validation_120.utils.configs.oc395_snps.oc395snps_limit_of_detection_config import \
    get_limit_of_detection_for_clinical_sample_htqc_oc395snp_rep1, \
    get_limit_of_detection_for_clinical_sample_htqc_oc395snp_rep2, \
    get_clinical_sample_concordance_in_lod_oc395snp_rep1, \
    get_clinical_sample_concordance_in_lod_oc395snp_rep2

from lab_validation_120.utils.configs.agilent700.agilent700_limit_of_detection_config import \
    get_clinical_sample_concordance_in_lod_agilent700_rep1, \
    get_clinical_sample_concordance_in_lod_agilent700_rep2, \
    get_limit_of_detection_for_clinical_sample_htqc_agilent700_rep1, \
    get_limit_of_detection_for_clinical_sample_htqc_agilent700_rep2

from lab_validation_120.utils.horizon_concordance_utilities import process_all_horizon_samples_for_variants, \
    dictionary_all_aa_changes
from lab_validation_120.utils.pivot_utilities import print_data_frame_pivot_outputs
from lab_validation_120.utils.plots.regression_plot_utilities import process_regression_and_bland_altman_analysis
from lab_validation_120.utils.concordance_qc_metrics import process_concordance_htqc_plots
from lab_validation_120.utils.configs.oc120plus_snps.oc120plus_snps_limit_of_detection_config import \
    get_limit_of_detection_for_horizon_htqc_oc120plus_snps, get_horizon_concordance_in_lod_oc120plus_snps, \
    get_clinical_sample_concordance_in_lod_oc120plus_snps_rep1, \
    get_clinical_sample_concordance_in_lod_oc120plus_snps_rep2, \
    get_limit_of_detection_for_clinical_sample_htqc_oc120plus_snps_rep1, \
    get_limit_of_detection_for_clinical_sample_htqc_oc120plus_snps_rep2


def main():
    """Main driver of the program"""

    args = get_cli_args()
    config = None
    sample = None
    if args.sample_type == 'horizon_oc120':
        config = get_limit_of_detection_for_horizon_htqc_oc120()
        sample = 'hd827'
    elif args.sample_type == 'clinical_rep1_oc120':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc120_rep1()
        sample = 'clinical_sample_rep1'
    elif args.sample_type == 'clinical_rep2_oc120':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc120_rep2()
        sample = 'clinical_sample_rep2'
    # new OC120plus
    elif args.sample_type == 'horizon_rep1_oc120plus':
        config = get_limit_of_detection_for_horizon_htqc_oc12plus_rep1()
        #sample = 'hd798'
        sample = 'hd798_rep1_oc120plus'
    elif args.sample_type == 'horizon_rep2_oc120plus':
        config = get_limit_of_detection_for_horizon_htqc_oc12plus_rep2()
        #sample = 'hd798'
        sample = 'hd798_rep2_oc120plus'
    elif args.sample_type == 'clinical_rep1_oc120plus':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc120plus_rep1()
        sample = 'clinical_sample_rep_oc120plus'
    elif args.sample_type == 'clinical_rep2_oc120plus':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc120plus_rep2()
        sample = 'clinical_sample_rep2_oc120plus'
    elif args.sample_type == 'horizon_rep1_oc395primer_boost':
        config = get_limit_of_detection_for_horizon_htqc_oc395primer_boost_rep1()
        sample = 'horizon_rep1_oc395primer_boost'
    elif args.sample_type == 'clinical_rep1_oc395primer_boost':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc395primer_boost_rep1()
        sample = 'clinical_rep1_oc395primer_boost'
    elif args.sample_type == 'clinical_rep2_oc395primer_boost':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc395primer_boost_rep2()
        sample = 'clinical_rep2_oc395primer_boost'
    elif args.sample_type == 'horizon_oc120plus_snps':
        config = get_limit_of_detection_for_horizon_htqc_oc120plus_snps()
        sample = 'horizon_oc120plus_snps'
    elif args.sample_type == 'clinical_rep1_oc120plus_snps':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc120plus_snps_rep1()
        sample = 'clinical_rep1_oc120plus_snps'
    elif args.sample_type == 'clinical_rep2_oc120plus_snps':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc120plus_snps_rep2()
        sample = 'clinical_rep2_oc120plus_snps'
    elif args.sample_type == 'clinical_rep1_oc395snp':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc395snp_rep1()
        sample = 'clinical_rep1_oc395snp'
    elif args.sample_type == 'clinical_rep2_oc395snp':
        config = get_limit_of_detection_for_clinical_sample_htqc_oc395snp_rep2()
        sample = 'clinical_rep2_oc395snp'
    elif args.sample_type == 'clinical_rep1_agilent700':
        config = get_limit_of_detection_for_clinical_sample_htqc_agilent700_rep1()
        sample = 'clinical_rep1_agilent800'
    elif args.sample_type == 'clinical_rep2_agilent700':
        config = get_limit_of_detection_for_clinical_sample_htqc_agilent700_rep2()
        sample = 'clinical_rep2_agilent800'


    name_for_all_samples_combined = f"{sample}_lod"
    key_to_config_usage = 'limit_of_detection'  # what key to walk into
    # what do you want to name the column that stores the samples
    col_name_4_sample_names = 'DNA_INPUT'
    list_of_heatmap_values = [("MEAN_COLLAPSED_COVERAGE", 1000, 10),
                              ("MEAN_FILTERED_COVERAGE", 1000, 10),
                              ("MEAN_ABS_COVERAGE", 4000, 8)]
    # go over all the concordance plots
    process_concordance_htqc_plots(config=config, key_to_config_usage=key_to_config_usage,
                                   col_name_4_sample_names=col_name_4_sample_names,
                                   list_of_heatmap_values=list_of_heatmap_values,
                                   name_for_all_samples_combined=name_for_all_samples_combined,
                                   only_high_level_metrics=True,
                                   htqc_summary_data_filename=f"{sample}_{args.snp_type}_all_htqc_summary_data.txt",
                                   intervals_df_filename=f"{sample}_{args.snp_type}_all_htqc_intervals_data.txt",
                                   heatmap_df_filename=f"{sample}_{args.snp_type}_heatmap_data.txt")
    group_by_key = ''
    if args.sample_type == 'horizon_oc120':
        config = get_horizon_concordance_in_lod_oc120(args.snp_type)
        group_by_key = "horizon_oc120"  # This can be named whatever you want...
    elif args.sample_type == 'horizon_rep1_oc120plus':
        config = get_horizon_concordance_in_lod_oc120plus_rep1(args.snp_type)
        group_by_key = "horizon_rep1_oc120plus"
    elif args.sample_type == 'horizon_rep2_oc120plus':
        config = get_horizon_concordance_in_lod_oc120plus_rep2(args.snp_type)
        group_by_key = "horizon_id_rep2_oc120plus"
    elif args.sample_type == 'clinical_rep1_oc120':
        config = get_clinical_sample_concordance_in_lod_oc120_rep1(args.snp_type)
        group_by_key = "clinical_rep1_oc120"
    elif args.sample_type == 'clinical_rep2_oc120':
        config = get_clinical_sample_concordance_in_lod_oc120_rep2(args.snp_type)
        group_by_key = "clinical_rep2_oc120"
    elif args.sample_type == 'clinical_rep1_oc120plus':
        config = get_clinical_sample_concordance_in_lod_oc120plus_rep1(args.snp_type)
        group_by_key = "clinical_rep1_oc120plus"
    elif args.sample_type == 'clinical_rep2_oc120plus':
        config = get_clinical_sample_concordance_in_lod_oc120plus_rep2(args.snp_type)
        group_by_key = "clinical_rep2_oc120plus"
    elif args.sample_type == 'horizon_rep1_oc395primer_boost':
        config = get_horizon_concordance_in_lod_oc395primer_boost_rep1(args.snp_type)
        group_by_key = "horizon_rep1_oc395primer_boost"
    elif args.sample_type == 'clinical_rep1_oc395primer_boost':
        config = get_clinical_sample_concordance_in_lod_oc395primer_boost_rep1(args.snp_type)
        group_by_key = "clinical_rep1_oc395primer_boost"
    elif args.sample_type == 'clinical_rep2_oc395primer_boost':
        config = get_clinical_sample_concordance_in_lod_oc395primer_boost_rep2(args.snp_type)
        group_by_key = "clinical_rep2_oc395primer_boost"
    elif args.sample_type == 'horizon_oc120plus_snps':
        config = get_horizon_concordance_in_lod_oc120plus_snps(args.snp_type)
        group_by_key = "horizon_oc120plus_snps"
    elif args.sample_type == 'clinical_rep1_oc120plus_snps':
        config = get_clinical_sample_concordance_in_lod_oc120plus_snps_rep1(args.snp_type)
        group_by_key = "clinical_rep1_oc120plus_snps"
    elif args.sample_type == 'clinical_rep2_oc120plus_snps':
        config = get_clinical_sample_concordance_in_lod_oc120plus_snps_rep2(args.snp_type)
        group_by_key = "clinical_rep2_oc120plus_snps"
    elif args.sample_type == 'clinical_rep1_oc395snp':
        config = get_clinical_sample_concordance_in_lod_oc395snp_rep1(args.snp_type)
        group_by_key = "clinical_rep1_oc395snp"
    elif args.sample_type == 'clinical_rep2_oc395snp':
        config = get_clinical_sample_concordance_in_lod_oc395snp_rep2(args.snp_type)
        group_by_key = "clinical_rep2_oc395snp"
    elif args.sample_type == 'clinical_rep1_agilent700':
        config = get_clinical_sample_concordance_in_lod_agilent700_rep1(args.snp_type)
        group_by_key = "clinical_rep1_agilent700"
    elif args.sample_type == 'clinical_rep2_agilent700':
        config = get_clinical_sample_concordance_in_lod_agilent700_rep2(args.snp_type)
        group_by_key = "clinical_rep2_agilent700"


    # set these thresholds, we'll still match, but add whether it matched with these thresholds
    key_to_config_usage = 'concordance_lod_samples'
    depth_threshold = config[key_to_config_usage]['depth_threshold']
    vaf_threshold = config[key_to_config_usage]['vaf_threshold']
    matched_results_df = process_all_horizon_samples_for_variants(
        config, group_by_key, vaf_threshold, depth_threshold, key_to_config_usage=key_to_config_usage)

    # The samples, e.g. clinical_rep2_oc120 are diluted with other clinical samples, we can mark them as being
    # in the dilution sample as well, if there was an entry in the config for this
    chrom_gene_aa_change = {}
    if 'dilution_sample_vcf' in config[key_to_config_usage] and config[key_to_config_usage]['dilution_sample_vcf']:
        chrom_gene_aa_change = dictionary_all_aa_changes(
            vcf_file_name=config[key_to_config_usage]['dilution_sample_vcf'])

    matched_results_df['Matched_dilution_sample'] = \
        matched_results_df.apply(lambda row:  _update_matched_sample(row, chrom_gene_aa_change), axis=1)

    subtitle = f"Thresholds: VAF >= {vaf_threshold} Depth >= {depth_threshold}"
    # at this point all data is in matched_results_df, so the actual regression analysis can be completed
    """
    process_regression_and_bland_altman_analysis(df=matched_results_df, group_by_key=group_by_key,
                                                 images_dir=config[key_to_config_usage]['images_dir'],
                                                 config_plot=config[key_to_config_usage]['vaf_regression_plot'],
                                                 subtitle=subtitle)
    """
    matched_results_df.sort_values(by=['Variant', group_by_key], inplace=True)
    print_data_frame_pivot_outputs(df=matched_results_df, args=args, group_by_key=group_by_key)


def _update_matched_sample(row, chrom_gene_aa_change):
    if row.Key in chrom_gene_aa_change:
        return True
    else:
        return False


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Process the limit of detection sample concordance  data')

    parser.add_argument('--tabular_output_file', dest='tabular_output_file',
                        type=str, help='Output file to store the data', required=True)

    parser.add_argument('--snp_type', dest='snp_type',
                        type=str, help='indels (indels) only, or both snps/indels (both)',
                        required=True, default='both')

    sample_types = ['horizon_oc120',
                    'horizon_rep1_oc120plus',
                    'horizon_rep2_oc120plus',
                    'clinical_rep1_oc120',
                    'clinical_rep2_oc120',
                    'clinical_rep1_oc120plus',
                    'clinical_rep2_oc120plus',
                    'horizon_rep1_oc395primer_boost',
                    'clinical_rep1_oc395primer_boost',
                    'clinical_rep2_oc395primer_boost',
                    'horizon_oc120plus_snps',
                    'clinical_rep1_oc120plus_snps',
                    'clinical_rep2_oc120plus_snps',
                    'clinical_rep1_oc395snp',
                    'clinical_rep2_oc395snp',
                    'clinical_rep1_agilent700',
                    'clinical_rep2_agilent700',
                    ]

    sample_types_str = ' / '.join(sample_types)

    parser.add_argument('--sample_type', dest='sample_type', type=str, required=True,
                        help=f'Must be {sample_types}')

    if parser.parse_args().snp_type.lower() in ['both', 'indels']:
        pass
    else:
        print("--snp_type must be either both or indels", file=sys.stderr)
        quit()

    if parser.parse_args().sample_type.lower() in sample_types:
        pass
    else:
        print(f"--sample_type must be either {sample_types_str}", file=sys.stderr)
        quit()

    return parser.parse_args()


if __name__ == "__main__":
    main()
