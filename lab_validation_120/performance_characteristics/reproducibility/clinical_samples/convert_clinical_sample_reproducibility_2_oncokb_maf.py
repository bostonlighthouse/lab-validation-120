"""Simple program to take the outputs from clinical_sample_reproducibility.py.
And get it ready fro Oncokb converting to a MAF file
"""
import argparse
import sys
from lab_validation_120.utils.configs.lab_validation_config import get_oc120plus_new_gene_list_for_variant_plex
from lab_validation_120.utils.vcf_utilities import get_jax_protein_change
import pprint as pp


def main():
    args = get_cli_args()
    process_input_file(args=args)


def process_input_file(args=None):
    """Open the input file and covert to MAF file
    :param args: instance of argparse arguments
    """

    tabular_intput_file_list = args.tabular_intput_file_list.replace('"', '')
    print_maf_header = True
    set_genes_printed = set()  # final set of genes that were printed
    number_times_sample_printed = {}  # how many entries were printed for the samples
    number_times_gene_printed = {}  # how many entries were printed for each gene
    sample_barcodes = {}
    for file in tabular_intput_file_list.split(' '):
        header = []
        with open(file) as infh:
            for line in infh:
                line = line.rstrip()
                if line.startswith("#"):
                    continue
                elif line.startswith("Sample_ID"):
                    header = line.split('\t')
                    continue
                values = line.split('\t')
                gene_printed, sample_barcode = \
                    print_2_maf(values_dict=dict(zip(header, values)), print_maf_header=print_maf_header,
                                concordant_type=args.concordant_discordant,
                                only_new_oc120plus_genes=args.only_new_oc120plus_genes,
                                simplify_barcode_names=args.simplify_barcode_names,
                                sample_barcodes=sample_barcodes)
                if gene_printed is not None:
                    set_genes_printed.add(gene_printed)
                    number_times_gene_printed[gene_printed] = number_times_gene_printed.get(gene_printed, 0) + 1
                    number_times_sample_printed[sample_barcode] = number_times_sample_printed.get(sample_barcode, 0) + 1
                print_maf_header = False  # set to false from now on...

    # print out the number of variant per sample
    [print(f"{k}\t{v}", file=sys.stderr) for k, v in sorted(number_times_sample_printed.items(), key=lambda x: x[1],
                                                            reverse=True)]
    # print out the number of variant per gene
    [print(f"{k}\t{v}", file=sys.stderr) for k, v in sorted(number_times_gene_printed.items(), key=lambda x: x[1],
                                                            reverse=True)]
    # print out some additional details about the genes
    print(f"Final set of genes with variants included {len(set_genes_printed)} genes", file=sys.stderr)
    set_new_gene_oc120plus = set(get_oc120plus_new_gene_list_for_variant_plex())
    print(f"New GS v2 genes that were not found {sorted(set_new_gene_oc120plus.difference(set_genes_printed))}",
          file=sys.stderr)
    if args.simplify_barcode_names is False:
        print("The following below can be used as the input for a generic tumor type of all samples: NSCLC",
              file=sys.stderr)
        print("Do not forget the first line:SAMPLE_ID       ONCOTREE_CODE\n\n", file=sys.stderr)
        print("SAMPLE_ID\tONCOTREE_CODE", file=sys.stderr)
        for k, v in sample_barcodes.items():
            print(f"{k}\t{v}", file=sys.stderr)


def print_2_maf(values_dict=None, print_maf_header=None, concordant_type=None, only_new_oc120plus_genes=None,
                simplify_barcode_names=False, sample_barcodes=None):
    """
    Take in the  dictionary of values, and the print out the MAF file entry (on line)
    :param values_dict:  The dictionary of values needed to print out the MAF data
    :param print_maf_header: Boolean on whether to print out the header
    :param concordant_type: What type of concordance are we looking for?
    :param only_new_oc120plus_genes: Keep only genes in the oc120plus list
    :param simplify_barcode_names: Boolean on whether to give all cids the same name, easy to annotate OncoKB, default False
    :param sample_barcodes: A dictionary of barcodes that have been seen, so that we ccan print out a OncoKB -c file
    """
    if 'Position' not in values_dict:
        print('No position, are the format of the file correct ', values_dict)
        quit()
    start = int(values_dict['Position'])
    if print_maf_header is True:
        # INTERNAL_BIOMARKER = since we do not use HGVSp for searching oncoKB and instead use genomic coordinates
        # it's usefull to keep the biomarker to map back when there's an issue
        print("\t".join(("NCBI_Build", "Hugo_Symbol", "Variant_Classification", "Tumor_Sample_Barcode",
                         "Chromosome", "Start_Position", "End_Position", "Reference_Allele",
                         "Tumor_Seq_Allele1", "Tumor_Seq_Allele2", "t_depth", "t_alt_count", "INTERNAL_BIOMARKER",
                         "HGVSP")))
    ncbi_build = 'GRCh37'
    hugo_symbol = values_dict['SYMBOL']
    variant_classification = _get_list_consequences(consequence=values_dict['Consequence'])
    tumor_sample_barcode, t_alt_count, t_depth = _get_values_based_on_concordance_type(values_dict=values_dict,
                                                                                       concordant_type=concordant_type)

    hgvs_p = values_dict['HGVSp']
    hgvs_c = values_dict['HGVSc']
    # was it not ND and then see if it was delimited
    if hgvs_p != 'ND':
        if ":" in hgvs_p:
            hgvs_p = hgvs_p.split(":")[1].replace('p.', '')
    # if it is ND, then just leave it blank
    elif hgvs_p == 'ND':
        hgvs_p = ''

    # was it not ND and then see if it was delimited
    if hgvs_c != 'ND':
        if ":" in hgvs_c:
            hgvs_c = hgvs_c.split(":")[1]

    hgvs_g, ref_allele, alt_allele, start, end = _get_hgvs_g(chrom=values_dict['Chromosome'],
                                                             start=start,
                                                             ref_allele=values_dict['Ref_allele'],
                                                             alt_allele=values_dict['Alt_allele'])

    print_variant_record = False  # will this fit the criteria for printing
    # are we looking for concordance in both, or are we looking for discordance, or we just want postivies
    # both is concordant in both
    if concordant_type == 'both':
        if values_dict['REP1'] == '+' and values_dict['REP2'] == '+':
            print_variant_record = True
    # Found in the first sample but not the second sample
    elif concordant_type == 'first':
        if values_dict['REP1'] == '+' and values_dict['REP2'] == '-':
            print_variant_record = True
    # Found in the second sample but not the first sample
    elif concordant_type == 'second':
        if values_dict['REP1'] == '-' and values_dict['REP2'] == '+':
            print_variant_record = True
    # Found in the first sample, don't care about the second
    elif concordant_type == 'first_positive':
        if values_dict['REP1'] == '+':
            print_variant_record = True
    # Found in the second sample, don't care about the first
    elif concordant_type == 'second_positive':
        if values_dict['REP2'] == '+':
            print_variant_record = True
    elif concordant_type == 'negative_either':
        if values_dict['REP1'] == '-' or values_dict['REP2'] == '-':
            print_variant_record = True
    # quick update to the barcode to remove the internal sample number from the cid
    if simplify_barcode_names is True:
        tumor_sample_barcode = "Sample_name"
    elif '-' in tumor_sample_barcode and 'cid' in tumor_sample_barcode:
        _, cid, numerical_val = tumor_sample_barcode.split('-')
        tumor_sample_barcode = '-'.join((cid, numerical_val))
        sample_barcodes[tumor_sample_barcode] = sample_barcodes.get(tumor_sample_barcode, 'NSCLC')
    elif 'POE' in tumor_sample_barcode:  # for genosity data
        sample_barcodes[tumor_sample_barcode] = sample_barcodes.get(tumor_sample_barcode, 'NSCLC')

    print_str = ("\t".join(str(v) for v in (ncbi_build, hugo_symbol, variant_classification[0], tumor_sample_barcode,
                                            values_dict['Chromosome'], str(start), str(end),
                                            ref_allele, alt_allele, alt_allele, t_depth, t_alt_count, hgvs_p,
                                            get_jax_protein_change(hgvs_p))))

    # printed out into the MAF file, not taking into account the new genes in oc120plus, and variant matched above
    if only_new_oc120plus_genes is False and print_variant_record is True:
        print(print_str)
        return hugo_symbol, tumor_sample_barcode
    # printed out into the MAF file, TAKING into account the new genes in oc120plus, and variant matched above
    elif only_new_oc120plus_genes is True and print_variant_record is True:
        if hugo_symbol in get_oc120plus_new_gene_list_for_variant_plex():
            print(print_str)
            return hugo_symbol, tumor_sample_barcode
    # No printing was done
    return None, None


def _get_values_based_on_concordance_type(values_dict=None, concordant_type=None):
    """
    Take in the  dictionary of values, and the print out the MAF file entry (on line)
    :param values_dict:  The dictionary of values needed to print out the MAF data
    :param concordant_type: What type of concordance are we looking for?
    """
    # are we looking for concordance in both, or are we looking for discordance
    # either way set to the second samples values, and only set to first if it was called out
    tumor_sample_barcode = values_dict['Sample_cid2']
    t_depth = values_dict['Allele_count2']
    t_alt_count = values_dict['Depth2']
    # only switch this if we are doing discordance in first
    if concordant_type == 'first':
        tumor_sample_barcode = values_dict['Sample_cid1']
        t_depth = values_dict['Allele_count1']
        t_alt_count = values_dict['Depth1']

    return tumor_sample_barcode, t_alt_count, t_depth


def _get_list_consequences(consequence=None):
    """
    Just return the list of consequences from the string
    :param consequence: String to be parsed
    """
    consequence = consequence.replace('[', '').replace(']', '').replace(' ', '').replace("'", '')
    return consequence.split(',')


def _get_hgvs_g(chrom=None, start=None, ref_allele=None, alt_allele=None):
    """
    :param chrom: Chromosome str
    :param start: Start of variant int
    :param ref_allele: Reference allele str
    :param alt_allele: Alternate allele str
    """
    # TODO, if we need hgvs_g to work, more work is required b/c of normalization
    # https://mutalyzer.nl/name-checker?description=NC_000005.9%3Ag.56177849_56177851del
    # NC_000005.9:g.56177849_56177851del      5       56177849        56177851        CAA     -       -
    # for now just use c.dot, and use the start and stop from here...
    #chrom = covert_chromosome_grch37_accession(chrom=chrom)
    #print(f"before {start} {ref_allele} {alt_allele}")
    hgvs_g = ''
    end = -1
    if len(ref_allele) == 1 and len(alt_allele) == 1:  # e.g. 3:g.178936082G>A
        end = start
        hgvs_g = f"{chrom}:g.{start}{ref_allele}>{alt_allele}"
    elif len(ref_allele) > len(alt_allele):   # CA>A or GTT>G in VCF DELETION
        ref_allele = ref_allele[1:]
        alt_allele = '-'
        start = start + 1
        end = (start + len(ref_allele)) - 1
        hgvs_g = f"{chrom}:g.{start}_{end}del"

    elif len(ref_allele) < len(alt_allele):   # T>TC in VCF INSERTION
        alt_allele = alt_allele[1:]
        ref_allele = '-'
        end = start + 1  #  start remains the same and end is just + 1
        hgvs_g = f"{chrom}:g.{start}_{end}ins{alt_allele}"
    else:
        print(f"Missed this example {chrom} {start} {ref_allele} {alt_allele}")
        quit()

    return hgvs_g, ref_allele, alt_allele, start, end


def covert_chromosome_grch37_accession(chrom=None):
    """Simple map to return the accession for the chromosome
    https://www.ncbi.nlm.nih.gov/assembly/GCF_000001405.13/
    """
    chrom_map = {
        "1": "NC_000001.10",
        "2": "NC_000002.11",
        "3": "NC_000003.11",
        "4": "NC_000004.11",
        "5": "NC_000005.9",
        "6": "NC_000006.11",
        "7": "NC_000007.13",
        "8": "NC_000008.10",
        "9": "NC_000009.11",
        "10": "NC_000010.10",
        "11": "NC_000011.9",
        "12": "NC_000012.11",
        "13": "NC_000013.10",
        "14": "NC_000014.8",
        "15": "NC_000015.9",
        "16": "NC_000016.9",
        "17": "NC_000017.10",
        "18": "NC_000018.9",
        "19": "NC_000019.9",
        "20": "NC_000020.10",
        "21": "NC_000021.8",
        "22": "NC_000022.10",
        "X": "NC_000023.10",
        "Y": "NC_000024.9",
    }
    return chrom_map[chrom]


def get_cli_args():
        """
        void: get_cli_args()
        Takes: no arguments
        Returns: instance of argparse arguments
        """
        parser = argparse.ArgumentParser(description='take the outputs from clinical_sample_reproducibility.py. '
                                                     'and get it ready fro Oncokb by converting to a MAF file')

        parser.add_argument('--tabular_intput_file_list', dest='tabular_intput_file_list',
                            type=str, help='Input file to convert to MAF', required=True)

        parser.add_argument('--concordant_discordant', dest='concordant_discordant',
                            type=str,
                            help='first means it was found '
                            'in the "first" sample but discordant in the second, and "second" is opposite, '
                            '"first_positive means it was positive in the first regardless of second, and '
                            '"second_positive means it was positive in the second regardless of the first',
                            required=True,
                            choices=['both', 'first', 'second', 'first_positive', 'second_positive', 'negative_either'])
        parser.add_argument('--simplify_barcode_names', dest='simplify_barcode_names', default=False, action='store_true',
                            help="Give all tumor names the same to make it easy to run Oncokb")

        parser.add_argument('--only_new_oc120plus_genes', action='store_true')
        return parser.parse_args()


if __name__ == '__main__':
    main()
