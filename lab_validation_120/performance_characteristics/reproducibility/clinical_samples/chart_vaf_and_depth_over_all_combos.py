import argparse

from lab_validation_120.utils.configs.oc395.oc395_oc395primer_boost_equiv_samples_config import \
    get_oc395_oc395primer_boost_reproducibility_11_samples, \
    get_oc395_oc395primer_boost_equiv_htqc_plots_11_samples

from lab_validation_120.utils.configs.oc395.oc395primer_boost_all_samples_config import \
    get_oc395primer_boost_htqc_plots_all_33_samples

from lab_validation_120.utils.configs.oc395.oc395primer_boost_vs_oc120plus_config import \
    get_oc395primer_boost_vs_oc120plus_htqc_plots_32_samples, \
    get_oc395primer_boost_vs_oc120plus_reproducibility_32_samples, \
    get_oc395primer_boost_vs_oc120plus_rerun_htqc_plots_16_samples, \
    get_oc395primer_boost_vs_oc120plus_rerun_reproducibility_16_samples, \
    get_oc395primer_boost_vs_oc395primer_boost_rerun_htqc_plots_16_samples

from lab_validation_120.utils.configs.oc120_vs_oc120plus_reproducibility_config import \
    get_oc120_vs_oc120plus_sample_reproducibility_oc120_bed_file, \
    get_oc120_vs_oc120plus_sample_reproducibility_htqc_plots_oc120_bed_file, \
    get_oc120_vs_oc120plus_sample_reproducibility_htqc_plots_oc120plus_bed_file, \
    get_oc120plus_equivalency_samples_for_htqc_plots_using_oc120plus_bed_file

from lab_validation_120.utils.configs.oc120plus_midoutput_vs_highoutput_flowcell_config import \
    get_oc120plus_midoutput_vs_highoutput_flow_cell_reproducibility_32_samples, \
    get_oc120plus_midoutput_vs_highoutput_flow_cell_htqc_plots_32_samples, \
    get_oc120plus_midoutput_vs_highoutput_flow_cell_reproducibility_24_samples, \
    get_oc120plus_midoutput_vs_highoutput_flow_cell_htqc_plots_24_samples

from lab_validation_120.utils.configs.oc120plus_qc_repeat_sample_config import \
    get_oc120plus_qc_repeat_sample_reproducibility, get_oc120plus_qc_repeat_sample_htqc_plots

from lab_validation_120.utils.plots.regression_plot_utilities import process_regressions_from_reproducibility_output
from lab_validation_120.utils.plots.boxplot_plot_utilities import process_boxplots_from_reproducibility_output, \
    process_boxplot_analysis_for_htqc_intervals_for_gene_list
from lab_validation_120.utils.concordance_utilites import get_overall_stem_plot_concordance_df
from lab_validation_120.utils.concordance_qc_metrics import process_concordance_htqc_plots
from lab_validation_120.utils.plots.stem_plot_utilities import reproducibility_stem_plot
from lab_validation_120.utils.configs.lab_validation_config import get_oc120plus_new_gene_list_for_variant_plex
from lab_validation_120.utils.htqc_utilites import output_normalized_gene_counts_by_sample
from lab_validation_120.utils.configs.oc120plus_snps.oc120plus_vs_oc120plus_snps_config_24_original_samples import \
    get_oc120plus_vs_oc120plus_snps_reproducibility_24_samples, \
    get_oc120plus_vs_oc120plus_snps_htqc_plots_24_samples
from lab_validation_120.utils.configs.oc120plus_without_snps.oc120plus_vs_oc120plus_without_snps_config import \
    get_oc120plus_vs_oc120plus_without_snps_reproducibility_11_samples, \
    get_oc120plus_vs_oc120plus_without_snps_htqc_plots_11_samples
from lab_validation_120.utils.configs.oc120plus_snps.oc120plus_vs_oc120plus_snps_config_11_resequenced_samples import \
    get_oc120plus_vs_oc120plus_snps_reproducibility_11_resequenced_samples, \
    get_oc120plus_vs_oc120plus_snps_htqc_plots_11_resequenced_samples

from lab_validation_120.utils.configs.oc395.oc395_vs_oc120plus_validation_config import \
    get_oc395_vs_oc120plus_reproducibility_samples, get_oc395_vs_oc120plus_htqc_plots

from lab_validation_120.utils.configs.oc395.oc395_reproduciblity_config import \
    get_oc395_reproducibility, get_oc395_reproducibility_htqc_plots

from lab_validation_120.utils.configs.oc395.oc395_genosity_config import \
    get_genosity_vs_oc395boosted_primer_sample_recall_all_samples, \
    get_genosity_vs_oc395boosted_primer_sample_recall_all_samples_no_germline

from lab_validation_120.utils.configs.oc395_snps.oc395snps_reproducibility_config import get_oc395snps_reproducibility, \
    get_oc395snps_reproducibility_htqc_plots

from lab_validation_120.utils.configs.oc395_snps.oc395snps_pon import get_oc395_pon_vs_oc395snps_pon_htqc_plots, \
    get_oc395snps_pon_htqc_plots

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples \
    import get_hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples_reproducibility, \
    get_hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples_htqc_plots

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc395_4_samples_vs_original_oc395_4_samples import \
    get_hybrid_oc395_4_samples_vs_original_oc395_4_samples_reproducibility, \
    get_hybrid_oc395_4_samples_vs_original_oc395_4_samples_htqc_plots

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples \
    import get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility, \
    get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_htqc_plots

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run2 \
    import get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run2, \
    get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_htqc_plots_run2

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3 \
    import get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run3, \
    get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_htqc_plots_run3

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc395_6_samples_vs_original_oc395_6_samples import \
    get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility, \
    get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_htqc_plots

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3 import \
    get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility_run3, \
    get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_htqc_plots_run3


from lab_validation_120.utils.configs.agilent700.agilent700_reproducibility_config import \
    get_agilent700_vs_oc180_reproducibility, get_agilent700_vs_oc395_reproducibility, \
    get_agilent700_vs_oc180_reproducibility_htqc_plots, get_agilent700_vs_oc395_reproducibility_htqc_plots


def main():
    """Main driver of the program"""

    args = get_cli_args()
    df_file_prefix = args.which_analysis
    config = None
    # DO NOT RUN THIS WITH THE OC120Plus bedfile, since we did an original comparision
    if args.which_analysis == 'oc120_vs_oc120plus_oc120_bed':
        config = get_oc120_vs_oc120plus_sample_reproducibility_oc120_bed_file()
        overall_output_file_to_parse = \
            config['clinical_sample_reproducibility']['final_overall_reproducibility_file']
        images_dir = config['clinical_sample_reproducibility']['images_dir']
        # get the values of concordance here
        df_overall_stem_plot_data = get_overall_stem_plot_concordance_df(file=overall_output_file_to_parse,
                                                                         equivalency_study=args.equivalency_study)
        consequential_impacts_only = args.consequential_impacts_only
        #consequential_impacts_only = config['clinical_sample_reproducibility']['consequential_impacts_only']

        # plotting of stem plots, uses e.g. file:
        # lab_validation_120/performance_characteristics/reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_120plus_all_values.txt
        reproducibility_stem_plot(df=df_overall_stem_plot_data, images_dir=images_dir, variant_type='snv',
                                  equivalency_study=args.equivalency_study)
        reproducibility_stem_plot(df=df_overall_stem_plot_data, images_dir=images_dir, variant_type='indel',
                                  equivalency_study=args.equivalency_study)
        # plot the box plots of reproducibility
        # used files compiles here:
        # clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_1.0_depth_50_consequential_impacts_only_indels_size_1_1_both_inter_and_intra.txt
        process_boxplots_from_reproducibility_output(config,
                                                     key_to_search='clinical_sample_reproducibility',
                                                     consequential_impacts_only=consequential_impacts_only,
                                                     equivalency_study=args.equivalency_study)

        # plot the regression, used the VCF files
        process_regressions_from_reproducibility_output(config=config, key_to_search='clinical_sample_reproducibility',
                                                        consequential_impacts_only=consequential_impacts_only,
                                                        alpha=0.6, df_file_prefix=df_file_prefix,
                                                        plot_regressions_for_individual_sample=False)
    elif args.which_analysis == 'oc120plus_flow_cell_comparison_24_samples':
        config = get_oc120plus_midoutput_vs_highoutput_flow_cell_reproducibility_24_samples()
    elif args.which_analysis == 'oc120plus_flow_cell_comparison_32_samples':
        config = get_oc120plus_midoutput_vs_highoutput_flow_cell_reproducibility_32_samples()
    elif args.which_analysis == 'oc120plus_qc_repeat_samples':
        config = get_oc120plus_qc_repeat_sample_reproducibility()
    elif args.which_analysis == 'oc395_oc395primer_boost_11_samples':
        config = get_oc395_oc395primer_boost_reproducibility_11_samples()
    elif args.which_analysis == 'oc120plus_vs_oc120plus_snps_24_samples':
        config = get_oc120plus_vs_oc120plus_snps_reproducibility_24_samples()
    elif args.which_analysis == 'oc120plus_vs_oc120plus_snps_11_resequenced_samples':
        config = get_oc120plus_vs_oc120plus_snps_reproducibility_11_resequenced_samples()
    elif args.which_analysis == 'oc120plus_vs_oc120plus_without_snps':
        config = get_oc120plus_vs_oc120plus_without_snps_reproducibility_11_samples()
    elif args.which_analysis == 'oc395primer_boost_vs_oc120plus_32_samples':
        config = get_oc395primer_boost_vs_oc120plus_reproducibility_32_samples()
    elif args.which_analysis == 'oc395primer_boost_vs_oc120plus_rerun_16_samples':
        config = get_oc395primer_boost_vs_oc120plus_rerun_reproducibility_16_samples()
    elif args.which_analysis == 'oc395_primer_boost_vs_oc120plus_validation':
        config = get_oc395_vs_oc120plus_reproducibility_samples()
    elif args.which_analysis == 'oc395_primer_boost_vs_genosity':
        config = get_genosity_vs_oc395boosted_primer_sample_recall_all_samples()
    elif args.which_analysis == 'oc395_primer_boost_vs_genosity_no_germline':
        config = get_genosity_vs_oc395boosted_primer_sample_recall_all_samples_no_germline()
    elif args.which_analysis == 'oc395_primer_boost_reproducibility':
        config = get_oc395_reproducibility()
    elif args.which_analysis == 'oc395snps_vs_oc120plus':
        config = get_oc395snps_reproducibility()
    elif args.which_analysis == 'hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples':
        config = get_hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples_reproducibility()
    elif args.which_analysis == 'hybrid_oc395_4_samples_vs_original_oc395_4_samples':
        config = get_hybrid_oc395_4_samples_vs_original_oc395_4_samples_reproducibility()
    elif args.which_analysis == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples':
        config = get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility()
    elif args.which_analysis == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run2':
        config = get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run2()
    elif args.which_analysis == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3':
        config = get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run3()
    elif args.which_analysis == 'hybrid_oc395_6_samples_vs_original_oc395_6_samples':
        config = get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility()
    elif args.which_analysis == 'hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3':
        config = get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility_run3()
    elif args.which_analysis == 'agilent700_vs_oc180':
        config = get_agilent700_vs_oc180_reproducibility()
    elif args.which_analysis == 'agilent700_vs_oc395':
        config = get_agilent700_vs_oc395_reproducibility()


    if args.which_analysis == 'oc120plus_flow_cell_comparison_24_samples' or \
            args.which_analysis == 'oc120plus_flow_cell_comparison_32_samples' or \
            args.which_analysis == 'oc120plus_qc_repeat_samples' or \
            args.which_analysis == 'oc395_oc395primer_boost_11_samples' or \
            args.which_analysis == 'oc120plus_vs_oc120plus_snps_24_samples' or \
            args.which_analysis == 'oc120plus_vs_oc120plus_snps_11_resequenced_samples' or \
            args.which_analysis == 'oc120plus_vs_oc120plus_without_snps' or \
            args.which_analysis == 'oc395primer_boost_vs_oc120plus_32_samples' or \
            args.which_analysis == 'oc395_primer_boost_vs_oc120plus_validation' or \
            args.which_analysis == 'oc395_primer_boost_vs_genosity' or \
            args.which_analysis == 'oc395_primer_boost_vs_genosity_no_germline' or \
            args.which_analysis == 'oc395_primer_boost_reproducibility' or \
            args.which_analysis == 'oc395snps_vs_oc120plus' or \
            args.which_analysis == 'hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples' or \
            args.which_analysis == 'hybrid_oc395_4_samples_vs_original_oc395_4_samples' or \
            args.which_analysis == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples' or \
            args.which_analysis == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run2' or \
            args.which_analysis == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3' or \
            args.which_analysis == 'hybrid_oc395_6_samples_vs_original_oc395_6_samples' or \
            args.which_analysis == 'hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3' or \
            args.which_analysis == 'agilent700_vs_oc180' or  \
            args.which_analysis == 'agilent700_vs_oc395':  # never did run2 analysis
        images_dir = config['clinical_sample_reproducibility']['images_dir']
        consequential_impacts_only = args.consequential_impacts_only

        # plot the box plots of reproducibility
        # used files compiles here:
        # clinical_sample_reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_oc120plus_50samples_vaf_1.0_depth_50_consequential_impacts_only_indels_size_1_1_both_inter_and_intra.txt
        #process_boxplots_from_reproducibility_output(config,
        #                                             key_to_search='clinical_sample_reproducibility',
        #                                             consequential_impacts_only=consequential_impacts_only,
        #                                             equivalency_study=args.equivalency_study)

        # plot the regression, used the VCF files
        process_regressions_from_reproducibility_output(config=config, key_to_search='clinical_sample_reproducibility',
                                                        consequential_impacts_only=consequential_impacts_only,
                                                        alpha=0.6, df_file_prefix=df_file_prefix,
                                                        plot_regressions_for_individual_sample=False)

    # Now run HTQC related metrics
    key_to_config_usage = 'oc120_vs_oc120plus_samples_reproducibility_qc_plots'  # what key to walk into
    col_name_4_sample_names = 'OC120_vs_OC120PLUS_SAMPLES'
    name_for_all_samples_combined = "all_oc120_vs_120plus_samples"
    only_high_level_metrics = True  # only care about high-level when comparing oc120 vs oc120 plus
    htqc_barchart_sort_x_by_value = False
    htqc_boxchart_sort_x_by_value = False
    alternate_bar_chart_colors = False
    htqc_boxchart_figsize_width = 30
    htqc_barchart_figsize_width = 30
    if args.which_analysis == 'oc120_vs_oc120plus_oc120_bed':
        config = get_oc120_vs_oc120plus_sample_reproducibility_htqc_plots_oc120_bed_file()
        alternate_bar_chart_colors = True
    elif args.which_analysis == 'oc120_vs_oc120plus_oc120plus_bed':
        config = get_oc120_vs_oc120plus_sample_reproducibility_htqc_plots_oc120plus_bed_file()
        alternate_bar_chart_colors = True
    elif args.which_analysis == 'equivalency_study_oc120plus_samples': # sample data for only oc120plus
        config = get_oc120plus_equivalency_samples_for_htqc_plots_using_oc120plus_bed_file()
        col_name_4_sample_names = 'OC120PLUS_EQUIV_SAMPLES'
        name_for_all_samples_combined = "oc120plus_equiv_samples"
        htqc_barchart_sort_x_by_value = True
        htqc_boxchart_sort_x_by_value = True
        key_to_config_usage = 'oc120plus_equivalency_qc_plots'
    elif args.which_analysis == 'equivalency_study_oc120_samples': # sample data for only oc120plus
        config = get_oc120plus_vs_oc120plus_snps_htqc_plots_24_samples()
        col_name_4_sample_names = 'OC120_EQUIV_SAMPLES'
        name_for_all_samples_combined = "oc120_equiv_samples"
        htqc_barchart_sort_x_by_value = True
        htqc_boxchart_sort_x_by_value = True
        key_to_config_usage = 'oc120_equivalency_qc_plots'
    elif args.which_analysis == 'oc120plus_flow_cell_comparison_24_samples':
        config = get_oc120plus_midoutput_vs_highoutput_flow_cell_htqc_plots_24_samples()
        col_name_4_sample_names = 'GS180 Flow Cell Comparison 24 Samples'
        name_for_all_samples_combined = "oc120plus_flow_cell_comparison_24_samples"
        alternate_bar_chart_colors = True
        key_to_config_usage = 'oc120plus_mid_output_vs_high_output_flow_cell_24_samples'
    elif args.which_analysis == 'oc120plus_flow_cell_comparison_32_samples':
        config = get_oc120plus_midoutput_vs_highoutput_flow_cell_htqc_plots_32_samples()
        col_name_4_sample_names = 'GS180 Flow Cell Comparison 32 Samples'
        name_for_all_samples_combined = "oc120plus_flow_cell_comparison_32_samples"
        alternate_bar_chart_colors = True
        key_to_config_usage = 'oc120plus_mid_output_vs_high_output_flow_cell_32_samples'
    elif args.which_analysis == 'oc120plus_qc_repeat_samples':
        config = get_oc120plus_qc_repeat_sample_htqc_plots()
        col_name_4_sample_names = 'GS180 Repeat Sample Comparison Samples'
        name_for_all_samples_combined = "oc120plus_qc_repeat_samples"
        alternate_bar_chart_colors = True
        key_to_config_usage = 'oc120plus_qc_repeat_samples'
    elif args.which_analysis == 'oc395_oc395primer_boost_11_samples':
        config = get_oc395_oc395primer_boost_equiv_htqc_plots_11_samples()
        col_name_4_sample_names = 'GS395 primer boost comparison'
        name_for_all_samples_combined = "gs395_primer_boost_comparison"
        alternate_bar_chart_colors = True
        key_to_config_usage = 'oc395_oc395primer_boost_equiv_htqc_plots_11_samples'
    elif args.which_analysis == 'oc395primer_boost_33_samples':
        config = get_oc395primer_boost_htqc_plots_all_33_samples()
        col_name_4_sample_names = 'GS395 primer boost - All Samples'
        name_for_all_samples_combined = "gs395_primer_boost_all_samples"
        alternate_bar_chart_colors = False
        htqc_barchart_sort_x_by_value = True
        htqc_boxchart_sort_x_by_value = True
        key_to_config_usage = 'oc395primer_boost_htqc_plots_all_33_samples'
        only_high_level_metrics = False  # only care about high-level when comparing oc120 vs oc120 plus
    elif args.which_analysis == 'oc120plus_vs_oc120plus_snps_24_samples':
        config = get_oc120plus_vs_oc120plus_snps_htqc_plots_24_samples()
        col_name_4_sample_names = 'GS180 vs GS180 SNPs - All 24 Samples'
        name_for_all_samples_combined = "gs180_vs_gs180_snps_24_samples"
        key_to_config_usage = 'oc120plus_vs_oc120plus_snps'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
    elif args.which_analysis == 'oc120plus_vs_oc120plus_snps_11_resequenced_samples':
        config = get_oc120plus_vs_oc120plus_snps_htqc_plots_11_resequenced_samples()
        col_name_4_sample_names = 'GS180 vs GS180 SNPs - All 11 re-sequenced Samples'
        name_for_all_samples_combined = "gs180_vs_gs180_snps_11_resequenced"
        key_to_config_usage = 'oc120plus_vs_oc120plus_snps_11_resequenced_samples'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
    elif args.which_analysis == 'oc120plus_vs_oc120plus_without_snps':
        config = get_oc120plus_vs_oc120plus_without_snps_htqc_plots_11_samples()
        col_name_4_sample_names = 'GS180 vs GS180 without SNPs - All Samples'
        name_for_all_samples_combined = "gs180_vs_gs180_without_snps"
        key_to_config_usage = 'oc120plus_vs_oc120plus_without_snps'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
    elif args.which_analysis == 'oc395primer_boost_vs_oc120plus_32_samples':
        config = get_oc395primer_boost_vs_oc120plus_htqc_plots_32_samples()
        col_name_4_sample_names = 'GS395 primer boost vs GS180 - 32 Samples'
        name_for_all_samples_combined = "gs395_boosted_vs_gs180"
        key_to_config_usage = 'oc395primer_boost_vs_oc120plus_reproducibility_32_samples'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
    elif args.which_analysis == 'oc395primer_boost_vs_oc120plus_rerun_16_samples':
        config = get_oc395primer_boost_vs_oc120plus_rerun_htqc_plots_16_samples()
        col_name_4_sample_names = 'GS395 primer boost vs GS180 RERUN - 16 Samples'
        name_for_all_samples_combined = "gs395_boosted_vs_gs180"
        key_to_config_usage = 'oc395primer_boost_vs_oc120plus_rerun_reproducibility_16_samples'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
    elif args.which_analysis == 'oc395primer_boost_vs_oc395primer_boost_rerun_16_samples':
        config = get_oc395primer_boost_vs_oc395primer_boost_rerun_htqc_plots_16_samples()
        col_name_4_sample_names = 'GS395 primer boost vs GS395 primer boost RERUN - 16 Samples'
        name_for_all_samples_combined = "gs395_boosted_vs_gs395_boosted_rerun"
        key_to_config_usage = 'oc395primer_boost_vs_oc395primer_boost_rerun_reproducibility_16_samples'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
    elif args.which_analysis == 'oc395_primer_boost_vs_oc120plus_validation':
        config = get_oc395_vs_oc120plus_htqc_plots()
        col_name_4_sample_names = 'GS395 vs GS180 Validation'
        name_for_all_samples_combined = "oc395_vs_oc120plus"
        key_to_config_usage = 'oc395_vs_oc120plus'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
    elif args.which_analysis == 'oc395_primer_boost_reproducibility':
        config = get_oc395_reproducibility_htqc_plots()
        col_name_4_sample_names = 'GS395 REP1 vs GS395 REP2'
        name_for_all_samples_combined = "oc395_rep1_vs_oc395_rep2"
        key_to_config_usage = 'oc395_reproducibility'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
    elif args.which_analysis == 'oc395snps_vs_oc120plus':
        config = get_oc395snps_reproducibility_htqc_plots()
        col_name_4_sample_names = 'GS395 SNPs vs GS180'
        name_for_all_samples_combined = "oc395_snps_vs_oc180"
        key_to_config_usage = 'oc395_snps_reproducibility'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
    elif args.which_analysis == 'oc395_pon_vs_oc395snps_pon':
        config = get_oc395_pon_vs_oc395snps_pon_htqc_plots()
        col_name_4_sample_names = 'GS395 PoN vs GS395 SNPs PoN'
        name_for_all_samples_combined = "oc395_pon_vs_oc395_snps_pon"
        key_to_config_usage = 'oc395_pon_vs_oc395_snps_pon'
        alternate_bar_chart_colors = True
        only_high_level_metrics = True
        htqc_boxchart_figsize_width = 45
        htqc_barchart_figsize_width = 45
    elif args.which_analysis == 'oc395snps_pon':
        config = get_oc395snps_pon_htqc_plots()
        col_name_4_sample_names = 'GS395 SNPs PoN'
        name_for_all_samples_combined = "oc395_snps_pon"
        key_to_config_usage = 'oc395_snps_pon'
        alternate_bar_chart_colors = False
        only_high_level_metrics = True
        htqc_boxchart_figsize_width = 40
        htqc_barchart_figsize_width = 40
        htqc_barchart_sort_x_by_value = True
        htqc_boxchart_sort_x_by_value = True
    elif args.which_analysis == 'hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples':
        config = get_hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples_htqc_plots()
        col_name_4_sample_names = 'GS180 Hybrid'
        name_for_all_samples_combined = "hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples_reproducibility"
        key_to_config_usage = 'hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples_reproducibility'
        htqc_boxchart_figsize_width = 50
        htqc_barchart_figsize_width = 50
        alternate_bar_chart_colors = True
    elif args.which_analysis == 'hybrid_oc395_4_samples_vs_original_oc395_4_samples':
        config = get_hybrid_oc395_4_samples_vs_original_oc395_4_samples_htqc_plots()
        col_name_4_sample_names = 'GS395 Hybrid'
        name_for_all_samples_combined = "hybrid_oc1395_4_samples_vs_original_oc395_4_samples_reproducibility"
        key_to_config_usage = 'hybrid_oc1395_4_samples_vs_original_oc395_4_samples_reproducibility'
        alternate_bar_chart_colors = True

    elif args.which_analysis == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples':
        config = get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_htqc_plots()
        col_name_4_sample_names = 'GS180 Hybrid'
        name_for_all_samples_combined = "hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility"
        key_to_config_usage = 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility'
        htqc_boxchart_figsize_width = 50
        htqc_barchart_figsize_width = 50
        alternate_bar_chart_colors = True

    elif args.which_analysis == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run2':
        config = get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_htqc_plots_run2()
        col_name_4_sample_names = 'GS180 Hybrid run2'
        name_for_all_samples_combined = "hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run2"
        key_to_config_usage = 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run2'
        htqc_boxchart_figsize_width = 50
        htqc_barchart_figsize_width = 50
        alternate_bar_chart_colors = True

    elif args.which_analysis == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3':
        config = get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_htqc_plots_run3()
        col_name_4_sample_names = 'GS180 Hybrid run3'
        name_for_all_samples_combined = "hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run3"
        key_to_config_usage = 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run3'
        htqc_boxchart_figsize_width = 50
        htqc_barchart_figsize_width = 50
        alternate_bar_chart_colors = True

    elif args.which_analysis == 'hybrid_oc395_6_samples_vs_original_oc395_6_samples':
        config = get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_htqc_plots()
        col_name_4_sample_names = 'GS395 Hybrid'
        name_for_all_samples_combined = "hybrid_oc1395_6_samples_vs_original_oc395_6_samples_reproducibility"
        key_to_config_usage = 'hybrid_oc1395_6_samples_vs_original_oc395_6_samples_reproducibility'
        alternate_bar_chart_colors = True

    elif args.which_analysis == 'hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3':
        config = get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_htqc_plots_run3()
        col_name_4_sample_names = 'GS395 Hybrid run3'
        name_for_all_samples_combined = "hybrid_oc1395_6_samples_vs_original_oc395_6_samples_reproducibility_run3"
        key_to_config_usage = 'hybrid_oc1395_6_samples_vs_original_oc395_6_samples_reproducibility_run3'
        alternate_bar_chart_colors = True

    elif args.which_analysis == 'agilent700_vs_oc180':
        config = get_agilent700_vs_oc180_reproducibility_htqc_plots()
        col_name_4_sample_names = 'Agilent Run vs oc180'
        name_for_all_samples_combined = "agilent700_vs_oc180"
        key_to_config_usage = 'agilent700_vs_oc180_reproducibility'
        htqc_boxchart_figsize_width = 45
        htqc_barchart_figsize_width = 45
        alternate_bar_chart_colors = True

    elif args.which_analysis == 'agilent700_vs_oc395':
        config = get_agilent700_vs_oc395_reproducibility_htqc_plots()
        col_name_4_sample_names = 'Agilent Run vs oc395'
        name_for_all_samples_combined = "agilent700_vs_oc395"
        key_to_config_usage = 'agilent700_vs_oc395_reproducibility'
        alternate_bar_chart_colors = True
        htqc_boxchart_figsize_width = 15
        htqc_barchart_figsize_width = 15

    else:
        quit(f"HTQC analysis not completed for this analysis {args.which_analysis}....")
        #config = get_concordance_clinical_samples_reproducibility_htqc_plots()
        #key_to_config_usage = 'clinical_samples_reproducibility_qc_plots'  # what key to walk into
        #col_name_4_sample_names = 'CLINICAL_SAMPLES'
        #name_for_all_samples_combined = "all_rap_samples"
        #only_high_level_metrics = False

    # if value are None here then just dont use limits on plotting, see process_concordance_htqc_plots
    list_of_heatmap_values = [("MEAN_COLLAPSED_COVERAGE", None, None),
                              ("MEAN_FILTERED_COVERAGE", None, None),
                              ("MEAN_ABS_COVERAGE", None, None)]

    #"""
    # go over all the concordance plots
    df_intevals = process_concordance_htqc_plots(
        config=config, key_to_config_usage=key_to_config_usage,
        col_name_4_sample_names=col_name_4_sample_names,
        list_of_heatmap_values=list_of_heatmap_values,
        name_for_all_samples_combined=name_for_all_samples_combined,
        htqc_barchart_sort_x_by_value=htqc_barchart_sort_x_by_value,
        htqc_boxchart_sort_x_by_value=htqc_boxchart_sort_x_by_value,
        htqc_boxchart_figsize_width=htqc_boxchart_figsize_width,
        htqc_barchart_figsize_width=htqc_barchart_figsize_width,
        alternate_colors=alternate_bar_chart_colors,
        sort_gc_analysis_charts_by_metric=False,
        only_high_level_metrics=only_high_level_metrics,
        intervals_df_filename='_'.join((df_file_prefix, 'intervals.txt')),
        htqc_summary_data_filename='_'.join((df_file_prefix, 'htqc_summary_metrics.txt')),
        heatmap_df_filename='_'.join((df_file_prefix, 'htqc_summary_metrics.txt')))
    #"""
    # config file coming in here only has HTQC metrics for oc120plus
    if args.which_analysis == 'equivalency_study_oc120plus_samples' \
            or args.which_analysis == 'equivalency_study_oc120_samples' \
            or args.which_analysis == 'oc120plus_vs_oc120plus_without_snps':
        if args.which_analysis == 'equivalency_study_oc120plus_samples':  # New genes only in oc120plus assay
            # create a copy of the df with only the new genes
            df_genes = df_intevals[df_intevals['GENE'].isin(get_oc120plus_new_gene_list_for_variant_plex())].copy()
            output_normalized_gene_counts_by_sample(df=df_genes,
                                                    col_name_4_sample_names=col_name_4_sample_names,
                                                    df_ouput_filename=f"{args.which_analysis}_52_new_gene_list.txt")

        output_normalized_gene_counts_by_sample(df=df_intevals,
                                                col_name_4_sample_names=col_name_4_sample_names,
                                                df_ouput_filename=f"{args.which_analysis}_all_gene_list.txt")

    if args.which_analysis == 'equivalency_study_oc120plus_samples':
        process_boxplot_analysis_for_htqc_intervals_for_gene_list(
            df=df_intevals,
            config=config,
            key_to_config_usage=key_to_config_usage,
            col_name_4_sample_names=col_name_4_sample_names,
            gene_list=get_oc120plus_new_gene_list_for_variant_plex(),
            gene_list_name=f"{args.which_analysis}_gene_list",
            only_plot_mcc=True
        )


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    bash_script = 'run_clinical_sample_reproducibility_over_parameters.sh'
    help_str = f"This program is used to parse the reproducibility data from the bash script {bash_script} that " \
               "called clinical_sample_reproducibility.py, over many different parameters to chart the output\n\n" \
               "There are no parameters needed for this program, it uses lab_validation_120.utils.lab_validation_config"

    parser = argparse.ArgumentParser(description=help_str)

    parser.add_argument('--which_analysis', dest='which_analysis', required=True, type=str,
                        help="Analyze which of the comparisons we did?",
                        choices=['oc120_vs_oc120plus_oc120_bed',
                                 'oc120_vs_oc120plus_oc120plus_bed',
                                 'equivalency_study_oc120plus_samples',
                                 'equivalency_study_oc120_samples',
                                 'oc120plus_flow_cell_comparison_24_samples',
                                 'oc120plus_flow_cell_comparison_32_samples',
                                 'oc120plus_qc_repeat_samples',
                                 'oc395_oc395primer_boost_11_samples',
                                 'oc395primer_boost_33_samples',
                                 'oc395primer_boost_vs_oc120plus_32_samples',
                                 'oc395primer_boost_vs_oc120plus_rerun_16_samples',
                                 'oc395primer_boost_vs_oc395primer_boost_rerun_16_samples',
                                 'oc120plus_vs_oc120plus_snps_24_samples',
                                 'oc120plus_vs_oc120plus_snps_11_resequenced_samples',
                                 'oc120plus_vs_oc120plus_without_snps',
                                 'oc395_primer_boost_vs_oc120plus_validation,',
                                 'oc395_primer_boost_vs_genosity',
                                 'oc395_primer_boost_vs_genosity_no_germline',
                                 'oc395_primer_boost_reproducibility',
                                 'oc395snps_vs_oc120plus',
                                 'oc395_pon_vs_oc395snps_pon',
                                 'oc395snps_pon',
                                 'hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples',
                                 'hybrid_oc395_4_samples_vs_original_oc395_4_samples',
                                 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples',
                                 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run2',
                                 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3',
                                 'hybrid_oc395_6_samples_vs_original_oc395_6_samples',
                                 'hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3',
                                 'agilent700_vs_oc180',
                                 'agilent700_vs_oc395',
                                 ])
    # only analyzed consequential impacts in regression?
    parser.add_argument('--consequential_impacts_only', dest='consequential_impacts_only', action='store_true',
                        default=False, help="only analyzed consequential impacts in regression and boxplots")

    help_str = "If you set the equivalency_study option, then it will only compare the second sample's equivalency only"
    parser.add_argument('--equivalency_study', dest='equivalency_study', action='store_true', default=False,
                        help=help_str)

    return parser.parse_args()


if __name__ == "__main__":
    main()


