"""Module to complete the concordance with the Horizon samples"""
import argparse
from lab_validation_120.utils.configs.clinical_sample_reproducibility_config import get_clinical_sample_reproducibility
from lab_validation_120.utils.configs.oc120_vs_oc120plus_reproducibility_config import \
    get_oc120_vs_oc120plus_sample_reproducibility_oc120_bed_file, \
    get_oc120_vs_oc120plus_sample_reproducibility_oc120plus_bed_file
from lab_validation_120.utils.concordance_utilites import process_samples_for_concordance, \
    prepare_matched_results_for_printing
from lab_validation_120.utils.configs.oc120plus_midoutput_vs_highoutput_flowcell_config import \
    get_oc120plus_midoutput_vs_highoutput_flow_cell_reproducibility_24_samples, \
    get_oc120plus_midoutput_vs_highoutput_flow_cell_reproducibility_32_samples
from lab_validation_120.utils.configs.oc120plus_qc_repeat_sample_config import \
    get_oc120plus_qc_repeat_sample_reproducibility

from lab_validation_120.utils.configs.oc395.oc395_oc395primer_boost_equiv_samples_config import \
    get_oc395_oc395primer_boost_reproducibility_11_samples
from lab_validation_120.utils.configs.oc395.oc395primer_boost_vs_oc120plus_config import \
    get_oc395primer_boost_vs_oc120plus_reproducibility_32_samples
from lab_validation_120.utils.configs.oc120plus_snps.oc120plus_vs_oc120plus_snps_config_24_original_samples import \
    get_oc120plus_vs_oc120plus_snps_reproducibility_24_samples
from lab_validation_120.utils.configs.oc120plus_without_snps.oc120plus_vs_oc120plus_without_snps_config import \
    get_oc120plus_vs_oc120plus_without_snps_reproducibility_11_samples
from lab_validation_120.utils.configs.oc120plus_snps.oc120plus_vs_oc120plus_snps_config_11_resequenced_samples import \
    get_oc120plus_vs_oc120plus_snps_reproducibility_11_resequenced_samples

from lab_validation_120.utils.configs.oc395.oc395_vs_oc120plus_validation_config import \
    get_oc395_vs_oc120plus_reproducibility_samples
from lab_validation_120.utils.configs.oc395.oc395_reproduciblity_config import get_oc395_reproducibility

from lab_validation_120.utils.configs.oc395_snps.oc395snps_reproducibility_config import get_oc395snps_reproducibility

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples \
    import get_hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples_reproducibility

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc395_4_samples_vs_original_oc395_4_samples import \
    get_hybrid_oc395_4_samples_vs_original_oc395_4_samples_reproducibility

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples \
    import get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run2 \
    import get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run2

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3 \
    import get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run3

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc395_6_samples_vs_original_oc395_6_samples import \
    get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc395_6_samples_vs_original_oc395_6_samples_run2 import \
    get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility_run2

from \
    lab_validation_120.utils.configs.hybrid_approach.hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3 import \
    get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility_run3

from lab_validation_120.utils.configs.agilent700.agilent700_reproducibility_config import \
    get_agilent700_vs_oc180_reproducibility, get_agilent700_vs_oc395_reproducibility


def main():
    """Main driver of the program"""

    args = get_cli_args()
    args.which_assay_comparison = args.which_assay_comparison.lower()
    config = None
    assay_intervals_for_overlap_analysis = None
    if args.which_assay_comparison == 'oc120':
        config = get_clinical_sample_reproducibility()
    elif args.which_assay_comparison == 'oc120_vs_oc120plus_oc120_bed_file':
        config = get_oc120_vs_oc120plus_sample_reproducibility_oc120_bed_file()
    elif args.which_assay_comparison == 'oc120_vs_oc120plus_oc120plus_bed_file':
        config = get_oc120_vs_oc120plus_sample_reproducibility_oc120plus_bed_file()
        assay_intervals_for_overlap_analysis = 'oc120_oc120plus_overlap'
    elif args.which_assay_comparison == 'oc120plus_flow_cell_comparison_24_samples':
        config = get_oc120plus_midoutput_vs_highoutput_flow_cell_reproducibility_24_samples()
    elif args.which_assay_comparison == 'oc120plus_flow_cell_comparison_32_samples':
        config = get_oc120plus_midoutput_vs_highoutput_flow_cell_reproducibility_32_samples()
    elif args.which_assay_comparison == 'oc120plus_qc_repeat_samples':
        config = get_oc120plus_qc_repeat_sample_reproducibility()
    elif args.which_assay_comparison == 'oc395_primer_boost_comparison':
        config = get_oc395_oc395primer_boost_reproducibility_11_samples()
    elif args.which_assay_comparison == 'oc120plus_vs_oc120plus_snps':
        config = get_oc120plus_vs_oc120plus_snps_reproducibility_24_samples()
    elif args.which_assay_comparison == 'oc395_primer_boost_vs_oc120plus':
        config = get_oc395primer_boost_vs_oc120plus_reproducibility_32_samples()
        assay_intervals_for_overlap_analysis = 'oc120plus_oc395_overlap'
    elif args.which_assay_comparison == 'oc120plus_vs_oc120plus_without_snps':
        config = get_oc120plus_vs_oc120plus_without_snps_reproducibility_11_samples()
    elif args.which_assay_comparison == 'oc120plus_vs_oc120plus_snps_resequenced_11_samples':
        config = get_oc120plus_vs_oc120plus_snps_reproducibility_11_resequenced_samples()
    elif args.which_assay_comparison == 'oc395_primer_boost_vs_oc120plus_validation':
        config = get_oc395_vs_oc120plus_reproducibility_samples()
        assay_intervals_for_overlap_analysis = 'oc120plus_oc395_overlap'
    elif args.which_assay_comparison == 'oc395_primer_boost_reproducibility':
        config = get_oc395_reproducibility()
    elif args.which_assay_comparison == 'oc395snps_vs_oc120plus':
        config = get_oc395snps_reproducibility()
        assay_intervals_for_overlap_analysis = 'oc120plus_oc395_overlap'
    elif args.which_assay_comparison == 'hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples':
        config = get_hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples_reproducibility()
    elif args.which_assay_comparison == 'hybrid_oc395_4_samples_vs_original_oc395_4_samples':
        config = get_hybrid_oc395_4_samples_vs_original_oc395_4_samples_reproducibility()
    elif args.which_assay_comparison == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples':
        config = get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility()
    elif args.which_assay_comparison == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run2':
        config = get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run2()
    elif args.which_assay_comparison == 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3':
        config = get_hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_reproducibility_run3()
    elif args.which_assay_comparison == 'hybrid_oc395_6_samples_vs_original_oc395_6_samples':
        config = get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility()
    elif args.which_assay_comparison == 'hybrid_oc395_6_samples_vs_original_oc395_6_samples_run2':
        config = get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility_run2()
    elif args.which_assay_comparison == 'hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3':
        config = get_hybrid_oc395_6_samples_vs_original_oc395_6_samples_reproducibility_run3()

    elif args.which_assay_comparison == 'agilent700_vs_oc180':
        config = get_agilent700_vs_oc180_reproducibility()
        assay_intervals_for_overlap_analysis = 'agilent700_vs_oc180'
    elif args.which_assay_comparison == 'agilent700_vs_oc395':
        config = get_agilent700_vs_oc395_reproducibility()
        assay_intervals_for_overlap_analysis = 'agilent700_vs_oc395'

    # over write the config if passed in
    if args.consequential_impacts_only is True:
        config['clinical_sample_reproducibility']['consequential_impacts_only'] = True
    else:
        config['clinical_sample_reproducibility']['consequential_impacts_only'] = False

    depth_threshold = args.depth_threshold
    vaf_threshold = args.vaf_threshold
    # get the df of matched values given the thresholds
    final_matched_results = \
        process_samples_for_concordance(config=config, vaf_threshold=vaf_threshold, depth_threshold=depth_threshold,
                                        key_to_search="clinical_sample_reproducibility",
                                        assay_intervals_for_overlap_analysis=assay_intervals_for_overlap_analysis)

    prepare_matched_results_for_printing(args=args, final_matched_results=final_matched_results,
                                         vaf_threshold=vaf_threshold, depth_threshold=depth_threshold)


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Process the samples according to configs concordance sample data')

    parser.add_argument('--tabular_output_file', dest='tabular_output_file',
                        type=str, help='Output file to store the data', required=True)

    parser.add_argument('--vaf_threshold', dest='vaf_threshold',
                        type=float, help='Variant allele fraction threshold, e.g. 5.0 = 5.0 Percentage',
                        required=True, default=None)

    parser.add_argument('--depth_threshold', dest='depth_threshold',
                        type=int, help='Depth of coverage threshold',
                        required=True, default=None)

    parser.add_argument('--print_final_output_header', dest='print_final_output_header',
                        help='Print the header line of final output (True)', required=False,
                        action='store_true', default=False)

    help_str = "This flag will only include consequential transcript impacts, i.e. not things like synonymous_variant" \
               " or 3_prime_UTR_variant.  See (is_a_consequential_impact) in vcf_utilities.py for more information"

    parser.add_argument('--consequential_impacts_only', dest='consequential_impacts_only',
                        action='store_true', default=False,
                        help=help_str)

    parser.add_argument('--which_assay_comparison', dest='which_assay_comparison', required=True, type=str,
                        choices=['oc120',
                                 'oc120_vs_oc120plus_oc120_bed_file',
                                 'oc120_vs_oc120plus_oc120plus_bed_file',
                                 'oc120plus_flow_cell_comparison_24_samples',
                                 'oc120plus_flow_cell_comparison_32_samples',
                                 'oc120plus_qc_repeat_samples',
                                 'oc395_primer_boost_comparison',
                                 'oc395_primer_boost_vs_oc120plus',
                                 'oc120plus_vs_oc120plus_snps',
                                 'oc120plus_vs_oc120plus_without_snps',
                                 'oc120plus_vs_oc120plus_snps_resequenced_11_samples',
                                 'oc395_primer_boost_vs_oc120plus_validation',
                                 'oc395_primer_boost_reproducibility',
                                 'oc395snps_vs_oc120plus',
                                 'hybrid_oc180snp_36_samples_vs_original_oc180snp_36_samples',
                                 'hybrid_oc395_4_samples_vs_original_oc395_4_samples',
                                 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples',
                                 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run2',
                                 'hybrid_oc180snp_30_samples_vs_original_oc180snp_30_samples_run3',
                                 'hybrid_oc395_6_samples_vs_original_oc395_6_samples',
                                 'hybrid_oc395_6_samples_vs_original_oc395_6_samples_run2',
                                 'hybrid_oc395_6_samples_vs_original_oc395_6_samples_run3',
                                 'agilent700_vs_oc180',
                                 'agilent700_vs_oc395',
                                 ])

    return parser.parse_args()


if __name__ == "__main__":
    main()
