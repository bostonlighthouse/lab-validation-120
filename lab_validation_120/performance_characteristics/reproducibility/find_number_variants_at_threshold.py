import argparse
import os.path
import sys

import biomodels.models.vcf.vcf_container as vc
from biomodels.models.vcf.utilities.vcf_utilities import return_most_severe_impact_transcript_obj,\
    ignore_caller_because_of_caller_thresholds, get_vaf_from_vf_field


from tools.utils.tools_utils import open_directory_get_list_files_recursively, get_cid_from_string


def main():
    args = get_cli_args()
    vcf_directory = args.vcf_directory
    # get the list of input files to open
    vcf_directory_list = open_directory_get_list_files_recursively(directory=vcf_directory)
    for vcf_file in vcf_directory_list:
        cid = get_cid_from_string(vcf_file)
        #vcf_file = os.path.join(vcf_directory, vcf_file)
        variant_container = vc.VcfContainer(vcf_file)
        total_entries = 0
        for i, variant_entry_obj in enumerate(variant_container.dictionary_vcf_entries.values()):
            ignore, _ = ignore_caller_because_of_caller_thresholds(variant_entry_obj=variant_entry_obj, lofreq_SB=10)
            if ignore is True:
                continue

            # get some values from on depth and vaf
            total_depth = variant_entry_obj.info_internal_2_vcf.REF_DP + variant_entry_obj.info_internal_2_vcf.ALT_DP
            vaf_2_use = get_vaf_from_vf_field(variant_entry_obj=variant_entry_obj)
            vaf = round(vaf_2_use * 100, 3)

            # get if vaf or total depth do not pass
            if vaf < 5 or total_depth < 100:
                continue
            total_entries += 1
        print(f"{cid}", file=sys.stderr)
        print("\t".join((cid, vcf_file, str(total_entries))))


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open a list of cider output files and create a docker bash script '
                                                 'for TMB calling on those VCF files found')


    parser.add_argument('--vcf_directory', dest='vcf_directory',
                        type=str, help='The directory to check for VCF output files from Cider ',
                        required=True)


    return parser.parse_args()


if __name__ == "__main__":
    main()