"""PLot a set of directories CNV gene data"""
import os
import argparse
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.colors
import math
import sys
import numpy as np


from lab_validation_120.utils.plots.plot_utilities import return_dictionary_x_y_values, get_x_y_list_of_points, \
    get_subplot_bounding_box_parameters
from lab_validation_120.utils.pandas_dataframes_utilities import read_dataset_and_get_pandas_df, \
    get_columns_from_cider_gene_cnv_file, get_columns_from_cider_cnv_file, print_data_frame, get_cid_from_data_frame_row
from lab_validation_120.utils.configs.lab_validation_config import get_oc120plus_gtf_file
from tools.cnv.utils import get_list_important_cnv_genes
from lab_validation_120.utils.plots.regression_plot_utilities import regression_plot
from tools.cnv.function_for_cnvs_levels_for_cbioportal import get_copy_number_thresholds, get_cbio_value

COPY_STATE_FOR_HIGH_LEVEL_AMPLIFICATION = 5
GLOBAL_TUMOR_PURITY = .4
IMAGE_TYPE_EXTENSION = 'pdf'


def main():

    args = get_cli_args()
    cluster_type = args.cluster_type
    assay1_name = args.assay1_name
    assay2_name = args.assay2_name
    cnv_gene_filter_bool = args.cnv_gene_filter_bool
    sample_type = args.sample_type
    final_gene_df, final_all_probes_df = get_cnv_data_frame_from_input_file(
        input_file=args.cnv_file,
        dir1=args.path_assay1,
        dir2=args.path_assay2,
        cnv_gene_filter_bool=cnv_gene_filter_bool,
        sample_type=sample_type
    )
    # print out the data for the overall comparison of dispersion
    find_overall_dispersion_from_sample_by_chr(df=final_all_probes_df,
                                               cnv_gene_filter_bool=cnv_gene_filter_bool,
                                               assay1_name=assay1_name,
                                               assay2_name=assay2_name)
    # go over both of the dataframes
    #"""
    for df, title, all_probes_data_bool, pivot_index in [(final_gene_df, 'gene_level', False, ['gene']),
                                            (final_all_probes_df, 'probe_level', True, ['gene', 'pos', 'exon'])
                                            ]:
        df['all_probes_data'] = 1  # set this column to 1, so we can create a regression of all probes data
        # do both regressions (sample and all data) for both cnv_gene_filter_bool = True and False
        regression_plots = [
            ('sample_cid_x', 'sample_cid_y', get_filename(filename=f'cid_correlation_{title}',
                                                          cnv_gene_filter_bool=cnv_gene_filter_bool,
                                                          extension=IMAGE_TYPE_EXTENSION)),
            ('all_probes_data', 'all_probes_data', get_filename(filename=f'all_correlation_{title}',
                                                                cnv_gene_filter_bool=cnv_gene_filter_bool,
                                                                extension=IMAGE_TYPE_EXTENSION))]
        if cnv_gene_filter_bool is True:  # only run this regression  on the subset of genes, since doing it on all will
            # not work due to the image size.  Plus we don't care about the other genes
            regression_plots.append(('gene', 'gene', get_filename(filename=f'gene_correlation_{title}',
                                                                  cnv_gene_filter_bool=cnv_gene_filter_bool,
                                                                  extension=IMAGE_TYPE_EXTENSION)))

        # process the regression values
        for values_to_list1, values_to_list2, image_name in regression_plots:
            process_regression(final_df=df,
                               values_to_list1=values_to_list2,
                               values_to_list2=values_to_list1,
                               image_filename=image_name,
                               assay1_name=assay2_name,
                               assay2_name=assay1_name)
        # process the distributions
        plot_distributions(final_df=df,
                           image_filename=get_filename(filename=f'distribution_cnr_values_{title}',
                                                       cnv_gene_filter_bool=cnv_gene_filter_bool,
                                                       extension=IMAGE_TYPE_EXTENSION),
                           assay1_name=assay1_name,
                           assay2_name=assay2_name)
        if cnv_gene_filter_bool is True:
            # only do heat maps on the gene level data, not all probes
            plot_heat_maps(df=df,
                           cluster_type=cluster_type,
                           cnv_gene_filter_bool=cnv_gene_filter_bool,
                           filename_addition=title,
                           pivot_index=pivot_index,
                           assay1_name=assay1_name,
                           assay2_name=assay2_name)
    #"""
    dispersion_values = [
        (final_all_probes_df, 'boxplot_all_cnr_values', 'sample_cid_x', 'sample_cid_y', None, 12, True),
        (final_gene_df, 'boxplot_gene_cnr_values', 'sample_cid_x', 'sample_cid_y', None, 12, False),
        (final_all_probes_df, 'boxplot_all_chr_cnr_values', 'chr_x', 'chr_y', (-2, 2), 24, True)
    ]

    if cnv_gene_filter_bool is True:  # do not run this if all genes, too large
        dispersion_values.append((final_all_probes_df, 'boxplot_all_gene_cnr_values', 'gene', 'gene', (-2, 2), 24, False))
    """
    for df, image_title, unique_val1, unique_val2, y_axis_limit, fig_width, all_probes_data_bool in dispersion_values:
        plot_boxplots(df=df, image_filename=get_filename(filename=image_title,
                                                         cnv_gene_filter_bool=cnv_gene_filter_bool,
                                                         extension=IMAGE_TYPE_EXTENSION),
                      y_axis_limit=y_axis_limit,
                      unique_val1=unique_val1, unique_val2=unique_val2, fig_width=fig_width,
                      text_filename=get_filename(filename=image_title,
                                                 cnv_gene_filter_bool=cnv_gene_filter_bool,
                                                 extension="txt"),
                      cnv_gene_filter_bool=cnv_gene_filter_bool,
                      all_probes_data_bool=all_probes_data_bool,
                      assay1_name=assay1_name,
                      assay2_name=assay2_name)
    """
    # print the data frame
    for df, filename in [(final_gene_df, 'cnv_data_both_run_gene_level'),
                         (final_all_probes_df, 'cnv_data_both_probe_level')
                         ]:
        print_data_frame(df=df, tabular_output_file=get_filename(filename=filename,
                                                                 cnv_gene_filter_bool=cnv_gene_filter_bool,
                                                                 extension="txt"))

def find_overall_dispersion_from_sample_by_chr(df=None, cnv_gene_filter_bool=None,
                                               assay1_name=None, assay2_name=None):
    sample_filter = 'sample_cid_x'
    chr_filter = 'chr_x'
    assay_unique_vals = df[sample_filter].unique()
    chromosomes = df[chr_filter].unique()
    with open(get_filename(filename='final_normalized_dispersion_per_sample',
                           cnv_gene_filter_bool=cnv_gene_filter_bool, extension="txt"), 'w') as out_fh:
        print(f"cid1\tcid2\texcel-name\t"
              f"{assay1_name} - variance normalized by chromosome\t"
              f"{assay2_name} - variance normalized by chromosome\t"
              f"{assay1_name} / {assay2_name}\t"
              f"{assay1_name} - variance by chromosome\t"
              f"{assay2_name} - variance by chromosome\t"
              f"{assay1_name} / {assay2_name}", file=out_fh)
        for cid1 in assay_unique_vals:
            norm_variance_total1 = 0  # sum this over each sample for first cid
            norm_variance_total2 = 0  # sum this over each sample for second cid
            unnorm_variance_total1 = 0  # sum this over each sample for first cid
            unnorm_variance_total2 = 0  # sum this over each sample for second cid
            df_reduced = df[df[sample_filter] == cid1]  # get the reduced df
            normalization_denominator = len(df_reduced)  # now many rows are there?
            for chromosome in chromosomes:
                df_reduced2 = df_reduced[df_reduced[chr_filter] == chromosome]  # reduced by chromosome
                cid2 = df_reduced2['sample_cid_y'].unique()[0]
                normalization_numerator = len(df_reduced2)  # how many rows per chr
                print(chromosome, normalization_numerator, file=sys.stderr)
                final_normalization = normalization_numerator / normalization_denominator  # used to normalize per chr
                variance1 = df_reduced2['log2cnr_x'].var().item()
                #mean1 = df_reduced2['log2cnr_x'].mean().item()
                variance2 = df_reduced2['log2cnr_y'].var().item()
                #mean2 = df_reduced2['log2cnr_y'].mean().item()
                norm_variance1 = round(variance1 * final_normalization, 6) * 10
                #norm_variance1 = round(variance1 / mean1, 6) * 10
                norm_variance2 = round(variance2 * final_normalization, 6) * 10
                #norm_variance2 = round(variance2 / mean2, 6) * 10
                unnorm_variance1 = round(variance1, 6)
                unnorm_variance2 = round(variance2, 6)
                norm_variance_total1 += norm_variance1
                norm_variance_total2 += norm_variance2
                unnorm_variance_total1 += unnorm_variance1
                unnorm_variance_total2 += unnorm_variance2
            norm_final_value = round(norm_variance_total1 / norm_variance_total2, 6)  # what's the overall number per sample
            unnorm_final_value = round(unnorm_variance_total1 / unnorm_variance_total2, 6)  # what's the overall number per sample
            print(f"{cid1}\t{cid2}\t{'-'.join((cid1, cid2))}\t"
                  f"{norm_variance_total1}\t{norm_variance_total2}\t{norm_final_value}\t"
                  f"{unnorm_variance_total1}\t{unnorm_variance_total2}\t{unnorm_final_value}\t", file=out_fh)


def plot_boxplots(df=None, image_filename=None, y_axis_limit=None,
                  unique_val1=None, unique_val2=None, fig_width=None, text_filename=None, cnv_gene_filter_bool=None,
                  all_probes_data_bool=None,
                  assay1_name=None,
                  assay2_name=None):
    assay_unique_vals1 = df[unique_val1].unique()  # e.g. sample_cid_x or chr_x
    assay_samples_vals2 = df[unique_val2].unique()  # e.g. sample_cid_y or chr_y
    if unique_val1.startswith('gene'):  # find to sort by gene, but do not do for sample_cid_y
        assay_unique_vals1 = sorted(assay_unique_vals1)
        assay_samples_vals2 = sorted(assay_samples_vals2)
    fh_out = open(text_filename, "w")
    # header for the output file
    print("\t".join(("filter1", "mean", "median", "count", "std", "variance",
                     "filter2", "mean", "median", "count", "std", "variance")),
          file=fh_out)
    plt.rcParams['figure.figsize'] = (fig_width, len(assay_unique_vals1) * 5)
    fig, ax = plt.subplots(len(assay_unique_vals1), 4)  # rows columns
    for i, (filter1, filter2) in enumerate(zip(assay_unique_vals1, assay_samples_vals2)):
        df_reduced1 = df[df[unique_val1] == filter1]
        df_reduced2 = df[df[unique_val2] == filter2]
        # use to_frame() to get a DataFrame from the Series (output of describe) and the format,
        # and then .T to transform the Series indices to column names.
        describe_df1 = df_reduced1['log2cnr_x'].describe().apply("{0:.3f}".format).to_frame().T
        describe_df2 = df_reduced2['log2cnr_y'].describe().apply("{0:.3f}".format).to_frame().T
        median1 = round(df_reduced1['log2cnr_x'].median(), 3)
        median2 = round(df_reduced2['log2cnr_y'].median(), 3)
        variance1 = round(df_reduced1['log2cnr_x'].var(), 3)
        variance2 = round(df_reduced2['log2cnr_y'].var(), 3)
        count1 = int(float(describe_df1['count'].iloc[0])) # conversion above it to a string..
        count2 = int(float(describe_df2['count'].iloc[0]))
        # values to create y lim for certain plots
        max1 = float(describe_df1['max'].iloc[0])
        max2 = float(describe_df2['max'].iloc[0])
        min1 = float(describe_df1['min'].iloc[0])
        min2 = float(describe_df2['min'].iloc[0])
        max_to_use = max1 if max1 > max2 else max2
        min_to_use = min1 if min1 < min2 else min2
        max_to_use = math.ceil(max_to_use)
        min_to_use = math.floor(min_to_use)

        # titles for  plots
        title1 = f"mean {describe_df1['mean'].iloc[0]} median {median1}\n" \
                 f"count {count1}  std {describe_df1['std'].iloc[0]} variance {variance1}"
        title2 = f"mean {describe_df2['mean'].iloc[0]} median {median2}\n" \
                 f"count {count2}  std {describe_df2['std'].iloc[0]} variance {variance2}"

        # change depending on the type of plot
        if unique_val1.startswith('sample_cid'):
            title1 = f'Sample: {filter1} {title1}'
            title2 = f'Sample: {filter2} {title2}'
            #y_axis_limit = (min_to_use, max_to_use)
            y_axis_limit = (-5, 5)

        elif unique_val1.startswith('chr'):
            title1 = f'Chromosome: {filter1} {title1}'
            title2 = f'Chromosome: {filter2} {title2}'
            #distribution_title = f"distribution_{filter1}_{filter2}_{image_filename}"
            distribution_title = f"distribution_{filter1}_{filter2}"
            # process the distributions for the chromosomes
            plot_distributions(final_df=df_reduced1,
                               image_filename=get_filename(filename=distribution_title,
                                                           cnv_gene_filter_bool=cnv_gene_filter_bool,
                                                           extension=IMAGE_TYPE_EXTENSION),
                               assay1_name=assay1_name,
                               assay2_name=assay2_name)

        elif unique_val1.startswith('gene'):
            title1 = f'Gene: {filter1} {title1}'
            title2 = f'Gene: {filter2} {title2}'
            y_axis_limit = (min_to_use, max_to_use)
        else:
            raise ValueError(f"did not implement filter: {unique_val1}")
        # print out the data output file
        print("\t".join((filter1, str(describe_df1['mean'].iloc[0]), str(median1),
                         str(count1), str(describe_df1['std'].iloc[0]), str(variance1),
                         filter2, str(describe_df2['mean'].iloc[0]), str(median2),
                         str(count2), str(describe_df2['std'].iloc[0]), str(variance2)
                         )
                        ),
              file=fh_out)
        # do the plotting
        for ii, (df2, column, data, title, palette, type_plot) \
                in enumerate((
                (df_reduced1, 'sample_cid_x', 'log2cnr_x', title1, 'Blues', 'boxplot'),
                (df_reduced2, 'sample_cid_y', 'log2cnr_y', title2, 'coolwarm', 'boxplot'),
                (df_reduced1, 'sample_cid_x', 'log2cnr_x', title1, 'Blues', 'violinplot'),
                (df_reduced2, 'sample_cid_y', 'log2cnr_y', title2, 'coolwarm', 'violinplot'),
        )):
            if type_plot == 'boxplot':
                # stripplot for dots
                sns.stripplot(x=df2[column], y=df2[data], ax=ax[i][ii], size=2)
                # boxplot for the rest, but remove dots
                box = sns.boxplot(x=df2[column], y=df2[data], ax=ax[i][ii], palette=palette, showfliers=False)
            else:
                box = sns.violinplot(x=df2[column], y=df2[data], ax=ax[i][ii], size=2)

            # labels on the x-axis at 45 degrees
            box.set_xticklabels(box.get_xticklabels(), rotation=45)
            box.set(xlabel=None)  # remove the xlabel
            ax[i][ii].set_title(title, fontdict={'fontsize': 7})
            ax[i][ii].set_ylim(y_axis_limit)

    fig.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(image_filename, dpi=300)
    plt.close(fig)
    fh_out.close()


def custom_describe(df, stats):
    d = df.describe()
    return d.append(df.reindex(d.columns, axis=1).agg(stats))


def plot_heat_maps(df=None, cnv_gene_filter_bool=None,
                   cluster_type=None,
                   filename_addition=None,
                   pivot_index=None,
                   assay1_name=None,
                   assay2_name=None):
    # https://htmlcolorcodes.com/colors/shades-of-white/
    red = '#FF0000'
    light_red = '#FAA0A0'
    beige = '#e0dcdc'
    blue = '#0000FF'
    light_blue = '#0096FF'
    colors = [blue, light_blue, beige, light_red, red]

    # https://seaborn.pydata.org/generated/seaborn.clustermap.html
    # https://docs.scipy.org/doc/scipy/reference/reference/generated/scipy.clusoter.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage
    # CANNOT USE Subplots with heatmap, b/c A clustermap comprises multiple axes that have to be
    # organized in a certain way.  So have to create seperate plots
    # https://github.com/mwaskom/seaborn/issues/417
    for i, (assay_type, samples, copy_number_vals, title_values) in enumerate([
        (assay1_name, 'sample_cid_x', 'log2cnr_x', 'log2 Ratios'),
        (assay1_name, 'sample_cid_x', 'copy_number_cbio_portal_x', 'cBioPortal Scale'),
        (assay2_name, 'sample_cid_y', 'log2cnr_y', 'log2 Ratios'),
        (assay2_name, 'sample_cid_y', 'copy_number_cbio_portal_y', 'cBioPortal Scale'),
    ]):
        title = f"{assay_type} hierarchical clustering ({cluster_type}) of {title_values}: "
        if copy_number_vals.startswith('log2'):  # use continuous scale
            cmap = 'coolwarm'
        else:  # use a discrete scale
            cmap = matplotlib.colors.ListedColormap(colors)
            title += f"High-level Copy Number State: {COPY_STATE_FOR_HIGH_LEVEL_AMPLIFICATION}, (cBioPortal = 2)"

        image_filename = get_filename(filename=f'cnv_heatmap_{copy_number_vals}_{assay_type.replace(" + ", "_")}_'
                                               f'{filename_addition}',
                                      cnv_gene_filter_bool=cnv_gene_filter_bool,
                                      extension=IMAGE_TYPE_EXTENSION)
        # pivot on 'gene' or 'pos'.  Probes get pivoted on 'pos'
        #print(f"pivot index here {pivot_index}")
        new_df = df.pivot_table(index=pivot_index, columns=samples, values=copy_number_vals)
        sns.set(font_scale=.4)
        size_fig = (10, 10)
        #print(new_df)
        if cnv_gene_filter_bool is True:  # can be smaller
            size_fig = (7, 7)
        cbar_kws = {"ticks": [-2, -1, 0, 1, 2]}
        if 'pos' in pivot_index:  # this is the probes df, so it's much larger, and will have some NaN values
            mask_na = 0.000666
            new_df = new_df.fillna(mask_na)  # mask with a value very close to 0
            size_fig = (10, 100)
            dendrogram_ratio = (.1, .025)
            cbar_kws["shrink"] = .025
        else:
            dendrogram_ratio = (.1, .2)
            cbar_kws["shrink"] = .60

        g = sns.clustermap(new_df, yticklabels=True, figsize=size_fig,
                           metric='euclidean', method=cluster_type, cmap=cmap, vmin=-2, vmax=2,
                           cbar_kws=cbar_kws, dendrogram_ratio=dendrogram_ratio)

        g.fig.suptitle(f'{title}', fontsize=5, y=0.995)
        if 'pos' in pivot_index:  # had to create a custom legend b/c by defuault it became way to large
            x0, _y0, _w, _h = g.cbar_pos
            # pos[left, bottom, width, height]
            g.ax_cbar.set_position([x0, 0.98, g.ax_row_dendrogram.get_position().width-.04, 0.01])
            g.ax_cbar.tick_params(axis='x', length=10)
            for spine in g.ax_cbar.spines:
                g.ax_cbar.spines[spine].set_color('crimson')
                g.ax_cbar.spines[spine].set_linewidth(1)

        plt.savefig(image_filename, dpi=300)
        plt.close()


def get_filename(filename=None, cnv_gene_filter_bool=None, extension=None):
    return f"{filename}_only_important_cnv_values_{cnv_gene_filter_bool}.{extension}"


def plot_distributions(final_df=None, image_filename=None, assay1_name=None, assay2_name=None):
    plt.rcParams['figure.figsize'] = (12, 6)
    fig, ax = plt.subplots(1, 2)
    # distribution #1 - assay being compared
    sns.histplot(data=final_df['log2cnr_x'], ax=ax[0], kde=True, color="blue")
    # distribution #2 - assay being compared
    sns.histplot(data=final_df['log2cnr_y'], ax=ax[1], kde=True, color="red")
    # set x and y axis the same for both plots
    kdeline = ax[0].lines[0]
    max_x1 = max(kdeline.get_xdata())
    min_x1 = min(kdeline.get_xdata())
    max_y1 = max(kdeline.get_ydata())
    kdeline = ax[1].lines[0]
    max_x2 = max(kdeline.get_xdata())
    min_x2 = min(kdeline.get_xdata())
    max_y2 = max(kdeline.get_ydata())

    max_x1 = max_x1 if max_x1 > max_x2 else max_x2
    min_x1 = min_x1 if min_x1 < min_x2 else min_x2

    max_y1 = max_y1 if max_y1 > max_y2 else max_y2
    max_x1 = int(round(max_x1)) + .5
    min_x1 = int(round(min_x1)) - .5
    if max_y1 <= 50:
        add_to_max_y = 10
    elif 50 < max_y1 <= 100:
        add_to_max_y = 20
    elif 100 < max_y1 <= 200:
        add_to_max_y = 40
    elif 200 < max_y1 <= 400:
        add_to_max_y = 60
    else:
        add_to_max_y = 80

    max_y1 = int(round(max_y1)) + add_to_max_y
    max_y_rounded = round(max_y1 / 10) * 10
    ax[0].set_ylim(0, max_y_rounded)
    ax[1].set_ylim(0, max_y_rounded)
    ax[0].set_xlim(min_x1, max_x1)
    ax[1].set_xlim(min_x1, max_x1)

    # values used for plot formatting
    list_assay1 = final_df['log2cnr_x'].to_list()
    list_assay2 = final_df['log2cnr_y'].to_list()
    list_both_values = list_assay1 + list_assay2  # get all values
    num_assay1 = len(list_assay1)  # how many points?
    num_assay2 = len(list_assay2)
    num_assay1_above_zero = len([i for i in list_assay1 if i > 0.0])  # how many above 0.0
    num_assay2_above_zero = len([i for i in list_assay2 if i > 0.0])
    # axis legend
    ax[0].legend(labels=[f'{assay1_name} CNR Values'])
    # set a vertical line at 0.0
    ax[0].axvline(x=0, color='black', linestyle='-')
    # set the title
    ax[0].title.set_text(f"{assay1_name} n = {num_assay1}, Above 0.0: "
                         f"{num_assay1_above_zero} (%{round((num_assay1_above_zero/num_assay1 * 100), 2)})")

    # set plot 2
    ax[1].legend(labels=[f'{assay2_name} CNR Values'])
    ax[1].axvline(x=0, color='black', linestyle='-')
    ax[1].title.set_text(f"{assay2_name} n = {num_assay2}, Above 0.0: "
                         f"{num_assay2_above_zero} (%{round((num_assay2_above_zero/num_assay2) * 100, 2)})")

    fig.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(image_filename)
    plt.close(fig)


def process_regression(final_df=None, values_to_list1=None, values_to_list2=None, image_filename=None,
                       assay1_name=None, assay2_name=None):

    # find the list of unique values to iterate over
    list_values = sorted(list(final_df[values_to_list1].unique()))
    #print(list_values)
    # set some figure related information
    num_unique_samples = len(list_values)
    if num_unique_samples == 1: # this is the data for all samples
        num_unique_samples = num_unique_samples + 1
        figsize_width = num_unique_samples * 6
        plt.rcParams['figure.figsize'] = (figsize_width, 6)
    else:  # this is the data for cids or genes
        figsize_width = num_unique_samples * 5
        plt.rcParams['figure.figsize'] = (figsize_width, 6)

    fig, ax = plt.subplots(1, num_unique_samples)  # rows x cols
    # loop over each values and create a smaller data frame
    for i, x_value in enumerate(list_values):
        df = final_df.loc[final_df[values_to_list1] == x_value].copy()
        df['cnr_diff'] = abs(df['log2cnr_x'] - df['log2cnr_y'])
        avg_cnr_diff = round(df['cnr_diff'].mean(), 2)
        print(f"value: {x_value} avg_cnr_diff {avg_cnr_diff}")
        x_value_key, y_value_key = (assay1_name, assay2_name)
        # get the dictionary of values that will be used for plotting
        dict_vals = dict(df.apply(lambda row:
                                  return_dictionary_x_y_values(row_from_df=row,
                                                               x_value_key=x_value_key,
                                                               y_value_key=y_value_key,
                                                               df_value1='log2cnr_y',
                                                               df_value2='log2cnr_x'),
                                  axis=1))
        y_value = list(df[values_to_list2].unique())[0]

        # get the actual x and y points
        x, y = get_x_y_list_of_points(dict_values=dict_vals, x_value_key=x_value_key, y_value_key=y_value_key)
        if num_unique_samples == 2:  # this is the data for all samples, so hard code this, 2 here b/c we added one
            # to be able to use the same loop when plotting via: fig, ax = plt.subplots(1, num_unique_samples)
            max_val = 2
            neg_max_val = -2
        else:  # find the maximum values from those two lists when its cids or genes
            max_val = abs(max([abs(i) for i in x + y]))
            # Find the max value for all x and y
            neg_max_val = max_val * -1

        # do the plotting
        regression_plot(ax=ax[i], x=x, y=y, max_x_axis_2_plot=None, max_y_axis_2_plot=None,
                        x_axis_label=f"{assay1_name} {x_value} CNR Values",
                        y_axis_label=f"{assay2_name} {y_value} CNR Values",
                        plot_reference_line=False,
                        size_point=10, r_value_squared_threshold=0.00,
                        horizontal_threshold_line_label=None,
                        add_2_x_axis="", add_2_y_axis="", legend_location=None)
        ax[i].axhline(y=0, color='r', linestyle='-')
        ax[i].axvline(x=0, color='r', linestyle='-')

        # set the min and max
        #ax[i].set_ylim(neg_max_val, max_val)
        #ax[i].set_xlim(neg_max_val, max_val)
        reference_line = None
        if values_to_list1 == 'gene':
            if x_value in ('ERBB2', 'EGFR'):
                ax[i].set_ylim(-2, 6)
                ax[i].set_xlim(-2, 6)
                reference_line = [*range(round(-2), round(6) + 1)]
            elif x_value in ('BAP1', 'CCNE1', 'FGFR3', 'NOTCH1', 'TSC2'):
                ax[i].set_ylim(-2, 3)
                ax[i].set_xlim(-2, 3)
                reference_line = [*range(round(-2), round(3) + 1)]
            elif x_value in ('APC', 'BRIP1', 'CCND1', 'FGF19', 'FGFR1', 'MDM2', 'MYC', 'MYCN', 'RAD51C', 'RAD51D',
                             'RET', 'VHL'):
                ax[i].set_ylim(-2, 2)
                ax[i].set_xlim(-2, 2)
                reference_line = [*range(round(-2), round(2) + 1)]
            else:
                ax[i].set_ylim(-1.5, 1)
                ax[i].set_xlim(-1.5, 1)
                # create the 45- degree line
                reference_line = [*range(round(-1.5), round(1) + 1)]


        # set Title
        title = f"CNR Values: {y_value} vs. {x_value}, Avg: CNR diff {avg_cnr_diff} n={len(x)}"
        ax[i].set_title(title, fontdict=None, loc='center', pad=None)
        if reference_line is not None:
            ax[i].plot(reference_line, reference_line, 'r--', label='45 degree line')

    if num_unique_samples == 2:  # see comment above as to why 2
        fig.delaxes(ax[1])

    fig.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(image_filename)
    df_file_name, _ = os.path.splitext(image_filename)
    final_df['cnr_diff'] = abs(final_df['log2cnr_x'] - final_df['log2cnr_y'])
    print_data_frame(df=final_df, tabular_output_file=f"{df_file_name}.txt")
    plt.close(fig)


def get_cnv_data_frame_from_input_file(input_file=None, dir1=None, dir2=None, cnv_gene_filter_bool=None,
                                       sample_type='tumor'):
    """
    This function will take the input file which is tab delimited, assay1 in first column and assay2 plus in the second
    it will then get data frames for each, and merge them on gene so we can compare the metrics like log2ratio
    it wll also calculate some additional columns like est. copy number gain for positive log2ratios
    @param input_file: The tab delimited input file to process
    @param dir1: directory where the files are stored for assay1
    @param dir2: directory where the files are stored for assay2
    @param cnv_gene_filter_bool: Report out only important CNV (True/False)
    @param sample_type: tumor or normal

    """
    final_list_gene_df = []
    final_list_probes_df = []

    with open(input_file) as in_fh:
        for line in in_fh:
            if line.startswith("#"):
                continue
            file_base1, file_base2 = line.rstrip().split("\t")
            dir1_updated = '/'.join((dir1, file_base1))
            #print(dir1_updated)
            gene_entry_name1 = return_file_type(
                directory=dir1_updated, file_name_base=file_base1, file_type=f'all_chroms.{sample_type}.gene.cnr.txt')
            gene_file_name1 = "/".join((dir1_updated, gene_entry_name1))
            gene_df_assay1 = read_dataset_and_get_pandas_df(col_vals=get_columns_from_cider_gene_cnv_file(),
                                                            file_name=gene_file_name1)
            all_entry_name1 = return_file_type(
                directory=dir1_updated, file_name_base=file_base1, file_type=f'all_chroms.{sample_type}.cnr.txt')
            all_file_name1 = "/".join((dir1_updated, all_entry_name1))
            #print(all_file_name1)
            all_df_assay1 = read_dataset_and_get_pandas_df(col_vals=get_columns_from_cider_cnv_file(),
                                                           file_name=all_file_name1)
            # get a dictionary of the number of primers per gene in assay1
            dict_gene_2_num_primers = get_dict_gene_2_num_primers(file=get_oc120plus_gtf_file())
            # use the dictionary of primers ot update the data frame
            gene_df_assay1['num_primers'] = \
                gene_df_assay1.apply(lambda row: _update_gene_with_primer(row=row,
                                                                          gene_2_primer_dict=dict_gene_2_num_primers),
                                     axis=1)
            # calculate the absolute copy number gain
            gene_df_assay1['copy_number'] = \
                gene_df_assay1.apply(lambda row: calculate_absolute_copy_number_gain(
                    row=row, df_col_name_log2_ratio='log2cnr'), axis=1)
            gene_df_assay1['copy_number_gain_loss'] = gene_df_assay1.apply(
                lambda row: calculate_estimated_copy_number_gain_loss(
                    row=row, df_col_name_absol_copy_number='copy_number', df_col_name_log2_ratio='log2cnr'),
                axis=1)

            gene_df_assay1['sample_name'] = gene_entry_name1
            all_df_assay1['sample_name'] = gene_entry_name1
            # update the cid in assay1
            gene_df_assay1['sample_cid'] = gene_df_assay1.apply(lambda row: get_cid_from_data_frame_row(row), axis=1)
            all_df_assay1['sample_cid'] = all_df_assay1.apply(lambda row: get_cid_from_data_frame_row(row), axis=1)
            # needs sample_cid
            gene_df_assay1['copy_number_cbio_portal'] = gene_df_assay1.apply(
                lambda row: calculate_cbio_portal_value(
                    row=row, df_col_name_log2_ratio='log2cnr', df_col_name_cid='sample_cid'),
                axis=1)
            all_df_assay1['copy_number_cbio_portal'] = all_df_assay1.apply(
                lambda row: calculate_cbio_portal_value(
                    row=row, df_col_name_log2_ratio='log2cnr', df_col_name_cid='sample_cid'),
                axis=1)

            # get a dict of genes from assay1
            genes_assay1_dict = {i: 1 for i in set(gene_df_assay1['gene'].tolist())}
            # clean up the dict of genes that should not be compared
            update_dict(dict_2_update=genes_assay1_dict)
            # subset it down to those genes, since we removed some of the non-standard gene names
            gene_df_assay1 = gene_df_assay1[gene_df_assay1['gene'].isin(list(genes_assay1_dict.keys()))]

            ############################## Refactor to use one function to do this

            # get the file for assay2
            dir2_updated = '/'.join((dir2, file_base2))
            gene_entry_name2 = return_file_type(directory=dir2_updated, file_name_base=file_base2,
                                                file_type=f'all_chroms.{sample_type}.gene.cnr.txt')
            gene_file_name2 = "/".join((dir2_updated, gene_entry_name2))
            gene_df_assay2 = read_dataset_and_get_pandas_df(col_vals=get_columns_from_cider_gene_cnv_file(),
                                                          file_name=gene_file_name2)

            all_entry_name2 = return_file_type(
                directory=dir2_updated, file_name_base=file_base2, file_type=f'all_chroms.{sample_type}.cnr.txt')
            all_file_name2 = "/".join((dir2_updated, all_entry_name2))
            #print(all_file_name2)
            all_df_assay2 = read_dataset_and_get_pandas_df(col_vals=get_columns_from_cider_cnv_file(),
                                                       file_name=all_file_name2)

            # get a dictionary of the number of primers per gene in assay2
            dict_gene_2_num_primers = get_dict_gene_2_num_primers(file=get_oc120plus_gtf_file())
            # use the dictionary of primers ot update the data frame
            gene_df_assay2['num_primers'] = gene_df_assay2.apply(lambda row:
                                                             _update_gene_with_primer(
                                                             row=row, gene_2_primer_dict=dict_gene_2_num_primers),
                                                             axis=1)

            gene_df_assay2['copy_number'] = gene_df_assay2.apply(
                lambda row: calculate_absolute_copy_number_gain(row=row, df_col_name_log2_ratio='log2cnr'), axis=1)
            gene_df_assay2['copy_number_gain_loss'] = gene_df_assay2.apply(
                lambda row: calculate_estimated_copy_number_gain_loss(
                    row=row, df_col_name_absol_copy_number='copy_number', df_col_name_log2_ratio='log2cnr'),
                axis=1)

            # add the file name
            gene_df_assay2['sample_name'] = gene_entry_name2
            all_df_assay2['sample_name'] = gene_entry_name2
            # update the cid in assay2
            gene_df_assay2['sample_cid'] = gene_df_assay2.apply(lambda row: get_cid_from_data_frame_row(row), axis=1)
            all_df_assay2['sample_cid'] = all_df_assay2.apply(lambda row: get_cid_from_data_frame_row(row), axis=1)
            # needs sample_cid
            gene_df_assay2['copy_number_cbio_portal'] = gene_df_assay2.apply(
                lambda row: calculate_cbio_portal_value(
                    row=row, df_col_name_log2_ratio='log2cnr', df_col_name_cid='sample_cid'),
                axis=1)
            all_df_assay2['copy_number_cbio_portal'] = all_df_assay2.apply(
                lambda row: calculate_cbio_portal_value(
                    row=row, df_col_name_log2_ratio='log2cnr', df_col_name_cid='sample_cid'),
                axis=1)

            # subset it down
            gene_df_assay2 = gene_df_assay2[gene_df_assay2['gene'].isin(list(genes_assay1_dict.keys()))]

            # merge them
            gene_final_df = pd.merge(gene_df_assay1, gene_df_assay2, on='gene')
            # can't merge on start end, b/c gs180 used full intervals, and gs180 + SNPs used 10bp in the GSPs in
            # a more logical way also, with gs180 + SNPs we selected down on intervals.
            # We can use 'pos', 'gene', and 'exon'
            all_final_df = pd.merge(all_df_assay1, all_df_assay2, on=['pos', 'gene_symbol', 'exon'])
            # remove the None gene
            gene_final_df = gene_final_df[~gene_final_df.gene.str.contains("None")]
            all_final_df = all_final_df[~all_final_df.gene_symbol.str.contains("None")]
            # some log2ratios are -inf so remove them
            all_final_df = all_final_df.replace([np.nan, -np.inf], 0)
            #all_final_df = all_final_df[~all_final_df.isin([-np.inf]).any(1)]
            if cnv_gene_filter_bool is True:
                # keep only important CNV genes
                gene_final_df = gene_final_df[gene_final_df.gene.isin(get_list_important_cnv_genes())]
                all_final_df = all_final_df[all_final_df.gene_symbol.isin(get_list_important_cnv_genes())]

            # add them to the list of df
            final_list_gene_df.append(gene_final_df)
            final_list_probes_df.append(all_final_df)
            #dispersion_df1 = pd.DataFrame({all_df_assay1['sample_cid'][0]: all_final_df['log2cnr_x'].describe()})
            #dispersion_df2 = pd.DataFrame({all_df_assay2['sample_cid'][0]: all_final_df['log2cnr_y'].describe()})
            #print(dispersion_df1, dispersion_df2)
    gene_df = pd.concat(final_list_gene_df).reset_index(drop=True)
    all_df = pd.concat(final_list_probes_df).reset_index(drop=True)
    all_df.rename(columns={'gene_symbol': 'gene'}, inplace=True) # make this consistent with gene_df
    return gene_df, all_df


def calculate_cbio_portal_value(row=None, df_col_name_log2_ratio=None, df_col_name_cid=None):
    """
    @param row:  Row from the data frame with the log2 ratio
    @param df_col_name_log2_ratio: column name for the log2 ratio
    @param thresholds: Dictionary of Dictionaries for of copy-state(s)
    """
    # get threshold determined by the CID's tumor purity if it's found, else it will use global tumor purity
    thresholds = get_copy_number_thresholds(
        copy_state_for_high_level_amplification=COPY_STATE_FOR_HIGH_LEVEL_AMPLIFICATION,
        tumor_purity=cid_to_tumor_percentage(cid=row[df_col_name_cid]))

    return get_cbio_value(row[df_col_name_log2_ratio], thresholds=thresholds,
                          copy_state_for_high_level_amplification=COPY_STATE_FOR_HIGH_LEVEL_AMPLIFICATION)


def cid_to_tumor_percentage(cid=None):
    data = {
        "cid21-1555": .40,
        "cid21-1558": .60,
        "cid21-448": .70,
        "cid21-505": .70,
        "cid21-551": .30,
        "cid21-765": .40,
        "cid21-557": .80,
        "cid21-581": .30,
        "cid21-1108": .90,
        "cid21-1126": .90,
        "cid21-1112": .60,
        "cid21-2375": .40,
        "cid21-2377": .60,
        "cid21-2378": .70,
        "cid21-2380": .70,
        "cid21-2381": .30,
        "cid21-2382": .40,
        "cid21-2374": .80,
        "cid21-2383": .30,
        "cid21-2384": .90,
        "cid21-2379": .90,
        "cid21-2376": .60,
        'cid21-2645': .50,
        'cid21-2625': .60,
        'cid21-484': .80,
        'cid21-486': .30,
        'cid21-555': .60,
        'cid21-494': .50,
        'cid21-775': .90,
        'cid21-1665': .50,
        'cid21-1515': .50,
        'cid21-1970': .90,
        'cid22-46': .30,
        'cid22-72': .60,
        'cid22-909': .50,
        'cid22-910': .60,
        'cid22-904': .80,
        'cid22-905': .30,
        'cid22-907':.60,
        'cid22-906': .50,
        'cid22-908': .90,
        'cid22-903': .50,
        'cid22-902': .50,
        'cid22-911': .90,
        'cid22-912': .30,
        'cid22-913': .60,
    }
    data = {}
    if cid in data:
        return data[cid]
    else:
        print(f"CID {cid} not found, using global tumor purity, update cid_to_tumor_percentage if you want precise",
              file=sys.stderr)
        return GLOBAL_TUMOR_PURITY


def calculate_estimated_copy_number_gain_loss(row=None, df_col_name_absol_copy_number=None, df_col_name_log2_ratio=None):
    """
    @param row:  Row from the data frame with the log2 ratio
    @param df_col_name_absol_copy_number: Absolute copy number from the log2 ratio
    @param df_col_name_log2_ratio: column name for the log2 ratio
    If it was a positive copy number again, then subtract 2 for diploid
    else then it was a loss and keep the log2 ratio since its an estimate, Remember:
    a single-copy loss is log2(1/2) = -1.0, both copies loss would be log2(.5/2) = -2.0
    **IMPORTANTLY, this assumes 100 tumor purity
    @param
    """
    log2_ratio = row[df_col_name_log2_ratio]
    if log2_ratio > 0:
        return row[df_col_name_absol_copy_number] - 2
    elif log2_ratio <= 0 and log2_ratio >= -0.75:
        return 0
    elif log2_ratio < -0.75 and log2_ratio >= -1.5:
        return -1
    elif log2_ratio < -1.5:
        return -2
    else:
        raise Exception("Missed condition")


def calculate_absolute_copy_number_gain(row=None, df_col_name_log2_ratio=None):
    """
    In a diploid genome, a single-copy gain in a perfectly pure, homogeneous sample has a copy ratio of 3/2.
    In log2 scale, this is log2(3/2) = 0.585, and a single-copy loss is log2(1/2) = -1.0, both copies loss
    would be log2(.5/2) = -2.0
    @param row:  Row from the data frame with the log2 ratio
    @param df_col_name_log2_ratio: The column name to find the log2 ratio, e.g. 'log2cnr'
    log2(x / 2) = CNR, where CNR is the log2 ratio, then taking x - 2 (diploid genome contains 2 copies of a gene)
    solve for X
    2^CNR * 2 = x
    """
    return (2 ** row[df_col_name_log2_ratio]) * 2

def _update_gene_with_primer(row=None, gene_2_primer_dict=None):
    if row['gene'] in gene_2_primer_dict:
        return gene_2_primer_dict[row['gene']]
    else:
        return 0


def get_dict_gene_2_num_primers(file=None):
    dict_gene_2_numer_primers = {}
    with open(file) as infh:
        for line in infh:
            value = line.rstrip().split("\t")[8].split("_")[0]
            value = value.replace('name "', '')
            dict_gene_2_numer_primers[value] = dict_gene_2_numer_primers.get(value, 0) + 1
    return dict_gene_2_numer_primers


def return_file_type(directory=None, file_name_base=None, file_type=None):
    """
    @param directory: A string that's a directory to search for the file_ext type
    @param file_name_base: A string that's the file name base match in the directory (this is typically the direcotry
    cider outputs for the sample
    @param file_type: What is the ending of the file to match
    @return string that matches the file_ext type
    """
    #print(directory)
    with os.scandir(directory) as entries:
        for entry in entries:  # entry is an object!
            if entry.is_file():
                if file_name_base in entry.name and file_type in entry.name:
                    return entry.name

def update_dict(dict_2_update=None):
    for key in ('12q24.33', '14q32.33', '15q14', '16q23.1', '17q12', '17q25.3', '19q13.2', '1p34.2', '20q13.1',
                '21q22.3', '22q12.3', '2p16.3', '2p25.1', '5p15.33', '7q33', '8q24.3', '9q34.3', 'X27.2', 'Xp21.32',
                'Xp22.2', 'Xp22.32', 'Xp24', 'LOC284294'):
        if key in dict_2_update:
            del dict_2_update[key]


def get_cnv_data_frame_from_directory(dir=None):
    """
    @param dir: The directory where to look and get the CNV data
    @return: Data frame (pandas)
    """
    list_dfs = []
    with os.scandir(dir) as entries:
        for entry in entries:  # entry is an object!
            if entry.is_file():
                path, ending = os.path.splitext(entry.name)
                file_name = "/".join((dir, entry.name))
                if 'all_chroms.tumor.gene.cnr' in path:
                    df = read_dataset_and_get_pandas_df(col_vals=get_columns_from_cider_gene_cnv_file(),
                                                        file_name=file_name)
                    df['sample_name'] = entry.name
                    list_dfs.append(df)
    return pd.concat(list_dfs)


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Output the gene CNR values for two batches so you can analyzed')

    parser.add_argument('--path_assay1', dest='path_assay1',
                        type=str, help='Destination of directory 1 cnv files are found',
                        required=True)
    parser.add_argument('--assay1_name', dest='assay1_name',
                        type=str, help='Assay 1 name, this is the assay in the 1st column',
                        required=True)
    parser.add_argument('--path_assay2', dest='path_assay2',
                        type=str, help='Destination of directory 2 cnv files are found',
                        required=True)
    parser.add_argument('--assay2_name', dest='assay2_name',
                        type=str, help='Assay 2 name, this is the assay in the 2nd column',
                        required=True)
    parser.add_argument('--cnv_file', dest='cnv_file',
                        type=str, help='Tab delimited file with assay1 in the first column and assay2 in second',
                        required=True)
    parser.add_argument('--cnv_gene_filter_bool', dest='cnv_gene_filter_bool',
                        default=False, action='store_true',
                        help='If set to True (default), then only genes Rodrigo has indicated are important '
                             'for CNVs will be reported')
    parser.add_argument('--cluster_type', dest='cluster_type', 
                        help="Cluster method",
                        default=None, required=True, type=str,
                        choices=['single', 'complete', 'average', 'ward', 'centroid'])

    parser.add_argument('--sample_type', dest='sample_type',
                        help="Were these files tumor or normal, default tumor",
                        default='tumor', choices=['tumor', 'normal'])

    return parser.parse_args()


if __name__ == "__main__":
    main()