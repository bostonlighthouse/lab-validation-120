"""Find the variant that are recalled in OC120plus vs Genosity"""
import argparse
import operator
import sys
from lab_validation_120.utils.configs.oc120_genosity_config import get_genosity_vs_oc120plus_sample_recall
from lab_validation_120.utils.genosity_utilities import process_genosity_samples_for_recall
from lab_validation_120.utils.concordance_utilites import prepare_matched_results_for_printing
from lab_validation_120.utils.configs.oc395.oc395_genosity_config import get_genosity_vs_oc395boosted_primer_sample_recall_all_samples, \
    get_genosity_vs_oc395boosted_primer_sample_recall_small_subset_samples, \
    get_genosity_vs_oc395boosted_primer_sample_recall_all_samples_no_germline


def main():
    args = get_cli_args()
    config = None
    key_to_search = None
    if args.which_analysis == 'oc120plus':
        config = get_genosity_vs_oc120plus_sample_recall()
        key_to_search = "genosity_vs_oc120plus_sample_recall"
    elif args.which_analysis == 'oc395primer_boost_33_samples':
        config = get_genosity_vs_oc395boosted_primer_sample_recall_all_samples()
        #config = get_genosity_vs_oc395boosted_primer_sample_recall_small_subset_samples()
        #key_to_search = 'genosity_vs_oc395boosted_primers_sample_recall'
        key_to_search = 'clinical_sample_reproducibility'
    elif args.which_analysis == 'oc395primer_boost_33_samples_no_germline':
        config = get_genosity_vs_oc395boosted_primer_sample_recall_all_samples_no_germline()
        key_to_search = 'clinical_sample_reproducibility'

    # over write the config if passed in
    if args.consequential_impacts_only is True:
        config[key_to_search]['consequential_impacts_only'] = True
    else:
        config[key_to_search]['consequential_impacts_only'] = False

    depth_threshold = args.depth_threshold
    vaf_threshold = args.vaf_threshold
    # get the df of matched values given the thresholds
    final_matched_results, recall_reasons_for_skipping_or_keeping, total_num_samples, recall_concordant, \
        recall_discordant = process_genosity_samples_for_recall(config=config, vaf_threshold=vaf_threshold,
                                                                depth_threshold=depth_threshold,
                                                                key_to_search=key_to_search)

    total_variants_found = 0
    print(f"\nPrinting numbers of variants analyzed in {total_num_samples} samples\n", file=sys.stderr)
    for reason, num in sorted(recall_reasons_for_skipping_or_keeping.items(), key=operator.itemgetter(1), reverse=True):
        total_variants_found += num
        print(reason, num, file=sys.stderr)
    print(f"\n\nTotal variants found passing artifact filtering {total_variants_found}", file=sys.stderr)
    if 'keep' in recall_reasons_for_skipping_or_keeping:
        print(f"Total variants removed due to additional filtering (e.g. germline, strand_bias, variant_location) "
              f"{total_variants_found - recall_reasons_for_skipping_or_keeping['keep']}",
              file=sys.stderr)
        print(f"Total variants kept {recall_reasons_for_skipping_or_keeping['keep']}", file=sys.stderr)

    print(f"Total recall concordant found {recall_concordant}", file=sys.stderr)
    print(f"Total recall discordant found {recall_discordant}", file=sys.stderr)

    #import pprint as pp
    #pp.pprint(final_matched_results)

    prepare_matched_results_for_printing(args=args, final_matched_results=final_matched_results,
                                         vaf_threshold=vaf_threshold, depth_threshold=depth_threshold,
                                         filter_hotspot_caller=False, only_print_intra_inter_tables=True)


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    @returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Process the Genosity samples according to configs concordance '
                                                 'sample data')

    parser.add_argument('--tabular_output_file', dest='tabular_output_file',
                        type=str, help='Output file to store the data', required=True)

    parser.add_argument('--vaf_threshold', dest='vaf_threshold',
                        type=float, help='Variant allele fraction threshold, e.g. 5.0 = 5.0 percent',
                        required=True, default=None)

    parser.add_argument('--depth_threshold', dest='depth_threshold',
                        type=int, help='Depth of coverage threshold',
                        required=True, default=None)

    parser.add_argument('--print_final_output_header', dest='print_final_output_header',
                        help='Print the header line of final output (True)', required=False,
                        action='store_true', default=False)

    help_str = "This flag will only include consequential transcript impacts, i.e. not things like synonymous_variant" \
               " or 3_prime_UTR_variant.  See (is_a_consequential_impact) in vcf_utilities.py for more information"

    parser.add_argument('--consequential_impacts_only', dest='consequential_impacts_only',
                        action='store_true', default=False,
                        help=help_str)

    parser.add_argument('--which_analysis', dest='which_analysis', required=True, type=str,
                        help="Analyze which of the comparisons we did?",
                        choices=['oc120plus',
                                 'oc395primer_boost_33_samples',
                                 'oc395primer_boost_33_samples_no_germline'])

    return parser.parse_args()


if __name__ == "__main__":
    main()

