import argparse
import os
from lab_validation_120.utils.genosity_utilities import get_genosity_variant_data, get_genosity_strand_bias_ratio

def main():

    args = get_cli_args()
    process_genosity_files(infile=args.genosity_summary_files)


def get_list_genosity_gs395_poe():
    return [
        'POE20_114_',
        'POE20_238_',
        'POE20_338_',
        'POE20_235_',
        'POE20_344_',
        'POE20_227_',
        'POE20_175_',
        'POE20_83_',
        'POE20_226_',
        'POE20_352_',
        'POE20_247_',
        'POE20_271_',
        'POE20_278_',
        'POE20_172_',
        'POE20_282_',
        'POE20_239_',
        'POE20_353_',
        'POE20_148_',
        'POE20_200_',
        'POE20_144_',
        'POE20_97_',
        'POE20_210_',
        'POE20_216_',
        'POE20_107_',
        'POE20_85_',
        'POE20_111_',
    ]

def is_genosity_file_a_gs395(file: str = None)-> bool:
    for poe in get_list_genosity_gs395_poe():
        if poe in file:
            return True
    return False

def process_genosity_files(infile: str = None):

    genosity_dir = 'lab_validation_120/external_data_sources/genosity'
    with open(infile, 'r') as infh:
        for genosity_file in infh:
            genosity_file = genosity_file.rstrip()
            if genosity_file.startswith('#'):
                continue
            final_file = os.path.join(genosity_dir, genosity_file)
            if not is_genosity_file_a_gs395(file=genosity_file):
                continue
            #print(genosity_file)
            genosity_sample_data = get_genosity_variant_data(final_file)
            tmb_score = get_tmb_from_genosity_data(data=genosity_sample_data)
            #quit()
            print(f"{genosity_file}\t{tmb_score}")

def get_tmb_from_genosity_data(data: dict = None) -> float:
    count = 0
    seen = {}
    for key in data:
        # do we include 'stop loss', 'start loss' ?
        if data[key]['VariantEffect'] not in ['missense', 'stop gain', 'frameshift', 'in-frame']:
            continue
        if data[key]['Comparison summary'] == 'Germline':  # was a germline variant
            continue
        if data[key]['FDP'] < 25:  # tumor depth at the allele
            continue
        if data[key]['FAD'] < 3:  # how many alleles observed?  AO
            continue
        if data[key]['FAF'] < 5.0:  # Allele fraction, AF
            continue
        if len(data[key]['Alt']) > 25:
            continue
        if len(data[key]['Ref']) > 25:
            continue
        if data[key]['Gnomad_MaxAltFreq'] > 0.005:
            continue
        if data[key]['1000G_MaxAF'] > 0.005:
            continue
        if data[key]['HomopolymerSpan'] > 4:
            continue
        if data[key]['NormalFAF'] >= 1.0:
            continue
        if data[key]['VariantType'] == 'substitution' and len(data[key]['Ref']) > 1:
            # Genosity has strange events:  'Ref': 'TGCC', 'Alt': 'CGCT',
            # The strand bias cannot be calculated on these...
            #print(key, data[key]['Ref'], data[key]['Alt'])
            pass
        elif get_genosity_strand_bias_ratio(genosity_dict=data[key]) < .40:
            continue
        # Genosity has multiple variants in teh same allele, so just remove it if it gets this far
        chromosome, start, _, ref_allele, alt_allele, counter = key.split('_')
        new_key = '_'.join((chromosome, start, ref_allele, alt_allele))
        if new_key in seen:
            continue
        seen[new_key] = 1
        #print(key, data[key], "\n")
        #print(f"Gnomad_MaxAltFreq {data[key]['ExAC_MaxAltAF']} ExAC_MaxAltAF {data[key]['ExAC_MaxAltAF']} 1000G_MaxAF {data[key]['1000G_MaxAF']}")
        count += 1
    #quit()
    return get_tmb_score(count=count)


def get_tmb_score(count, sig_rounding=3):
    """
    Takes number of variants that passed and calculates the TMB
    :param sig_rounding: Number of significant digits to round to
    :return: float for the TMB score
    """

    # calculate the TMB score, 1000000 = 1 Megabase, and TMB is the number of variants per Megabase
    # found 38_491_095 by:
    # awk '{ SUM += ($3-$2)+1} END { printf "%.2f",  SUM } ' /Users/cleslin/Downloads/XGenExomeV2_Regions.bed
    # https://www.idtdna.com/pages/products/next-generation-sequencing/workflow/xgen-ngs-hybridization-capture/pre-designed-hyb-cap-panels/exome-hyb-panel-v2
    denominator = (39_000_000 / float(1000000))
    tmb_score = round((count / denominator), sig_rounding)
    return tmb_score


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    @returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Find the variants that overlap the GS v1 or GS v2.0 BED file')

    parser.add_argument('--genosity_summary_files', dest='genosity_summary_files',
                        type=str, help='The variant files from genosity, one per line',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()


"""
less POE19_363_GER2016627_D1_N1_LP201009248_xGen_UDI_Index_23_HP200591_PL200327_SEQ_2010220277.14888.variants.summary.tumor.comparison.tsv | cut -f 39 | sort | uniq -c | sort -k1,1
   1 VariantEffect
  16 stop loss
  30 start loss
 132 stop gain
 418 frameshift
 582 in-frame
11892 missense
12475 synonymous
"""