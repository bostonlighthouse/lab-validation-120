import argparse
import csv
import os

from lab_validation_120.utils.genosity_utilities import get_genosity_variant_data
from lab_validation_120.utils.pandas_dataframes_utilities import \
    get_columns_from_genosity_variants_summary_tumor_comparison_tsv
from lab_validation_120.utils.genomic_metadata_utilities import variant_position_overlapped_bed_file, \
    get_df_bed_intervals_by_assay


def main():
    args = get_cli_args()
    which_oc_assay = args.which_oc_assay
    process_list_of_genosity_data(file_list=args.genosity_summary_files,
                                  which_oc_assay=which_oc_assay,
                                  filter_germline_variants=args.filter_germline_variants)


def process_list_of_genosity_data(file_list: str = None, which_oc_assay: str = None,
                                  filter_germline_variants: bool = False):
    """
    Go over the input file and process each of the Genosity samples
    :param file_list: Input file with a list of the files to process
    :param which_oc_assay:  This will be used to open which BED file.
    :param filter_germline_variants: Boolean on whether to filter out Germline calls in the Genosity Data
    """

    df_assay_bed = get_df_bed_intervals_by_assay(which_oc_assay=which_oc_assay)

    # get the list of dictionaries from the BED file
    list_of_dict_df_bed = df_assay_bed.to_dict('records')

    # open the infile list of genosity data
    final_list_output_file = []
    with open(file_list) as fh:
        for file in fh:
            file = file.rstrip()
            # name of the output file to store
            final_output_file = os.path.splitext(file)[0] + f".overlapping_{which_oc_assay}_" \
                                                            f"filter_germline_variants_{filter_germline_variants}.tsv"
            final_list_output_file.append(final_output_file)
            dict_of_dict_genosity_set_variants = \
                get_genosity_variant_data(file=file, filter_germline_variants=filter_germline_variants)

            # go over each entry in the genosity dictionary and find out where there was overalp
            # and write them to an output file
            find_oc_assay_overlapping_genosity_variants(
                dict_of_dict_genosity_set_variants=dict_of_dict_genosity_set_variants,
                list_of_dict_df_bed=list_of_dict_df_bed,
                output_file=final_output_file
            )
    # write the final list of new files
    final_output_file = os.path.splitext(file_list)[0] + f"_list_overlapping_{which_oc_assay}_files.txt"
    with open(final_output_file, 'w') as fh:
        for file in final_list_output_file:
            print(file, file=fh)


def find_oc_assay_overlapping_genosity_variants(dict_of_dict_genosity_set_variants: dict,
                                                list_of_dict_df_bed: list, output_file: str):
    """
    Go over all the genosity data and find out if there was overlap with the oc120plus bedfile.  If there was an
    overlap with the variant, write that entry out to a condensed version of the genosity output file in output_file
    :param dict_of_dict_genosity_set_variants: A dict of dictionaries of all the genosity data.  Key look like the
    following: 1_877831_._T_C

    :param list_of_dict_df_bed: A list of dictionaries from the BED file:
    {'CHROM': '1', 'START_ZERO_BASED': 6846842, 'END': 6846843, 'NAME': 'CAMTA1_l58', 'SCORE': 0, 'STRAND': '+',
    '_START1': 6846842, '_STOP1': 6846843}

    :param output_file: Where to store the data of overlapped genosity variant entries that overlapped the BED file
    """

    list_dict_overlapping_genosity_variants = []  # return a list of df of all data, that can later be printed out
    # go over all entries in the golden set and see if there is correlation
    for i, (key, value) in enumerate(dict_of_dict_genosity_set_variants.items()):
        pos = dict_of_dict_genosity_set_variants[key]['Pos']
        chrom = dict_of_dict_genosity_set_variants[key]['Chrom']
        # was there overlap, if so print out the data
        if variant_position_overlapped_bed_file(chrom=chrom, position=pos, list_of_dict_df_bed=list_of_dict_df_bed):
            list_dict_overlapping_genosity_variants.append(dict_of_dict_genosity_set_variants[key])
            
    with open(output_file, 'w') as fh:
        writer = csv.DictWriter(fh, fieldnames=get_columns_from_genosity_variants_summary_tumor_comparison_tsv(),
                                delimiter='\t')
        writer.writeheader()
        writer.writerows(list_dict_overlapping_genosity_variants)


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    @returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Find the variants that overlap the GS v1 or GS v2.0 BED file')

    parser.add_argument('--genosity_summary_files', dest='genosity_summary_files',
                        type=str, help='The variant files from genosity',
                        required=True)

    parser.add_argument('--filter_germline_variants',
                        dest='filter_germline_variants',
                        action='store_true',
                        help='Filter out germline variants when filtering the Genosity TSV file',
                        default=False)

    parser.add_argument('--which_oc_assay', dest='which_oc_assay', required=True, type=str,
                        help="must be oc120/oc120Plus/oc120archer/oc395boosted_primers",
                        choices=['oc120', 'oc120plus', 'oc120archer', 'oc395boosted_primers'])

    return parser.parse_args()


if __name__ == "__main__":
    main()