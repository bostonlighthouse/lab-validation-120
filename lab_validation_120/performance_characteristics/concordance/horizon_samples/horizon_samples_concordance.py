"""Module to complete the concordance with the Horizon samples"""
import argparse
import sys
import pandas as pd

from lab_validation_120.utils.genomic_metadata_utilities import is_overlapping
from lab_validation_120.utils.horizon_concordance_utilities import process_all_horizon_samples_for_variants
from lab_validation_120.utils.plots.regression_plot_utilities import process_regression_and_bland_altman_analysis, plot_regression_and_bland_altman
from lab_validation_120.utils.pandas_dataframes_utilities import read_dataset_and_get_pandas_df, print_data_frame, \
    get_columns_from_hd827_variant_file, read_dataset_with_no_header_and_get_pandas_df, \
    get_columns_from_bed_with_no_header, get_columns_from_horizon_bed_with_no_header, \
    get_columns_from_vcf
from lab_validation_120.utils.vcf_utilities import return_most_severe_impact_transcript_obj, is_a_consequential_impact, ignore_caller_because_of_caller_thresholds, \
    is_dbsnp_variant, is_population_variant, get_all_transcript_consequences
from lab_validation_120.utils.configs.horizon_samples_concordance_config import get_concordance_horizon_samples, \
    get_concordance_horizon_samples_qc_plots, get_list_hd827_genes
from lab_validation_120.utils.configs.lab_validation_config import get_oc395_targets_bed_file
from lab_validation_120.utils.r_function_stats import pbinom
from lab_validation_120.utils.concordance_qc_metrics import process_concordance_htqc_plots
import biomodels.models.vcf.vcf_container as vc
from lab_validation_120.utils.pivot_utilities import print_data_frame_pivot_outputs


def main():
    """Main driver of the program"""

    args = get_cli_args()
    config = get_concordance_horizon_samples()

    """
    # based on Band-Altman and distribution I needed to remove 2 outliers from this analysis
    find_hd827_concordance(config=config, non_vcf_file_to_parse='all_variants_for_ngs_file', vaf_key_in_df='NGS_VAF',
                           horizon_analysis_type='NGS', file_name_add_on='2_removed_outliers',
                           print_overall_df=True, test_false_postives=True,
                           horizon_low_confidence_vcf_file1='all_variants_for_ngs_vcf_file_gatk',
                           horizon_low_confidence_vcf_file2='all_variants_for_ngs_vcf_file_lowfreq',
                           horizon_low_confidence_vcf_bed_file='all_variants_for_ngs_vcf_bed')
    find_hd827_concordance(config=config, non_vcf_file_to_parse='all_variants_for_ddpcr_file', vaf_key_in_df='ddPCR_VAF',
                           horizon_analysis_type='ddPCR', file_name_add_on='0_removed_outliers',
                           print_overall_df=False)
    """

    # go over all horizon samples in the config, and compare with the configured VCF file
    group_by_key = "Horizon_ID"  # This can be named whatever you want...
    # set these thresholds, we'll still match, but add whether it matched with these thresholds
    key_to_config_usage = 'concordance_horizon_samples'  # what key to walk into
    depth_threshold = config[key_to_config_usage]['depth_threshold']
    vaf_threshold = config[key_to_config_usage]['vaf_threshold']
    matched_results_df = process_all_horizon_samples_for_variants(config, group_by_key, vaf_threshold,
                                                                   depth_threshold,
                                                                  key_to_config_usage=key_to_config_usage)

    #add_binomial_probabilities_to_df(df=matched_results_df)

    subtitle = f"Thresholds: VAF >= {vaf_threshold} Depth >= {depth_threshold}"
    # at this point all data is in matched_results_df, so the actual regression analysis can be completed
    process_regression_and_bland_altman_analysis(df=matched_results_df, group_by_key=group_by_key,
                                                 images_dir=config[key_to_config_usage]['images_dir'],
                                                 config_plot=config[key_to_config_usage]['vaf_regression_plot'],
                                                 subtitle=subtitle)
    print_data_frame_pivot_outputs(df=matched_results_df, args=args, group_by_key=group_by_key)

    # Output the data
    # have to wait till it matched
    # based on Band-Altman and distribution I needed to remove 3 outliers from this analysis
    #outliers_removed = '3_removed_outliers'
    outliers_removed = None # Not removing any for this analysis

    #(filename, extension) = os.path.splitext(args.tabular_output_file)
    #print_data_frame(matched_results_df, "".join((filename, ".", outliers_removed, extension)),
    #                 index_label="Index", na_rep='ND')

    # now we create an 'All_Grouped' column and repeat the regression for for everything

    all_samples_name = 'All_Grouped'
    matched_results_df[all_samples_name] = "All_Horizon"
    # Re-do regression with all data
    process_regression_and_bland_altman_analysis(df=matched_results_df, group_by_key=all_samples_name,
                                                 images_dir=config[key_to_config_usage]['images_dir'],
                                                 config_plot=config[key_to_config_usage]['vaf_regression_plot'],
                                                 subtitle=subtitle, file_name_add_on=outliers_removed)

    # dictionary needed for process_concordance_htqc_plots
    config = get_concordance_horizon_samples_qc_plots()
    key_to_config_usage = 'concordance_horizon_samples_qc_plots'  # what key to walk into
    # what do you want to name the column that stores the samples
    col_name_4_sample_names = 'HORIZON_SAMPLE'
    name_for_all_samples_combined = "all_horizon"
    list_of_heatmap_values = [("MEAN_COLLAPSED_COVERAGE", 800, 8),
                              ("MEAN_FILTERED_COVERAGE", 1000, 10),
                              ("MEAN_ABS_COVERAGE", 4000, 8)]
    # go over all the concordance plots
    process_concordance_htqc_plots(config=config, key_to_config_usage=key_to_config_usage,
                                   col_name_4_sample_names=col_name_4_sample_names,
                                   list_of_heatmap_values=list_of_heatmap_values,
                                   name_for_all_samples_combined=name_for_all_samples_combined,
                                   htqc_summary_data_filename="horizon_htqc_summary_metrics.txt",
                                   only_high_level_metrics=True)


def find_hd827_concordance(config=None, non_vcf_file_to_parse=None, vaf_key_in_df=None, horizon_analysis_type=None,
                           file_name_add_on=None, print_overall_df=False, test_false_postives=False,
                           horizon_low_confidence_vcf_file1=None, horizon_low_confidence_vcf_file2=None,
                           horizon_low_confidence_vcf_bed_file=None):
    """
    Main function finding the concordance b/t our OC935 run and the Horizon HD827 all variant file
    :param config: simple dictionary of values from the dict passed in
    :param non_vcf_file_to_parse: What horizon sample to parse
    :param vaf_key_in_df: There are two different keys for VAF: NGS_VAF ddPCR_VAF
    :param horizon_analysis_type:  NGS or ddPCR
    :param file_name_add_on:  Just add onto the regression output file
    :param test_false_postives: Do through and look at remaining variants for False Positives
    """

    # get the two files needed for hd827
    horizon_high_confidence_variants_file, ocd395_vcf_file = None, None
    for horizon_sample in config['concordance_horizon_samples']['concordance_files']:
        if horizon_sample['sample_id'] == 'hd827':
            ocd395_vcf_file = horizon_sample['vcf_file']
            horizon_high_confidence_variants_file = horizon_sample[non_vcf_file_to_parse]
            if horizon_low_confidence_vcf_file1 is not None:
                horizon_low_confidence_vcf_file1 = horizon_sample[horizon_low_confidence_vcf_file1]
                horizon_low_confidence_vcf_file2 = horizon_sample[horizon_low_confidence_vcf_file2]
                horizon_low_confidence_vcf_bed_file = horizon_sample[horizon_low_confidence_vcf_bed_file]
            break

    # get the VCF records of entries for HD827 run on OC395
    vcf_container = vc.VcfContainer(ocd395_vcf_file)

    # read the Horizon golden set into a data frame
    df_horizon_golden_set_variants = read_dataset_and_get_pandas_df(file_name=horizon_high_confidence_variants_file,
                                                                    col_vals=get_columns_from_hd827_variant_file())

    # read the OC395 BED file into a data frame
    df_oc395_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_bed_with_no_header(),
                                                                 file_name=get_oc395_targets_bed_file())

    # get the list of dictionaries from the BED file
    list_of_dict_df_oc395_bed = df_oc395_bed.to_dict('records')

    dict_list_of_dict_horizon_golden_set_variants = _get_dict_list_of_dict(chrom="CHROM",
                                                                           df=df_horizon_golden_set_variants)

    # now through the newly created dictionary of horizon data: dict_list_of_dict_horizon_golden_set_variants and see if those
    # hits were found in our vcf_container, if they were found delete them from the vcf_container, so we can test for
    # false positives after.  Also get data for plotting regression
    list_dict_all_data, dict_for_regression = _get_correlation_results(dict_list_of_dict_golden_set_variants=
                                                                       dict_list_of_dict_horizon_golden_set_variants,
                                                                       vcf_container=vcf_container,
                                                                       list_of_dict_df_bed=list_of_dict_df_oc395_bed,
                                                                       vaf_key_in_df=vaf_key_in_df)

    plot_regression_and_bland_altman(dict_sample_vals=dict_for_regression,
                                     images_dir=config['concordance_horizon_samples']['images_dir'],
                                     sample_name="HD827",
                                     x_axis_label="OC395",
                                     y_axis_label=f"Horizon {horizon_analysis_type} HD827",
                                     x_value_key='395_vaf',
                                     y_value_key='horizon_vaf',
                                     file_name_add_on=f"oc395_vs_horizon_hd827_analysis_{horizon_analysis_type}_{file_name_add_on}",
                                     subtitle=f"OC395 vs HD827 {horizon_analysis_type}",
                                     max_x_axis_2_plot=1.0,
                                     value_plotted="VAF",
                                     )

    if print_overall_df is True:
        df2 = pd.DataFrame(list_dict_all_data)
        df2['Differences'] = abs(df2['OC395_VAF'] - df2[vaf_key_in_df])
        df2.sort_values(inplace=True, by=['Differences'], ascending=False)
        print_data_frame(
            df=df2,
            tabular_output_file=f"horizon_hd827_concordance_{horizon_analysis_type}_{file_name_add_on}.txt"
        )

    if test_false_postives is True:
        find_false_positives(horizon_low_confidence_vcf_bed_file=horizon_low_confidence_vcf_bed_file,
                             horizon_low_confidence_vcf_file1=horizon_low_confidence_vcf_file1,
                             horizon_low_confidence_vcf_file2=horizon_low_confidence_vcf_file2,
                             vcf_container=vcf_container)


def find_false_positives(horizon_low_confidence_vcf_bed_file=None, horizon_low_confidence_vcf_file1=None,
                     horizon_low_confidence_vcf_file2=None, vcf_container=None):
    """

    """

    # read the OC395 BED file into a data frame
    df_horizon_bed = read_dataset_with_no_header_and_get_pandas_df(
        col_vals=get_columns_from_horizon_bed_with_no_header(), file_name=horizon_low_confidence_vcf_bed_file)
    # fix the chr1 -> 1 from horizon data
    df_horizon_bed["CHROM"] = df_horizon_bed["CHROM"].replace("chr", "", regex=True)
    # get the first data frame from horizon that was called with gatk
    df_horizon_low_confidence_vcf_file1 = read_dataset_and_get_pandas_df(col_vals=get_columns_from_vcf(),
                                                                         file_name=horizon_low_confidence_vcf_file1,
                                                                         ignore_line_starts_with="##")
    # get the first data frame from horizon that was called with lofreq
    df_horizon_low_confidence_vcf_file2 = read_dataset_and_get_pandas_df(col_vals=get_columns_from_vcf(),
                                                                         file_name=horizon_low_confidence_vcf_file2,
                                                                         ignore_line_starts_with="##")
    # merge the data frames
    df_horizon_low_confidence_vcf_file = pd.concat([df_horizon_low_confidence_vcf_file1,
                                                   df_horizon_low_confidence_vcf_file2])

    # fix the chr1 -> 1 from horizon data
    df_horizon_low_confidence_vcf_file["#CHROM"] = \
    df_horizon_low_confidence_vcf_file['#CHROM'].replace("chr", "", regex=True)

    # get a dictionary of lists from the data
    dict_list_of_dict_horizon_low_confidence_variants = _get_dict_list_of_dict(
        chrom="#CHROM", df=df_horizon_low_confidence_vcf_file)

    # go over all entries in the container and check to see if they were True Positives
    for key in vcf_container.dictionary_vcf_entries.keys():
        # if it was in the horizon data, it was a match
        if key in dict_list_of_dict_horizon_low_confidence_variants:
            continue
        variant_entry_obj = vcf_container.get_key(key)
        variant_transcript_obj = return_most_severe_impact_transcript_obj(variant_entry_obj=variant_entry_obj)
        passed_tp_test, reason, pvalue = is_true_positive(variant_entry_obj=variant_entry_obj,
                                                          variant_transcript_obj=variant_transcript_obj,)
        if passed_tp_test is True:
            print(f"{variant_entry_obj.CHROM} {variant_entry_obj.POS}",
                  f"{variant_entry_obj.most_severe_consequence} {variant_entry_obj.info_internal_2_vcf.ALT_AF}",
                  f"{pvalue}",
                  file=sys.stderr)


def _get_dict_list_of_dict(chrom=None, df=None):
    # Keys in vcf_container look like: 1_4849384_._T_C
    # so create them here
    # get a list of dictionaries from the data frame
    list_of_dictionaries = df.to_dict('records')
    dict_list_of_dict_variants = {}
    for dict_ in list_of_dictionaries:
        # create a key that looks like it does in vcf_container
        key = "_".join((dict_[chrom], str(dict_['POS']), '.', dict_['REF'], dict_['ALT']))
        dict_list_of_dict_variants[key] = dict_

    return dict_list_of_dict_variants


def _get_correlation_results(dict_list_of_dict_golden_set_variants=None, vcf_container=None, list_of_dict_df_bed=None,
                             vaf_key_in_df=None):
    list_dict_all_data = []  # return a list of df of all data, can later be printed out
    dict_for_regression = {}  # this dictionary will be used for printing the regression line
    counter_for_hits = 0  #  # how many of the horizon entries had a hit in OC395

    # go over all entries in the golden set and see if there is correlation
    for i, (key, value) in enumerate(dict_list_of_dict_golden_set_variants.items()):
        variant_entry_obj = vcf_container.get_key(key)
        # key was found, so there was a hit in the OC395 assay
        if key in vcf_container.dictionary_vcf_entries:
            variant_transcript_obj = return_most_severe_impact_transcript_obj(variant_entry_obj=variant_entry_obj)
            # just looking to see what these were called.  Many will be rejected, but it's ok b/c they were called by
            # horizon as "high confidence variants"
            passed_tp_test, reason, pvalue = is_true_positive(variant_entry_obj=variant_entry_obj,
                                                              variant_transcript_obj=variant_transcript_obj,
                                                              ignore_rs_numbers=False,
                                                              ignore_population=False, ignore_consequential_impact=False,
                                                              ignore_caller_thresholds=False)
            # create a dictionary like for regression
            # {0: {'horizon_vaf': 10.7, '395_vaf': 9.9}, 1: {'horizon_vaf': 10.0, '395_vaf': 10.7}
            dict_for_regression[i] = {
               '395_vaf': variant_entry_obj.info_internal_2_vcf.ALT_AF,
               'horizon_vaf':  dict_list_of_dict_golden_set_variants[key][vaf_key_in_df]
            }
            # store all the data in a list of dictionaries
            list_dict_all_data.append(return_cocordance_dictionary(
                dict_list_of_dict_golden_set_variants=dict_list_of_dict_golden_set_variants,
                variant_entry_obj=variant_entry_obj, passed_tp_test=passed_tp_test, reason=reason,
                vaf_key_in_df=vaf_key_in_df, key=key, pvalue=pvalue)
            )
            # remove from the container
            del vcf_container.dictionary_vcf_entries[key]
            counter_for_hits += 1
        # we could have missed it b/c it a place we did not look in OC395, so test for this using the function below
        else:
            overlap = golden_set_variant_overlapped_oc395_bed(chrom=dict_list_of_dict_golden_set_variants[key]['CHROM'],
                                                              position=
                                                              dict_list_of_dict_golden_set_variants[key]['POS'],
                                                              list_of_dict_df_bed=list_of_dict_df_bed)
            if overlap is True:
                print(f"Not found {key} with overlap", file=sys.stderr)
                # orignally had this added here, but then realized it should be removed for the regression analysis
                # keep the commented code for historical purposes to show I had it here originally
                #dict_for_regression[i] = {
                #    '395_vaf': 0,
                #    'horizon_vaf': dict_list_of_dict_golden_set_variants[key][vaf_key_in_df]
                #}
                # store all the data in a list of dictionaries
                list_dict_all_data.append(return_cocordance_dictionary(
                    dict_list_of_dict_golden_set_variants=dict_list_of_dict_golden_set_variants,
                    variant_entry_obj=variant_entry_obj, vaf_key_in_df=vaf_key_in_df, key=key)
                )
            else:
                #print(f"Not found {key} with NO overlap")
                #print(f"Missed Gene {dict_list_of_dict_golden_set_variants[key]['GENE']}")
                pass

    return list_dict_all_data, dict_for_regression


def is_true_positive(variant_entry_obj=None, variant_transcript_obj=None, ignore_rs_numbers=False,
                     ignore_population=False, ignore_consequential_impact=False, ignore_caller_thresholds=False):
    """Function used to test if the variant we found was a TP
    :param variant_entry_obj: The biomodels variant_entry object
    :param variant_transcript_obj: This is the variant_transcript_obj that is the most severe, if you don't pass one in
    it will find it for you
    :param ignore_rs_numbers: Should you ignore the function used to look at dbSNP data, default False
    :param ignore_population: Should you ignore the function used to look at population data, default False
    :param ignore_consequential_impact: Should you ignore the function to test if it's a consequential impact,
    default False
    :param ignore_caller_thresholds: Should you ignore the function to test if there was a filter flag by one of our
    callers, default False
    """
    if variant_transcript_obj is None:
        variant_transcript_obj = return_most_severe_impact_transcript_obj(variant_entry_obj=variant_entry_obj)

    total_depth = variant_entry_obj.info_internal_2_vcf.ALT_DP + variant_entry_obj.info_internal_2_vcf.REF_DP
    # was the gene found in the OC395 variant_entry_obj one that was part of the Horizon sequencing?
    p = 1 - pbinom(5 - 1, total_depth, prob=variant_entry_obj.info_internal_2_vcf.ALT_AF)
    if p < .99:
        return False, "p-value", p
    elif not is_hd827_gene(variant_entry_obj=variant_entry_obj):
        return False, "is_hd827_gene", p
    # only look at consequential impacts
    elif ignore_consequential_impact is False and \
            is_a_consequential_impact(variant_consequence_list=variant_transcript_obj.Consequence) is False:
        return False, "ignore_consequential_impact", p
    # was this circulating in the population?  Then we can ignore
    elif ignore_population is False and is_population_variant(variant_entry_obj=variant_entry_obj):
        return False, "ignore_population", p
    # was this a dbSNP variant that was not a COSMIC ID
    elif ignore_rs_numbers is False and is_dbsnp_variant(variant_entry_obj=variant_entry_obj, rescue_cosmic=True):
        return False, "ignore_rs_numbers", p
    # there are many variants that need to be filtered
    elif ignore_caller_thresholds is False:
        ignore, reason2 = ignore_caller_because_of_caller_thresholds(variant_entry_obj=variant_entry_obj,
                                                                     variant_transcript_obj=variant_transcript_obj)
        if ignore is True:
            return False, "_".join(("ignore_caller_because_of_caller_thresholds", reason2)), p
    # was it a hotspotter
    elif variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER == 'hotspotter':
        print(f"{variant_entry_obj.CHROM} {variant_entry_obj.POS} "
              f"{variant_entry_obj.most_severe_consequence} {variant_entry_obj.info_internal_2_vcf.ALT_AF}",
              file=sys.stderr)
        return False, "hotspotter", p
    return True, "Passed", p



def is_hd827_gene(variant_entry_obj=None):

    # get all transcripts
    all_transcript_consequences = get_all_transcript_consequences(variant_entry_obj=variant_entry_obj)
    hd827_genes = get_list_hd827_genes()
    for info_vep in all_transcript_consequences:
        if info_vep.SYMBOL in hd827_genes:
            return True
    return False


def return_cocordance_dictionary(dict_list_of_dict_golden_set_variants=None, variant_entry_obj=None,
                                 passed_tp_test=None, reason=None, vaf_key_in_df=None, key=None, pvalue=None):

    total_dp = 'ND'
    if variant_entry_obj is not None:
        total_dp = variant_entry_obj.info_internal_2_vcf.ALT_DP + variant_entry_obj.info_internal_2_vcf.REF_DP

    return {
        "OC395_VAF": variant_entry_obj.info_internal_2_vcf.ALT_AF if variant_entry_obj is not None else 0.00,
        vaf_key_in_df: dict_list_of_dict_golden_set_variants[key][vaf_key_in_df],
        "Chromosome":  dict_list_of_dict_golden_set_variants[key]['CHROM'],
        "Gene": return_most_severe_impact_transcript_obj(variant_entry_obj).SYMBOL
        if variant_entry_obj is not None else 'NA',
        "Position": dict_list_of_dict_golden_set_variants[key]['POS'],
        "Ref_allele": dict_list_of_dict_golden_set_variants[key]['REF'],
        "Alt_allele": dict_list_of_dict_golden_set_variants[key]['ALT'],
        "OC395_ALT_DP": variant_entry_obj.info_internal_2_vcf.ALT_DP if variant_entry_obj is not None else 'ND',
        "OC395_REF_DP": variant_entry_obj.info_internal_2_vcf.REF_DP if variant_entry_obj is not None else 'ND',
        "OC395_TOAL_DP": total_dp,
        "Pvalue": pvalue,
        "Variant_type": dict_list_of_dict_golden_set_variants[key]['VARIANT_TYPE'],
        "dbSNP_Amino_Acid": dict_list_of_dict_golden_set_variants[key]['dbSNP_Amino_Acid'],
        "True_positive": passed_tp_test if passed_tp_test is not None else 'NA',
        "Reason_pass_or_fail": reason if reason is not None else 'NA',
        "Primary_caller": variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER if variant_entry_obj is not None
        else 'NA',

    }

def golden_set_variant_overlapped_oc395_bed(chrom=None, position=None, list_of_dict_df_bed=None):
    """
    At this point, there was no vcf entry found for the OC395 sequenced HD827, so next see if we actually look at that
    region in the BED file.  Go over all intervals in the BED file, once the chromosomes maps to the BED file,
    see if there was overlap using the is_overlapping function
    :param chrom: String of the chromosome
    :param position: Position in the genome
    :param list_of_dict_df_bed: A list of dictionaries, where each entry is a row in a BED file
    :return bool:  True/False if there was overlap (uses is_overlap function)

    """
    overlap = False
    for entry in list_of_dict_df_bed:
        # do the chromosomes match
        if entry['CHROM'] == chrom:
            # remember BED file is 0 based, so y1+1
            overlap = is_overlapping(x1=position, x2=position, y1=entry['START_ZERO_BASED'] + 1, y2=entry['END'])
            if overlap is True:
                return overlap
    return overlap


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Find the horizon sample concordance')

    parser.add_argument('--tabular_output_file', dest='tabular_output_file',
                        type=str, help='Output file to store the data', required=True)
    return parser.parse_args()


if __name__ == "__main__":
    main()
