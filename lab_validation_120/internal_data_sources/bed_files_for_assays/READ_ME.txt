bedtools nuc -s -fi /Users/cleslin/BLI-repo/scratch/human_genome/hs37d5.fa -bed variantplex_03012017_coverage_OC120.bed >variantplex_03012017_coverage_OC120.gc.bed 

grep --colour=never 'CNV_' variantplex_03012017_coverage_OC120.bed | grep --colour=never -v 'CNV_None' | cut -f 4 >variantplex_03012017_coverage_OC120.cnv.list

grep --colour=never 'CNV' VariantPlex_OncoClinicas_Solid_Tumor_v3-v1.0.gtf | cut -f 9 >VariantPlex_OncoClinicas_Solid_Tumor_v3-v1.0.cnv.list

sort -V -k1,1 -k4,4 VariantPlex_OncoClinicas_Solid_Tumor_v3-v1.0.gtf >VariantPlex_OncoClinicas_Solid_Tumor_v3-v1.0.sorted.gtf

# gene_OC120.txt was made from variantplex_03012017_coverage_OC120.bed, using excel to just create the gene list, and then sort -u on that list to get gene_OC120.txt
paste - - - - - - - - - - - - - - - - - -  < genes_OC120.txt 




# GS395 Intervals
TargetsRefSeqExonsMap_OC395.bed

# overalp b/t GS395 and GS180

bedtools intersect -s -a VariantPlex_OC_v3_16897_primers_targets.sorted.noChr.bed \
-b TargetsRefSeqExonsMap_OC395.bed \
>VariantPlex_OC_v3_16897_primers_targets.sorted.noChr_intersect_TargetsRefSeqExonsMap_OC395.bed


# New intervals for GS120+ from Elina Wegner (VariantPlex_OC_v3_16897_primers_targets.bed.orig updated H3F3A->H3-3A, H3F3B->H3-3B, MRE11A->MRE11 genes)
VariantPlex_OC_v3_16897_primers_targets.bed
sort -V -k1,1 -k2,2 VariantPlex_OC_v3_16897_primers_targets.bed >VariantPlex_OC_v3_16897_primers_targets.sorted.bed
# GS120+ VariantPlex gtf
VariantPlex_OncoClinicas_Solid_Tumor_v3-v1.0.gtf


# GS120 from Elina Wegner
VariantPlex_OC_v2_cSA5075_primers_targets.bed
sort -V -k1,1 -k2,2 VariantPlex_OC_v2_cSA5075_primers_targets.bed >VariantPlex_OC_v2_cSA5075_primers_targets.sorted.bed
# FusionPlex_OncoClinicas_SolidTumor_cSK5058-v1.1.gtf, from Fernanda... Fusion GS120.
grep -v 'CONTROL' FusionPlex_OncoClinicas_SolidTumor_cSK5058-v1.1.gtf | perl -ne 'if($_ =~ /gene_id\s+"(\S+)"/){ @arr=split/_/, $1; print "$arr[0]\n"; }'  | sort -u
# the Spreadsheet comparing OC120 to OC120plus
VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx -> renamed to VariantPlex_OncoclinicasSolidTumor_v1_v3.xlsx


# GS120 CNV bed from John and in the pipeline
variantplex_04042017_CNV.bed
# GS120 bed in the pipeline
variantplex_03012017_coverage_OC120.bed


#GS120 gft from Fernanda
VariantPlex_OncoClinicas_SolidTumor-v1.0.gtf
VariantPlex_OncoClinicas_SolidTumor-v1.0.nochr.gtf #VIM substitution on all chr
sort -V -k1,1 -k4,4 VariantPlex_OncoClinicas_SolidTumor-v1.0.nochr.gtf >VariantPlex_OncoClinicas_SolidTumor-v1.0.nochr.sorted.gtf


# Found in bli@10.240.0.37:/export/resources/snapshot-onco/ 
onco-snapshot.variantplex.cnv.20190925.bed
# sorted
sort -V -k1,1 -k2,2 onco-snapshot.variantplex.cnv.20190925.bed >onco-snapshot.variantplex.cnv.20190925.sorted.bed



# pulling out the CNV genes for OC120
grep -v '"None' VariantPlex_OncoClinicas_SolidTumor-v1.0.gtf | grep --colour=never 'CNV'   | perl -ne '@arr=split/ /, $_; @arr2=split/_/, $arr[1]; $arr2[0]=~s/"//g; print"$arr2[0]\n";' | sort -u | wc

# see how many Primers are found per gene for CNV:
# pulling out the CNV genes for OC120Pluse
grep -v '"None' VariantPlex_OncoClinicas_Solid_Tumor_v3-v1.0.gtf | grep 'CNV'   | perl -ne '@arr=split/\t/, $_; @arr2=split/_/, $arr[8]; $arr2[0]=~s/"//g; $arr2[0]=~s/name //; print"$arr2[0]\n";' | sort | uniq -c | sort -k1,1 -r  >VariantPlex_OncoClinicas_Solid_Tumor_v3-v1.0.cnv.txt

cut -f4 -d' ' VariantPlex_OncoClinicas_Solid_Tumor_v3-v1.0.gtf | sort -u
"SNP_ID";
"SNV,CNV";
"SNV,CNV,DNA_ANOMALY";
"SNV,CNV,MICROSATELLITE_INSTABILITY";
"SNV,SNP_ID";


snapshot_hotspots_actionable_resistence_biomarkers_v14_20200721.bed was copied from ../../../../lab-validation-395/lab_validation_395/internal_data_sources/bed_files_for_assays 
needed to add in chr to the beginning

# From Enrique: Which is a map of genes to Ensemble Transcripts
snapshot2_map.json


# From Holly for the new content in GS180 SNPs
cat VariantPlex_OC_v3_16897_primers_targets.bed 19638_VariantPlex_GS180v3_Additions_for_OC_Review_primers_targets.bed >VariantPlex_OC_v3_16897_primers_targets.Additions_for_OC_Review_primers_targets.bed
19638_VariantPlex_GS180v3_Additions_for_OC_Review_primers_targets.bed
19638_VariantPlex_GS180v3_Additions_for_OC_Review_primers_gsp2.bed
