"""Look over the exons that were produced from the excel file and the BED file"""
import argparse
import json

def main():
    args = get_cli_args()
    json_file = args.json_file
    with open(json_file) as infh:
        data_struct = json.load(infh)
    for gene, data_dict in data_struct['data'].items():
        if len(data_dict['refseq_transcripts']['snvs'].keys()) > 0:
            transcripts = data_dict['refseq_transcripts']['snvs'].keys()
            total_exons1 = []
            total_exons2 = []
            for transcript in transcripts:
                exons1 = data_dict['refseq_transcripts']['snvs'][transcript]['exons']
                total_exons1 += exons1
                exons2 = data_dict['refseq_transcripts_from_bed']['snvs'][transcript]['exons']
                total_exons2 += exons2
                if exons1 == exons2:
                    print(f"{gene}\tequal\t{transcript}\t{exons1}\t{exons2}")
                else:
                    print(f"{gene}\tnot equal\t{transcript}\t{exons1}\t{exons2}")
            total_exons1 = set(total_exons1)
            total_exons2 = set(total_exons2)
            len1 = len(total_exons1)
            len2 = len(total_exons2)
            if len1 != len2:
                print(f"{gene}\tDifferent\tTotal Size1\t{len1}\tTotal Size2\t{len2}\tdiff\t{len1-len2}")
            else:
                print(f"{gene}\tSame\tTotal Size1\t{len1}\tTotal Size2\t{len2}\tdiff\t{len1-len2}")
        elif gene in ['ASXL1', 'ROS1', 'DICER1', 'CBWD3', 'ASXL2', 'H3-3A']:
            transcripts = data_dict['refseq_transcripts_from_bed']['snvs'].keys()
            total_exons2 = []
            for transcript in transcripts:
                exons2 = data_dict['refseq_transcripts_from_bed']['snvs'][transcript]['exons']
                total_exons2 += exons2
            total_exons2 = set(total_exons2)
            len2 = len(total_exons2)
            print(f"{gene}\tDifferent\tTotal Size1\t0\tTotal Size2\t{len2}\tdiff\t{0 - len2}")


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Look over the exons that were produced from the excel file and the BED file')

    parser.add_argument('--json_file', dest='json_file',
                        type=str, help='Gene Map Json file',
                        required=True)

    return parser.parse_args()

if __name__ == '__main__':
    main()