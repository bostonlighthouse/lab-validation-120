"""Module to complete the intervals file for gs120Plus (aka GS v2.0)"""
import sys
from lab_validation_120.utils.configs.lab_validation_config import get_oc395_targets_bed_file, \
    get_oc395_gene_list_for_variant_plex, get_oc120plus_new_gene_list_to_keep_for_gs_v2,\
    get_cnv_annotation_dict_from_cnv_bed_file, get_v2_fusion_annotation_list, \
    get_oc395_archer_exons_excel_file, get_oc395_targets_bed_file
from tools.gene_maps.gene_map_utils import get_gene_2_transcript_2_exon_map_for_snvs, get_cli_args, \
    update_with_ensembl_transcripts_from_vv, update_with_fusion_genes, update_with_snv_genes, \
    update_with_cnv_genes, print_gene_map_json, get_gene_2_transcript_2_exon_map_for_snvs_from_bed_file


def main():
    # used the file from Archer: VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx (dSA16897_v3) in
    # lab_validation_120.utils.configs.lab_validation_config
    args = get_cli_args()
    assay_name = args.assay_name
    # get the gene lists needed
    oc395_gene_list = get_oc395_gene_list_for_variant_plex()
    oc395_gene_list.append('None')  # this is a gene name in the .bed file, so just add here

    # get the map of genes to transcripts to exons
    gene_2_transcript_2_exon_map_for_snvs = \
        get_gene_2_transcript_2_exon_map_for_snvs(assay_name=assay_name,
                                                  callable_function=get_oc395_archer_exons_excel_file,
                                                  skip_first_row=True)
    gene_2_transcript_2_exon_map_for_snvs_from_bed_file = \
        get_gene_2_transcript_2_exon_map_for_snvs_from_bed_file(assay_name=assay_name,
                                                                callable_function=get_oc395_targets_bed_file)
    # was not in column A of the excel file,  but could be found in the bed targets_file column
    not_in_gene_column_of_excel = {}
    # where all the data is is finally stored
    final_map = {}
    clinical_gene_map = {}
    technical_list_snv_gene = []
    clinical_list_snv_gene = []

    # go over the bed file
    bed_file_function = get_oc395_targets_bed_file
    with open(bed_file_function(), 'r') \
            as interval_file_fh, open(args.bed_file_2_print, 'w') as bed_outfh:
        for line in interval_file_fh:
            # The Archer bed file does not have consistent pattern of labels, e.g.
            # so temporarily replace _
            line = line.rstrip().replace('_', ';')

            # get the gene name
            values = line.split("\t")
            values[1] = int(values[1])
            values[2] = int(values[2])
            gene_name = values[3].split(";")[0]
            if gene_name.startswith('rs'):  # do not care about RS numbers
                pass
            elif gene_name == 'g.139390145T>C':
                # strange entry:  g.139390145T>C;c.*7668+378A>G;g.139390152T>C;c.*7668+371A>G;NM_017617.4_Exon_34 0
                continue

            # replace values like they were before for printing of the intervals file
            # Note these are not the actual exon names, this comes from the spreadsheet in:
            # get_oc395_archer_exons_excel_file()
            #line = line.replace('exon;', 'exon_')
            #line = line.replace('Exon;', 'Exon_')
            #line = line.replace('NM;', 'NM_')
            # was it a gene that was in the new list

            print(line, file=bed_outfh)
            if gene_name in oc395_gene_list:
                update_with_snv_genes(values=values, gene_name=gene_name, final_map=final_map, is_snv_match=True,
                                      gene_2_exon_map=gene_2_transcript_2_exon_map_for_snvs,
                                      gene_2_exon_map_from_bed=gene_2_transcript_2_exon_map_for_snvs_from_bed_file,
                                      clinical_list_snv_gene=clinical_list_snv_gene, assay_name=assay_name)
                clinical_gene_map[gene_name] = clinical_gene_map.get(gene_name, 0) + 1

            else:
                # it was not in the oc120plust_gene_list, and was not in the new_genes_in_oc120plus_2_remove, so these
                # are cases we still want to have in the intervals file, b/c Archer uses coordinate based targets_file for
                # specific hot spots or SNPs targeted, defining CNV only primers, or exon targets_file that are not found in
                # the human reference used by their design software.
                #print(f"Found this gene {gene_name}")
                not_in_gene_column_of_excel[gene_name] = not_in_gene_column_of_excel.get(gene_name, 0) + 1
                # either way, put into the final map
                update_with_snv_genes(values=values, gene_name=gene_name, final_map=final_map, is_snv_match=False,
                                      gene_2_exon_map=gene_2_transcript_2_exon_map_for_snvs,
                                      gene_2_exon_map_from_bed=gene_2_transcript_2_exon_map_for_snvs_from_bed_file,
                                      technical_list_snv_gene=technical_list_snv_gene, assay_name=assay_name)
    # update the CNV data
    technical_list_cnv_gene, clincial_list_cnv_gene = \
        update_with_cnv_genes(assay_name=assay_name,
                              final_map=final_map, callable_function=get_cnv_annotation_dict_from_cnv_bed_file)

    technical_list_fusion_gene, clincial_list_fusion_gene, control_list_fusion_gene = \
        update_with_fusion_genes(final_map=final_map, callable_function=get_v2_fusion_annotation_list)

    #remove_msi_labels(final_map=final_map)

    update_with_ensembl_transcripts_from_vv(final_map=final_map)

    # print out the sorted genes sorted by occurrence for genes that were not in columns A-C of:
    # VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx
    with open(args.no_match_file, 'w') as outfh:
        for k, v in sorted(not_in_gene_column_of_excel.items(), key=lambda item: item[1], reverse=True):
            print(f"{k}\t{v}", file=outfh)
    assert len(not_in_gene_column_of_excel) == 1006, f"not_in_gene_column_of_excel is not 1006, got " \
                                                     f"{len(not_in_gene_column_of_excel)}"

    final_genes_list_snvs_indels_cnvs = \
        sorted(list(set(get_oc395_gene_list_for_variant_plex())))

    assert len(final_genes_list_snvs_indels_cnvs) == 389, f"Final list of genes is not 389, got {len(final_genes_list_snvs_indels_cnvs)}"

    #""""
    assert_returns = run_assertions(final_map=final_map,
                                    final_genes_list_snvs_indels_cnvs=final_genes_list_snvs_indels_cnvs,
                                    technical_list_cnv_gene=technical_list_cnv_gene,
                                    clincial_list_cnv_gene=clincial_list_cnv_gene,
                                    technical_list_fusion_gene=technical_list_fusion_gene,
                                    clincial_list_fusion_gene=clincial_list_fusion_gene,
                                    control_list_fusion_gene=control_list_fusion_gene,
                                    clinical_list_snv_gene=clinical_list_snv_gene,
                                    technical_list_snv_gene = technical_list_snv_gene,
                                    assay_name=assay_name)
    #"""

    print_gene_map_json(version=args.json_file_name_version, assay_name=assay_name, final_map=final_map,
                        assertions=assert_returns)

    #pp = pprint.PrettyPrinter(indent=3)
    #pp.pprint(final_map)


def run_assertions(final_map=None, final_genes_list_snvs_indels_cnvs=None, technical_list_cnv_gene=None,
                   clincial_list_cnv_gene=None,
                   technical_list_fusion_gene=None, clincial_list_fusion_gene=None, control_list_fusion_gene=None,
                   clinical_list_snv_gene=None,
                   technical_list_snv_gene=None,
                   assay_name=None):
    """
    Just do some checking to make this matches the VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx (dSA16897_v3)
    """
    # test a gene for individual exon elements for snvs
    assert final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons'][0] == 1, "ABL1 exons[0] is not 1"
    assert final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons'][1] == 2, "ABL1 exons[0] is not 2"
    assert final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons'][2] == 3, "ABL1 exons[0] is not 3"
    assert final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons'][3] == 4, "ABL1 exons[0] is not 4"
    assert len(final_map['ABL1']['snv_meta_data']['snps']) == 0, "ABL1 snps is not empty"
    assert len(final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons']) == 11, "ABL1 exons size is not 11"
    #assert final_map['ABL1']['snv_meta_data']['is_clinical'] is True, "ABL1 is_clinical is not True"
    #assert final_map['ABL1']['snv_meta_data']['is_clinical'] is True, 'ABL1 does not have True for snv is_clinical'
    assert final_map['ABL1']['snv_meta_data']['is_technical'] is True, 'ABL1 does not have True for snv is_technical'
    #assert final_map['ABL1']['cnv_meta_data']['is_clinical'] is True, 'ABL1 does not have True for cnv is_clinical'
    assert final_map['ABL1']['cnv_meta_data']['is_technical'] is True, 'ABL1 does not have True for cnv is_technical'
    assert final_map['ABL1']['fusion_meta_data']['is_technical'] is False, \
        'ABL1 does not have True for fusion is_technical'
    assert final_map['ABL1']['fusion_meta_data']['is_technical'] is False, \
        'ABL1 does not have  True for fusion is_technical'

    # test gene SDHB
    #assert final_map['SDHB']['snv_meta_data']['is_clinical'] is True, 'SDHB does not have True for snv is_clinical'
    assert final_map['SDHB']['snv_meta_data']['is_technical'] is True, 'SDHB does not have True for snv is_technical'
    #assert final_map['SDHB']['cnv_meta_data']['is_clinical'] is True, 'SDHB does not have False for cnv is_clinical'
    #assert final_map['SDHB']['cnv_meta_data']['is_clinical'] is True, 'SDHB does not have False for cnv is_clinical'
    assert final_map['SDHB']['cnv_meta_data']['num_primers'] == 16, 'SDHB num_primers is not 16'
    assert final_map['SDHB']['fusion_meta_data']['is_technical'] is False, \
        'SDHB does not have False for fusion is_technical'
    assert final_map['SDHB']['fusion_meta_data']['is_technical'] is False, \
        'SDHB does not have False for fusion is_technical'
    assert final_map['SDHB']['fusion_meta_data']['is_control'] is False, \
        'RAB7A does not have False for fusion is_technical'

    # test the control genes gene, that was only a FUSION control
    for gene in ['RAB7A', 'GPI', 'CHMP2A', 'VCP']:
        #assert final_map[gene]['snv_meta_data']['is_clinical'] is False, \
        #    'RAB7A does not have False for snv is_clinical'
        assert final_map[gene]['snv_meta_data']['is_technical'] is False, \
            'RAB7A does not have False for snv is_technical'
        assert final_map[gene]['cnv_meta_data']['is_technical'] is False, \
            'RAB7A does not have False for cnv is_technical'
        assert final_map[gene]['cnv_meta_data']['is_control'] is False, \
            'RAB7A does not have False for cnv is_technical'
        assert final_map[gene]['fusion_meta_data']['is_technical'] is False, \
            'RAB7A does not have False for fusion is_technical'
        assert final_map[gene]['fusion_meta_data']['is_technical'] is False, \
            'RAB7A does not have False for fusion is_technical'
        assert final_map[gene]['fusion_meta_data']['is_control'] is True, \
            'RAB7A does not have True for fusion is_technical'

    # check out exons for fusions TMPRSS2 that has two transcripts
    assert final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_001135099']['exons'][0] == 1, \
        "TMPRSS2 (NM_001135099)  exons[0] is not 1"
    assert final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_001135099']['exons'][1] == 2, \
        "TMPRSS2 (NM_001135099) exons[0] is not 2"
    assert len(final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_001135099']['exons']) == 2, \
        "TMPRSS2 (NM_001135099) length is not 2"

    assert final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_005656']['exons'][4] == 6, \
        "TMPRSS2 (NM_005656) exons[0] is not 2"
    assert len(final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_005656']['exons']) == 5, \
        "TMPRSS2 (NM_005656) length is not 2"
    assert len(final_map['TMPRSS2']['refseq_transcripts']['fusions'].keys()) == 2, \
        "TMPRSS2 trascripts for fusions is not 2"

    # test another gene with one transcript
    assert final_map['NPM1']['refseq_transcripts']['snvs']['NM_002520']['exons'][0] == 1, "NPM1 exons size is not 1"
    assert len(final_map['NPM1']['refseq_transcripts']['snvs']['NM_002520']['exons']) == 11, "NPM1 exons size is not 11"

    # test another gene with two transcripts
    assert len(final_map['BRCA1']['refseq_transcripts']['snvs']['NM_007300']['exons']) == 23, \
        "BRCA1 exons size is not 23"
    assert len(final_map['BRCA1']['refseq_transcripts']['snvs']['NM_007294']['exons']) == 22, \
        "BRCA1 exons size is not 22"
    assert final_map['BRCA1']['refseq_transcripts']['snvs']['NM_007294']['exons'][9] == 11, "BRCA1 exons[9] is not 11"

    # test some sizes
    assert len(final_map['TSC1']['refseq_transcripts']['snvs']['NM_000368']['exons']) == 21, "NPM1 exons size is not 21"
    assert len(final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons']) == 29, "NPM1 exons size is not 29"

    # test snps and other feathers in KAZN
    assert len(final_map['KAZN']['snv_meta_data']['snps']) == 1, \
        "AMLEX has the incorrect number of snps"
    assert final_map['KAZN']['snv_meta_data']['snps'][0]['alt_allele'] == 'T', \
        "snps [0] has the incorrect alt_allele"
    assert final_map['KAZN']['snv_meta_data']['snps'][0]['ref_allele'] == 'C', \
        "snps [0]  has the incorrect ref_allele"
    assert final_map['KAZN']['snv_meta_data']['snps'][0]['start'] == 15296018, \
        "snps [0]  has the incorrect start"
    assert final_map['KAZN']['snv_meta_data']['snps'][0]['stop'] == 15296019, \
        "snps [0]  has the incorrect stop"
    assert final_map['KAZN']['snv_meta_data']['snps'][0]['rs_number'] == 'rs880922', \
        "snps [0]  has the rs_number"
    #assert final_map['KAZN']['snv_meta_data']['is_clinical'] is False, "KAZN is_clinical is not False"

    # BYSL CNV data, does have one pimer for CNV, but this is not enough to do CNV calling
    assert final_map['CAMTA1']['cnv_meta_data']['is_technical'] is False, 'CAMTA1 has False for is_technical'

    # test the intervals
    assert final_map['XRCC3']['snv_meta_data']['intervals'][0]['start'] == 104165132, \
        "intervals [0] has the incorrect start"
    assert final_map['XRCC3']['snv_meta_data']['intervals'][0]['stop'] == 104165356, \
        "intervals [0] has the incorrect stop"

    assert final_map['XRCC3']['snv_meta_data']['intervals'][1]['start'] == 104165467, \
        "intervals [1] has the incorrect start"
    assert final_map['XRCC3']['snv_meta_data']['intervals'][1]['stop'] == 104165518, \
        "intervals [1] has the incorrect stop"

    assert final_map['XRCC3']['snv_meta_data']['intervals'][6]['start'] == 104177367, \
        "intervals [1] has the incorrect start"
    assert final_map['XRCC3']['snv_meta_data']['intervals'][6]['stop'] == 104177426, \
        "intervals [1] has the incorrect stop"

    assert len(final_map['XRCC3']['snv_meta_data']['intervals']) == 7, \
        "intervals has an incorrect length for intervals"

    """ Validation data for CAMTA1 """
    assert final_map['CAMTA1']['cnv_meta_data']['is_technical'] is False, "CAMTA1 cnv is_technical is not True"
    assert final_map['CAMTA1']['cnv_meta_data']['num_primers'] == 0, "CAMTA1 cnv num_primers is not 0"
    #assert final_map['CAMTA1']['fusion_meta_data']['is_clinical'] is True, "CAMTA1 fusion is_clinical is not True"
    assert final_map['CAMTA1']['fusion_meta_data']['is_technical'] is True, "CAMTA1 fusion is_technical is not True"
    assert final_map['CAMTA1']['fusion_meta_data']['is_control'] is False, "CAMTA1 fusion is_control is not False"
    # match data
    assert len(final_map['CAMTA1']['snv_meta_data']['snps']) == 2, "CAMTA1 snps list length is not 2"
    # intervals
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][0]['start'] == 7434543, \
        "CAMTA1 intervals[0] is not 7434543"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][0]['stop'] == 7434544, \
        "CAMTA1 intervals[0] is not 7434544"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][1]['start'] == 7520128, \
        "CAMTA1 intervals[1] is not 7520128"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][1]['stop'] == 7520129, \
        "CAMTA1 intervals[1] is not 7520129"
    assert len(final_map['CAMTA1']['snv_meta_data']['intervals']) == 2, \
        "CAMTA1 intervals length is not 2"
    # test transcripts data
    assert len(final_map['CAMTA1']['refseq_transcripts']['snvs'].keys()) == 0, \
        "CAMTA1 refseq_transcripts snvs length is not 0"
    assert len(final_map['CAMTA1']['refseq_transcripts']['fusions'].keys()) == 1, \
        "CAMTA1 refseq_transcripts fusions length is not 1"

    """ Validation data for ALK """
    # match data
    #assert final_map['ALK']['cnv_meta_data']['is_clinical'] is True, "ALK cnv is_clinical is not True"
    assert final_map['ALK']['cnv_meta_data']['is_technical'] is True, "ALK cnv is_technical is not True"
    assert final_map['ALK']['cnv_meta_data']['num_primers'] == 18, "ALK cnv num_primers is not 18"
    #assert final_map['ALK']['fusion_meta_data']['is_clinical'] is True, "ALK fusion is_clinical is not True"
    assert final_map['ALK']['fusion_meta_data']['is_technical'] is True, "ALK fusion is_technical is not True"
    assert final_map['ALK']['fusion_meta_data']['is_control'] is False, "ALK fusion is_control is not False"
    #assert final_map['ALK']['snv_meta_data']['is_clinical'] is True, "ALK snv is_clinical is not True"
    assert final_map['ALK']['snv_meta_data']['is_technical'] is True, "ALK snv is_clinical is not True"
    assert len(final_map['ALK']['snv_meta_data']['snps']) == 0, "ALK snps list length is not 0"
    assert final_map['ALK']['snv_meta_data']['intervals'][-4]['start'] == 29754778, \
        "ALK intervals[-4] is not 29754778,,"
    assert final_map['ALK']['snv_meta_data']['intervals'][-4]['stop'] == 29754984, "ALK intervals[-4] is not 29754984"
    assert final_map['ALK']['snv_meta_data']['intervals'][-3]['start'] == 29917713, \
        "ALK intervals[-3] is not 29917713,,"
    assert final_map['ALK']['snv_meta_data']['intervals'][-3]['stop'] == 29917882, "ALK intervals[-3] is not 29917882"
    assert final_map['ALK']['snv_meta_data']['intervals'][-2]['start'] == 29940441, \
        "ALK intervals[-2] is not 29940441,"
    assert final_map['ALK']['snv_meta_data']['intervals'][-2]['stop'] == 29940565, "ALK intervals[-2] is not 29940565"
    assert final_map['ALK']['snv_meta_data']['intervals'][-1]['start'] == 30142856, \
        "ALK intervals[-1] is not 30142856,"
    assert final_map['ALK']['snv_meta_data']['intervals'][-1]['stop'] == 30143527, "ALK intervals[-1] is not 30143527"
    assert len(final_map['ALK']['snv_meta_data']['intervals']) == 29, "ALK intervals length is not 29"
    # test transcripts snv data
    assert len(final_map['ALK']['refseq_transcripts']['snvs'].keys()) == 1, \
        "ALK refseq_transcripts snvs length is not 1"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][0] == 1, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 1"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][28] == 29, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 29"

    # test refseq_transcripts fusion data
    assert len(final_map['ALK']['refseq_transcripts']['fusions'].keys()) == 1, \
        "ALK refseq_transcripts fusions length is not 1"
    assert final_map['ALK']['refseq_transcripts']['fusions']['NM_004304']['exons'][0] == 2, \
        "ALK refseq_transcripts->fusions->MM_004304->exons is not 2"
    assert final_map['ALK']['refseq_transcripts']['fusions']['NM_004304']['exons'][-1] == 26, \
        "ALK refseq_transcripts->fusions->MM_004304->exons is not 26"

    assert len(final_map['ALK']['refseq_transcripts']['fusions'].keys()) == 1, \
        "ALK refseq_transcripts fusions length is not 1"

    # Fusion controls
    assert final_map['GPI']['fusion_meta_data']['is_control'] is True, "GPI fusion is not True"
    assert final_map['VCP']['fusion_meta_data']['is_control'] is True, "VCP fusion is not True"
    assert final_map['RAB7A']['fusion_meta_data']['is_control'] is True, "RAB7A fusion is not True"
    assert final_map['CHMP2A']['fusion_meta_data']['is_control'] is True, "CHMP2A fusion is not True"

    # all genes in the gene_2_transcript_2_exon_map_for_snvs should be is_snv_match True
    for gene in final_genes_list_snvs_indels_cnvs:
        assert final_map[gene]['snv_meta_data']['is_technical'] is True, f"{gene} is_clinical is not True"

    #at_least_one_clinical_match = 0   # look for genes with at least one clinical match in snv, cnv, or fusion
    #at_least_one_clinical_match_gene_list = []  # store the genes from above
    at_least_one_technical_match = 0   # look for genes with at least one technical match in snv, cnv, or fusion
    at_least_one_technical_match_gene_list = []  # store the genes from above
    #all_snv_fusion_cvn_clinical_match_inclusive = 0  # look for genes
    #all_snv_fusion_cvn_clinical_match_gene_list_inclusive = []

    non_technical_match = 0  # no technical match in snv, cnv, or fusion
    #clinical_snv_match = 0  # at least matched snv clinical
    #clinical_snv_match_list = []
    clinical_cnv_match = 0  # at least matched snv clinical
    clinical_cnv_match_list = []
    #clinical_fusion_match = 0  # at least matched snv clinical
    #clinical_fusion_match_list = []
    technical_cnv_match = 0  # at least matched cnv technical
    technical_cnv_match_list = []
    technical_snv_match = 0  # at least matched snv technical
    technical_snv_match_list = []
    technical_fusion_match = 0  # at least matched snv clinical
    technical_fusion_match_list = []


    for gene in final_map:
        # how many genes had at least one is_technical match in the snv, cnv, or fusion
        if final_map[gene]['cnv_meta_data']['is_technical'] is True or \
                final_map[gene]['fusion_meta_data']['is_technical'] is True or \
                final_map[gene]['snv_meta_data']['is_technical'] is True:
            at_least_one_technical_match += 1
            at_least_one_technical_match_gene_list.append(gene)

        # how many genes were not clinical at all
        if final_map[gene]['cnv_meta_data']['is_technical'] is False and \
                final_map[gene]['fusion_meta_data']['is_technical'] is False and \
                final_map[gene]['snv_meta_data']['is_technical'] is False:
            non_technical_match += 1

        if final_map[gene]['snv_meta_data']['is_technical'] is True:
            technical_snv_match += 1
            technical_snv_match_list.append(gene)
        if final_map[gene]['cnv_meta_data']['is_technical'] is True:
            technical_cnv_match += 1
            technical_cnv_match_list.append(gene)
        if final_map[gene]['fusion_meta_data']['is_technical'] is True:
            technical_fusion_match += 1
            technical_fusion_match_list.append(gene)

    assert_returns = {}
    assert len(final_map.keys()) == 916, f'The total number of genes is {len(final_map.keys())}'
    assert_returns['num_genes_in_map'] = {'num': len(final_map.keys()), 'doc_str': f'The total number of genes is {len(final_map.keys())}'}

    assert at_least_one_technical_match == 463, f'The total number of technical genes is {at_least_one_technical_match}'
    assert_returns['at_least_one_technical_match'] = \
        {'num': at_least_one_technical_match, 'doc_str': f'The total number of technical genes is {at_least_one_technical_match}'}

    assert non_technical_match == 453, f'The total number of non clinical genes is {non_technical_match}'
    assert_returns['non_technical_match'] = {'num': non_technical_match, 'doc_str': f'The total number of non clinical genes is {non_technical_match}'}

    assert technical_snv_match == 394, f'The total number of technical snv genes is {technical_snv_match}'
    assert_returns['technical_snv_match'] = {'num': technical_snv_match, 'doc_str': f'The total number of technical snv genes is {technical_snv_match}'}

    assert technical_cnv_match == 394, f'The total number of clinical cnv genes is {technical_cnv_match}'
    assert_returns['technical_cnv_match'] = {'num': technical_cnv_match, 'doc_str': f'The total number of technical cnv genes is {technical_cnv_match}'}

    #print(f"difference  technical_snv_match_list > technical_cnv_match_list {set(technical_snv_match_list).difference(technical_cnv_match_list)}")
    #print(f"difference  technical_cnv_match_list > technical_snv_match {set(technical_cnv_match_list).difference(technical_snv_match_list)}")
    print(sorted(set(technical_cnv_match_list + technical_snv_match_list + technical_fusion_match_list)))
    technical_snv_cnv_match = len(set(technical_list_snv_gene + technical_cnv_match_list))
    assert technical_snv_cnv_match == 394, f'The total number of technical snv genes is {technical_snv_cnv_match}'
    assert_returns['technical_snv_cnv_match'] = {'num': technical_snv_cnv_match, 'doc_str': f'The total number of technical snv + cnv genes is {technical_snv_cnv_match}'}

    assert technical_fusion_match == 100, f'The total number of clinical fusion genes is {technical_fusion_match}'
    assert_returns['technical_fusion_match'] = {'num': technical_fusion_match, 'doc_str': f'The total number of clinical fusion genes is {technical_fusion_match}'}

    # test the CNVs,
    for gene in technical_list_cnv_gene:
        assert final_map[gene]['cnv_meta_data']['is_technical'] is True, f"{gene} cnv is_technical is not True"

    # remove the clinical from technical and then make sure they are all True for technical and False for clinical
    for gene in technical_list_fusion_gene:
        assert final_map[gene]['fusion_meta_data']['is_technical'] is True, f"{gene} fusion is_technical is not True"

    # test the control fusions
    for gene in control_list_fusion_gene:
        assert final_map[gene]['fusion_meta_data']['is_technical'] is False, f"{gene} fusion is_technical is not False"
        assert final_map[gene]['fusion_meta_data']['is_control'] is True, f"{gene} fusion is_control is not True"

    # this gene is listed as a Mutation in the excel file, but is in the gtf file w/o exons
    for gene in ('HRAS', 'CTNNB1', 'IDH1', 'IDH2', 'KRAS', 'MAP2K1'):
        assert gene not in technical_list_fusion_gene, f'{gene} is in technical_list_fusion_gene'

    for exon in (12, 13, 14):  # these were mutation based in the excel file
        assert exon not in final_map['FGFR2']['refseq_transcripts']['fusions']['NM_000141']['exons'], \
            f"FGFR2 refseq_transcripts->fusions->MM_004304->exons have exon {exon}"

    # go through and make sure the invalid HUGO-gene name are fixed:
    # MRE11A old -->  MRE11
    # H3F3A old -->  H3-3A
    # H3F3B old -->  H3-3B
    assert 'MRE11A' not in final_map, "MRE11A is still in final map, and should not be"
    assert 'H3F3A' not in final_map, "H3F3A is still in final map, and should not be"
    assert 'H3F3C' not in final_map, "H3F3C is still in final map, and should not be"
    assert 'MRE11' in final_map, "MRE11 is in not final map"
    assert 'H3-3A' in final_map, "H3-3A is in not final map"
    assert 'H3-5' in final_map, "H3-5 is in not final map"

    # go over gene lists
    for gene in get_oc395_gene_list_for_variant_plex():
        assert gene in final_map, f"{gene} is not in final_map, and should be"

    for gene in get_oc120plus_new_gene_list_to_keep_for_gs_v2():
        assert gene in final_map, f"{gene} is not in final_map, and should be"

    for gene in final_map.keys():
        if final_map[gene]['cnv_meta_data']['is_technical'] is True or \
                final_map[gene]['fusion_meta_data']['is_technical'] is True or \
                final_map[gene]['snv_meta_data']['is_technical'] is True or (
                final_map[gene]['cnv_meta_data']['num_primers'] is not None and
                final_map[gene]['cnv_meta_data']['num_primers'] >= 1) or \
                final_map[gene]['fusion_meta_data']['is_control'] is True:
            pass
        else:
            #print(gene)
            pass
    # this list of genes was found by the above for loop, and verified to be SNPs
    for gene, type in {"AJAP1": 'SNP', "CCDC190": 'SNP', "RGS8": 'SNP', "CELF2": 'SNP', "SHLD2": 'SNP',
                       "DENND2B": 'SNP', "PIK3C2G": 'SNP', "OR4K2": 'SNP', "SYNE2": 'SNP', "DNAH5": 'SNP', "SNX18": 'SNP',
                       "LYRM4": 'SNP', "COL10A1": 'SNP', "PRKN": 'SNP', "ERICH1": 'SNP', "CASC8": 'SNP'}\
            .items():
        assert final_map[gene]['snv_meta_data']['is_technical'] is False, f"{gene} snp is_technical is False"
        if type == 'SNP':
            assert len(final_map[gene]['snv_meta_data']['snps']) >= 1, f"{gene} snp is >= 1"

    print("done", file=sys.stderr)

    return assert_returns


if __name__ == "__main__":
    main()