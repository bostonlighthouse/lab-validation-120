"""
Module to generate a BED file for Cider, using format we have from Archer GS395
Agilent originally sent us a BEF file hg38:
lab_validation_120/external_data_sources/agilent/hg38_A3416642_Covered_DNA.bed

"""
import argparse
import pandas as pd
from tools.gene_maps.agilent.create_agilent_bed_file_for_cider import update_with_ensembl_information, \
    get_df_for_analysis,  MoleculeType, get_ensembl_transcripts


def main():
    """
    Main business logic
    :return: NoneType
    """
    args = get_cli_args()
    bed_file_version = args.bed_file_version
    bed_file = args.bed_file
    # get the data from teh BED files for each molecule type
    htqc_bed_df = pd.read_csv(bed_file, sep="\t", names=['chrom', 'start', 'end', 'hg38', 'ignore'])
    print(htqc_bed_df.head(5))
    print(htqc_bed_df.dtypes)
    update_with_ensembl_information(df=htqc_bed_df)
    # print out the BED file
    htqc_bed_df.to_excel("test.xlsx")

    hg19_df_dna, genes_in_agilent_brochure_dna_set = get_df_for_analysis(molecule_type=MoleculeType.dna.name)
    print(len(hg19_df_dna))

    gene_transcripts_aggregated_exons_intervals_df = \
        get_ensembl_transcripts(df_temp_mol_type=hg19_df_dna,
                                genes_in_agilent_brochure_set=genes_in_agilent_brochure_dna_set)

    print(gene_transcripts_aggregated_exons_intervals_df.head(10))


def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description="Create the BED file for Agilent's HTQC")

    parser.add_argument('--bed_file_version',
                        dest='bed_file_version',
                        type=int,
                        help='Version to print out... this will be added to final BED file',
                        required=True)

    parser.add_argument('--bed_file',
                        dest='bed_file',
                        type=str,
                        help='HTQC BED file',
                        required=True)

    return parser.parse_args()


if __name__ == '__main__':
    main()
