"""
Module to generate a BED file for Cider, using format we have from Archer GS395
Agilent originally sent us a BEF file hg38:
lab_validation_120/external_data_sources/agilent/hg38_A3416642_Covered_DNA.bed

chr1    2556616 2556782 TNFRSF14
chr1    2557677 2557842 TNFRSF14
chr1    2558342 2558507 TNFRSF14
..
..
chr1    3712192 3712313 CNVBackbone

There are these entries that have multiple gene, cnv, MSI found by:
less lab_validation_120/external_data_sources/agilent/hg38_A3416642_Covered_DNA.bed | \
cut -f 4 | grep -E ';|,' | sort | uniq -c | less

   1 CCND2;FGF6
   1 CNVBackbone;RAF1   # gene is on the panel with 19 entries
   1 FGF4;CCND1
   1 FGFR1;WHSC1L1
   1 FGFR3;WHSC1
   1 MSICovered,QKI     # gene is on the panel with 12 entries
   1 MSICovered,RAD50   # gene is on the panel with 26 entries
   1 MSICovered,RPL22   # gene is on the panel with 6 entries
   1 PTEN;CNVBackbone   # gene is on the panel with 76 entries
   1 SERPINB3;SERPINB4
   2 WHSC1;FGFR3

The most common gene/regions:
less lab_validation_120/external_data_sources/agilent/hg38_A3416642_Covered_DNA.bed | \
cut -f 4 | sort | uniq -c | sort -k1,1 -nr | less

 715 CNVBackbone
 142 ALK
 123 AR
 103 BRCA2
  98 MET
  95 FGFR3
  90 LRP1B
  88 FGFR1
  88 EGFR
  84 FGFR2
  83 BRAF



I then asked them to send us an hg19 version:
lab_validation_120/external_data_sources/agilent/hg19_A3416642_Covered_DNA.bed

chr1    2488055 2488221 chr1:2556617-2556782    1
chr1    2489116 2489281 chr1:2557678-2557842    1
chr1    2489781 2489946 chr1:2558343-2558507    1

As you can see above, the fourth column maps into the hg38 version. We need the mapping
for the CNVBackbone.  The rest of the information we need we come from grch37 ensembl

OC395 bed file format

1       4849383 4849384 rs2235438       0       -
1       4955861 4955862 rs6426446       0       -
..
1       11199358        11199494        MTOR;NM_004958.3_Exon_36        0       -
1       11199587        11199717        MTOR;NM_004958.3_Exon_35        0       -
1       11204702        11204814        MTOR;NM_004958.3_Exon_34,NR_046600.1_Exon_2     0

"""
import argparse
from enum import Enum
import logging
import os
from typing import Tuple
import pandas as pd

from lab_validation_120.utils.configs.lab_validation_config \
    import get_agilent_hg19_hybrid_capture_dna_targets_bed_file, \
    get_agilent_hg38_hybrid_capture_dna_targets_bed_file, get_agilent_hybrid_capture_dna_gene_list_from_brochure, \
    get_agilent_hg38_hybrid_capture_rna_targets_bed_file, get_agilent_hybrid_capture_rna_gene_list_from_brochure, \
    get_agilent_hg19_hybrid_capture_rna_targets_bed_file, get_archer_fusion_direction_file, get_agilent_hybrid_capture_rna_gene_directionality, \
    get_agilent_hg19_hybrid_capture_dna_targets_bed_file_for_ifcnv_bed_file_generation

from tools.ensembl.rest.ensembl_rest_client import EnsemblRestClient
from tools.ensembl.mysql.get_ensembl_gene_transcript_from_refseq_gene_transcript import get_pymysql_connection, \
    query_to_get_refseq_accession_from_ensembl_id

from tools.gene_maps.gene_map_utils import get_init_gene_data_structure, get_json_assertions_for_gene_map, \
    print_gene_map_json, get_ensembl_overlap_api_data, get_storage_dir

from tools.cnv.utils import get_list_important_cnv_genes

# pylint: disable=logging-fstring-interpolation
logging.basicConfig(level=logging.DEBUG)

# just for my printing
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


class MoleculeType(Enum):
    """
    Simple Enum for this program since we have both rna and dna BED files
    The JSON mapping will require fusions (rna) and SNV/CNV (dna)
    """
    dna = 1
    rna = 2


def main():
    """
    Main business logic
    :return: NoneType
    """
    args = get_cli_args()
    json_file_name_version = args.json_file_name_version
    # get the data from teh BED files for each molecule type
    hg19_df_dna, genes_in_agilent_brochure_dna_set = get_df_for_analysis(molecule_type=MoleculeType.dna.name)
    hg19_df_rna, genes_in_agilent_brochure_rna_set = get_df_for_analysis(molecule_type=MoleculeType.rna.name)
    # these genes are from Table 1 and Table 2 of the brochure (plus couple updates to Table 1, see file in repo
    # containing the genes get_agilent_hybrid_capture_dna_gene_list_from_brochure)
    # Note the genes in CNVs and Translocation (Table 1) are covered in the SNV/INDELS section
    # brochure-cancer-cgp-assay-5994-5801EN-agilent.pdf.
    # All translocations from Table 1, including exons, are also covered in the RNA Table 2.
    genes_in_agilent_brochure_set = genes_in_agilent_brochure_dna_set | genes_in_agilent_brochure_rna_set
    # combine the data frames so there is both dna and rna in one
    hg19_and_hg38_dna_and_rna_df = pd.concat([hg19_df_dna, hg19_df_rna]).reset_index(drop=True)
    # log some information
    logging.debug(f"number of genes in dna {len(genes_in_agilent_brochure_dna_set)}")
    logging.debug(f"number of genes in rna {len(genes_in_agilent_brochure_rna_set)}")
    logging.debug(f"number of genes in dna/rna {len(genes_in_agilent_brochure_set)}")
    # print out some data to excel for validation
    hg19_and_hg38_dna_and_rna_df.to_excel(f'hg19_and_hg38_dna_and_rna_v{json_file_name_version}.xlsx')

    # get genes that are on the RNA but not on DNA panel
    diff_genes_rna_from_dna_set = genes_in_agilent_brochure_rna_set.difference(genes_in_agilent_brochure_dna_set)
    # test to make sure the genes that are on the RNA panel, but not on DNA panel, are correct (relative to brochure)
    assert diff_genes_rna_from_dna_set == genes_on_rna_not_on_dna_via_brochure(), "Fusion diff is not correct"

    # get a data frame with the relevant exon and intervals information summarized per row:
    # chrom    gene       transcript  transcript_version  is_canonical                                exons  num_exons                                          intervals molecule_type    # noqa: E501
    #  chrX   ZRSR2  ENST00000307771                   7             1  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]         11  [15808556-15808721, 15809061-15809182, 1581797...           dna    # noqa: E501
    #  chrX   ZRSR2  ENST00000380308                   3             0                         [1, 2, 3, 4]          4  [15808556-15808721, 15809061-15809182, 1581797...           dna    # noqa: E501
    #  chrX   ZRSR2  ENST00000468028                   1             0                         [1, 2, 3, 4]          4  [15808556-15808721, 15809061-15809182, 1581797...           dna    # noqa: E501
    gene_transcripts_aggregated_exons_intervals_df = \
        get_ensembl_transcripts(df_temp_mol_type=hg19_and_hg38_dna_and_rna_df,
                                genes_in_agilent_brochure_set=genes_in_agilent_brochure_set)
    gene_transcripts_aggregated_exons_intervals_df.to_excel(f"gene_transcript_exon_v{json_file_name_version}.xlsx")
    # do some testing to make sure we have genes found in the BED
    # TODO remove comments around asserts
    assert genes_in_agilent_brochure_set == set(gene_transcripts_aggregated_exons_intervals_df['gene'].to_list()), \
        "Sets from BED file and genes with ensembl transcripts df  are different"

    # get the gene to refSeq df
    #         gene  is_canonical       transcript  transcript_version refseq_accession    external_db.db_name
    # 0      ZRSR2             1  ENST00000307771                 7.0      NM_005089.3            RefSeq_mRNA
    # 1      ZRSR2             1  ENST00000307771                 7.0   XM_005274597.1  RefSeq_mRNA_predicted
    # 2      ZRSR2             0  ENST00000380308                 NaN              NaN                    NaN
    gene_refseq_transcripts_df = \
        get_ref_seqs_from_ensembl_transcripts(genes_found_df=gene_transcripts_aggregated_exons_intervals_df)


    # do some testing to make sure we have genes found in the BED
    print(sorted(genes_in_agilent_brochure_set))
    print("\n\n")
    print(sorted(set(gene_refseq_transcripts_df['gene'].to_list())))

    assert genes_in_agilent_brochure_set == set(gene_refseq_transcripts_df['gene'].to_list()), \
        "Sets from BED file and genes in the refseq df are different"

    # combine the data frames above
    gene_transcripts_refseq_aggregated_exons_intervals_df = \
        gene_transcripts_aggregated_exons_intervals_df.merge(gene_refseq_transcripts_df,
                                                             on=['gene', 'transcript'],
                                                             how="left", suffixes=('_x', '_y')).reset_index(drop=True)
    # print out some data to excel for validation
    gene_transcripts_refseq_aggregated_exons_intervals_df.to_excel(
        f"gene_transcript_refseq_aggregated_exons_intervals_df_v{json_file_name_version}.xlsx")

    # Get the datastructure for the final json gene map
    final_map, assertions = \
        prepare_json_gene_map(
            genes_found_with_transcripts_exons_df=gene_transcripts_refseq_aggregated_exons_intervals_df,
            genes_in_agilent_brochure_dna_set=genes_in_agilent_brochure_dna_set,
            genes_in_agilent_brochure_rna_set=genes_in_agilent_brochure_rna_set)

    # test the final map to make sure it's correct
    # TODO remove comments around this call
    run_final_map_asserts(final_map=final_map,
                          genes_in_agilent_brochure_dna_set=genes_in_agilent_brochure_dna_set,
                          genes_in_agilent_brochure_rna_set=genes_in_agilent_brochure_rna_set)

    update_final_map_for_gene_naming_in_vv(final_map)

    # print in JSON format gene map
    print_gene_map_json(version=json_file_name_version, assay_name='oc700',
                        final_map=final_map, assertions=assertions)

    test_fusion_direction_file(genes_in_agilent_brochure_rna_set=genes_in_agilent_brochure_rna_set)

    # print out the fusion directionality file
    print_directionality_file_for_cider()
    # print out the fusion directionality file
    print_platypus_file_for_cider(df=hg19_df_dna)
    # print out the Agilent BED file for genes so it can be parsed to generate a ifCNV formatted BED file
    print_bed_file_for_ifcnv_bed_file_generation(
        hg19_and_hg38_dna_and_rna_df=hg19_and_hg38_dna_and_rna_df,
        gene_transcripts_refseq_aggregated_exons_intervals_df=gene_transcripts_refseq_aggregated_exons_intervals_df)

def update_final_map_for_gene_naming_in_vv(final_map: dict=None):
    """
    VV has grch37 from ensembl so mapping will need to be updated.  Do that here at the end
    :param final_map:
    :return:
    """
    final_map['GPR124'] = final_map.pop('ADGRA2')



def print_bed_file_for_ifcnv_bed_file_generation(
        hg19_and_hg38_dna_and_rna_df: pd.DataFrame = None,
        gene_transcripts_refseq_aggregated_exons_intervals_df: pd.DataFrame = None) -> None:
    """
    Print out the bed file format with exon information so that it can be later parsed by:
    pipenv run python3 -m tools.cnv.ifCNV.get_ifCNV_bed_format --print_simple_bed_file True --assay_name gs700 >test.bed

    test.bed can then be used by ifCNV for finding CNVs (Note running ordinary BED file will not work, see:
    https://github.com/SimCab-CHU/ifCNV#changing-the-resolution

    :param hg19_and_hg38_dna_and_rna_df: df with the exon level information for each intervals
    :param gene_transcripts_refseq_aggregated_exons_intervals_df: df with ensembl to refSeq mapping
    :return: NoneType
    """

    # do not care about rna intervals for CNV and ifCNV
    hg19_and_hg38_dna_and_rna_df = hg19_and_hg38_dna_and_rna_df[hg19_and_hg38_dna_and_rna_df['molecule_type'] == 'dna']

    file = get_agilent_hg19_hybrid_capture_dna_targets_bed_file_for_ifcnv_bed_file_generation()
    print(len(hg19_and_hg38_dna_and_rna_df))
    with(open(file, "w", encoding='utf-8')) as out_fh:
        for _, row in hg19_and_hg38_dna_and_rna_df.iterrows():
            # get some values from the series
            ds_data = row['ds_data']
            gene = row['gene']
            chrom = row['chrom']
            start = row['start']
            end = row['end']
            # ignore these b/c ifCNV will not leverage them
            if gene in ('CNVBackbone', 'MSICovered'):
                continue
            if str(gene) == 'SERPINB4;SERPINB3':
                gene = 'SERPINB4'

            # if the gene is not in the ds_data, this is a case where agilent gene did not map to ensembl mapping so
            # we will skip these for the ifCNV bed file
            if gene not in ds_data:
                if str(gene) == 'nan':
                    print("\t".join((chrom.replace('chr', ''), str(start), str(end), f'NOT_ON_GENE_PANEL', '1')),
                          file=out_fh)
                else:
                    print("\t".join((chrom.replace('chr', ''), str(start), str(end), f'{str(gene)};NOT_IN_CANONICAL_EXON', '1')),
                          file=out_fh)
                continue
            gene_data = ds_data[gene]
            # go over each transcript from the data structure and use the canonical
            found = 0
            for transcript, dict_ in gene_data.items():
                if dict_['is_canonical'] == 1 and found == 0:
                    exons = dict_['exon']
                    strand = dict_['strand']
                    strand = '+' if strand == 1 else '-'
                    # get the df where the ensembl transcripts match so I can get the RefSeq transcript
                    df_temp = gene_transcripts_refseq_aggregated_exons_intervals_df[
                        gene_transcripts_refseq_aggregated_exons_intervals_df['transcript'] == transcript
                    ]
                    # exons can be size 0 here if the interval is covering a gene in an intron region of a gene,
                    # so it doesn't cover an exon e.g: chr1	93307174	93307304
                    # skip these in the bed file that will be parsed for ifCNV format
                    if len(exons) == 0:
                        continue
                    # get some values for the BED file

                    # only care for the first one since this is a BED file for ifCNV.  THis can happen when an
                    # interval covers more than one exon
                    exon = str(exons[0])

                    # can just get the first refseq_accession b/c they will all map to a sorted df where the first is
                    # the one we want due to the sorting that was done previously
                    # see function get_ref_seqs_from_ensembl_transcripts
                    # e.g. ENST00000013807 maps to this df
                    #       chrom   gene       transcript  transcript_version_x  is_canonical_x                     exons  num_exons                                          intervals molecule_type  is_canonical_y  transcript_version_y refseq_accession    external_db.db_name
                    # 4849  chr19  ERCC1  ENST00000013807                     5               1  [1, 2, 3, 4, 5, 6, 7, 8]          8  [45916801-45916966, 45917153-45917318, 4591810...           dna               1                   5.0      NM_202001.2            RefSeq_mRNA
                    # 4850  chr19  ERCC1  ENST00000013807                     5               1  [1, 2, 3, 4, 5, 6, 7, 8]          8  [45916801-45916966, 45917153-45917318, 4591810...           dna               1                   5.0     XM_005258634  RefSeq_mRNA_predicted
                    # 4851  chr19  ERCC1  ENST00000013807                     5               1  [1, 2, 3, 4, 5, 6, 7, 8]          8  [45916801-45916966, 45917153-45917318, 4591810...           dna               1                   5.0     XM_005258635  RefSeq_mRNA_predicted
                    # 4852  chr19  ERCC1  ENST00000013807                     5               1  [1, 2, 3, 4, 5, 6, 7, 8]          8  [45916801-45916966, 45917153-45917318, 4591810...           dna               1                   5.0     XM_005258636  RefSeq_mRNA_predicted
                    ref_seq = df_temp['refseq_accession'].iloc[0]

                    bed_string = f"{gene};{ref_seq}_Exon_{exon}"  # the annotation column
                    # print out the BED file
                    print("\t".join((chrom.replace('chr', ''), str(start), str(end), bed_string, '1')),
                          file=out_fh)
                    found = 1
            if found == 0:
                print("\t".join((chrom.replace('chr', ''), str(start), str(end), f'{gene};NOT_IN_CANONICAL_EXON', '1')),
                      file=out_fh)


def print_platypus_file_for_cider(df: pd.DataFrame = None) -> None:
    """
    Print out the bed file for platypus.
    Note the start doesn't specify BED format (0-based) so + 1 to all starts
    https://rahmanteamdevelopment.github.io/Platypus/documentation.html#specifying-calling-regions
    :param df: df with information on intervals
    :return: NoneType
    """
    # 1:4849384-4849384
    col_name = 'platypus_format'
    df[col_name] = df.apply(lambda row: ":".join((row['chrom'].replace('chr', ''),
                                                  f"{str(row['start'] + 1)}-{str(row['end'])}")), axis=1)
    file_name = os.path.splitext(get_agilent_hg19_hybrid_capture_dna_targets_bed_file())[0]
    file_name = ".".join((f"{os.path.basename(file_name)}_{col_name}", "txt"))
    df.to_csv(file_name, header=False, index=False, columns=[col_name])


def print_directionality_file_for_cider() -> None:
    """
    Get the directionality fusion file for Cider and print it out
    :return:
    """
    file, worksheet = get_agilent_hybrid_capture_rna_gene_directionality()
    df = pd.read_excel(file, sheet_name=worksheet, usecols="D,E")
    values = ['left', 'right']
    assert all(df['direction'].isin(values)), f"Value not equal to {values}"
    df.to_csv("fusion_direction_agilent.tsv", sep="\t", header=False, index=False)


def test_fusion_direction_file(genes_in_agilent_brochure_rna_set: set = None) -> None:
    """
    Go over and test there were the correct number of genes in the Agilent panel that were not in the Archer direction
    file
    :param genes_in_agilent_brochure_rna_set: Genes in the RNA seq panel for Agilent
    :return: NoneType
    """
    fusion_df = pd.read_csv(get_archer_fusion_direction_file(), names=['gene', 'direction'], sep="\t", comment="#")
    fusion_dict = dict(zip(fusion_df['gene'].to_list(), fusion_df['direction'].to_list()))
    not_in_fusion_direction_set = genes_in_agilent_brochure_rna_set.difference(fusion_dict.keys())
    num_not_in_archer = len(not_in_fusion_direction_set)
    assert len(not_in_fusion_direction_set) == num_not_in_archer, "There should be 41 genes in the agilent panel, " \
                                                                  "not found in the Archer directionality file: " \
                                                                  f"{num_not_in_archer}"


def prepare_json_gene_map(genes_found_with_transcripts_exons_df: pd.DataFrame = None,
                          genes_in_agilent_brochure_dna_set: set = None,
                          genes_in_agilent_brochure_rna_set: set = None,
                          ) -> Tuple[dict, dict]:
    """
    Take the two dataframes and print out the JSON map
    :param genes_found_with_transcripts_exons_df: df of data needed for this json printing
    :param genes_in_agilent_brochure_dna_set: Set of genes found in the DNA Table 1
    :param genes_in_agilent_brochure_rna_set: Set of genes found in the RNA Table 2
    :return: Tuple
    """
    genes = sorted(set(genes_found_with_transcripts_exons_df['gene'].to_list()))
    final_map = {gene: get_init_gene_data_structure() for gene in genes}
    # TODO discuss the EGFR not being in this list
    #cnv_df = pd.read_csv(get_agilent_hybrid_capture_dna_cnv_gene_list_from_brochure(), names=['gene'], comment='#')
    cnv_df = pd.DataFrame(get_list_important_cnv_genes(), columns=['gene'])
    cnv_dict = {v: k for k, v in cnv_df['gene'].to_dict().items()}

    technical_snv_match = 0
    technical_cnv_match = 0
    technical_fusion_match = 0
    for gene in genes:
        temp_genes_found_with_transcripts_exons_df = \
            genes_found_with_transcripts_exons_df[genes_found_with_transcripts_exons_df['gene'] == gene]
        # go over rna and dna
        for molecule_type in [MoleculeType.dna.name, MoleculeType.rna.name]:
            # it's possible for genes to have intervals that were annotated in the agilent interval file
            # but were not actually the correct gene (e.g. intervals annotated as PALB2, but after
            # annotation by ensembl were found to be gene PRKCB)
            target_set = genes_in_agilent_brochure_dna_set if molecule_type == MoleculeType.dna.name \
                else genes_in_agilent_brochure_rna_set
            if gene not in target_set:
                continue

            temp_genes_found_with_transcripts_exons_df2 = \
                temp_genes_found_with_transcripts_exons_df[temp_genes_found_with_transcripts_exons_df['molecule_type']
                                                           == molecule_type]
            if len(temp_genes_found_with_transcripts_exons_df2) > 0:  # possible for dna or rna to not have results
                if molecule_type == MoleculeType.dna.name:
                    technical_snv_match += 1
                    # only dna has intervals for now...
                    intervals = \
                        sorted({item
                                for sublist in temp_genes_found_with_transcripts_exons_df2['intervals'].to_list()
                                for item in sublist})
                    intervals = [{'start': interval.split('-')[0], 'stop': interval.split('-')[1]}
                                 for interval in intervals]
                    final_map[gene]['snv_meta_data']['intervals'] = intervals
                    final_map[gene]['snv_meta_data']['is_technical'] = True

                    # update the cnv
                    if gene in cnv_dict:
                        technical_cnv_match += 1
                        final_map[gene]['cnv_meta_data']['is_technical'] = True
                else:
                    technical_fusion_match += 1
                    final_map[gene]['fusion_meta_data']['is_technical'] = True

                _update_json_refseq_mapping(gene_transcripts_exons_df=temp_genes_found_with_transcripts_exons_df2,
                                            gene=gene, final_map=final_map, molecule_type=molecule_type)
        chromosome = str(temp_genes_found_with_transcripts_exons_df['chrom'].iloc[0]).replace('chr', '')
        final_map[gene]['chrom'] = chromosome
    at_least_one_technical_match = 0
    for k, v in final_map.items():
        if v['cnv_meta_data']['is_technical'] is True or \
                v['fusion_meta_data']['is_technical'] is True or \
                v['snv_meta_data']['is_technical'] is True:
            at_least_one_technical_match += 1

    assertions = get_json_assertions_for_gene_map(num_genes_in_map=len(genes),
                                                  at_least_one_technical_match=at_least_one_technical_match,
                                                  technical_snv_match=technical_snv_match,
                                                  technical_cnv_match=technical_cnv_match,
                                                  technical_snv_cnv_match=technical_snv_match,
                                                  technical_fusion_match=technical_fusion_match)

    return final_map, assertions


def run_final_map_asserts(final_map: dict = None,
                          genes_in_agilent_brochure_dna_set: set = None,
                          genes_in_agilent_brochure_rna_set: set = None) -> None:
    """
    Some final tests on the final map to verify the integrity of the data
    (The final_map will become the JSON used in nucleus)
    :param final_map: The final dictionary that will be turned in the JSON map
    :param genes_in_agilent_brochure_dna_set: Set of genes found in the DNA Table 1
    :param genes_in_agilent_brochure_rna_set: Set of genes found in the RNA Table 2
    :return: NoneType
    """
    snv_gene_set = {gene for gene, ds in final_map.items() if ds['snv_meta_data']['is_technical'] is True}
    fusion_gene_set = {gene for gene, ds in final_map.items() if ds['fusion_meta_data']['is_technical'] is True}
    cnv_gene_set = {gene for gene, ds in final_map.items() if ds['cnv_meta_data']['is_technical'] is True}
    # run some final asserts on SNV and Fusions to be sure the data is consistent with the brochure
    assert genes_in_agilent_brochure_dna_set == snv_gene_set, \
        "gene set is different than genes_in_agilent_brochure_dna_set"
    assert genes_in_agilent_brochure_rna_set == fusion_gene_set, \
        "fusion set is different than genes_in_agilent_brochure_rna_set"

    # get the cnvs from Agilent Table 1
    #cnv_df = pd.read_csv(get_agilent_hybrid_capture_dna_cnv_gene_list_from_brochure(), names=['gene'], comment='#')
    cnv_df = pd.DataFrame(get_list_important_cnv_genes(), columns=['gene'])
    cnv_set = set(cnv_df['gene'].to_dict().values())   # must be values() here
    # run the CNV asserts to be sure the data is consistent with the brochure
    assert cnv_set == cnv_gene_set, \
        f"cnv set is different than CNVs from agilent brochure {cnv_set.difference(cnv_gene_set)}"

    # test to make sure the fusion DNA genes from Agilent's brochure and the exons are covered in the RNA component.
    # Currently we list fusions as an RNA component, so all exons covered in Agilent's DNA component need to be
    # covered in the RNA (Table 2) or we have an issue from a reporting standpoint.  This will do the test with asserts
    fusion_table1_dna_dict = fusion_genes_exons_on_dna_panel()
    for gene, ds in final_map.items():
        if gene in fusion_table1_dna_dict:
            preferred_transcript = final_map[gene]['preferred_ensembl_transcript']
            # drop into the gene level transcript with the preferred_transcript (ensembl)
            preferred_transcript_exon_set = \
                {exon
                 for ref_seq, vals in final_map[gene]['refseq_transcripts']['fusions'].items()
                 if preferred_transcript == vals['ensembl_transcript']
                 for exon in vals['exons']}
            assert fusion_table1_dna_dict[gene].intersection(preferred_transcript_exon_set)

    # get genes that are on the RNA but not on DNA panel
    diff_genes_rna_from_dna_set = fusion_gene_set.difference(snv_gene_set)
    # test to make sure the genes that are on the RNA panel, but not on DNA panel, are correct (relative to brochure)
    assert diff_genes_rna_from_dna_set == genes_on_rna_not_on_dna_via_brochure(), "Fusion diff is not correct"

    assert len(final_map) == 691, f"Total number of genes in the map is not 691: {len(final_map)}"
    assert len(snv_gene_set | cnv_gene_set | fusion_gene_set) == 691, f"At least one technical in the map is not " \
                                                                      f"691: {len(final_map)}"
    assert len(snv_gene_set) == 678, f"Total number of technical snv genes is not 678: {len(snv_gene_set)}"
    assert len(cnv_gene_set) == 75, f"Total number of technical cnv genes is not 75: {len(cnv_gene_set)}"
    assert len(snv_gene_set | cnv_gene_set) == 678, f"Total number of technical snv + cnv genes is not 678: " \
                                                    f"{len(snv_gene_set | cnv_gene_set)}"
    assert len(fusion_gene_set) == 80, f"Total number of technical fusion genes is not 80: {len(fusion_gene_set)}"


def _update_json_refseq_mapping(gene_transcripts_exons_df: pd.DataFrame = None,
                                gene: str = None,
                                final_map: dict = None, molecule_type: str = None) -> None:
    """
    Update some of the gene mappings
    :param gene_transcripts_exons_df: Data frame required for the mapping below
    :param gene: The gene of interest
    :param final_map: The final mapping that will be converted to JSON for the nucleus config
    :param molecule_type: dna or rna
    :return: NoneType
    """
    # just need the right key in the mapping.  dna = snvs, rna = fusions
    if molecule_type == MoleculeType.dna.name:
        var_type = 'snvs'
    else:
        var_type = 'fusions'

    # update the refseqs for the gene's refseq_transcripts mapping
    first_canonical_found = 0
    for _, row in gene_transcripts_exons_df.iterrows():
        exons = row['exons']
        # update type
        refseq_acc = row['refseq_accession']
        ensembl_transcript = '.'.join((row['transcript'], str(row['transcript_version_x'])))
        if str(refseq_acc) != 'nan':
            # refseq_acc map to multiple ensembl
            # XM_005267402.1 => ENST00000554581, canonical
            # XM_005267402  =>  ENST00000349310, non canonical
            # canonical will come first gene_transcripts_exons_df, so skip any others with continue
            if refseq_acc in final_map[gene]['refseq_transcripts'][var_type]:
                continue
            else:
                final_map[gene]['refseq_transcripts'][var_type][refseq_acc] = {}
                final_map[gene]['refseq_transcripts'][var_type][refseq_acc]['exons'] = exons
                final_map[gene]['refseq_transcripts'][var_type][refseq_acc]['ensembl_transcript'] = ensembl_transcript
        if row['is_canonical_x'] == 1 and first_canonical_found == 0:
            first_canonical_found = 1
            final_map[gene]['preferred_ensembl_transcript'] = ensembl_transcript
        # store the chromosome level information
        if 'chrom' not in final_map[gene]:
            final_map[gene]['chrom'] = row['chrom'].replace('chr', '')


def genes_on_rna_not_on_dna_via_brochure() -> set:
    """
    Send back a list of genes from the fusion RNA panel that are not on the DNA panel, from the brochure Table 1 & 2
    :return: Set of genes
    """
    return {'BRD3', 'FGR', 'MAST1', 'MAST2', 'MSMB', 'MUSK',
            'NUMBL', 'PKN1', 'PRKCA', 'PRKCB', 'RELA', 'RSPO3', 'THADA'}


def fusion_genes_exons_on_dna_panel() -> dict:
    """
    All of the genes in the DNA that aren for translocations via Table 1. of the brochure
    :return:
    """
    return {
        'ALK': {18, 19},
        'BRAF': {8, 9, 10},
        'CIC': {18, 19},
        'EGFR': {24, 25, 26},
        'FGFR1': {3, 4, 5, 6, 7, 8, 9},
        'FGFR2': {17},
        'FGFR3': {17, 18},
        'NTRK1': {8, 9, 10, 11},
        'RAF1': {7, 8, 9},
        'RET': {7, 10, 11},
        'ROS1': {31, 33, 34, 35},
        'TMPRSS2': {1, 2, 3, 4}
    }


def update_gene_dataframe(df: pd.DataFrame=None, col: str='gene') -> pd.DataFrame:
    df = df.drop(df[df[col] == 'HIST2H3C'].index)
    return df

def get_df_for_analysis(molecule_type: str = None) -> Tuple[pd.DataFrame, set]:
    """
    Main function of the program responsible for getting the required data for each molecule type
    Agilent sent us hg38 data and then sent us a lift over for hg19
    :param molecule_type: dna or rna
    :return: the data frame and the set of genes for a given molecule type
    """
    # open the BED file for the molecule types
    if molecule_type == MoleculeType.dna.name:
        hg19_df = pd.read_csv(get_agilent_hg19_hybrid_capture_dna_targets_bed_file(), comment="#",
                              sep="\t", names=['chrom', 'start', 'end', 'shared_mapping', 'score'])
        hg38_df = pd.read_csv(get_agilent_hg38_hybrid_capture_dna_targets_bed_file(), comment="#",
                              sep="\t", names=['chrom', 'start', 'end', 'gene'])
        # get a data from a file found from the brochure
        genes_from_brochure_df = pd.read_csv(get_agilent_hybrid_capture_dna_gene_list_from_brochure(),
                                             comment="#", names=['gene'])

        hg38_df = update_gene_dataframe(hg38_df)
        genes_from_brochure_df = update_gene_dataframe(genes_from_brochure_df)
        # no coverage found for HIST2H3C

    else:
        hg19_df = pd.read_csv(get_agilent_hg19_hybrid_capture_rna_targets_bed_file(), comment="#",
                              sep="\t", names=['chrom', 'start', 'end', 'shared_mapping', 'score'])
        # this BED file has more columns, but we only care about the first 4
        hg38_df = pd.read_csv(get_agilent_hg38_hybrid_capture_rna_targets_bed_file(), comment="#",
                              sep="\t", usecols=[0, 1, 2, 3], names=['chrom', 'start', 'end', 'gene'])
        # this BED file has gene names that require updating
        hg38_df['gene'] = hg38_df.apply(lambda row: _update_rna_bed_gene_names(row=row), axis=1)
        # get a data from a file found from the brochure
        genes_from_brochure_df = pd.read_csv(get_agilent_hybrid_capture_rna_gene_list_from_brochure(),
                                             comment="#", names=['gene'])

    # get the list of genes from the dataframe
    genes_in_hg38_bed_set = set(hg38_df['gene'].to_list())
    # go over and update DNA list b/c there were issues with some gene naming
    if molecule_type == MoleculeType.dna.name:
        genes_in_hg38_bed_set = _fix_dna_gene_set_from_hg38_bed(set_genes=genes_in_hg38_bed_set)

    # get the list of genes from the brochure (note these gene names updated, see the file comments for more info)
    genes_in_brochure_set = set(genes_from_brochure_df['gene'].to_list())
    # test to make sure these are the same
    assert genes_in_brochure_set == genes_in_hg38_bed_set, "Sets from brochure and BED file are different"

    # update the hg38_df to have the hg19 mapping that agilent sent us, i.e.
    # + 1 on the str(x['start'] + 1) b/c the mapping (chr1:2556617-2556782) in the hg19 is not 0-based like a BED file
    # for the RNA BED.  The updated the DNA BED files they sent us after did not conform to 0-based and instead
    # used 1-based, so add 0
    plus_interval = 0 if molecule_type == MoleculeType.dna.name else 1  # + 1 for RNA BED
    hg19_hg38_df = update_shared_mapping(df1=hg38_df, df2=hg19_df, plus_interval=plus_interval)

    # just print out missing intervals after merging
    temp_df = hg19_df.merge(hg38_df, on='shared_mapping', how='outer', suffixes=('', '_y')).reset_index(drop=True)
    logging.debug(f"Missing rows in merged assay {molecule_type}")
    temp_df = temp_df[temp_df['start'].isna()]
    logging.debug(temp_df)
    missing_intervals = 4 if molecule_type == MoleculeType.dna.name else 0
    # there were four intervals that did not map from hg38 -> hg19, and I tested these at:
    # https://liftover.broadinstitute.org/
    # But just testing here
    # TODO remove comments around asserts
    assert len(temp_df) == missing_intervals, f"There were not {missing_intervals} missing hg19 intervals compared " \
                                              f"to hg19 for assay type: {molecule_type}, temp_df len: {len(temp_df)}"

    update_with_ensembl_information(df=hg19_hg38_df)
    hg19_hg38_df['molecule_type'] = molecule_type  # add this column for future reference what BED file it came from

    # Log some values
    logging.debug(f"Number of rows in {molecule_type} hg38_df {len(hg38_df)}")
    logging.debug(f"Number of rows in {molecule_type} hg19_df {len(hg19_df)}")
    logging.debug(f"Number of rows in {molecule_type} hg19_hg38_df {len(hg19_hg38_df)}")

    # final test to verify the gene lists in hg19_hg38_df for each molecular type.  These df will be eventually concat
    set_genes_in_hg19_df = set(hg19_hg38_df['gene'].to_list())
    if molecule_type == MoleculeType.dna.name:
        # superset here b/c there are gene entries in in the dna component like: CNVBackbone, MSICovered,RPL22, etc.
        # TODO remove comments around asserts and remove pass
        assert set_genes_in_hg19_df.issuperset(genes_in_brochure_set), f"{molecule_type}: set of genes in hg19 df " \
                                                                       f"is not a superset of set from the brochure"
    else:
        assert set_genes_in_hg19_df == genes_in_brochure_set, f"{molecule_type}: set of genes in hg19 df are the same" \
                                                              f"from the brochure"

    return hg19_hg38_df, genes_in_brochure_set


def update_with_ensembl_information(df: pd.DataFrame) -> None:
    """
    Get the ensembl information from the API
    :param df: Dataframe to get the information to query
    :return: NoneType
    """
    assert all(item in df.columns for item in ['chrom','start', 'end']), "Columns ['chrom','start', 'end'] not in df"

    # get a client for downloading ensembl data if required in _get_ensembl_api_data
    ensembl_client = EnsemblRestClient()  # used to download Ensembl data from REST endpoint
    # get the ensemble data (gene, transcript, and exon)
    df['ds_data'] = \
        df.apply(lambda row: _get_ensembl_api_data(row=row, ensembl_client=ensembl_client), axis=1)
    # how many genes were found for each row?
    df['num_genes'] = df.apply(lambda row: len(row['ds_data']), axis=1)

def update_shared_mapping(df1: pd.DataFrame, df2: pd.DataFrame, plus_interval: int) -> pd.DataFrame:
    """
    Update the dataframe (df1) shared mapping so it can be merged to df2
    :param df1: df to update the shared_mapping
    :param df2: df to merge
    :param plus_interval: add 1 or 0
    :return: pd.Dataframe
    """
    df1['shared_mapping'] = \
        df1.apply(lambda x: ":".join((x['chrom'], '-'.join((str(x['start'] + plus_interval), str(x['end']))))), axis=1)
    # merge these data frames
    return df2.merge(df1, on='shared_mapping', how='left', suffixes=('', '_y')).reset_index(drop=True)

def get_ref_seqs_from_ensembl_transcripts(genes_found_df: pd.DataFrame = None) -> pd.DataFrame:
    """
    Take in df (gene, transcript, is_canonical) to query ensembl to get the RefSeq that matches
    to the given transcript
    :param genes_found_df: df of genes and other meta data
    :retrun: data frame of gene, ensembl, refseq ids
    """
    columns = ["gene", "is_canonical", "transcript", "transcript_version", "refseq_accession", "external_db.db_name"]
    database = 'homo_sapiens_core_75_37'
    db_connection = get_pymysql_connection(database=database)
    sep = "\t"
    gene_with_transcript_ids_file = os.path.join(get_storage_dir(), f"{database}_gene_with_transcript_ids.txt")

    gene_transcripts_df = pd.DataFrame(columns=columns)
    if os.path.exists(gene_with_transcript_ids_file):  # if there was data get it
        gene_transcripts_df = pd.read_csv(gene_with_transcript_ids_file, sep=sep)

    final_data = []  # store any results from ensembl, will be added to final df
    # logging.debug("RefSeq Section")
    for _, row in genes_found_df.iterrows():
        gene = row['gene']
        ensembl_id = row['transcript']
        is_canonical = row['is_canonical']
        # if it was already in the df, no need to query ensembl, it was saved already.  This just speeds the program up
        if len(gene_transcripts_df[(gene_transcripts_df['gene'] == gene)
                                   & (gene_transcripts_df['transcript'] == ensembl_id)]) > 0:
            # logging.debug(f"Skipping {gene} {ensembl_id}")
            continue
        # need to query Ensembl
        results = query_to_get_refseq_accession_from_ensembl_id(pymysql_connection=db_connection,
                                                                ensembl_id=ensembl_id, gene=gene)
        # logging.debug(f"Size of results {len(results)}")
        if results:
            if len(results) > 1:
                logging.debug(f"Query returned multiple refseqs {results}")
            else:
                logging.debug(f"{gene} {results}")
            # store for later, since there was a Query
            for result in results:
                # results is a tuple, so convert to a list and add gene and is_canonical
                result = [gene, is_canonical] + list(result)
                final_data.append(result)
        else:
            logging.debug(f"{gene} No RefSeq")
            final_data.append([gene, is_canonical, ensembl_id, None, None, None])

    db_connection.close()

    # there was some data that needs to be added to the file for future runs
    if len(final_data) > 0:
        logging.debug(f"Adding new data to the stored data in {gene_with_transcript_ids_file}")
        temp_df = pd.DataFrame(final_data, columns=columns)
        gene_transcripts_df = pd.concat([gene_transcripts_df, temp_df]).reset_index(drop=True)

    # sort refseq_accession True, so canonical is first, then e.g. NM_001204185.1 comes before XM_005244779.1
    gene_transcripts_df.sort_values(by=['gene', 'is_canonical', 'refseq_accession'],
                                    inplace=True, ascending=[False, False, True])
    # print out the results for future runs of this program so they don't require this querying
    gene_transcripts_df.to_csv(gene_with_transcript_ids_file, sep=sep, index=False)
    # update some manual transcripts
    _manual_refseq_updates(df=gene_transcripts_df)
    # run some asserts over the data
    _run_asserts_on_transcripts(df=gene_transcripts_df,
                                set_genes_test=set(genes_found_df['gene'].to_list()))
    gene_transcripts_df = update_gene_dataframe(gene_transcripts_df)
    return gene_transcripts_df


def _manual_refseq_updates(df: pd.DataFrame) -> None:
    """
    There were two genes that required a manual mapping to refseq b/c they didn't have matches
    :param df: df to updated
    :return: NoneType
    """
    # TERC = lincRNA
    # MAP3K14 = processed_transcript
    # need manual updating
    # TERC https://www.ncbi.nlm.nih.gov/nuccore/NR_001566.1
    df.loc[df['transcript'] == 'ENST00000602385',
           ['refseq_accession', 'external_db.db_name']] = 'NR_001566.1', 'manual_update'
    # https://useast.ensembl.org/Homo_sapiens/Gene/Summary?db=core;g=ENSG00000006062;r=17:45263119-45317029
    df.loc[df['transcript'] == 'ENST00000344686',
           ['refseq_accession', 'external_db.db_name']] = 'NM_003954.5', 'manual_update'


def _run_asserts_on_transcripts(df: pd.DataFrame = None,
                                set_genes_test: set = None) -> None:
    """
    Simple helper to runs some asserts on the input data frame
    :param df: df to test
    :param set_genes_test: set of genes to go over in the df
    :return:
    """
    # verify no genes
    genes_with_no_canonical_ensembl = []  # store any ensembl genes that didn't have a canonical
    genes_with_no_refseq = []  # store any genes that didn't have a refseq match to any ensembl transcript
    genes_where_canonical_with_no_refseq = []  # store genes that have a ensembl canonical but no reseq
    for gene in set_genes_test:
        temp_df = df[df['gene'] == gene]
        # just make sure all ensembl transcripts have a canonical transcript
        if 1 not in temp_df['is_canonical'].to_list():
            logging.debug(f"{gene} has no canonical transcript")
            genes_with_no_canonical_ensembl.append(gene)
        # were there genes that had no refSeq transcript?  They will be NaN
        if temp_df['refseq_accession'].isnull().all():
            genes_with_no_refseq.append(gene)
        # find out what ensembl canonical transcripts are missing a refseq match
        temp_df = temp_df[temp_df["is_canonical"] == 1]
        if temp_df['refseq_accession'].isnull().all():
            genes_where_canonical_with_no_refseq.append(gene)

    assert len(genes_with_no_canonical_ensembl) == 0, f"Some genes did not have an ensembl canonical transcript " \
                                                      f"{genes_with_no_canonical_ensembl}"

    # Only MAP3K14 and TERC did not have a refseq transcript match, but these were updated via _manual_refseq_updates
    # so this is now 0
    assert len(genes_with_no_refseq) == 0, f"All genes should not have a refseq transcript match: " \
                                           f"{genes_with_no_refseq}"
    # used to be 43, but we mapped MAP3K14 and TERC via _manual_refseq_updates
    # TODO remove comments around asserts
    assert len(genes_where_canonical_with_no_refseq) == 41, f"41 genes don't have a ensembl canonical and refseq " \
                                                            f"transcript match to " \
                                                            f"the canonical {genes_where_canonical_with_no_refseq}"

    logging.debug(f"These {len(genes_with_no_refseq)} genes have no refseq match to any ensembl transcript\n"
                  f"{sorted(genes_with_no_refseq)}")

    logging.debug(f"These {len(genes_where_canonical_with_no_refseq)} genes have an ensembl canonical with no refseq\n"
                  f"{sorted(genes_where_canonical_with_no_refseq)}")


def _fix_dna_gene_set_from_hg38_bed(set_genes: set = None) -> set:
    """
    The set will come in with some issues like:
    {'MSICovered,RPL22', 'MSICovered,RAD50', 'SERPINB3;SERPINB4', 'FGF4;CCND1', 'FGFR1;WHSC1L1',
    'MSICovered', 'WHSC1;FGFR3', 'PTEN;CNVBackbone', 'CNVBackbone', 'CNVBackbone;RAF1', 'MSICovered,QKI',
    'FGFR3;WHSC1', 'CCND2;FGF6'}

    So go over this list and resolve it
    :param set_genes:  Original list
    :return: list of resolved genes
    """
    final_list = []
    for gene in set_genes:
        if ',' in gene:
            temp_list_genes = gene.split(",")
            temp_list_genes.remove("MSICovered")
            assert len(temp_list_genes) == 1, "List not equal to one here ','"
            final_list += temp_list_genes
        elif ';' in gene:
            temp_list_genes = gene.split(";")
            if "MSICovered" in temp_list_genes:
                temp_list_genes.remove("MSICovered")
                assert len(temp_list_genes) == 1, "List not equal to one here 'MSICovered'"
            elif "CNVBackbone" in temp_list_genes:
                temp_list_genes.remove("CNVBackbone")
                assert len(temp_list_genes) == 1, "List not equal to one here 'CNVBackbone'"
            final_list += temp_list_genes
        elif gene in ('MSICovered', 'CNVBackbone'):  # do not touch these since they are their own intervals for CNV/MSI
            continue
        else:
            final_list += [gene]
    return set(final_list)


def get_ensembl_transcripts(df_temp_mol_type: pd.DataFrame = None,
                            genes_in_agilent_brochure_set: set = None) -> pd.DataFrame:
    """
    Go over each ds in the ds_data column and get the set of ensembl IDs
    :param df_temp_mol_type: dataframe of all data with transcript and exon information
    :param genes_in_agilent_brochure_set: A list of all genes found from the Agilent Brochure (Table 1 and Table2)
    :return: A dictionary of canonical Ensembl transcripts
    """
    gene_transcript_list = []  # store the data for each row pulling out each transcript and exon level information
    for _, row in df_temp_mol_type.iterrows():
        if row['gene'] in ['CNVBackbone', 'MSICovered']:  # these are unique to CNV and MSI
            continue
        for gene, transcripts in row['ds_data'].items():  # gene (key) has a object as its value
            # in this analysis, TAP2 and KMT2B had multiple canonical transcripts, b/c of their source
            # i.e. one was from 'havana' and the other from 'ensembl'.  This is rare but happened in GRCh37.
            # In this case, store or update to the 'ensembl' source when there are multiple canonical
            if gene in genes_in_agilent_brochure_set:  # was it in the hg38 gene list from Agilent
                for transcript, vals in transcripts.items():
                    # store the values for the data frame later on
                    gene_transcript_list.append([row['chrom'], gene, transcript, vals['version'], vals['is_canonical'],
                                                 vals['exon'], row['molecule_type'],
                                                 '-'.join((str(row['start']), str(row['end'])))])

    gene_to_transcript_id_df = pd.DataFrame(gene_transcript_list,
                                            columns=["chrom", "gene", "transcript", "transcript_version",
                                                     "is_canonical", "exon",
                                                     "molecule_type", "interval"])
    # now summarize each transcript at the exon level, i.e. a transcript will have e.g. 4 exons
    gene_transcripts_aggregated_exons_intervals_df = \
        _get_df_gene_transcript_total_exons(df=gene_to_transcript_id_df)
    return gene_transcripts_aggregated_exons_intervals_df


def _get_df_gene_transcript_total_exons(df: pd.DataFrame) -> pd.DataFrame:
    """
    Go over all transcripts for both DNA and RNA and get back a data frame totalling exons and num of exons for each
    transcript
    :param df: data frame of genes and transcripts
    :return: new dataframe with exons totalled for each transcript
    """
    final_values = []
    # get the set of ensembl transcripts here b/c there will be alot of overlap from the transcripts and intervals
    # but we will use the inner loop to cover the dna and rna
    for transcript in sorted(set(df['transcript'].to_list())):
        df_temp = df[df['transcript'] == transcript]
        # do this for DNA and RNA
        for molecule_type in [MoleculeType.dna.name, MoleculeType.rna.name]:
            df_temp_mol_type = df_temp[df_temp['molecule_type'] == molecule_type]
            if len(df_temp_mol_type) > 0:  # both molecule types, so possible the df will not have rows
                # multiple exons can be covered by one bait (interval) so set, then sort
                exons = sorted({item for sublist in df_temp_mol_type['exon'].to_list() for item in sublist})
                # get a list of intervals
                intervals = df_temp_mol_type['interval'].to_list()
                # logging.debug([df_temp_mol_type['gene'].iloc[0], transcript, df_temp_mol_type['is_canonical'].iloc[0],
                #              exons, len(exons), molecule_type])
                # store the data for later converting to df
                final_values.append([df_temp_mol_type['chrom'].iloc[0], df_temp_mol_type['gene'].iloc[0], transcript,
                                     df_temp_mol_type['transcript_version'].iloc[0],
                                     df_temp_mol_type['is_canonical'].iloc[0], exons, len(exons), intervals,
                                     molecule_type])
    df = pd.DataFrame(final_values,
                      columns=(["chrom", "gene", "transcript", "transcript_version", "is_canonical", "exons",
                                "num_exons", "intervals", "molecule_type"]))
    # sometimes there's multiple canonical so just order it by the canonical that has the most exons
    df.sort_values(by=['gene', 'is_canonical', 'num_exons'],
                   inplace=True, ascending=[False, False, False])
    return df


def _update_rna_bed_gene_names(row: pd.Series):
    """
    Update the RNA Bed file to include only the gene name
    :param row:
    :return:
    """
    # ace|PAX7,ref|PAX7,ref|NM_001135254,ref|NM_013945,ref|NM_002584,ens|ENST00000420770,ens|ENST00000375375
    # ace|FGR 1
    val = row['gene'].split(',')[0].split('|')[1]
    if val == 'GOPCandROS1':
        val = 'ROS1'
    elif val == 'MSMBandNCOA4':
        val = 'MSMB'
    elif val == 'C15orf55':  # outdated: https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/HGNC:29919
        val = 'NUTM1'
    elif val == 'BRCA1P1':
        val = 'BRCA1'
    elif val == 'MLL':  # outdated: https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/HGNC:7132
        val = 'KMT2A'
    return val


def _get_ensembl_api_data(row: pd.Series = None, ensembl_client: EnsemblRestClient = None):
    """
    Get the datastructure that will be used to produce the BED file
    :param row: Series from Pandas
    :param ensembl_client: Instance of the EnsemblRestClient
    :return: Dictionary of either information, or an empty dictionary b/c the interval did not map to anything
    """
    ensembl_objects = get_ensembl_overlap_api_data(row=row, ensembl_client=ensembl_client)

    # could have multiple genes
    # https://grch37.rest.ensembl.org/overlap/region/human/2:58386889-58386950?feature=gene;feature=exon;content-type=application/json
    gene_feature_match = []  # store the gene objects
    transcript_feature_match = []  # store the transcript objects
    exon_feature_match = []  # store the exon objects
    # check gene information matches our gene name in the mapping file passed in at the CLI
    for ensembl_obj in ensembl_objects:
        if ensembl_obj['feature_type'] == 'gene':  # gene information
            gene_feature_match.append(ensembl_obj)
        # store all the transcripts and exon objects for further processing
        elif ensembl_obj['feature_type'] == 'transcript':  # transcript information
            transcript_feature_match.append(ensembl_obj)
        elif ensembl_obj['feature_type'] == 'exon':  # exon information
            exon_feature_match.append(ensembl_obj)

    return_ds = get_return_datastructure(gene_feature_match=gene_feature_match,
                                         transcript_feature_match=transcript_feature_match,
                                         exon_feature_match=exon_feature_match)

    # logging.debug("\n\n")
    # logging.debug(row)
    # logging.debug(pp.pformat(return_ds, indent=3))
    return return_ds


def all_biotypes_seen() -> list:
    """
    All biotypes that should have been used in this analysis
    :return:
    """
    # TERC = lincRNA
    # MAP3K14 = processed_transcript
    return biotypes_to_ignore() + ['protein_coding', 'lincRNA', 'processed_transcript']


def biotypes_to_ignore() -> list:
    """
    http://useast.ensembl.org/info/genome/genebuild/biotypes.html
    :return: A list of biotypes that we will not consider
    """
    return [
        'antisense',
        'miRNA',
        'misc_RNA',
        'non_stop_decay',
        'nonsense_mediated_decay',
        'processed_pseudogene',
        'pseudogene',
        'retained_intron',
        'sense_intronic',
        'sense_overlapping',
        'snRNA',
        'snoRNA',
        'transcribed_processed_pseudogene',
        'transcribed_unprocessed_pseudogene',
        'unitary_pseudogene',
        'unprocessed_pseudogene',
    ]


def get_return_datastructure(gene_feature_match: list = None, transcript_feature_match: list = None,
                             exon_feature_match: list = None):
    """
    Take in the gene, transcript and exon objects (in lists) and produce the final data structure
    :param gene_feature_match: List of ensembl gene objects
    :param transcript_feature_match: List of ensembl transcript objects
    :param exon_feature_match: List of ensembl exon objects
    :return:
    """
    return_ds = {}  # what will be returned
    # go over the gene objects and match the transcripts and exons via their parents (via the 'id')
    for gene_obj in gene_feature_match:
        gene_id = gene_obj['id']
        gene_name = gene_obj['external_name']
        strand = gene_obj['strand']

        # this gene was updated on Agilent Panel
        # https://useast.ensembl.org/Homo_sapiens/Gene/Summary?db=core;g=ENSG00000020181;r=8:37784191-37844896
        # http://feb2014.archive.ensembl.org/Homo_sapiens/Gene/Summary?db=core;g=ENSG00000020181;r=8:37641709-37702414
        if gene_name == 'GPR124':
            gene_name = 'ADGRA2'

        # process the transcript objects
        for transcript_obj in transcript_feature_match:
            if transcript_obj["biotype"] in biotypes_to_ignore():
                # skip these b/c these are not transcripts we care about from a clinical perspective since
                # they do not generate a functioning protein
                continue
            # store some values
            transcript_parent_id = transcript_obj['Parent']
            transcript_id = transcript_obj['id']
            is_canonical = transcript_obj['is_canonical']
            biotype = transcript_obj['biotype']
            source = transcript_obj['source']
            version = transcript_obj['version']
            # test its a biotypes we've seen
            assert biotype in all_biotypes_seen(), f"Missing biotype {biotype}"

            if transcript_parent_id == gene_id:  # this was a match to the Parent gene
                if gene_name not in return_ds:
                    return_ds[gene_name] = {}  # init the dictionary of dictionaries
                return_ds[gene_name][transcript_id] = {
                    'is_canonical': is_canonical,
                    'biotype': biotype,
                    'exon': [],
                    'source': source,
                    'version': version,
                    'strand': strand
                }  # init the datastructure
        # process the exon objects and place the exon in the correct transcript
        for exon_obj in exon_feature_match:
            exon_parent_id = exon_obj['Parent']
            exon_num = exon_obj['rank']
            # it's possible that a gene name was not in the ds, b/c we skipped some gene from havana only,
            # or the transcript was not a biotype we'd consider
            # and one interval (bait) can cover multiple exon, e.g.
            # chr3	12641654	12641914
            # RAF1: ENST00000251849 (canonical) covers exons 8 & 9
            # chr8	38271153	38271846
            # FGFR1: ENST00000425967 (canonical) covers 17, 18, 19
            if gene_name in return_ds and exon_parent_id in return_ds[gene_name]:
                return_ds[gene_name][exon_parent_id]['exon'].append(exon_num)

    return return_ds


def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Create the gene map json for the Agilent GS700 assay')

    parser.add_argument('--json_file_name_version',
                        dest='json_file_name_version',
                        type=int,
                        help='Version to print out... this will be added to final JSON file',
                        required=True)

    return parser.parse_args()


if __name__ == '__main__':
    main()
