"""Look over the exons that were produced from the excel file and the BED file"""
import argparse
import json

def main():
    args = get_cli_args()
    json_file = args.json_file
    with open(json_file) as infh:
        data_struct = json.load(infh)
    for gene, data_dict in data_struct['data'].items():
        total_exons1 = return_total_exons(dict_transcripts=data_dict['refseq_transcripts']['snvs'])
        total_exons2 = return_total_exons(dict_transcripts=data_dict['refseq_transcripts_from_bed']['snvs'])
        len1 = len(total_exons1)
        len2 = len(total_exons2)
        if len1 > 0 or len2 > 0:
            contents_same = total_exons1 == total_exons2
            if len1 != len2:
                print(f"{gene}\tDifferent\tTotal Size1\t{len1}\tTotal Size2\t{len2}\tdiff\t{len1-len2}\t{contents_same}")
            else:
                print(f"{gene}\tSame\tTotal Size1\t{len1}\tTotal Size2\t{len2}\tdiff\t{len1-len2}\t{contents_same}")

def return_total_exons(dict_transcripts: dict=None) -> list:
    total_exons1 = []
    for transcript in dict_transcripts:
        exons1 = dict_transcripts[transcript]['exons']
        total_exons1 += exons1
    return list(set(total_exons1))


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Look over the exons that were produced from the excel file and the '
                                                 'BED file in oc180')

    parser.add_argument('--json_file', dest='json_file',
                        type=str, help='Gene Map Json file',
                        required=True)

    return parser.parse_args()


if __name__ == '__main__':
    main()