"""Module to complete the intervals file for gs120Plus (aka GS v2.0)"""
import json
import pprint
import sys
from lab_validation_120.utils.configs.lab_validation_config import get_oc120plus_targets_bed_file, \
    get_oc120plus_snps_targets_bed_file_from_archer, \
    get_oc120plus_gene_list_for_variant_plex, get_oc120plus_new_gene_list_for_variant_plex, get_oc120plus_new_gene_list_to_keep_for_gs_v2,\
    get_oc120plus_cnv_annotation_dict_from_excel_file, \
    get_oc120plus_msi_list, get_oc120plus_archer_exons_excel_file, \
    get_v2_fusion_annotation_list, get_cnv_annotation_dict_from_cnv_bed_file
from lab_validation_120.utils.eutils import get_annotation_from_dbsnp_id
from tools.gene_maps.gene_map_utils import get_gene_2_transcript_2_exon_map_for_snvs, get_cli_args, \
    update_with_ensembl_transcripts_from_vv, update_with_fusion_genes, update_with_snv_genes, \
    get_init_gene_data_structure, update_with_cnv_genes, print_gene_map_json, \
    get_gene_2_transcript_2_exon_map_for_snvs_from_bed_file

def main():
    # used the file from Archer: VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx (dSA16897_v3) in
    # lab_validation_120.utils.configs.lab_validation_config
    args = get_cli_args()
    assay_name = args.assay_name
    # get the gene lists needed
    oc120plus_gene_list = get_oc120plus_gene_list_for_variant_plex(assay_name=assay_name)
    oc120plus_gene_list.append('None')  # this is a gene name in the .bed file, so just add here

    # get the new set of genes and remove Rodrigo's list that he wants to keep
    new_genes_in_oc120plus_2_remove = \
        set(get_oc120plus_new_gene_list_for_variant_plex(assay_name=assay_name)) - \
        set(get_oc120plus_new_gene_list_to_keep_for_gs_v2(assay_name=assay_name))

    # get the map of genes to transcripts to exons
    gene_2_transcript_2_exon_map_for_snvs = get_gene_2_transcript_2_exon_map_for_snvs(assay_name=assay_name, callable_function=get_oc120plus_archer_exons_excel_file)

    gene_2_transcript_2_exon_map_for_snvs_from_bed_file = \
        get_gene_2_transcript_2_exon_map_for_snvs_from_bed_file(assay_name=assay_name,
                                                                callable_function=get_oc120plus_snps_targets_bed_file_from_archer)

    # was not in column A of the excel file,  but could be found in the bed targets_file column
    not_in_gene_column_of_excel = {}
    # was in the list of genes to be removed by Rodrigo
    new_genes_removed = {}
    # where all the data is is finally stored
    final_map = {}
    clinical_gene_map = {}
    technical_list_snv_gene = []
    clinical_list_snv_gene = []

    # go over the bed file
    bed_file_function = get_oc120plus_targets_bed_file
    if assay_name == 'oc120plus_snps':
        bed_file_function = get_oc120plus_snps_targets_bed_file_from_archer
    with open(bed_file_function(), 'r') \
            as interval_file_fh, open(args.bed_file_2_print, 'w') as bed_outfh:
        if assay_name == 'oc120plus':  # this interval vile has a: track name="16897_targets" ...... on first line
            print(interval_file_fh.readline(), end='', file=bed_outfh)  # print the header

        for line in interval_file_fh:
            # The Archer bed file does not have consisent pattern of labels, e.g.
            # archer has e.g. like IGF1R_NM_000875_exon_021_TILE_i5ymWZsJ
            # archer has e.g. like TSC2;NM_001114382.2_Exon_2
            # arhcer has e.g. like E2F2_l216
            # so temporarily replace _

            # replace values like they were before for printing of the intervals file
            # Note these are not the actual exon names, this comes from the spreadsheet in:
            # get_oc120plus_archer_exons_excel_file()
            line = line.replace('exon;', 'Exon_')
            line = line.replace('Exon;', 'Exon_')
            line = line.replace('NM;', 'NM_')

            line = line.rstrip().replace('_', ';')

            # get the gene name
            values = line.split("\t")
            values[1] = int(values[1])
            values[2] = int(values[2])
            gene_name = values[3].split(";")[0]


            # was it a gene that was in the new list, but not in the list to remove
            if gene_name in oc120plus_gene_list and gene_name not in new_genes_in_oc120plus_2_remove:
                print(line, file=bed_outfh)
                update_with_snv_genes(values=values, gene_name=gene_name, final_map=final_map, is_snv_match=True,
                                      gene_2_exon_map=gene_2_transcript_2_exon_map_for_snvs,
                                      gene_2_exon_map_from_bed=gene_2_transcript_2_exon_map_for_snvs_from_bed_file,
                                      clinical_list_snv_gene=clinical_list_snv_gene, assay_name=assay_name)
                clinical_gene_map[gene_name] = clinical_gene_map.get(gene_name, 0) + 1

            else:
                # was this a gene that is part of the larger panel for GS v2, but Rodrigo has asked to remove it for 2.0
                if gene_name in new_genes_in_oc120plus_2_remove:
                    new_genes_removed[gene_name] = new_genes_removed.get(gene_name, 0) + 1

                # it was not in the oc120plust_gene_list, and was not in the new_genes_in_oc120plus_2_remove, so these
                # are cases we still want to have in the intervals file, b/c Archer uses coordinate based targets_file for
                # specific hot spots or SNPs targeted, defining CNV only primers, or exon targets_file that are not found in
                # the human reference used by their design software.
                else:
                    print(line, file=bed_outfh)
                    not_in_gene_column_of_excel[gene_name] = not_in_gene_column_of_excel.get(gene_name, 0) + 1
                # either way, put into the final map
                update_with_snv_genes(values=values, gene_name=gene_name, final_map=final_map, is_snv_match=False,
                                      gene_2_exon_map=gene_2_transcript_2_exon_map_for_snvs,
                                      gene_2_exon_map_from_bed=gene_2_transcript_2_exon_map_for_snvs_from_bed_file,
                                      technical_list_snv_gene=technical_list_snv_gene, assay_name=assay_name)
    # update the CNV data
    #technical_list_cnv_gene, clincial_list_cnv_gene = update_with_cnv_genes_old(final_map=final_map, assay_name=assay_name)
    technical_list_cnv_gene, clincial_list_cnv_gene = \
        update_with_cnv_genes(assay_name=assay_name,
                              final_map=final_map, callable_function=get_cnv_annotation_dict_from_cnv_bed_file)

    technical_list_fusion_gene, clincial_list_fusion_gene, control_list_fusion_gene = \
        update_with_fusion_genes(final_map=final_map, callable_function=get_v2_fusion_annotation_list)
    remove_msi_labels(final_map=final_map)
    update_with_ensembl_transcripts_from_vv(final_map=final_map)

    # print out the sorted genes sorted by occurrence for genes that were not in columns A-C of:
    # VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx
    with open(args.no_match_file, 'w') as outfh:
        for k, v in sorted(not_in_gene_column_of_excel.items(), key=lambda item: item[1], reverse=True):
            print(f"{k}\t{v}", file=outfh)
    assert len(not_in_gene_column_of_excel) == 54, f"not_in_gene_column_of_excel is not 54, got " \
                                                   f"{len(not_in_gene_column_of_excel)}"

    # print out the list of genes that were removed, should be 52 - 15 = 37
    with open(args.not_in_gs_2, 'w') as outfh:
        for k, v in sorted(new_genes_removed.items(), key=lambda item: item[1], reverse=True):
            print(f"{k}\t{v}", file=outfh)
    assert len(new_genes_removed) == 37, f"New genes is not 37, got {len(new_genes_removed)}"

    final_genes_list_snvs_indels_cnvs = \
        sorted(list(set(get_oc120plus_gene_list_for_variant_plex(assay_name=assay_name)) -
                    new_genes_in_oc120plus_2_remove))
    if assay_name == 'oc120plus':
        assert len(final_genes_list_snvs_indels_cnvs) == 105, f"Final list of genes is not 105, got {len(final_genes_list_snvs_indels_cnvs)}"
    elif assay_name == 'oc120plus_snps':
        assert len(final_genes_list_snvs_indels_cnvs) == 108, f"Final list of genes is not 108, got {len(final_genes_list_snvs_indels_cnvs)}"

    assert_returns = run_assertions(final_map=final_map,
                                    final_genes_list_snvs_indels_cnvs=final_genes_list_snvs_indels_cnvs,
                                    technical_list_cnv_gene=technical_list_cnv_gene,
                                    clincial_list_cnv_gene=clincial_list_cnv_gene,
                                    technical_list_fusion_gene=technical_list_fusion_gene,
                                    clincial_list_fusion_gene=clincial_list_fusion_gene,
                                    control_list_fusion_gene=control_list_fusion_gene,
                                    clinical_list_snv_gene=clinical_list_snv_gene,
                                    technical_list_snv_gene = technical_list_snv_gene,
                                    assay_name=assay_name)

    print_gene_map_json(version=args.json_file_name_version, assay_name=assay_name, final_map=final_map,
                        assertions=assert_returns)

    #pp = pprint.PrettyPrinter(indent=3)
    #pp.pprint(final_map)


def remove_msi_labels(final_map=None):
    """Remove the MSI labels, these were not genes, so they will be removed
    :param final_map: The final data structure that will be printed out in JSON format
    """
    for label in get_oc120plus_msi_list():
        if label not in final_map:
            raise Exception
        del final_map[label]


def update_with_cnv_genes_old(final_map=None, assay_name=None):
    """
    Open the excel file I created for excel, and covert it to a dictionary and return it
    :param final_map: The final data structure that will be printed out in JSON format
    """
    # open the spreadsheet and get the data and return a ds like documented below
    cnv_dict = get_oc120plus_cnv_annotation_dict_from_excel_file(assay_name=assay_name)
    #print(cnv_dict)
    # update the final_map
    #     gene1: {
    #           "cnv_meta_data": {
    #           "is_clinical": false,
    #           "is_technical": true,
    #           "is_control": false,
    #           "num_primers": 58
    #},
    technical_list_cnv_gene = []
    clincial_list_cnv_gene = []
    for gene, dict_ in cnv_dict.items():
        if gene not in final_map:
            final_map[gene] = get_init_gene_data_structure()  # init the key
            final_map[gene]['cnv_meta_data']['num_primers'] = dict_['num_primers']
        # test values in the spreadsheet that was passed in and turned into a dictionary
        if cnv_dict[gene]['num_primers'] >= 3:  # >= 3 or more primers and Archer said we could use for CNV analysis
            final_map[gene]['cnv_meta_data']['is_technical'] = True
            final_map[gene]['cnv_meta_data']['num_primers'] = cnv_dict[gene]['num_primers']
            technical_list_cnv_gene.append(gene)
            if cnv_dict[gene]['clinically_relevant'] is True:  # has to be true b/c Rodrido listed it in the spreadsheet
                final_map[gene]['cnv_meta_data']['is_clinical'] = True
                clincial_list_cnv_gene.append(gene)

    return technical_list_cnv_gene, clincial_list_cnv_gene


def update_with_snv_genes2(values=None, gene_name=None, final_map=None, is_snv_match=None, gene_2_exon_map=None,
                          clinical_list_snv_gene=None, technical_list_snv_gene=None):
    """
    Update the final map for VV
    :param values: The line was split on tab into values
    :param gene_name: The gene name for what was sent it, this could bd an rs number or other as well...
    :param final_map: The final mapping of genes to the values needed for VV, see top of module for more information
    :param is_snv_match:  Just a boolean to state whether it was a match or not to GS v2.0 genes
    :param gene_2_exon_map: Gene 2 exon map returned by get_gene_2_transcript_2_exon_map_for_snvs, pass in so we dont have to call many times
    """
    rs_number = None
    if gene_name.startswith("rs"):
        gene_name, rs_number = gene_name_from_rs_number(rs_number=gene_name, values=values)
        # if not gene name came back then rs is skipped, also skipping adding in genes like: LOC105373422
        if gene_name is None or gene_name.startswith('LOC'):
            return
    # These can be ignored can be ignored, and will end up in the intervals file, just not the map
    # bed;target;0;chr6:117631374-117631375,
    # SNP_ID_X_chr
    elif gene_name in ('bed', 'SNP'):
        return

    start = values[1]
    stop = values[2]
    diff = stop - start
    hotspot = None
    if diff == 1:
        hotspot = {
            'start': start,
            'stop': stop,
        }

    if gene_name in final_map:
        if rs_number:
            final_map[gene_name]['snv_meta_data']['snps'].append(rs_number)
        elif hotspot:
            final_map[gene_name]['snv_meta_data']['hotspots'].append(hotspot)
        final_map[gene_name]['snv_meta_data']['intervals'].append({'start': start, 'stop': stop})
    else:
        final_map[gene_name] = get_init_gene_data_structure()
        final_map[gene_name]['snv_meta_data']['intervals'] = [{'start': start, 'stop': stop}]
        # all snvs get mapped here once it gets here
        final_map[gene_name]['snv_meta_data']['hotspots'] = [hotspot] if hotspot and not rs_number else []
        final_map[gene_name]['snv_meta_data']['snps'] = [rs_number] if rs_number else []
        final_map[gene_name]['refseq_transcripts']['snvs'] = gene_2_exon_map[gene_name] if gene_name in gene_2_exon_map else {}
        # right now if it's a SNP or just a hotspot it will get the SNV, INDEL

    if is_snv_match is True:
        final_map[gene_name]['snv_meta_data']['is_clinical'] = True
        final_map[gene_name]['snv_meta_data']['is_technical'] = True
        clinical_list_snv_gene.append(gene_name)
    else: # only is_technical
        if hotspot is None and rs_number is None:
            final_map[gene_name]['snv_meta_data']['is_technical'] = True
            technical_list_snv_gene.append(gene_name)


def gene_name_from_rs_number2(rs_number=None, values=None):
    """
    Go off and get the gene name from the rs_number
    :param rs_number: What was rs_number number passed in to query, or get the file
    :param values: The values from the interval line, split into a list, where I'll further split
    """
    # store this for later
    rs_number_values = values[3].split(';')
    rs_number_ref_allele = rs_number_values[1]
    rs_number_alt_allele = rs_number_values[2]
    rs_number_alt_allele_freq = float(rs_number_values[3])
    # get the actual gene name
    #print(rs_number, file=sys.stderr)
    snp_id, eutils = get_annotation_from_dbsnp_id(rs_number=rs_number)
    gene_name = None
    rs_number_dict = {}
    if eutils and len(eutils["result"][snp_id]["genes"]) > 0:
        gene_name = eutils["result"][snp_id]["genes"][0]['name']
        rs_number_dict = {
            'start': values[1],
            'stop': values[2],
            'ref_allele': rs_number_ref_allele,
            'alt_allele': rs_number_alt_allele,
            'alt_allele_freq': rs_number_alt_allele_freq,
            'rs_number': rs_number
        }
    return gene_name, rs_number_dict


def run_assertions(final_map=None, final_genes_list_snvs_indels_cnvs=None, technical_list_cnv_gene=None,
                   clincial_list_cnv_gene=None,
                   technical_list_fusion_gene=None, clincial_list_fusion_gene=None, control_list_fusion_gene=None,
                   clinical_list_snv_gene=None,
                   technical_list_snv_gene=None,
                   assay_name=None):
    """
    Just do some checking to make this matches the VariantPlex®_OncoclinicasSolidTumor_v1_v3.xlsx (dSA16897_v3)
    """
    # test a gene for individual exon elements for snvs
    assert final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons'][0] == 4, "ABL1 exons[0] is not 4"
    assert final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons'][1] == 5, "ABL1 exons[0] is not 5"
    assert final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons'][2] == 6, "ABL1 exons[0] is not 6"
    assert final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons'][3] == 7, "ABL1 exons[0] is not 7"
    assert len(final_map['ABL1']['snv_meta_data']['snps']) == 0, "ABL1 snps is not empty"
    assert len(final_map['ABL1']['refseq_transcripts']['snvs']['NM_005157']['exons']) == 4, "ABL1 exons size is not 4"
    assert final_map['ABL1']['snv_meta_data']['is_technical'] is True, 'ABL1 does not have True for snv is_technical'
    assert final_map['ABL1']['cnv_meta_data']['is_technical'] is True, 'ABL1 does not have True for cnv is_technical'
    assert final_map['ABL1']['fusion_meta_data']['is_technical'] is False, \
        'ABL1 does not have True for fusion is_technical'
    assert final_map['ABL1']['fusion_meta_data']['is_technical'] is False, \
        'ABL1 does not have  True for fusion is_technical'

    # test gene SDHB
    assert final_map['SDHB']['snv_meta_data']['is_technical'] is True, 'SDHB does not have True for snv is_technical'
    assert final_map['SDHB']['cnv_meta_data']['is_technical'] is True, 'SDHB does not have True for cnv is_technical'
    assert final_map['SDHB']['fusion_meta_data']['is_technical'] is False, \
        'SDHB does not have False for fusion is_technical'
    assert final_map['SDHB']['fusion_meta_data']['is_technical'] is False, \
        'SDHB does not have False for fusion is_technical'
    assert final_map['SDHB']['fusion_meta_data']['is_control'] is False, \
        'RAB7A does not have False for fusion is_technical'

    # test the control genes gene, that was only a FUSION control
    for gene in ['RAB7A', 'GPI', 'CHMP2A', 'VCP']:
        assert final_map[gene]['snv_meta_data']['is_technical'] is False, \
            'RAB7A does not have False for snv is_technical'
        assert final_map[gene]['cnv_meta_data']['is_technical'] is False, \
            'RAB7A does not have False for cnv is_technical'
        assert final_map[gene]['cnv_meta_data']['is_control'] is False, \
            'RAB7A does not have False for cnv is_technical'
        assert final_map[gene]['fusion_meta_data']['is_technical'] is False, \
            'RAB7A does not have False for fusion is_technical'
        assert final_map[gene]['fusion_meta_data']['is_technical'] is False, \
            'RAB7A does not have False for fusion is_technical'
        assert final_map[gene]['fusion_meta_data']['is_control'] is True, \
            'RAB7A does not have True for fusion is_technical'

    # check out exons for fusions TMPRSS2 that has two transcripts
    assert final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_001135099']['exons'][0] == 1, \
        "TMPRSS2 (NM_001135099)  exons[0] is not 1"
    assert final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_001135099']['exons'][1] == 2, \
        "TMPRSS2 (NM_001135099) exons[0] is not 2"
    assert len(final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_001135099']['exons']) == 2, \
        "TMPRSS2 (NM_001135099) length is not 2"

    assert final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_005656']['exons'][4] == 6, \
        "TMPRSS2 (NM_005656) exons[0] is not 2"
    assert len(final_map['TMPRSS2']['refseq_transcripts']['fusions']['NM_005656']['exons']) == 5, \
        "TMPRSS2 (NM_005656) length is not 2"
    assert len(final_map['TMPRSS2']['refseq_transcripts']['fusions'].keys()) == 2, \
        "TMPRSS2 trascripts for fusions is not 2"


    # test another gene with one transcript
    assert final_map['NPM1']['refseq_transcripts']['snvs']['NM_002520']['exons'][0] == 11, "NPM1 exons size is not 11"
    assert len(final_map['NPM1']['refseq_transcripts']['snvs']['NM_002520']['exons']) == 1, "NPM1 exons size is not 1"

    # test another gene with two transcripts
    assert len(final_map['BRCA1']['refseq_transcripts']['snvs']['NM_007300']['exons']) == 23, \
        "BRCA1 exons size is not 23"
    assert len(final_map['BRCA1']['refseq_transcripts']['snvs']['NM_007294']['exons']) == 23, \
        "BRCA1 exons size is not 23"
    assert final_map['BRCA1']['refseq_transcripts']['snvs']['NM_007294']['exons'][9] == 10, "BRCA1 exons[9] is not 10"

    # test some sizes
    assert len(final_map['TSC1']['refseq_transcripts']['snvs']['NM_000368']['exons']) == 21, "NPM1 exons size is not 21"
    assert len(final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons']) == 9, "NPM1 exons size is not 9"

    # test snps and other feathers in AMLEX
    assert len(final_map['AMELX']['snv_meta_data']['snps']) == 1, \
        "AMLEX has the incorrect number of snps"
    assert final_map['AMELX']['snv_meta_data']['snps'][0]['alt_allele'] == 'A', \
        "snps [0] has the incorrect alt_allele"
    assert final_map['AMELX']['snv_meta_data']['snps'][0]['ref_allele'] == 'G', \
        "snps [0]  has the incorrect ref_allele"
    assert final_map['AMELX']['snv_meta_data']['snps'][0]['start'] == 11314634, \
        "snps [0]  has the incorrect start"
    assert final_map['AMELX']['snv_meta_data']['snps'][0]['stop'] == 11314635, \
        "snps [0]  has the incorrect stop"
    assert final_map['AMELX']['snv_meta_data']['snps'][0]['rs_number'] == 'rs5979395', \
        "snps [0]  has the rs_number"

    # BYSL CNV data, does have one pimer for CNV, but this is not enough to do CNV calling
    assert final_map['BYSL']['cnv_meta_data']['is_technical'] is False, 'DACH2 has False for is_technical'

    # test hotspots
    assert final_map['AKT2']['snv_meta_data']['hotspots'][0]['start'] == 40742349, \
        "incorrect start for the first hotspots for AKT2"
    assert final_map['AKT2']['snv_meta_data']['hotspots'][0]['stop'] == 40742350, \
        "incorrect stop for the first hotspots for AKT2"
    assert final_map['AKT2']['snv_meta_data']['hotspots'][1]['start'] == 40748059, \
        "incorrect start for the second hotspots for AKT2"
    assert final_map['AKT2']['snv_meta_data']['hotspots'][1]['stop'] == 40748060, \
        "incorrect stop for the second hotspots for AKT2"
    assert final_map['AKT2']['refseq_transcripts']['snvs']['NM_001626']['exons'][0] == 2, "AKT2 exons[0] is not 2"

    # test the intervals
    assert final_map['XRCC3']['snv_meta_data']['intervals'][0]['start'] == 104165132, \
        "intervals [0] has the incorrect start"
    assert final_map['XRCC3']['snv_meta_data']['intervals'][0]['stop'] == 104165356, \
        "intervals [0] has the incorrect stop"

    assert final_map['XRCC3']['snv_meta_data']['intervals'][1]['start'] == 104165467, \
        "intervals [1] has the incorrect start"
    assert final_map['XRCC3']['snv_meta_data']['intervals'][1]['stop'] == 104165518, \
        "intervals [1] has the incorrect stop"

    assert final_map['XRCC3']['snv_meta_data']['intervals'][6]['start'] == 104177367, \
        "intervals [1] has the incorrect start"
    assert final_map['XRCC3']['snv_meta_data']['intervals'][6]['stop'] == 104177426, \
        "intervals [1] has the incorrect stop"

    assert len(final_map['XRCC3']['snv_meta_data']['intervals']) == 7, \
        "intervals has an incorrect length for intervals"

    """ Validation data for CAMTA1 """
    assert final_map['CAMTA1']['cnv_meta_data']['is_technical'] is True, "CAMTA1 cnv is_technical is not True"
    assert final_map['CAMTA1']['cnv_meta_data']['num_primers'] == 4, "CAMTA1 cnv num_primers is not 4"
    assert final_map['CAMTA1']['fusion_meta_data']['is_technical'] is True, "CAMTA1 fusion is_technical is not True"
    assert final_map['CAMTA1']['fusion_meta_data']['is_control'] is False, "CAMTA1 fusion is_control is not False"
    # match data
    assert len(final_map['CAMTA1']['snv_meta_data']['snps']) == 0, "CAMTA1 snps list length is not 0"
    # hotspots
    assert final_map['CAMTA1']['snv_meta_data']['hotspots'][0]['start'] == 6846842, \
        "CAMTA1 hotspots[0] is not 6846842"
    assert final_map['CAMTA1']['snv_meta_data']['hotspots'][0]['stop'] == 6846843, \
        "CAMTA1 hotspots[0] is not 6846843"
    assert final_map['CAMTA1']['snv_meta_data']['hotspots'][1]['start'] == 6894652, \
        "CAMTA1 hotspots[1] is not 6894652"
    assert final_map['CAMTA1']['snv_meta_data']['hotspots'][1]['stop'] == 6894653, \
        "CAMTA1 hotspots[1] is not 6894653"
    assert final_map['CAMTA1']['snv_meta_data']['hotspots'][2]['start'] == 7525813, \
        "CAMTA1 hotspots[2] is not 7525813"
    assert final_map['CAMTA1']['snv_meta_data']['hotspots'][2]['stop'] == 7525814, \
        "CAMTA1 hotspots[2] is not 7525814"
    assert final_map['CAMTA1']['snv_meta_data']['hotspots'][3]['start'] == 7820874, \
        "CAMTA1 hotspots[3] is not 7820874"
    assert final_map['CAMTA1']['snv_meta_data']['hotspots'][3]['stop'] == 7820875, \
        "CAMTA1 hotspots[3] is not 7820875"
    assert len(final_map['CAMTA1']['snv_meta_data']['hotspots']) == 4, \
        "CAMTA1 hotspots length is not 4"
    # intervals
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][0]['start'] == 6846842, \
        "CAMTA1 intervals[0] is not 6846842"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][0]['stop'] == 6846843, \
        "CAMTA1 intervals[0] is not 6846843"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][1]['start'] == 6894652, \
        "CAMTA1 intervals[1] is not 6894652"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][1]['stop'] == 6894653, \
        "CAMTA1 intervals[1] is not 6894653"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][2]['start'] == 7525813, \
        "CAMTA1 intervals[2] is not 7525813"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][2]['stop'] == 7525814, \
        "CAMTA1 intervals[2] is not 7525814"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][3]['start'] == 7820874, \
        "CAMTA1 intervals[3] is not 7820874"
    assert final_map['CAMTA1']['snv_meta_data']['intervals'][3]['stop'] == 7820875, \
        "CAMTA1 intervals[3] is not 7820875"
    assert len(final_map['CAMTA1']['snv_meta_data']['intervals']) == 4, \
        "CAMTA1 intervals length is not 4"
    # test transcripts data
    assert len(final_map['CAMTA1']['refseq_transcripts']['snvs'].keys()) == 0, \
        "CAMTA1 refseq_transcripts snvs length is not 0"
    assert len(final_map['CAMTA1']['refseq_transcripts']['fusions'].keys()) == 1, \
        "CAMTA1 refseq_transcripts fusions length is not 1"

    """ Validation data for ALK """
    # match data
    assert final_map['ALK']['cnv_meta_data']['is_technical'] is True, "ALK cnv is_technical is not True"
    assert final_map['ALK']['cnv_meta_data']['num_primers'] == 28, "ALK cnv num_primers is not 32"
    assert final_map['ALK']['fusion_meta_data']['is_technical'] is True, "ALK fusion is_technical is not True"
    assert final_map['ALK']['fusion_meta_data']['is_control'] is False, "ALK fusion is_control is not False"
    assert final_map['ALK']['snv_meta_data']['is_technical'] is True, "ALK snv is_technical is not True"
    assert len(final_map['ALK']['snv_meta_data']['snps']) == 0, "ALK snps list length is not 0"
    # hotspots
    assert final_map['ALK']['snv_meta_data']['hotspots'][0]['start'] == 29416942, \
        "ALK hotspots[0] is not 29416942,"
    assert final_map['ALK']['snv_meta_data']['hotspots'][0]['stop'] == 29416943, \
        "ALK hotspots[0] is not 29416943"
    assert final_map['ALK']['snv_meta_data']['hotspots'][1]['start'] == 29456569, \
        "ALK hotspots[1] is not 29456569,"
    assert final_map['ALK']['snv_meta_data']['hotspots'][1]['stop'] == 29456570, \
        "ALK hotspots[1] is not 29456570"
    assert final_map['ALK']['snv_meta_data']['hotspots'][2]['start'] == 29543584, \
        "ALK hotspots[2] is not 29543584,"
    assert final_map['ALK']['snv_meta_data']['hotspots'][2]['stop'] == 29543585, \
        "ALK hotspots[2] is not 29543585"
    assert final_map['ALK']['snv_meta_data']['hotspots'][3]['start'] == 29940578, \
        "ALK hotspots[3] is not 29940578,"
    assert final_map['ALK']['snv_meta_data']['hotspots'][3]['stop'] == 29940579, \
        "ALK hotspots[3] is not 29940579"
    assert len(final_map['ALK']['snv_meta_data']['hotspots']) == 4, "ALK hotspots length is not 4"
    # intervals
    assert final_map['ALK']['snv_meta_data']['intervals'][-4]['start'] == 29456569, \
        "ALK intervals[-4] is not 29456569,"
    assert final_map['ALK']['snv_meta_data']['intervals'][-4]['stop'] == 29456570, "ALK intervals[-4] is not 29456570"
    assert final_map['ALK']['snv_meta_data']['intervals'][-3]['start'] == 29543584, \
        "ALK intervals[-3] is not 29543584,"
    assert final_map['ALK']['snv_meta_data']['intervals'][-3]['stop'] == 29543585, "ALK intervals[-3] is not 29543585"
    assert final_map['ALK']['snv_meta_data']['intervals'][-2]['start'] == 29940578, \
        "ALK intervals[-2] is not 29940578,"
    assert final_map['ALK']['snv_meta_data']['intervals'][-2]['stop'] == 29940579, "ALK intervals[-2] is not 29940579"
    assert final_map['ALK']['snv_meta_data']['intervals'][-1]['start'] == 30142856, \
        "ALK intervals[-1] is not 30142856,"
    assert final_map['ALK']['snv_meta_data']['intervals'][-1]['stop'] == 30143527, "ALK intervals[-1] is not 30143527"
    assert len(final_map['ALK']['snv_meta_data']['intervals']) == 13, "ALK intervals length is not 13"
    # test transcripts snv data
    assert len(final_map['ALK']['refseq_transcripts']['snvs'].keys()) == 1, \
        "ALK refseq_transcripts snvs length is not 1"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][0] == 1, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 1"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][1] == 21, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 21"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][2] == 22, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 22"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][3] == 23, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 23"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][4] == 24, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 24"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][5] == 25, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 25"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][6] == 26, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 26"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][7] == 27, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 27"
    assert final_map['ALK']['refseq_transcripts']['snvs']['NM_004304']['exons'][8] == 28, \
        "ALK refseq_transcripts->snvs->MM_004304->exons is not 28"
    # test refseq_transcripts fusion data
    assert len(final_map['ALK']['refseq_transcripts']['fusions'].keys()) == 1, \
        "ALK refseq_transcripts fusions length is not 1"
    assert final_map['ALK']['refseq_transcripts']['fusions']['NM_004304']['exons'][0] == 2, \
        "ALK refseq_transcripts->fusions->MM_004304->exons is not 2"
    assert final_map['ALK']['refseq_transcripts']['fusions']['NM_004304']['exons'][-1] == 26, \
        "ALK refseq_transcripts->fusions->MM_004304->exons is not 26"

    assert len(final_map['ALK']['refseq_transcripts']['fusions'].keys()) == 1, \
        "ALK refseq_transcripts fusions length is not 1"

    # Fusion controls
    assert final_map['GPI']['fusion_meta_data']['is_control'] is True, "GPI fusion is not True"
    assert final_map['VCP']['fusion_meta_data']['is_control'] is True, "VCP fusion is not True"
    assert final_map['RAB7A']['fusion_meta_data']['is_control'] is True, "RAB7A fusion is not True"
    assert final_map['CHMP2A']['fusion_meta_data']['is_control'] is True, "CHMP2A fusion is not True"

    at_least_one_technical_match = 0   # look for genes with at least one technical match in snv, cnv, or fusion
    at_least_one_technical_match_gene_list = []  # store the genes from above
    all_snv_fusion_cvn_clinical_match_inclusive = 0  # look for genes
    all_snv_fusion_cvn_clinical_match_gene_list_inclusive = []

    technical_cnv_match = 0  # at least matched cnv technical
    technical_cnv_match_list = []
    technical_snv_match = 0  # at least matched snv technical
    technical_snv_match_list = []
    technical_fusion_match = 0  # at least matched snv technical
    technical_fusion_match_list = []

    for gene in final_map:
        # how many genes had at least one is_technical match in the snv, cnv, or fusion
        if final_map[gene]['cnv_meta_data']['is_technical'] is True or \
                final_map[gene]['fusion_meta_data']['is_technical'] is True or \
                final_map[gene]['snv_meta_data']['is_technical'] is True:
            at_least_one_technical_match += 1
            at_least_one_technical_match_gene_list.append(gene)

        if final_map[gene]['snv_meta_data']['is_technical'] is True:
            technical_snv_match += 1
            technical_snv_match_list.append(gene)
        if final_map[gene]['cnv_meta_data']['is_technical'] is True:
            technical_cnv_match += 1
            technical_cnv_match_list.append(gene)
        if final_map[gene]['fusion_meta_data']['is_technical'] is True:
            technical_fusion_match += 1
            technical_fusion_match_list.append(gene)
    plus = 0
    if assay_name == 'oc120plus_snps':
        plus = 3  # three new genes in this assay
    assert_returns = {}
    assert len(final_map.keys()) == 246 + plus, f'The total number of genes is {len(final_map.keys())}'
    assert_returns['num_genes_in_map'] = {'num': len(final_map.keys()), 'doc_str': f'The total number of genes is {len(final_map.keys())}'}

    assert at_least_one_technical_match == 224 + plus, f'The total number of technical genes is {at_least_one_technical_match}'
    assert_returns['at_least_one_technical_match'] = \
        {'num': at_least_one_technical_match, 'doc_str': f'The total number of technical genes is {at_least_one_technical_match}'}

    assert technical_snv_match == 143 + plus, f'The total number of technical snv genes is {technical_snv_match}'
    assert_returns['technical_snv_match'] = {'num': technical_snv_match, 'doc_str': f'The total number of techncial snv genes is {technical_snv_match}'}

    #technical_snv_match_list_set = set(technical_snv_match_list)
    # print(sorted(technical_snv_match_list), "\n\n")
    # print(sorted(technical_snv_match_list), "\n\n")
    #print(sorted(set(technical_cnv_match_list + technical_snv_match_list + technical_fusion_match_list)))

    assert technical_cnv_match == 142 + plus, f'The total number of clinical cnv genes is {technical_cnv_match}'
    assert_returns['technical_cnv_match'] = {'num': technical_cnv_match, 'doc_str': f'The total number of technical cnv genes is {technical_cnv_match}'}

    assert technical_cnv_match == 142 + plus, f'The total number of clinical cnv genes is {technical_cnv_match}'
    assert_returns['technical_cnv_match'] = {'num': technical_cnv_match, 'doc_str': f'The total number of technical cnv genes is {technical_cnv_match}'}

    technical_snv_cnv_match = len(set(technical_snv_match_list + technical_cnv_match_list))
    assert technical_snv_cnv_match == 159, f'The total number of technical snv + cnv genes is {technical_snv_cnv_match}'
    assert_returns['technical_snv_cnv_match'] = {'num': technical_snv_cnv_match, 'doc_str': f'The total number of technical snv + cnv genes is {technical_snv_cnv_match}'}

    assert technical_fusion_match == 100, f'The total number of technical fusion genes is {technical_fusion_match}'
    assert_returns['technical_fusion_match'] = {'num': technical_fusion_match, 'doc_str': f'The total number of technical fusion genes is {technical_fusion_match}'}

    # remove the clinical from technical and then make sure they are all True for technical and False for clinical
    for gene in (list(set(technical_list_cnv_gene) - set(clincial_list_cnv_gene))):
        raise
        assert final_map[gene]['cnv_meta_data']['is_technical'] is True, f"{gene} cnv is_technical is not True"

    # test the Fusions
    for gene in technical_list_fusion_gene:
        assert final_map[gene]['fusion_meta_data']['is_technical'] is True, f"{gene} fusion is_technical is not True"

    # test the control fusions
    for gene in control_list_fusion_gene:
        assert final_map[gene]['fusion_meta_data']['is_technical'] is False, f"{gene} fusion is_technical is not False"
        assert final_map[gene]['fusion_meta_data']['is_control'] is True, f"{gene} fusion is_control is not True"

    for gene in list(set(technical_list_snv_gene) - set(get_oc120plus_msi_list())):
        if gene.startswith('rs') or gene == 'SNP' or gene == 'bed':
            continue
        assert final_map[gene]['snv_meta_data']['is_technical'] is True, f"{gene} snv is_technical is not True"

    # go through and make sure the invalid HUGO-gene name are fixed:
    # MRE11A old -->  MRE11
    # H3F3A old -->  H3-3A
    # H3F3B old -->  H3-3B
    assert 'MRE11A' not in final_map, "MRE11A is still in final map, and should not be"
    assert 'H3F3A' not in final_map, "H3F3A is still in final map, and should not be"
    assert 'H3F3B' not in final_map, "H3F3A is still in final map, and should not be"
    assert 'MRE11' in final_map, "MRE11 is in final map"
    assert 'H3-3A' in final_map, "H3-3A is in final map"
    assert 'H3-3B' in final_map, "H3-3A is in final map"

    # go over gene lists
    for gene in get_oc120plus_gene_list_for_variant_plex():
        assert gene in final_map, f"{gene} is not in final_map, and should be"

    for gene in get_oc120plus_new_gene_list_to_keep_for_gs_v2():
        assert gene in final_map, f"{gene} is not in final_map, and should be"

    for gene in final_map.keys():
        if final_map[gene]['cnv_meta_data']['is_technical'] is True or \
                final_map[gene]['fusion_meta_data']['is_technical'] is True or \
                final_map[gene]['snv_meta_data']['is_technical'] is True or (
                final_map[gene]['cnv_meta_data']['num_primers'] is not None and
                final_map[gene]['cnv_meta_data']['num_primers'] >= 1) or \
                final_map[gene]['fusion_meta_data']['is_control'] is True:
            pass
        else:
            pass
    # this list of genes was found by the above for loop, and verifed to be hotspots or SNPs
    for gene, type in {"LY9": 'SNP', "ATP13A4": 'SNP', "PALLD": 'SNP', "SPOCK1": 'SNP', "BYSL": 'HOT', "SYNE1": 'SNP',
                       "PTN": 'SNP', "NAPRT": 'SNP', "HSPA12A": 'SNP', "TRIM22": 'SNP', "UBAC2": 'SNP', "RAB31": 'SNP',
                       "CD79A": 'HOT', "LARGE1": 'SNP', "AMELX": 'SNP', "DMD": 'SNP', "SYN1": 'SNP', "DACH2": 'SNP'}\
            .items():
        assert final_map[gene]['snv_meta_data']['is_technical'] is False, f"{gene} snp is_technical is False"
        if type == 'SNP':
            assert len(final_map[gene]['snv_meta_data']['snps']) >= 1, f"{gene} snp is >= 1"
        else:
            assert len(final_map[gene]['snv_meta_data']['hotspots']) >= 1, f"{gene} snp is >= 1"
    print("done", file=sys.stderr)

    return assert_returns


if __name__ == "__main__":
    main()