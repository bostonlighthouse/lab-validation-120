"""Module to create the JSON gene mapping file for the HRR assay"""
import argparse
import logging
import os
import sys
import pandas as pd
from tools.gene_maps.gene_map_utils import get_init_gene_data_structure, print_gene_map_json, \
    get_json_assertions_for_gene_map, get_ensembl_overlap_api_data
from tools.ensembl.rest.ensembl_rest_client import EnsemblRestClient
from typing import Tuple


logging.basicConfig(level=logging.DEBUG)
# pylint: disable=logging-fstring-interpolation


def main() -> None:
    """
    Business logic
    :return: NoneType
    """
    args = get_cli_args()
    assay_name = args.assay_name
    gene_mapping_df = pd.read_csv(args.gene_mapping_file, sep='\t')
    # can skip this step for speed (development purposes)
    if args.excel_file is None:
        final_excel_file = f'final_hrr_bed_and_exon_for_{assay_name}.xlsx'
        bed_file_exon_df, gene_mapping_df \
            = process_bed_file(bed_file=args.bed_file, gene_mapping_df=gene_mapping_df, assay_name=assay_name)
        bed_file_exon_df.to_excel(final_excel_file)
    else:
        final_excel_file = args.excel_file
        if not os.path.exists(final_excel_file):
            print(f"\n\nExcel file '{final_excel_file}' does not exist.\nRun program w/o --excel_file option\n\n")
            sys.exit()
        bed_file_exon_df = pd.read_excel(final_excel_file)

    if assay_name == 'ocGermline':
        # the BED file has the gene name as MRE11A, but that HUGO gene name is MRE11
        # https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/HGNC:7230
        bed_file_exon_df.loc[bed_file_exon_df['gene'] == 'MRE11A', 'gene'] = 'MRE11'
        gene_mapping_df.loc[gene_mapping_df['gene'] == 'MRE11A', 'gene'] = 'MRE11'

    # convert the df to the gene map datastructure
    final_map = df_to_gene_map_ds(df=bed_file_exon_df, assay_name=assay_name)

    # add in the gene map validation key and values
    num_genes_in_map = len(final_map.keys())  # number of genes
    # ocHRR has 0 gene for CNV, but all genes on ocGermline assay are technical CNV
    cnv_technical_true = 0
    if assay_name == 'ocGermline':
        cnv_technical_true = num_genes_in_map
    assertions = get_json_assertions_for_gene_map(num_genes_in_map=num_genes_in_map,
                                                  at_least_one_technical_match=num_genes_in_map,
                                                  technical_snv_match=num_genes_in_map,
                                                  technical_cnv_match=cnv_technical_true,
                                                  technical_snv_cnv_match=num_genes_in_map,
                                                  technical_fusion_match=0)
    # print in JSON format
    print_gene_map_json(version=args.json_file_name_version, assay_name=assay_name,
                        final_map=final_map, assertions=assertions)

    print_bed_file_for_htqc_bed(hg19_df = bed_file_exon_df, file_name = args.bed_file)

def print_bed_file_for_htqc_bed(
        hg19_df: pd.DataFrame = None, file_name: str = None) -> None:
    """
    Print out the bed file format for HTQC that will fill the QC tab correctly

    :param hg19_df: df with the exon level information for each intervals
    :param file_name: use this for the new file name.
    :return: NoneType
    """
    file_name = '_'.join((os.path.splitext(file_name)[0], 'annotated.bed'))
    with(open(file_name, "w", encoding='utf-8')) as out_fh:
        for _, row in hg19_df.iterrows():
            # get some values from the series
            gene = row['gene']
            chrom = row['chrom']
            start = row['start']
            end = row['end']
            exon = row['rank']
            strand = row['strand']
            ref_seq = row['ref_seq_from_input']

            if exon >= 1:
                bed_string = f"{gene};{ref_seq}_Exon_{exon}"  # the annotation column
                # print out the BED file
                print("\t".join((chrom.replace('chr', ''), str(start), str(end), bed_string, '1')), file=out_fh)
            else:
                print("\t".join((chrom.replace('chr', ''), str(start), str(end), f'{gene};NOT_IN_CANONICAL_EXON', '1')),
                      file=out_fh)


def df_to_gene_map_ds(df: pd.DataFrame, assay_name: str=None) -> dict:
    """
    Take the data frame passed in and generate the gene map ds
    :param df: data frame required with transcript and exon information
    :return: dictionary of the gene map
    """

    # The ds returned by get_init_data_structure:
    # {
    #  'cnv_meta_data': {
    #                       'is_control': False,
    #                       'is_technical': False,
    #                       'num_primers': 0
    #                    },
    # 'fusion_meta_data': {
    #                       'is_control': False,
    #                       'is_technical': False
    #                     },
    # 'preferred_ensembl_transcript': None,
    # 'refseq_transcripts': {
    #                           'fusions': {},
    #                           'snvs': {}
    #                        },
    # 'snv_meta_data': {
    #                   'hotspots': [],
    #                   'intervals': [],
    #                   'is_technical': False,
    #                   'snps': []
    #                   }
    # }

    df = df[df['rank'] != -1]  # remove the -1 ranks, b/c these were where no exon was found for the given transcript

    total_ds = {}
    # go over all genes in the dataframe
    for gene in df['gene'].unique():
        gene_df = df[df['gene'] == gene]
        chromosome = str(gene_df['chrom'].iloc[0])
        intervals = []

        #ref_seq_transcript = gene_df['ref_seq_from_input'].iloc[0]  # all the same ref_seq ID here..
        ref_seq_transcripts = gene_df['ref_seq_from_input'].to_list()
        #preferred_ensembl_transcript = gene_df['ensembl'].iloc[0]  # all the same transcript ID here..
        preferred_ensembl_transcripts = gene_df['ensembl'].to_list()  # all the same transcript ID here..

        gene_map_ds = get_init_gene_data_structure()  # get the initialized data structure

        # put these into the ref seq / preferred ensembl data structure
        transcripts = zip(ref_seq_transcripts, preferred_ensembl_transcripts)

        # go over each row in the gene df and store the intervals and exon information
        found_interval = {}  # since there could be two Transcripts for a gene, only store unique intervals
        # get each combo of ref_seq_transcript AND preferred_ensembl_transcript
        for ref_seq_transcript, preferred_ensembl_transcript in transcripts:
            exons = []  # exons are unique to a given transcript
            # get the df for a gene and ref_seq_transcript
            gene_ref_seq_transcript_df = gene_df[(gene_df['gene'] == gene) &
                                                 (gene_df['ref_seq_from_input'] == ref_seq_transcript)]
            # iterate over all rows for this transcript
            for _, row in gene_ref_seq_transcript_df.iterrows():
                key = f"{row['start']} {row['end']}"
                if key in found_interval:
                    pass
                else:
                    found_interval[key] = 1
                    intervals.append({'start': row['start'], 'stop': row['end']})
                exons.append(row['rank'])  # this will be set later on, so will be unique

            # you can have duplicates b/c multiple intervals could cover the same exon
            exons = sorted(list(set(exons)))
            # fill in the data structure for the transcript
            gene_map_ds['refseq_transcripts']['snvs'][ref_seq_transcript] = \
                {'exons': exons,
                 'ensembl_transcript': preferred_ensembl_transcript}

        # fill in the gene level
        gene_map_ds['snv_meta_data']['is_technical'] = True
        gene_map_ds['snv_meta_data']['intervals'] = intervals
        #gene_map_ds['preferred_ensembl_transcript'] = preferred_ensembl_transcript
        gene_map_ds['preferred_ensembl_transcript'] = gene_df['ensembl'].iloc[0]  # take the first since it's the overall
        gene_map_ds['chrom'] = chromosome
        # preferred transcript in the --gene_mapping_file.

        if assay_name == 'ocGermline':
            gene_map_ds['cnv_meta_data']['is_technical'] = True

        # add to the overall data structure
        total_ds[gene] = gene_map_ds

    return total_ds


def process_bed_file(bed_file: str = None, gene_mapping_df: pd.DataFrame = None, assay_name: str = None) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Take in the bed file and query Ensemble for gene and exon level information
    :param bed_file: Qiagen BED file data frame
    :param gene_mapping_df: Gene mapping data frame gene->refseq_ID->ensembl_ID
    :parms assay_name: Name of the type of assay
    :return: data frame with information required for the gene map
    """
    ensembl_client = EnsemblRestClient()  # used to download Ensembl data from REST endpoint
    bed_file_df = pd.read_csv(bed_file, sep="\t", names=get_bed_cols(), usecols=[0, 1, 2, 3])
    print(f"LENGTH BEFORE PANEL: {len(bed_file_df)}")
    # update the gene column to only have genes on the panel, e.g. get rid of examples like ths: LRRC41;RAD54L
    if assay_name == 'ocGermline':
        # remove b/c this is a gene only for a control on the Y chromosome, and not to be signed out
        bed_file_df = bed_file_df[bed_file_df['gene'] != 'XGPY2']
    elif assay_name == 'ocGermlineCoverageFile':
        # for the mapping only keep one CDKN2A
        gene_mapping_df = gene_mapping_df[gene_mapping_df['ref_seq_from_input'] != 'NM_058195.4']
        # Data to add as a new row
        # technically XGPY2 is
        new_data = pd.DataFrame([{"gene": "SRY", "ref_seq_from_input": "NM_003140.2", "ref_seq": "NM_003140.2",
                                  "ensembl": "ENST00000383070.1"}])
        # the bed file has XGPY2 in it, but this was incorrect, the interval was for SRY
        bed_file_df.loc[bed_file_df['gene'] == 'XGPY2', 'gene'] = 'SRY'
        # Append the new row to the DataFrame
        gene_mapping_df = pd.concat([gene_mapping_df, new_data], ignore_index=True)

    all_genes_on_panel = gene_mapping_df['gene'].to_list()

    bed_file_df['gene'] = \
        bed_file_df.apply(lambda row: _update_gene_col(row=row,
                                                       gene_mapping_list=all_genes_on_panel), axis=1)

    set_all_genes_on_panel = set(all_genes_on_panel)
    set_all_genes_on_bed = set(bed_file_df['gene'].unique())
    # test to make sure the gene sets are the same b/t the gene mapping input and the BED file
    assert set_all_genes_on_panel == set_all_genes_on_bed, "Genes in gene mapping do not match BED genes"

    # merge the data
    bed_file_df = bed_file_df.merge(gene_mapping_df, right_on='gene', left_on='gene')
    # have a column with no version number ENST00000371975.4 = ENST00000371975
    bed_file_df['ensembl_no_version'] = bed_file_df.apply(lambda row: row['ensembl'].split(".")[0], axis=1)
    bed_file_df['ensembl_exon_obj'] = \
        bed_file_df.apply(lambda row: _get_exon_data(row=row,
                                                     ensembl_client=ensembl_client), axis=1)
    print(f"LENGTH AFTER PANEL: {len(bed_file_df)}")
    # create a dataframe of unique columns since it was an dictionary
    exon_df = pd.DataFrame(bed_file_df.pop('ensembl_exon_obj').values.tolist())
    # test to make sure they were the same size
    assert len(bed_file_df) == len(exon_df), "bed and exon dataframes are not the same size"
    # join them and update the start and end in teh exon_df
    bed_file_df = bed_file_df.join(exon_df, rsuffix='_exon')

    # did the BED interval on the 5' end start before the start_exon, do not need to consider strand here ...
    bed_file_df['start_delta'] = bed_file_df['start'] - bed_file_df['start_exon'] + 1

    # True here means the BED interval includes the start of the exon
    # pylint: disable=simplifiable-if-expression
    bed_file_df['exon_5_prime_inclusive'] = \
        bed_file_df.apply(lambda row: True if row['start_delta'] < 0 else False, axis=1)

    # Did the BED interval on the 3' end end after the start_exon, do not need to consider strand here ...
    bed_file_df['end_delta'] = bed_file_df['end'] - bed_file_df['end_exon'] + 1
    # True here means the BED interval includes the end of the exon
    bed_file_df['exon_3_prime_inclusive'] = \
        bed_file_df.apply(lambda row: True if row['end_delta'] > 0 else False, axis=1)  # pylint: disable=R1719

    # revert to pd.NA when there was no exon i.e. rank was -1
    for value in ['end_exon', 'start_exon', 'start_delta', 'exon_5_prime_inclusive', 'end_delta',
                  'exon_3_prime_inclusive']:
        bed_file_df[value] = bed_file_df.apply(lambda row, val=value: pd.NA if row['rank'] == -1 else row[val], axis=1)
    # pylint: enable=simplifiable-if-expression

    # the only False for exon_5_prime_inclusive or exon_3_prime_inclusive above were in the first or last exon of the
    # transcript, where the UTR begins or end upstream or downstream of the BED interval (manual inspection)
    return bed_file_df, gene_mapping_df


def _get_exon_data(row: pd.Series = None, ensembl_client: EnsemblRestClient = None) -> dict:
    """
    Query Ensembl and get the gene and exon level information
    :param row: Series from Pandas
    :param ensembl_client: Instance of the EnsemblRestClient
    :return: Dictionary of either information, or an empty dictionary b/c the interval did not map to anything
    """
    #endpoint = f"/overlap/region/human/{row['chrom']}:{row['start']}-{row['end']}"  # ensembl endpoint
    #endpoint = '?'.join((endpoint, 'feature=gene;feature=exon'))  # add in some features to the query
    row_gene = row['gene']  # gene from this row
    #logging.debug(f"NEW EXON ENDPOINT QUERY: {endpoint}")
    #logging.debug(row)
    # call on custom class to get a list of the ensembl transcripts
    #ensembl_objects = ensembl_client.perform_rest_action(endpoint=endpoint)
    ensembl_objects = get_ensembl_overlap_api_data(row=row, ensembl_client=ensembl_client)

    # could have multiple genes
    # https://grch37.rest.ensembl.org/overlap/region/human/2:58386889-58386950?feature=gene;feature=exon;content-type=application/json
    gene_feature_match = {}  # store the gene object that matches the gene of the mapping passed in
    all_gene_features = []  # keep a list of genes that overlap for debugging
    gene_founds = set()  # store the genes that were found here for debugging
    # check gene information matches our gene name in the mapping file passed in at the CLI
    for ensembl_obj in ensembl_objects:
        if ensembl_obj['feature_type'] == 'gene':  # gene information
            logging.debug(f"ENSEMBL\n{ensembl_obj}")
            gene_founds.add(ensembl_obj['external_name'])
            if row_gene == ensembl_obj['external_name']:  # gene matched the mapping
                gene_feature_match = ensembl_obj
            all_gene_features.append(ensembl_obj)  # store all genes

    # should not happen unless there's an incorrect mapping in the gene_mapping_df found in main() OR
    # there something outside a gene like the TERT promoter region which doesn't fall into the gene
    # When this happens the call to the API returns an empty list for ensembl_objects above...
    if not ensembl_objects:
        logging.warning(f"No data found for row\n{row}")
        return empty_exon_dict(gene_founds=gene_founds)
    # here we did not have a match to the gene name in the mapping file passed in at the CLI
    if not gene_feature_match:
        logging.warning(f"Genes do not match ours {row_gene} theirs {all_gene_features}")
        return empty_exon_dict(gene_founds=gene_founds)

    # store the exon level information for the ensembl transcript
    exons = []
    for ensembl_obj in ensembl_objects:
        if 'Parent' not in ensembl_obj:  # only care about entries that have a parent gene object
            continue
        if ensembl_obj['Parent'] == row['ensembl_no_version']:  # does the Parent match our gene transcript
            exons.append(ensembl_obj)
            logging.debug(ensembl_obj)

    # should not be more than one exon entry for a given transcript, but it can happen:
    # example where this can happen: chrom 8 145738725 145738777 RECQL4 b/c the overlap covered both exon regions
    if len(exons) > 1:
        logging.warning(f"There was more than 1 exon, should not happen often, so check it out...\n{exons}")
    elif len(exons) == 1:  # here we had just one match of exon information to the transcripte
        logging.debug(f"Normal\n{exons}")
    else:
        logging.debug("No exons found")

    if len(exons) > 0:  # return the first exon
        exons[0]['number_exons'] = len(exons)  # store number of exon found to analyze later
        exons[0]['gene_founds'] = gene_founds  # store the genes found at this position
        return exons[0]

    # Did not overlap an exon entry.
    # Could be the BED interval did not overlap the mapped transcript so produce a blank entry for this exon
    logging.warning(f"Did not overlap an exon entry....\n{row}")
    return empty_exon_dict(gene_founds=gene_founds)


def empty_exon_dict(gene_founds: set) -> dict:
    """
    If there was no exon, this information will be pd.NA or -1.  These keys are what's returned for an ensembl exon
    overlap object returned by the endpoint
    :return:
    """
    return {
        'exon_id': pd.NA,
        'rank': -1,  # always keep this, since it means there was no exon at that interval for the mapped transcript
        'assembly_name': pd.NA,
        'source': pd.NA,
        'feature_type': pd.NA,
        'ensembl_phase': pd.NA,
        'strand': pd.NA,
        'start': -1,  # store these b/c we'll do arithmetic later on using an apply, and pd.NA will not work
        'seq_region_name': pd.NA,
        'constitutive': pd.NA,
        'end': -1,  # store these b/c we'll do arithmetic later on using an apply, and pd.NA will not work
        'ensembl_end_phase': pd.NA,
        'Parent': pd.NA,
        'id': pd.NA,
        'version': pd.NA,
        'number_exons': pd.NA,
        'gene_founds': gene_founds if gene_founds else pd.NA
    }


def _update_gene_col(row: pd.Series = None, gene_mapping_list: list = None) -> str:
    """
    Some rows have genes like: LRRC41;RAD54L
    Only keep the gene that matched in the gene_mapping_list
    :param row: pd.Series of data from the df
    :param gene_mapping_list: genes that are in the mapping
    :return: the gene name
    """
    return_genes = []
    if ';' in row['gene']:
        genes = row['gene'].split(";")  # e.g. LRRC41;RAD54L
        for gene in genes:
            if gene in gene_mapping_list:
                return_genes.append(gene)
    else:
        return_genes.append(row['gene'])

    if row['gene'] not in gene_mapping_list:
        logging.warning(f"Strange row: gene {row['gene']}, but asserting below so we are good..")
    # should only be one that matches
    assert len(return_genes) == 1, "Size is not equal 1 for the genes"

    return return_genes[0]


def get_bed_cols() -> list:
    """
    Column names for the BED file
    :return: list of column names
    """
    return [
        'chrom',
        'start',
        'end',
        'gene',
    ]


def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Create the gene map json for the HRR assay')

    parser.add_argument('--bed_file',
                        dest='bed_file',
                        type=str,
                        help='BED file',
                        required=True)

    parser.add_argument('--gene_mapping_file',
                        dest='gene_mapping_file',
                        type=str,
                        help='Gene mapping file for the HRR assay',
                        required=True)

    parser.add_argument('--excel_file',
                        dest='excel_file',
                        type=str,
                        help="If the program has been run before you can give it this excel file to produce the"
                             "required pandas dataframe",
                        required=False)

    parser.add_argument('--assay_name',
                        dest='assay_name',
                        type=str,
                        help='The assay name',
                        required=True,
                        choices=['ocHRR', 'ocGermline', 'ocFocus', 'ocGermlineCoverageFile'])

    parser.add_argument('--json_file_name_version',
                        dest='json_file_name_version',
                        type=int,
                        help='Version to print out... this will be added to final JSON file',
                        required=True)

    return parser.parse_args()


if __name__ == '__main__':
    main()
