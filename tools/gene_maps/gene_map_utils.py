import argparse
import json
import logging
import os
from datetime import date

import pandas as pd

from lab_validation_120.utils.configs.lab_validation_config import get_gene_to_esemble_mapping_from_vv
from lab_validation_120.utils.eutils import get_annotation_from_dbsnp_id
from tools.ensembl.rest.ensembl_rest_client import EnsemblRestClient


def get_gene_2_transcript_2_exon_map_for_snvs_from_bed_file(assay_name=None, callable_function=None, skip_first_row=False):
    """
    Open up the file BED file from Archer, and return the map

    Return a diction of dictionary with each entry like:

    GENE1: {
            NM_ABC: {
                    exons: [1, 2, 3, 4, 5],
                }
            NM_XYZ: {
                    exons: [5, 6, 7],
                }
            }
    }
    """
    gene_2_exon_map = {}
    #print(callable_function())
    with open(callable_function()) as infh:
        for line in infh:
            if 'Exon' not in line and'exon' not in line:
                continue
            # skip this entry: g.139390145T>C;c.*7668+378A>G;g.139390152T>C;c.*7668+371A>G;NM_017617.4_Exon_34'
            if assay_name == 'oc395' and 'g.139390145T>C;' in line:
                continue
            # 1       11186676        11186855        MTOR;NM_004958.3_Exon_46        0       -
            # 1       11204702        11204814        MTOR;NM_004958.3_Exon_34,NR_046600.1_Exon_2     0
            # 1       36748162        36748303        THRAP3;NM_001321471.1_Exon_4,NM_005119.3_Exon_3,NM_001321473.1_Exon_3   0       +
            values = line.rstrip().split("\t")
            #print(values)
            gene, transcript_exon_info = values[3].split(';')  # get the gene name and the rest
            # remember, multiple entires on the same line for some transcripts
            transcript_exon_vals = transcript_exon_info.split(',')
            # loop over all of them and split the transcript to the exon out
            for transcript_exon in transcript_exon_vals:
                # NM_001321471.1_Exon_4
                list_vals = transcript_exon.split("_")
                #print(list_vals)
                #assert len(list_vals) == 4, f"Elements are not 4 , something went wrong with this entry {transcript_exon}"
                ncbi_type, transcript, _, exon = list_vals[0], list_vals[1], list_vals[2], list_vals[3],
                exon = int(exon)
                if '.' in transcript:
                    transcript, transcript_version = transcript.split('.') # e.g. NM_001321471.1
                transcript = ncbi_type + '_' + transcript  # concatenate them back together
                if gene in gene_2_exon_map:
                    if transcript in gene_2_exon_map[gene]:
                        # there's already a transcript with a list, so concatenate the new list
                        gene_2_exon_map[gene][transcript]['exons'] += [exon]
                        # remove any duplicate exons by using set, and turn back to a list
                        gene_2_exon_map[gene][transcript]['exons'] = list(
                            set(gene_2_exon_map[gene][transcript]['exons']))
                    else:
                        # this must be a different transcript, so just create a new entry in the dict of dict
                        gene_2_exon_map[gene][transcript] = {'exons': [exon]}
                else:
                    # initialize it
                    gene_2_exon_map[gene] = {transcript: {'exons': [exon]}}

    # just sort them if needed
    for gene, dod_values in gene_2_exon_map.items():
        for transcript in dod_values:
            #print(gene, gene_2_exon_map[gene])
            gene_2_exon_map[gene][transcript]['exons'] = sorted(gene_2_exon_map[gene][transcript]['exons'])
    return gene_2_exon_map

def get_gene_2_transcript_2_exon_map_for_snvs(assay_name=None, callable_function=None, skip_first_row=False):
    """
    Open up the Exdel file from Archer, and return the map
    """

    """
     GENE1: {
            NM_ABC: {
                    exons: [1, 2, 3, 4, 5],
                }
            NM_XYZ: {
                    exons: [5, 6, 7],
                }
            }
    }
    """
    # go off and get the excel spreadsheet sent to us by Archer
    excel_file, sheet_name = callable_function(assay_name=assay_name)
    if skip_first_row is True:
        df = pd.read_excel(excel_file, sheet_name=sheet_name, usecols="A,B,C", skiprows=[0])
    else:
        df = pd.read_excel(excel_file, sheet_name=sheet_name, usecols="A,B,C")

    df = df.dropna(how="all")
    gene_2_exon_map = {}
    # store a dictionary of dicts with transcripts mapped ot exons
    for gene, transcript, exon_str in zip(df.Gene, df.Accession, df.Exon):
        exon_str = str(exon_str)
        if gene in gene_2_exon_map:
            # ATM and TP53 have multiple transcript entries
            if transcript in gene_2_exon_map[gene]:
                # there's already a transcript with a list, so concatenate the new list
                gene_2_exon_map[gene][transcript]['exons'] += [int(x) for x in exon_str.split(",")]
                # remove any duplicate exons by using set, and turn back to a list
                gene_2_exon_map[gene][transcript]['exons'] = list(set(gene_2_exon_map[gene][transcript]['exons']))
            else:
                # this must be a different transcript, so just
                gene_2_exon_map[gene][transcript] = {'exons': [int(x) for x in exon_str.split(",")]}
        else:
            # initialize it
            gene_2_exon_map[gene] = {transcript: {'exons': [int(x) for x in exon_str.split(",")]}}
        # sort the data
        gene_2_exon_map[gene][transcript]['exons'].sort()

    return gene_2_exon_map


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Create the intervals file for oc120Plus, oc120plus_snps, or oc395')

    parser.add_argument('--no_match_file', dest='no_match_file',
                        type=str, help='Output genes that did not match, but will still be printed to interval file',
                        required=True)
    parser.add_argument('--not_in_gs_2', dest='not_in_gs_2',
                        type=str, help='Output genes that matched but are not in OC102Plus (aka GS v2.0)',
                        required=True)
    parser.add_argument('--json_file_name_version', dest='json_file_name_version',
                        type=int, help='Version to print out... this will be added to final JSON file',
                        required=True)
    parser.add_argument('--bed_file_2_print', dest='bed_file_2_print',
                        type=str, help='Output the subset bed file',
                        required=True)
    parser.add_argument('--assay_name', dest='assay_name',
                        type=str, help='The assay version',
                        required=True,
                        #choices=['oc120plus', 'oc120plus_snps', 'oc395'])
                        choices=['oc120plus_snps', 'oc395'])


    return parser.parse_args()


def update_with_ensembl_transcripts_from_vv(final_map=None):
    """Get the ensembl mapping and map that to the final_map
    :param final_map: The final data structure that will be printed out in JSON format
    """

    ensembl_mapping = get_gene_to_esemble_mapping_from_vv()
    # if it was in the ensembl map, add it to the final_map
    for gene in final_map:
        if gene in ensembl_mapping:
            final_map[gene]['preferred_ensembl_transcript'] = ensembl_mapping[gene]


def mutation_genes_listed_in_fusion_excel():
    # these genes are only listed as Mutation and should not be in the final list
    return ('CTNNB1', 'HRAS', 'IDH1', 'IDH2', 'KRAS', 'MAP2K1', 'MYOD1', 'NRAS')


def update_with_fusion_genes(final_map=None, callable_function=None):
    """Add in the fusion data from the fusion GTF file
    :param final_map: The final data structure that will be printed out in JSON format
    """
    fusion_list = callable_function()
    technical_list_fusion_gene = []
    clincial_list_fusion_gene = []
    control_list_fusion_gene = []
    # go over all fusions from the genes in the GTF
    for gene, annotation_dict in fusion_list:
        # skip these genes b/c they only suppose to be mutation genes
        if gene in mutation_genes_listed_in_fusion_excel():
            continue
        # if the gene is in the final map, just add it to the fusion_meta_data key
        if gene not in final_map:
            final_map[gene] = get_init_gene_data_structure()  # init the key

        # was this a control gene for fusions, e.g.
        # 'gene_id "GPI_chr19_34890194_20_+_A1_GSP2"', ' transcript_id ""', ' function "CONTROL"'
        if annotation_dict['function'] == 'CONTROL':
            final_map[gene]['fusion_meta_data']['is_control'] = True
            control_list_fusion_gene.append(gene)
        elif 'FUSION' in annotation_dict['function']:
            #final_map[gene]['fusion_meta_data']['is_clinical'] = True  # all genes in fusion are ok on the PDF report
            final_map[gene]['fusion_meta_data']['is_technical'] = True  # all genes in fusion are technical
            technical_list_fusion_gene.append(gene)
            clincial_list_fusion_gene.append(gene)
        else:
            raise ValueError(f"Have not seen this function in the fusion gtf {annotation_dict['function']}")

    # update the exons
        if 'exon_number' in annotation_dict and 'transcript_id' in annotation_dict:
            transcript_id = annotation_dict['transcript_id']
            if transcript_id in final_map[gene]['refseq_transcripts']['fusions']:
                final_map[gene]['refseq_transcripts']['fusions'][transcript_id]['exons'].append(int(annotation_dict['exon_number']))
                # some genes like BRAF have multiple primers to the same exon (7) and transcript NM_004333
                # so set them and then list and sorted, must use sorted here b/c we're doing assignment
                final_map[gene]['refseq_transcripts']['fusions'][transcript_id]['exons'] = \
                    sorted(list(set(final_map[gene]['refseq_transcripts']['fusions'][transcript_id]['exons'])))
            else:
                final_map[gene]['refseq_transcripts']['fusions'][transcript_id] = {'exons': [int(annotation_dict['exon_number'])]}

    return technical_list_fusion_gene, clincial_list_fusion_gene, control_list_fusion_gene


def get_init_gene_data_structure():
    """Return the data structure for each gene in the final map"""
    return {
        'cnv_meta_data':  {
            #'is_clinical': False,
            'is_technical': False,
            'is_control': False,
            'num_primers': 0,
        },
        'fusion_meta_data': {
            #'is_clinical': False,
            'is_technical': False,
            'is_control': False,
        },
        'snv_meta_data': {
            #'is_clinical': False,
            'is_technical': False,
            'intervals': [],
            'hotspots': [],
            'snps': [],
        },
        'refseq_transcripts': {
            'snvs': {},
            'fusions': {},
        },
        #'refseq_transcripts_from_bed': {
        #    'snvs': {},
        #},
        'preferred_ensembl_transcript': None,
        'chrom': None
    }


def update_with_snv_genes(assay_name=None, values=None, gene_name=None, final_map=None,
                          is_snv_match=None,
                          gene_2_exon_map=None,
                          gene_2_exon_map_from_bed=None,
                          clinical_list_snv_gene=None, technical_list_snv_gene=None):
    """
    Update the final map for VV
    :param values: The line was split on tab into values
    :param gene_name: The gene name for what was sent it, this could bd an rs number or other as well...
    :param final_map: The final mapping of genes to the values needed for VV, see top of module for more information
    :param is_snv_match:  Just a boolean to state whether it was a match or not to GS v2.0 genes
    :param gene_2_exon_map: Gene 2 exon map returned by get_gene_2_transcript_2_exon_map_for_snvs, pass in so we dont have to call many times
    """
    rs_number = None
    if gene_name.startswith("rs"):
        gene_name, rs_number = gene_name_from_rs_number(rs_number=gene_name, values=values)
        # if not gene name came back then rs is skipped, also skipping adding in genes like: LOC105373422
        if gene_name is None or gene_name.startswith('LOC'):
            return
    # These can be ignored can be ignored, and will end up in the intervals file, just not the map
    # bed;target;0;chr6:117631374-117631375,
    # SNP_ID_X_chr
    elif gene_name in ('bed', 'SNP'):  # do not proceed any further
        return

    start = values[1]
    stop = values[2]
    diff = stop - start
    hotspot = None
    if diff == 1:
        hotspot = {
            'start': start,
            'stop': stop,
        }

    _helper_update_with_snv_genes_oc395(gene_name=gene_name, final_map=final_map,
                                        clinical_list_snv_gene=clinical_list_snv_gene,
                                        technical_list_snv_gene=technical_list_snv_gene,
                                        gene_2_exon_map=gene_2_exon_map,
                                        gene_2_exon_map_from_bed=gene_2_exon_map_from_bed,
                                        is_snv_match=is_snv_match,
                                        rs_number=rs_number, hotspot=hotspot, start=start, stop=stop,
                                        values=values)


def _helper_update_with_snv_genes_oc395(gene_name=None, final_map=None, clinical_list_snv_gene=None, technical_list_snv_gene=None,
                                        gene_2_exon_map=None,
                                        gene_2_exon_map_from_bed=None,
                                        is_snv_match=None, rs_number=None, hotspot=None,
                                        start=None, stop=None, values=None):
    if gene_name in final_map:
        if rs_number:
            final_map[gene_name]['snv_meta_data']['snps'].append(rs_number)
        elif hotspot:
            final_map[gene_name]['snv_meta_data']['hotspots'].append(hotspot)
        final_map[gene_name]['snv_meta_data']['intervals'].append({'start': start, 'stop': stop})
    else:
        final_map[gene_name] = get_init_gene_data_structure()
        final_map[gene_name]['snv_meta_data']['intervals'] = [{'start': start, 'stop': stop}]
        # all snvs get mapped here once it gets here
        final_map[gene_name]['snv_meta_data']['hotspots'] = [hotspot] if hotspot and not rs_number else []
        final_map[gene_name]['snv_meta_data']['snps'] = [rs_number] if rs_number else []
        #final_map[gene_name]['refseq_transcripts']['snvs'] = gene_2_exon_map[gene_name] if gene_name in gene_2_exon_map else {}
        #final_map[gene_name]['refseq_transcripts_from_bed']['snvs'] = gene_2_exon_map_from_bed[gene_name] if gene_name in gene_2_exon_map_from_bed else {}
        final_map[gene_name]['refseq_transcripts']['snvs'] = gene_2_exon_map_from_bed[gene_name] if gene_name in gene_2_exon_map_from_bed else {}

        # right now if it's a SNP or just a hotspot it will get the SNV, INDEL

    if is_snv_match is True:
        #final_map[gene_name]['snv_meta_data']['is_clinical'] = True
        final_map[gene_name]['snv_meta_data']['is_technical'] = True
        clinical_list_snv_gene.append(gene_name)
    else: # only is_technical
        if hotspot is None and rs_number is None:
            final_map[gene_name]['snv_meta_data']['is_technical'] = True
            technical_list_snv_gene.append(gene_name)


def gene_name_from_rs_number(rs_number=None, values=None):
    """
    Go off and get the gene name from the rs_number
    :param rs_number: What was rs_number number passed in to query, or get the file
    :param values: The values from the interval line, split into a list, where I'll further split
    """
    # query eutils to pull this information
    snp_id, eutils = get_annotation_from_dbsnp_id(rs_number=rs_number)
    gene_name = None
    rs_number_dict = {}  # what to return
    if eutils and len(eutils["result"][snp_id]["genes"]) > 0 and len(eutils["result"][snp_id]["global_mafs"]) > 0:
        gene_name = eutils["result"][snp_id]["genes"][0]['name']
        # like: spdi": "NC_000002.12:9945592:G:A,NC_000002.12:9945592:G:C",
        rs_number_ref_allele = eutils["result"][snp_id]["spdi"].split(',')[0].split(":")[2]
        rs_number_alt_allele = eutils["result"][snp_id]["spdi"].split(',')[0].split(":")[3]
        # like: freq": "G=0.49838/78766"
        rs_number_alt_allele_freq = float(eutils["result"][snp_id]["global_mafs"][0]["freq"].split('=')[1].split("/")[0])
        freq_allele = eutils["result"][snp_id]["global_mafs"][0]["freq"].split('=')[0]
        # swap if the freq_allele is not the alt
        if freq_allele == rs_number_ref_allele:
            #print(f'Not matching:  freq_allele = {freq_allele} rs_number_alt_allele {rs_number_alt_allele} rs_number_ref_allele {rs_number_ref_allele} spid {eutils["result"][snp_id]["spdi"]}')
            rs_number_alt_allele_freq = 1 - rs_number_alt_allele_freq
        if freq_allele != rs_number_ref_allele:  # it's not the ref so use this...
            rs_number_alt_allele = freq_allele

        # init what will be returned
        rs_number_dict = {
            'start': values[1],
            'stop': values[2],
            'ref_allele': rs_number_ref_allele,
            'alt_allele': rs_number_alt_allele,
            'alt_allele_freq': round(rs_number_alt_allele_freq, 6),
            'rs_number': rs_number
        }
    #print(rs_number_dict)
    return gene_name, rs_number_dict


def update_with_cnv_genes(final_map=None, assay_name=None, callable_function=None):
    """
    Open the the file called by callable_function and covert it to a dictionary and return it
    :param final_map: The final data structure that will be printed out in JSON format
    :param assay_name:  Assay type passed in, if needed
    :param callable_function: The function passed that will return cnv dictionary below
    """
    # open the file called by  callable_function and get the data and return a ds like documented below
    cnv_dict = callable_function(assay_name=assay_name)
    # update the final_map
    #     gene1: {
    #           "cnv_meta_data": {
    #           "is_clinical": false,
    #           "is_technical": true,
    #           "is_control": false,
    #           "num_primers": 58
    #},
    technical_list_cnv_gene = []
    clincial_list_cnv_gene = []
    for gene, dict_ in cnv_dict.items():
        if gene not in final_map:
            final_map[gene] = get_init_gene_data_structure()  # init the key
        # test values in the spreadsheet that was passed in and turned into a dictionary
        if cnv_dict[gene]['num_primers'] >= 3:  # >= 3 or more primers and Archer said we could use for CNV analysis
            final_map[gene]['cnv_meta_data']['is_technical'] = True
            #final_map[gene]['cnv_meta_data']['is_clinical'] = True
            technical_list_cnv_gene.append(gene)
            clincial_list_cnv_gene.append(gene)
        final_map[gene]['cnv_meta_data']['num_primers'] = dict_['num_primers']
    return technical_list_cnv_gene, clincial_list_cnv_gene


def print_gene_map_json(assay_name: str = None, final_map: dict = None, assertions: dict = None,
                        version: int = None) -> None:
    """
    Simple function to create the final JSON output that will be passed along to the Report App team
    @param assay_name:  Assay name, checked at the argparse level
    @param final_map: The final map of all gene level data
    @param assertions: The assertions that will be updated to the final_map
    @param version: An int for the version of the JSON file
    """
    file_name = None
    todays_date = date.today()
    # update with the version and date
    ending = f"{version}-{todays_date.year:02d}{todays_date.month:02d}{todays_date.day:02d}.json"
    # what assay was it?
    if assay_name == 'oc395':
        file_name = f"gs395-gene-list-v{ending}"
    elif assay_name == 'oc120plus_snps':
        file_name = f"gs-v2.0-gene-list-v{ending}"
    elif assay_name == 'ocHRR':
        file_name = f"gsHRR-gene-list-v{ending}"
    elif assay_name == 'ocGermline':
        file_name = f"gsGermline-gene-list-v{ending}"
    elif assay_name == 'oc700':
        file_name = f"gs700-gene-list-v{ending}"
    elif assay_name == 'ocFocus':
        file_name = f"gsFocus-gene-list-v{ending}"
    elif assay_name == 'ocGermlineCoverageFile':
        file_name = f"ocGermlineCoverageFile-gene-list-v{ending}"
    else:
        raise ValueError(f"Unknown assay_name {assay_name}")
    # add in the values for the final JSON
    final_map = {
        'version': f"v{version}.0.0",
        'data': final_map,
        'validation': assertions
    }
    # finally, print out the JSON for the new genes, this is 90 shared between GS v1.0 and GS v2.0 + 15 from Rodrigo
    json_formatted_str = json.dumps(final_map, indent=2)
    with open(file_name, 'w') as json_out:
        print(json_formatted_str, file=json_out)


def get_json_assertions_for_gene_map(num_genes_in_map: int = None,
                                     at_least_one_technical_match: int = None,
                                     technical_snv_match: int = None,
                                     technical_cnv_match: int = None,
                                     technical_snv_cnv_match: int = None,
                                     technical_fusion_match: int = None,
                                     ) -> dict:
    """
    Just add in the validation data for the data structure
    :param num_genes_in_map: Number of genes on the map
    :param at_least_one_technical_match: Number of genes with one technical match
    :param technical_snv_match: Number of SNVs that were technical
    :param technical_cnv_match: Number of CNVs that were technical
    :param technical_snv_cnv_match: Number of SNVs + CNVs that were technical
    :param technical_fusion_match: Number of Fusions that were technical
    :return:
    """

    json_assertions = {
        "num_genes_in_map": {
            "num": num_genes_in_map,
            "doc_str": f"The total number of genes is {num_genes_in_map}"
        },
        "at_least_one_technical_match": {
            "num": at_least_one_technical_match,
            "doc_str": f"The total number of technical genes is {at_least_one_technical_match}"
        },
        "technical_snv_match": {
            "num": technical_snv_match,
            "doc_str": f"The total number of technical snv genes is {technical_snv_match}"
        },
        "technical_cnv_match": {
            "num": technical_cnv_match,
            "doc_str": f"The total number of technical cnv genes is {technical_cnv_match}"
        },
        "technical_snv_cnv_match": {
            "num": technical_snv_cnv_match,
            "doc_str": f"The total number of technical snv + cnv genes is {technical_snv_cnv_match}"
        },
        "technical_fusion_match": {
            "num": technical_fusion_match,
            "doc_str": f"The total number of technical fusion genes is {technical_fusion_match}"
        }
    }
    return json_assertions


def get_ensembl_overlap_api_data(row: pd.Series = None, ensembl_client: EnsemblRestClient = None) -> list:
    """
    Get a stored file or Query Ensembl and get the gene, transcript, and  exon level information
    :param row: Series from Pandas
    :param ensembl_client: Instance of the EnsemblRestClient
    :return: Dictionary of either information, or an empty dictionary b/c the interval did not map to anything
    """
    ensembl_dir = get_storage_dir()
    json_output = os.path.join(ensembl_dir, f"{row['chrom']}_{row['start']}_{row['end']}.json")
    if os.path.exists(json_output):
        # logging.debug(f"Using existing JSON file: {json_output}")
        with(open(json_output, encoding='UTF-8')) as in_fh:  # noqa: E275
            ensembl_objects = json.load(in_fh)
    else:
        endpoint = f"/overlap/region/human/{row['chrom']}:{row['start']}-{row['end']}"  # ensembl endpoint
        # add in some features to the query
        endpoint = '?'.join((endpoint, 'feature=gene;feature=transcript;feature=exon'))
        logging.debug(f"Required Downloading: {endpoint}")
        # logging.debug(row)
        # call on custom class to get a list of the ensembl transcripts
        ensembl_objects = ensembl_client.perform_rest_action(endpoint=endpoint)
        # logging.debug(ensembl_objects)
        with(open(json_output, "w", encoding='UTF-8')) as out_fh:  # noqa: E275
            print(json.dumps(ensembl_objects, sort_keys=True, indent=4), file=out_fh)

    return ensembl_objects


def get_storage_dir():
    """
    Where to store files
    :return:
    """
    return "scratch/json/ensembl/overlap"
