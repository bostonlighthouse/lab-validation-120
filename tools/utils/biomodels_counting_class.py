from collections import Counter


class VarId:
    """Class for VAR_ID data structure """
    def __init__(self, var_id=None, depth=None, vaf=None, protein_change=None):
        self.var_id = var_id
        self.depth = depth
        self.vaf = vaf
        self.protein_change = protein_change

    def __str__(self):
        return f"{self.var_id} {self.vaf} {self.depth} {self.protein_change}"


class BiomodelsCountingClass:
    """Class to store types of data throughout the filtering process..."""
    def __init__(self):
        self.genes_list = []
        self.protein_impacts = []
        self.var_ids = {}

        self.total_variants_passed_filtering = 0
        self.total_variants_analyzed = 0
        self.total_samples_analyzed = 0  # how many total samples were looked at

    def get_protein_occurrences(self):
        return Counter(self.protein_impacts)

    def get_var_id_ojbs(self):
        return self.var_ids

    def update_variant_level_occurrences(self, variant_transcript_obj=None, variant_entry_obj=None, var_id=None,
                                         vaf=None, depth=None):
        self.genes_list.append(variant_transcript_obj.SYMBOL)
        protein_change = get_hgvsp_key_2_use(variant_entry_obj=variant_entry_obj,
                                                        variant_transcript_obj=variant_transcript_obj)
        self.protein_impacts.append(protein_change)
        var_id_obj = VarId(var_id=var_id, vaf=vaf, depth=depth, protein_change=protein_change)
        if var_id in self.var_ids:
            self.var_ids[var_id].append(var_id_obj)
        else:
            self.var_ids[var_id] = [var_id_obj]


def get_hgvsp_key_2_use(variant_transcript_obj=None, variant_entry_obj=None):
    """
    How to form the HGVSp when there is not protein
    :param variant_transcript_obj:  The variant transcript object from biomodels
    :param variant_entry_obj: The variant entry object from biomodels
    :return: String
    """
    # using SYMBOL and HGSVp here, this was used to show data in is_commonly_found_hsvp_variant_passed_filtering
    if variant_transcript_obj.HGVSp is not None:
        hgvsp_2_use = variant_transcript_obj.HGVSp
    elif variant_transcript_obj.HGVSc is not None:
        hgvsp_2_use = variant_transcript_obj.HGVSc
    else:
        hgvsp_2_use = f"g.{variant_entry_obj.CHROM}:{variant_entry_obj.vep_key}"

    return ":".join((str(variant_transcript_obj.SYMBOL), str(hgvsp_2_use)))


def get_dna_key_2_use(variant_transcript_obj=None, variant_entry_obj=None):
    """
    How to form the HGVSp when there is not protein
    :param variant_transcript_obj:  The variant transcript object from biomodels
    :param variant_entry_obj: The variant entry object from biomodels
    :return: String
    """
    return ':'.join((str(variant_transcript_obj.SYMBOL), variant_entry_obj.vep_key))