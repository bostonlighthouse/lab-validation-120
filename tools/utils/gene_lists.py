
def get_loh_genes():
    """
    Just a list of genes known to be important in LOH
    list came from
    https://aacrjournals.org/clincancerres/article/28/7/1412/682202/Pan-cancer-Analysis-of-Homologous-Recombination
    :return: list of genes
    """

    return ['ATR', 'ATRX', 'PALB2', 'RAD51B', 'RAD51C', 'BRIP1', 'RAD51D', 'ATM', 'CHEK1', 'CHEK2', 'BARD1',
            'BRCA1', 'BRCA2', 'BAP1', 'NBN', 'MRE11']

def get_hrd_genes():
    """
    Just a list of genes known to be important in HRD
    list came from Rodrigo and the "cohort summary" excel worksheet: "HR_gene_screen" where
    column "Total Alterations: loss, indels, sub somatic and germline (n)" > 2
    https://data.mendeley.com/datasets/2mn4ctdpxp/1
    lab_validation_120/external_data_sources/Whole-genome-sequencing-of-triple-negative-breast-cancers-a-population-study/cohort-summary/SupplementaryDataTable.xlsx
    :return: list of genes
    """

    #return ['BRCA1', 'BRCA2', 'CDKN2A', 'NEIL1', 'PALB2', 'POLN', 'PTEN', 'RAD51B', 'RAD51C', 'SDHD', 'TP53']
    return ['APEX1', 'CHEK2', 'DDB2', 'PMS2', 'RAD50', 'RMI1', 'SECISBP2', 'SIRT3', 'XRCC2', 'BAP1', 'OGG1', 'PIF1',
            'RECQL', 'RIF1', 'STK11', 'RAD51L1', 'FANCA', 'NBN', 'POLK', 'RECQL5', 'BRIP1', 'EXO1', 'FANCM', 'MUTYH',
            'POLQ', 'POLN', 'SDHD', 'PALB2', 'NEIL1', 'CDKN2A', 'BRCA2', 'BRCA1', 'PTEN', 'TP53']
