import subprocess
import sys
from typing import Tuple

def run_system_cmd_and_create_log_files(cmd: str, log: str, error_log: str) -> Tuple[str, str]:
    """
    Do not capture output, instead store in stderr and stdout error files
    :param cmd:  String of the command to run
    :param log: String for the standard output file to open
    :param error_log: String for the standard error file to open
    :return: Tuple of containing the standard out string and the standard error string
    """
    # If shell=True, the command string is interpreted as a raw shell command.
    # Using shell=True may expose you to code injection if you use user input to build the command string.
    # call can call a string, i.e. dont need a list
    #returned_output = subprocess.call(cmd, shell=True)
    # If you are on Python 3.5+, use subprocess.run() instead as it's safer.
    # create two files to hold the output and errors, respectively
    with open(log, 'w+') as fout:
        with open(error_log, 'w+') as ferr:
            # must be a list you pass into call
            returned_output = subprocess.call(list(cmd.split(" ")), stdout=fout, stderr=ferr)
            # reset file to read from it
            fout.seek(0)
            # save output (if any) in variable
            stdout = fout.read()

            # reset file to read from it
            ferr.seek(0) 
            # save errors (if any) in variable
            stderr = ferr.read()

    return stdout, stderr


def run_system_cmd_capture_stderr_stdout_no_shell(cmd: str) -> Tuple[str, str]:
    """
    Interact with process:
    Send data to stdin. Read data from stdout and stderr, until end-of-file is reached
    and send back to the variable below
    Wait for process to terminate.
    :param cmd:  String of the command to run
    :return: Tuple of containing the standard out string and the standard error string
    """

    # https://docs.python.org/2/library/subprocess.html
    p = subprocess.Popen(list(cmd.split(" ")),
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         universal_newlines=True)

    stdout, stderr = p.communicate()
    issue_with_stderr(stderr)
    return stdout, stderr


def run_system_cmd_capture_stderr_stdout_with_shell(cmd: str) -> Tuple[str, str]:
    """
    Sometimes you have thing like globbing (ls -l *), the trick is to pass everything as a single string,
    and not as a list. When you’re invoking commands with shell=True, you’re basically telling Python that
    the shell should break apart your arguments and expand them.
    If you pass a list to the shell, then the parsing is done the wrong number of times, and in the wrong places,
    and you will not get what you want
    :param cmd:  String of the command to run
    :return: Tuple of containing the standard out string and the standard error string
    """

    p = subprocess.Popen(cmd,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         universal_newlines=True,
                         shell=True)

    # Interact with process: 
    # Send data to stdin. Read data from stdout and stderr, until end-of-file is reached
    # and send back to the variable below 
    # Wait for process to terminate. 
    stdout, stderr = p.communicate()
    issue_with_stderr(stderr)
    return stdout, stderr


def issue_with_stderr(stderr: str):
    """
    Just print out STDERR if it's there
    :param stderr: String of STDERR
    :return: NoneType
    """
    if stderr:
        print(f"\nWas there a problem? Review STDERR below:\n\n{stderr}", file=sys.stderr)