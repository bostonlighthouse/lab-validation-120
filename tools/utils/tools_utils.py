import json
import os
import re
from pathlib import Path

from lab_validation_120.utils.r_function_stats import pbinom


def get_chromosomes(add_chr_str: bool=True, add_y_chr: bool=False) -> list:
    """
    Get a list of chromosomes
    :param add_chr_str: string, prepend chr to each chromosome -> chr1, chr2, etc
    :param add_y_chr:  Add the Y chromosome to the list
    :return: list of chromosomes
    """

    chromosomes =  [str(i) for i in list(range(1, 23)) + ['X']]
    if add_y_chr is True:
        chromosomes += ['Y']

    if add_chr_str is True:
        return [f'chr{i}' for i in chromosomes]
    return chromosomes


def test_absolute_path(*args) -> None:
    """
    Test if the path passed in are absolute
    :param args: A list of paths to chec
    :return: None
    """

    arg_abs = [os.path.isabs(dirs) for dirs in args]

    if all(arg_abs):
        pass
    else:
        raise OSError(f"One or more directories: {args}\nAre not absolute")


def get_onco_tree_data(json_file=None):
    with open(json_file) as infh:
        return json.load(infh)


def open_directory_get_list_files(directory: str = None, file_extension: str = '.vcf') -> list:
    """
    Open a directory a pull out a certain file type and return a list of them
    @param directory: The directory to open
    @param file_extension: What file extension to pull out
    """

    list_files = []
    try:
        with os.scandir(directory) as entries:
            for entry in entries:  # entry is an object!
                if entry.is_file():
                    _, ending = os.path.splitext(entry.name)
                    if ending == file_extension:
                        list_files.append(entry.name)
    except FileNotFoundError as err:
        print(f"Directory not found {err}")

    return list_files


def open_directory_get_list_files_recursively(directory: str = None, file_glob: str = '*.vcf') -> list:
    """
    Open a directory a pull out a certain file type and return a list of them
    @param directory: The directory to open
    @param file_glob: What file extension to pull out
    """

    list_files = []
    try:
        for path in Path(directory).rglob(file_glob):
            _, ending = os.path.splitext(path)
            list_files.append(str(path))
    except FileNotFoundError as err:
        print(f"Directory not found {err}")
    return list_files


def return_file_type(dir=None, file_ext=None):
    """
    @param dir: A string that's a directory to search for the file_ext type
    @param file_ext: A string that's a file extension
    @return string that matches the file_ext type
    """
    for file in os.listdir(dir):
        if file.endswith(file_ext):
            return os.path.join(dir, file)


def get_cid_from_string(string: str = None) -> str:
    """
    Parse out the cid from the bam files from Cider pipeline
    @param string: The string to parse
    """
    match = re.search(r"cid\d+-\d+", string)
    cid = None
    if match:
        cid = match.group(0)
    return cid


def get_batch_from_string(string: str = None) -> str:
    """
    Parse out the batch from the bam files from Cider pipeline
    @param string: The string to parse
    """
    match = re.search(r"b2\d-\d+", string)
    cid = None
    if match:
        cid = match.group(0)
    return cid

def return_oc395_pon_counts_json():
    """
    Return the JSON for the counts for oc395
    :return:
    """
    return "lab_validation_120/internal_data_sources/pon/clinical_snapshot395_cider2_prod_normal_freq.json"


def get_pon_counts_data(json_file: str) -> dict:
    """
    get a data structure from a PoN counts json file
    :param json_file:
    :return:
    """
    with open(json_file, encoding="utf8") as infh:
         data_struc = json.load(infh)
    # return a data structure with a key being the VAR_ID
    return_data_struc = {}
    for ds in data_struc:
        ds['count'] = len(ds['IN_SAMPLES_TOTAL'])
        return_data_struc[ds['VAR_ID']] = ds  # udpate the ds with the VAR_ID key and element

    return return_data_struc


def get_binomial_probability(total_depth: int, vaf: float, sig_rounding: int = 3) -> float:
    """
    Helper function to get the probability, based on VAF (known vaf, not our measured VAF),
    a minimum of 5 reads, and total coverage at that site
    :param total_depth: Total depth of the variant Ref + Alt
    :param vaf: Varaiant allele in  100 or 50.1 format, not fractional 1.0 or .501
    :param vaf:  How many digits to round to?
    :return ret_val: float
    """
    # the minimum reads, i.e. >= min_variant_reads are used in the pbnom call
    min_variant_reads = 5

    return round(1 - pbinom(min_variant_reads - 1, int(total_depth), prob=vaf/100), sig_rounding)
