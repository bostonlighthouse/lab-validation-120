from math import sqrt
from scipy.special import ndtri
from typing import Tuple

def _proportion_confidence_interval(observed_subjects: int,
                                    sample_size: int,
                                    z_score: float,
                                    sig_digits: int = 3) -> Tuple[float, float]:
    """
    Compute confidence interval for a proportion.
    Follows notation described on pages 46--47 of [1].
    References
    ----------
    [1] R. G. Newcombe and D. G. Altman, Proportions and their differences, in Statistics
    with Confidence: Confidence intervals and statistical guidelines, 2nd Ed., D. G. Altman,
    D. Machin, T. N. Bryant and M. J. Gardner (Eds.), pp. 45-57, BMJ Books, 2000.

    :param observed_subjects:  int, Observed number of subjects with some feature in a sample of size n
    :param sample_size: int, Sample size
    :param z_score: float, The Z score, e.g. the Z sore for 95% confidence is Z = 1.96.
    :return: Tuple with the upper and lower
    """
    
    # As the book mentions: First, calculate the three quantities A, B, and C in the book
    quantity1 = 2 * observed_subjects + z_score ** 2
    quantity2 = z_score * sqrt(z_score ** 2 + 4 * observed_subjects * (1 - observed_subjects / sample_size))
    quantity3 = 2 * (sample_size + z_score ** 2)

    return round(((quantity1 - quantity2) / quantity3), sig_digits), \
           round(((quantity1 + quantity2) / quantity3), sig_digits)


def sensitivity_and_specificity_with_confidence_intervals(tp: int, fp: int, fn: int, tn: int, alpha: float = 0.95)\
        -> Tuple[float, float, Tuple[float, float], Tuple[float, float]]:
    """
    Compute confidence intervals for sensitivity and specificity using Wilson's method.

    This method does not rely on a normal approximation and results in accurate
    confidence intervals even for small sample sizes.
    :param tp: int, Number of true positives
    :param fp: int, Number of false positives
    :param fn: int, Number of false negatives
    :param tn: int, Number of true negatives
    :param alpha: float, Desired confidence. Defaults to 0.95, which yields a 95% confidence interval.
    :return:

    Returns
    -------
    sensitivity : float
        Numerical estimate of the test sensitivity
    specificity : float
        Numerical estimate of the test specificity
    sensitivity_confidence_interval : Tuple (float, float)
        Lower and upper bounds on the alpha confidence interval for sensitivity
    specificity_confidence_interval
        Lower and upper bounds on the alpha confidence interval for specificity

    References
    ----------
    [1] R. G. Newcombe and D. G. Altman, Proportions and their differences, in Statisics
    with Confidence: Confidence intervals and statisctical guidelines, 2nd Ed., D. G. Altman,
    D. Machin, T. N. Bryant and M. J. Gardner (Eds.), pp. 45-57, BMJ Books, 2000.
    [2] E. B. Wilson, Probable inference, the law of succession, and statistical inference,
    J Am Stat Assoc 22:209-12, 1927.
    """

    # Get z score
    z_score = -ndtri((1.0 - alpha) / 2)

    # Compute sensitivity using method described in [1]
    sensitivity = tp / (tp + fn)
    sensitivity_confidence_interval = _proportion_confidence_interval(observed_subjects=tp,
                                                                      sample_size=tp + fn,
                                                                      z_score=z_score)

    # Compute specificity using method described in [1]
    specificity = tn / (tn + fp)

    specificity_confidence_interval = _proportion_confidence_interval(observed_subjects=tn,
                                                                      sample_size=tn + fp,
                                                                      z_score=z_score)

    return sensitivity, specificity, \
        sensitivity_confidence_interval, specificity_confidence_interval

"""
if __name__ == '__main__':
    for a in [0., 0.5, 0.9, 0.95, 0.99, 0.999999]:
        sensitivity_point_estimate, specificity_point_estimate, \
            sensitivity_confidence_interval, specificity_confidence_interval \
            = sensitivity_and_specificity_with_confidence_intervals(5, 3, 1, 117, alpha=a)
        print("Sensitivity: %f, Specificity: %f" % (sensitivity_point_estimate, specificity_point_estimate))
        print("alpha = %f CI for sensitivity:" % a, sensitivity_confidence_interval)
        print("alpha = %f CI for specificity:" % a, specificity_confidence_interval)
        print("")
"""
