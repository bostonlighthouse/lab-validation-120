"""Go off and query the LIMS and pull out sample information from a cid"""
import argparse
import pandas as pd
import os

from tools.sample_data_from_lims.get_sample_results import query_wikitools_graphql, query_wikitools_graphql_nested


def main():
    """Business logic"""
    args = get_cli_args()
    df = process_input_file(input_file=args.cid_file)
    cid_file = os.path.splitext(args.cid_file)[0]
    df.to_excel(f'{cid_file}_lims_data.xlsx')


def process_input_file(input_file: str = None) -> pd.DataFrame:
    """
    Go over the input file and query the LIMS and the graphql api's to get the data
    :param input_file: Input file to parse
    """
    poe_link_values_to_query_graphql = ['ID', 'Dateofbirth', 'Firstname', 'Middlename', 'Lastname',
                                        'Gender', 'InternalProviderNumber', 'TestCategory']
    derivative_link_values_to_query_graphql = ['ID', 'PrimarySite', 'PrimarySiteDiagnosis', 'PrimarySubSite',
                                               'SecondarySite', 'SecondarySiteDiagnosis', 'SecondarySubSite',
                                               'TumorPercentage']
    nested_key = 'Derivative'
    poe_key = 'POE'
    final_data = {}
    with open(input_file, encoding='utf-8') as in_fh:
        for i, cid in enumerate(in_fh):
            if cid.startswith('#'):
                continue
            cid = cid.rstrip().replace('cid', 'CID')
            poe_dict = query_wikitools_graphql(id_to_query=cid,
                                               values_to_query_graphql=poe_link_values_to_query_graphql,
                                               key_to_query='POELink')
            final_data[cid] = {}
            final_data[cid][poe_key] = poe_dict['data']['cid'][0]['POELink']
            # get the nested level data, here the 'Derivative'
            derivative_dict = query_wikitools_graphql_nested(id_to_query=cid,
                                               values_to_query_graphql=derivative_link_values_to_query_graphql,
                                               key_to_query='POELink', key_to_query_nested=nested_key)
            final_data[cid][nested_key] = derivative_dict['data']['cid'][0]['POELink'][nested_key][0]

            #if i == 2:
            #    break

    # Flattening the dictionary and adding the 'cid' key
    flattened_data = []
    for cid, details in final_data.items():
        # both dicts will have ID, so update
        details[poe_key]['ID_POE'] = details[poe_key]['ID']
        details[poe_key]['ID_DERIVATIVE'] = details[nested_key]['ID']
        # merge the dictionaries
        details = merge_dict(details[poe_key], details[nested_key])
        details['cid'] = cid.replace('CID', 'cid')
        # store for df later
        flattened_data.append(details)

    # Creating a DataFrame from the flattened data
    df = pd.DataFrame(flattened_data)

    # Rearranging the columns as specified
    #column_order = ['cid', 'ID', 'Dateofbirth', 'Firstname', 'Middlename', 'Lastname', 'Gender',
    #                'InternalProviderNumber', 'TestCategory']
    #df = df[column_order]
    return df


def merge_dict(dict1: dict=None, dict2: dict=None) -> dict:
    """
    Merge two dictionaries
    :param dict1: dictionary 1
    :param dict2: dictionary 1
    :return: merged dictionary
    """
    merge_dict = {**dict1, **dict2}
    return merge_dict


def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Go off and query the LIMS and pull out sample information from a cid"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--cid_file', dest='cid_file',
                        required=True, help="CIDs, one per line...")


    return parser.parse_args()


if __name__ == "__main__":
    main()
