"""Get the CID informaiton needed"""
import argparse
import sys

from tools.lims_api.get_lims_data import get_json_result, return_batch_and_cid

# http://www.cathdb.info/search/by_sequence
URL_BASE = "http://api.lims.demo.bostonlighthouse.us/"
RESULT_TYPE = 'reportv2'
HEADERS = {'Accept': 'application/json'}

def main():
    """Business Logic"""

    args = get_cli_args()

    if args.cid_file_from_cosmos_table is not None:
        cid_file_from_cosmos_table(args=args)
    if args.cid_file_one_per_line is not None:
        cid_file_one_per_line(args=args)


def cid_file_one_per_line(args=None):
    # go over the list of CIDs and get the results object from the LIMS, and then print what's needed in a tab delimited
    print_header = True
    with open(args.cid_file_one_per_line, 'r') as in_fh:
        for cid in in_fh:
            cid = cid.rstrip()
            batch=None
            print_data(get_json_result(cid,
                                       "".join((URL_BASE, RESULT_TYPE)), HEADERS),
                       print_header_line=print_header, batch=batch, cid=cid)
            print_header = False


def cid_file_from_cosmos_table(args=None):
    # go over the list of CIDs and get the results object from the LIMS, and then print what's needed in a tab delimited
    print_header = True
    with open(args.cid_file_from_cosmos_table, 'r') as in_fh:
        for cid in in_fh:
            if cid.startswith("#"):
                continue
            cid = cid.split(" ")[1]  # split off the cid
            batch, cid = return_batch_and_cid(input=cid)
            #match = re.findall(r"(b\d+-\d+)-(cid\d+-\d+)", cid)  # match the batch and cid
            #batch = match[0][0]
            #cid = match[0][1]

            cid = cid.rstrip().replace('cid', "CID")
            print_data(get_json_result(cid,
                                       "".join((URL_BASE, RESULT_TYPE)), HEADERS),
                       print_header_line=print_header, batch=batch, cid=cid)
            print_header = False


def print_data(json_data, print_header_line=None, batch=None, cid=None):
    """
    void : print_data(json_data)
    Takes: Deserialized JSON object, and prints out the data
    @param json_data: The LIMS Json object
    @param batch: The batch from the run
    @param cid: CID being called
    Returns: None
    """
    print(f"Attempting to print CID {cid}", file=sys.stderr)
    if print_header_line is True:
        # print out the header
        print("\t".join(("Batch Number",
                         "Assay type",
                         "Accession Number",
                         "Primary Site",
                         "Primary Site Diagnosis",
                         "Tumor Percentage",
                         "MCC",
                         "Fraction GTE Upper DP",
                         "Sample QC")))

    # was there an error in the json object
    if json_data['result']['error'] == 'PASS':
        pass
    else:
        print(f"Error with CID {cid} code: {json_data['result']['error']}")
        return

    # simplify some calls in printing
    dict_data = json_data['result']['report']
    derivative_data = dict_data['derivative_data']
    vv_data = dict_data['vv_data']
    assay_type = dict_data['test_data']['test']

    if "SNAPSHOT" in assay_type:
        mcc = str(vv_data['coverage_info']['MEAN_COLLAPSED_COVERAGE'])
        fgte = str(vv_data['coverage_info']['Fraction_GTE_Upper_DP']).replace("%", "")
    else:
        mcc = 'N/A'
        fgte = 'N/A'

    #
    tumor_percentage = derivative_data['tumor_percentage'] if derivative_data['tumor_percentage'] is not None else "N/A"
    # print the data out
    print("\t".join((
        batch,
        dict_data['test_data']['test'],
        dict_data['Accession Number'],
        derivative_data['primary_site'],
        derivative_data['primary_site_diagnosis'],
        tumor_percentage,
        mcc,
        fgte,
        str(vv_data['sample_qc']),
    )))


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Go off and query the LIMS for a CID result object.  One cid per line"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--cid_file_from_cosmos_table', dest='cid_file_from_cosmos_table',
                        required=False, help="cosmos input file")
    parser.add_argument('--cid_file_one_per_line', dest='cid_file_one_per_line',
                        required=False, help="List of CIDs to get, one per line")
    return parser.parse_args()


if __name__ == "__main__":
    # results came from http://api.lims.demo.bostonlighthouse.us/reportv2/$CID
    main()
    # json_data = json_printer()
    # print_data(json_data)
