"""
Get the CID information when there are no report results, e.g. at the time gs395 had no results so
get_cid_report_results did not function
"""
import argparse
import sys
import urllib

import requests
from contextlib import contextmanager


# Constants for OAuth and GraphQL endpoints
GRAPHQL_URL = 'http://wikitools3.cgs.bostonlighthouse.us/graphql'
AUTH_TOKEN_URL = "http://auth.internal.bostonlighthouse.us/realms/bli/protocol/openid-connect/token"
SVC_USER = 'bli_dev'
SVC_PASS = 'BLI2020'


from tools.lims_api.get_lims_data import return_batch_and_cid


def main():
    """Business logic"""
    args = get_cli_args()
    cid_pipeline_directory_file_file = args.cid_pipeline_directory_file
    staging_vv_urls_file = args.staging_vv_urls_file
    staging_vv_urls = None
    if staging_vv_urls_file:
        staging_vv_urls = get_staging_vv_urls(input_file=staging_vv_urls_file)
    # process the input file and the staging_vv_urls
    process_input_file(input_file=cid_pipeline_directory_file_file, staging_vv_urls=staging_vv_urls)



def get_auth_token(session):
    """
    Request an auth token from oauth2 endpoint.
    :param session: requests session
    :return: auth token
    """
    response = session.post(
        url=AUTH_TOKEN_URL,
        data={
            'client_id': 'oauth2-proxy',
            "grant_type": "password",
            'username': SVC_USER,
            'password': SVC_PASS,
        },
        timeout=30
    )
    response.raise_for_status()  # It's a good idea to check for errors
    return response.json()['access_token']


@contextmanager
def oauth_session():
    """
    Use OAuth2 session.
    """
    session = requests.Session()
    try:
        token = get_auth_token(session)
        session.headers.update({'Authorization': f'Bearer {token}'})
        yield session
    finally:
        session.close()



def get_staging_vv_urls(input_file: str = None) -> dict:
    """
    Some values were in staging VV, so the redirect will not work.  Here we have a file:
    :param input_file: File to open and get the data.  File looks like:

    CID     Batch   URL Before
    CID22-217       B22-12  vv.stage.bostonlighthouse.us/varvetter/prod/s......
    """

    staging_vv_urls = {}
    with open(input_file, encoding='utf-8') as in_fh:
        in_fh.readline()  # get rid of the header
        for line in in_fh:
            values = line.rstrip().split("\t")
            cid, batch, url = values
            url = f"http://{url}"
            #assert 'vv.stage.bostonlighthouse.us' in url, f"The {url} does not have vv.stage.bostonlighthouse.us in it"
            staging_vv_urls[f"{cid}-{batch}"] = url
    return staging_vv_urls


def process_input_file(input_file: str = None, staging_vv_urls: dict = None):
    """
    Go over the input file and query the LIMS and the graphql api's to get the data
    :param input_file: Input file to parse
    :param staging_vv_urls: Dictionary of values, key like: CID22-231-B22-12 and values like:
    vv.stage.bostonlighthouse.us/varvetter/prod/snapshot395/tumor/research/2022fev15-oc395-run0-b22-12-cid22-217-
    snapshot.B22-12.CID22-217.C511_N703
    """
    i = 0
    with open(input_file, encoding='utf-8') as in_fh:
        for line in in_fh:
            if line.startswith('#'):
                continue
            line = line.rstrip()
            batch, cid = return_batch_and_cid(input=line)
            print(f"Attempting to get {batch} {cid}", file=sys.stderr)
            batch = batch.upper()
            cid = cid.upper()
            vv_url = get_vv_url_redirect(batch=batch, cid=cid, staging_vv_urls=staging_vv_urls)
            # we now have the url to get the JSON data from VV
            sample_vv_dict = get_vv_info(url=vv_url)
            if len(sample_vv_dict) == 0:
                print(f"Failed1 to get {batch} {cid}... skipping", file=sys.stderr)
                cid2 = '-'.join((cid, 'cnv'))
                vv_url = get_vv_url_redirect(batch=batch, cid=cid2, staging_vv_urls=staging_vv_urls,
                                             second_attempt=True)
                sample_vv_dict = get_vv_info(url=vv_url)
                if len(sample_vv_dict) == 0:
                    print(f"Failed2 to get {batch} {cid}... skipping", file=sys.stderr)
                    #print(f"{batch}\t{cid}\tFAILED")  # print this out to the output file.  I can manually file in if needed
                    continue
            derivative_values_to_query_qraphql = ['ID', 'PrimarySite', 'PrimarySiteDiagnosis', 'TumorPercentage',
                                                  'Metastatic']
            derivative_dict = query_wikitools_graphql(id_to_query=cid,
                                                      values_to_query_graphql=derivative_values_to_query_qraphql,
                                                      key_to_query='Derivative')

            poe_link_values_to_query_graphql = ['ID', 'Dateofbirth', 'Firstname', 'Middlename', 'Lastname',
                                                'Gender', 'InternalProviderNumber', 'TestCategory']
            poe_dict = query_wikitools_graphql(id_to_query=cid,
                                               values_to_query_graphql=poe_link_values_to_query_graphql,
                                               key_to_query='POELink')
            mrn_link_values_to_query_graphql = ['ID', 'Offset', 'CreationDate', 'ModificationDate', 'PageNotFound',
                                                'URL', 'Patient_ID']
            mrn_dict = query_wikitools_graphql(top_key_to_query='poe',
                                               id_to_query=poe_dict['data']['cid'][0]['POELink']['ID'],
                                               values_to_query_graphql=mrn_link_values_to_query_graphql,
                                               key_to_query='MRN')

            #if i == 2:
            #    sys.exit()
            # print out the data
            print_data(batch=batch, cid=cid, vv_url=vv_url, sample_vv_dict=sample_vv_dict,
                       derivative_dict=derivative_dict,
                       derivative_values_to_query_graphql=derivative_values_to_query_qraphql,
                       poe_dict=poe_dict,
                       poe_link_values_to_query_graphql=poe_link_values_to_query_graphql,
                       mrn_dict=mrn_dict,
                       mrn_link_values_to_query_graphql=mrn_link_values_to_query_graphql,
                       num_queries=i)
            i += 1


def print_data(batch: str = None, cid: str = None, vv_url: str = None, sample_vv_dict: dict = None,
               derivative_dict: dict = None,
               derivative_values_to_query_graphql: list = None,
               poe_dict: dict = None,
               poe_link_values_to_query_graphql: list = None,
               mrn_dict: dict = None,
               mrn_link_values_to_query_graphql: list = None,
               num_queries: int = None):
    """
    This subroutine will print out all the data
    :param batch: Batch of the run
    :param cid: the CID of the sample
    :param vv_url: The VV URL
    :param sample_vv_dict: The dictionary of data from VV
    :param derivative_dict: The dictionary from graphql
    :param derivative_values_to_query_graphql: The values we queried from graphql
    :param poe_dict: The dictionary from graphql
    :param poe_link_values_to_query_graphql: The values we queried from graphql for the POE
    :param mrn_dict: The dictionary from graphql
    :param mrn_link_values_to_query_graphql: The values we queried from graphql for the MRN
    :param num_queries: If it's the first line, print the header for the tab-delmited file
    """
    if num_queries == 0:  # only print out header 1st time
        print("\t".join(['batch', 'cid', 'vv_url', 'FRACTION_lower_than_UPPER_DP', 'Fraction_GTE_Upper_DP',
                         'MEAN_COLLAPSED_COVERAGE', 'num_variants']
                        + derivative_values_to_query_graphql + poe_link_values_to_query_graphql
                        + mrn_link_values_to_query_graphql))
    # store values that will be printed
    values_to_print = [batch,
                       cid,
                       vv_url.replace("/vargetter", "/varvetter"),  # replace this so it links to actual VV
                       sample_vv_dict['coverage_info']['FRACTION_lower_than_UPPER_DP'],
                       sample_vv_dict['coverage_info']['Fraction_GTE_Upper_DP'],
                       sample_vv_dict['coverage_info']['MEAN_COLLAPSED_COVERAGE'],
                       len(sample_vv_dict['variants'])]

    # go over the data from graphql print out the derivative
    for derivative in derivative_dict['data']['cid'][0]['Derivative']:
        id_to_check = derivative['ID'].lower()
        # don't want anything that was a normal tissue
        if 'saliva' in id_to_check or 'buccal' in id_to_check:
            continue
        for val in derivative_values_to_query_graphql:
            values_to_print.append(derivative[val])
        break  # we are done after finding one

    dict_vals = poe_dict['data']['cid'][0]['POELink']
    for val in poe_link_values_to_query_graphql:
        values_to_print.append(dict_vals[val])

    dict_vals = mrn_dict['data']['poe'][0]['MRN']
    for val in mrn_link_values_to_query_graphql:
        values_to_print.append(dict_vals[val])

    # print out the data
    print("\t".join([str(i) for i in values_to_print]))


def query_wikitools_graphql(top_key_to_query: str = 'cid', id_to_query: str = None,
                            values_to_query_graphql: list = None, key_to_query: str = None):
    """
    This will query graphql
    :param id_to_query: the CID of the sample
    :param values_to_query_graphql: The values we queried from graphql
    """

    query = f'{{{top_key_to_query} (ID:"{id_to_query}") {{ {key_to_query} ' \
            f'{{ {" ".join(values_to_query_graphql)} }} }} }}'
    with oauth_session() as session:
        # Post the GraphQL query using the authenticated session
        response = session.post(
            url=GRAPHQL_URL,
            json={'query': query}
        )
        response.raise_for_status()  # It's a good idea to check for errors
        return response.json()

def query_wikitools_graphql_nested(top_key_to_query: str = 'cid', id_to_query: str = None,
                                   values_to_query_graphql: list = None, key_to_query: str = None,
                                   key_to_query_nested: str = None):
    """
    This will query graphql
    :param id_to_query: the CID of the sample
    :param values_to_query_graphql: The values we queried from graphql
    """
    query = f'{{{top_key_to_query} (ID:"{id_to_query}") {{ {key_to_query} ' \
            f'{{ {key_to_query_nested} {{ {" ".join(values_to_query_graphql)} }} }} }} }}'

    with oauth_session() as session:
        # Post the GraphQL query using the authenticated session
        response = session.post(
            url=GRAPHQL_URL,
            json={'query': query}
        )
        response.raise_for_status()  # It's a good idea to check for errors
        return response.json()


def get_vv_info(url: str = None) -> dict:
    """
    Get the VV level JSON data
    :param url: URL to query
    """
    print(f"test {url}")
    request = requests.get(url, auth=("bli_admin", "BLI2020"))
    json_dict = request.json()
    if 'result' in json_dict:
        return json_dict['result']
    return {}


def get_vv_url_redirect(batch: str = None, cid: str = None, staging_vv_urls: dict = None,
                        second_attempt: bool = False,
                        collection: str = 'prod',
                        assay_class: str = 'snapshot395',
                        sample_class: str = 'research',
                        sample_type: str = 'tumor') -> str:
    """
    Get the vv redirect, e.g.
    :param batch: Batch of the run
    :param cid: the CID of the sample
    :param staging_vv_urls: Dictionary of values, key like: CID22-231-B22-12 and values like:
    vv.stage.bostonlighthouse.us/varvetter/prod/snapshot395/tumor/research/2022fev15-oc395-run0-b22-12-cid22-217-
    snapshot.B22-12.CID22-217.C511_N703
    """
    search_key = f"{cid}-{batch}"
    if staging_vv_urls and search_key in staging_vv_urls:
        url = staging_vv_urls[search_key]
        print(f"No need for redirect, getting {url}", file=sys.stderr)
        # continue
    else:
        url_to_get = f"http://api.lims.demo.bostonlighthouse.us/varvetter/{batch}/{cid}"
        if second_attempt is True:
            url_to_get = f"http://vv.cgs.bostonlighthouse.us/varvetter/search/redirect/{collection}/{assay_class}/{sample_type}/{sample_class}/{cid}"
        print(f"must redirect {url_to_get}", file=sys.stderr)
        request1 = requests.get(url_to_get)
        request2 = requests.get(request1.url, auth=("bli_admin", "BLI2020"))
        url = request2.url
    # update the url
    return url.replace("/varvetter", "/vargetter")


def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Go off and query the LIMS and pull out sample information.  One cid pipeline directory output per line"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--cid_pipeline_directory_file', dest='cid_pipeline_directory_file',
                        required=True, help="CID pipline directories... one per line")
    parser.add_argument('--staging_vv_urls_file', dest='staging_vv_urls_file',
                        required=False, help="VV staging urls file.  CID<tab>Batch<tab>URL",
                        default=None)

    return parser.parse_args()


if __name__ == "__main__":
    main()
