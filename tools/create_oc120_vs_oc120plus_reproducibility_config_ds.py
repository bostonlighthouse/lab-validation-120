import argparse
import os
import pprint as pp
import re

from tools.utils.tools_utils import return_file_type


def main():
    args = get_cli_args()
    if args.ds_type == 'reproducibility_vcf':
        print_ds_for_reproducibility_vcf(file=args.pair_file, path1=args.path1, path2=args.path2,
                                         sample_num=args.sample_num)
    elif args.ds_type == 'reproducibility_qc_metrics':
        print_ds_for_reproduciblity_qc_metrics(file=args.pair_file, path1=args.path1, path2=args.path2,
                                               sample_num=args.sample_num)
    elif args.ds_type == 'reproducibility_cnv_metrics':
        print_ds_for_reproduciblity_cnv_values(file=args.pair_file, path1=args.path1, path2=args.path2,
                                               sample_num=args.sample_num)
    elif args.ds_type == 'single_assay_qc_metrics':
        print_ds_for_single_assay_qc_metrics(file=args.pair_file, path=args.path1,
                                             sample_num=args.sample_num)
    elif args.ds_type == 'hotspots_pair':
        print_ds_for_hotspots_for_pair(file=args.pair_file, path1=args.path1, path2=args.path2)
    elif args.ds_type == 'hotspots_single':
        print_ds_for_hotspots_for_single(file=args.pair_file, path1=args.path1)
    elif args.ds_type == 'genosity':
        print_ds_for_genosity(file=args.pair_file, path=args.path1, sample_num=args.sample_num)
    elif args.ds_type == 'lod':
        print_ds_for_lod(file=args.pair_file, path=args.path1)


def print_ds_for_lod(file=None, path=None):
    """
    Print data structure for LOD analysis
    @param file: The file to open for the data structure
    @param path: Destination of directories found in --pair_file
    """
    if 'oc395' in file:
        file_prefix = os.path.splitext(file)[0].replace('tools/input_files/oc395/', '')
    elif 'agilent_700' in file:
        file_prefix = os.path.splitext(file)[0].replace('tools/input_files/agilent_700/', '')
    else:
        file_prefix = os.path.splitext(file)[0].replace('tools/input_files/gs180_snps/', '')
    # create some clinical sample names
    sample_names = ['_'.join((file_prefix, i)) for i in ['100%', '50%', '25%', '12.5%', '6.25%', '3.125%']]

    print("file", file)
    print("test", sample_names)
    with open(file) as fh:
        lod_vcf_data = []
        lod_htqc_data = []
        sample_num = 1
        for i, line in enumerate(fh):
            if line.startswith("#"):
                continue
            print(sample_names[i])
            dir1 = line.rstrip()
            complete_dir = '/'.join((path, dir1))
            vcf_file = return_file_type(dir=complete_dir, file_ext=".vcf")
            qc_htqc_summary_file = return_file_type(dir=complete_dir, file_ext="coverage.summary.htqc.txt")
            qc_htqc_file = return_file_type(dir=complete_dir, file_ext="coverage.htqc.txt")
            create_new_vcf_lod(lod_vcf_data=lod_vcf_data, vcf1=vcf_file, sample_name=sample_names[i],
                               sample_num=sample_num)
            create_new_qc_metrics_entry_lod(qc_metrics_list=lod_htqc_data, qc_htqc_summary_file=qc_htqc_summary_file,
                                            sample_num=sample_num,
                                            qc_htqc_file=qc_htqc_file, sample_name=sample_names[i])
            sample_num += 1

    pp.pprint(lod_vcf_data, indent=5)
    print("\n\n\n****SECOND LIST-- HTQC\n\n\n")
    pp.pprint(lod_htqc_data, indent=5)


def print_ds_for_genosity(file=None, path=None, sample_num=None):
    """
    Print data structure for reproducibility analysis
    @param file: The file to open for the data structure
    @param path: Destination of directories found in --pair_file
    @param sample_num:  Just an int on where to start printing
    """
    with open(file) as fh:
        concordance_dict = {}
        for line in fh:
            dir1, genosity_overlap_variant_file = line.rstrip().split("\t")
            #print(genosity_overlap_variant_file)
            #quit()
            complete_dir1 = '/'.join((path, dir1))
            genosity_overlap_variant_file = '/'.join(('lab_validation_120/external_data_sources/genosity',
                                                      genosity_overlap_variant_file))
            oc_vcf_file = return_file_type(dir=complete_dir1, file_ext=".vcf")

            create_new_genosity_entry(concordance_dict=concordance_dict, oc_vcf_file=oc_vcf_file,
                                      genosity_overlap_variant_file=genosity_overlap_variant_file,
                                      sample_num=sample_num)
            sample_num += 1

    pp.pprint(concordance_dict, indent=5, sort_dicts=False)


def print_ds_for_hotspots_for_pair(file=None, path1=None, path2=None):
    """
    Print data structure for hotspot analysis
    @param file: The file to open for the data structure
    @param path1: Destination of directories found in --pair_file (1st entry)
    @param path2: Destination of directories found in --pair_file (2nd entry)
    """
    with open(file) as fh:
        hotspot_list1 = []
        hotspot_list2 = []
        for line in fh:
            if line.startswith("#"):
                continue
            dir1, dir2 = line.rstrip().split("\t")
            complete_dir1 = '/'.join((path1, dir1))
            complete_dir2 = '/'.join((path2, dir2))
            hotspot_file1 = return_file_type(dir=complete_dir1, file_ext="hotspots.htqc.txt")
            hotspot_file2 = return_file_type(dir=complete_dir2, file_ext="hotspots.htqc.txt")
            create_new_hotspot_pair_entry(hotspot_list1=hotspot_list1, hotspot_list2=hotspot_list2,
                                          hotspot_file1=hotspot_file1, hotspot_file2=hotspot_file2)

    pp.pprint(hotspot_list1, indent=5)
    print("\n\n\n****SECOND LIST\n\n\n")
    pp.pprint(hotspot_list2, indent=5)


def print_ds_for_hotspots_for_single(file=None, path1=None):
    """
    Print data structure for hotspot analysis
    @param file: The file to open for the data structure
    @param path1: Destination of directories found in --pair_file (1st entry)
    """
    with open(file) as fh:
        hotspot_list1 = []
        for line in fh:
            dir1 = line.rstrip()
            complete_dir1 = '/'.join((path1, dir1))
            hotspot_file1 = return_file_type(dir=complete_dir1, file_ext="hotspots.htqc.txt")
            create_new_hotspot_pair_entry(hotspot_list1=hotspot_list1, hotspot_list2=None,
                                          hotspot_file1=hotspot_file1, hotspot_file2=None)

    pp.pprint(hotspot_list1, indent=5)


def print_ds_for_single_assay_qc_metrics(file=None, path=None, sample_num=None):
    """
    Print data structure for qc metrics analysis
    @param file: The file to open for the data structure
    @param path: Destination of directories found in --pair_file (1st entry)
    @param sample_num:  Just an int on where to start printing
    """
    with open(file) as fh:
        qc_metrics_list = []
        for line in fh:
            dir1 = line.rstrip()
            complete_dir1 = '/'.join((path, dir1))
            qc_htqc_summary_file = return_file_type(dir=complete_dir1, file_ext="coverage.summary.htqc.txt")
            qc_htqc_file = return_file_type(dir=complete_dir1, file_ext="coverage.htqc.txt")

            create_new_qc_metrics_entry(qc_metrics_list=qc_metrics_list, qc_htqc_summary_file1=qc_htqc_summary_file,
                                        qc_htqc_file1=qc_htqc_file, sample_num=sample_num)
            sample_num += 1

    pp.pprint(qc_metrics_list, indent=5)

def print_ds_for_reproduciblity_qc_metrics(file=None, path1=None, path2=None, sample_num=None):
    """
    Print data structure for qc metrics analysis
    @param file: The file to open for the data structure
    @param path1: Destination of directories found in --pair_file (1st entry)
    @param path2: Destination of directories found in --pair_file (2nd entry)
    @param sample_num:  Just an int on where to start printing
    """
    with open(file) as fh:
        qc_metrics_list = []
        for line in fh:
            if line.startswith('#'):
                continue
            dir1, dir2 = line.rstrip().split("\t")
            complete_dir1 = '/'.join((path1, dir1))
            complete_dir2 = '/'.join((path2, dir2))
            qc_htqc_summary_file1 = return_file_type(dir=complete_dir1, file_ext="coverage.summary.htqc.txt")
            qc_htqc_summary_file2 = return_file_type(dir=complete_dir2, file_ext="coverage.summary.htqc.txt")
            qc_htqc_file1 = return_file_type(dir=complete_dir1, file_ext="coverage.htqc.txt")
            qc_htqc_file2 = return_file_type(dir=complete_dir2, file_ext="coverage.htqc.txt")

            create_new_qc_metrics_entry(qc_metrics_list=qc_metrics_list, qc_htqc_summary_file1=qc_htqc_summary_file1,
                                        qc_htqc_summary_file2=qc_htqc_summary_file2, qc_htqc_file1=qc_htqc_file1,
                                        qc_htqc_file2=qc_htqc_file2, sample_num=sample_num)
            sample_num += 1

    pp.pprint(qc_metrics_list, indent=5)

def print_ds_for_reproduciblity_cnv_values(file=None, path1=None, path2=None, sample_num=None):
    """
    Print data structure for qc metrics analysis
    @param file: The file to open for the data structure
    @param path1: Destination of directories found in --pair_file (1st entry)
    @param path2: Destination of directories found in --pair_file (2nd entry)
    @param sample_num:  Just an int on where to start printing
    """
    with open(file) as fh:
        cnv_metrics_list = []
        for line in fh:
            dir1, dir2 = line.rstrip().split("\t")
            complete_dir1 = '/'.join((path1, dir1))
            complete_dir2 = '/'.join((path2, dir2))
            cnr_file1 = return_file_type(dir=complete_dir1, file_ext="tumor.cnr.txt")
            cnr_file2 = return_file_type(dir=complete_dir2, file_ext="tumor.gene.cnr.txt")
            gene_cnr_file1 = return_file_type(dir=complete_dir1, file_ext="coverage.htqc.txt")
            gene_cnr_file2 = return_file_type(dir=complete_dir2, file_ext="coverage.htqc.txt")

            create_new_cnv_entry(cnv_metrics_list=cnv_metrics_list, cnv_file1=cnr_file1,
                                 cnv_file2=cnr_file2, gene_cnv_file1=gene_cnr_file1,
                                 gene_cnv_file2=gene_cnr_file2, sample_num=sample_num)
            sample_num += 1

    pp.pprint(cnv_metrics_list, indent=5)


def print_ds_for_reproducibility_vcf(file=None, path1=None, path2=None, sample_num=None):
    """
    Print data structure for reproducibility analysis
    @param file: The file to open for the data structure
    @param path1: Destination of directories found in --pair_file (1st entry)
    @param path2: Destination of directories found in --pair_file (2nd entry)
    @param sample_num:  Just an int on where to start printing
    """

    concordance_dict = {}
    with open(file) as fh:
        for line in fh:
            if line.startswith("#"):
                continue
            dir1, dir2 = line.rstrip().split("\t")
            complete_dir1 = '/'.join((path1, dir1))
            complete_dir2 = '/'.join((path2, dir2))
            vcf1 = return_file_type(dir=complete_dir1, file_ext=".vcf")
            vcf2 = return_file_type(dir=complete_dir2, file_ext=".vcf")
            create_new_reproducibility_entry(concordance_dict=concordance_dict, vcf1=vcf1, vcf2=vcf2,
                                             sample_num=sample_num)
            sample_num += 1
    _print_python_version_dependent(ds=concordance_dict)

def _print_python_version_dependent(ds=None):
    """
    Small helper function that lets me print out sorted data structure, like a dictionary, since pprint has issues with
    saving the order of insertions.  Not an issue with a list
    @parma ds: The data structure to print

    """
    # only works with Python 3.8+ sort_dicts=False, so need to run this outside of Pycharm to get the sample names
    # in the order I place them in.  Can't use OrderedCollection either from the collections module b/c pretty print
    pp.pprint(ds, indent=5, sort_dicts=False)
    #pp.pprint(ds, indent=5)

def create_new_genosity_entry(concordance_dict=None, oc_vcf_file=None, genosity_overlap_variant_file=None,
                              sample_num=None):
    """
    Add a new entry in the data structure for the gs120 vs genosity reproducibility analysis
    @param concordance_dict: what dictionary to to add the qc metrics ds to
    @param oc_vcf_file: Name of the oc120 vcf file to place in the data structure
    @param genosity_overlap_variant_file: Name of genosity variant file to place in the data structure
    @param sample_num:  Just an int on where to start printing
    """
    inter_sample = f'Sample{str(sample_num)}'
    concordance_dict[inter_sample] = []
    cid = re.search(r'(cid\d+-\d+)-', oc_vcf_file).group(1)
    concordance_dict[inter_sample].append({
        "sample_id": f"{sample_num}a-{cid}",
        "oc_vcf_file": oc_vcf_file,
        "variant_type": "snvs"
    })

    poe = re.search(r'(POE\d+_\d+)_', genosity_overlap_variant_file).group(1)
    # POE20_200_GER2016621_D1_N1_LP201009248_xGen_UDI_Index_17_HP200589_PL200327_SEQ_2010220277.14890.variants.summary.tumor.comparison.tsv
    list_ = genosity_overlap_variant_file.split('.')
    #genosity_overlap_variant_file = '.'.join((list_[0], list_[1])) + '.variants.summary.tumor.comparison.overlapping_oc120plus.tsv'
    concordance_dict[inter_sample].append({
        "sample_id": f"{sample_num}b-{poe}",
        "genosity_overlap_variant_file": genosity_overlap_variant_file,
        "variant_type": "snvs"
    })
    concordance_dict[inter_sample].append({
        "experiment_type": "oc120plus_recall_vs_genosity"
    })


def create_new_hotspot_pair_entry(hotspot_list1=None, hotspot_list2=None, hotspot_file1=None, hotspot_file2=None):
    """
    Add a new entry in the data structure for the hotspot analysis
    @param hotspot_list1: what list to to add the hotspot ds to
    @param hotspot_list2: what other list to to add the hotspot ds to
    @param hotspot_file1: Name of the first hotspot file to place in the data structure
    @param hotspot_file2: Name of the first hotspot file to place in the data structure
    """

    cid = re.search(r'(cid\d+-\d+)-', hotspot_file1).group(1)
    hotspot_list1.append({
        "sample_id": f"{cid}",
        "qc_htqc_file": hotspot_file1,
        "sample_type": "tumor"
    })

    if hotspot_list2 is not None:
        cid = re.search(r'(cid\d+-\d+)-', hotspot_file2).group(1)
        hotspot_list2.append({
            "sample_id": f"{cid}",
            "qc_htqc_file": hotspot_file2,
            "sample_type": "tumor"
        })


def create_new_qc_metrics_entry(qc_metrics_list=None, qc_htqc_summary_file1=None, qc_htqc_summary_file2=None,
                                qc_htqc_file1=None, qc_htqc_file2=None, sample_num=None):
    """
    Add a new entry in the data structure for the htqc metrics analysis
    @param qc_metrics_list: what list to to add the qc metrics ds to
    @param qc_htqc_summary_file1: Name of the first htqc summary metrics file to place in the data structure
    @param qc_htqc_summary_file2: Name of the second htqc summary metrics file to place in the data structure
    @param qc_htqc_file1: Name of the first htqc metrics file to place in the data structure
    @param qc_htqc_file2: Name of the second htqc metrics file to place in the data structure
    @param
    """
    cid = re.search(r'(cid\d+-\d+)', qc_htqc_summary_file1).group(1)
    qc_metrics_list.append({
        "sample_id": f"{sample_num}a-{cid}",
        "qc_htqc_summary_file": qc_htqc_summary_file1,
        "qc_htqc_file": qc_htqc_file1,
        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
    })
    if qc_htqc_summary_file2 is not None and qc_htqc_file1 is not None:
        cid = re.search(r'(cid\d+-\d+)', qc_htqc_summary_file2).group(1)
        qc_metrics_list.append({
            "sample_id": f"{sample_num}b-{cid}",
            "qc_htqc_summary_file": qc_htqc_summary_file2,
            "qc_htqc_file": qc_htqc_file2,
            "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
        })


def create_new_cnv_entry(cnv_metrics_list=None, cnv_file1=None, cnv_file2=None,
                         gene_cnv_file1=None, gene_cnv_file2=None, sample_num=None):
    """
    Add a new entry in the data structure for the htqc metrics analysis
    @param cnv_metrics_list: what list to to add the qc metrics ds to
    @param cnv_file1: Name of the first htqc summary metrics file to place in the data structure
    @param cnv_file2: Name of the second htqc summary metrics file to place in the data structure
    @param gene_cnv_file1: Name of the first htqc metrics file to place in the data structure
    @param gene_cnv_file2: Name of the second htqc metrics file to place in the data structure
    @param
    """
    cid = re.search(r'(cid\d+-\d+)', cnv_file1).group(1)
    cnv_metrics_list.append({
        "sample_id": f"{sample_num}a-{cid}",
        "cnv_file": cnv_file1,
        "gene_cnv_file": gene_cnv_file1,
        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
    })
    if cnv_file2 is not None and gene_cnv_file1 is not None:
        cid = re.search(r'(cid\d+-\d+)', cnv_file2).group(1)
        cnv_metrics_list.append({
            "sample_id": f"{sample_num}b-{cid}",
            "cnv_file": cnv_file2,
            "gene_cnv_file": gene_cnv_file2,
            "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
        })


def create_new_vcf_lod(lod_vcf_data=None, vcf1=None, sample_name=None, sample_num=None):
    """
    Add a new entry in the data structure for the vcf lod analysis
    @param lod_vcf_data: what dictionary to to add the qc metrics ds to
    @param vcf1: Name of the first vcf file to place in the data structure
    @param sample_name:  Just a string for the sample name
    @param sample_num:  Just an int on where to start printing
    """
    horizon_file = 'horizon_file'  # this is matched in the config file
    lod_vcf_data.append({
        "sample_id": f"{sample_num}_{sample_name}",
        "vcf_file": vcf1,
        "variant_type": "snvs",
        "horizon_file": horizon_file
    })


def create_new_qc_metrics_entry_lod(qc_metrics_list=None, qc_htqc_summary_file=None, qc_htqc_file=None,
                                    sample_name=None, sample_num=None):
    """
    Add a new entry in the data structure for the htqc metrics analysis for LOD
    @param qc_metrics_list: what list to to add the qc metrics ds to
    @param qc_htqc_summary_file: Name of the first htqc summary metrics file to place in the data structure
    @param qc_htqc_file: Name of the first htqc metrics file to place in the data structure
    @param sample_name:  Just a string for the sample name
    @param sample_num:  Just an int on where to start printing
    """

    qc_metrics_list.append({
        "sample_id": f"{sample_num}_{sample_name}",
        "qc_htqc_summary_file": qc_htqc_summary_file,
        "qc_htqc_file": qc_htqc_file,
        "sample_type": "Tumor",
    })


def create_new_qc_metrics_lod(qc_metrics_list=None, qc_htqc_summary_file=None, qc_htqc_file=None, sample_name=None):
    """
    Add a new entry in the data structure for the htqc metrics analysis
    @param qc_metrics_list: what list to to add the qc metrics ds to
    @param qc_htqc_summary_file: Name of the first htqc summary metrics file to place in the data structure
    @param qc_htqc_file: Name of the first htqc metrics file to place in the data structure
    @param sample_name:  Just a string for the sample name
    @param
    """

    qc_metrics_list.append({
        "sample_id": f"{sample_num}a-{cid}",
        "qc_htqc_summary_file": qc_htqc_summary_file,
        "qc_htqc_file": qc_htqc_file,
        "sample_type": "",  # intentionally left blank throughout this list of dictionaries below
    })


def create_new_reproducibility_entry(concordance_dict=None, vcf1=None, vcf2=None, sample_num=None):
    """
    Add a new entry in the data structure for the vcf reproducibility analysis
    @param concordance_dict: what dictionary to to add the qc metrics ds to
    @param vcf1: Name of the first vcf file to place in the data structure
    @param vcf2: Name of the second vcf file to place in the data structure
    @param sample_num:  Just an int on where to start printing
    """
    inter_sample = f'inter_Sample{str(sample_num)}'
    concordance_dict[inter_sample] = []
    cid = re.search(r'(cid\d+-\d+)', vcf1).group(1)
    concordance_dict[inter_sample].append({
        "sample_id": f"{sample_num}a-{cid}",
        "vcf_file": vcf1,
        "variant_type": "snvs"
    })
    if vcf2 is not None:
        cid = re.search(r'(cid\d+-\d+)', vcf2).group(1)
        concordance_dict[inter_sample].append({
            "sample_id": f"{sample_num}b-{cid}",
            "vcf_file": vcf2,
            "variant_type": "snvs"
        })
        concordance_dict[inter_sample].append({
            "experiment_type": "inter-run"
        })


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Create the data structure for '
                                                 'oc120_vs_oc120plus_reproducibility_config.py)')

    parser.add_argument('--pair_file', dest='pair_file',
                        type=str, help='What samples input file to use (could be paired per line or not)',
                        required=True)
    parser.add_argument('--path1', dest='path1',
                        type=str, help='Destination of directories found in --pair_file (first entry)',
                        required=True)
    parser.add_argument('--path2', dest='path2',
                        type=str, help='Destination of directories found in --pair_file (second entry) - not required',
                        required=False)
    parser.add_argument('--ds_type', dest='ds_type',
                        type=str, help='Print what data structure type',
                        choices=['reproducibility_vcf', 'reproducibility_qc_metrics', 'hotspots_pair', 'lod', 'genosity',
                                 'single_assay_qc_metrics', 'reproducibility_cnv_metrics', 'hotspots_single'],
                        required=True)
    parser.add_argument('--sample_num', dest='sample_num',
                        type=int, help='Sample number to start with, e.g. 1 or 2',
                        required=False, default=1)

    return parser.parse_args()


if __name__ == "__main__":
    main()