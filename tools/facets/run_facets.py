import os
import argparse
import re
import sys

from tools.facets.facets_utils import get_facets_cval_steps_gs180, get_facets_cval_steps_gs700
from tools.utils.tools_utils import get_cid_from_string


def main():
    args = get_cli_args()
    tumor_bam_files = args.tumor_bam_files
    tumor_bam_path = args.tumor_bam_path
    normal_bam = args.normal_bam
    normal_bam_path = args.normal_bam_path
    snp_overlap_vcf = args.snp_overlap_vcf
    assay_type = args.assay_type
    resource_path = args.resource_path
    targets_file = args.targets_file
    annotation_file = args.annotation_file
    print_docker_shell(tumor_bam_files=tumor_bam_files,
                       tumor_bam_path=tumor_bam_path,
                       normal_bam=normal_bam,
                       normal_bam_path=normal_bam_path,
                       targets_file=targets_file,
                       annotation_file=annotation_file,
                       snp_overlap_vcf=snp_overlap_vcf,
                       resource_path=resource_path,
                       assay_type=assay_type)


def get_insert_size(assay_type: str):
    assays = {
        # this came from running the facets w/o  --nbhd-snp flag, i.e. the first time when I use the BAM files
        # ##est_insert_size=138.1
        'agilent700': 138
    }
    if assay_type in assays:
        return assays[assay_type]
    else:
        raise ValueError(f"Unknown assay type {assay_type}")

def print_docker_shell(tumor_bam_files: str = None,
                       tumor_bam_path: str = None,
                       normal_bam: str = None,
                       normal_bam_path: str = None,
                       snp_overlap_vcf: str = None,
                       resource_path: str = None,
                       targets_file: str = None,
                       annotation_file: str = None,
                       assay_type: str = None) -> None:
    """
    Simple function to print out a bash script that can be run on the docker image
    :param tumor_bam_files:  A file with one bam file per line
    :param tumor_bam_path: The path to the bam files, this will be prepended to each bam file.  This should be the path
    mounted on the docker image
    :param normal_bam: Name of the normal to be used for the comparison
    :param normal_bam_path: The path to the Normal bam file, this will be prepended to normal bam file.
    This should be the path mounted on the docker image
    :param targets_file: The BED targets file to used by cnv_facets.R
    :param annotation_file: The annotation file that should be used by cnv_facets.R
    :param cid_to_print: If this has a value, only print out this cid to the bash script
    :param assay_type: Type of assay to run (this is used to get the cval types)

    """
    num_threads = 23
    snp_mapq = 30
    snp_baq = 30
    gbuild = 'hg19'
    cval_function = None
    if assay_type == 'gs180':
        cval_function = get_facets_cval_steps_gs180
    elif assay_type == 'agilent700':
        cval_function = get_facets_cval_steps_gs700

    shell_file = f"cnv_facets_for_agilent.sh"
    if os.path.exists(shell_file):  # remove old file b/c it was necessary to append
        os.unlink(shell_file)
    #for cval1 in range(5, 55, 5):
    #for cval1 in range(10, 60, 10):
    with open(tumor_bam_files) as infh, open(shell_file, "a") as outfh:
        for bam_file in infh:
            i = 0
            cvals = cval_function()
            cid = get_cid_from_string(bam_file)
            for cval1, cval2 in cvals:
                if cval1 > cval2:
                    continue
                bam_file = bam_file.rstrip()
                facets_out = f"{cid}_cval1_{cval1}_cval2_{cval2}_100bp_window"
                cmd = "\\time /home/cider/helpers/cnv_facets.R \\\n"
                cmd += f"--targets {os.path.join(resource_path, targets_file)} "
                cmd += f"--annotation {os.path.join(resource_path, annotation_file)} "
                #cmd += f"--depth 50 4000 "

                # read pileup file
                if i == 0:
                    cmd += f"-vcf {os.path.join(resource_path, snp_overlap_vcf)} "
                    cmd += f"--snp-tumour {os.path.join(tumor_bam_path, bam_file)} "
                    cmd += f"--snp-normal {os.path.join(normal_bam_path, normal_bam)} "
                    cmd += f"--snp-nprocs {num_threads} "  # Number of parallel processes to run to prepare the
                    cmd += f"--snp-mapq {snp_mapq} "  # Sets the minimum threshold for mapping quality
                    cmd += f"--snp-baq {snp_baq} "  # Sets the minimum threshold for base quality
                else:
                    cmd += f"--pileup {cid}.45m.csv.gz "
                    cmd += f"--nbhd-snp {get_insert_size(assay_type)} "

                cmd += f"--gbuild {gbuild}  "  # Sets the minimum threshold for base quality
                cmd += f"--cval {cval1} {cval2} "  # Sets the minimum threshold for base quality
                cmd += f"--out {facets_out} >{facets_out}.stdout 2>{facets_out}.stderr"
                print(cmd, file=outfh)
                if i == 0:
                    print(f"mv {facets_out}.csv.gz {cid}.45m.csv.gz # for the rest of {cid} to use", file=outfh)
                i += 1

    print(f"\n\nShell file printed to {shell_file}\n\n", file=sys.stderr)
def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Output the gene CNR values for two batches so you can analyzed')

    parser.add_argument('--tumor_bam_files', dest='tumor_bam_files',
                        type=str, help='List of bam files to run facets on',
                        required=True)
    parser.add_argument('--tumor_bam_path', dest='tumor_bam_path',
                        type=str, help='Path to tumor bam files on the docker container',
                        required=True)

    parser.add_argument('--normal_bam', dest='normal_bam',
                        type=str, help='Normal bam to test',
                        required=True)
    parser.add_argument('--normal_bam_path', dest='normal_bam_path',
                        type=str, help='Path to normal bam files on the docker container',
                        required=True)

    parser.add_argument('--snp_overlap_vcf', dest='snp_overlap_vcf',
                        type=str, help='Name of the snp overlap file',
                        required=True)


    parser.add_argument('--targets_file', dest='targets_file',
                        type=str, help='Targets BED file for the --targets argument used in cvn_facets.R. BED file of '
                                       'target regions to scan. It may be the target regions from WEX or panel '
                                       'sequencing protocols. It is not required, even for targeted sequencing, '
                                       'but it may improve the results',
                        required=True, default=None)

    parser.add_argument('--annotation_file', dest='annotation_file',
                        type=str, help='Annotation BED file for the --targets argument used in cvn_facets.R '
                                       'where the 4th column contains the feature name (e.g. gene name). CNVs will be '
                                       'annotated with an additional INFO/TAG reporting all the overalapping features',
                        required=True, default=None)

    parser.add_argument('--resource_path', dest='resource_path',
                        type=str, help='Where are the resource files',
                        required=True)


    parser.add_argument('--assay_type', dest='assay_type',
                        type=str, help='gs180 or agilent700',
                        required=True, choices=['gs180', 'agilent700'])

    return parser.parse_args()


if __name__ == "__main__":
    main()