import argparse
import seaborn as sns
from operator import itemgetter
import matplotlib.pyplot as plt
from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame
from lab_validation_120.utils.plots.plot_utilities import get_subplot_bounding_box_parameters
from tools.cnv.utils import get_list_important_cnv_genes_with_oncokb, \
    plot_cnv_cluster_or_heat_map, get_heatmap_cmap, plot_barchart_median_values_per_gene
from tools.utils.gene_lists import get_loh_genes
import pandas as pd
import numpy as np
import os
import io
import re

from tools.facets.utils.call_genome_level_loh_metrics import get_df_from_vcf_file, get_fraction_genome_altered, \
    get_fraction_genome_with_loh
from tools.utils.tools_utils import open_directory_get_list_files

ADD_STR_TO_OUTPUT_FILES = ''

def main():
    """Business logic"""
    args = get_cli_args()
    assay_type = args.assay_type
    global ADD_STR_TO_OUTPUT_FILES
    ADD_STR_TO_OUTPUT_FILES = args.add_str_to_output_files
    if args.facets_vcf_file is not None:
        facets_vcf_file = args.facets_vcf_file
        each_segment_per_cid_df = get_df_from_vcf_file(facets_vcf_file=facets_vcf_file)
        print_data_frame(each_segment_per_cid_df, f'final_file_{ADD_STR_TO_OUTPUT_FILES}.txt')
    elif args.facets_vcf_directory is not None:
        facets_vcf_directory = args.facets_vcf_directory
        list_vcf_files = open_directory_get_list_files(directory=facets_vcf_directory)
        each_segment_per_cid_df, overall_fractions_per_cval_pairs_df = \
            get_df_from_list_vcf_files(list_facets_vcf_files=list_vcf_files,
                                       facets_vcf_directory=facets_vcf_directory)
        # plot the distribution
        plot_distribution_genome_metric_diffs(overall_fractions_per_cval_pairs_df=overall_fractions_per_cval_pairs_df)

        ### TCN_EM data
        raw_gene_value_df = get_raw_gene_values(df=each_segment_per_cid_df, col_to_use='TCN_EM')
        base_file_name = f'facets_clustermap_TCN_EM_{ADD_STR_TO_OUTPUT_FILES}'
        tcn_em_medians = plot_gene_medians(df=raw_gene_value_df,
                                           base_file_name=base_file_name,
                                           value_plotted='TCN_EM',
                                           center_value=2, is_clustermap=True,
                                           replace_value=2.0,
                                           analysis_for_title=ADD_STR_TO_OUTPUT_FILES)

        base_file_name = f'facets_barchart_median_TCN_EM_{ADD_STR_TO_OUTPUT_FILES}'
        _ = plot_barchart_median_values_per_gene(heatmap_data=tcn_em_medians,
                                                 base_file_name=base_file_name,
                                                 fig_size=(10,10), xlim=(-2, 4),
                                                 value_to_plot='TCN_EM',
                                                 analysis_for_title=ADD_STR_TO_OUTPUT_FILES)
        ### CNLR data
        raw_gene_value_df = get_raw_gene_values(df=each_segment_per_cid_df, col_to_use='CNLR_MEDIAN')
        base_file_name = f'facets_clustermap_CNLR_MEDIAN_{ADD_STR_TO_OUTPUT_FILES}'
        cnlr_median_medians = plot_gene_medians(df=raw_gene_value_df,
                                                base_file_name=base_file_name,
                                                value_plotted='CNLR_MEDIAN',
                                                replace_value=0.0,
                                                center_value=0,
                                                is_clustermap=True,
                                                analysis_for_title=ADD_STR_TO_OUTPUT_FILES)

        base_file_name = f'facets_heatmap_CNLR_MEDIAN_{ADD_STR_TO_OUTPUT_FILES}'
        _ = plot_gene_medians(df=cnlr_median_medians,
                              base_file_name=base_file_name,
                              value_plotted='CNLR_MEDIAN',
                              replace_value=0.0,
                              center_value=0,
                              is_clustermap=False,
                              analysis_for_title=ADD_STR_TO_OUTPUT_FILES)

        base_file_name = f'facets_barchart_median_CNLR_MEDIAN_{ADD_STR_TO_OUTPUT_FILES}'
        plot_barchart_median_values_per_gene(heatmap_data=cnlr_median_medians,
                                             base_file_name=base_file_name,
                                             fig_size=(10,10),
                                             value_to_plot='CNLR_MEDIAN',
                                             analysis_for_title=ADD_STR_TO_OUTPUT_FILES)
        quit()

        if assay_type == 'agilent700':
            #for gene in get_list_important_cnv_genes():
            for gene in ['ERBB2', 'EGFR', 'CCNE1']:
                analyze_gene_level(each_segment_per_cid_df=each_segment_per_cid_df, gene=gene,
                                   cid_values_to_filter=get_erbb2_cnv_pos_agilent_cids())

        print_data_frame(df=each_segment_per_cid_df, tabular_output_file=f'final_file_{ADD_STR_TO_OUTPUT_FILES}.txt')
        print_data_frame(df=overall_fractions_per_cval_pairs_df,
                         tabular_output_file=f'overall_fractions_per_cval_pairs_df_{ADD_STR_TO_OUTPUT_FILES}.txt')
        plot_histograms(overall_fractions_per_cval_pairs_df=overall_fractions_per_cval_pairs_df,
                        each_segment_per_cid_df=each_segment_per_cid_df)

        plot_violin(segments_per_cval_df=each_segment_per_cid_df, x_axis='SVTYPE', y_axis='CNLR_MEDIAN')
        plot_violin(segments_per_cval_df=each_segment_per_cid_df, x_axis='SVTYPE', y_axis='NHET')


def get_updated_median_df(df: pd.DataFrame=None, replace_value: float=2.0) -> pd.DataFrame:
    """
    Data frame of values
    :param df:
    :return:
    """
    # convert gene columns to numerical values
    df.iloc[:, 2:] = df.iloc[:, 2:].apply(pd.to_numeric, errors='coerce')
    # Group by 'sample' and calculate median for each gene
    median_data = df.groupby('sample').median()
    # Replace NaN values with replace_value.  The NaN's were not counted in the median, and should not be replaced
    # with the replace_value.  By default use 2.0
    print(median_data)
    median_data = median_data.fillna(replace_value)
    return median_data


def plot_gene_medians(df: pd.DataFrame=None, replace_value: float=2.0, fig_size: tuple=(15, 13),
                      base_file_name: str=None,
                      value_plotted: str=None,
                      center_value: int=None,
                      is_clustermap: bool=True,
                      analysis_for_title: str=None) -> pd.DataFrame:
    """
    Create the gene data frame that can be plotted by heat map or clustermap
    :param df: data frame to use
    :return: None
    """
    median_data = get_updated_median_df(df=df, replace_value=replace_value)
    # Reset index to make 'sample' a column again
    #median_data.reset_index(inplace=True)
    # figure params
    params = {
        'legend.fontsize': 'x-large',
        'figure.figsize': fig_size,
        'axes.labelsize': 'x-large',
        'axes.titlesize': 'x-large',
        'xtick.labelsize': 10,
        'ytick.labelsize': 10
    }

    # Display the first few rows of the resulting dataframe
    plot_cnv_cluster_or_heat_map(heatmap_data=median_data, params=params, cmap=get_heatmap_cmap(),
                                 base_file_name=base_file_name, value_plotted=value_plotted,
                                 fig_size=fig_size, center_value=center_value, is_clustermap=is_clustermap,
                                 analysis_for_title=analysis_for_title)
    return median_data



def get_raw_gene_values(df: pd.DataFrame, col_to_use: str=None) -> pd.DataFrame:
    """
    Get the raw values for the column passed in the data frame passed in
    :param df: Data Frame to use
    :param col_to_use: What column to pull the data out from?
    :return: pd.DataFrame
    """

    cids = set(df['cid'].to_list())
    df_list = []  # store the data frames as they are being made
    # Convert the string in each cell to a list by splitting on the comma
    df['gene_list'] = df['CNV_ANN'].apply(lambda x: x.split(','))
    # go over all the important genes
    for gene in get_list_important_cnv_genes_with_oncokb():
        # filter out some data
        df_gene = df[df['gene_list'].apply(lambda x: gene in x)]
        df_gene = df_gene[df_gene['LCN_EM'] != '.']  # this means there was not enough NHET, so do not use
        # go over each CID
        for cid in cids:
            # if it's not found, fill with pd.NA for now b/c it will not be used in median
            gene_cid_to_store = {gene: [pd.NA]}

            df_gene_cid = df_gene[df_gene['cid'] == cid] # filter down by cid
            if len(df_gene_cid) > 0:
                gene_cid_to_store[gene] = df_gene_cid[col_to_use].to_list() # get the list of TCN_EM values
            # Create a DataFrame with the given structure
            df_of_values = pd.DataFrame(gene_cid_to_store[gene], columns=[gene])
            df_of_values.insert(0, 'sample', cid)  # add a new column with the CIDs
            df_list.append(df_of_values)
    # combine them and return
    return pd.concat(df_list).sort_values('sample').reset_index(drop=True)

def analyze_gene_level(each_segment_per_cid_df: pd.DataFrame = None,
                       cid_values_to_filter: list = None,
                       gene: str = None):
    """
    Analyze the erbb2 and egfr genes for agilent700
    :param each_segment_per_cid_df: Each segement's valuee from the FACETS vcf for each CID
    :param cid_values_to_filter: list of cids to filter
    :param gene: gene to filter on
    :return: None
    """
    if gene == 'ERBB2':
        function_to_get_cids = get_erbb2_cnv_pos_agilent_cids
    elif gene == 'EGFR':
        function_to_get_cids = get_egfr_cnv_pos_agilent_cids
    else:
        function_to_get_cids = None

    filtered_df = fiter_data_frame_cid(df=each_segment_per_cid_df, gene=gene, filter_list=cid_values_to_filter)
    print_data_frame(df=filtered_df, tabular_output_file=f'final_filtered_{gene}_file_{ADD_STR_TO_OUTPUT_FILES}.txt')
    median_tcm_em = filtered_df.groupby('cid')['TCN_EM'].median().reset_index()
    median_tcn_em_sorted = median_tcm_em.sort_values(by='TCN_EM', ascending=False)
    median_cf_em = filtered_df.groupby('cid')['CF_EM'].median().reset_index()
    if function_to_get_cids:
        print(f"gene {gene} before max_svlen {ADD_STR_TO_OUTPUT_FILES}")
        tmp_df1 = median_tcm_em[median_tcm_em['cid'].isin(function_to_get_cids())]
        tmp_df2 = median_cf_em[median_cf_em['cid'].isin(function_to_get_cids())]
        print(pd.merge(tmp_df1, tmp_df2, on='cid'))
    # Create the boxplot
    plt.figure(figsize=(16, 8))
    sns.set_style("whitegrid")
    sns.stripplot(data=filtered_df, x='cid', y='TCN_EM', jitter=True,
                  order=median_tcn_em_sorted['cid'], hue='cval1_cval2', s=4)
    sns.boxplot(data=filtered_df, x='cid', y='TCN_EM', fliersize=0,
                  order=median_tcn_em_sorted['cid'])
    # Define the positions of y-axis tick marks
    yaxis_max = 70
    yaxis_min = 0
    y_tick_positions = list(range(yaxis_min, yaxis_max + 5, 2))  # Adjust as needed
    # Set y-axis tick positions and labels
    plt.yticks(y_tick_positions)
    plt.ylim(yaxis_min, yaxis_max)
    plt.xticks(rotation=90)  # Rotate x-axis labels for better visibility
    plt.xlabel('CID')
    plt.ylabel('TCN_EM')
    plt.title('Boxplot of TCN_EM for each CID')
    plt.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(f'boxplot_{ADD_STR_TO_OUTPUT_FILES}_{gene}.svg')
    plt.close()

    # redo the same analysis but with a filtered dataframe, should be refactored to run this once
    max_svlen = 1_000_000
    filtered_df = filtered_df[filtered_df['SVLEN'] < max_svlen]
    median_tcm_em = filtered_df.groupby('cid')['TCN_EM'].median().reset_index()
    median_cf_em = filtered_df.groupby('cid')['CF_EM'].median().reset_index()
    if function_to_get_cids:
        print(f"gene {gene} after max_svlen {max_svlen} {ADD_STR_TO_OUTPUT_FILES}")
        tmp_df1 = median_tcm_em[median_tcm_em['cid'].isin(function_to_get_cids())]
        tmp_df2 = median_cf_em[median_cf_em['cid'].isin(function_to_get_cids())]
        print(pd.merge(tmp_df1, tmp_df2, on='cid'))
    median_tcn_em_sorted = median_tcm_em.sort_values(by='TCN_EM', ascending=False)
    plt.figure(figsize=(16, 8))
    sns.set_style("whitegrid")
    sns.stripplot(data=filtered_df, x='cid', y='TCN_EM', jitter=True,
                  order=median_tcn_em_sorted['cid'], hue='cval1_cval2', s=4)
    sns.boxplot(data=filtered_df, x='cid', y='TCN_EM', fliersize=0,
                  order=median_tcn_em_sorted['cid'])
    # Define the positions of y-axis tick marks
    y_tick_positions = list(range(yaxis_min, yaxis_max + 5, 2))  # Adjust as needed
    # Set y-axis tick positions and labels
    plt.yticks(y_tick_positions)
    plt.ylim(yaxis_min, yaxis_max)
    plt.xticks(rotation=90)  # Rotate x-axis labels for better visibility
    plt.xlabel('CID')
    plt.ylabel('TCN_EM')
    plt.title(f'Boxplot of TCN_EM for each cid (SVLEN < {max_svlen})')
    plt.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(f'boxplot_{ADD_STR_TO_OUTPUT_FILES}_{gene}_max_svlen_{max_svlen}.svg')
    plt.close()


def get_egfr_cnv_pos_agilent_cids():
    """EGFR copy number CIDs"""
    cid_values_to_filter = [
        'cid23-1830',
        'cid23-1833',
    ]
    return cid_values_to_filter

def get_erbb2_cnv_pos_agilent_cids():
    """ERBB2 copy number CIDs"""
    # List of CID values to filter
    cid_values_to_filter = [
        'cid23-1684',
        'cid23-1693',
        'cid23-1694',
        'cid23-1696',
        'cid23-1695',
        'cid23-1682',
        'cid23-1833',
        'cid23-1832',
        'cid23-1834',
        'cid23-1837',
        'cid23-1839'
    ]
    return cid_values_to_filter


def fiter_data_frame_cid(df: pd.DataFrame=None, gene: str=None, filter_list: list=None) -> pd.DataFrame:
    """
    One off to do some filtering
    :param df: Data frame to be filtered
    :param gene: Gene to filter on
    :param filter_list: list of cids to filter
    :return: pd.Data frame
    """
    cvals = [
        #'25_100',
        #'25_125',
        #'25_50',
        #'25_75',
        #'50_100',
        #'50_125',
        #'50_150',
        #'50_175'
             ]
    # just normalize
    filter_list = [x.lower() for x in filter_list]
    # Filter the DataFrame based on the specified criteria
    #return df[df['cid'].isin(filter_list) & df['CNV_ANN'].str.contains(gene)]
    #return df[df['CNV_ANN'].str.contains(gene)]
    condition1 = ~df['cval1_cval2'].isin(cvals)
    condition2 = df['CNV_ANN'].str.contains(gene)
    condition3 = df['LCN_EM'] != '.'
    final_condition = condition1 & condition2 & condition3
    return df[final_condition]


def add_labels_plot(ax: plt.Axes = None):
    labels = []
    for p in ax.patches:
        h = p.get_height()
        labels.append(str(h) if h else '')
    ax.bar_label(ax.containers[0], labels=labels)


def plot_distribution_genome_metric_diffs(overall_fractions_per_cval_pairs_df: pd.DataFrame = None):
    plt.rcParams["figure.figsize"] = [10, 5]
    fig, ax = plt.subplots(1, 2)  # rows x cols = Create 4 subplots
    diffs_loh = []
    diffs_altered = []
    # go over each cid and get the max difference between the metris for fraction genome with LOH and fraction of genome
    # altered
    for cid in overall_fractions_per_cval_pairs_df['cid'].unique():
        filtered_df = overall_fractions_per_cval_pairs_df[overall_fractions_per_cval_pairs_df['cid'] == cid]
        # store the diffs
        diffs_loh.append(filtered_df['fraction_genome_with_loh'].max() - filtered_df['fraction_genome_with_loh'].min())
        diffs_altered.append(filtered_df['fraction_genome_altered'].max() -
                             filtered_df['fraction_genome_altered'].min())
    # plot the differences
    y = sns.histplot(diffs_loh, ax=ax[0], bins=20).set(
        title='Difference in Fraction Genome LOH\nfor each CID, max - min, at all cvals')
    ax[0].set_xlabel("Difference max - min each sample\n(Fraction Genome with LOH)")
    add_labels_plot(ax=ax[0])

    sns.histplot(diffs_altered, ax=ax[1], bins=20).set(
        title='Difference in Fraction Genome Altered\nfor each CID, max - min, at all cvals')
    ax[1].set_xlabel("Difference max - min each sample\n(Fraction Genome Altered)")
    add_labels_plot(ax=ax[1])
    fig.tight_layout(rect=get_subplot_bounding_box_parameters())
    fig.savefig(f'differences_distributions_{ADD_STR_TO_OUTPUT_FILES}.svg')
    plt.close()


def plot_histograms(overall_fractions_per_cval_pairs_df: pd.DataFrame = None,
                    each_segment_per_cid_df: pd.DataFrame = None) -> None:
    # get the unique cval pairs used to loop over
    df2 = overall_fractions_per_cval_pairs_df[['cval1', 'cval2']].drop_duplicates()
    total_number_cids = len(overall_fractions_per_cval_pairs_df['cid'].unique())
    list_tuples = list(zip(df2['cval1'], df2['cval2']))  # get a list of tuples of those pairs
    plt.rcParams["figure.figsize"] = [35, 50]
    fig, ax = plt.subplots(len(list_tuples), 6)  # rows x cols = Create 4 subplots
    fraction_data_list = [] # store fractions of data, e.g. fraction fo genome with LCN_EM = '.', or fraction of samples
    # that passed
    for i, (cval1, cval2) in enumerate(sorted(list_tuples, key=itemgetter(0, 1))):
        print(cval1, cval2)
        segments_per_cval_df_filtered = \
            overall_fractions_per_cval_pairs_df[(overall_fractions_per_cval_pairs_df['cval1'] == cval1) &
                                                (overall_fractions_per_cval_pairs_df['cval2'] == cval2)].copy(deep=True)
        total_number_cids_for_cval_pair = len(segments_per_cval_df_filtered['cid'].unique())
        #print_data_frame(segments_per_cval_df_filtered, "test.txt")
        # plot out the three plots on a row
        sns.histplot(segments_per_cval_df_filtered['fraction_genome_with_loh'], ax=ax[i][0], bins=20)
        add_labels_plot(ax=ax[i][0])
        sns.histplot(segments_per_cval_df_filtered['fraction_genome_altered'], ax=ax[i][1], bins=20)
        add_labels_plot(ax=ax[i][1])
        sns.histplot(segments_per_cval_df_filtered['fraction_genome_na'], ax=ax[i][2], bins=20)
        # set the min and max to all the same here and set title
        for chart_index in [0, 1, 2]:
            ax[i][chart_index].set_xlim(0, 1)
            title = f"cval1 {cval1} cval2 {cval2}, # Samples = {len(segments_per_cval_df_filtered)}"
            if chart_index == 0:
                title = f"Fraction of the Genome with LOH: {title}"
            elif chart_index == 1:
                title = f"Fraction of the Genome Altered: {title}"
            elif chart_index == 2:
                title = f"Fraction Per Sample LCN_EM = '.': {title}\n" \
                        f"Len of segments where LCN_EM = '.' / Len of all segments"
            ax[i][chart_index].set_title(title, fontsize=9)

        # next get data for the violinplot
        df_cval_pairs = each_segment_per_cid_df[(each_segment_per_cid_df['cval1'] == cval1) &
                                                (each_segment_per_cid_df['cval2'] == cval2)].copy(deep=True)
        # create a new column based on the following condition
        df_cval_pairs['CLASS'] = np.where(df_cval_pairs['LCN_EM'] == '.', 'NO_VALUE', "HAS_VALUE")
        df_cval_pairs["NHET"] = df_cval_pairs["NHET"].astype(int)
        #print_data_frame(df_cval_pairs, "test.txt")
        sns.boxplot(data=df_cval_pairs, x='CLASS', y='NHET', ax=ax[i][3], order=['NO_VALUE', "HAS_VALUE"])
        sns.violinplot(data=df_cval_pairs, x='CLASS', y='NHET', ax=ax[i][4], order=['NO_VALUE', "HAS_VALUE"],
                       density_norm='count')
                       #scale="count")  # scale="count" the width of the violins will be scaled by the number of obs.

        numerator = len(df_cval_pairs[df_cval_pairs['CLASS'] == 'NO_VALUE'])
        denominator = len(df_cval_pairs)
        wo_lcm_fraction = round((numerator / denominator), 3)
        sample_passing_fraction = round((total_number_cids_for_cval_pair / total_number_cids), 3)
        # plot title for the violinplot
        title = f"cval1 {cval1} cval2 {cval2}, # Segments = {len(df_cval_pairs)}\n" \
                f"All segments per sample: w/ value {len(df_cval_pairs[df_cval_pairs['CLASS'] == 'HAS_VALUE'])}, " \
                f"w/o value {len(df_cval_pairs[df_cval_pairs['CLASS'] == 'NO_VALUE'])}\n" \
                f"Fraction Segments with LCM_EM = '.' : {wo_lcm_fraction} ({numerator}/{denominator})"

        sns.histplot(data=df_cval_pairs, x="NHET", ax=ax[i][5], bins=140)
        ax[i][3].set_title(title, fontsize=9)
        ax[i][4].set_title(title, fontsize=9)
        ax[i][5].set_title(f"NHET Distribution: {title}", fontsize=9)

        fraction_data_list.append(["_".join([str(cval1), str(cval2)]), wo_lcm_fraction, sample_passing_fraction])

    fig.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(f'fraction_genome_histogram_facets_{ADD_STR_TO_OUTPUT_FILES}.svg')
    plt.close()  # have to close

    # line graph of the percent genome with LCN_EM = '.'
    fraction_df = pd.DataFrame(fraction_data_list, columns=("cval_pair", "fraction_genome", "fraction_samples_passing"))

    # histogram NHET
    plt.rcParams["figure.figsize"] = [10, 5]
    fig, ax = plt.subplots()
    sns.histplot(data=each_segment_per_cid_df, x="NHET", ax=ax, bins=140).set(title="NHET Distribution")
    #ax.xaxis.set_major_locator(plt.MaxNLocator(28))
    plt.locator_params(axis='x', nbins=28)
    fig.savefig(f'nhet_distribution_{ADD_STR_TO_OUTPUT_FILES}.svg')
    plt.close()


    # line plot
    plt.rcParams["figure.figsize"] = [10, 5]
    fig, ax = plt.subplots()
    sns.lineplot(data=fraction_df, x="cval_pair", y="fraction_genome", label="Fraction genome LCN_EM = '.'", ax=ax)
    sns.lineplot(data=fraction_df, x="cval_pair", y="fraction_samples_passing", ax=ax,
                      label=f"Fraction samples passing FACETS, n={total_number_cids}").set(title="Line Plots")
    ax.set_xlabel("Cval Pair")
    ax.set_ylabel("Fraction")
    ax.grid()  # just add the grid
    ax.legend()
    ax.tick_params(axis='x', labelrotation=45)
    ax.yaxis.set_major_locator(plt.MaxNLocator(11))
    fig.savefig(f'fraction_genome_line_plot_{ADD_STR_TO_OUTPUT_FILES}.svg')
    plt.close()


def plot_violin(segments_per_cval_df: pd.DataFrame = None, x_axis: str = None, y_axis: str = None) -> None:
    df2 = segments_per_cval_df[['cval1', 'cval2']].drop_duplicates()  # get the unique cval pairs used to loop over
    list_tuples = list(zip(df2['cval1'], df2['cval2']))  # get a list of tuples of those pairs
    plt.rcParams["figure.figsize"] = [7, 50]
    fig, ax = plt.subplots(len(list_tuples))  # rows x cols = Create 4 subplots
    svtypes = ["NEUTR", "DUP", "DEL", "HEMIZYG", "LOH", "DUP-LOH"]
    for i, (cval1, cval2) in enumerate(sorted(list_tuples, key=itemgetter(0, 1))):
        print(cval1, cval2)
        segments_per_cval_df_filtered = segments_per_cval_df[(segments_per_cval_df['cval1'] == cval1) &
                                                             (segments_per_cval_df['cval2'] == cval2)].copy(deep=True)
        sns.violinplot(data=segments_per_cval_df_filtered, x=x_axis, y=y_axis, ax=ax[i],
                       order=svtypes,
                       density_norm='count')
                       #scale="count")  # scale="count" the width of the violins will be scaled by the number of obs.
        percentages = {}
        for svtype in svtypes:
            filtered_df = segments_per_cval_df_filtered[segments_per_cval_df_filtered[x_axis] == svtype]
            percentages[svtype] = round(len(filtered_df) / len(segments_per_cval_df_filtered), 3)

        title_percentage = " : ".join([f"{k} {v}" for k, v in percentages.items()])
        # plot out the three plots on a row
        title = f"cval1 {cval1} cval2 {cval2}, # Segments = {len(segments_per_cval_df_filtered)}\n" \
                f"{title_percentage}"
        ax[i].set_title(title, fontsize=9)
        if y_axis == 'CNLR_MEDIAN':
            ax[i].set_ylim(-2, 2)

    fig.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(f'violin_svtype_{x_axis}_{y_axis}_facets_{ADD_STR_TO_OUTPUT_FILES}.svg')
    plt.close(fig)  # have to close


def get_df_from_list_vcf_files(list_facets_vcf_files: list = None,
                               facets_vcf_directory: str = None) -> tuple:
    """
    Send in a list of vcf files from FACETS and produce one combined Pandas data frame
    :param list_facets_vcf_files: The list of VCF files found in a directory
    :param facets_vcf_directory: Directory where the VCF files are found
    :return: one combined Pandas data frame from all the FACETS Data
    """

    final_list_df = []
    cid_data_per_cval_pair = []
    column_names_cid_data_per_cval_pair = ["cid", "cval1", "cval2", "cva1_cval2", "num_segments",
                                           "fraction_genome_with_loh", "fraction_genome_altered",
                                           "fraction_genome_na"]
    loh_genes = get_loh_genes()
    gene_loss = ['TP53', 'PTEN', 'RB1']

    # go over the list of VCF files
    #list_facets_vcf_files = list_facets_vcf_files[:20]
    for i, vcf_file in enumerate(sorted(list_facets_vcf_files)):
        cid, cval1, cval2 = get_cvals_from_file(file=vcf_file)
        vcf_df = get_df_from_vcf_file(facets_vcf_file=os.path.join(facets_vcf_directory, vcf_file))
        vcf_df['cid'] = cid
        vcf_df['cval1'] = cval1
        vcf_df['cval2'] = cval2
        vcf_df['cval1_cval2'] = "_".join((str(cval1), str(cval2)))
        #vcf_df['TCN_EM'] = vcf_df.apply(lambda row: update_col(row, 'TCN_EM'), axis=1)

        # was there a LOH event with one of these genes?
        gene_loh_bool = []
        for gene in loh_genes:
            column_name = f'{gene}_loh_positive'
            vcf_df[column_name] = \
                vcf_df.apply(lambda row: is_gene_positive(row=row, gene=gene,
                                                          svtype_list=['LOH', 'DUP-LOH', 'HEMIZYG']), axis=1)
            # was there a True LOH found in the current gene column, if so there was an LOH even for this gene
            gene_loh_bool.append(True in vcf_df[column_name].unique())
            if i == 0:  # only do this once
                # append on the name to the columns names list.
                column_names_cid_data_per_cval_pair.append(column_name)

        # was there a LOSS event with one of these genes?
        gene_loss_bool = []
        for gene in gene_loss:
            column_name = f'{gene}_loss_positive'
            vcf_df[column_name] = \
                vcf_df.apply(lambda row: is_gene_positive(row=row, gene=gene,
                                                          svtype_list=['DEL']), axis=1)  # no 'DUP-LOH, 'LOH', 'HEMIZYG'
            # was there a True loss found in the current gene column, if so there was an LOH even for this gene
            gene_loss_bool.append(True in vcf_df[column_name].unique())
            if i == 0:  # only do this once
                # append on the name to the columns names list.
                column_names_cid_data_per_cval_pair.append(column_name)

        # get the fraction numbers
        fraction_genome_na = fraction_genome_with_na(df=vcf_df)
        fraction_genome_with_loh = get_fraction_genome_with_loh(df=vcf_df, sig_digits=3)
        fraction_genome_altered = get_fraction_genome_altered(df=vcf_df, sig_digits=3)

        # add the overall metrics from the cid / cval pairs
        cid_data_per_cval_pair.append([cid, cval1, cval2, f"{cval1}_{cval2}", len(vcf_df), fraction_genome_with_loh,
                                       fraction_genome_altered, fraction_genome_na] + gene_loh_bool + gene_loss_bool)
        # add the vcf entry vcf_df to the list
        final_list_df.append(vcf_df)

    assert len(final_list_df) > 0, "\n\ndid not find any results, did you gunzip the VCF files?\n\n"
    # create the cid / cval pair entry data frame
    overall_fractions_per_cval_pairs_df = pd.DataFrame(cid_data_per_cval_pair,
                                                       columns=column_names_cid_data_per_cval_pair)
    all_df = pd.concat(final_list_df).reset_index(drop=True)
    return all_df, overall_fractions_per_cval_pairs_df


def update_col(row: pd.Series = None, col: str = None):
    if row[col] == '.':
        print("here")
        return -1
    return row[col]

def fraction_genome_with_na(df: pd.DataFrame = None, sig_digits:  int = 6) -> float:
    """
    print out the overall percentage of segments that had a '.' annotation with LCM_EM
    :param df: pandas Dataframe created the FACETS output VCF
    :param sig_digits: Number of significant digits for the fraction of genome with loh
    :return: float representing the fraction of genome that was altered
    """
    #df["SVLEN"] = pd.to_numeric(df["SVLEN"])
    total_sv_len = df['SVLEN'].sum()  # SVLEN is the total length of structural variant
    df_sv_na = df[df['LCN_EM'] == '.']
    total_sv_genome_na = df_sv_na['SVLEN'].sum()
    fraction_genome_na = round((total_sv_genome_na / total_sv_len), sig_digits)
    return fraction_genome_na


def is_gene_positive(row: pd.Series = None, gene: str = None, svtype_list: list = None) -> bool:
    """
    Is the Pandas series contain a gene was event of the type SVTYPE passed in
    :param row: Pandas series
    :param gene: Gene to search for
    :return: Bool
    """
    return True if gene in row['CNV_ANN'] and row['SVTYPE'] in svtype_list else False




def get_cvals_from_file(file: str = None) -> tuple:
    """
    Get the cid and cvals from the file
    :param file: string of the file
    :return: tuple containg the
    """
    match = re.search(r"(cid\d+-\d+)_cval1_(\d+)_cval2_(\d+)", file)
    if match:
        cid = match.group(1)
        cval1 = match.group(2)
        cval2 = match.group(3)
    else:
        raise ValueError("The VCF file did not have the correct format")
    return cid, int(cval1), int(cval2)


def read_vcf(facets_vcf_file: str = None) -> pd.DataFrame:
    """
    Read in the VCF file into a Pandas data frame
    :param facets_vcf_file: string of the VCF file open
    :return: Return a pandas data frame of the VCF, INFO column is not discrete at this point
    """
    with open(facets_vcf_file, 'r') as f:
        lines = [l for l in f if not l.startswith('##')]
    return pd.read_csv(
        io.StringIO(''.join(lines)),
        dtype={'#CHROM': str, 'POS': int, 'ID': str, 'REF': str, 'ALT': str,
               'QUAL': str, 'FILTER': str, 'INFO': str},
        sep='\t'
    ).rename(columns={'#CHROM': 'CHROM'})


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Parse out the FACETS VCF file')
    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('--facets_vcf_file', dest='facets_vcf_file',
                       type=str, help='FACETS VCF file')

    group.add_argument('--facets_vcf_directory', dest='facets_vcf_directory',
                       type=str, help='Directory where to find the VCF files from FACET')

    parser.add_argument('--add_str_to_output_files', dest='add_str_to_output_files', required=True,
                       type=str, help='Add this string to all output files')

    parser.add_argument('--assay_type', dest='assay_type',
                        type=str, help='gs180 or agilent700',
                        required=True, choices=['gs180', 'agilent700'])

    return parser.parse_args()


if __name__ == "__main__":
    main()