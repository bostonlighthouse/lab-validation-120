"""
Standalone python module to call genome level metrics from a FACETS VCF output file:
    i. Fraction of the genome with LOH events (genome-wide LOH) - Float
    ii. Fraction of the genome altered - Float
For more information see the presentation LOH-v11-20221016.pdf:
https://drive.google.com/file/d/1AK1rloi-ZDq3LUGHKuxOiAt05pJInAAB/view?usp=sharing
"""

import argparse
import io
import pandas as pd


def main():
    """
    Main Business Logic
    :return: None
    """

    args = get_cli_args()

    # get the data frame from the facets output
    df = get_df_from_vcf_file(facets_vcf_file=args.facets_vcf_file)

    # get the two metrics
    fraction_genome_altered = get_fraction_genome_altered(df=df)
    fraction_genome_with_loh = get_fraction_genome_with_loh(df=df)

    # print the output
    with(open(args.facets_loh_output_file, "w", encoding="utf8", )) as out_fh:
        print("\t".join(("cid", "fraction_genome_with_loh", "fraction_genome_altered")), file=out_fh)
        print("\t".join((str(x) for x in (args.cid, fraction_genome_with_loh, fraction_genome_altered))), file=out_fh)


def get_df_from_vcf_file(facets_vcf_file: str = None) -> pd.DataFrame:
    """
    :param facets_vcf_file: string of the VCF file open
    :return: Return a pandas data frame of the VCF, with discrete elements for the INFO column
    """
    vcf_df = _read_dataset_and_get_pandas_df(col_vals=_get_columns_from_facets_vcf(),
                                             file_name=facets_vcf_file,
                                             ignore_line_starts_with='##')
    # Parse out the INFO column and create a new data frame that will be merged
    # Browsing a Series is much faster that iterating across the rows of a dataframe.
    vcf_df_updated = pd.DataFrame([dict([x.split('=') for x in t.split(';')])
                                   for t in vcf_df['INFO']], index=vcf_df['ID']).reset_index()

    # update these columns to numeric types
    for k, v in {'SVLEN': int, 'CNLR_MEDIAN': float, 'NHET': int, 'TCN_EM': int}.items():
        # pylint: disable=unsubscriptable-object
        # pylint: disable=unsupported-assignment-operation
        vcf_df_updated[k] = vcf_df_updated[k].astype(v)

    # two data frames should be the same, so just join on ID for the VCF output, since this is unique for one
    # Facets VCF file
    assert len(vcf_df) == len(vcf_df_updated), "data frames were not the same size, and should be"
    final_df = pd.merge(vcf_df, vcf_df_updated, on='ID')
    return final_df


def _get_columns_from_facets_vcf() -> dict:
    """
    :return: dictionary columns (keys) with types (values) from a VCF file for FACETS
    """

    col_vals = {
        "#CHROM": str,
        "POS": int,
        "ID": str,
        "REF": str,
        "ALT": str,
        "QUAL": str,
        "FILTER": str,
        "INFO": str
    }
    return col_vals


def _read_dataset_and_get_pandas_df(col_vals: dict = None, file_name: str = None, keep_default_na: bool = True,
                                    ignore_line_starts_with: str = "#"):
    """
    :param col_vals: Dictionary of column names and types
    :param file_name: File to open
    :param keep_default_na: Boolean to keep the NAs or covert them via Pandas
    :param ignore_line_starts_with: What character to ignore, default #
    Input:  The column values as a dictionary with the key = to the column, and the value = type, e.g. int, str, etc
    Output: pandas data frame
    """

    with open(file_name, 'r', encoding="utf8") as fh:
        lines = [lines for lines in fh if not lines.startswith(ignore_line_starts_with)]  # ignore comments
    # get the dataframe
    df = pd.read_table(
        io.StringIO(''.join(lines)),
        usecols=list(col_vals.keys()),
        dtype=col_vals,  # pass in the dictionary of key values, key is the column, key is the type, e.g. int, float...
        keep_default_na=keep_default_na,
        sep='\t'
    )
    return df


def _get_total_sum_genome(df: pd.DataFrame = None) -> int:
    """
    SVLEN is the total length of structural variant in the data frame so just return that sum
    :param df: pandas Dataframe created the FACETS output VCF
    :return: Int of the total size of the genome
    """

    return df['SVLEN'].sum()  # SVLEN is the total length of structural variant


def get_fraction_genome_altered(df: pd.DataFrame = None, sig_digits: int = 3) -> float:
    """
    The idea here is to sum the total size of all segments (each segment is SVLEN)
    and then divide the total size of segments where the TCN_EM (total diploid copy number) and
    LCN_EM (minor allele copy number) are NOT 2 and 1 respectively by the total size of all segments to
    get the fraction of genome altered.  Remove FILTER==neutral from the numberator
    For more information see the presentation LOH-v11-20221016.pdf:
    https://drive.google.com/file/d/1AK1rloi-ZDq3LUGHKuxOiAt05pJInAAB/view?usp=sharing
    :param df: pandas Dataframe created the FACETS output VCF
    :param sig_digits: Number of significant digits for the fraction of genome with loh
    :return: float representing the fraction of genome that was altered
    """

    total_sv_len = _get_total_sum_genome(df=df)
    # remove any FILTER == neutral since these are events that are always SVTYPE == NEUTR
    # The remaining are all events where the genome was alterd: DEL, DUP, LOH, DUP-LOH, HEMIZYG
    df2 = df[~(df['FILTER'] == 'neutral')]
    # columns both have entries like '.' and other string ints so == like this.  Have removed
    df_sv_altered_genome = df2[(df2['LCN_EM'] != '1') | (df2['TCN_EM'] != '2')]
    total_sv_genome_altered = df_sv_altered_genome['SVLEN'].sum()
    fraction_genome_altered = round((total_sv_genome_altered / total_sv_len), sig_digits)
    return fraction_genome_altered


def get_fraction_genome_with_loh(df: pd.DataFrame = None, sig_digits: int = 3) -> float:
    """
    The idea here is to sum the total size of all segments (each segment is SVLEN)
    and then divide the total size of segments where the LCN = 0 (LCN_EM, minor copy number estimate) by the total size
    of all segments to get the fraction of genome with LOH
    For more information see the presentation LOH-v11-20221016.pdf:
    https://drive.google.com/file/d/1AK1rloi-ZDq3LUGHKuxOiAt05pJInAAB/view?usp=sharing
    :param df: pandas Dataframe created the FACETS output VCF
    :param sig_digits: Number of significant digits for the fraction of genome with loh
    :return: float representing the fraction of genome with LOH
    """

    total_sv_len = _get_total_sum_genome(df=df)
    df_sv_len_lcn_zero = df[df['LCN_EM'] == '0']  # this column has both '.' and other string ints so == like this
    total_sv_len_lcn_zero = df_sv_len_lcn_zero['SVLEN'].sum()
    fraction_genome_with_loh = round((total_sv_len_lcn_zero / total_sv_len), sig_digits)
    return fraction_genome_with_loh


def get_cli_args() -> argparse:
    """
    Get the argparse instance
    Takes: no arguments
    :return: instance of argparse arguments
    """

    parser = argparse.ArgumentParser(description="This caller will take a VCF from FACETS and calculate the "
                                                 "genome-level LOH metrics")
    parser.add_argument('--facets_vcf_file',
                        dest='facets_vcf_file',
                        type=str,
                        help='VCF file from FACETS output.  Used to calculate the genome-level metrics for LOH',
                        required=True)
    parser.add_argument('--facets_loh_output_file',
                        dest='facets_loh_output_file',
                        type=str,
                        help='Output file for the genome-level metrics for LOH',
                        required=True)
    parser.add_argument('--cid',
                        dest='cid',
                        type=str,
                        help='CID from the sample',
                        required=True)

    return parser.parse_args()


if __name__ == '__main__':
    main()
