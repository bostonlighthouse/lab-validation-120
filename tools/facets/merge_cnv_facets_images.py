"""
Module to open a group of cnv_facets images and merge them together for overview analysis

# to run a directory of images, producing one merged image per cid
# files need to have the pattern: cid21-2854_cval1_5_cval2_5_cnv_facets3.cnv.png
# if not, then code will need to be updated
# the BAM files list below has the following format:

# 2022jan10-oc180-run6-b22-1-cid21-2852-snapshot.B22-1.CID21-2852.C525_N701.normal.all_chroms.bam
# 2022jan10-oc180-run6-b22-1-cid21-2853-snapshot.B22-1.CID21-2853.C526_N702.normal.all_chroms.bam
# 2022jan10-oc180-run6-b22-1-cid21-2854-snapshot.B22-1.CID21-2854.C527_N703.normal.all_chroms.bam

pipenv run python3 -m tools.facets.merge_cnv_facets_images \
--facets_vcf_directory directory_to_facets_vcfs \
--bam_files oc180snps-29-sample-normals.txt \
--num_images_per_row 4 \
--final_image_prefix pon_vs_10M-read-all-normals-Rcode-v3

# to run on one cid only, i.e. passing no BAM file list in...

pipenv run python3 -m tools.facets.merge_cnv_facets_images \
--facets_vcf_directory ../scratch/bioinformatics_software/facets/all_samples_15M/ \
--cid cid22-985  \
--num_images_per_row 4 \
--final_image_prefix pon_vs_10M-read-all-normals-Rcode-v3


"""
import argparse
import os
from PIL import Image

from tools.facets.facets_utils import get_facets_cval_steps_gs180, get_facets_cval_steps_gs700
from tools.utils.tools_utils import open_directory_get_list_files, get_cid_from_string


def main() -> None:
    """Business Logic"""
    # get some arguments that will be used
    args = get_cli_args()
    cid = args.cid  # if this was provided at the CLI, then there's only on CID to process
    bam_files = args.bam_files  # if this was provided at the CLI, then there's a file with bam files from cider
    facets_vcf_directory = args.facets_vcf_directory
    num_images_per_row = args.num_images_per_row
    final_image_prefix = args.final_image_prefix
    assay_type = args.assay_type
    # get the list of PNGs to process from the directory passed in at the CLI
    list_png_files = open_directory_get_list_files(directory=facets_vcf_directory, file_extension=".png")
    cids_to_process = []  # use this to store all cids that will be processed

    # if there was a file of bam files to open then get all the cids, else there was just one sent in
    if bam_files:
        cids_to_process = get_list_cids_from_bam_files(file=bam_files)
    else:
        cids_to_process.append(cid)
    for cid in cids_to_process:
        # there are three image types created by cnv_facets, but for now only complete the .cnv.png merging
        for image_type in [".cnv.png"]:  # , ".spider.png", ".cov.png"):
            merge_files(image_type=image_type,
                        cid=cid,
                        list_png_files=list_png_files,
                        directory=facets_vcf_directory,
                        num_images_per_row=num_images_per_row,
                        final_image_prefix=final_image_prefix,
                        assay_type=assay_type)


def get_list_cids_from_bam_files(file: str = None) -> list:
    """
    Open the input file with bam files, and pull out the cids
    :param file: Input file with bam file names from cider
    @return: List of cids
    """
    list_cids = []
    with open(file) as infh:
        for bam_file in infh:
            cid = get_cid_from_string(bam_file)
            if cid:
                list_cids.append(cid)
    assert len(list_cids) > 0, "No CIDS found...."
    return list_cids


def append_images(images: list = None, direction: str = 'horizontal',
                  bg_color: tuple = (255, 255, 255), alignment: str = 'center') -> Image.Image:
    """
    Appends images in horizontal/vertical direction.
    :param images: List of PIL images
    :param direction: direction of concatenation, 'horizontal' or 'vertical'
    :param bg_color: Background color (default: white)
    :param alignment: alignment mode if images need padding; 'left', 'right', 'top', 'bottom', or 'center'
    @return: Concatenated image as a new PIL image object.
    """
    # go over the list of PIL and get tuples for widths and heights
    widths, heights = zip(*(i.size for i in images))
    # Depending on how this should be printed
    if direction == 'horizontal':
        new_width = sum(widths)
        new_height = max(heights)
    else:
        new_width = max(widths)
        new_height = sum(heights)
    # Create a new images that will be printed too
    new_image = Image.new('RGB', (new_width, new_height), color=bg_color)

    offset = 0  # after images
    for im in images:
        if direction == 'horizontal':
            placement = 0
            if alignment == 'center':  # calculate center
                placement = int((new_height - im.size[1]) / 2)
            elif alignment == 'bottom':
                placement = new_height - im.size[1]
            new_image.paste(im, (offset, placement))
            offset += im.size[0]
        else:
            placement = 0
            if alignment == 'center':
                placement = int((new_width - im.size[0]) / 2)
            elif alignment == 'right':
                placement = new_width - im.size[0]
            new_image.paste(im, (placement, offset))
            offset += im.size[1]

    return new_image


def get_lists_of_files_for_image_processing(list_files: list = None,
                                            num_images_per_row: int = None,
                                            assay_type: str = None) -> list:
    """
    Go over and create a 2-D list of file names, based on the num_images_per_row
    :param list_files: List of files to place into the 2-D list
    :param num_images_per_row: How many images per row, i.e. how many files per index in the 2-D list
    :param assay_type: Type of assay to run (this is used to get the cval types)
    :return: 2-D list of files
    """
    final_list_names = []  # the final 2-D list of images that will be merged.
    temp_list_files = []  # used to store images that were found for each row
    # go over all the step values for cval1 and cval2
    cval_function = None
    if assay_type == 'gs180':
        cval_function = get_facets_cval_steps_gs180
    elif assay_type == 'agilent700':
        cval_function = get_facets_cval_steps_gs700

    for i, steps in enumerate(cval_function(), start=1):
        step1, step2 = steps
        # possible to stay None, b/c when facets is run at some combinations of cval1 and cval2 it fails
        file_to_store = None
        # go over all the files and find the ont that matches the steps.  This could be sped up with a dictionary
        # but given the sizes we are dealing with this is not problematic
        for file in list_files:
            # if there was a match, this is the file we want so break
            if f"cval1_{step1}_cval2_{step2}" in file:
                file_to_store = file
                break
        # possible that cnv_facets.R failed. So no file and don't want to store if the file was not found
        if file_to_store is not None:
            temp_list_files.append(file_to_store)  # append on to the temp list

        if i % num_images_per_row == 0:  # we are at the num_images_per_row limit passed in
            copy_files = temp_list_files.copy()  # make a copy, b/c we'll blank out temp
            if len(temp_list_files) > 0:  # could have failed for all cvals steps
                final_list_names.append(copy_files)  # add to the 2-D list to be returned
            temp_list_files = []

    if len(temp_list_files) > 0:  # it's possible we didn't go into the i % num_images_per_row == 0
        final_list_names.append(temp_list_files)

    return final_list_names


def merge_files(image_type: str = '.cnv.png', cid: str = None, list_png_files: list = None,
                directory: str = None, num_images_per_row: int = None,
                final_image_prefix: str = None,
                assay_type: str = None) -> None:
    """
    Get the file type, eg.s. .cnv.png, and merge them
    :param image_type:  .cnv.png or .spider.png
    :param cid: CID to look for
    :param list_png_files: The list of png files to filter down
    :param directory: The path to the images
    :param num_images_per_row: How many images per row, i.e. how many files per index in the 2-D list
    :param final_image_prefix: Prepend the final images with this prefix
    :param assay_type: Type of assay to run (this is used to get the cval types)
    @return: None
    """
    # at this point we have all file types of png, so filter them down to what is needed, i.e. the cid and e.g. .cnv.png
    list_filtered_png_files = [i for i in list_png_files if cid in i and image_type in i]
    assert len(list_filtered_png_files) > 0, "\n\nNo CIDs found in the list of images.  Did the list of BAM files or " \
                                             "the CID passed in have matches in the directory?\n"
    # get the final list of names
    final_list_names = get_lists_of_files_for_image_processing(list_files=list_filtered_png_files,
                                                               num_images_per_row=num_images_per_row,
                                                               assay_type=assay_type)
    final_list_images = []
    for images in final_list_names:  # 2-d list of files
        # add on the directory path
        # then use map to call the Image.open to get a list of PIL images
        # finally append onto the final_list_images in a horizontal way
        final_list_images.append(append_images(list(map(Image.open, [os.path.join(directory, i)
                                                                     for i in images])),
                                               direction='horizontal'))
    # add in all images just created in a vertical manner
    final = append_images(final_list_images, direction='vertical')
    # save the image with this name
    final.save(f"final_combined_{final_image_prefix}_{cid}{image_type}")


def get_cli_args() -> argparse.Namespace:
    """
    Takes: no arguments
    @return: instance of argparse arguments
    """

    parser = argparse.ArgumentParser(description='Parse out the FACETS VCF file')
    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('--cid',
                       dest='cid',
                       type=str,
                       help='CID to merge images fr')

    group.add_argument('--bam_files',
                       dest='bam_files',
                       type=str,
                       help='file to get a list of bam files to parse cids from')

    parser.add_argument('--facets_vcf_directory',
                        dest='facets_vcf_directory',
                        type=str,
                        help='Directory where to find the VCF files from FACET',
                        required=True)

    parser.add_argument('--num_images_per_row',
                        dest='num_images_per_row',
                        type=int,
                        help='How many pictures per row?',
                        default=4)

    parser.add_argument('--final_image_prefix',
                        dest='final_image_prefix',
                        type=str,
                        help='Prepend the final images with this prefix',
                        required=True)

    parser.add_argument('--assay_type',
                        dest='assay_type',
                        type=str,
                        help='gs180 or agilent700',
                        required=True,
                        choices=['gs180', 'agilent700'])

    return parser.parse_args()


if __name__ == '__main__':
    main()
