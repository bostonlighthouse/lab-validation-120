"""
Program to open FACETS VCF files in a directory or a single VCF record and aggregate entries into on output file_name

TO Parse a Single VCF File:

pipenv run python3 -m parse_facets_vcf_standalone
--facets_vcf_file cid22-902_cval1_5_cval2_50.vcf \
--final_output_file test.txt

TO Parse a  VCF Directory:

pipenv run python3 -m parse_facets_vcf_standalone \
--directory 2nd_interation_cvals_using_00-common_gs180snps-10M-reads/ \
--final_output_file test.txt

"""
import argparse
import io
import os
import re
import pandas as pd


def main():
    """Business Logic"""
    args = get_cli_args()
    final_output_file = args.final_output_file
    final_df = None
    # can parse a single VCF file
    if args.facets_vcf_file is not None:
        facets_vcf_file = args.facets_vcf_file
        final_df = return_df_from_vcf_file(facets_vcf_file=facets_vcf_file)
    elif args.facets_vcf_directory is not None:  # will open a directory and parse out all the VCF files
        facets_vcf_directory = args.facets_vcf_directory
        final_df = open_directory_vcf_files(facets_vcf_directory=facets_vcf_directory)
    # print out the final aggregated data frame
    final_df.to_csv(final_output_file, index=False, sep='\t')


def open_directory_vcf_files(facets_vcf_directory: str = None) -> pd.DataFrame:
    """
    Open the directory and scan for the FACETS VCF file_name
    @param facets_vcf_directory: The directory where the VCF files are located
    """

    final_list_df = []
    with os.scandir(facets_vcf_directory) as entries:  # get a generator of the file_name listing
        for entry in entries:  # entry is an object!
            if entry.is_file():
                file_ending = os.path.splitext(entry.name)[1]  # only need the file ending
                if file_ending == '.vcf':  # only care about VCF files
                    # File name should follow the pattern: cid22-905_cval1_30_cval2_45.vcf
                    cid, cval1, cval2 = get_cvals_from_file(file_name=entry.name)
                    df = return_df_from_vcf_file(facets_vcf_file=os.path.join(facets_vcf_directory, entry.name))
                    # add some values to the df
                    df['cid'] = cid
                    df['cval1'] = cval1
                    df['cval2'] = cval2
                    final_list_df.append(df)
    # return the final aggregated data frame
    return pd.concat(final_list_df).reset_index(drop=True)


def get_cvals_from_file(file_name: str = None) -> tuple:
    """
    Simple helper function to pull out the CID and the cval1 and cval2 values for this VCF file_name
    @file_name: File name for the VCF file
    """
    match = re.search(r"(cid\d+-\d+)_cval1_(\d+)_cval2_(\d+)", file_name)
    cid, cval1, cval2 = None, None, None
    if match:
        cid = match.group(1)
        cval1 = match.group(2)
        cval2 = match.group(3)
    else:
        ValueError(f"{file_name} did not follow correct format, e.g. cid22-905_cval1_30_cval2_45.vcf")
    return cid, cval1, cval2


def return_df_from_vcf_file(facets_vcf_file: str = None) -> pd.DataFrame:
    """
    @param facets_vcf_file: string of the VCF file_name open
    @return: Return a pandas data frame of the VCF, with discrete elements for the INFO column
    """
    vcf_df = read_vcf(facets_vcf_file=facets_vcf_file)
    # Parse out the INFO column and create a new data frame that will be merged
    # Browsing a Series is much faster that iterating across the rows of a dataframe.
    vcf_df_updated = pd.DataFrame([dict([x.split('=') for x in t.split(';')])
                                   for t in vcf_df['INFO']], index=vcf_df['ID']).reset_index()
    # two data frames should be the same, so just join on ID for the VCF output, since this is unique for one
    # Facets VCF file_name
    assert len(vcf_df) == len(vcf_df_updated)
    final_df = pd.merge(vcf_df, vcf_df_updated, on='ID')
    return final_df


def read_vcf(facets_vcf_file: str = None) -> pd.DataFrame:
    """
    @param facets_vcf_file: string of the VCF file_name open
    @return: Return a data frame of the VCF, INFO column is not discrete at this point
    """
    with open(facets_vcf_file, 'r') as f:
        lines = [l for l in f if not l.startswith('##')]
    return pd.read_csv(
        io.StringIO(''.join(lines)),
        dtype={'#CHROM': str, 'POS': int, 'ID': str, 'REF': str, 'ALT': str,
               'QUAL': str, 'FILTER': str, 'INFO': str},
        sep='\t'
    ).rename(columns={'#CHROM': 'CHROM'})


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Parse out the FACETS VCF file_name')
    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('--facets_vcf_file', dest='facets_vcf_file',
                       type=str, help='FACETS VCF file_name')

    group.add_argument('--directory', dest='directory',
                       type=str, help='Directory where to find the VCF files from FACET')

    parser.add_argument('--final_output_file', dest='final_output_file',
                        type=str, help='Final output name',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()
