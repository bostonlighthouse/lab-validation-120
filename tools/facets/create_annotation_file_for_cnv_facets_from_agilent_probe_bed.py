"""Update the Agilent probes files for an annotation file for FACETS"""
import argparse
import pandas as pd

from lab_validation_120.utils.configs.lab_validation_config import get_hg19_agilent_700_probe_interval_100bp, \
    get_hg38_agilent_700_probe_interval, get_hg19_agilent_700_probe_interval_100bp_facets_bed_file
from tools.gene_maps.agilent.create_agilent_bed_file_for_cider import update_shared_mapping, \
    update_with_ensembl_information, get_agilent_hybrid_capture_dna_gene_list_from_brochure

def main():
    """Business Logic"""
    # get the hg38 probe file sent to us by agilent
    hg38_df = pd.read_csv(get_hg38_agilent_700_probe_interval(), comment="#",
                          sep="\t", names=['chrom', 'start', 'end', 'gene'])
    # get the hg19 conversion sent to use by agilent
    hg19_df = pd.read_csv(get_hg19_agilent_700_probe_interval_100bp(), comment="#",
                          sep="\t", names=['chrom', 'start', 'end',
                                           'chrom_hg38', 'start_hg38', 'end_hg38',
                                           'shared_mapping', 'score'])
    # update the intervals so the bed and the shared_mapping matchi
    hg19_hg38_df = update_shared_mapping(df1=hg38_df, df2=hg19_df, plus_interval=1)
    # add chr b/c the ensembl queries have always used it
    hg19_hg38_df['chrom'] = hg19_hg38_df.apply(lambda row: "".join(('chr', str(row['chrom']))), axis=1)
    # now get the ensebl data
    update_with_ensembl_information(df=hg19_hg38_df)
    # update back to 1 for chr1
    hg19_hg38_df['chrom'] = hg19_hg38_df['chrom'].str.replace('chr', '')
    # print out the BED file
    hg19_hg38_df.to_excel("test.xlsx")




    print_bed_file_for_facets_gene_annotation(df=hg19_hg38_df)

def print_bed_file_for_facets_gene_annotation(df: pd.DataFrame = None) -> None:
    """
    Print out the gene annotation file for the bed file format FACETS
    :param df: df with the exon level information for each intervals
    :return: NoneType
    """
    file = get_hg19_agilent_700_probe_interval_100bp_facets_bed_file()
    # store some temp data
    tmp_gene = None
    tmp_chr = None
    tmp_starts = []
    tmp_ends = []
    all_genes = []
    with(open(file, "w", encoding='utf-8')) as out_fh:
        for _, row in df.iterrows():
            # get some values from the series
            ds_data = row['ds_data']
            gene = row['gene']
            chrom = row['chrom']
            start = row['start']
            end = row['end']
            # update here.  There genes are on the Agilent Panel
            if gene == 'MSICovered,RPL22':
                gene = 'RPL22'
            elif gene == 'MSICovered,RAD50':
                gene = 'RAD50'
            elif gene == 'MSICovered,QKI':
                gene = 'QKI'
            elif gene == 'CNVBackbone;RAF1':
                gene = 'RAF1'
            elif gene == 'PTEN;CNVBackbone':
                gene = 'PTEN'
            elif gene == 'FGFR3;WHSC1':
                gene = 'FGFR3'
            elif gene == ' WHSC1;FGFR3':
                gene = 'WHSC1'
            elif gene == 'FGF4;CCND1':
                gene = 'FGF4'
            elif gene == 'CCND2;FGF6':
                gene = 'CCND2'
            elif gene == 'SERPINB3;SERPINB4':
                gene = 'SERPINB3'

            # handle situations where there is a CNVBackbone in the gene
            # 1       3598900 3628800 TP73
            # 1       3628800 3638600 CNVBackbone
            # 1       3638600 6241500 TP73
            if gene == 'CNVBackbone':
                if tmp_gene in ds_data.keys(): # in this case, we should see TP73 in ds_data.keys()
                    gene = tmp_gene

            # the ds_data.keys are the genes that were found by Ensembl
            if gene in ds_data.keys() or gene in ('CNVBackbone', 'MSICovered'):
                pass
            else:
                # here Agilent had a gene annotated in their BED file that not part of the gene, b/c it was not in
                # in the ds_data.keys(), so log it and skip printing and updating the tmp variables
                print(f"gene not in dict {gene} -> {ds_data.keys()} : {chrom} {start} {end}")
                continue

            # this was a new gene b/c it was != temp, so print out all the old data from the past gene to the BED file
            if tmp_gene is not None and gene != tmp_gene:
                print("\t".join((tmp_chr, str(min(tmp_starts)), str(max(tmp_ends)), tmp_gene)), file=out_fh)
                # reset the values
                tmp_starts = []
                tmp_ends = []
                if tmp_gene not in all_genes:
                    if tmp_gene not in ('CNVBackbone', 'MSICovered'):
                        all_genes.append(tmp_gene)


            # get the last line of data for testing when I go through the next iteration
            tmp_gene = gene
            tmp_chr = chrom
            tmp_starts.append(start)
            tmp_ends.append(end)

        # the ds_data.keys are the genes that were found by Ensembl
        if tmp_gene in ds_data.keys() or tmp_gene in ('CNVBackbone', 'MSICovered'):
            print("\t".join((tmp_chr, str(min(tmp_starts)), str(max(tmp_ends)), tmp_gene)), file=out_fh)
            all_genes.append(tmp_gene)
        else:
            # here Agilent had a gene annotated in their BED file that not part of the gene, b/c it was not in
            # in the ds_data.keys(), so log it and skip printing and updating the tmp variables
            print(f"gene not in dict {tmp_gene} -> {ds_data.keys()} : {chrom} {start} {end}")

    # get a data from a file found from the brochure
    genes_from_brochure_df = pd.read_csv(get_agilent_hybrid_capture_dna_gene_list_from_brochure(),
                                         comment="#", names=['gene'])

    # get the list of genes from the brochure (note these gene names updated, see the file comments for more info)
    genes_in_brochure_set = set(genes_from_brochure_df['gene'].to_list())
    all_genes_set = set(all_genes)
    print(f"len of genes_in_brochure_set set {len(genes_in_brochure_set)}")
    print(f"len of all_genes_set set {len(all_genes_set)}")
    # test to make sure these are the same
    assert genes_in_brochure_set == all_genes_set, "Sets from brochure and BED file are different"


if __name__ == "__main__":
    main()