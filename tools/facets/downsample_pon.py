import argparse
import pandas as pd
from random import randint
import os

def main():
    args = get_cli_args()
    normal_downsample_file = args.normal_downsample_file
    normal_fastq_path = args.normal_fastq_path
    normal_fastq_files = args.normal_fastq_files
    final_fastq_file_name_base = args.final_fastq_file_name_base

    # get the data for downsampling
    normal_downsample_file_df = pd.read_csv(normal_downsample_file, sep='\t')
    normal_downsample_file_df['CID'] = normal_downsample_file_df['CID'].str.lower()  # get a cid column
    normal_downsample_file_df['rand_int'] = \
        normal_downsample_file_df.apply(lambda x: randint(1, x['downsample']), axis=1)  # used for seqtk seeding
        # keep this the same for each CID, since R1 and R2 need to be seeded

    # get the data for the fastq names
    normal_fastq_names_df = pd.read_csv(normal_fastq_files, header=None, names=['file_name'])
    normal_fastq_names_df['CID'] = normal_fastq_names_df['file_name'].str.extract(r'(CID\d+-\d+)')
    normal_fastq_names_df['CID'] = normal_fastq_names_df['CID'].str.lower()
    # get the final name = there's only 2, one for R1 and one for R2
    normal_fastq_names_df['final_fastq_file_name'] = normal_fastq_names_df.apply(lambda x: final_file_name(x, final_fastq_file_name_base), axis=1)

    # merge them
    merged_df = pd.merge(normal_fastq_names_df, normal_downsample_file_df, on='CID')

    # go over the merge and call seqtk
    for row_index, row in merged_df.iterrows():
        print(f"seqtk sample -s {row['rand_int']} "
              f"{os.path.join(normal_fastq_path, row['file_name'])} "
              f"{row['downsample']} ", end='')
        if row_index < 2:  # first two files
            print(">", end='')
        else:
            print(">>", end='')
        print(f"{row['final_fastq_file_name']};")

    unique_final_files = merged_df['final_fastq_file_name'].unique()
    [print(f'gzip {x}') for x in unique_final_files]

def final_file_name(row, final_fastq_file_name_base):
    #print(row)
    if '_R2_' in row['file_name']:
        return "".join((final_fastq_file_name_base, "_R2_001.fastq"))
    elif '_R1_' in row['file_name']:
        return "".join((final_fastq_file_name_base, "_R1_001.fastq"))
    else:
        raise ValueError(f"do not know this type file_name {row['file_name']}")


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open the PoN sequencing depth file and downsample the FASTQ files')

    parser.add_argument('--normal_fastq_files', dest='normal_fastq_files',
                        type=str, help='Normal FASTQ file to downsample',
                        required=True)
    parser.add_argument('--normal_fastq_path', dest='normal_fastq_path',
                        type=str, help='Path to normal FASTQ files on the docker container',
                        required=True)
    parser.add_argument('--normal_downsample_file', dest='normal_downsample_file',
                        type=str, help='file that contains the CIDs and what to downsample to',
                        required=True)
    parser.add_argument('--final_fastq_file_name_base', dest='final_fastq_file_name_base',
                        type=str, help='Final FASTQ file name base for the final downsampled FASTQ file',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()