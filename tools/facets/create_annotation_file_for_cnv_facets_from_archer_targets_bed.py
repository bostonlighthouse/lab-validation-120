import os
import argparse
import re
from contextlib import redirect_stdout

def main():
    args = get_cli_args()
    with open(args.targets_file) as in_fh:
        for line in in_fh:
            values = line.rstrip().split()
            gene = values[3]  # in teh BED file, the 4th element contains the annotation data
            if ';' in gene:  # get gene from entries like: 'IDH2;NM_001289910.1_Exon_4',
                gene = gene.split(';')[0]
            elif '_' in gene:  # get gene from entries like: MYCL_TTTC or NOTCH2_l159 or rs560681_A_G_0.33726_1111
                gene = gene.split('_')[0]
            # some values will be gene = EGFR but others might equal rs9957901,rs9957908
            # do not split for now, since this is two SNPs in the same interval from Archer
            print("\t".join((values[0], values[1], values[2], gene)))


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Take a targets BED file and parse for annotation file for cnv_facets')

    parser.add_argument('--targets_file', dest='targets_file',
                        type=str, help='Targets BED file that will be parsed for an annotation file for cnv_facets',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()