"""Simple module of helper functions for the FACETS project"""


def get_facets_cval_steps_gs180() -> list:
    """Just return a list of tuples for the cval steps"""
    return [
        (5, 5),
        (5, 10),
        (5, 20),
        (5, 30),
        (10, 10),
        (10, 20),
        (10, 30),
        (10, 40),
        (20, 20),
        (20, 30),
        (20, 40),
        (20, 50),
        (30, 30),
        (30, 40),
        (30, 50),
        (30, 60)
    ]


def get_facets_cval_steps_gs700() -> list:
    """Just return a list of tuples for the cval steps"""
    return [
        (25, 50),
        (25, 75),
        (25, 100),
        (25, 125),
        (50, 100),
        (50, 125),
        (50, 150),
        (50, 175),
        (100, 150),
        (100, 175),
        (100, 200),
        (100, 225),
        (150, 200),
        (150, 225),
        (150, 250),
        (150, 275),
        (200, 250),
        (200, 275),
        (200, 300),
        (200, 325),
        (250, 300),
        (250, 325),
        (250, 350),
        (250, 375),

    ]


