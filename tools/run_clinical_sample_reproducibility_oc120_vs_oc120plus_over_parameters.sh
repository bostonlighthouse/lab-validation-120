
set -e
output_file='lab_validation_120/performance_characteristics/reproducibility/oc120_vs_oc120plus/clinical_sample_reproducibility_oc120_vs_120plus_all_values.txt'
tabular_output_dir='clinical_sample_reproducibility/oc120_vs_oc120plus'
# this turned out to only be 32 samples: clinical_sample_reproducibility_oc120_vs_oc120plus_50samples
tabular_output_file='clinical_sample_reproducibility_oc120_vs_oc120plus_50samples.txt'


for vaf in $(seq 1 1 10) # VAF
do
    for depth in $(seq 50 50 1000)  # DEPTH
    do
     if [ "$vaf" -eq 1 ] && [ "$depth" -eq 50 ]
      then
        # run the first time with the header
        pipenv run python3 \
        -m lab_validation_120.performance_characteristics.reproducibility.clinical_samples.clinical_sample_reproducibility \
        --tabular_output_file $tabular_output_dir/$tabular_output_file \
        --vaf_threshold $vaf \
        --depth_threshold $depth \
        --print_final_output_header \
        --which_oc120 oc120plus \
        >$output_file #2>/dev/null

        # then run with --consequential_impacts_only
        pipenv run python3 \
        -m lab_validation_120.performance_characteristics.reproducibility.clinical_samples.clinical_sample_reproducibility \
        --tabular_output_file $tabular_output_dir/$tabular_output_file \
        --vaf_threshold $vaf \
        --depth_threshold $depth \
        --consequential_impacts_only \
        --which_oc120 oc120plus \
        >>$output_file #2>/dev/null

      else
        pipenv run python3 \
        -m lab_validation_120.performance_characteristics.reproducibility.clinical_samples.clinical_sample_reproducibility \
        --tabular_output_file $tabular_output_dir/$tabular_output_file \
        --vaf_threshold "$vaf" \
        --depth_threshold "$depth" \
        --which_oc120 oc120plus \
        >>$output_file #2>/dev/null

        # then run with --consequential_impacts_only
        pipenv run python3 \
        -m lab_validation_120.performance_characteristics.reproducibility.clinical_samples.clinical_sample_reproducibility \
        --tabular_output_file $tabular_output_dir/$tabular_output_file \
        --vaf_threshold $vaf \
        --depth_threshold $depth \
        --consequential_impacts_only \
        --which_oc120 oc120plus \
        >>$output_file #2>/dev/null

      fi
      echo "done with inner run $vaf $depth"
    done
    echo "done with outer run $vaf"
done
