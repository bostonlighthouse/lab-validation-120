
set -e

# run LOD for INDELS REP1 - Horizon
\time pipenv run python3 -m lab_validation_120.performance_characteristics.limit_of_dectection.limit_of_detection \
--snp_type indels \
--sample_type horizon_rep1_oc120plus


# run LOD for INDELS REP1 - Clinical
\time pipenv run python3 -m lab_validation_120.performance_characteristics.limit_of_dectection.limit_of_detection \
--snp_type indels \
--sample_type clinical_rep1_oc120plus