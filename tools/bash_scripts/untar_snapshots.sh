for a in *.tar.gz
do
    a_dir=`expr $a : '\(.*\).tar.gz'`
    mkdir $a_dir 2>/dev/null
    tar -xzf $a -C $a_dir
done
gunzip -f */*.vcf.gz
rm -rf *.tar.gz