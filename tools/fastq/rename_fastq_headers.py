"""
Take the headers from a set of fastq (R1 and R2) and rename in the other fastq set (R1 and R2)

The CIDER pipeline will not take in the simulated FASTQ files or FASTQ data form another source,
because the FASTQ headers do not conform to the Illumina format used in the pipeline. So the following program will
take a FASTQ data set from our pipeline (R1 and R2 fastq files) and use those headers for the FASTQ dataset (R1 and R2)
requiring the updated headers.

pipenv run python3 -m tools.fastq.rename_fastq_headers \
--fastq_source_r1 ../scratch/fastq/CID21-2351-C506-N706_S6_R1_001.fastq \
--fastq_source_r2 ../scratch/fastq/CID21-2351-C506-N706_S6_R2_001.fastq \
--fastq_target_r1 ../scratch/bioinformatics_software/NEAT/simulated_reads10_ploidy_7_read1.fq \
--fastq_target_r2 ../scratch/bioinformatics_software/NEAT/simulated_reads10_ploidy_7_read2.fq

"""

import argparse
from typing import TextIO
import os

from Bio import SeqIO
import Bio.SeqRecord


def main():
    """
    Go over the input FASTQ files (fastq_source_r1 and fastq_source_r2) and replace the headers in the FASTQ
    files (fastq_target_r1 and fastq_target_r1) with the input FASTQ files (source).
    :return: None
    """
    # get the cli options
    args = get_cli_args()
    fastq_source_r1 = args.fastq_source_r1
    fastq_source_r2 = args.fastq_source_r2
    fastq_target_r1 = args.fastq_target_r1
    fastq_target_r2 = args.fastq_target_r2

    validate_data(fastq_source_r1, fastq_source_r2, fastq_target_r1, fastq_target_r2)
    process_data(fastq_source_r1, fastq_source_r2, fastq_target_r1, fastq_target_r2)


def process_data(fastq_source_r1: str = None,
                 fastq_source_r2: str = None,
                 fastq_target_r1: str = None,
                 fastq_target_r2: str = None) -> None:
    """
    Print the headers from source FASTQ files (R1 and R2) from the into the target FASTQ files (R1 and R2)
    :param fastq_source_r1: Read 1 file from the source FASTQ
    :param fastq_source_r2: Read 2 file from the source FASTQ
    :param fastq_target_r1: Read 1 file from the target FASTQ
    :param fastq_target_r2: Read 2 file from the target FASTQ
    :return: None
    """

    # open SeqIO iterators
    records_fastq_source_r1 = SeqIO.parse(fastq_source_r1, 'fastq')
    records_fastq_source_r2 = SeqIO.parse(fastq_source_r2, 'fastq')
    records_fastq_target_r1 = SeqIO.parse(fastq_target_r1, 'fastq')
    records_fastq_target_r2 = SeqIO.parse(fastq_target_r2, 'fastq')

    # Get two output files for FASTQ data from the input CLI options
    file1, _ = os.path.splitext(fastq_target_r1)
    file2, _ = os.path.splitext(fastq_target_r2)
    outfile1 = f"{file1}_updated_with_bli_headers.fastq"
    outfile2 = f"{file2}_updated_with_bli_headers.fastq"

    # open two output FASTQ files
    with open(outfile1, "w", encoding='utf8') as out_fh1, open(outfile2, "w", encoding='utf8') as out_fh2:
        # loop over the iterators using zip.  Since zip will stop when the smaller input file dies we can do the
        # following to get the records from each of the iterators
        for rec1, rec2, rec3, rec4 in zip(records_fastq_source_r1,
                                          records_fastq_source_r2,
                                          records_fastq_target_r1,
                                          records_fastq_target_r2):
            # print out read1
            print_seq(record1=rec1, record2=rec3, fh=out_fh1)
            # print out read2
            print_seq(record1=rec2, record2=rec4, fh=out_fh2)


def validate_data(fastq_source_r1: str = None,
                  fastq_source_r2: str = None,
                  fastq_target_r1: str = None,
                  fastq_target_r2: str = None) -> None:
    """
    Validate the files input sets (R1 and R2) from the source and target have A). The same number of lines for the set
    B). That the target has fewer lines than the source.
    :param fastq_source_r1: Read 1 file from the source FASTQ
    :param fastq_source_r2: Read 2 file from the source FASTQ
    :param fastq_target_r1: Read 1 file from the target FASTQ
    :param fastq_target_r2: Read 2 file from the target FASTQ
    :return: None
    """

    # find the number of lines
    num_lines_fastq_source_r1 = get_num_lines_in_file(file=fastq_source_r1)
    num_lines_fastq_source_r2 = get_num_lines_in_file(file=fastq_source_r2)
    num_lines_fastq_target_r1 = get_num_lines_in_file(file=fastq_target_r1)
    num_lines_fastq_target_r2 = get_num_lines_in_file(file=fastq_target_r2)

    if num_lines_fastq_source_r1 != num_lines_fastq_source_r2:
        raise ValueError(f"Number of lines in input1 {fastq_source_r1} {num_lines_fastq_source_r1}, "
                         f"did not match "
                         f"Number of lines in input2 {fastq_source_r2} {num_lines_fastq_source_r2}")

    if num_lines_fastq_source_r1 != num_lines_fastq_source_r2:
        raise ValueError(f"Number of lines in input3 {fastq_target_r1} {num_lines_fastq_target_r1}, "
                         f"did not match "
                         f"Number of lines in input4 {fastq_target_r2} {num_lines_fastq_target_r2}")
    if num_lines_fastq_source_r1 < num_lines_fastq_target_r1:
        raise ValueError("Number of lines in the FASTQ files to be replaced (target) is more than the source "
                         "FASTQ file.\n"
                         "The source FASTQ files must have more entries in order to rename all header files in the "
                         "target")


def get_num_lines_in_file(file: str = None) -> int:
    """
    Return the number of lines in a file
    :param file: File to open and count lines
    :return: int of the number of lines
    """
    i = 0
    with open(file, encoding='utf8') as infh:
        for _ in infh:
            i = i + 1
    return i


def print_seq(record1: Bio.SeqRecord = None, record2: Bio.SeqRecord = None, fh: TextIO = None) -> None:
    """
    Take the record1 object (source) and print out the header for the record2 object (target)
    :param record1: Bio.SeqRecord #1 source
    :param record2: Bio.SeqRecord #2 target
    :param fh: Filehandle to write too.
    :return:
    """
    # updated the ID
    record2.id = record1.id
    # update the description
    record2.description = record1.description
    # print out the entry, do not include a new line character, b/c the method format does this already
    print(record2.format("fastq"), file=fh, end='')


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description="Take two FASTQ files (source R1 & R2) and replace the headers "
                                                 "in other the FASTQ files (target). "
                                                 "Keeping the sequence and quality data in the target files")

    parser.add_argument('--fastq_source_r1', dest='fastq_source_r1',
                        type=str, help='Headers to use from the source Read 1 FASTQ', required=True)
    parser.add_argument('--fastq_source_r2', dest='fastq_source_r2',
                        type=str, help='Headers to use from the source Read 2 FASTQ', required=True)

    parser.add_argument('--fastq_target_r1', dest='fastq_target_r1',
                        type=str, help='Headers to be replaced in Read 2 FASTQ', required=True)
    parser.add_argument('--fastq_target_r2', dest='fastq_target_r2',
                        type=str, help='Headers to be replaced in Read 2 FASTQ', required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()
