#!/usr/bin/env python

"""
Demonstrates plotting chromosome ideograms and snps or genes (or any features, really)
using matplotlib.

1) Assumes a file from UCSC's Table Browser from the "cytoBandIdeo" table,
saved as "ideogram.txt". Lines look like this::

    #chrom  chromStart  chromEnd  name    gieStain
    chr1    0           2300000   p36.33  gneg
    chr1    2300000     5300000   p36.32  gpos25
    chr1    5300000     7100000   p36.31  gneg

2) Assumes another file, "ucsc_genes.txt", which is a BED format file
   downloaded from UCSC's Table Browser. This script will work with any
   BED-format file.

"""

from matplotlib import pyplot as plt
from matplotlib.collections import BrokenBarHCollection
import pandas


# Here's the function that we'll call for each dataframe (once for chromosome
# ideograms, once for snps, and once for genes).  The rest of this script will be prepping data
# for input to this function
#
def chromosome_collections(df, y_positions, height,  **kwargs):
    """

    Yields BrokenBarHCollection of features that can be added to an Axes
    object.

    Parameters
    ----------

    df : pandas.DataFrame
        Must at least have columns ['chrom', 'start', 'end', 'color']. If no
        column 'width', it will be calculated from start/end.

    y_positions : dict
        Keys are chromosomes, values are y-value at which to anchor the
        BrokenBarHCollection

    height : float
        Height of each BrokenBarHCollection

    Additional kwargs are passed to BrokenBarHCollection
    """
    del_width = False
    if 'width' not in df.columns:
        del_width = True
        df['width'] = df['end'] - df['start']
    for chrom, group in df.groupby('chrom'):
        yrange = (y_positions[chrom], height)
        xranges = group[['start', 'width']].values
        yield BrokenBarHCollection(
            xranges, yrange, facecolors=group['colors'], **kwargs)
    if del_width:
        del df['width']


# Height of each ideogram
chrom_height = 2

# Spacing between consecutive ideograms
chrom_spacing = 2.5

# Height of the snp track. Should be smaller than `chrom_spacing` in order to
# fit correctly
snp_height = .8
gene_height = snp_height

# Padding between the top of a snp track and its corresponding ideogram
snp_padding = 0.1
# Padding between the top of a gene track and its corresponding ideogram
gene_padding = 1

# Width, height (in inches)
figsize = (24, 20)

# Decide which chromosomes to use
chromosome_list = ['chr%s' % i for i in list(range(1, 23)) + ['X']]

# Keep track of the y positions for ideograms and snp for each chromosome,
# and the center of each ideogram (which is where we'll put the ytick labels)
ybase = 0
chrom_ybase = {}
snp_ybase = {}
gene_ybase = {}
chrom_centers = {}

# Iterate in reverse so that items in the beginning of `chromosome_list` will
# appear at the top of the plot
for chrom in chromosome_list[::-1]:
    chrom_ybase[chrom] = ybase
    chrom_centers[chrom] = ybase + chrom_height / 2.
    snp_ybase[chrom] = ybase - snp_height - snp_padding
    gene_ybase[chrom] = ybase - gene_height - gene_padding
    ybase += chrom_height + chrom_spacing

# Read in ideogram.txt, downloaded from UCSC Table Browser
ideo = pandas.read_table(
    'scratch/SNPs/cytoBandIdeo.txt',
    skiprows=1,
    names=['chrom', 'start', 'end', 'name', 'gieStain']
)

# Filter out chromosomes not in our list
ideo = ideo[ideo.chrom.apply(lambda x: x in chromosome_list)]

# Add a new column for width
ideo['width'] = ideo.end - ideo.start

# Colors for different chromosome stains
# https://groups.google.com/a/soe.ucsc.edu/g/genome/c/Lymdl_dJvYQ?pli=1
color_lookup = {
    # The first 5 in the list below
    # reflect the level of color obtained via Giemsa staining"
    # colors see here: https://www.tug.org/pracjourn/2007-4/walden/color.pdf
    'gneg': (1.0, 1.0, 1.0),
    'gpos25': (0.6, 0.6, 0.6),
    'gpos50': (0.4, 0.4, 0.4),
    'gpos75': (0.2, 0.2, 0.2),
    'gpos100': (0.0, 0.0, 0.0),
    #'acen': (.8, .4, .4),  # centromeric
    'acen': "#9cff19",
    'gvar': (0.8, 0.8, 0.8), # "gvar" bands tend to be heterochomatin, either pericentric or telomeric.
    'stalk': (0.9, 0.9, 0.9), # stalk" refers to the short arm of acrocentric chromosomes chr13, 14, 15, 21, 22;
}

# Add a new column for colors
ideo['colors'] = ideo['gieStain'].apply(lambda x: color_lookup[x])

# Same thing for snps
# grep -E '\trs' --colour=never\
# lab_validation_120/internal_data_sources/bed_files_for_assays/19638_VariantPlex_GS180v3_Additions_for_OC_Review_primers_targets.bed \
# >gs_180_new_updates_rs_positions.txt
snps = pandas.read_table(
    'scratch/SNPs/gs_180_new_updates_rs_positions.txt',
    names=['chrom', 'start', 'end', 'name'],
    usecols=range(4))
snps = snps[snps.chrom.apply(lambda x: x in chromosome_list)]
snps['width'] = (snps.end - snps.start) + 100_000  # adding here to make it actually show up on the map
snps['colors'] = '#2243a8'

# grep -E -v '\trs' --colour=never \
# lab_validation_120/internal_data_sources/bed_files_for_assays/VariantPlex_OC_v3_16897_primers_targets.sorted.bed  \
# lab_validation_120/internal_data_sources/bed_files_for_assays/19638_VariantPlex_GS180v3_Additions_for_OC_Review_primers_targets.bed \
# >gs_180_no_rs_positions.txt
genes = pandas.read_table(
    'scratch/SNPs/gs_180_no_rs_positions.txt',
    names=['chrom', 'start', 'end', 'name'],
    usecols=range(4))
genes = genes[genes.chrom.apply(lambda x: x in chromosome_list)]
genes['width'] = (genes.end - genes.start) + 100_000
genes['colors'] = '#050505'

fig = plt.figure(figsize=figsize)
ax = fig.add_subplot(111)
ax.set(facecolor='#dcdcdc')

# Now all we have to do is call our function for the ideogram data...
print("adding ideograms...")
for collection in chromosome_collections(ideo, chrom_ybase, chrom_height):
    ax.add_collection(collection)

# ...and the SNP data
print("adding SNPs...")
for collection in chromosome_collections(snps, snp_ybase, snp_height, alpha=.9, linewidths=0):
    ax.add_collection(collection)

# ...and the Gene Panel data
print("adding Gene Panel Data...")
for collection in chromosome_collections(genes, gene_ybase, gene_height, linewidths=0):
    ax.add_collection(collection)

# Axes tweaking
ax.set_yticks([chrom_centers[i] for i in chromosome_list])
ax.set_yticklabels(chromosome_list)
ax.axis('tight')
plt.savefig("test.svg")
plt.close(fig)  # have to close