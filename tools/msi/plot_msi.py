import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from lab_validation_120.utils.plots.plot_utilities import global_rc_set

# set the size of the plots
global_rc_set()
plt.rcParams['figure.figsize'] = (10.0, 20.0)
gs = gridspec.GridSpec(3, 1)  # 3 rows, 2 columns
# create the axes subplots
fig = plt.figure()
ax0_0 = fig.add_subplot(gs[0])
ax0_1 = fig.add_subplot(gs[1])
ax0_2 = fig.add_subplot(gs[2])

# read the data in
msi_data = pd.read_csv("tools/msi/MSI_validacao_MCC_gerater_equal_60_PCR_confirmed_BLI-20220106.txt", sep="\t")
order=['GS120_MSI-H', 'GS180_MSI-H', 'GS120_MSS', 'GS180_MSS']

# first plot: MSI Score
sns.stripplot(x="ASSAY_CATEGORY", y="MSI SCORE", data=msi_data, ax=ax0_0, order=order)
ax0_0.axes.set_title("Overall MSI Scores", fontsize=16)
ax0_0.set_xlabel("Assay_MSI Status", fontsize=14)
ax0_0.set_ylabel("MSI Score % (total somatic sites / total number of sites)",  fontsize=14)
ax0_0.tick_params(labelsize=10)
# horizontal lines
line_at = 30
ax0_0.axhline(line_at, ls='--', color='red')
plt.setp(ax0_0, yticks=list(range(0, 105, 5)))

# second plot: Total Number of Sites
sns.stripplot(x="ASSAY_CATEGORY", y="Total_Number_of_Sites", data=msi_data, ax=ax0_1, order=order)
ax0_1.axes.set_title("Total Number of Sites", fontsize=16)
ax0_0.set_xlabel("Assay_MSI Status", fontsize=14)
ax0_1.set_ylabel("Total Number of Sites",  fontsize=14)
ax0_1.tick_params(labelsize=10)
plt.setp(ax0_1, yticks=list(range(0, 70, 5)))

# Third plot Number of Somatic Sites
sns.stripplot(x="ASSAY_CATEGORY", y="Number_of_Somatic_Sites", data=msi_data, ax=ax0_2, order=order)
ax0_2.axes.set_title("Number of Somatic Sites", fontsize=16)
ax0_0.set_xlabel("Assay_MSI Status", fontsize=14)
ax0_2.set_ylabel("Number of Somatic Sites",  fontsize=14)
ax0_2.tick_params(labelsize=10)
plt.setp(ax0_2, yticks=list(range(0, 70, 5)))

# output file name
fig.tight_layout()
plot_file_name = f"tools/msi/MSI-Score-threshold-{line_at}.svg"
plt.savefig(plot_file_name)
plt.close(fig)  # have to close
