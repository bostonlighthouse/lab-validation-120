"""Test script showing the importing of data from gs-v2.0-gene-list-v18-20201208.jsonle_gs_v2.py"""

import json

file = 'scratch/gs-v2.0-gene-list-v19-20201208.json'
with open(file) as infh:
    final_map = json.load(infh)

at_least_one_clinical_match = 0  # look for genes with at least one clinical match in snv, cnv, or fusion
at_least_one_clinical_match_gene_list = []  # store the genes from above
at_least_one_technical_match = 0  # look for genes with at least one technical match in snv, cnv, or fusion
at_least_one_clinical_match_snv_cnv = 0  # how many were technically cnv and snv, since this is snapshot assay
at_least_one_technical_match_gene_list_snv_cnv = []
at_least_one_clinical_match_snv = 0  # how many were technically cnv and snv, since this is snapshot assay
at_least_one_technical_match_gene_list_snv = []
at_least_one_clinical_match_cnv = 0  # how many were technically cnv and snv, since this is snapshot assay
at_least_one_technical_match_gene_list_cnv = []

at_least_one_technical_match_gene_list = []  # store the genes from above
all_snv_fusion_cvn_clinical_match_inclusive = 0  # look for genes
all_snv_fusion_cvn_clinical_match_gene_list_inclusive = []
non_clinical_match = 0  # no clinical match in snv, cnv, or fusion
non_technical_match = 0  # no technical match in snv, cnv, or fusion
non_clinical_technical_match = 0  # genes that did not have a clinical match or technical match
clinical_snv_match = 0  # at least matched snv clinical
clinical_cnv_match = 0  # at least matched snv clinical
clinical_fusion_match = 0  # at least matched snv clinical

cnv_match = []
for gene in final_map['data']:
    # how many genes had at least one clinical match in the snv, cnv, or fusion
    # these would be genes that we'd want formatted in some capacity on the PDF, and also to be part of the VV filter
    # As for SNV sign-out, at VV we'll just want the ones for ['snv_meta_data']['is_clinical']
    if final_map['data'][gene]['cnv_meta_data']['is_clinical'] is True or \
            final_map['data'][gene]['fusion_meta_data']['is_clinical'] is True or \
            final_map['data'][gene]['snv_meta_data']['is_clinical'] is True:
        at_least_one_clinical_match += 1
        at_least_one_clinical_match_gene_list.append(gene)

    # how many genes had at least one is_technical match in the snv, cnv, or fusion
    # these are gene that have the technical flag denoted, meaning they could be eventually turned on
    if final_map['data'][gene]['cnv_meta_data']['is_technical'] is True or \
            final_map['data'][gene]['fusion_meta_data']['is_technical'] is True or \
            final_map['data'][gene]['snv_meta_data']['is_technical'] is True:
        at_least_one_technical_match += 1
        at_least_one_technical_match_gene_list.append(gene)

    # how many were technically cnv and snv, since this is snapshot assay
    if final_map['data'][gene]['cnv_meta_data']['is_technical'] is True or \
            final_map['data'][gene]['snv_meta_data']['is_clinical'] is True:
        at_least_one_clinical_match_snv_cnv += 1
        at_least_one_technical_match_gene_list_snv_cnv.append(gene)

    # how many were technically cnv and snv, since this is snapshot assay
    if final_map['data'][gene]['cnv_meta_data']['is_technical'] is True:
        at_least_one_clinical_match_cnv += 1
        at_least_one_technical_match_gene_list_cnv.append(gene)

    # how many were technically cnv and snv, since this is snapshot assay
    if final_map['data'][gene]['snv_meta_data']['is_technical'] is True:
        at_least_one_clinical_match_snv += 1
        at_least_one_technical_match_gene_list_snv.append(gene)


    # how many genes were matches to snv, cnv, and fusions (inclusive)
    if final_map['data'][gene]['cnv_meta_data']['is_clinical'] is True and \
            final_map['data'][gene]['fusion_meta_data']['is_clinical'] is True and \
            final_map['data'][gene]['snv_meta_data']['is_clinical'] is True:
        all_snv_fusion_cvn_clinical_match_inclusive += 1
        all_snv_fusion_cvn_clinical_match_gene_list_inclusive.append(gene)

    # how many genes were not clinical at all
    if final_map['data'][gene]['cnv_meta_data']['is_clinical'] is False and \
            final_map['data'][gene]['fusion_meta_data']['is_clinical'] is False and \
            final_map['data'][gene]['snv_meta_data']['is_clinical'] is False:
        non_clinical_match += 1

    # how many genes were not technical at all
    if final_map['data'][gene]['cnv_meta_data']['is_technical'] is False and \
            final_map['data'][gene]['fusion_meta_data']['is_technical'] is False and \
            final_map['data'][gene]['snv_meta_data']['is_technical'] is False:
        non_technical_match += 1

    # how many genes were not clinical or technical at all
    if final_map['data'][gene]['cnv_meta_data']['is_clinical'] is False and \
            final_map['data'][gene]['fusion_meta_data']['is_clinical'] is False and \
            final_map['data'][gene]['snv_meta_data']['is_clinical'] is False and \
            final_map['data'][gene]['cnv_meta_data']['is_technical'] is False and \
            final_map['data'][gene]['fusion_meta_data']['is_technical'] is False and \
            final_map['data'][gene]['snv_meta_data']['is_technical'] is False:
        non_clinical_technical_match += 1

    # store how many genes are clinical for each cnv, fusion, and snv
    if final_map['data'][gene]['snv_meta_data']['is_clinical'] is True:
        clinical_snv_match += 1
    if final_map['data'][gene]['cnv_meta_data']['is_clinical'] is True:
        clinical_cnv_match += 1
        cnv_match.append(gene)
    if final_map['data'][gene]['fusion_meta_data']['is_clinical'] is True:
        clinical_fusion_match += 1

assert len(final_map['data'].keys()) == 217, "The total number of genes is 217"
assert at_least_one_clinical_match == 146, "The total number of clinical genes is 146"
assert at_least_one_technical_match == 189, "The total number of technical genes is 189"
assert non_clinical_match == 71, "The total number of non clinical genes is 71"
assert non_clinical_technical_match == 28, "The total number of non clinical and non technical genes is 28"
assert clinical_snv_match == 105, "The total number of clinical snv genes is 105"
assert clinical_cnv_match == 13, "The total number of clinical cnv genes is 13"
assert clinical_fusion_match == 57, "The total number of clinical fusion genes is 57"
assert at_least_one_clinical_match_snv_cnv == 148, "The total number of technical snv and fusion is 148"
assert at_least_one_clinical_match_snv == 143, "The total number of technical snv and fusion is 143"
assert at_least_one_clinical_match_cnv == 144, "The total number of technical cnv and fusion is 144"


print(f"at_least_one_clinical_match_snv_cnv = {at_least_one_clinical_match_snv_cnv}")
print(f"at_least_one_clinical_match_snv = {at_least_one_clinical_match_snv}")
print(f"at_least_one_clinical_match_cnv = {at_least_one_clinical_match_cnv}")

#[print(i) for i in sorted(at_least_one_technical_match_gene_list_snv)]

#for gene in sorted(at_least_one_technical_match_gene_list):
#    print(gene)

