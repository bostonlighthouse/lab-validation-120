import argparse
from lab_validation_120.utils.vcf_utilities import get_jax_protein_change


def main():
    args = get_cli_args()
    print("\t".join(("Chromosome", "Gene", "Variant", "VAF", "Variant_Type")))
    with open(args.vv_input_file) as infh:
        header = infh.readline()
        #print(header)
        for line in infh:
            values = line.rstrip().split("\t")
            if values[7] == 'null':  # skip the hgvps where it's null
                continue
            elif values[-14] == 'vargrouper':  # ignore when primary caller is vargrouper
                continue
            (gene, hgvsp, chrom, start, end, vaf, variant_class) = get_values(values)
            print("\t".join((chrom, gene, hgvsp, vaf, variant_class)))
            #break

def get_values(values):
    gene = values[6]
    hgvsp = (get_jax_protein_change(values[7].split(":")[1])).replace('p.', '')
    # the file format I use does not include p.
    chrom = f"chr.{values[-6]}"
    start = values[-5]
    end = values[-4]
    vaf = str(round((float(values[11]) * 100), 1))
    variant_class = values[1]
    if variant_class in ("SNV", "substitution"):
        variant_class = 'SNV'
    elif variant_class in ("insertion"):
        variant_class = 'INS'
    elif variant_class in ("deletion", "indel"):
        variant_class = 'DEL'
    else:
        raise ValueError(f"Don't know this variant_class {variant_class}")
    if hgvsp[-1] == '=':
        hgvsp = hgvsp[:-1] + '%3D'  # update hgvsp the way VEP 104 VCF files have them

    return (gene, hgvsp, chrom, start, end, vaf, variant_class)

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Take VV input and ouput the variant file for LOD study')

    parser.add_argument('-v', '--vv_input_file', dest='vv_input_file',
                        type=str, help='VV Output file', required=True)


    return parser.parse_args()


if __name__ == "__main__":
    main()
