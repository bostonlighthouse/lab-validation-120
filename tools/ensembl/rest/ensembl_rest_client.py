""" Example Ensemble Client"""

import json
import time
import sys
from urllib.parse import urlencode
from urllib.request import urlopen, Request
from urllib.error import HTTPError


class EnsemblRestClient():
    """Class to store the values returned by the REST Ensemble API"""
    def __init__(self, server='https://grch37.rest.ensembl.org', reqs_per_sec=15, store_docs=False):
        self.server = server
        self.reqs_per_sec = reqs_per_sec
        self.req_count = 0
        self.last_req = 0
        self.store_docs = store_docs

    def perform_rest_action(self, endpoint, headers=None, params=None):
        """
        Does the actual downloading of the data, and testing if we've hit the rate limit
        :param endpoint: What endpoint to hit
        :param headers: What headers to use
        :param params: What parameters to pass
        :return: The JSON object that was Deserialize into a python data structure
        """
        if headers is None:
            headers = {}

        if 'Content-Type' not in headers:
            headers['Content-Type'] = 'application/json'

        if params:
            endpoint += '?' + urlencode(params)

        data = None

        # check if we need to rate limit ourselves
        if self.req_count >= self.reqs_per_sec:
            delta = time.time() - self.last_req
            if delta < 1:
                time.sleep(1 - delta)
            self.last_req = time.time()
            self.req_count = 0

        try:
            request = Request(self.server + endpoint, headers=headers)
            response = urlopen(request)
            content = response.read()
            # was there content, in JSON at this point
            if content:
                # Deserialize s (a str, bytes or bytearray instance containing a JSON document)
                # to a Python object
                data = json.loads(content)
            self.req_count += 1

        except HTTPError as err:
            # see codes here https://github.com/Ensembl/ensembl-rest/wiki/HTTP-Response-Codes
            # 429 is:
            # You have been rate-limited; wait and retry. The headers X-RateLimit-Reset, X-RateLimit-Limit
            # and X-RateLimit-Remaining will inform you of how long you have until your limit is reset and what
            # that limit was. If you get this response and have not exceeded your limit then check if you have made
            # too many requests per second.
            if err.code == 429:
                if 'Retry-After' in err.headers:
                    # Retry-After sent back when we hit a 429 error and exceed the rate limit.
                    # Client should wait for this period before sending another request
                    retry = err.headers['Retry-After']
                    time.sleep(float(retry))
                    self.perform_rest_action(endpoint, headers, params)
            else:
                sys.stderr.write(
                    'Request failed for {0}: Status code: {1.code} Reason: {1.reason}\n'.format(endpoint, err))

        return data
