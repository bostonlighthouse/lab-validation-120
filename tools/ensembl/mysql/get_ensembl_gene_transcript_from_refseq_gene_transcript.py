"""
Program to take in the Ref Seq Accession and get the Ensembl version for GRCH37 Via the Ensembl MYSQL instance
Located at ensembldb.ensembl.org, via the DB homo_sapiens_core_75_37
"""
import argparse
import logging
import sys
import pandas as pd
import pymysql


def main():
    """Business Logic"""
    args = get_cli_args()
    database = 'homo_sapiens_core_75_37'
    db = get_pymysql_connection(database=database)
    gene_to_refseq_df = pd.read_csv(args.gene_to_ref_seq, sep='\t')
    required_col_ref_seq = 'ref_seq'
    required_col_gene = 'gene'

    assert required_col_ref_seq in gene_to_refseq_df, f"Required column {required_col_ref_seq} not present in " \
                                                      f"{args.gene_to_ref_seq}"

    assert required_col_gene in gene_to_refseq_df, f"Required column {required_col_gene} not present in " \
                                                   f"{args.gene_to_ref_seq}"

    # print out an output file
    with open(f"gene-mapping-{database.replace('_', '-')}.txt", "w", encoding="utf-8") as out_fh:
        # print out the header
        print("\t".join(("gene", "ref_seq_from_input", "ref_seq", "ensembl")), file=out_fh)
        # loop over each gene and get the Ensembl ID, the version and the RefSeq Accession found (based on version)
        for _, row in gene_to_refseq_df.iterrows():
            ref_seq_acc = row[required_col_ref_seq]
            gene = row[required_col_gene]
            ensembl, version, ref_seq_acc_found = \
                query_to_get_ensembl_id_from_refseq_accession(pymysql_connection=db, ref_seq_acc=ref_seq_acc, gene=gene)
            print("\t".join((gene, ref_seq_acc, ref_seq_acc_found, ".".join((ensembl, str(version))))), file=out_fh)

    db.close()


def get_pymysql_connection(database: str = 'homo_sapiens_core_75_37', host: str = 'ensembldb.ensembl.org',
                           user: str = "anonymous", password: str = "") -> pymysql.connections.Connection:
    """
    Get a
    :param database:  DB to connect
    :param host: host
    :param user: user name
    :param password: password if need
    :return: return a pymysql connection
    """
    return pymysql.connect(host=host, user=user, password=password, database=database)  # core version 75, GRCh = 37


def query_to_get_ensembl_id_from_refseq_accession(pymysql_connection: pymysql.connections.Connection, ref_seq_acc: str,
                                                  gene: str) -> tuple:

    """
    Query the EnsemblData base and get back the Ensembl ID associated with the RefSeq ID.  The RefSeq ID used might not
    be the correct version so don't use an version and instead query with LIKE
    :param pymysql_connection:  Connection to query
    :param ref_seq_acc:  Ref Seq Accession to get the Ensemble ID for
    :param gene: Gene searched for
    :return: tuple of data
    """
    cursor = pymysql_connection.cursor()
    if '.' in ref_seq_acc:
        ref_seq_acc, _ = ref_seq_acc.split('.')  # split off the version, since we don't need it
    # possible for the version to not be the one in Ensembl, so use a LIKE
    query = "SELECT transcript.stable_id, transcript.version, xref.display_label " \
            "FROM transcript, object_xref, xref,external_db " \
            "WHERE transcript.transcript_id = object_xref.ensembl_id " \
            "AND object_xref.ensembl_object_type = 'Transcript' " \
            "AND object_xref.xref_id = xref.xref_id " \
            "AND xref.external_db_id = external_db.external_db_id " \
            f"AND external_db.db_name = 'RefSeq_mRNA' and display_label LIKE '{ref_seq_acc}%';"
    cursor.execute(query)
    results = cursor.fetchall()
    assert len(results) < 2, f"Results are not == 1 {results}"  # there should not be multiple ensembl transcripts
    cursor.close()

    if len(results) == 1:  # should be a transcript, else it was not found
        return results[0]
    print(f"No Ensembl ID found for RefSeq Accession {ref_seq_acc}, gene {gene}", file=sys.stderr)
    return '', '', ''


def query_to_get_refseq_accession_from_ensembl_id(pymysql_connection: pymysql.connections.Connection, ensembl_id: str,
                                                  gene: str) -> tuple:

    """
    Query the Ensembl Database and get back the RefSeq Accession associated with the Ensembl ID.
    :param pymysql_connection:  Connection to query
    :param ensembl_id:  Ensemble ID to get the Ref Seq Accession for
    :param gene: Gene searched for
    :return: tuple of data
    """
    cursor = pymysql_connection.cursor()
    if '.' in ensembl_id:
        ensembl_id, _ = ensembl_id.split('.')  # split off the version, since we don't need it
    # possible for the version to not be the one in Ensembl, so use a LIKE
    # get either RefSeq_mRNA RefSeq_mRNA_predicted
    query = "SELECT transcript.stable_id, transcript.version, xref.display_label, external_db.db_name " \
            "FROM transcript, object_xref, xref,external_db " \
            "WHERE transcript.transcript_id = object_xref.ensembl_id " \
            "AND object_xref.ensembl_object_type = 'Transcript' " \
            "AND object_xref.xref_id = xref.xref_id " \
            "AND xref.external_db_id = external_db.external_db_id " \
            f"AND (external_db.db_name = 'RefSeq_mRNA' OR external_db.db_name = 'RefSeq_mRNA_predicted') " \
            f"AND stable_id ='{ensembl_id}';"
    logging.debug(query)
    cursor.execute(query)
    results = cursor.fetchall()
    cursor.close()
    return results



def get_cli_args() -> argparse.Namespace:
    """
    Get the command line options
    :return: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open a Gene and RefSeq file and product Ensembl Transcripts based '
                                                 'on the RefSeq Transcript based on Esembl MYSQL DB: '
                                                 'homo_sapiens_core_75_37')

    parser.add_argument('--gene_to_ref_seq', dest='gene_to_ref_seq',
                        type=str, help='Gene and RefSeq file.  Gene column = "gene" '
                                       'RefSeq colunm = "ref_seq".  Both are required',
                        required=True)

    return parser.parse_args()


if __name__ == '__main__':
    main()
