""""Analyze a batch of HTQC files"""
import argparse
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from lab_validation_120.utils.htqc_utilites import get_htqc_summary_df_from_directory_files
from tools.agilent700.analyze_hsmetrcis_outputs import SAMPLE_COL
from tools.utils.tools_utils import get_cid_from_string


def main():
    """Business Logic"""
    args = get_cli_args()
    assay_type = args.assay_type
    # what directory to to get the data
    if assay_type == 'gs180':
        directory = 'scratch/htqc/oc180/met/'
    else:  # choices are used for args.assay_type
        directory = 'scratch/htqc/oc395/met/'

    # get the HTQC df from the list of files
    htqc_summary_df = get_htqc_summary_df_from_directory_files(directory=directory, file_glob="*output_summary_met.txt",
                                                       sample_col=SAMPLE_COL, is_summary=True)
    htqc_summary_df = htqc_summary_df.sort_values(by='MEAN_COLLAPSED_COVERAGE', ascending=False)
    # get the summary data for HTQC
    htqc_df = get_htqc_summary_df_from_directory_files(directory=directory, file_glob="*output_met.txt",
                                                       sample_col=SAMPLE_COL, is_summary=False)
    # create a cid name
    htqc_df['cid'] = htqc_df.apply(lambda row: get_cid_from_string(row[SAMPLE_COL]), axis=1)
    # this is the name that will be plotted on the xaxis
    htqc_df['chr_start_stop'] = htqc_df.apply(lambda row: "_".join((str(row['CHROM']), str(row['START']), str(row['END']))), axis=1)
    plot_data(htqc_df=htqc_df, htqc_summary_df=htqc_summary_df, assay_type=assay_type)


def plot_data(htqc_df: pd.DataFrame, htqc_summary_df: pd.DataFrame, assay_type: str) -> None:
    """
    Plot the data
    :param htqc_df: the interval level data from HTQC
    :param htqc_summary_df: the summary level data from HTQC
    :param assay_type: Assay name
    :return: NoneType
    """

    sns.set_style("whitegrid")
    # Create box plots using Seaborn
    plt.figure(figsize=(18, 6))  # Adjust the figure size as needed
    # two different plots - box and strip
    sns.stripplot(x="chr_start_stop", y="MEAN_COLLAPSED_COVERAGE", hue="NAME", data=htqc_df, legend=False, s=1,
                  palette='dark:black')
    sns.violinplot(x="chr_start_stop", y="MEAN_COLLAPSED_COVERAGE", hue="NAME", data=htqc_df, legend=False,
                   linewidth=2, width=1)

    # Customize the plot
    plt.title(f"Plots / MEAN_COLLAPSED_COVERAGE by Interval / {assay_type} n={len(htqc_summary_df)}")
    plt.xlabel("Interval")
    plt.ylabel("MEAN_COLLAPSED_COVERAGE")
    plt.xticks(rotation=90)  # Rotate x-axis labels for better readability
    plt.xlabel("Your X-Axis Label", fontsize=6, fontweight='bold', fontfamily='serif')

    # Show the plot
    plt.tight_layout()
    plt.axhline(y=100, color='red', linestyle='-')
    plt.axhline(y=200, color='blue', linestyle='--')
    plt.axhline(y=300, color='black', linestyle='--')
    plt.ylim(0, 900)
    plt.savefig(f"boxplot_{assay_type}.svg")


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Analyze a batch of HTQC files and do some plots"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--assay_type', dest='assay_type',
                        help="Assay name", required=True, choices=['gs180', 'gs395'])


    return parser.parse_args()


if __name__ == "__main__":
    main()