"""Program to take our BED file and calculate the allele frequencies for each of those SNPs"""
import argparse
import sys
import functools
print = functools.partial(print, flush=True)  # pylint: disable=W0622


class VcfEntry:
    # pylint: disable=too-many-instance-attributes
    """Store the variant data for the Vcf entry that matched our panel"""

    def __init__(self, chrom=None, pos=None, rs_number=None, ref=None, total_alleles=None,
                 alleles_string=None):
        """
        Each VcfEntry will contain the following attributes.  The list are lists of AlleleEntry
        @param chrom: CHROM from the VCF
        @param pos:  POS from the VCF
        @param rs_number: ID from the VCF
        @param ref: REF from the VCF
        @param total_alleles: Number of alleles for the from the VCF line
        @param alleles_string: allele string e.g.: G,C,T
        """
        self.chrom = chrom
        self.pos = pos
        self.alleles_string = alleles_string
        self.rs_number = rs_number
        self.total_alleles = total_alleles
        self.ref = ref
        self.pop_total = []
        self.pop_latin_american1 = []
        self.pop_latin_american2 = []

    def update_entry(self, allele_entry=None, population=None):
        """
        Take AlleleEntry in and set the correct attribute based on the population passed in
        @param allele_entry: The class of AlleleEntry
        @param population: The population to fill.  At this timeo only looking at total and Latin America
        """
        if population == 'Latin American 1':
            self.pop_latin_american1.append(allele_entry)
        elif population == 'Latin American 2':
            self.pop_latin_american2.append(allele_entry)
        elif population == 'Total':
            self.pop_total.append(allele_entry)
        else:
            sys.exit(f"Did not implement this population type: {population}\n"
                     f"To add a new population create a new attribute\n"
                     "update methods: update_entry,  __sort_alleles_by_max_freq, __str__")

    def print_vcf_entry(self):
        """Print out the output for a tab-delimited file"""
        print("\t".join([str(v) for v in (self.chrom, self.pos, self.rs_number, self.ref, str(self.total_alleles),
                                          self.alleles_string)]),
              end="\t")
        # if there was one allele no need to sort, just print
        if self.total_alleles > 1:
            self.__sort_alleles_by_max_freq()
        self.__print_populations()

    def __sort_alleles_by_max_freq(self):
        """
        Sort each allele by the maximum frequency, since there are more than one allele at this position
        if this method was called
        """
        self.pop_total = sorted(self.pop_total, key=lambda x: x.frequency, reverse=True)
        self.pop_latin_american1 = sorted(self.pop_latin_american1, key=lambda x: x.frequency, reverse=True)
        self.pop_latin_american2 = sorted(self.pop_latin_american2, key=lambda x: x.frequency, reverse=True)

    def __print_populations(self):
        # at this point the alleles have been sorted by freq. so just print out the first element in each
        for entry in (self.pop_total[0], self.pop_latin_american1[0], self.pop_latin_american2[0]):
            print("\t".join((entry.allele, str(entry.count), str(entry.frequency), str(entry.population))), end="\t")
        #  need on last print for the new line b/c above was tab delimited
        print("")

    def __str__(self):
        delimiter = ' :: '
        delimiter2 = '|'
        str_2_return = f"Chromosome {self.chrom}\n" \
                       f"Position {self.pos}\n" \
                       f"RS number {self.rs_number}\n" \
                       f"Ref allele(s) {self.ref}\n" \
                       f"Total alt allele {self.total_alleles}\n" \
                       f"list of Total {delimiter2} {delimiter.join([str(k) for k in self.pop_total])}\n" \
                       f"list of Latin American 1 " \
                       f"{delimiter2} {delimiter.join([str(k) for k in self.pop_latin_american1])}\n" \
                       f"list of Latin American 2 " \
                       f"{delimiter2} {delimiter.join([str(k) for k in self.pop_latin_american2])}"
        return str_2_return

    @staticmethod
    def print_header():
        """
        Just a static method to print out the header on what's being printed via print_vcf_entry
        """
        values = [
            "chromosome",
            "position",
            "dbSNP",
            "Ref",
            "number_alleles",
            "allele_string",
            "total_max_allele",
            "total_max_allele_cnt",
            "total_max_allele_freq",
            "population_1",
            "la1_max_allele",
            "la1_max_allele_cnt",
            "la1_max_allele_freq",
            "population_2",
            "la2_max_allele",
            "la2_max_allele_cnt",
            "la2_max_allele_freq",
            "population_3",
        ]
        print("\t".join(values))


class AllelePopulationEntry:
    # pylint: disable=too-few-public-methods
    """
    Each allele in a VCF entry will have counts and frequency data, these will be stored in the VcfEntry
    attributes: pop_latin_american1, pop_latin_american2, pop_total
    """
    def __init__(self, ref=None, allele=None, count=None, frequency=None, population=None):
        """
        Each VcfEntry can have 1 or more AlleleEntry, for each population
        """
        self.ref = ref
        self.allele = allele
        self.count = count
        self.frequency = frequency
        self.population = population

    def __str__(self):
        return f"AlleleEntry, allele: {self.allele} count: {self.count} frequency: {self.frequency} " \
               f"population {self.population}"


def main():
    """Main driver of the program"""

    args = get_cli_args()
    # get the rs numbers from the BED file
    dict_rs_numbers = get_rs_numbers_from_bed_file(args.bed_file)
    # the VCF file from NCBI to go through
    allele_freq_file = '/Users/cleslin/BLI-repo/scratch/freq.vcf'
    # get the list of entries that were found
    get_rs_numbers_from_ncbi_vcf_file(file=allele_freq_file, dict_rs_numbers=dict_rs_numbers)

    # how many rs numbers were not found:
    print(f"{len(dict_rs_numbers)} RS IDs were not found: {dict_rs_numbers.keys()}", file=sys.stderr)


def get_rs_numbers_from_bed_file(file=None):
    """
    @param file: The BED file to parse for dbSNP ID's
    @return: Dictionary of dbSNP ID's.  Started with a list, but more efficient to test for existence in a dict than
    a list
    """

    list_rs_numbers = []
    with open(file) as in_fh:
        _ = in_fh.readline()  # skip the header
        for line in in_fh:
            values = line.split("\t")
            if values[3].startswith('rs'):
                rs_numbers = values[3].split(",")
                list_rs_numbers += rs_numbers
    # return a dictionary of all the RS numbers
    return {k: None for k in list_rs_numbers}


def get_rs_numbers_from_ncbi_vcf_file(file=None, dict_rs_numbers=None):
    """
    VCF file from: https://www.ncbi.nlm.nih.gov/snp/docs/gsr/alfa/
    https://ftp.ncbi.nih.gov/snp/population_frequency/latest_release/freq.vcf.gz

    Population data from Here: https://useast.ensembl.org/info/genome/variation/species/populations.html
    ALFA:SAMN10492699	-	Latin American 1 = column 13
    ALFA:SAMN10492700	-	Latin American 2 = column 14
    """
    VcfEntry.print_header()
    dict_vcf_columns = get_dictionary_key_to_index()
    dict_population_values = get_dictionary_populations()
    list_variant_entries = []
    with open(file) as in_fh:
        for i, line in enumerate(in_fh):
            if i % 10_000_000 == 0:
                print(f"processed {i} entries", file=sys.stderr)
            line = line.rstrip()
            if line.startswith("#"):
                continue
            values = dict(zip(dict_vcf_columns.keys(), line.split("\t")))
            if values["ID"].startswith("rs"):
                if values["ID"] in dict_rs_numbers:
                    variant_entry = process_vcf_entry(db_snp=values["ID"], dict_values=values,
                                                      dict_population_values=dict_population_values)
                    list_variant_entries.append(variant_entry)
                    variant_entry.print_vcf_entry()
                    del dict_rs_numbers[values["ID"]]

    return list_variant_entries


def process_vcf_entry(db_snp=None, dict_values=None, dict_population_values=None):
    """
    Process the individual VCF entry
    @param db_snp: the RS ID for the SNP
    @parm values: The dictionary from the row of values
    @param dict_population_values: The dictionary of popultions
    """
    variant_entry = VcfEntry(chrom=dict_values['CHROM'], pos=dict_values['POS'], rs_number=db_snp,
                             ref=dict_values['REF'], total_alleles=len(dict_values['ALT'].split(',')),
                             alleles_string=dict_values['ALT'])
    for population, population_value in dict_population_values.items():
        if population_value not in ['Total', 'Latin American 1', 'Latin American 2']:
            continue
        all_population_alt_freq = 0
        all_population_alt_count = 0
        for i, alt_allele in enumerate(dict_values['ALT'].split(',')):
            # values come in like:  8560:5387 for one alt or 4456:85,8,45 when there are multiple alts
            population_total, all_alleles = dict_values[population].split(":")
            # fix the data so we have ints
            population_total = int(population_total)
            all_alleles_totals = [int(allele) for allele in all_alleles.split(",")]
            # calculate the frequency of the alt allele
            allele_freq = round(all_alleles_totals[i] / population_total, 3)
            # store this for later
            all_population_alt_freq += allele_freq
            all_population_alt_count += all_alleles_totals[i]
            # instantiate an object for the allele
            allele_entry = AllelePopulationEntry(ref=dict_values['REF'], allele=alt_allele,
                                                 count=all_alleles_totals[i],
                                                 frequency=allele_freq,
                                                 population=population_value)
            variant_entry.update_entry(population=population_value,
                                       allele_entry=allele_entry)
    return variant_entry


def get_dictionary_key_to_index():
    """
    VCF file from: https://www.ncbi.nlm.nih.gov/snp/docs/gsr/alfa/
    https://ftp.ncbi.nih.gov/snp/population_frequency/latest_release/freq.vcf.gz

    Population data from Here: https://useast.ensembl.org/info/genome/variation/species/populations.html
    ALFA:SAMN10492699	-	Latin American 1 = column 13
    ALFA:SAMN10492700	-	Latin American 2 = column 14

     More on the populations can be seen here:
    https://www.ncbi.nlm.nih.gov/snp/docs/gsr/data_inclusion/#population
    """
    return {
        "CHROM": 0,
        "POS": 1,
        "ID": 2,
        "REF": 3,
        "ALT": 4,
        "QUAL": 5,
        "FILTER": 6,
        "INFO": 7,
        "FORMAT": 8,
        "SAMN10492695": 9,
        "SAMN10492696": 10,
        "SAMN10492697": 11,
        "SAMN10492698": 12,
        "SAMN10492699": 13,
        "SAMN10492700": 14,
        "SAMN10492701": 15,
        "SAMN10492702": 16,
        "SAMN11605645": 17,
        "SAMN10492703": 18,
        "SAMN10492704": 19,
        "SAMN10492705": 20,
    }


def get_dictionary_populations():
    """
    VCF file from: https://www.ncbi.nlm.nih.gov/snp/docs/gsr/alfa/
    https://ftp.ncbi.nih.gov/snp/population_frequency/latest_release/freq.vcf.gz

    Population data from Here: https://useast.ensembl.org/info/genome/variation/species/populations.html
    ALFA:SAMN10492699	-	Latin American 1 = column 13
    ALFA:SAMN10492700	-	Latin American 2 = column 14

    More on the populations can be seen here:
    https://www.ncbi.nlm.nih.gov/snp/docs/gsr/data_inclusion/#population
    """
    return {
        "SAMN10492705": 'Total',  # Total (~global) across all populations
        "SAMN10492695": 'European',  # European
        "SAMN10492696": 'African Others',
        "SAMN10492697": 'East Asian',
        "SAMN10492698": 'African American',
        "SAMN10492699": 'Latin American 1',  # Latin American individuals with Afro-Caribbean ancestry
        "SAMN10492700": 'Latin American 2',  # Latin American individuals with mostly European and
        # Native American Ancestry
        "SAMN10492701": 'Other Asian',  # Asian individiuals excluding South or East Asian
        "SAMN10492702": 'South Asian',  # South Asian
        "SAMN11605645": 'Other',  # The self-reported population is inconsistent with the GRAF-assigned population
        "SAMN10492703": 'African',  # All Africans, AFO and AFA Individuals
        "SAMN10492704": 'Asian',  # All Asian individuals (EAS and OAS) excluding South Asian (SAS)
    }


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Process new BED file for SNPS and get the allele frequency')

    parser.add_argument('--bed_file', dest='bed_file',
                        type=str, help='BED file to pull out dbSNP ids', required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()
