"""
Remove the duplicate sites from the VCF file generated from CosmicCodingMuts.vcf
bedtool intersect is used to find the overlap between the COSMIC dataset (CosmicCodingMuts.vcf) and the hotspot
BED file used in our pipeline.

Be sure to use the COSMIC file first (-a CosmicCodingMuts.vcf ) so that the output is in VCF format

bedtools intersect \
-a CosmicCodingMuts.vcf \
-b actionable_L1_L2_R1_R2_oncokb_biomarker_drug_associations-v10-20221104.bed \
>temp_vcf.vcf

perl -ne \
'@arr=split("\t"); if (length($arr[3]) == 1 && length($arr[4]) == 1){print $_}' \
temp_vcf.vcf >variants.vcf


Here variants.vcf would be the input to this program:

pipenv run python3 -m tools.cosmic.reduce_duplicates \
--cosmic_vcf_file variants.vcf \
>variants.vcf_deduplicated_by_CNT.vcf

"""

import argparse


def main():
    """
    Go over the rows in the COSMIC formatted VCF file and remove duplicates by taking into account the CNT field
    in the INFO column and using only non-synonymous changes with the highest CNT
    """
    args = get_cli_args()
    final_value = {}  # store the final data to be printed
    with open(args.cosmic_vcf_file, encoding='utf8') as infh:
        for line in infh:
            line = line.rstrip()
            values = line.split("\t")
            key = "_".join((values[0], values[1], values[3]))  # concatenate the CHROM and POSITION and the REF
            format_values = values[7].split(";")  # 7 index is the column I care about with the values
            temp_dict = {}
            # gov over the format values and split, and create a temp dictionary
            for element in format_values:
                key2, value = element.split("=")
                temp_dict[key2] = value

            if 'AA' in temp_dict and '%3D' in temp_dict['AA']:  # skip these b/c there was no impact
                # the hotspot befile has regions that go outside by 1bp of a codon, and also we dont care about
                # hits that had synonymous change
                continue

            if 'CNT' in temp_dict:  # update the CNT to an int if it was found
                temp_dict['CNT'] = int(temp_dict[key2])
            else:  # didn't have CNT just set to one
                temp_dict['CNT'] = 1

            if key in final_value:  # was the site found before, and if so should it be updated with a row with the
                # higher CNT value
                current_count = final_value[key]['CNT']
                if temp_dict['CNT'] > current_count:  # was the row found with a higher current_count
                    final_value[key] = {'CNT': temp_dict['CNT'], 'row_value': line}
            else:
                final_value[key] = {'CNT': temp_dict['CNT'], 'row_value': line}

    # print out the VCF header
    print("\t".join(("#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO")))
    # print out the rows
    for row in final_value.values():
        print(row['row_value'])


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description="open a CosmicCodingMuts.vcf formatted file and remove the redundancy"
                                                 "based at the site level, keeping the entry with the higher CNT "
                                                 "value ")

    parser.add_argument('--cosmic_vcf_file', dest='cosmic_vcf_file',
                        type=str, help='Open a CosmicCodingMuts.vcf formatted file', required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()
