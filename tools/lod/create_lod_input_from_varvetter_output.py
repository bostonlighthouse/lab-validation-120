"""Covert varvetter output into some input that can be used for LOD variant file"""
import argparse
import sys
from lab_validation_120.utils.vcf_utilities import get_jax_protein_change, is_a_consequential_impact


def main():
    args = get_cli_args()
    process_varvetter_input(args.varvetter_input_file)


def process_varvetter_input(input_file):
    """Go over all variants and transform to an input file for LOD"""
    vaf_threshold = 0
    depth_threshold = 0
    null_gene_or_aa = 0
    print("\t".join(('Chromosome', 'Gene', 'Variant', 'VAF', 'Variant_Type', 'variant_impact')))
    # TODO Use pandas for this and not split
    with open(input_file) as infh:
        header = infh.readline()
        for line in infh:
            list_val = line.rstrip().split("\t")
            variant_class = _return_variatn_class(list_val[1])
            # needed values
            variant_impact = list_val[4]
            aa_change = list_val[7]
            cdot_change = list_val[8]
            hgvsg = list_val[9]
            depth = int(list_val[10]) + int(list_val[11])
            gene = list_val[6]
            primary_caller = list_val[32]
            chromosome = list_val[-2].split('_')[0]
            vaf = round((float(list_val[12]) * 100), 1)
            if primary_caller == 'vargrouper':  # skip vargrouper as primary caller
                continue

            # originally was going to use this.  Keeping here for now
            if ignore_variant_impact(variant_impact):
                continue
            # originally was going to use this.  Keeping here for now
            #if variant_class == 'SUB':
            #    continue
            # if ther was a amino acid change, then get it and turn into variant we can use VEP uses three-letter codes

            # LOD comparison  uses one letter code, so convert
            if aa_change != 'null':
                aa_change = get_jax_protein_change(aa_change.split(':')[1])
                aa_change = aa_change.replace('p.', '')
                # pre VEP updates?
                #aa_change = aa_change.replace('(=)', '(%3D)')
                aa_change = aa_change.replace('=', '%3D')
            # if this amino acid was null, and cdot was not null then use c.dot as the chnages
            if aa_change == 'null' and cdot_change != 'null':
                aa_change = get_jax_protein_change(cdot_change.split(':')[1])
            # some depth and vaf thresholds to use
            if vaf < 5:
                vaf_threshold += 1
                #print(aa_change, file=sys.stderr)
                continue
            if depth < 10:
                depth_threshold += 1
                continue
            # if both of these are null, then ignore
            if gene == 'null' or aa_change == 'null':
                null_gene_or_aa += 1
                continue
            #if aa_change.con

            # print out the config data
            print("\t".join((chromosome, gene, aa_change, str(vaf), variant_class, variant_impact)))
    print(f"found {vaf_threshold} variants that did not meet vaf thresholds", file=sys.stderr)
    print(f"found {depth_threshold} variants that did not meet depth thresholds", file=sys.stderr)
    print(f"found {null_gene_or_aa} variants that did not have gene or aa_changes", file=sys.stderr)


def ignore_variant_impact(variant_impact=None):
    """Only look at some variants
        3_prime_UTR_variant: IGNORE
        5_prime_UTR_variant: IGNORE
        TF_binding_site_variant
        coding_sequence_variant
        downstream_gene_variant: IGNORE
        frameshift_variant
        inframe_deletion
        inframe_insertion
        intergenic_variant: IGNORE
        intron_variant: IGNORE
        missense_variant
        non_coding_transcript_exon_variant: IGNORE
        protein_altering_variant
        regulatory_region_variant
        splice_acceptor_variant
        splice_donor_variant
        splice_region_variant
        start_lost
        stop_gained
        stop_lost
        synonymous_variant
        upstream_gene_variant: IGNORE
        @param variant_impact: string of the variant impact to look at
    """
    if variant_impact in ['3_prime_UTR_variant', '5_prime_UTR_variant', 'downstream_gene_variant', 'intergenic_variant',
                          'non_coding_transcript_exon_variant', 'upstream_gene_variant', 'intron_variant',
                          'synonymous_variant']:
        return True
    return False


def _return_variatn_class(variant_class=None):
    """Convert the varvetter variant_class to LOD format variants, SNV, DEL, INS
    @param variant_class: Variant class column from varbetter
    @retrun string: The variant class used by LOD
    """
    if variant_class == 'deletion':
        return 'DEL'
    elif variant_class == 'insertion':
        return 'INS'
    elif variant_class == 'indel':
        return 'INS'
    elif variant_class == 'substitution':
        return 'SUB'
    elif variant_class == 'SNV':
        return variant_class
    else:
        print(f"Did not see this case: {variant_class}")
        sys.exit()


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(
        description='Covert varvetter output into some input that can be used for LOD variant file')

    parser.add_argument('--varvetter_input_file', dest='varvetter_input_file',
                        type=str, help='The output from varvetter',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()