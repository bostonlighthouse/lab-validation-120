file: genosity_data_list_overlapping_oc120archer_files.txt
These files where the genosity data with overlap to the archer oc120 bed file

file: genosity_data_list_overlapping_oc120plus_files.txt
These files where the genosity data with overlap to the oc120plus bed file


file: oc120plus_equiv_samples.txt
The 32 samples from the equivalency study that were sequenced with the oc120plus assay.  There were two files for each
of these, and it's passed by the path e.g:
lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file
lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file
e.g. run:
pipenv run python3 -m tools.create_oc120_vs_oc120plus_reproducibility_config_ds \
--pair_file tools/input_files/oc120plus_equiv_samples.txt \
--path1  lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file \
--ds_type single_assay_qc_metrics

file: oc120_oc120plus_equiv_samples.txt
The 32 samples from the equivalency study that were sequenced with both the oc120 and oc120plus assay.
The first column are the oc120 samples, the second column is the oc120plus samples
e.g. run:
python3 tools/create_oc120_vs_oc120plus_reproducibility_config_ds.py \
--pair_file tools/input_files/oc120_oc120plus_equiv_samples.txt  \
--path1  'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file/'  \
--path2 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file' \
--ds_type qc_metrics

file: oc120_equiv_samples.txt
The 32 samples from the equivalency study that were sequenced with the oc120 original assay.  There were two files for each
of these, and it's passed by the path e.g:
lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_original_oc120_bed_file
lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/comparison_using_new_oc120plus_bed_file