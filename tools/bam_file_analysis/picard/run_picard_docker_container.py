import argparse
import os
import re
import sys

def main():
    args = get_cli_args()
    tool = args.picard_tool

    with open(args.bam_file) as in_fh:
        bams = [os.path.splitext(bam.rstrip())[0] for bam in in_fh]

    mol_type = 'DNA'
    #bam_container_dir = '/export/bamstore/fusion395'
    bam_container_dir = '/export/bamstore/snapshot395'
    interval_container_dir = '/export/resources/scratch/agilent'
    output_container_dir = f'/export/resources/scratch/agilent/{mol_type}'
    refgenome_container_dir = '/export/resources/hg19'
    print("#!/bin/bash")
    print("set -e")
    for bam_file in bams:
        command = ''
        bam_file_command = f"-I {bam_container_dir}/{bam_file}.bam \\\n"
        ref_genome_command = f"-R {refgenome_container_dir}/Homo_sapiens_assembly19.fasta \\\n"
        max_records_command = f"--MAX_RECORDS_IN_RAM 30000000 \\\n"
        verbosity_command = f"--VERBOSITY DEBUG\n\n"
        gatk_location = '/gatk/gatk'
        if tool == 'CollectHsMetrics':
            command = f"{gatk_location} CollectHsMetrics \\\n" \
                      f"{bam_file_command}" \
                      f"-O {output_container_dir}/{bam_file}.hsmetrics.{mol_type}_100bp_window.interval.txt \\\n" \
                      f"{ref_genome_command}" \
                      f"--BAIT_INTERVALS {interval_container_dir}/A3416642_Regions_hg19lft_intervname_100bp_window.interval_list \\\n" \
                      f"--TARGET_INTERVALS {interval_container_dir}/A3416642_Regions_hg19lft_intervname_100bp_window.interval_list \\\n" \
                      f"{max_records_command}" \
                      f"--PER_TARGET_COVERAGE {output_container_dir}/{bam_file}.per_target_cov.{mol_type}_100bp_window.interval.txt \\\n" \
                      f"{verbosity_command}"
        elif tool == 'CollectGcBiasMetrics':
            command = f"{gatk_location} CollectGcBiasMetrics \\\n" \
                      f"{bam_file_command}" \
                      f"-O {output_container_dir}/{bam_file}.gc_bias_metrics.{mol_type}.summary.txt \\\n" \
                      f"-S {output_container_dir}/{bam_file}.gc_bias_metrics.{mol_type}.txt \\\n" \
                      f"--CHART {output_container_dir}/{bam_file}.gc_bias_metrics.{mol_type}.pdf \\\n" \
                      f"{ref_genome_command}" \
                      f"--ALSO_IGNORE_DUPLICATES true \\\n" \
                      f"--MAX_RECORDS_IN_RAM 30000000 \\\n" \
                      f"{verbosity_command}"
        if tool == 'DepthOfCoverage':
            command = f"{gatk_location} DepthOfCoverage \\\n" \
                      f"{bam_file_command}" \
                      f"-O {output_container_dir}/{bam_file}.depth_of_coverage.{mol_type}_100bp_window.interval.txt \\\n" \
                      f"{ref_genome_command}" \
                      f"-L {interval_container_dir}/A3416642_Regions_hg19lft_intervname_100bp_window.interval_list\n"

        print(command)

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Run gatk4 CollectHsMetrics on a container, assumming mounted drives"
    parser = argparse.ArgumentParser(description=help_str)
    parser.add_argument('--bam_file',
                        dest='bam_file',
                        help="List of BAM, one per line",
                        type=str,
                        required=True)
    parser.add_argument('--picard_tool',
                        dest='picard_tool',
                        help="Picard tool to run",
                        type=str,
                        choices=["CollectHsMetrics", "CollectGcBiasMetrics", "DepthOfCoverage"],
                        required=True)



    return parser.parse_args()


if __name__ == "__main__":
    main()