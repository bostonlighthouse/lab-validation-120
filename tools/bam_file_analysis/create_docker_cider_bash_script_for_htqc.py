import argparse
import os
import pprint as pp
import re
import sys


def main():
    args = get_cli_args()

    print("Enter the BAM files, and then hit Control-D", file=sys.stderr)
    bams = sys.stdin.readlines()
    bams = [bam.rstrip() for bam in bams]

    print("#!/bin/bash")
    print("set -e")
    cid = None
    all_bams = []
    for bam in bams:
        bam_no_ext, ext = os.path.splitext(bam)
        htqc_output_file = f"{bam_no_ext}_htqc_output.txt"
        htqc_output_file = os.path.basename(htqc_output_file)
        htqc_output_file = f"{htqc_output_file}"

        htqc_summary_output_file = f"{bam_no_ext}_htqc_output_summary.txt"
        htqc_summary_output_file = os.path.basename(htqc_summary_output_file)
        #args.bed_path = os.path.basename(args.bed_path)
        htqc_summary_output_file = f"{os.path.basename(htqc_summary_output_file)}"

        # first create the coverage file
        print(
              #f"docker run --rm -v /export/bamstore/:/export/bamstore/ "
              #f"-v /export/scratch/samtools/bams/:/export/scratch/samtools/bams/ "
              #f"-v /export/scratch/samtools/htqc/:/export/scratch/samtools/htqc/ "
              #f"-v {args.resource_path}/:{args.resource_path}/ "
              #f"{args.image_name} "
              f"htqc coverage "
              f"--bam-path {args.bam_path}/{bam} "
              f"--bed-path {args.bed_path} "
              f"--stats-name mean "
              f"--output-file {args.resource_path}/{htqc_output_file} ")
        # then create the summary file
        print(""
              #f"docker run --rm -v /export/bamstore/:/export/bamstore/ "
              #f"-v /export/scratch/samtools/bams/:/export/scratch/samtools/bams/ "
              #f"-v /export/scratch/samtools/htqc/:/export/scratch/samtools/htqc/ "
              #f"-v /export/resources/gsv2/:/export/resources/gsv2/ "
              #f"{args.image_name} "
              f"htqc summary "
              f"--coverage-path {args.resource_path}/{htqc_output_file} "
              f"--output-path {args.resource_path}/{htqc_summary_output_file} ")


def print_ds_for_reproducibility_vcf(file=None, path1=None, path2=None, sample_num=None):
    """
    Print data structure for reproducibility analysis
    @param file: The file to open for the data structure
    @param path1: Destination of directories found in --pair_file (1st entry)
    @param path2: Destination of directories found in --pair_file (2nd entry)
    @param sample_num:  Just an int on where to start printing
    """


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Create a script to run htqc coverage on a list of bam files"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--image_name', dest='image_name', help="Image name for cider", required=True)
    parser.add_argument('--bed_path', dest='bed_path', help="Path to the bed file", required=True)
    parser.add_argument('--resource_path', dest='resource_path', help="Path to mount the resource volume",
                        required=True)
    parser.add_argument('--bam_path', dest='bam_path', help="Path to the BAM files",
                        required=True)
    """
    snapshot run with GS180 SNPs
    ls /export/bamstore/snapshotGSV2/*b21-110-*-snapshot*bam
    snapshot run with GS180 SNPs that were rerun with the new BED file with SNPs
    ls /export/bamstore/snapshotGSV2/*b21-110-*-rerun*bam
    Bed files to compare
    /export/resources/gsv2/VariantPlex_OC_v3_16897_primers_targets.sorted.nochr.bed
    /export/resources/gsv2_snp/19638_VariantPlex_GS180v3_Additions_for_OC_Review_primers_targets.no_chr.bed
    /export/resources/gsv2_snp/VariantPlex_OC_v3_16897_primers_targets.Additions_for_OC_Review_primers_targets.sorted.bed
    resource_path
    /export/resources/gsv2
    /export/resources/gsv2_snp
    
    --bed_path /export/resources/gsv2/VariantPlex_OC_v3_16897_primers_targets.sorted.nochr.bed --resource_path /export/resources/gsv2 
    --bed_path /export/resources/gsv2_snp/19638_VariantPlex_GS180v3_Additions_for_OC_Review_primers_targets.no_chr.bed --resource_path /export/resources/gsv2_snp
    --bed_path /export/resources/gsv2_snp/VariantPlex_OC_v3_16897_primers_targets.Additions_for_OC_Review_primers_targets.sorted.bed --resource_path /export/resources/gsv2_snp
    
    """
    return parser.parse_args()


if __name__ == "__main__":
    main()