# plot_type: 'table'
# section_name: 'samtools stats Alignment Stats'
# description: 'Summary stats describing the number of reads which mapped and passed various filters'
# pconfig:
#     namespace: 'Alignment Stats'
# headers:
#     raw_total_sequences:
#         title: 'raw_total_sequences'
#         description: 'total number of reads in a file, excluding supplementary and secondary reads. Same number reported by samtools view -c'
#         format: '{:,.0f}'
#         placement: 1000
#     sequences:
#         title: 'sequences'
#         description: 'number of processed reads.'
#         format: '{:,.0f}'
#         placement: 1010
#     reads_mapped:
#         title: 'reads_mapped'
#         description: 'number of reads, paired or single, that are mapped (flag 0x4 or 0x8 not set).'
#         format: '{:,.0f}'
#         placement: 1020
#     reads_mapped_and_paired:
#         title: 'reads_mapped_and_paired'
#         description: 'number of mapped paired reads (flag 0x1 is set and flags 0x4 and 0x8 are not set).'
#         format: '{:,.0f}'
#         placement: 1030
#     reads_unmapped:
#         title: 'reads_unmapped'
#         description: 'number of unmapped reads (flag 0x4 is set).'
#         format: '{:,.0f}'
#         placement: 1040
#     reads_properly_paired:
#         title: 'reads_properly_paired'
#         description: 'number of mapped paired reads with flag 0x2 set.'
#         format: '{:,.0f}'
#         placement: 1050
#     paired:
#         title: 'paired'
#         description: 'number of paired reads, mapped or unmapped, that are neither secondary nor supplementary (flag 0x1 is set and flags 0x100 (256) and 0x800 (2048) are not set).'
#         format: '{:,.0f}'
#         placement: 1060
#     reads_duplicated:
#         title: 'reads_duplicated'
#         description: 'number of duplicate reads (flag 0x400 (1024) is set).'
#         format: '{:,.0f}'
#         placement: 1070
#     reads_MQ0:
#         title: 'reads_MQ0'
#         description: 'number of mapped reads with mapping quality 0.'
#         format: '{:,.0f}'
#         placement: 1080
#     reads_QC_failed:
#         title: 'reads_QC_failed'
#         description: 'number of reads that failed the quality checks (flag 0x200 (512) is set).'
#         format: '{:,.0f}'
#         placement: 1090
#     total_length:
#         title: 'total_length'
#         description: 'number of reads that failed the quality checks (flag 0x200 (512) is set).'
#         format: '{:,.0f}'
#         placement: 1090

