import os

def get_samtools_stats_values(file: str) -> dict:
    """
    Return the insert size and insert size std
    :param file:
    :return:
    """


    dictionary_stats = {}
    for k, v in (('insert_size_average', -1.0), ('insert_size_std', -1.0), ('num_sequences', -1)):
       dictionary_stats[k] = v

    if not os.path.exists(file):
        return dictionary_stats

    with open(file, 'r', encoding='utf-8') as fh:
        for line in fh:
            line = line.strip()
            values = line.split('\t')
            if line.startswith('SN\tinsert size average:'):
                dictionary_stats['insert_size_average'] = float(values[2])
            if line.startswith('SN\tsequences:'):
                dictionary_stats['num_sequences'] = int(values[2])
            elif line.startswith('SN\tinsert size standard deviation:'):
                dictionary_stats['insert_size_std'] = float(values[2])
                break

    return dictionary_stats
