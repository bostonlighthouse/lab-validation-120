import argparse
import matplotlib.pyplot as plt
import seaborn as sns
import argparse
from lab_validation_120.utils.pandas_dataframes_utilities import read_dataset_and_get_pandas_df, \
    get_column_types_htqc
from lab_validation_120.utils.plots.plot_utilities import get_subplot_bounding_box_parameters
from lab_validation_120.utils.assay_utils import get_filename
import re

IMAGE_TYPE_EXTENSION = 'pdf'

def main():

    args = get_cli_args()
    htqc_output_file = args.htqc_output_file
    df = read_dataset_and_get_pandas_df(col_vals=get_column_types_htqc(),
                                        file_name=htqc_output_file)
    match = re.search(r'(cid\d+-\d+)', htqc_output_file)
    cid = None
    if match:
        cid = match.group(1)
    for stat in ['MEAN_FILTERED_COVERAGE', 'MEAN_COLLAPSED_COVERAGE']:
        plot_distribution(df=df, cid=cid, stat=stat)


def plot_distribution(df=None, cid=None, stat='MEAN_FILTERED_COVERAGE'):
    bins = None
    plt.rcParams['figure.figsize'] = (8, 8)
    fig, ax = plt.subplots(1, 1)
    if bins is not None:
        plot_obj = sns.histplot(data=df[stat], ax=ax, kde=True, color="blue",
                                bins=bins)
    else:
        pot_obj = sns.histplot(data=df[stat], ax=ax, kde=True, color="blue")

    title_text = f"Distribution of {stat} n={len(df)}"
    #pot_obj.set_xlim(-25, 765)  # 610
    pot_obj.set_xlim(-25, 610)  # 610
    #pot_obj.set_ylim(0, 188)
    if cid is not None:
        title_text += f", from sample {cid}"
    ax.title.set_text(title_text)

    fig.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(get_filename(filename=f'{stat}_distribution_htqc_{cid}', extension=IMAGE_TYPE_EXTENSION))


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Parse out the FACETS VCF file')
    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('--htqc_output_file', dest='htqc_output_file',
                       type=str, help='output from htqc')

    return parser.parse_args()


if __name__ == "__main__":
    main()