"""
open a samtools depth file and map that to positions in vcf files found in a list of directories passed in
One you have the data, print out some metrics based on how much coverage we see"""
import pandas as pd
import argparse

from lab_validation_120.utils.pandas_dataframes_utilities import get_columns_from_vcf, read_dataset_and_get_pandas_df
from tools.utils.tools_utils import open_directory_get_list_files


def main() -> None:
    """Business logic"""
    args = get_cli_args()
    chrom = '#CHROM'
    pos = 'POS'
    depth = 'DEPTH'
    vcf_directories = args.vcf_directories
    samtools_depth_df = pd.read_csv(args.samtools_depth_output, sep="\t", names=[chrom, pos, depth],
                                    dtype={chrom: "string", pos: int, depth: int})
    # get a data frame from all the vcf files from the list vcf_directories that are mapped to the depth found in
    # the data frame samtools_depth_df
    final_vcf_to_depth_df = get_vcf_to_depth_df(vcf_directories=vcf_directories, samtools_depth_df=samtools_depth_df,
                                                chrom=chrom, pos=pos)
    print_final_results(df=final_vcf_to_depth_df, depth=depth)


def print_final_results(df: pd.DataFrame, depth: str):
    """
    Print out the final results
    :param df: Data frame used for printing
    :param depth: Simple column name
    :return: None
    """
    sig_digits = 1
    print("\t".join(('num_sites', 'mean_depth', 'median_depth', 'std_depth',
                     'quantile_25%_depth',
                     'quantile_50%_depth',
                     'quantile_75%_depth',
                     'sites_less_100x_coverage',
                     'sites_less_50x_coverage',
                     'sites_less_5x_coverage',
                     'sites_less_0x_coverage',
                     'directory')))
    for directory in df['dir'].unique():
        temp_df = df[df['dir'] == directory]
        num_sites = len(temp_df)
        mean = round(temp_df[depth].mean(), sig_digits)
        median = round(temp_df[depth].median(), sig_digits)
        std = round(temp_df[depth].std(), sig_digits)
        quantile_dict = temp_df[depth].quantile(q=[.25, .50, .75]).to_dict()
        sites_less_100x_coverage = len(temp_df[temp_df[depth] < 100])
        sites_less_50x_coverage = len(temp_df[temp_df[depth] < 50])
        sites_less_5x_coverage = len(temp_df[temp_df[depth] < 5])
        sites_less_0_coverage = len(temp_df[temp_df[depth] == 0])
        print('\t'.join([str(x) for x in [num_sites, mean, median, std,
                                          quantile_dict[.25],
                                          quantile_dict[.50],
                                          quantile_dict[.75],
                                          sites_less_100x_coverage,
                                          sites_less_50x_coverage,
                                          sites_less_5x_coverage,
                                          sites_less_0_coverage,
                                          directory]]))


def get_vcf_to_depth_df(vcf_directories: list, samtools_depth_df: pd.DataFrame,
                        chrom: str, pos: str) -> pd.DataFrame:
    """
    Get a final data frame with all vcf positions found per directory, mapped to the coverage from samtools
    :param vcf_directories: VCF directories to look into
    :param samtools_depth_df: data from samtools
    :param chrom: column name for chromosome
    :param pos: column name for the position
    :return: None
    """
    all_final_df = []  # store a list of data frames to be concat at the end
    for vcf_in_directory in vcf_directories:
        # get a list of VCF files from the directory passed in
        vcf_files = open_directory_get_list_files(vcf_in_directory, file_extension=".vcf")
        # update the list with the path
        vcf_files = [f"{vcf_in_directory}/{file}" for file in vcf_files]
        # get a list of data frames from the VCF file
        list_vcf_files_df = [read_dataset_and_get_pandas_df(file_name=file, col_vals=get_columns_from_vcf(),
                                                            ignore_line_starts_with="##") for file in vcf_files]
        # create a final vcf data frame from the vcf_in_directory
        final_vcf_files_df = pd.concat(list_vcf_files_df).reset_index(drop=True)
        # combine the depth data
        merged_df = pd.merge(final_vcf_files_df, samtools_depth_df, on=[chrom,  pos])
        # update a column
        merged_df['dir'] = vcf_in_directory
        # store
        all_final_df.append(merged_df)
    # concat them all together and return
    return pd.concat(all_final_df).reset_index(drop=True)


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Compare samtools depth output and show coverage at VCF positions')

    parser.add_argument('--vcf_directories',
                        dest='vcf_directories',
                        nargs='+',
                        type=str,
                        required=True,
                        help='Directory where VCF files are located')
    parser.add_argument('--samtools_depth_output',
                        dest='samtools_depth_output',
                        type=str,
                        required=True,
                        help='samtools depth output from a BAM file')

    return parser.parse_args()


if __name__ == "__main__":
    main()