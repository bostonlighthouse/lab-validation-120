"""
Module to parse htqc summary stats and export as a TSV to be opened by multiqc. The module will require python 3
to print out the header and values in the correct order, since their are dictionaries used.
"""

import argparse

from tools.bam_file_analysis.samtools.multqc.multiqc_utils import get_data_structure_key_names, \
    print_multiqc_output_file, parse_tool_summary_stats_values


def main():
    """Main business logic"""
    args = get_cli_args()
    # get the dictionary of parsed values
    list_of_dict_tool_values = \
        parse_tool_summary_stats_values(input_files=args.htqc_summary_stats_input_files,
                                        function_to_get_tool_column_names=get_dict_htqc_summary_stats_column_values,
                                        tool_name='htqc_summary_stats')
    # print the dictionary of parsed values
    print_multiqc_output_file(list_of_dict_tool_values=list_of_dict_tool_values,
                              output_file=args.htqc_summary_stats_parsed_output_file,
                              section_name='HTQC Summary Alignment Stats',
                              description='Summary stats described by HTQC',
                              namespace='Alignment Stats', plot_type='table',
                              function_to_get_tool_column_names=get_dict_htqc_summary_stats_column_values,
                              sig_digits=3)


def get_dict_htqc_summary_stats_column_values() -> dict:
    """
    Values that need to be parsed from htqc summary stats output
    @return list of the the dictionaries.  This function is also be used to print out the header config values that are
    needed in the multiqc input file

    htqc summary stats has values like:

    SIZE (bp)=337044
    MEAN_ABS_COVERAGE=1989.506
    MEAN_FILTERED_COVERAGE=191.208
    MEAN_COLLAPSED_COVERAGE=103.539
    FRACTION_WITH_ZERO_DP=0.0
    FRACTION_LTE_LOWER_DP=0.047
    FRACTION_GTE_UPPER_DP=0.401
    """
    # list out the types you want to parse in the file
    type_key, desc_key, use_in_output_key, _ = get_data_structure_key_names()
    # keys here are actual matches to htqc summary stats output
    dict_htqc_summary_stats_values_to_parse = {
        'MEAN_ABS_COVERAGE': {
            type_key: float,  # type for the data
            desc_key: 'Mean absolute coverage for the sample',  # description to show
            use_in_output_key: True},  # True to show, False to hide
        'MEAN_FILTERED_COVERAGE': {
            type_key: float,
            desc_key: 'Mean filtered coverage for the sample',
            use_in_output_key: True},
        'MEAN_COLLAPSED_COVERAGE': {
            type_key: float,
            desc_key: 'Mean collapsed coverage for the sample',
            use_in_output_key: True},
        'FRACTION_WITH_ZERO_DP': {
            type_key: float,
            desc_key: 'Fraction of intervals with 0 coverage for the sample',
            use_in_output_key: True},
        'FRACTION_LTE_LOWER_DP': {
            type_key: float,
            desc_key: 'Fraction of intervals with < 100 MFC for the sample',
            use_in_output_key: True},
        'FRACTION_GTE_UPPER_DP': {
            type_key: float,
            desc_key: 'Fraction of intervals with >= 100 MFC for the sample',
            use_in_output_key: True},

    }
    return dict_htqc_summary_stats_values_to_parse


def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    @return: instance of argparse arguments
    """

    help_str = "Module to parse htqc summary stats from a list of samples and export as a TSV to be opened with multiqc"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--htqc_summary_stats_input_files', dest='htqc_summary_stats_input_files',
                        help="input files from HTQC summary stats.  String of values separated by space, "
                             "or a single file name", required=True)

    parser.add_argument('--htqc_summary_stats_parsed_output_file', dest='htqc_summary_stats_parsed_output_file',
                        help="output file from parsing htqc summary stats", required=True)
    return parser.parse_args()


if __name__ == "__main__":
    main()
