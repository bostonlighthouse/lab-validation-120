"""
Module to parse samtools stats and export as a TSV to be opened by multiqc. The module will require python 3
to print out the header and values in the correct order, since their are dictionaries used.
"""

import argparse

from tools.bam_file_analysis.samtools.multqc.multiqc_utils import get_data_structure_key_names, \
    print_multiqc_output_file, parse_tool_summary_stats_values


def main():
    """Main business logic"""
    args = get_cli_args()
    # get the dictionary of parsed values
    list_of_dict_tool_values = \
        parse_tool_summary_stats_values(input_files=args.samtools_stats_input_files,
                                        function_to_get_tool_column_names=get_dict_samtools_stats_column_values,
                                        tool_name='samtools_stats', break_start_string='FFQ')
    # print the dictionary of parsed values
    print_multiqc_output_file(list_of_dict_tool_values=list_of_dict_tool_values,
                              output_file=args.samtools_stats_parsed_output_file,
                              section_name='Samtools Alignment Stats',
                              description='Summary stats describing the number of reads which mapped and passed '
                                          'various filters',
                              namespace='Samtools', plot_type='table',
                              function_to_get_tool_column_names=get_dict_samtools_stats_column_values)


def get_dict_samtools_stats_column_values() -> dict:
    """
    Values that need to be parsed from samtools stats output
    @return list of the the dictionaries.  This function is also be used to print out the header config values that are
    needed in the multiqc input file

    samtools stats has  like:
    SN      raw total sequences:    290018  # excluding supplementary and secondary reads
    SN      filtered sequences:     0
    SN      sequences:      290018
    SN      is sorted:      1
    SN      1st fragments:  145009
    SN      last fragments: 145009
    SN      reads mapped:   255160
    ...
    ...
    ...
    The value after SN became the keys below
    """
    # list out the types you want to parse in the file
    type_key, desc_key, use_in_output_key, _ = get_data_structure_key_names()
    # http://www.htslib.org/doc/samtools-stats.html
    # keys here are actual matches to samtools stats output
    dict_samtools_stats_values_to_parse = {
        'raw_total_sequences': {
            type_key: int,  # type for the data
            desc_key: 'total number of reads in a file, excluding supplementary and secondary reads. '
                      'Same number reported by samtools view -c.',  # description to show
            use_in_output_key: True},  # True to show, False to hide on the multiqc html output
        'filtered_sequences': {
            type_key: int,
            desc_key: 'number of discarded reads when using -f or -F option.',
            use_in_output_key: False},
        'sequences': {
            type_key: int,
            desc_key: 'number of processed reads.',
            use_in_output_key: True},
        'is_sorted': {
            type_key: int,
            desc_key: 'flag indicating whether the file is coordinate sorted (1) or not (0).',
            use_in_output_key: False},
        '1st_fragments': {
            type_key: int,
            desc_key: 'number of first fragment reads '
                      '(flags 0x01 not set; or flags 0x01 and 0x40 set, 0x80 not set).',
            use_in_output_key: True},
        'last_fragments': {
            type_key: int,
            desc_key: 'number of last fragment reads (flags 0x01 and 0x80 set, 0x40 not set).',
            use_in_output_key: True},
        'reads_mapped': {
            type_key: int,
            desc_key: 'number of reads, paired or single, that are mapped (flag 0x4 or 0x8 not set).',
            use_in_output_key: True},
        'reads_mapped_and_paired': {
            type_key: int,
            desc_key: 'number of mapped paired reads (flag 0x1 is set and flags 0x4 and 0x8 are not set).',
            use_in_output_key: True},
        'reads_unmapped': {
            type_key: int,
            desc_key: 'number of unmapped reads (flag 0x4 is set).',
            use_in_output_key: True},
        'reads_properly_paired': {
            type_key: int,
            desc_key: 'number of mapped paired reads with flag 0x2 set.',
            use_in_output_key: True},
        'reads_paired': {
            type_key: int,
            desc_key: 'number of paired reads, mapped or unmapped, that are neither secondary nor supplementary '
                      '(flag 0x1 is set and flags 0x100 (256) and 0x800 (2048) are not set).',
            use_in_output_key: True},
        'reads_duplicated': {
            type_key: int,
            desc_key: 'number of duplicate reads (flag 0x400 (1024) is set).',
            use_in_output_key: True},
        'reads_MQ0': {
            type_key: int,
            desc_key: 'number of mapped reads with mapping quality 0.',
            use_in_output_key: True},
        'reads_QC_failed': {
            type_key: int,
            desc_key: 'number of reads that failed the quality checks (flag 0x200 (512) is set).',
            use_in_output_key: False},
        'non-primary_alignments': {
            type_key: int,
            desc_key: 'number of secondary reads (flag 0x100 (256) set).',
            use_in_output_key: False},
        'supplementary_alignments': {
            type_key: int,
            desc_key: 'number of supplementary reads (flag 0x800 (2048) set).',
            use_in_output_key: False},
        'total_length': {
            type_key: int,
            desc_key: 'number of processed bases from reads that are neither secondary nor supplementary '
                      '(flags 0x100 (256) and 0x800 (2048) are not set).',
            use_in_output_key: True},
        'total_first_fragment_length': {
            type_key: int,
            desc_key: 'number of processed bases that belong to first fragments.',
            use_in_output_key: True},
        'total_last_fragment_length': {
            type_key: int,
            desc_key: 'number of processed bases that belong to last fragments.',
            use_in_output_key: True},
        'bases_mapped': {
            type_key: int,
            desc_key: 'number of processed bases that belong to reads mapped.',
            use_in_output_key: False},
        'bases_mapped_(cigar)': {  # must be escaped
            type_key: int,
            desc_key: 'number of mapped bases filtered by the CIGAR string corresponding to the read they belong to. '
                      'Only alignment matches(M), inserts(I), sequence matches(=) and sequence mismatches(X) are '
                      'counted.',
            use_in_output_key: True},
        'bases_trimmed': {
            type_key: int,
            desc_key: 'number of bases trimmed by bwa, that belong to non secondary and non supplementary reads. '
                      'Enabled by -q option.',
            use_in_output_key: True},
        'bases_duplicated': {
            type_key: int,
            desc_key: 'number of bases that belong to reads duplicated.',
            use_in_output_key: True},
        'mismatches': {
            type_key: int,
            desc_key: 'number of mismatched bases, as reported by the NM tag associated with a read, if present.',
            use_in_output_key: True},
        'error_rate': {
            type_key: float,
            desc_key: 'ratio between mismatches and bases mapped (cigar).',
            use_in_output_key: False},
        'average_length': {
            type_key: int,
            desc_key: 'ratio between total length and sequences.',
            use_in_output_key: True},
        'average_first_fragment_length': {
            type_key: int,
            desc_key: 'ratio between total first fragment length and 1st fragments.',
            use_in_output_key: True},
        'average_last_fragment_length': {
            type_key: int,
            desc_key: 'ratio between total last fragment length and last fragments.',
            use_in_output_key: True},
        'maximum_length': {
            type_key: int,
            desc_key: 'length of the longest read (includes hard-clipped bases).',
            use_in_output_key: True},
        'maximum_first_fragment_length': {
            type_key: int,
            desc_key: 'length of the longest first fragment read (includes hard-clipped bases).',
            use_in_output_key: True},
        'maximum_last_fragment_length': {
            type_key: int,
            desc_key: 'length of the longest last fragment read (includes hard-clipped bases).',
            use_in_output_key: True},
        'average_quality': {
            type_key: float,
            desc_key: 'ratio between the sum of base qualities and total length.',
            use_in_output_key: True},
        'insert_size_average': {
            type_key: float,
            desc_key: 'the average absolute template length for paired and mapped reads.',
            use_in_output_key: True},
        'insert_size_standard_deviation': {
            type_key: float,
            desc_key: 'standard deviation for the average template length distribution.',
            use_in_output_key: True},
        'inward_oriented_pairs': {
            type_key: int,
            desc_key: 'number of paired reads with flag 0x40 (64) set and flag 0x10 (16) not set or with '
                      'flag 0x80 (128) set and flag 0x10 (16) set.',
            use_in_output_key: True},
        'outward_oriented_pairs': {
            type_key: int,
            desc_key: 'number of paired reads with flag 0x40 (64) set and flag 0x10 (16) set or with flag 0x80 (128) '
                      'set and flag 0x10 (16) not set.',
            use_in_output_key: True},
        'pairs_with_other_orientation': {
            type_key: int,
            desc_key: "number of paired reads that dont fall in any of the above two categories.",
            use_in_output_key: True},
        'pairs_on_different_chromosomes': {
            type_key: int,
            desc_key: 'number of pairs where one read is on one chromosome and the pair read is on a different '
                      'chromosome.',
            use_in_output_key: True},
        'percentage_of_properly_paired_reads_(%)': {  # must be escaped
            type_key: float,
            desc_key: 'percentage of reads properly paired out of sequences.',
            use_in_output_key: True}
    }
    return dict_samtools_stats_values_to_parse


def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    @return: instance of argparse arguments
    """

    help_str = "Module to parse samtools  stats from a list of samples and export as a TSV to be opened with multiqc"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--samtools_stats_input_files', dest='samtools_stats_input_files',
                        help="input  files from samtools stats.  String of values separated by space, or a single file "
                             "name", required=True)

    parser.add_argument('--samtools_stats_parsed_output_file', dest='samtools_stats_parsed_output_file',
                        help="output file from parsing samtools stats", required=True)
    return parser.parse_args()


if __name__ == "__main__":
    main()
