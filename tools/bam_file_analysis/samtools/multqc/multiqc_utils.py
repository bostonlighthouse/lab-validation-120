"""Module for general functions to print out for MultiQC table printing"""
import re
from typing import Callable


def get_data_structure_key_names() -> (str, str, str, str):
    """
    return keys used for data structure creation and subsequent access in this project
    @return: Tuple of all values
    """

    type_key = 'type'  # the key for types, e.g. int, float
    desc_key = 'desc'  # the key for the description for each value
    use_in_output_key = 'use_in_output'  # this key will dictate whether or not it's shown in MultiQC output
    sample_key = 'Sample'  # used for the sample of each input file.  Sample needs to be uppercase
    return type_key, desc_key, use_in_output_key, sample_key


def multiqc_plot_types() -> list:
    """
    Plot types that can be used in multiqc
    @return: list of values
    https://github.com/ewels/MultiQC/blob/master/docs/custom_content.md
    """
    return ['generalstats', 'table', 'bargraph', 'linegraph', 'scatter', 'heatmap', 'beeswarm']


def get_dict_compiled_regexs_for_given_tool(function_to_get_tool_column_names: Callable,
                                            tool_type: str) -> dict:
    """
    You can speed up your searches and matches by 'compiling' the regex,
    Note the keyword compile.  This just return a dictionary of those compiled matches.
    What's nice about having them in a dictionary, you have all regexps all in one place, and then
    you can look up a given regular expression by a key when you need for it when parsing
    @param function_to_get_tool_column_names: function to get the columns names,
    @param tool_type:  What tool was used for parsing, e.g.: samtools_stats, htqc_summary_stats
    @return: Complex nested data structure of the data
    """

    # return a dictionary w/ keys bering the values to be parsed and the value being a compiled regex
    dict_compiled_regex = {}
    # all compiled regular expression keys should come from get_dict_htqc_summary_stats_column_values()
    for key, _ in function_to_get_tool_column_names().items():
        # key example: 'MEAN_ABS_COVERAGE' or 'MEAN_FILTERED_COVERAGE'.  Examples here are HTQC
        if tool_type == 'htqc_summary_stats':
            dict_compiled_regex[key] = re.compile(rf'^{key}=(\S+)')
        # key example: 'raw_total_sequences' or 'filtered_sequences'. Examples here are Samtools
        elif tool_type == 'samtools_stats':
            # the actual keys have spaces, and occasionally ()
            regex_string_to_use = key.replace('_', ' ').replace('(', '\\(').replace(')', '\\)')
            dict_compiled_regex[key] = re.compile(rf'^SN\t{regex_string_to_use}:\t(\S+)')
        else:
            raise ValueError(f"tool type not defined: '{tool_type}'")

    # return a dictionary like for HTQC:
    # {
    #   'MEAN_ABS_COVERAGE': re.compile('^MEAN_ABS_COVERAGE=(\\S+)')
    #   'MEAN_FILTERED_COVERAGE': re.compile('^MEAN_FILTERED_COVERAGE=(\\S+)'),
    #   ...
    #   ...
    # }
    # or Samtools
    # {
    #   'raw_total_sequences': re.compile('^SN\\traw total sequences:\\t(\\S+)'),
    #   'filtered_sequences': re.compile('^SN\\tfiltered sequences:\\t(\\S+)'),
    #   ...
    #   ...
    # }
    return dict_compiled_regex


def size_assertion_check(regex_dict: dict, keys_found: list):
    """
    Check to make sure the keys found during parsing the number of columns
    @param regex_dict:  Dictionary of columns that were to be found via parsing
    @param keys_found: Keys that were found during parsing

    """
    assert len(regex_dict) == len(keys_found), f"The number of parsed values does is incorrect. " \
                                               f"{len(regex_dict)} != " \
                                               f"{len(keys_found)}.  " \
                                               f"Missed parsed values for:" \
                                               f" {set(regex_dict.keys()).difference(set(keys_found))}"


def parse_tool_summary_stats_values(input_files: str, function_to_get_tool_column_names: Callable,
                                    tool_name: str, break_start_string: str = None) -> list:
    """
    Parse out the values from the htqc summary stats infile
    @param input_files: Take in the input file string, this is a string with spaces separating each file name
    @param function_to_get_tool_column_names: This should be the dictionary of dictionaries with keys as the columns
    @param tool_name: What tool was used to generate the input files
    @param break_start_string: string that is checked
    @return: list of dictionaries of values parsed for each sample
    """

    regex_dict = get_dict_compiled_regexs_for_given_tool(
        function_to_get_tool_column_names=function_to_get_tool_column_names,
        tool_type=tool_name)  # get the dictionary of compiled regular expressions
    list_of_dict_values = []  # The list of dictionaries that will be returned
    type_key, _, _, sample_key = get_data_structure_key_names()
    tool_summary_stats_column_values_dict = function_to_get_tool_column_names()
    for sample_num, input_file in enumerate(input_files.split()):
        sample_cid_values = {}  # final parsed values for the CID
        with open(input_file, encoding='UTF-8') as in_fh:
            # the keys found- Keep track. Make sure it's the same number as the len(regex_dict) for each sample
            keys_found = []
            sample_cid_values[sample_key] = _get_generic_multiqc_sample_name(input_file, sample_num)
            for line in in_fh:
                if break_start_string and line.startswith(break_start_string):  # Makes parsing more efficient
                    break
                # go over all column names that are guaranteed to have a compiled regular expression regex_dict
                # at this point.
                for column_name in tool_summary_stats_column_values_dict.keys():
                    match = regex_dict[column_name].match(line.rstrip())
                    if match:  # was there a match?
                        # type and store the value
                        sample_cid_values[column_name] = \
                            tool_summary_stats_column_values_dict[column_name][type_key](match.group(1))
                        keys_found.append(column_name)  # was the column_name found
                        break  # go to next line to be parsed
        list_of_dict_values.append(sample_cid_values)  # add the sample_cid_values onto the list.  Sample completed
        # quick assert all values were found during parsing for each sample
        size_assertion_check(regex_dict, keys_found)

    # Data structure being returned being returned for HTQC
    # [
    #    # 1st sample
    #        {'Sample': 'cid21-1032', 'MEAN_ABS_COVERAGE': 46.312, 'MEAN_FILTERED_COVERAGE': 15.008, .....
    #    # next sample
    #        {'Sample': 'cid21-1025', 'MEAN_ABS_COVERAGE': 152.482, 'MEAN_FILTERED_COVERAGE': 39.387, .....
    # ]
    # Data structure being returned for Samtools
    # [
    #    # 1st sample
    #        {'Sample': 'cid21-1005', 'raw_total_sequences': 21203406, 'filtered_sequences': 0, .....
    #    # next sample
    #        {'Sample': 'cid21-1006', 'raw_total_sequences': 20202208, 'filtered_sequences': 0, .....
    # ]

    return list_of_dict_values


def _get_generic_multiqc_sample_name(input_file: str, sample_num: int):
    """
    Get a sample name that will be placed when we don't have a CID that matched
    @param sample_num:  Just append a sample num
    """
    sample_key = get_data_structure_key_names()[3]
    match = re.match(r'.*(cid\d+-\d+)', input_file)  # get CID from file
    if match:
        cid_name = match.group(1)
    else:  # if this does not match give it a generic name
        cid_name = f'{sample_key}_num_{sample_num}'

    return cid_name


def print_multiqc_output_file(list_of_dict_tool_values: list, output_file: str,
                              section_name: str, description: str, namespace: str,
                              function_to_get_tool_column_names: Callable,
                              plot_type: str = 'table', sig_digits: int = 1) -> None:
    """
    @param list_of_dict_tool_values: The list of dictionaries containing the values and column names to be printed
    @param function_to_get_tool_column_names: This should be the dictionary of dictionaries with keys as the columns
    that were parsed.  Note, these will be used to print out the config section for the output file multiqc requires
    See function_to_get_tool_column_names() as an example that was passed in
    @param output_file: The outfile name to print the compiled data to
    @param section_name: Section name of the top header section
    @param description: Description for the section in the HTML output
    @param namespace: Namespace for the table
    @param plot_type:  MultiQC table type, default table
    plot_type: generalstats | table | bargraph | linegraph | scatter | heatmap | beeswarm
    # https://github.com/ewels/MultiQC/blob/master/docs/custom_content.md
    @param sig_digits:  Significant digits for floats in the table, default 1

    Print out the TSV file that can be opened via multiqc.  This function is abstract and should work for other
    list of dictionaries (list_of_dict_tool_values) as long as it follows the pattern:
    [
        # 1st sample
            {'Sample': 'cid21-1005', 'raw_total_sequences': 21203406, 'filtered_sequences': 0, .....
        # next sample
            {'Sample': 'cid21-1006', 'raw_total_sequences': 20202208, 'filtered_sequences': 0, .....
    ]
    Importantly, Sample is the first key in the data structure above (list_of_dict_tool_values).

    And the function name passed (function_name_for_list_tool_values) returns a DS as a dictionary of dictionaries:
    {
        'raw_total_sequences': {
            'desc': 'total number of reads in a file, excluding supplementary and secondary reads. Same '
            'number reported by samtools view -c.',
            'type': <class 'int'>,
            'use in output': True
        },
        'filtered_sequences': {
            'desc': 'number of discarded reads when using -f or -F option.',
            'type': <class 'int'>,
             'use in output': False
        },
        ....
    }
    ****
    Notice the first data structure has Sample name, while the second one does not.  The Sample type is not printed
    in the header config section, it's reserved for the first value in the table of values after the header config
    section
    ****
`   Here is an example of what the header config section looks like (only 2 columns shown here)

    # plot_type: 'table'
    # section_name: 'Samtools Alignment Stats'
    # description: 'Summary stats describing the number of reads which mapped and passed various filters'
    # pconfig:
    #     namespace: 'Samtools'
    # headers:
    #     raw_total_sequences:
    #          title: 'raw_total_sequences'
    #          description: 'total number of reads in a file, excluding supplementary and secondary reads.'
    #          format: '{:,.0f}'
    #          placement: 1000
    #          hidden: False
    #     filtered_sequences:
    #          title: 'filtered_sequences'
    #          description: 'number of discarded reads when using -f or -F option.'
    #          format: '{:,.0f}'
    #          placement: 1010
    #          hidden: True

    """
    # will the plot type passed in work with multiqc?
    if plot_type not in (multiqc_plot_types()):
        raise ValueError(f"{plot_type} not in {multiqc_plot_types()}")
    padding = ' ' * 5  # multiqc requires padding- it must start after the # sign.  Should be a minimum of 2, using 5
    type_key, desc_key, use_in_output_key, _ = get_data_structure_key_names()
    with open(output_file, 'w', encoding='UTF-8') as out_fh:
        # begin to print out the header config section
        print(f"# plot_type: '{plot_type}'", file=out_fh)
        print(f"# section_name: '{section_name}'", file=out_fh)
        print(f"# description: '{description}'",
              file=out_fh)
        print("# pconfig:", file=out_fh)
        print(f"#{padding}namespace: '{namespace}'", file=out_fh)
        print("# headers:", file=out_fh)
        placement = 1000  # following same format as example
        # call to function to get the values to be printed in the header config section
        for title, key_dict in function_to_get_tool_column_names().items():
            print(f"#{padding}{title}:", file=out_fh)
            # Short title, table column title
            print(f"#{padding}{padding}title: '{title}'", file=out_fh)
            # Longer description, goes in mouse hover text
            print(f"#{padding}{padding}description: '{key_dict[desc_key]}'", file=out_fh)
            # int or float formatting.  Float formatting is based on the number of digits passed in by sig_digits
            format_string = "'{:,.0f}'" if key_dict[type_key] is int else f"'{{:,.{sig_digits}f}}'"
            # Output format() string
            print(f"#{padding}{padding}format: {format_string}", file=out_fh)
            # Alter the default ordering of columns in the table
            print(f"#{padding}{padding}placement: {placement}", file=out_fh)
            # Set to True to hide the column on page load
            print(f"#{padding}{padding}hidden: {not bool(key_dict[use_in_output_key])}", file=out_fh)
            placement += 10

        # print out the actual values parsed from samtools stats for each sample
        for sample_num, return_values in enumerate(list_of_dict_tool_values):
            if sample_num == 0:  # only print out header if it's the first sample in the data structure
                print("\t".join(list(return_values.keys())), file=out_fh)
            print("\t".join([str(v) for v in return_values.values()]), file=out_fh)
