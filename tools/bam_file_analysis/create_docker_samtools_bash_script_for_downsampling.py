import argparse
import os
import pprint as pp
import re
import sys


def main():
    args = get_cli_args()
    cid_list = []

    file_to_open = None
    if args.cosmos_file is not None:
        file_to_open = args.cosmos_file
    elif args.cid_file is not None:
        file_to_open = args.cid_file
    else:
        sys.exit("Don't know what to do...")

    with open(file_to_open, 'r') as in_fh:
        for i, line in enumerate(in_fh):
            if args.cosmos_file is not None:
                # string coming in looks like this:
                # <Execution[4697] 2021out23-oc180-r53--b21-151-cid21-2040-snapshot>   yes   Successfully Finished   2021-10-25 02:05:43.311088  2021-10-25 10:17:36.976671  8:11:53.665583
                cid = line.strip().split(" ")[1]  # split off the cid
                match = re.findall(r"(b\d+-\d+)-(cid\d+-\d+)", cid)  # match the batch and cid
                if 'snapshot' in line:
                    cid = match[0][1]
                    cid_list.append(cid)

    bash_string = " ".join([f'"{x}"' for x in cid_list])

    print(f"cd /export/pipeline/; for i in {bash_string}; do ls /export/bamstore/snapshotGSV2/*$i-*bam; done;",
          file=sys.stderr)

    print("After running command above, enter the BAM files, and then hit Control-D", file=sys.stderr)
    bams = sys.stdin.readlines()

    bams = [bam.rstrip() for bam in bams]

    print("#!/bin/bash")
    print("set -e")
    cid = None
    all_bams = []
    for bam in bams:
        match = re.search('(cid\d+-\d+)', bam)
        if match:
            cid = match.group(1)
        # avoid
        #  /export/bamstore/snapshotGSV2/cid21-1091-rerun.B21-000-TEST-SNAPSHOT.CID21-1098.A548_N706.tumor.all_chroms.bam

        all_bams.append(bam)
        for percentage in (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7):
            bam_no_ext, ext = os.path.splitext(bam)
            downsampled_bam_file = f"{bam_no_ext}_{percentage}.bam"
            downsampled_bam_file = os.path.basename(downsampled_bam_file)
            downsampled_bam_file = f"/export/scratch/samtools/bams/{downsampled_bam_file}"
            print(f"docker run --rm -v /export/bamstore/:/export/bamstore/ "
                  f"-v /export/scratch/samtools/bams/:/export/scratch/samtools/bams/ "
                  f"staphb/samtools:latest "
                  f"samtools view -b --subsample {percentage} {bam} "
                  f"--threads 5 "
                  f">{downsampled_bam_file}")
            all_bams.append(downsampled_bam_file)

    [print(file, file=sys.stderr) for file in all_bams]

def print_ds_for_reproducibility_vcf(file=None, path1=None, path2=None, sample_num=None):
    """
    Print data structure for reproducibility analysis
    @param file: The file to open for the data structure
    @param path1: Destination of directories found in --pair_file (1st entry)
    @param path2: Destination of directories found in --pair_file (2nd entry)
    @param sample_num:  Just an int on where to start printing
    """


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Go off and query the LIMS for a CID result object.  One cid per line"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--cosmos_file', dest='cosmos_file', help="List of CIDs to get, one per line", default=None)
    return parser.parse_args()


if __name__ == "__main__":
    main()