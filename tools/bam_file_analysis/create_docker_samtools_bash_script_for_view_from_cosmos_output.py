import argparse
import os
import pprint as pp
import re
import sys


def main():
    args = get_cli_args()
    cid_list = []
    cid_to_data = {}

    file_to_open = None
    if args.cosmos_file is not None:
        file_to_open = args.cosmos_file
    elif args.from_excel_file is not None:
        file_to_open = args.from_excel_file
    elif args.cid_file is not None:
        file_to_open = args.cid_file
    else:
        sys.exit("Don't know what to do...")
    with open(file_to_open, 'r') as in_fh:
        for i, line in enumerate(in_fh):
            mcc = None
            cid = None
            gte = None
            sample_qc = None
            batch = None
            assay = None
            if args.cosmos_file is not None:
                # string coming in looks like this:
                # <Execution[4697] 2021out23-oc180-r53--b21-151-cid21-2040-snapshot>   yes   Successfully Finished   2021-10-25 02:05:43.311088  2021-10-25 10:17:36.976671  8:11:53.665583
                cid = line.strip().split(" ")[1]  # split off the cid
                match = re.findall(r"(b\d+-\d+)-(cid\d+-\d+)", cid)  # match the batch and cid
                if 'snapshot' in line or 'fusion' in line:
                    cid = match[0][1]
                    cid_list.append(cid)
            elif args.cid_file is not None:
                cid = line.strip()
                cid_list.append(cid)
                exit("Not tested")
            elif args.from_excel_file is not None and i > 0:
                values = line.rstrip().split("\t")
                cid = values[3].replace('CID', 'cid')  # fourth column from the excel data
                mcc = values[7]
                sample_qc = values[-2]
                gte = values[-3]
                cid_list.append(cid)
                batch = values[1]
                assay = values[2]

            cid_to_data[cid] = {'MCC': mcc, 'Sample_QC': sample_qc, 'Frac_GTE': gte, 'Batch': batch, 'Assay': assay}

            #if i == 2:
            #    break


    bash_string = " ".join([f'"{x}"' for x in cid_list])

    print(f"cd /export/bamstore/; for i in {bash_string}; do ls /export/bamstore/fusion/*$i-*bam; done;",
          file=sys.stderr)

    print("After running command above, enter the BAM files, and then hit Control-D", file=sys.stderr)
    bams = sys.stdin.readlines()

    bams = [bam.rstrip() for bam in bams]

    print("#!/bin/bash")
    print("set -e")
    cid = None
    past_cid = None
    for bam in bams:
        match = re.search('(cid\d+-\d+)', bam)
        if match:
            cid = match.group(1)
        # avoid
        #  /export/bamstore/snapshotGSV2/cid21-1091-rerun.B21-000-TEST-SNAPSHOT.CID21-1098.A548_N706.tumor.all_chroms.bam
        if 'TEST-SNAPSHOT' in bam:
            continue
        if 'vep104' in bam:
            continue
        if args.echo_string:
            echo = f"echo 'running {cid} " \
                   f"Batch {cid_to_data[cid]['Batch']} " \
                   f"Assay {cid_to_data[cid]['Assay']} " \
                   f"MCC {cid_to_data[cid]['MCC']} " \
                   f"Sample_QC {cid_to_data[cid]['Sample_QC']} " \
                   f"Frac_GTE {cid_to_data[cid]['Frac_GTE']}'"
        base, ext = os.path.splitext(bam)
        basename = os.path.basename(base)
        output_file_dedup = f"{basename}.samtools_stats.dedup.txt"
        """
        if args.echo_string:
            print(f"{echo} >{output_file_dedup}")

        print(f"docker run --rm -v /export/bamstore/:/export/bamstore/ "
              f"-v /export/scratch/samtools/bams/:/export/scratch/samtools/bams/ "
              f"staphb/samtools:latest "
              f"samtools view --threads 5  -bF 0x400 {bam} | "
              f"samtools stats "
              f"--threads 5 "
          f">{output_file_dedup}")
        """
        output_file_with_dups = f"{basename}.samtools_stats.with_dups.txt"
        if args.echo_string:
            print(f"{echo} >{output_file_with_dups}")
        print(f"docker run --rm -v /export/bamstore/:/export/bamstore/ "
              f"-v /export/scratch/samtools/bams/:/export/scratch/samtools/bams/ "
              f"-v /export/scratch/samtools/:/export/scratch/samtools/ "
              f"staphb/samtools:latest "
              f"samtools stats {bam} "
              f"--threads 5 "
              f"-t /export/scratch/samtools/VariantPlex_OC_v3_16897_primers_targets.sorted.nochr.bed "
              f">{output_file_with_dups}")
        if past_cid == cid:
            print(f"found CID {cid} again", file=sys.stderr)
        past_cid = cid

def print_ds_for_reproducibility_vcf(file=None, path1=None, path2=None, sample_num=None):
    """
    Print data structure for reproducibility analysis
    @param file: The file to open for the data structure
    @param path1: Destination of directories found in --pair_file (1st entry)
    @param path2: Destination of directories found in --pair_file (2nd entry)
    @param sample_num:  Just an int on where to start printing
    """


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Go off and query the LIMS for a CID result object.  One cid per line"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--cosmos_file', dest='cosmos_file', help="List of CIDs to get, one per line", default=None)
    parser.add_argument('--from_excel_file', dest='from_excel_file', help="List snapshot data from excel (gs180-runs.xlsx",
                        default=None)
    parser.add_argument('--cid_file', dest='cid_file', help="List of CIDs - one per line", default=None)
    parser.add_argument('--echo_string', dest='echo_string', action='store_true')
    parser.add_argument('--no_echo_string', dest='echo_string', action='store_false')
    parser.set_defaults(echo_string=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()