from Bio import Entrez, SeqIO
import sys
# Reopen sys.stdout with line buffering
sys.stdout = open(sys.stdout.fileno(), mode='w', buffering=1)

from tools.cnv.utils import get_list_important_cnv_genes

# Set your email for NCBI Entrez
Entrez.email = "cleslin@bostonlighthouse.us"


def fetch_gene_sequence(gene_name):
    try:
        # Search for the gene in the NCBI Nucleotide database
        search_handle = Entrez.esearch(db="nucleotide", term=gene_name + "[Gene]", retmax=1)
        search_results = Entrez.read(search_handle)
        search_handle.close()

        # Get the ID of the first result
        if search_results["IdList"]:
            gene_id = search_results["IdList"][0]
        else:
            print(f"Gene {gene_name} not found.")
            return None

        # Fetch the gene sequence
        fetch_handle = Entrez.efetch(db="nucleotide", id=gene_id, rettype="gb", retmode="text")
        record = SeqIO.read(fetch_handle, "genbank")
        fetch_handle.close()

        # Check if the sequence is valid
        sequence = str(record.seq)
        if not sequence:  # Empty sequence check
            print(f"Gene {gene_name} returned an empty sequence.")
            return None

        return sequence

    except Exception as e:
        print(f"An error occurred while fetching the sequence for {gene_name}: {e}")
        return None


def calculate_gc_content(sequence):
    # Ensure the sequence is uppercase to count both 'g' and 'G', 'c' and 'C'
    sequence = sequence.upper()

    # Calculate the number of G and C bases
    gc_count = sequence.count('G') + sequence.count('C')

    # Calculate GC content as a percentage
    gc_content = (gc_count / len(sequence)) * 100

    return gc_content


def get_gc_content_for_genes(genes_list):
    gc_contents = {}
    for gene in genes_list:
        sequence = fetch_gene_sequence(gene)
        if sequence:
            gc_content = calculate_gc_content(sequence)
            gc_contents[gene] = gc_content
        else:
            gc_contents[gene] = None
    return gc_contents


# Example usage
genes_list = get_list_important_cnv_genes()
gene_list = genes_list[0:2]
gc_content_results = get_gc_content_for_genes(genes_list)
gc_content_dict = {}
for gene, gc_content in gc_content_results.items():
    if gc_content is not None:
        print(f'{gene}: GC Content = {gc_content:.2f}%')
        gc_content_dict[gene] = f'{gc_content:.2f}'
    else:
        print(f'{gene}: Sequence not found.')
        gc_content_dict[gene] = None

print(gc_content_dict)