import requests
from Bio import Entrez, SeqIO
import sys

from tools.cnv.utils import get_list_important_cnv_genes

# Reopen sys.stdout with line buffering
sys.stdout = open(sys.stdout.fileno(), mode='w', buffering=1)

# Set your email for NCBI Entrez
Entrez.email = "your.email@example.com"

def main():
    # Example usage
    genes_list = get_list_important_cnv_genes()
    gc_content_results = get_gc_content_for_genes(genes_list)

    gc_content_dict = {}
    for gene, contents in gc_content_results.items():
        gene_gc = contents["gene_gc_content"]
        exon_gc = contents["exon_gc_content"]
        if gene_gc is not None:
            print_string = f'{gene}: Whole Gene GC Content = {gene_gc:.2f}% | '
        else:
            print_string = f'{gene}: Whole Gene Sequence not found or could not be processed. | '

        if exon_gc is not None:
            print_string += f'{gene}: Exon Only GC Content = {exon_gc:.2f}%'
        else:
            print_string += f'{gene}: Exon Sequence not found or could not be processed.'
        print(print_string)
        gc_content_dict[gene] = {
            "gene_gc_content": f'{gene_gc:.2f}%' if gene_gc is not None else None,
            "exon_gc_content": f'{exon_gc:.2f}%' if exon_gc is not None else None
        }

    print(gc_content_dict)

def fetch_canonical_transcript(gene_name):
    """Fetches the canonical transcript ID for a given gene from Ensembl."""
    server = "https://rest.ensembl.org"
    ext = f"/lookup/symbol/homo_sapiens/{gene_name}?expand=1"

    headers = {"Content-Type": "application/json"}
    r = requests.get(server + ext, headers=headers)

    if not r.ok:
        r.raise_for_status()
        return None

    decoded = r.json()
    for transcript in decoded.get('Transcript', []):
        if transcript.get('is_canonical'):
            return transcript['id']  # Return the canonical transcript ID

    print(f"Canonical transcript for {gene_name} not found.")
    return None


def fetch_exon_sequences(transcript_id):
    """Fetches exon sequences for a given transcript from Ensembl."""
    server = "https://rest.ensembl.org"
    ext = f"/sequence/id/{transcript_id}?type=genomic;expand=exon"

    headers = {"Content-Type": "text/x-fasta"}
    r = requests.get(server + ext, headers=headers)

    if not r.ok:
        r.raise_for_status()
        return None

    fasta_data = r.text
    sequence = "".join(line.strip() for line in fasta_data.splitlines() if not line.startswith(">"))
    return sequence


def fetch_gene_sequence(gene_name):
    """Fetches the entire gene sequence from NCBI."""
    try:
        search_handle = Entrez.esearch(db="nucleotide", term=gene_name + "[Gene]", retmax=1)
        search_results = Entrez.read(search_handle)
        search_handle.close()

        if search_results["IdList"]:
            gene_id = search_results["IdList"][0]
        else:
            print(f"Gene {gene_name} not found.")
            return None

        fetch_handle = Entrez.efetch(db="nucleotide", id=gene_id, rettype="gb", retmode="text")
        record = SeqIO.read(fetch_handle, "genbank")
        fetch_handle.close()

        sequence = str(record.seq)
        if not sequence:  # Empty sequence check
            print(f"Gene {gene_name} returned an empty sequence.")
            return None

        return sequence

    except Exception as e:
        print(f"An error occurred while fetching the sequence for {gene_name}: {e}")
        return None


def calculate_gc_content(sequence):
    """Calculates the GC content of a DNA sequence."""
    sequence = sequence.upper()
    gc_count = sequence.count('G') + sequence.count('C')
    gc_content = (gc_count / len(sequence)) * 100
    return gc_content


def get_gc_content_for_genes(genes_list):
    """Fetches GC content for genes (whole gene and exons only for canonical transcript)."""
    gc_contents = {}
    for gene in genes_list:
        if gene == 'MRE11A':
            gene = 'MRE11'
        gene_gc_content = None
        exon_gc_content = None

        # Fetch the entire gene sequence
        gene_sequence = fetch_gene_sequence(gene)
        if gene_sequence:
            gene_gc_content = calculate_gc_content(gene_sequence)

        # Fetch the canonical transcript and its exons
        transcript_id = fetch_canonical_transcript(gene)
        if transcript_id:
            exon_sequence = fetch_exon_sequences(transcript_id)
            if exon_sequence:
                exon_gc_content = calculate_gc_content(exon_sequence)

        gc_contents[gene] = {
            "gene_gc_content": gene_gc_content,
            "exon_gc_content": exon_gc_content
        }

    return gc_contents



if __name__ == '__main__':

