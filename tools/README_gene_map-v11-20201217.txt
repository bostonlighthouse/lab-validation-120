 \time pipenv run python3 -m tools.create_oc120plust_interval_file_gs_v2 \
 --no_match_file no_match_gene.txt \
 --not_in_gs_2 not_in_gs_2.txt \
 --json_file_name_final_genes gs-v2.0-gene-list-v19-20201208.json \
 --bed_file_2_print VariantPlex_OC_v3_16897_primers_targets.sorted.subset.gs2.0.bed \
 >genes7.py


{
    'data': {
          "CAMTA1": {   # gene name
            "cnv_meta_data": {
              "is_clinical": false,   # should it be reported on during sign-out (It was not fitered at VV step)
              "is_technical": true,   # can the gene be used for CNV calling, must have num_primers >= 3
                                      # These will be filterd at VV
              "is_control": false,    # Was this a control gene for CNV, at this point Archer did not include controls
              "num_primers": 4        # must have num_primers >= 3 to be considered is_technical
            },
            "fusion_meta_data": {
              "is_clinical": false,   # should it be reported on during sign-out (It was not fitered at VV step)
              "is_technical": false,  # can the gene be used for Fusion calling, at this point all Fusions can be used
                                      # The # of is_clinical is True should be thes same as is_techncial True
              "is_control": false     # Was this a control gene for Fusion,
            },
            "snv_meta_data": {        # all data related to SNV/INDELS
              "is_clinical": false,   # should it be reported on during sign-out (It was not fitered at VV step)
              "is_technical": false,  # These will be filterd at VV
              "intervals": [          # All intervals we look at for SNV/INDELS
                {
                  "start": 6846842,
                  "stop": 6846843
                },
                {
                  "start": 6894652,
                  "stop": 6894653
                },
                {
                  "start": 7525813,
                  "stop": 7525814
                },
                {
                  "start": 7820874,
                  "stop": 7820875
                }
              ],
              "hotspots": [           # defined as only 1 nt interval that was not a SNP (snps), see LY9 gene below
                {
                  "start": 6846842,
                  "stop": 6846843
                },
                {
                  "start": 6894652,
                  "stop": 6894653
                },
                {
                  "start": 7525813,
                  "stop": 7525814
                },
                {
                  "start": 7820874,
                  "stop": 7820875
                }
              ],
              "snps": []
            },
            "refseq_transcripts": {  # no snvs, no fusions, this was only a hotspots or only a snps, potentially both
              "snvs": {},
              "fusions": {}
            },
            "preferred_ensembl_transcript": null
          },

          # example with only a SNP

            "LY9": {
            "cnv_meta_data": {
              "is_clinical": false,
              "is_technical": false,
              "is_control": false,
              "num_primers": null
            },
            "fusion_meta_data": {
              "is_clinical": false,
              "is_technical": false,
              "is_control": false
            },
            "snv_meta_data": {
              "is_clinical": false,
              "is_technical": false,
              "intervals": [
                {
                  "start": 160786669,
                  "stop": 160786670
                }
              ],
              "hotspots": [],
              "snps": [              # gene was only a SNP spot we look at
                {
                  "start": 160786669,
                  "stop": 160786670,
                  "ref_allele": "A",
                  "alt_allele": "G",
                  "alt_allele_freq": 0.33726,
                  "rs_number": "rs560681"
                }
              ]
            },
            "refseq_transcripts": {
              "snvs": {},
              "fusions": {}
            },
            "preferred_ensembl_transcript": null
          },


          # example with both hotspots and intervals that have transcipts and a preferred_ensembl_transcript
          "DDR2": {
            "cnv_meta_data": {
              "is_clinical": false,
              "is_technical": true,
              "is_control": false,
              "num_primers": 18
            },
            "fusion_meta_data": {
              "is_clinical": false,
              "is_technical": false,
              "is_control": false
            },
            "snv_meta_data": {
              "is_clinical": true,
              "is_technical": true,
              "intervals": [
                {
                  "start": 162688851,
                  "stop": 162688937
                },
                {
                  "start": 162725721,
                  "stop": 162725722
                },
                {
                  "start": 162735780,
                  "stop": 162735781
                },
                {
                  "start": 162740089,
                  "stop": 162740304
                },
                {
                  "start": 162741811,
                  "stop": 162742039
                },
                {
                  "start": 162743256,
                  "stop": 162743388
                },
                {
                  "start": 162745439,
                  "stop": 162745635
                },
                {
                  "start": 162745923,
                  "stop": 162746162
                },
                {
                  "start": 162748367,
                  "stop": 162748521
                },
                {
                  "start": 162749899,
                  "stop": 162750038
                }
              ],
              "hotspots": [
                {
                  "start": 162725721,
                  "stop": 162725722
                },
                {
                  "start": 162735780,
                  "stop": 162735781
                }
              ],
              "snps": []
            },
            "refseq_transcripts": {
              "snvs": {
                "NM_006182": {
                  "exons": [
                    3,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18
                  ]
                }
              },
              "fusions": {}
            },                                  (ENSEMBL)
            "preferred_ensembl_transcript": "ENST00000367922.3"
          },
    },

    # here is the validation data for this JSON
     "validation": {
            "num_genes_in_map": {
              "num": 217,
              "doc_str": "The total number of genes is 217"
            },
            "at_least_one_clinical_match": {
              "num": 146,
              "doc_str": "The total number of clinical genes is 146"
            },
            "at_least_one_technical_match": {
              "num": 189,
              "doc_str": "The total number of technical genes is 189"
            },
            "non_clinical_match": {
              "num": 71,
              "doc_str": "The total number of non clinical genes is 71"
            },
            "non_clinical_technical_match": {
              "num": 28,
              "doc_str": "The total number of non clinical and non technical genes is 28"
            },
            "clinical_snv_match": {
              "num": 105,
              "doc_str": "The total number of clinical snv genes is 105"
            },
            "clinical_cnv_match": {
              "num": 13,
              "doc_str": "The total number of clinical cnv genes is 13"
            },
            "clinical_fusion_match": {
              "num": 57,
              "doc_str": "The total number of clinical fusion genes is 57"
            }
          }
       }
}



             c                    n              n          n
p------------1-------------------1---------------1----------1----------||----------------1--------------1-------------1-------------------1----q

BIG list of GENES on the PDF report

A -B- C  D  E  -F-  -G-    # this -B- would be a gene that was is_clinical = True, and the other are is_technical = True, for either CNV, SNV(INDEL), Fusions

** Gene that are in bold had some clinical or biological relevance

 (NCBI)
NM_0000001      Exon 1                       Intron 1                Exon 2
Gene A      -------------------------|--------------------|---------------      Fair Game for SNV is_technical = True
NM_0000001      Exon 2                             Intron 1          Exon 2
Gene A      -------------------------------------|--------|----------------     Fair Game for SNV is_technical = True

GTF         -    -     --     -    -                       -    --   -   -

Intervals  --------------------------                   --------------------



            Exon 1                       Intron 1                 Exon 2
Gene B     -------------------------|--------------------|---------------     For for SNV calling is_technical = False
GTF                     - (snp)
Intervals               -
                        
                        ref A>C
Orthogonal test         C



            Exon 1                       Intron 1                 Exon 2
Gene C     -------------------------|--------------------|---------------
GTF         - (hotspots) - (hotspots)                       - (hotspots)
Intervals   -            -                                  -
NGS       --------     ------- 151bp



