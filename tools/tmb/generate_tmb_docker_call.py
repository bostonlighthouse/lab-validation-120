import argparse
import os
import sys

from tools.utils.tools_utils import return_file_type
from lab_validation_120.utils.assay_utils import get_assay_names

def main():
    args = get_cli_args()
    input_file = args.pipeline_directories
    path = args.path
    assay_name = args.assay_name
    output_dir = args.output_dir
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    vcf_files = get_list_vcf_files(input_file=input_file, path=path)
    process_vcf_files(vcf_files=vcf_files, path=path, assay_name=assay_name, output_dir=output_dir)


def process_vcf_files(vcf_files: list = None,  path: str = None, assay_name: str = None,
                      output_dir: str = None) -> None:
    cleslin = '/Users/cleslin/BLI-repo/lab-validation-120'
    local_ouput_data_dir = f"{cleslin}/{output_dir}"
    tmb_repo = '/Users/cleslin/BLI-repo/tmb-caller'
    docker_output_dir = '/data'
    for i, vcf_file in enumerate(vcf_files):
        file, ext = os.path.splitext(vcf_file)
        path, base_file = os.path.split(file)
        final_mount_path, vcf_dir = os.path.split(path)
        if i == 0:
            print("Run this to launch the container:\n\n", file=sys.stderr)
            print(f"docker run --rm -it \\", file=sys.stderr)
            print(f"-v {cleslin}/{final_mount_path}:/input \\", file=sys.stderr)
            print(f"-v {local_ouput_data_dir}:{docker_output_dir} \\", file=sys.stderr)
            print(f"-v {tmb_repo}/tmb_caller:/app/tmb_caller \\", file=sys.stderr)
            print(f"-v {tmb_repo}/scratch:/app/scratch \\", file=sys.stderr)
            print(f"-v {tmb_repo}/helpers:/app/helpers \\", file=sys.stderr)
            print("--env-file .env \\", file=sys.stderr)
            print("tmb-caller:python3  bash\n", file=sys.stderr)
            print("apt install time;")

        print("\\time pipenv run python -m tmb_caller.main \\")
        print(f"--vcf_file  /input/{vcf_dir}/{base_file}.vcf \\")
        if assay_name == 'gs180':
            print(f"--assay_name snapshotGSV2 \\")
        elif assay_name == 'gs395':
            print(f"--assay_name snapshot395 \\")
        elif assay_name == 'gs700':
            print(f"--assay_name snapshot700 \\")
        else:
            raise ValueError(f"did not implement assay_name: {assay_name}")
        print(f"--tmb_output_file {docker_output_dir}/{base_file}.tmb \\")
        print(f"--tmb_variant_tsv_output_file {docker_output_dir}/{base_file}.tmb.tsv \\")
        print(f"--tmb_variant_json_output_file {docker_output_dir}/{base_file}.tmb.json")
        #print(f"--remove_cosmic --remove_pon_variants ")
        print(f'\necho "completed {base_file}"\n')

def get_list_vcf_files(input_file: str = None, path: str = None) -> list:
    """
    Get a list of VCF files to process with docker
    :param input_file: Cider pipeline output directories to serach for the VCF file to call TMB on
    :param path: The path to the directories
    """

    with open(input_file) as fh:
        vcf_data = []
        for i, line in enumerate(fh):
            if line.startswith('#'):
                continue
            dir1 = line.rstrip()
            complete_dir = '/'.join((path, dir1))
            vcf_file = return_file_type(dir=complete_dir, file_ext=".vcf")
            vcf_data.append(vcf_file)
    return vcf_data

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open a list of cider output files and create a docker bash script '
                                                 'for TMB calling on those VCF files found')

    parser.add_argument('--pipeline_directories', dest='pipeline_directories',
                        type=str, help='Cider pipeline output directories to serach for the VCF file to call TMB on ',
                        required=True)
    parser.add_argument('--path', dest='path',
                        type=str, help='Destination of directories found in --pipeline_directories ',
                        required=True)
    parser.add_argument('--assay_name', dest='assay_name',
                        type=str, help='Assay name', choices=get_assay_names(),
                        required=True)
    parser.add_argument('--output_dir', dest='output_dir',
                        type=str, help="Directory where to store the data in the bash script created",
                        required=True)


    return parser.parse_args()


if __name__ == "__main__":
    main()