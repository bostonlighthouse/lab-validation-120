"""
Module to open the JSON file from GS180 and GS395
To process and look for frequently occurring germline mutations

Snapshot395: http://analytics3.cgs.bostonlighthouse.us/question/200-reportable-tmb-variants-derivative-snapshot395
SnapshotGSV2: http://analytics3.cgs.bostonlighthouse.us/question/198-reportable-tmbs-variants-derivative-snapshotgsv2

The JSON from teach table was joined here

"""
import pandas as pd
import json
import argparse
from tools.utils.tools_utils import get_cid_from_string

def main() -> None:
    """
    Businness Logic
    :return: None
    """
    args = get_cli_args()
    gs395_json_file = args.gs395_json_file
    gs180_json_file = args.gs180_json_file

    # get the DataFrames
    df_gs395 = get_df(json_file=gs395_json_file)
    temp_df = df_gs395.drop_duplicates(subset=['cid'])
    temp_df = temp_df[['cid', 'PrimarySite', 'PrimarySiteDiagnosis']]
    temp_df.to_excel("gs395.xlsx")
    quit()
    df_gs180 = get_df(json_file=gs180_json_file)
    # compare a set of the columns to verify they are the same
    df_gs180_cols = set(df_gs180.columns)
    df_gs395_cols = set(df_gs395.columns)
    assert df_gs395_cols == df_gs180_cols, "Column names are different"

    # concat these two DataFrames together
    temp_df_final = pd.concat([df_gs180, df_gs395], ignore_index=True, axis=0)
    temp_df_final.columns = temp_df_final.columns.str.replace(" ", "_")
    temp_df_final.columns = temp_df_final.columns.str.replace("__", "_")
    temp_df_final['IGV'] = temp_df_final['IGV'].str.replace("merge=false", "merge=ask")

    # the tmb classification is sometimes subject, i.e. it might be 14.745 and put down as inconclusive
    # these next two steps just use the true thresholds
    temp_df_final['tmb_classification'] = temp_df_final.apply(lambda row: tmb_class(row=row), axis=1)
    temp_df_final['msi_classification'] = temp_df_final.apply(lambda row: msi_class(row=row), axis=1)

    # get the final DataFrame with the number of  times a var_id occurred, plus aggregated results
    df_final = get_final_df(df=temp_df_final)
    df_final['IS_CONTROL_SAMPLE'] = df_final.apply(lambda row: is_control_sample(row=row), axis=1)
    # only use out certain columns I need
    df_final = df_final[return_final_columns()]
    df_final.to_excel("final_tmb_var_id_results2.xlsx")


def is_control_sample(row: pd.Series) -> str:
    """
    Helper function to classify if the variant_id was assocaited with a control sample.  Here there will be the same
    derivative (num_derivatives == 1) and the number of times it was used in TMB calculation (num_times_used_for_tmb)
    was >=4.
    :param row: pd.Series used in the df.apply
    :return: str
    """
    if row['num_derivatives'] == 1 and row['num_times_used_for_tmb'] >= 4:
        return 'TRUE'
    return 'UNKNOWN'


def tmb_class(row: pd.Series) -> str:
    """
    Helper function to classify TMB based on the validated TMB thresholds given the assay
    :param row: pd.Series used in the df.apply
    :return: string HIGH / LOW
    """
    tmb_threshold = 14.745  # validated threshold for GS180
    if row['assay'] == 'snapshot395':
        tmb_threshold = 10.00  # validated threshold for GS395
    if row['TMBScore'] >= tmb_threshold:
        return 'HIGH'
    return 'LOW'


def msi_class(row: pd.Series) -> str:
    """
    Helper function to classify MSI based on the validated MSI-H threshold
    :param row: pd.Series used in the df.apply
    :return: string HIGH / LOW
    """
    msi_threshold = 30.00
    if row['MSIScore'] >= msi_threshold:
        return 'HIGH'
    return 'LOW'


def return_final_columns() -> list:
    """
    These are the columns to appear in the final excel
    :return: list columns in the DataFrame
    """
    return [
        'IS_CONTROL_SAMPLE',
        'transcript_consequences_gene_symbol',
        'cTOTAL',
        'cYES',
        'VAR_ID',
        'cosmic',
        'clinvar',
        'gnomad',
        'variant_class',
        'transcript_consequences_cadd_phred',
        'transcript_consequences_polyphen_prediction',
        'transcript_consequences_sift_prediction',
        'transcript_consequences_consequence_terms',
        'transcript_consequences_hgvsp',
        'transcript_consequences_hgvsc',
        'transcript_consequences_hgvsg',
        'ONCO_KB_HIGHEST_LEVEL',
        'num_cases_oc180',
        'num_cases_oc395',
        'num_derivatives',
        'num_times_used_for_tmb',
        'num_cases_tmb_high',
        'num_cases_msi_high',
        # aggregated columns added to the results, not the plural form
        'diagnoses',
        'sample_qcs',
        'primary_sites',
        'vafs',
        'tumor_percentages',
        'tmb_scores',
        'tmb_statuses',
        'tmb_classifications',
        'msi_scores',
        'msi_statuses',
        'msi_classifications',
        'sample_urls',
        'assay_names',
        'primary_callers',
        'igv_links',
        'cids',
        'poes',
        'derivatives',
        'batches',
        'vetts',
    ]

def get_final_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Go over the DataFrame and get the number of times a var_id occurred, and aggreate some rows into new columns
    Only report out one row per var_id, plus the aggreated data
    :param df: DataFrame of combined queries:
    Snapshot395: http://analytics3.cgs.bostonlighthouse.us/question/200-reportable-tmb-variants-derivative-snapshot395
    SnapshotGSV2: http://analytics3.cgs.bostonlighthouse.us/question/198-reportable-tmbs-variants-derivative-snapshotgsv2
    :return: final DataFrame
    """
    # go over each var_id occurrence to get the total number of times it was found based on var_id
    df_list = []  # store each DataFrame
    df['cid'] = df.apply(lambda row: get_cid_from_string(row['sample_url']), axis=1)  # get the cid from the url
    df['sample_url'] = 'http://vv.cgs.bostonlighthouse.us/' + df['sample_url']  # update the sample_url
    for count, var_id in zip(df['var_id'].value_counts(),
                             df['var_id'].value_counts().index.tolist()):
        # deal with a copy of the larger DataFrame
        temp_df = df.loc[df['var_id'] == var_id].copy()
        temp_df['num_times_used_for_tmb'] = count  # create a new column
        temp_df['cTOTAL'] = temp_df['cTOTAL'].max()  # store the max throughout since we'll only use one row
        # aggregate some columns so we can review in the final excel spreadsheet
        for new_df_col, col_key in (('vafs', 'QC_ALT_AF'),
                                    ('sample_urls', 'sample_url'),
                                    ('assay_names', 'assay'),
                                    ('primary_callers', 'QC_PRIMARY_CALLER'),
                                    ('igv_links', 'IGV'),
                                    ('tmb_scores', 'TMBScore'),
                                    ('tmb_statuses', 'TMBStatus'),
                                    ('sample_qcs', 'sample_qc'),
                                    ('diagnoses', 'Diagnosis'),
                                    ('msi_scores', 'MSIScore'),
                                    ('msi_statuses', 'MSIStatus'),
                                    ('primary_sites', 'PrimarySiteDiagnosis'),
                                    ('tumor_percentages', 'TumorPercentage'),
                                    ('cids', 'cid'),
                                    ('poes', 'POE'),
                                    ('derivatives', 'Derivative'),
                                    ('batches', 'Batch'),
                                    ('tmb_classifications', 'tmb_classification'),
                                    ('msi_classifications', 'msi_classification'),
                                    ('vetts', 'VETT')):
            temp_list = []
            temp_df.apply(lambda row: store_value(row=row, value_list=temp_list, key=col_key), axis=1)
            if new_df_col == 'derivatives':
                temp_df['num_derivatives'] = len(set(temp_list))
            temp_df[new_df_col] = str(temp_list)
        temp_df = temp_df.head(1)  # only want the first row
        # count the number of times the assay was found in the assay_names
        temp_df['num_cases_oc395'] = temp_df['assay_names'].str.count("snapshot395")
        temp_df['num_cases_oc180'] = temp_df['assay_names'].str.count("snapshotGSV2")
        # count the number of times there was an MSI or TMB
        temp_df['num_cases_tmb_high'] = temp_df['tmb_classifications'].str.count("HIGH")
        temp_df['num_cases_msi_high'] = temp_df['msi_classifications'].str.count("HIGH")
        # append on to concat at the end
        df_list.append(temp_df)

    return pd.concat(df_list).reset_index(drop=True)


def store_value(row: pd.Series, value_list: list, key: str) -> None:
    """
    append on the results of each column into the value_list
    :param row: pd.Series from each row in the DataFrame
    :param value_list: The list to append onto
    :param key: key to use in the series, this is a DataFrame col name
    :return: None
    """
    value_list.append(row[key])


def get_df(json_file: str) -> pd.DataFrame:
    """
    Create a df from the input
    :param json_file: JSON file to open
    :return: pd.DataFrame
    """
    with open(json_file) as infh:
        data = json.load(infh)
    return pd.DataFrame(data)


def get_cli_args() -> argparse.Namespace:
    """
    Get the command line options
    :return: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open the TMB JSON files')

    parser.add_argument('--gs395_json_file', dest='gs395_json_file',
                        type=str, help='json file of variants to be analyzed from gs395',
                        required=True)

    parser.add_argument('--gs180_json_file', dest='gs180_json_file',
                        type=str, help='json file of variants to be analyzed from gs180',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()
