import argparse
from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame
from tools.tmb.utils.tmb_utils import get_df_from_tmb_caller_json_output
from tools.utils.tools_utils import get_cid_from_string


def main():
    args = get_cli_args()
    tmb_json_file = args.tmb_json_file
    tmb_df, _, _ = get_df_from_tmb_caller_json_output(input_file=tmb_json_file, print_tmb_variants=True)
    cid = get_cid_from_string(get_cid_from_string(tmb_json_file))
    tmb_df.to_excel(f"{cid}-tmb-variants.xlsx", index=False)


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open the TMB json variants and analyzed them')

    parser.add_argument('--tmb_json_file', dest='tmb_json_file',
                        type=str, help='TMB json file of variants to be analyzed ',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()