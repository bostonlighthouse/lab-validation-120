import argparse
import os.path
import pandas as pd
import seaborn as sns
from collections import Counter
from matplotlib import pyplot as plt
import sys

from tools.utils.tools_utils import open_directory_get_list_files_recursively, get_cid_from_string
from lab_validation_120.utils.assay_utils import get_assay_names
from tools.tmb.utils.tmb_utils import get_df_from_tmb_caller_json_output, list_tmb_categroies, get_lims_data
from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame
from lab_validation_120.utils.plots.plot_utilities import global_rc_set, get_plot_file_type


def main():
    args = get_cli_args()
    paired_input_file = args.paired_cid_file
    append_mode = args.append_mode
    assay_name = args.assay_name
    cid_directory = args.cid_directory
    # get the list of input files to open
    cid_tmb_directory_list = open_directory_get_list_files_recursively(directory=cid_directory, file_glob="*.tmb")
    # covert the list to dictionary: key = cid, file name = value
    dictionary_cid_tmb_file_name = {get_cid_from_string(i): i for i in cid_tmb_directory_list}

    if paired_input_file is not None:
        process_paired_input(input_file=paired_input_file, dictionary_cid_tmb_file_name=dictionary_cid_tmb_file_name,
                             directory=cid_directory)
    else:
        # get the list of input files to open
        cid_tmb_json_directory_list = open_directory_get_list_files_recursively(directory=cid_directory, file_glob="*.tmb.json")
        # covert the list to dictionary: key = cid, file name = value
        dictionary_cid_tmb_json_file_name = {get_cid_from_string(i): i for i in cid_tmb_json_directory_list}
        # process the dictionaries and get back the data frames
        final_json_df, final_tmb_df, final_list_existing_variation_id_for_tmb_variants = \
            process_single_input(dictionary_cid_tmb_file_name=dictionary_cid_tmb_file_name, directory=cid_directory,
                                 dictionary_cid_tmb_json_file_name=dictionary_cid_tmb_json_file_name,
                                 assay_name=assay_name)
        #quit()
        # get some additional directory name for the final outputs
        output_file_postfix = cid_directory.rstrip('/').split('/')[-1]
        # only get LIMS data and merge if this was a gs395 assay
        if assay_name == 'gs395':
            #lims_395_df = get_lims_data(assay_name=assay_name)  # get lims data
            # merge this data so I have lims data with the final tmb data
            #final_tmb_df = pd.merge(final_tmb_df, lims_395_df, how="left", on='cid')
            pass
        elif assay_name == 'gs180':
            lims_180_df = get_lims_data(assay_name=assay_name)  # get lims data
            # merge this data so I have lims data with the final tmb data
            final_tmb_df = pd.merge(final_tmb_df, lims_180_df, how="left", on='cid')

        final_tmb_df['directory_used'] = cid_directory  # add the directory to this data frame
        # print out the TMB dataset
        #print_data_frame(final_tmb_df, f"cid_tmb_lims_{output_file_postfix}_removed_cid21-1745_{assay_name}.txt")
        #print_data_frame(final_tmb_df, f"cid_tmb_lims_{output_file_postfix}_removed_4_CIDs_{assay_name}.txt")
        #print_data_frame(final_tmb_df, f"cid_tmb_lims_{output_file_postfix}_{assay_name}.txt")

        #output_file = f"cid_tmb_lims_{output_file_postfix}_{assay_name}.txt"
        output_file = f"cid_tmb_lims_{output_file_postfix}_{assay_name}.txt"
        if append_mode is False:
            print_data_frame(final_tmb_df, output_file)
        else:
            print_data_frame(final_tmb_df, output_file, append_mode=append_mode, header=False)

        #quit()
        # print out the large data frame
        #print_data_frame(final_json_df, f"tmb_variants_{output_file_postfix}_removed_cid21-1745_{assay_name}.txt")
        #print_data_frame(final_json_df, f"tmb_variants_{output_file_postfix}_removed_4_CIDs_{assay_name}.txt")
        print_data_frame(final_json_df, f"tmb_variants_{output_file_postfix}_{assay_name}.txt")
        # print out the image
        #plot_box_plots(df=final_tmb_df,
        #               image_file_name=os.path.join(f"boxplot_{output_file_postfix}_removed_cid21-1745_{assay_name}" +
        #                                            get_plot_file_type()))
        #plot_box_plots(df=final_tmb_df,
        #               image_file_name=os.path.join(f"boxplot_{output_file_postfix}_removed_4_CIDs_{assay_name}" +
        #                                            get_plot_file_type()))
        plot_box_plots(df=final_tmb_df, assay_name=assay_name,
                       image_file_name=os.path.join(f"boxplot_{output_file_postfix}_{assay_name}" +
                                                    get_plot_file_type()), print_title=True)

        final_dict_existing_variation_id_for_tmb_variants = Counter(final_list_existing_variation_id_for_tmb_variants)
        print(f"total variant in final_dict_existing_variation_id_for_tmb_variants: {len(final_dict_existing_variation_id_for_tmb_variants)}")
        print(final_dict_existing_variation_id_for_tmb_variants)

        quit()
        if assay_name in ['gs395', 'gs180']:
            tissues = ['Lung', 'Breast', 'Brain', 'Colon', 'Ovary']
            for primary in tissues:
                temp_df = final_tmb_df[final_tmb_df['primary_site'] == primary]
                plot_box_plots(df=temp_df, assay_name=assay_name,
                               image_file_name=os.path.join(f"boxplot_{primary}_{output_file_postfix}_{assay_name}" +
                                                            get_plot_file_type()), primarysite=primary)

            temp_df = final_tmb_df[~final_tmb_df['primary_site'].isin(tissues)]
            plot_box_plots(df=temp_df, assay_name=assay_name,
                           image_file_name=os.path.join(f"boxplot_all_other_primary_{output_file_postfix}_{assay_name}" +
                                                        get_plot_file_type()), primarysite='All_others')


def plot_box_plots(df: pd.DataFrame = None, image_file_name: str = None, primarysite: str = 'All',
                   print_title: bool = False, assay_name: str = None) -> None:
    """
    Print out the boxplot and stripplot
    :param df:  Data frame of values
    :param image_file_name: The additional string to add to the image file

    14.745 High for GS180 14.338 for GS395

    """
    original_tmb_high = None
    if assay_name == 'gs395':
        original_tmb_high = 10
    elif assay_name == 'gs180':
        original_tmb_high = 14.745

    if print_title is True:
        print("\t".join(("primarysite", "Num Samples", ">= 10", f">= {original_tmb_high}", "% TMB High 10",
                         f"% TMB High {original_tmb_high}","25%", "Median", "75%")))
    global_rc_set()
    plt.rcParams["figure.figsize"] = [5, 35]
    values = ["total_variants", "tmb_score"] + list_tmb_categroies()
    fig, ax = plt.subplots(len(values), 1)  # rows x cols
    for i, category in enumerate(values):
        # get quantiles
        if category not in df:
            continue
        quantile_dict = df[category].quantile(q=[.25, .50, .75]).to_dict()
        boxplot = sns.boxplot(x=df[category], ax=ax[i], showfliers=False)
        # print out numbers for excel
        num_samples = len(df[category])
        if category == 'tmb_score':
            tmb_high_count1 = (df[category] >= 10).sum()
            tmb_high_count2 = (df[category] >= original_tmb_high).sum()
            print("\t".join((primarysite, str(num_samples),
                             str(tmb_high_count1), str(tmb_high_count2),
                             str(round((tmb_high_count1/num_samples)*100, 2)),
                             str(round((tmb_high_count2/num_samples)*100, 2)),
                             str(quantile_dict[.25]), str(quantile_dict[.50]), str(quantile_dict[.75]))))
        # set a tile
        boxplot.axes.set_title(f"{primarysite} Quantiles (n={num_samples}): 25%={quantile_dict[.25]} median={quantile_dict[.50]} "
                               f"75%={quantile_dict[.75]}", fontsize=8)
        sns.stripplot(x=df[category],  marker="o", alpha=0.4, color="black", ax=ax[i], size=4)

    fig.tight_layout()
    plt.savefig(image_file_name)
    plt.close(fig)  # have to close


def get_foundation_cdx_tmb_gs395_cids() -> dict:
    """Values were provided by Fernanda"""
    tmb_values_gs395 = {
        'cid22-144': '5 muts/Mb',
        'cid22-148': '3 muts/Mb',
        'cid21-1707': '4 muts/Mb',
        'cid22-524': '45 muts/Mb',
        'cid22-523': '3 muts/Mb',
        'cid21-1176': '1 mut/Mb',
    }
    return tmb_values_gs395


def process_single_input(directory: str = None, dictionary_cid_tmb_file_name: dict = None,
                         dictionary_cid_tmb_json_file_name: dict = None,
                         assay_name: str = None) -> (pd.DataFrame, pd.DataFrame):
    """
    This was implemented to open a directory and get all values
    :param directory: The directory to open
    :param dictionary_cid_tmb_json_file_name:  cid key, with JSON file names as the values
    :param dictionary_cid_tmb_file_name: cid key, with TMB file names as the values
    :param assay_name: What's the assay name, if provided only open those files with the assay name in it
    :return: Two dataframes.  One with aggregated TMB categories per CID and the other with all rows
    """

    print("\t".join(('CID', 'TMB')))
    list_tmb_json_df = []  # store the df's from the JSON file
    list_cid_and_tmbs_df = []  # store the df's of aggregated data
    final_list_existing_variation_id_for_tmb_variants = []
    foundation_cdx = get_foundation_cdx_tmb_gs395_cids()
    i = 0
    for cid in dictionary_cid_tmb_file_name:  # go over all CIDs
        #tmb_file = f"{directory}/{dictionary_cid_tmb_file_name[cid]}"
        tmb_file = dictionary_cid_tmb_file_name[cid]
        tmb_score = get_tmb_value_from_file(input_file=tmb_file)
        store_tmb_df = False
        #if cid in ['cid21-1126', 'cid21-1745', 'cid21-1064', 'cid21-1763', 'cid22-772', 'cid21-2379', 'cid22-445',
        #           'cid21-2379', 'cid22-445', 'cid21-851', 'cid21-500']:  # very high TMB value so removed for scaling
        #    continue
        #    #pass
        if assay_name is not None:  # assay name came in so only open files with that assay name

            temp_file = os.path.split(tmb_file)[1]  # must only use the file name, and not the path
            assay_name_updated = assay_name.replace('gs', 'oc')  # some files will have oc, and not gs
            if assay_name in temp_file or assay_name_updated in temp_file:  # could be oc or gs
                # if assay name was passed in, then verify the assay
                store_tmb_df = True
            # these  batches were gs180 but run under oc120, so filename has oc120
            elif assay_name == 'gs180' and any(batch in temp_file for batch in ['b20-126-', 'b20-129-', 'b20-113-',
                                                                                'b20-117-', 'b21-45-', 'b20-125-',
                                                                                'b20-142-', 'b21-38-']):
                store_tmb_df = True
            elif assay_name == 'gs180' and any(batch in temp_file for batch in ['gs18-', 'r56-2-ns55' ]):
                # There were some files named that were missing gs180- -> gs18-
                # 2021dez30-run63-gs18-b21-184-cid21-2833-snapshot.B21-184.CID21-2833.B525_N701.merged_concatenated.tmb
                # 2021nov13-r56-2-ns55-b21-162-cid21-2429-snapshot.B21-162.CID21-2429.B539_N707.merged_concatenated.tmb
                store_tmb_df = True

            else:
                #print(f"{temp_file} not found in assay name {assay_name}", file=sys.stderr)
                store_tmb_df = True

        else:
            store_tmb_df = True

        if store_tmb_df is True:  # it's possible this is not true, so only print if data was found
            print('\t'.join((cid, str(tmb_score))))
            #tmb_json_file = os.path.join(directory, dictionary_cid_tmb_json_file_name[cid])
            tmb_json_file = dictionary_cid_tmb_json_file_name[cid]
            # get the data frames from the JSON file
            tmb_df, num_of_each_category_df, list_existing_variation_id_for_tmb_variants = \
                get_df_from_tmb_caller_json_output(input_file=tmb_json_file)
            # store all the existing variation_id
            final_list_existing_variation_id_for_tmb_variants += list_existing_variation_id_for_tmb_variants
            # this num_of_each_category_df df is len 1
            assert len(num_of_each_category_df) == 1, f"The tmb data frame is not == 1, it's: {len(tmb_df)}"

            # add some values
            tmb_df['cid'] = cid
            tmb_df['file'] = dictionary_cid_tmb_file_name[cid]

            # this df has all the rows of variants, and each classification
            num_of_each_category_df['cid'] = cid
            num_of_each_category_df['file'] = dictionary_cid_tmb_file_name[cid]
            num_of_each_category_df['tmb_score'] = tmb_score

            # fill in the Foundation Data if available
            if cid in foundation_cdx:
                num_of_each_category_df['foundation_cdx'] = foundation_cdx[cid]
            else:
                num_of_each_category_df['foundation_cdx'] = 'N/A'

            # store them for later pdf.concat
            list_tmb_json_df.append(tmb_df)
            list_cid_and_tmbs_df.append(num_of_each_category_df)

        if i > 10:  # just here to speed up testing...
            pass
            #break
        i += 1

    # all dont so create DF for each
    final_json_df = pd.concat(list_tmb_json_df).reset_index(drop=True)
    final_tmb_df = pd.concat(list_cid_and_tmbs_df).reset_index(drop=True)
    return final_json_df, final_tmb_df, final_list_existing_variation_id_for_tmb_variants


def process_paired_input(input_file: str = None, directory: str = None,
                         dictionary_cid_tmb_file_name: dict = None) -> None:
    """
    Was it a paired input file then find the TMB values for each
    :param input_file: Pair file to open
    :param directory: The directory to open
    :param dictionary_cid_tmb_file_name: cid key, with TMB file names as the values
    """
    print("\t".join(('CID_GS395', 'TMB_GS395', 'CID_GS180', 'TMB_GS180')))
    with open(input_file) as in_fh:
        for line in in_fh:
            if 'GS395' in line or 'GS180' in line:  # ignore is there is a header
                continue
            elif line.startswith('#'):
                continue
            cid1, cid2 = line.rstrip().lower().split("\t")
            file1 = os.path.join(directory, dictionary_cid_tmb_file_name[cid1])
            file2 = os.path.join(directory, dictionary_cid_tmb_file_name[cid2])
            tmb1 = get_tmb_value_from_file(input_file=file1)
            tmb2 = get_tmb_value_from_file(input_file=file2)
            print('\t'.join((cid1, str(tmb1), cid2, str(tmb2))))


def get_tmb_value_from_file(input_file: str = None) -> float:
    """
    open the input file and get the TMB value
    :param input_file: The input file to open
    :return float
    """
    with open(input_file) as in_fh:
        in_fh.readline()
        for line in in_fh:
            return float(line.rstrip().split("\t")[0])  # 26.146  1.2.1


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open a list of cider output files and create a docker bash script '
                                                 'for TMB calling on those VCF files found')

    parser.add_argument('--paired_cid_file', dest='paired_cid_file',
                        type=str, help='pair cids, two per line ', default=None)
    parser.add_argument('--cid_directory', dest='cid_directory',
                        type=str, help='The directory to check for the TMB outputs ',
                        required=True)
    parser.add_argument('--assay_name', dest='assay_name',
                        type=str, help='Assay name', choices=get_assay_names(),
                        required=True)
    parser.add_argument('--append_mode', dest='append_mode',
                        type=bool, help='If this is run this way, it will append to the final tmb data frame',
                        default=False, required=False)

    return parser.parse_args()


if __name__ == "__main__":
    main()