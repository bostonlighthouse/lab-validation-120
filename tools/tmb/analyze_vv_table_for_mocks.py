import argparse
import pandas as pd
import pprint as pp


def main():
    args = get_cli_args()
    vv_variant_table_file = args.vv_variant_table_file
    vv_df = pd.read_csv(vv_variant_table_file, sep="\t")
    vv_df = vv_df[vv_df['VETT'] == "YES"]
    print(vv_df)
    mock_data_list = []
    for index, row in vv_df.iterrows():
            mock = {}
            mock["transcript_consequences.gene_symbol"] = row['transcript_consequences.gene_symbol']
            mock["transcript_consequences.hgvsp"] = row['transcript_consequences.hgvsp']
            mock["transcript_consequences.hgvsc"] = row['transcript_consequences.hgvsc']
            mock["most_severe_consequence"] = row['most_severe_consequence']
            mock["QC.ALT_AF"] = row['QC.ALT_AF']
            mock["VETT"] = 'YES'
            mock["variant_class"] = row['variant_class']
            mock_data_list.append(mock)
    pp.pprint(mock_data_list, sort_dicts=False)


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open VV variant table output and put this in Mock format')

    parser.add_argument('--vv_variant_table_file', dest='vv_variant_table_file',
                        type=str, help='VV Table of variants ', default=None, required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()