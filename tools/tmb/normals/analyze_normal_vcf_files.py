import argparse
import json
import pprint
from matplotlib import pyplot as plt
import seaborn as sns

from tools.utils.tools_utils import open_directory_get_list_files_recursively, get_binomial_probability, \
    get_cid_from_string
from biomodels.models.vcf.utilities.vcf_utilities import return_most_severe_impact_transcript_obj, \
    ignore_caller_because_of_caller_thresholds, get_vaf_from_vf_field
import biomodels.models.vcf.vcf_container as vc
from tools.utils.biomodels_counting_class import BiomodelsCountingClass
from lab_validation_120.utils.plots.classes.histplot_class import HistplotClass



class GermlineBiomodelsCountingClass(BiomodelsCountingClass):
    """Just update with a new attribute"""
    def __init__(self):
        super().__init__()
        self.total_gatk_primary_caller = 0


def main():
    """Business logic"""
    args = get_cli_args()

    vcf_files = open_directory_get_list_files_recursively(directory=args.normal_path)
    process_vcf_files(list_files=vcf_files, args=args)


def process_vcf_files(list_files: list=None, args: argparse.Namespace=None):
    """
    Process all the VCF files passed in
    :param list_files:  List of VCF files
    :parma args:  argparse instance
    :return:
    """
    assay_type = 'agilent'
    pon_data = {}  # data found for each sample as we process through the list of PoN samples
    filtered_counter_class = GermlineBiomodelsCountingClass()
    all_vafs_for_variants_passing_filtering = []
    num_files = 0
    for file in list_files:
        print(file)
        num_files += 1
        sample_had_one_variant_pass = False
        cid = get_cid_from_string(file)
        genes = []  # the genes for this sample where variants were found
        num_vars = 0
        pon_data[cid] = {}  # store the data for this sample
        for variant_key, variant_entry_obj in \
                vc.VcfContainer(file).dictionary_vcf_entries.items():
            num_vars += 1
            # the total amount of transcripts
            filtered_counter_class.total_variants_analyzed += 1
            variant_transcript_obj = return_most_severe_impact_transcript_obj(variant_entry_obj=variant_entry_obj)

            if variant_entry_obj.info_internal_2_vcf.ALT_DP is None or variant_entry_obj.info_internal_2_vcf.REF_DP is None:
                continue

            # vaf and depth filtering, do this after is_a_germline_variant_to_filter
            total_depth = variant_entry_obj.info_internal_2_vcf.REF_DP + variant_entry_obj.info_internal_2_vcf.ALT_DP
            vaf_2_use = get_vaf_from_vf_field(variant_entry_obj=variant_entry_obj)
            vaf = round(vaf_2_use * 100, 3)
            gene = variant_transcript_obj.SYMBOL
            binomial_prob = get_binomial_probability(total_depth=total_depth, vaf=vaf)
            # did it pass the vaf and depth thresholds
            if vaf < args.vaf_threshold or total_depth < args.depth_threshold or binomial_prob < \
                    args.binomial_prob_threshold:
                continue

            # store so we can plot the vaf in a hist at the end
            all_vafs_for_variants_passing_filtering.append(vaf)
            # update the class
            filtered_counter_class.total_variants_passed_filtering += 1
            filtered_counter_class.update_variant_level_occurrences(
                variant_transcript_obj=variant_transcript_obj,
                variant_entry_obj=variant_entry_obj, var_id=variant_entry_obj.vep_key,
                depth=total_depth, vaf=vaf
            )
            genes.append(gene)
            sample_had_one_variant_pass = True
            # was a gatk call that passed filtering, just storing the total number of gatk calls
            if variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER == 'gatk':
                filtered_counter_class.total_gatk_primary_caller += 1
            #if num_vars == 100:
            #    break
        #if num_files == 3:
        #    break

        # was there a least one variant that passed filtering for the sample
        if sample_had_one_variant_pass is True:
            filtered_counter_class.total_samples_analyzed += 1
        # store all the genes that passed filtering for the sample
        pon_data[cid]['genes'] = genes

    base_file_name = "_".join([str(i) for i in (f"{get_tmb_output_directory()}/{assay_type}_pon_variant_filtering",
                                                args.vaf_threshold, "depth_threshold",
                                                args.depth_threshold, "binomial_prob_threshold",
                                                args.binomial_prob_threshold)])

    plot_distribution_from_list(base_file_name=base_file_name,
                                list_values=all_vafs_for_variants_passing_filtering,
                                plot_title="All VAFs: Distribution", size_image=(18, 10), args=args)


    vaf_passed = print_counter_class(counter_class=filtered_counter_class, file_name=base_file_name)

    plot_distribution_from_list(base_file_name=f"{base_file_name}_vaf_passed",
                                list_values=vaf_passed,
                                plot_title="VAFs Passing thresholds: Distribution", size_image=(18, 10), args=args)


def plot_distribution_from_list(base_file_name=None, list_values=None, value_type="vaf",
                                plot_title=None, size_image=None, fig=None, ax=None, log_scale=None,
                                xticks=range(0, 105, 5), bins=100, args=None):
    """
    Plot the  distribution for all values  passed into list_values
    :param base_file_name: Used to store the actual plot
    :param list_values: The values to plot
    :param plot_title: Title for the plot
    :param size_image: The size of the image
    :param ax: The seanborn axis object, can be None, and then we'll create our own
    :param fig: the fig object from a plt.subplots calls, can be None and then we'll create our own
    :param log_scale: Draw it in log scale?  Boolean
    :param xticks: The xticks to draw
    :param args: Args from argparse
    :return: HistplotClass object
    """

    plot_title = f"{plot_title} (n={len(list_values)})\n" \
                 f"vaf threshold: {args.vaf_threshold}% depth threshold: {args.depth_threshold} " \
                 f"binomial threshold: {args.binomial_prob_threshold}"

    # if a base_file_name file name was sent in, then we want to create
    file_name = None
    if base_file_name is not None:
        # here we're not creating subplots, so it's simple
        sns.set_theme(style="whitegrid")
        fig, ax = plt.subplots(1)  # rows x cols
        file_name = f"{base_file_name}_{value_type}_distribution.svg"

    # instantiate a new instace of the plotting class and provide everthing it would need
    hist_plot = HistplotClass(kde=False, color='red', bins=bins, ax=ax,
                              file_name=file_name,
                              title=plot_title,
                              xlabel="Variant Allele %",
                              size_image=size_image, fig=fig, x_data_list=list_values,
                              xticks=xticks,
                              log_scale=log_scale, ylim=(0, 1400))

    # if a base_file_name file name was sent in, then we want to finalize
    if base_file_name is not None:
        plt.setp(ax.artists, edgecolor='k', facecolor='w')
        plt.setp(ax.lines, color='k')
        hist_plot.finalize_plot()

    return hist_plot


def get_tmb_output_directory():
    """
    Return a simple string where to store data from the PoN output
    :return: String
    """

    return "scratch/agilent700/tmb/"


def print_counter_class(file_name=None, counter_class=None):
    """
    Simple function to print out final outputs to files
    :param file_name: Base name used for the file naming
    :param counter_class: Class with the counts
    :return: None
    """
    with open(f"{file_name}.txt", "w") as fh_out:
        print(pprint.pformat(counter_class.get_protein_occurrences(), indent=4), file=fh_out)
        print(pprint.pformat(counter_class.get_var_id_ojbs(), indent=4), file=fh_out)

    vafs = []
    var_ids = []
    with open(f"{file_name}.json", "w") as fh_out:
        #print(json.dumps(counter_class.get_protein_occurrences(),  indent=4), file=fh_out)
        #print(counter_class.get_var_ids())
        for var_id, var_id_list in sorted(counter_class.get_var_id_ojbs().items(), key=lambda x: len(x[1]), reverse=True):
            # get all of the VAFs for this VAR_ID
            temp_vafs = [var_obj.vaf for var_obj in var_id_list]

            # was this VAR_ID found in the PoN >= 2 times, then keep for additional threshold testing, otherwise do not
            if len(var_id_list) < 2:
                continue
            # additional testing to see if the variant fell around 50% VAF or 100% VAF
            range_start_low = 40
            range_end_low = 60
            range_start_high = 90
            range_end_high = 100
            # Check if each element is in the range
            in_range = [range_start_low <= value <= range_end_low or \
                        range_start_high <= value <= range_end_high
                        for value in temp_vafs]
            # if any are True, then there was one variant in the ranges so keep it
            if any(in_range):
                pass
            else:
                continue

            vafs += temp_vafs
            var_ids.append(var_id)
        #print(json.dumps(counter_class.get_var_ids(),  indent=4), file=fh_out)
    return vafs



def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Analyze the normal VCF file to produce germline VAR_IDs to remove')


    parser.add_argument('--normal_path', dest='normal_path',
                        type=str, help='Path to normal vcf files',
                        required=True)
    parser.add_argument('--vaf_threshold', dest='vaf_threshold', required=True, type=float,
                        help="The VAF threshold to use, i.e. no variant with a lower vaf will be used")

    parser.add_argument('--binomial_prob_threshold', dest='binomial_prob_threshold', default=0.99, type=float,
                        help="Binomial probability threshold, default 0.99")

    parser.add_argument('--depth_threshold', dest='depth_threshold', required=True, type=int,
                        help="The total depth threshold to use, i.e. no variant with a lower total depth will be used")


    return parser.parse_args()


if __name__ == "__main__":
    main()