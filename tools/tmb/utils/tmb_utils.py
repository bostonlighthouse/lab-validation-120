import json

import pandas as pd

from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame, read_dataset_and_get_pandas_df, \
    get_columns_from_vv_and_graphql_endpoints


def get_df_from_tmb_caller_json_output(input_file: str = 'None', print_tmb_variants: bool = False) -> \
        (pd.DataFrame, dict, dict):
    """
    Open the json and return a dataframe of all the data in a tmb.json run from tmb-caller
    :param input_file: The JSON file to open
    :param print_tmb_variants: If this was called one time, we can print the output
    """
    final_list_df = []
    num_in_each_category = {}
    list_existing_variation_id_for_tmb_variants = []
    total_variants = 0
    with open(input_file) as in_fh:
        data = json.load(in_fh)
        for subtype in list_tmb_categroies():  # go over each category in the JSON
            if subtype not in data:
                continue
            tmb_df = pd.DataFrame(data[subtype])  # create a df of this category
            if print_tmb_variants is True and subtype == 'tmb_variants':  # only print out tmb variants if called to
                print_data_frame(tmb_df, "tmb_variants.txt")

            if len(tmb_df) > 0 and subtype == 'tmb_variants':  # get the list of existing variants
                dict_values = tmb_df['transcript'].to_dict()
                for k in dict_values:
                    if len(dict_values[k]['Existing_variation']) > 0:
                        list_existing_variation_id_for_tmb_variants += dict_values[k]['Existing_variation']

            tmb_df['classification'] = subtype
            num_in_each_category[subtype] = num_in_each_category.get(subtype, 0) + len(tmb_df)
            total_variants += len(tmb_df)
            final_list_df.append(tmb_df)

    final_json_df = pd.concat(final_list_df).reset_index(drop=True)
    num_in_each_category['total_variants'] = total_variants
    final_tmb_df = pd.DataFrame([num_in_each_category])
    #print(list_existing_variation_id_for_tmb_variants)
    return final_json_df, final_tmb_df, list_existing_variation_id_for_tmb_variants


def list_tmb_categroies() -> list:
    """The list of categories in a TMB JSON output"""
    return ['tmb_variants', 'binomial_variants', 'cosmic_variants', 'empirically_derived_artifact_variants',
                        'further_variants_to_remove_variants', 'hgvsp_none', 'ignore_bc_caller_threshold_variants',
                        'karolinska_gene_biomarker_variants', 'no_alt_dp', 'no_ref_dp', 'non_consequential_variants',
                        'pattern_derived_artifact_gene_biomarker_variants',
                        'pattern_derived_germline_gene_biomarker_variants',
                        'pon_variants', 'population_variants', 'total_depth_variants', 'vaf_variants',
            'artifacts_variants_based_var_ids', 'germline_variants_based_var_ids']


def get_lims_data(assay_name: str = None, get_pon_data: bool = True) -> pd.DataFrame:
    """Get the LIMS data"""
    if assay_name == 'gs395':
        infile = 'scratch/oc395/TMB/all_oc395_validation_samples_pipeline_directory_v2.txt'  # gs395 data
        df = read_dataset_and_get_pandas_df(col_vals=get_columns_from_vv_and_graphql_endpoints(), file_name=infile)
        infile = 'scratch/oc395/TMB/all_oc395_validation_pon.txt'  # PoN data
        if get_pon_data is True:
            df2 = read_dataset_and_get_pandas_df(col_vals=get_columns_from_vv_and_graphql_endpoints(), file_name=infile)
            df = pd.concat([df, df2]).reset_index(drop=True)
    elif assay_name == 'gs180':
        infile = 'scratch/oc395/TMB/gs180_lims_data_v2.txt'  # gs180 data
        df = read_dataset_and_get_pandas_df(col_vals=get_columns_from_vv_and_graphql_endpoints(), file_name=infile)
    # just make the column lower case
    df['cid'] = df['cid'].str.replace('CID', 'cid')
    # just update the keys that were found
    df = df.drop_duplicates(keep='first')
    return df.rename(columns={"Firstname": "first_name", "Lastname": "last_name", "Middlename": "middle_name",
                              "Dateofbirth": "date_of_birth", "Gender": "gender", "Patient_ID": "mrn",
                              "PrimarySite": "primary_site", "PrimarySiteDiagnosis": "primary_site_diagnosis",
                              "TumorPercentage": "tumor_percentage", "MEAN_COLLAPSED_COVERAGE": "mcc",
                              "Fraction_GTE_Upper_DP": "fraction_gte_upper_dp", "TestCategory": "test_category",
                              "ID_2": "poe"},)