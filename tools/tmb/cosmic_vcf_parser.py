import argparse
from collections import defaultdict
import operator
import json

def main():
    pass
    args = get_cli_args()
    cosmic_vcf_file = args.cosmic_vcf_file
    total_values = parse_cosmic_vcf_file(input_file = cosmic_vcf_file)
    final_values = defaultdict()
    # create a new sorted dictionary
    for key, val in sorted(total_values.items(), key=operator.itemgetter(1), reverse=True):
        final_values[key] = val
    # print that out
    print(json.dumps(final_values))

def parse_cosmic_vcf_file(input_file: str = None) -> dict:
    total_values = defaultdict()
    with open(input_file) as infh:
        for i, line in enumerate(infh):
            line = line.rstrip()
            if line.startswith('#'):
                continue
            values = line.split("\t")
            # values looks like:
            # ['1', '69224', 'COSV58737130', 'A', 'C', '.', '.',
            # 'GENE=OR4F5;STRAND=+;LEGACY_ID=COSM3677745;CDS=c.134A>C;AA=p.D45A;HGVSC=ENST00000335137.3:c.134A>C;HGVSP=ENSP00000334393.3:p.Asp45Ala;HGVSG=1:g.69224A>C;CNT=1']
            cosmic_id = values[2]
            additional_values_str = values[7]
            index_to_use = 8
            # sometimes there is not HGVSP, so the index to use is one less
            # GENE=AL627309.1;STRAND=-;LEGACY_ID=COSN8890225;CDS=c.*14T>G;AA=p.?;HGVSC=ENST00000423372.3:c.*14T>G;HGVSG=1:g.138516A>C;CNT=1
            if 'HGVSP' not in additional_values_str:
                index_to_use = 7

            # get all the actual additional values in a list
            additional_values = values[7].split(";")
            # additional_values looks like:
            # ['GENE=OR4F5', 'STRAND=+', 'LEGACY_ID=COSM3677745', 'CDS=c.134A>C', 'AA=p.D45A',
            # 'HGVSC=ENST00000335137.3:c.134A>C', 'HGVSP=ENSP00000334393.3:p.Asp45Ala', 'HGVSG=1:g.69224A>C', 'CNT=1']

            # get the legacy COSMIC ID
            cosmic_legacy_id = additional_values[2]
            assert cosmic_legacy_id.startswith("LEGACY"), "cosmic_legacy_id does not start with LEGACY"
            _, cosmic_legacy_id = cosmic_legacy_id.split("=")

            # get the count by using the index_to_use
            count = additional_values[index_to_use]
            assert count.startswith("CNT"), "count does not start with CNT"
            _, count = count.split("=")
            count = int(count)

            # fill dictionary
            total_values[cosmic_legacy_id] = count
            total_values[cosmic_id] = count
            if i == 100:
                break

    return total_values

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(
        description="Parse out the COSMIC VCF file to create a list of COSMIC ID's and counts")

    parser.add_argument('--cosmic_vcf_file', dest='cosmic_vcf_file',
                        type=str, help="COSMIC VCF file to be parsed for COSMIC ID's and counts ",
                        required=True)
    return parser.parse_args()


if __name__ == "__main__":
    main()



