import argparse
import pandas as pd
from lab_validation_120.utils.assay_utils import get_assay_names
from tools.tmb.utils.tmb_utils import get_lims_data
from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame

def main():
    args = get_cli_args()
    lims_df = get_lims_data(assay_name=args.assay_name)  # get lims data
    tmb_df = pd.read_table(args.tmb_distribution_data)
    tmb_df = tmb_df.rename(columns={"Cid": "cid", "Batch ID": "batch"})
    tmb_df['cid'] = tmb_df['cid'].str.replace('CID', 'cid')
    final_tmb_df = pd.merge(tmb_df, lims_df, how="left", on=['cid', 'batch'])
    print_data_frame(final_tmb_df, "final_table.txt")


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Merge data from table Metabase table '
                                                 '"Snapshotgsv2 Meta Clinical Prod Tumor"')

    parser.add_argument('--assay_name', dest='assay_name',
                        type=str, help='Assay name', choices=get_assay_names(),
                        required=True)
    parser.add_argument('--tmb_distribution_data', dest='tmb_distribution_data',
                        type=str, help='The TMB distribution data from Metbase table '
                                       '"Snapshotgsv2 Meta Clinical Prod Tumor"',
                        required=True)
    return parser.parse_args()


if __name__ == "__main__":
    main()