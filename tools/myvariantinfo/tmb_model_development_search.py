"""
Use this to go through and find how many times a variant that was called is germline based on
 MyVariantInfo - Clinvar
"""
import argparse
import pandas as pd
import sys
from myvariant import MyVariantInfo

from tools.myvariantinfo.utils import get_num_germline_submitters_from_clinvar


def main():
    args = get_cli_args()
    tmb_output_file = args.tmb_output_file
    df = pd.read_csv(tmb_output_file, sep='\t')
    mv = MyVariantInfo()
    for i, row in df.iterrows():
        hgvsg = ''.join(('chr', row['hgvsg']))
        if row['used_in_tmb'] is False:
            clinvar = mv.getvariant(hgvsg, fields='clinvar')
            print(f"line num {i} {hgvsg}", file=sys.stderr)
            print(clinvar, file=sys.stderr)
            if clinvar:
                num_subs, origins = get_num_germline_submitters_from_clinvar(clinvar)
                if 'germline' in origins:
                    print(row['var_id'])



def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open an output file from:'
                                                 'pipenv run python3 -m tools.tmb.compare_TMB_values '
                                                 '--cid_directory scratch/agilent700/tmb/run1_run2_iteration1_tmb '
                                                 '--assay_name gs700 '
                                                 'Output: tmb_variants_run1_run2_iteration1_tmb_gs700.txt')

    parser.add_argument('--tmb_output_file', dest='tmb_output_file',
                        type=str,
                        help='tmb_output_file from pipenv run python3 -m tools.tmb.compare_TMB_values',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()