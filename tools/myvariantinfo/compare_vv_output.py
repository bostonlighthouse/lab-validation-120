import argparse
import os.path

import pandas as pd
from tools.myvariantinfo.utils import get_num_rows_to_remove, get_vars_per_tmb
from tools.utils.tools_utils import (open_directory_get_list_files_recursively, get_cid_from_string,
                                     get_batch_from_string)
from tools.bam_file_analysis.samtools.utils import get_samtools_stats_values

from lab_validation_120.utils.htqc_utilites import get_htqc_summary_df_from_directory_files

from myvariant import MyVariantInfo
from typing import Tuple
import json
import sys

def main():
    """
    Business logic
    :return: NoneType
    """
    args = get_cli_args()
    vv_file = args.vv_file
    metabase_csv_file = args.metabase_csv_file
    tmb_json_file = args.tmb_json_file
    pipeline_output = args.pipeline_output
    df_tmb = None
    germline_var_ids = []
    if vv_file:
        df = pd.read_csv(vv_file, sep="\t")
        df_tmb = df[df['tmb_selected'] == True]
    if metabase_csv_file:
        df = pd.read_csv(metabase_csv_file)
        df_tmb = df[df['Tmb Selected'] == True]
        df_tmb = df_tmb.rename(columns={'Var ID': "VAR_ID",
                                        'Transcript Consequences Hgvsg': 'transcript_consequences.hgvsg'}
                               )
        df_tmb['QC.PRIMARY_CALLER'] = 'fake_caller'
    elif tmb_json_file:
        df_tmb = get_tmb_df(file=tmb_json_file)
        df_tmb.to_excel('junk.xlsx', index=False)

    elif pipeline_output:
        # get the list of input files to open
        cid_tmb_json_directory_list = open_directory_get_list_files_recursively(directory=pipeline_output, file_glob="*.tmb.json")
        total_germline_var_ids = []
        list_data = []
        batches = []
        for file in cid_tmb_json_directory_list:
            directory = os.path.dirname(file)
            htqc_df = get_htqc_summary_df_from_directory_files(directory=directory,
                                                               file_glob="*coverage.summary.htqc.txt",
                                                               sample_col='sample')
            file_samtools_stats = file.replace('.tmb.json', '.samtools_stats.with_dups.txt')
            dict_stats = get_samtools_stats_values(file_samtools_stats)
            cid = get_cid_from_string(file)
            # get the data frame of tmb variants
            df_tmb = get_tmb_df(file=file)
            # process the results
            original_tmb, new_tmb, germline_var_ids = process_tmb_df(df=df_tmb)
            total_germline_var_ids = list(set(total_germline_var_ids + germline_var_ids))
            # store for later conversion to df
            dict_ = {'cid': cid, 'original_num_tmb_vars': len(df_tmb), 'original_tmb': original_tmb,
                     'new_num_tmb_vars': len(df_tmb) - len(germline_var_ids), 'new_tmb': new_tmb,
                     'MEAN_FILTERED_COVERAGE': htqc_df['MEAN_FILTERED_COVERAGE'].iloc[0],
                     'MEAN_COLLAPSED_COVERAGE': htqc_df['MEAN_COLLAPSED_COVERAGE'].iloc[0],
                     'FRACTION_GTE_UPPER_DP': htqc_df['FRACTION_GTE_UPPER_DP'].iloc[0],
                     'insert_size': dict_stats['insert_size_average'],
                     'insert_size_std': dict_stats['insert_size_std'],
                     'num_sequences': dict_stats['num_sequences']
                     }
            list_data.append(dict_)
            batches.append(get_batch_from_string(file))

        batches = list(set(batches))
        batches = '-'.join(batches)
        df = pd.DataFrame(list_data)
        df.sort_values(by='original_tmb', ascending=False, inplace=True)

        # Create a Pandas Excel writer using openpyxl as the engine
        writer = pd.ExcelWriter(f'{batches}-tmb-updates.xlsx', engine='openpyxl')
        sheet_name = 'tmb_update'
        # Write the DataFrame to the Excel file
        df.to_excel(writer, index=False, sheet_name=sheet_name)
        # Load the workbook and select the sheet
        workbook = writer.book
        worksheet = writer.sheets[sheet_name]
        # Apply bold format to each cell in column B
        from openpyxl.styles import Font
        bold_font = Font(bold=True)
        for cell in worksheet['E']:
            cell.font = bold_font
        # Save the file
        writer.close()
        germline_var_ids = total_germline_var_ids  # store so it can be printed laster

    else:
        raise ValueError('No intputs provided. Please specify')


    if pipeline_output is None:
        original_tmb, new_tmb, germline_var_ids = process_tmb_df(df=df_tmb)
        print(f"Original number of TMB {len(df_tmb)} TMB: {original_tmb}", file=sys.stderr)
        print(f"New number of TMB after removing {len(germline_var_ids)} variants TMB: {new_tmb}", file=sys.stderr)

    for var_id in sorted(germline_var_ids):
        print(var_id)

def get_tmb_df(file:str = None) -> pd.DataFrame:
    """
    Take in the JSON file and get the data frame
    :param file: JSON file path
    :return: pd.DataFrame
    """

    with open(file) as infh:
        ds = json.load(infh)
        ds = ds['tmb_variants']  # only need to worry about these
        df_tmb = pd.json_normalize(ds, max_level=3)
        df_tmb.rename(columns={'var_id': 'VAR_ID', 'hgvsg': 'transcript_consequences.hgvsg'}, inplace=True)
        df_tmb['QC.PRIMARY_CALLER'] = 'fake_caller'

    return df_tmb

def process_tmb_df(df: pd.DataFrame) -> Tuple[float, float, list]:
    """
    Pass in the pandas dataframe and process to update the before and after TMB and print out var_ids
    :param df:
    :return:
    """
    my_variant_info = MyVariantInfo()
    tmb_per_variant = get_vars_per_tmb(assay_type='gs700')

    original_tmb = round(len(df) * tmb_per_variant, 3)
    num_rows_to_remove, origins, germline_var_ids \
        = get_num_rows_to_remove(df=df, my_variant=my_variant_info,
                                 num_submitters_threshold=1)

    if num_rows_to_remove > 0:
        new_tmb = round(original_tmb - round(num_rows_to_remove * tmb_per_variant, 3), 3)
    else:
        new_tmb = original_tmb

    #print(f"Original number of TMB {len(df)} TMB: {original_tmb}", file=sys.stderr)
    #print(f"New number of TMB after removing {len(germline_var_ids)} variants TMB: {new_tmb}", file=sys.stderr)

    #for item in germline_var_ids:
    #    print(item)

    return original_tmb, new_tmb, germline_var_ids


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open the VV output file from the website OR '
                                                 'A TMB JSON output file and extract the variant for TMB and '
                                                 'Look up the variant in MyVariantInfo')

    parser.add_argument('--vv_file', dest='vv_file',
                        type=str, help='Open the VV output file from the website',
                        required=False)

    parser.add_argument('--metabase_csv_file', dest='metabase_csv_file',
                        type=str, help='Open the Metabase output file (selected "Tmb Selected" is True',
                        required=False)

    parser.add_argument('--tmb_json_file', dest='tmb_json_file',
                        type=str, help='Open the TMB JSON output file',
                        required=False)

    parser.add_argument('--pipeline_output', dest='pipeline_output',
                        type=str, help='Directory of pipeline output',
                        required=False)

    return parser.parse_args()


if __name__ == '__main__':
    main()