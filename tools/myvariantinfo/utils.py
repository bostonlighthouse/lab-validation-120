import json
import os
from typing import Tuple

import pandas as pd
from myvariant import MyVariantInfo


def get_num_rows_to_remove(df: pd.DataFrame,
                           my_variant: MyVariantInfo,
                           num_submitters_threshold: int=2) -> Tuple[int, list, list]:
    """
    Find the number of rows that should be removed from the dataframe, i.e not counted
    :param df: Data frame to analzyed
    :param my_variant: MyVariantInfo instance
    :param num_submitters_threshold: How many submitters need to be found to consider for removal
    :return: int of the number of rows to not count for TMB, and a list of all origins
    """
    num_row_remove = 0
    all_origins = []
    germline_var_ids_to_remove = []
    scratch = 'scratch/myvariant/'
    for i, row in df.iterrows():
        var_id = row['VAR_ID']
        if row['QC.PRIMARY_CALLER'] == 'pindel':
            continue
        file = os.path.join(scratch, f"{var_id}.json")
        my_var = f"chr{row['transcript_consequences.hgvsg']}"
        if os.path.exists(file):
            with open(file, 'r') as f:
                # Load the JSON data into a Python data structure
                clinvar = json.load(f)
        else:
            clinvar = my_variant.getvariant(my_var, fields='clinvar')
            # Open a file in write mode
            with open(file, 'w') as fh_out:
                # Pretty print the data structure to the file
                json.dump(clinvar, fh_out, indent=4)

        if clinvar is None: # no results in MyVariantInfo for this variant
            # e.g. https://myvariant.info/v1/variant/chr20:g.46279863_46279865del?fields=clinvar
            #print(f"{my_var} {row['variant_class']} not in Clinvar", flush=True)
            continue
        num_submitters, origins = get_num_germline_submitters_from_clinvar(clinvar)
        all_origins = all_origins + origins  # store for later to pass back
        if  num_submitters >= num_submitters_threshold:  # if there were 2 or more submitters, this row can be removed from TMB calculation
            num_row_remove += 1
            germline_var_ids_to_remove.append(var_id)
        #print(f"{my_var} {row['variant_class']} num submitters {num_submitters} {origins}", flush=True)
        #print(f"{my_var} {num_subs} germline variant submissions")


    return num_row_remove, all_origins, germline_var_ids_to_remove


def get_num_germline_submitters_from_clinvar(clinvar: dict=None) -> Tuple[int, list]:
    """
    A clinvar object returned from the MyVariantInfo
    :param clinvar: dictionary results from end point
    :return: int, the number of submitters and a list of origins found in the RCV entries
    """


    num_germline_submitters = 0
    all_origins = []
    if 'clinvar' in clinvar:
        clinvar_rcv = clinvar['clinvar']['rcv']  # get the object we want, either a list or dictionary
        if isinstance(clinvar_rcv, list):  # if it's a dictionary then there's multiple RCV
            for rcv in clinvar_rcv:  # get each RCV in the list
                if 'origin' in rcv:
                    origin = rcv['origin']
                    all_origins.append(origin)
                    if origin == 'germline':
                        num_germline_submitters += rcv['number_submitters']
        elif isinstance(clinvar_rcv, dict):  # just one value for the RCV, not a list of RCVs
            origin = clinvar_rcv['origin']
            all_origins.append(origin)
            if origin == 'germline':
                num_germline_submitters += clinvar_rcv['number_submitters']
        else:
            raise ValueError(f"Unknown clinvar type here: {type(clinvar_rcv)} -> {clinvar_rcv}")

    # Allele origin. ClinVar includes interpretations of variants identified in the germline and as somatic events.
    # Note that allele origin refers to an observation of a variant, not the variant itself, so the same variant may
    # have been reported both as germline and as somatic.
    # https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5753237/
    # e.g. https://myvariant.info/v1/variant/chr1:g.162740327T>C?fields=clinvar
    # e.g. https://myvariant.info/v1/variant/chr10:g.89623716G>A?fields=clinvar
    if "somatic" in all_origins:
        num_germline_submitters = 0

    return num_germline_submitters, all_origins


def get_vars_per_tmb(assay_type: str) -> float:

    if assay_type == 'gs395':
        return 0.84344
    elif assay_type == 'gs180':
        return 2.949
    elif assay_type == 'gs700':
        return 0.56548995746
    else:
        ValueError(f"Unknown assay type {assay_type}")
