"""
Usage of MyVariant.info
chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://docs.myvariant.info/_/downloads/en/latest/pdf/
"""
from myvariant import MyVariantInfo

from tools.myvariantinfo.utils import get_num_rows_to_remove, get_num_germline_submitters_from_clinvar
from tools.utils.tools_utils import get_cid_from_string
from collections import Counter
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import linregress

ORIGINAL_TMB_THRESHOLD = None
UPDATED_TMB_THRESHOLD = None
NUM_GERMLINE_SUBMITTERS_THRESHOLD = 2  # ClinVar needs to have said germline this many times or more


def plot(assay_type: str):
    """
    Business logic
    :return: NoneType
    """

    excel_file = '/Users/cleslin/Desktop/BLI/Bioinformatics/TMB/Samples_TMB_Too_high/Sample-Suspect-High-TMB-v10-20231030.xlsx'
    all_results_excel_file = '/Users/cleslin/Desktop/BLI/Bioinformatics/TMB/Samples_TMB_Too_high/Full-Results-v10-20231102.xlsx'
    model_excel_file = f'/Users/cleslin/Desktop/BLI/Bioinformatics/TMB/Samples_TMB_Too_high/Final_models_{assay_type}.xlsx'
    all_results_df = pd.read_excel(all_results_excel_file,
                                   sheet_name='all_results', usecols=["CID", "PrimarySite", "PrimarySiteDiagnosis"])
    all_results_df['CID'] = all_results_df['CID'].str.lower()
    global UPDATED_TMB_THRESHOLD
    global ORIGINAL_TMB_THRESHOLD
    # ran the submission threshold 1 - 3
    if os.path.exists(model_excel_file):
        os.remove(model_excel_file)
    with pd.ExcelWriter(model_excel_file, engine='openpyxl', mode='w') as writer:
        for submission_threshold in range(1, 4):
            # depending on the assay type
            if assay_type == 'gs395':
                sheet_name = f'analysis_gs395_threshold_{submission_threshold}'
                ORIGINAL_TMB_THRESHOLD = 10
                UPDATED_TMB_THRESHOLD = 10
            elif assay_type == 'gs180':
                sheet_name = f'analysis_gs180_threshold_{submission_threshold}'
                ORIGINAL_TMB_THRESHOLD = 14.745
                UPDATED_TMB_THRESHOLD = 14.745
            else:
                ValueError(f"Unknown assay type {assay_type}")
            # get the data
            df = pd.read_excel(excel_file, sheet_name=sheet_name, usecols=["CID", "original_tmb", "updated_tmb", "vars_removed"])

            # Adding the number of samples >= 10 annotation
            num_samples_original_tmb_ge = sum(df['original_tmb'] >= ORIGINAL_TMB_THRESHOLD)
            num_samples_updated_tmb_ge = sum(df['updated_tmb'] >= UPDATED_TMB_THRESHOLD)
            # plot out the data
            hist_plots(df, num_samples_original_tmb_ge, num_samples_updated_tmb_ge,
                       submission_threshold=submission_threshold, assay_type=assay_type)
            box_plots(df, num_samples_original_tmb_ge, num_samples_updated_tmb_ge,
                      submission_threshold=submission_threshold, assay_type=assay_type)
            box_two_d_histogram(df,  num_samples_original_tmb_ge, num_samples_updated_tmb_ge, type_plot='histplot',
                                submission_threshold=submission_threshold, assay_type=assay_type)
            box_two_d_histogram(df,  num_samples_original_tmb_ge, num_samples_updated_tmb_ge, type_plot='regplot',
                                submission_threshold=submission_threshold, assay_type=assay_type)
            df = pd.merge(df, all_results_df, on='CID', how="left")
            # original_tmb	updated_tmb
            df['delta_tmb'] = df['original_tmb'] - df['updated_tmb']
            df.sort_values(by='delta_tmb', ascending=False, inplace=True)
            df.to_excel(writer, sheet_name=sheet_name)

def box_two_d_histogram(df: pd.DataFrame, num_samples_original_tmb_ge: int, num_samples_updated_tmb_ge: int,
                        type_plot: str=None, submission_threshold: int=2, assay_type: str=None):

    # Creating the boxplots
    plt.figure(figsize=(get_plot_size()))
    title = None
    if assay_type == 'gs395':
        bins = 52
    elif assay_type == 'gs180':  # 20
        bins = 18
    else:
        ValueError(f"Unknown assay type {assay_type}")

    if type_plot == 'histplot':
        sns.histplot(df, x='original_tmb', y='updated_tmb', bins=bins, cbar=True, ) # 34 37 52
        title = '2D Histogram'
    elif type_plot == 'regplot':
        sns.regplot(x='original_tmb', y='updated_tmb', data=df, ci=95, scatter_kws={'s': 50})
        title = 'Regression Plot'
    else:
        ValueError(f"unknown plot {type_plot}")

    plt.axvline(10, color='red', linestyle='--',
                label=f'TMB-H Threshold >= 10 mut/Mb')
    plt.axhline(10, color='red', linestyle='--',
                label=f'TMB-H Threshold >= 10 mut/Mb')

    # Calculating the linear regression parameters
    slope, intercept, r_value, p_value, std_err = linregress(df['original_tmb'], df['updated_tmb'])

    # Calculating R^2
    r_squared = r_value ** 2

    plot_subtitle(num_samples_original_tmb_ge, num_samples_updated_tmb_ge)

    # Adding labels and title
    plt.title(f'{title} of Updated TMB vs Original TMB n={len(df)} submission_threshold={submission_threshold} '
              f'(R^2 = {r_squared:.4f})')
    plt.xlabel('Original TMB')
    plt.ylabel('Updated TMB')
    plt.xlim(0, 50)
    plt.ylim(0, 50)
    plt.plot([0, 50], [0, 50], '--', color='lightgray')
    plt.savefig(f"TMB_{type_plot}_threshold_{submission_threshold}_{assay_type}.svg")
    plt.close()

def box_plots(df: pd.DataFrame, num_samples_original_tmb_ge: int, num_samples_updated_tmb_ge: int,
              submission_threshold: int=2, assay_type: str=None):

    # Melting the DataFrame to long-form for boxplot
    df_long = pd.melt(df, id_vars='CID', var_name='TMB Type', value_name='Value')
    df_long.to_excel("test.xlsx")
    df_long = df_long[df_long['TMB Type'] != 'vars_removed']

    # Creating the boxplots
    plt.figure(figsize=(get_plot_size()))
    sns.stripplot(x='TMB Type', y='Value', data=df_long, size=3, color='red', jitter=True,  linewidth=0.5)
    sns.violinplot(x='TMB Type', y='Value', data=df_long, inner="quartile")

    plt.axhline(10, color='red', linestyle='--',
                label=f'TMB-H Threshold >= 10 mut/Mb')

    plot_subtitle(num_samples_original_tmb_ge, num_samples_updated_tmb_ge)

    # Adding labels and title
    plt.title(f'Boxplots of Original and Updated TMB n={len(df)} submission_threshold={submission_threshold}')
    plt.ylabel('TMB')
    plt.savefig(f"TMB_boxplots_threshold_{submission_threshold}_{assay_type}.svg")
    plt.close()


def hist_plots(df: pd.DataFrame, num_samples_original_tmb_ge: int, num_samples_updated_tmb_ge: int,
               submission_threshold: int=2, assay_type: str=None):
    # Setting up the aesthetics for the plot
    sns.set(style="whitegrid")

    # Calculating the median values
    median_original_tmb = df['original_tmb'].median()
    median_updated_tmb = df['updated_tmb'].median()

    # Creating the distribution plots
    plt.figure(figsize=(get_plot_size()))
    sns.kdeplot(df['original_tmb'], fill=True, label='Original TMB')
    sns.kdeplot(df['updated_tmb'], fill=True, label='Updated TMB')
    # Adding the median lines
    plt.axvline(median_original_tmb, color='blue', linestyle='--',
                label=f'Median Original TMB: {median_original_tmb:.2f}')
    plt.axvline(median_updated_tmb, color='orange', linestyle='--',
                label=f'Median Updated TMB: {median_updated_tmb:.2f}')
    plot_subtitle(num_samples_original_tmb_ge, num_samples_updated_tmb_ge)
    # Adding labels and title
    plt.title(f'Distribution of Original and Updated TMB n={len(df)} submission_threshold {submission_threshold}')
    plt.xlabel('TMB')
    plt.ylabel('Density')

    # Adding a legend
    plt.legend()
    plt.savefig(f"TMB_kde_threshold_{submission_threshold}_{assay_type}.svg")
    plt.close()

def get_plot_size():
    return 10, 10

def plot_subtitle(num_samples_original_tmb_ge: int, num_samples_updated_tmb_ge: int):
    """
    Generic Subtitle plot
    :param num_samples_original_tmb_ge:
    :param num_samples_updated_tmb_ge:
    :return:
    """
    # Adding subtitles with the number of samples >= 10
    plt.text(0.5, -0.15, f'Number of Original TMB samples >= {ORIGINAL_TMB_THRESHOLD}: {num_samples_original_tmb_ge}', ha='center',
             va='top', transform=plt.gca().transAxes, fontsize=10)
    plt.text(0.5, -0.2, f'Number of Updated TMB samples >= {UPDATED_TMB_THRESHOLD}: {num_samples_updated_tmb_ge}', ha='center', va='top',
             transform=plt.gca().transAxes, fontsize=10)
    # Adjusting the plot layout to accommodate the subtitles
    plt.subplots_adjust(bottom=0.25)


def run_all_oc_samples_data_from_metabase(assay_type: str):
    """
    Business logic
    :return: NoneType
    """
    tmb_per_variant, sheet_name = None, None
    if assay_type == 'gs395':
        tmb_per_variant = 0.84344
        sheet_name = 'all_tmb_selected_gs395'
    elif assay_type == 'gs180':
        sheet_name = 'all_tmb_selected_gs180'
        tmb_per_variant = 2.949
    else:
        ValueError(f"Unknown assay type {assay_type}")

    my_variant = MyVariantInfo()
    excel_file = '/Users/cleslin/Desktop/BLI/Bioinformatics/TMB/updated_model/GS180_GS395_TMB_Value_True-v10-20231102.xlsx'
    all_origins = []
    germline_var_ids_to_remove = []
    df = pd.read_excel(excel_file, sheet_name=sheet_name)
    df['cid'] = df.apply(lambda row: get_cid_from_string(row['ID']), axis=1)
    cids = sorted(list(set(df['cid'].tolist())))
    for i, cid in enumerate(cids):
        df_filtered = df[df['cid'] == cid]
        original_tmb = round(len(df_filtered) * tmb_per_variant, 3)
        num_rows_to_remove, origins, germline_var_ids \
            = get_num_rows_to_remove(df=df_filtered, my_variant=my_variant,
                                     num_submitters_threshold=NUM_GERMLINE_SUBMITTERS_THRESHOLD)
        all_origins = all_origins + origins
        germline_var_ids_to_remove += germline_var_ids
        if num_rows_to_remove > 0:
            new_tmb = round(original_tmb - round(num_rows_to_remove * tmb_per_variant, 3), 3)
        else:
            new_tmb = original_tmb

        #print(f"cid {cid} original TMB {original_tmb} new tmb {new_tmb} removed {num_rows_to_remove} variants",
        #     flush=True)

    # Ran this across an entire spreadsheet's of data
    # CID23-2428
    # cid test_many_variants original TMB 7539.51 new tmb 6632.812, removed 1075 variants
    # Counter({'germline': 4260, 'unknown': 200, 'somatic': 164, 'not applicable': 30, 'not provided': 2, 'maternal': 1, 'de novo': 1})
    #final_origins = Counter(all_origins)
    #print(final_origins)
    #filtered = df[df['QC_PRIMARY_CALLER'] != 'pindel']
    #final_var_ids = Counter(filtered['VAR_ID'].tolist())
    #print(final_var_ids)
    #print(Counter(germline_var_ids_to_remove))
    [print(n) for n in Counter(germline_var_ids_to_remove)]


def main():
    """
    Business logic
    :return: NoneType
    """
    #examples_data()
    tmb_per_variant = 0.84344
    my_variant = MyVariantInfo()
    excel_file = '/Users/cleslin/Desktop/BLI/Bioinformatics/TMB/Samples_TMB_Too_high/Sample-Suspect-High-TMB-v10-20231030.xlsx'
    #excel_file = '/Users/cleslin/Desktop/BLI/Bioinformatics/TMB/Samples_TMB_Too_high/test_many_variants.xlsx'
    #excel_file = '/Users/cleslin/Desktop/BLI/Bioinformatics/TMB/Samples_TMB_Too_high/test_many_variants2.xlsx'
    all_origins = []
    for sheet_name in pd.ExcelFile(excel_file).sheet_names:
        #if sheet_name not in ('CID23-2428-agilent'):
        #   continue
        if sheet_name in ('Stopped') or sheet_name.startswith('analysis'):
            continue
        print(f"{sheet_name}\t", end='', flush=True)
        df = pd.read_excel(excel_file, sheet_name=sheet_name, skiprows=[0,1])
        original_tmb = round(len(df) * tmb_per_variant, 3)
        num_rows_to_remove, origins, _ = get_num_rows_to_remove(df=df, my_variant=my_variant,
                                                                num_submitters_threshold=2)
        all_origins = all_origins + origins
        if num_rows_to_remove > 0:
            new_tmb = round(original_tmb - round(num_rows_to_remove * tmb_per_variant, 3), 3)
        else:
            new_tmb = original_tmb

        print(f"cid {sheet_name} original TMB {original_tmb} new tmb {new_tmb}, removed {num_rows_to_remove} variants",
              flush=True)

    # Ran this across an entire spreadsheet's of data
    # CID23-2428
    # cid test_many_variants original TMB 7539.51 new tmb 6632.812, removed 1075 variants
    # Counter({'germline': 4260, 'unknown': 200, 'somatic': 164, 'not applicable': 30, 'not provided': 2, 'maternal': 1, 'de novo': 1})
    final_origins = Counter(all_origins)


def examples_data():
    mv = MyVariantInfo()
    #var = mv.format_hgvs("11", 32421544, "A", "G")  # has 5 total germline submission 1,1,1,2
    #print(var)
    # var = mv.format_hgvs("1", 158604367, "C", "T")  # has 1 total germline submission 1
    # var = mv.format_hgvs("1", 16458645, "C", "T")  # has 3 total germline submission 1,2
    # var = mv.format_hgvs("1", 39783013, "G", "T")  # has 1 total germline submission 1, dbSNP ID not found in excel
    # var = mv.format_hgvs("1", 39853641, "G", "T")  # has 1 total germline submission 1, dbSNP ID not found in excel
    # var = mv.format_hgvs("17", 7797182, "C", "T")  # has 0 total germline submission
    # var = mv.format_hgvs("2", 25505471, "C", "T")  # has 1 total germline submission 1
    # var = mv.format_hgvs("4", 187539783, "C", "T")  # has 1 total germline submission 1
    # var = mv.format_hgvs("6", 157525120, "A", "G")  # has 6 total germline submission 1, dbSNP ID not found in excel

    # clinvar = mv.getvariant(var, fields='clinvar')
    my_vars = []
    my_vars.append('chr16:g.57071113_57071114delinsCG')  #  delins example: submitters 1 ['germline']
    my_vars.append('chr10:g.104264107C>T')  #  SNV example: submitters 9 ['germline', 'germline', 'germline', 'germline', 'germline']
    my_vars.append('chr3:g.142215368_142215369del')  #  deletion example: submitters 5 ['germline', 'germline', 'germline', 'germline']
    my_vars.append('chr18:g.42456670_42456671insTCTT')  #  insertion example: submitters 4 ['germline', 'germline', 'germline']
    my_vars.append('chr10:g.89623716G>A')  #  Somatic SNV example: submitters  0 ['germline', 'germline', 'somatic', 'germline']
    my_vars.append('chr12:g.25398281C>T')  #  Somatic SNV example: submitters  0  ['somatic', 'somatic', 'somatic', 'somatic', 'somatic', 'somatic', 'somatic', 'somatic', 'somatic', 'germline', 'germline', 'somatic', 'germline', 's
    my_vars.append('chr13:g.32954030dup')  #  Somatic Insertion example: submitters  0  ['germline', 'germline', 'germline', 'germline', 'germline', 'germline', 'germline', 'somatic', 'unknown', 'germline', 'germline', 'germline']

    for my_var in my_vars:
        clinvar = mv.getvariant(my_var, fields='clinvar')
        num_subs, origins = get_num_germline_submitters_from_clinvar(clinvar)
        print(f"submitters {num_subs} {origins}")


if __name__ == "__main__":
    #examples_data()
    #main()
    run_all_oc_samples_data_from_metabase(assay_type='gs395')
    #run_all_oc_samples_data_from_metabase(assay_type='gs180')
    #plot(assay_type='gs180')
    #plot(assay_type='gs395')
