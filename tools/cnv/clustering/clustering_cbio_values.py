"""
Module used to cluster the cBioPortal - Discrete Copy Number Data calculated by Alexis


https://docs.cbioportal.org/5.1-data-loading/data-loading/file-formats#discrete-copy-number-data
For each gene-sample combination, a copy number level is specified:
"-2" is a deep loss, possibly a homozygous deletion
"-1" is a single-copy loss (heterozygous deletion)
"0" is diploid
"1" indicates a low-level gain
"2" is a high-level amplification.

Genes we care about the most.  From Rodrigo

ATM
BRCA1
BRCA2
CCND1
CDKN2A
EGFR
ERBB2
ESR1
FGF19
FGFR1
FGFR2
FGFR3
MET
MYC
MYCN
NOTCH1
NOTCH2
PIK3CA
PTEN
TP53


"""
import argparse
import os
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame
import math

from tools.cnv.utils import get_list_important_cnv_genes
from tools.utils.tools_utils import get_onco_tree_data

def main():
    """Main driver for the program"""
    args = get_cli_args()
    infile_cbio = args.input_cbio_discrete_values
    infile_full_results = args.input_full_results_metabase
    cluster_type = args.cluster_type
    mcc_filter = args.mcc_filter
    cid_file_list = args.cid_file

    if "snapshotgsv2" in infile_cbio:
        assay = "snapshotgsv2"
    elif "snapshot2" in infile_cbio:
        assay = "snapshot2"
    else:
        raise ValueError(f"Dont know this type of assay in {infile_cbio}")

    list_cids_to_keep = []

    title = f"Assay {assay} MCC filter {mcc_filter} hierarchical clustering method {cluster_type}"

    # name the image output files
    outfile_all = f'cnv_cbio_{assay}_values_mcc_{mcc_filter}_all_data_cluster_type_{cluster_type}.pdf'
    outfile_cnv_genes = f'cnv_cbio_{assay}_values_mcc_{mcc_filter}_filtered_genes_data_cluster_type_{cluster_type}.pdf'
    research_cid_filtered_str = 'research_cid_filtered'

    df_full_results = pd.read_csv(infile_full_results)
    regex = r'Results\d+'
    # update the CIDs
    df_full_results['Results'] = df_full_results['Results'].str.replace(regex, '', regex=True).str.replace('CID', 'cid')



    # take care of oncoTree mapping
    onco_tree_data = get_onco_tree_data(json_file=args.onco_json_file)
    cid_to_primary_dx_dict = \
        pd.Series(df_full_results['Primary_Site_Diagnosis'].values, index=df_full_results['Results']).to_dict()

    # if you get "AttributeError: 'float' object has no attribute 'lower'" then print out and see what CIDs have
    # non string as their value
    # { print(f"{k}: {v} {type(v)}") for k, v in sorted(cid_to_primary_dx_dict.items()) }

    # lower case the primary site
    cid_to_primary_dx_dict = {k: v.lower() for k, v in cid_to_primary_dx_dict.items()}

    # get dictionaries that can be used later on for filtering
    cid_to_batch_mode_dict = pd.Series(df_full_results['Batch_Mode'].values, index=df_full_results['Results']).to_dict()
    cid_to_MCC_mode_dict = pd.Series(df_full_results['MCC'].values, index=df_full_results['Results']).to_dict()

    # import data from csv to dataframe using pandas..
    df_cnv_cbio = pd.read_csv(infile_cbio, sep="\t")
    df_cnv_cbio = df_cnv_cbio.drop('sample_url', 1)  # get rid of the column

    # change the header name to remove the CID and replace with cid
    df_cnv_cbio.columns = df_cnv_cbio.columns.str.replace(regex, '', regex=True).str.replace('CID', 'cid')
    print(df_cnv_cbio.columns)
    # was there an input file containing CIDs to keep.  If so remove all CIDs that were not in this file
    print(f"Original  size of cbio data frame before removing CIDs {len(df_cnv_cbio.columns)}")
    if cid_file_list is not None:
        # get the list of cids we should keep
        list_cids_to_keep = get_list_cids_from_file(file=cid_file_list)
        for col_name in df_cnv_cbio.columns:
            if col_name.startswith('cid'):
                # if it was not in the list drop
                if col_name not in list_cids_to_keep:
                    try:
                        df_cnv_cbio.drop(col_name, axis=1, inplace=True)
                    except KeyError:  # entries likes cid20-620 that have cid20-620Results1 cid20-620Results2 that
                        # will be deleted already when the first came
                        pass
        print(f"Size of cbio data frame after removing CIDs {len(df_cnv_cbio.columns)}")

    print(f"length of df_cnv_cbio BEFORE dropping 'clinical' or MCC threshold {len(df_cnv_cbio.columns)}")
    if mcc_filter:
        # drop the columns with CIDs not in clinical or do not pass MCC
        for cid, batch_mode in cid_to_batch_mode_dict.items():
            if not batch_mode == 'clinical' or cid_to_MCC_mode_dict[cid] < mcc_filter:
                if cid in df_cnv_cbio.columns:
                    #print(f"Dropping here {cid}")
                    df_cnv_cbio.drop(cid, axis=1, inplace=True)

    print(f"length of df_cnv_cbio AFTER dropping 'clinical' or MCC threshold {len(df_cnv_cbio.columns)}")

    # remove rows with None, these were SNPs
    df_cnv_cbio = df_cnv_cbio[~df_cnv_cbio['Hugo_Symbol'].str.contains("None")]

    df_cnv_cbio_copy = df_cnv_cbio.copy()  # create a copy
    gene_column_all = df_cnv_cbio_copy.pop('Hugo_Symbol')  # pop off Hugo_Symbol and use this for labels

    # first drop the Entrez_Gene_Id b/c this will throw off clustering
    #df_cnv_cbio_copy.drop('Entrez_Gene_Id', axis=1, inplace=True)
    entrez_ids_all = df_cnv_cbio_copy.pop('Entrez_Gene_Id')  # pop off to use later
    # update the column names df_cnv_cbio_copy with oncoTree codes

    #column_names = ['_'.join((col, onco_tree_data[cid_to_primary_dx_dict[col]]["ONCOTREE_CODE"].replace(" ", '')))
    #                for col in df_cnv_cbio_copy.columns]
    # updated column names
    #df_cnv_cbio_copy.columns = column_names

    """
    The clustermap module within the Seaborn package does not allow for NaN values. 
    So we must create a mask value that does not interfere much with the clustering and is likely to be unique. 
    No imputation is used. By using a value close to 0, we are saying that these are unchanged between samples. 
    Better solutions should be used to deal with missing data points.
    """
    if assay == 'snapshot2':
        mask_na = 0.000666
        df_cnv_cbio_copy = df_cnv_cbio_copy.fillna(mask_na)

    cmap = 'coolwarm'
    # https://seaborn.pydata.org/generated/seaborn.clustermap.html
    # https://docs.scipy.org/doc/scipy/reference/reference/generated/scipy.clusoter.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage
    # https://www.youtube.com/watch?v=vg1w5ZUF5lA
    g = sns.clustermap(df_cnv_cbio_copy, yticklabels=gene_column_all, figsize=(200, 50), metric='euclidean',
                       method=cluster_type, cmap=cmap)
    g.fig.suptitle(f'{title}', fontsize=40)
    plt.savefig(outfile_all, dpi=150)

    # filter the data
    df_cnv_cbio_filtered_by_gene = df_cnv_cbio[df_cnv_cbio['Hugo_Symbol'].isin(get_list_important_cnv_genes())].copy()
    gene_column_filtered = df_cnv_cbio_filtered_by_gene.pop('Hugo_Symbol')

    # update the column names df_cnv_cbio_copy with oncoTree codes
    entrez_ids_filtered = df_cnv_cbio_filtered_by_gene.pop('Entrez_Gene_Id')
    #column_names = ['_'.join((col, onco_tree_data[cid_to_primary_dx_dict[col]]["ONCOTREE_CODE"].replace(" ", '')))
    #                for col in df_cnv_cbio_filtered_by_gene.columns]
    #df_cnv_cbio_filtered_by_gene.columns = column_names

    g = sns.clustermap(df_cnv_cbio_filtered_by_gene, yticklabels=gene_column_filtered, figsize=(200, 20),
                       metric='euclidean', method=cluster_type, cmap=cmap)
    g.fig.suptitle(f'{title}', fontsize=40)
    plt.savefig(outfile_cnv_genes, dpi=150)

    # get new TSV files names
    outfile_all = "_".join((os.path.basename(infile_cbio).split('.')[0], "mcc", str(mcc_filter),
                            research_cid_filtered_str + ".tsv"), )
    outfile_cnv_genes = "_".join((os.path.basename(infile_cbio).split('.')[0], "mcc", str(mcc_filter), "filtered_genes",
                                  research_cid_filtered_str + ".tsv"))
    # insert the Hugo_Symbol and Entrez_Gene_Id columns in all data
    df_cnv_cbio_copy.insert(loc=0, column='Hugo_Symbol', value=gene_column_all)
    # a few genes had nulls which get NaN when this column as coverted to floats.  MET, TERT, MEN1 and TSC1
    df_cnv_cbio_copy.insert(loc=1, column='Entrez_Gene_Id',
                            value=[-1 if math.isnan(x) else int(x) for x in entrez_ids_all])
    # print out the all data TSV
    print_data_frame(df=df_cnv_cbio_copy, tabular_output_file=outfile_all)

    # insert the Hugo_Symbol and Entrez_Gene_Id columns in gene filtered list
    df_cnv_cbio_filtered_by_gene.insert(loc=0, column='Hugo_Symbol', value=gene_column_filtered)
    df_cnv_cbio_filtered_by_gene.insert(loc=1, column='Entrez_Gene_Id', value=[-1 if math.isnan(x) else int(x)
                                                                               for x in entrez_ids_filtered])
    # print out the gene filtered TSV
    print_data_frame(df=df_cnv_cbio_filtered_by_gene, tabular_output_file=outfile_cnv_genes)


def get_list_cids_from_file(file: str) -> list:
    """
    Open the list of one cid per line and return a list
    """
    with open(file) as infh:
        return list(set([x.rstrip() for x in infh]))

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Module used to cluster the cBioPortal - Discrete Copy Number Data calculated by Alexis"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--input_cbio_discrete_values',
                        dest='input_cbio_discrete_values',
                        help="cBioPortal - Discrete Copy Number Data calculated by Alexis", required=True,
                        type=str)

    parser.add_argument('--input_full_results_metabase',
                        dest='input_full_results_metabase',
                        help="Metabase query of full results to map to oncocodes", required=True,
                        type=str)

    parser.add_argument('--onco_json_file', dest='onco_json_file', help="OncoTree JSON file from Cider",
                        default=None, required=True, type=str)

    parser.add_argument('--cid_file', dest='cid_file', help="Only keep CIDs in this list in the overall analysis, i.e.,"
                                                            "this acts a filter to only keep the CIDs in the list",
                        default=None, required=False, type=str)

    parser.add_argument('--mcc_filter', dest='mcc_filter', help="Filter based on mcc",
                        default=None, required=True, type=int)

    parser.add_argument('--cluster_type', dest='cluster_type', help="Cluster method",
                        default=None, required=True, type=str, choices=['single', 'complete', 'average', 'ward', 'centroid'])

    return parser.parse_args()


if __name__ == "__main__":
    main()
