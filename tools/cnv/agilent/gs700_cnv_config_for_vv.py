""" Generate the cnv config used for the CNV visualization in VV """
import argparse
import json
import sys
import os

from tools.cnv.utils import get_list_important_cnv_genes


def main():
    """Business logic"""

    args = get_cli_args()
    json_input = args.gene_map_json
    # get the gene map from the JSON
    with open(json_input, 'r', encoding='utf-8') as infh:
        gene_map_ds = json.load(infh)

    # get the gene map dictionary.  gene -> list of
    chromosome_to_gene_map = get_chromosome_to_gene_map(gene_list=gene_map_ds['data'].keys(),
                                                        gene_map=gene_map_ds['data'])
    # We do not have chromosome X in GS700 CNV
    #del chromosome_to_gene_map['X']

    # update the map based on target number of genes to keep per interval and if the gene was an important CNV
    update_gene_map(chromosome_to_gene_map=chromosome_to_gene_map, target_gene_count=15)

    # Count the number of important_cnv == True
    important_cnv_count = count_important_cnv(chromosome_to_gene_map)
    # Test to make sure the total number kept is the same number found in get_list_important_cnv_genes()
    assert important_cnv_count == len(get_list_important_cnv_genes()), \
        "Lengths for found genes not same as get_list_important_cnv_genes "

    # print out the final JSON for the CNV visualization
    output_final_config(chromosome_to_gene_map, file=json_input)


def output_final_config(gene_map, file) -> None:
    """
    Print the final JSON config for the CNV visualization
    :param gene_map: Dictionary of list
    :param file: gene map json that will be used to create the name of the file
    :return: None

    Output the final config in the format:
    [
    {
        gene: gene
        clinical: true/false
        chr: chr
    },
    ..
    ..
    ]
    """

    json_file = f"cnv-visualization-{os.path.basename(file)}"
    final_data = []
    for chrom, gene_list in sorted(gene_map.items(), key=lambda x: x[0]):
        for gene in gene_list:
            final_data.append({'gene': gene['gene'], 'clinical': gene['important_cnv'], 'chr': chrom})

    with open(json_file, 'w', encoding='utf-8') as out_fh:
        json.dump(final_data, out_fh, indent=4)


def count_important_cnv(gene_map) -> int:
    """
    Function to count the number of important_cnv == True
    :param gene_map:
    :return: int
    """

    count = 0
    for chromosome in gene_map.values():
        for gene in chromosome:
            if gene['important_cnv']:
                assert gene['gene'] in get_list_important_cnv_genes(), f"{gene['gene']} is not a CNV gene"
                count += 1
    return count


def update_gene_map(chromosome_to_gene_map: dict = None, target_gene_count: int = None):
    """
     Update the map based on target number of genes to keep per interval and if the gene was an important CNV
    :param chromosome_to_gene_map:
    :param target_gene_count:
    :return:
    """

    for chromosome in sorted(chromosome_to_gene_map.keys(), key=lambda x: x):  # pylint: disable=unnecessary-lambda
        assert chromosome in chromosome_to_gene_map, f"{chromosome} not in gene_amp"
        print(f"chromosome {chromosome} ORIGINALLY has {len(chromosome_to_gene_map[chromosome])} genes",
              file=sys.stderr)
        reduced_gene_list_sorted = reduce_genes_for_chromosome(chromosome_to_gene_map[chromosome],
                                                               target_gene_count=target_gene_count,
                                                               chromosome=chromosome)
        chromosome_to_gene_map[chromosome] = reduced_gene_list_sorted
        print(f"chromosome {chromosome} NOW has {len(chromosome_to_gene_map[chromosome])} genes",
              file=sys.stderr)


def calculate_spacing(genes):
    """
    Function to calculate spacing between genes from the list of gene dictionaries
    :param genes: list of gene dictionaries
    :return: list of spacings
    """

    spacings = []
    for i in range(len(genes) - 1):
        spacing = genes[i + 1]['start'] - genes[i]['stop']
        spacings.append((spacing, i))
    return spacings


def reduce_genes_for_chromosome(gene_data: list = None,
                                target_gene_count: int = None,
                                chromosome: str = None) -> list:
    """
    Processes a chromosome's gene list to reduce the total number of genes to target_gene_count
    while preserving important genes (important_cnv is True) as much as possible and considering gene
    intervals for optimal spacing. This function will:
    1). Keep all important genes.
    2). Remove less important genes, aiming for an even distribution across the chromosome by considering their start
    and stop positions.
    3). Prioritize the removal of genes that are closer together, to maximize the spacing between remaining genes,
    especially around important ones.

    :param gene_data: list of genes dictionaries in a chromosome
    :param target_gene_count:  # number of genes to keep
    :param chromosome: str of the chromosome
    :return: Update list of genes dictionaries in a chromosome
    """

    # Separate important and non-important genes
    important_genes = [gene for gene in gene_data if gene['important_cnv']]
    len_important_genes = len(important_genes)
    print(f"chromosome {chromosome} has {len_important_genes} important CNV genes", file=sys.stderr)
    non_important_genes = [gene for gene in gene_data if not gene['important_cnv']]

    # Sort non-important genes by their start position
    non_important_genes_sorted = sorted(non_important_genes, key=lambda x: x['start'])

    # Calculate the number of genes to remove
    total_genes_to_keep = len(important_genes) + (target_gene_count - len(important_genes))
    genes_to_remove = len(gene_data) - total_genes_to_keep

    # Remove genes with the smallest spacing until we reach the desired count
    while genes_to_remove > 0 and non_important_genes_sorted:
        spacings = calculate_spacing(non_important_genes_sorted)
        spacings.sort()  # Sort by spacing to find the smallest
        if not spacings:
            break  # Break if there are no more spacings to evaluate
        _, idx_to_remove = spacings[0]  # Get index of the gene to remove
        del non_important_genes_sorted[idx_to_remove]  # Remove the gene with the smallest spacing
        genes_to_remove -= 1

    # Combine important genes with the remaining non-important genes
    reduced_gene_list = important_genes + non_important_genes_sorted

    # Sort the final list by start position to maintain chromosome order
    reduced_gene_list_sorted = sorted(reduced_gene_list, key=lambda x: x['start'])
    return reduced_gene_list_sorted


def get_chromosome_to_gene_map(gene_list: list = None, gene_map: dict = None) -> dict:
    """
    :param gene_list: List of genes to retreive
    :param gene_map: The gene map used in nucleus
    :return: dict (see below)

    Example gene_data returned for chromosome 9 (key) -> (value) is a list:
    [
    {'gene': 'ABL1', 'start': '133589706', 'stop': '133761070', 'important_cnv': False},
    {'gene': 'CD274', 'start': '5456113', 'stop': '5467862', 'important_cnv': True},
    {'gene': 'CDKN2A', 'start': '21968207', 'stop': '21994330', 'important_cnv': True},
    {'gene': 'CDKN2B', 'start': '22005985', 'stop': '22008952', 'important_cnv': True},
    {'gene': 'EGFL7', 'start': '139562734', 'stop': '139566738', 'important_cnv': False},
    {'gene': 'FANCC', 'start': '97863988', 'stop': '98011573', 'important_cnv': True},
    {'gene': 'FANCG', 'start': '35074104', 'stop': '35079521', 'important_cnv': False},
    {'gene': 'GNAQ', 'start': '80336238', 'stop': '80646151', 'important_cnv': False},
    {'gene': 'HNRNPK', 'start': '86584264', 'stop': '86593167', 'important_cnv': False},
    {'gene': 'JAK2', 'start': '5021987', 'stop': '5126791', 'important_cnv': False},
    {'gene': 'KLF4', 'start': '110248031', 'stop': '110251453', 'important_cnv': False},
    {'gene': 'MLLT3', 'start': '20346440', 'stop': '20622255', 'important_cnv': False},
    {'gene': 'MTAP', 'start': '21802747', 'stop': '21931198', 'important_cnv': False},
    {'gene': 'NOTCH1', 'start': '139390522', 'stop': '139440238', 'important_cnv': True}
    ]
    """

    chrom_map = {}
    for gene in gene_list:
        chrom = gene_map[gene]['chrom']
        if len(gene_map[gene]['snv_meta_data']['intervals']) == 0:  # these are fusion only genes, i.e. not for CNVs
            print(f"No intervals for fusion only {gene}", file=sys.stderr)
            continue
        start = int(gene_map[gene]['snv_meta_data']['intervals'][0]['start'])
        stop = int(gene_map[gene]['snv_meta_data']['intervals'][-1]['stop'])
        # was this found to be a list of important genes for CNVs
        important_cnv = False
        if gene in get_list_important_cnv_genes():
            important_cnv = True
        # store the following dictionary in the list
        gene_start_stop = {'gene': gene, 'start': start, 'stop': stop, 'important_cnv': important_cnv}
        if chrom in chrom_map:
            chrom_map[chrom].append(gene_start_stop)
        else:
            chrom_map[chrom] = [gene_start_stop]
    return chrom_map


def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Generate the CNV Visualization Config used for the CNV visualization in VV"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--gene_map_json',
                        dest='gene_map_json',
                        required=True,
                        help="The gene map to use, e.g. gs700-gene-list-v13-20230726.json")

    return parser.parse_args()


if __name__ == "__main__":
    main()
