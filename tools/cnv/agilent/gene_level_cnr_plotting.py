import argparse
import pandas as pd
import numpy as np

from tools.utils.tools_utils import open_directory_get_list_files_recursively
from tools.agilent700.analyze_hsmetrcis_outputs import SAMPLE_COL
from lab_validation_120.utils.pandas_dataframes_utilities import get_columns_from_cider_gene_cnv_file, \
    read_dataset_and_get_pandas_df
from tools.utils.tools_utils import get_cid_from_string
from tools.cnv.utils import get_list_important_cnv_genes_with_oncokb, get_heatmap_cmap, plot_cnv_cluster_or_heat_map, \
    plot_barchart_median_values_per_gene


def main():
    """Business Logic"""
    args = get_cli_args()
    analysis = args.analysis
    all_genes = args.all_genes
    filter_required = False
    directory = ''
    type_tissue = 'tumor'
    if  analysis == 'agilent_run_1_2_after_cnv_model_update':
        directory = 'scratch/agilent700/cnv/cnv-validation/somatic_samples/after_cnv_model_update/'

    elif analysis == 'agilent_run_pon_after_cnv_model_update_1':
        directory = 'scratch/agilent700/cnv/cnv-validation/pon_samples/after_cnv_model_update_1/'
        type_tissue = 'normal'

    elif analysis == 'agilent_run_pon_after_cnv_model_update_1_removed_high_variance_samples':
        directory = 'scratch/agilent700/cnv/cnv-validation/pon_samples/after_cnv_model_update_1_removed_high_variance_samples'
        type_tissue = 'normal'

    elif analysis == 'gs180_gs395_samples_used_in_validation':
        directory = 'scratch/agilent700/cnv/cnv-validation/somatic_samples/gs180_gs395_samples_used_in_validation/'
        filter_required = True

    elif analysis == 'agilent_run_1_2_before_cnv_model_update':
        directory = 'scratch/agilent700/cnv/cnv-validation/somatic_samples/before_cnv_model_update/'
        filter_required = True

    elif analysis == 'agilent_run_pon_before_cnv_model_update':
        directory = 'scratch/agilent700/cnv/cnv-validation/pon_samples/before_cnv_model_update/'
        type_tissue = 'normal'
        filter_required = True

    elif analysis == 'gs395_run_after_cnv_model_update':
        directory = 'scratch/agilent700/cnv/cnv-validation/pon_samples/before_cnv_model_update/'
        type_tissue = 'normal'
        filter_required = True

    elif analysis == 'gs395_run_pon_after_cnv_model_update':
        directory = 'lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_pon/used_for_cnv_model'
        type_tissue = 'normal'
        filter_required = True

    gene_cnr_df = process_cnr_metrics(directory=directory, sample_col=SAMPLE_COL, type_tissue=type_tissue)
    # Set the threshold value
    threshold_neg = -9.0
    # Replace values below the threshold with a max negative number
    max_negative_number = -9.0  # Set this value as per your preference
    gene_cnr_df['log2cnr'] = np.where(gene_cnr_df['log2cnr'] < threshold_neg, max_negative_number, gene_cnr_df['log2cnr'])

    threshold_pos = abs(max_negative_number)
    max_positive_number = threshold_pos  # Set this value as per your preference
    gene_cnr_df['log2cnr'] = np.where(gene_cnr_df['log2cnr'] > threshold_pos, max_positive_number, gene_cnr_df['log2cnr'])

    if all_genes is False:
        gene_cnr_df = gene_cnr_df[gene_cnr_df['gene'].isin(get_list_important_cnv_genes_with_oncokb())]
        plt_figure = (15, 13)
        dendrogram_ratio = (0.05, 0.05)
        cell_font_size = 8
        box_plt_figure = (10, 10)
    else:
        plt_figure = (150, 8)
        dendrogram_ratio = (0.002, 0.05)
        cell_font_size = 4
        box_plt_figure = (10, 90)

    # Pivot the DataFrame to create a heatmap
    heatmap_data = gene_cnr_df.pivot(index='sample', columns='gene', values='log2cnr')
    heatmap_data.to_excel(f"{analysis}_cnv_matrix_important_genes_{all_genes}.xlsx")

    if filter_required is True:
        # Define the threshold for the number of -inf values allowed in a column
        threshold = 5  # Adjust this value as needed
        # Count the number of -inf values in each gene column
        inf_counts = heatmap_data.iloc[:, 1:].apply(lambda col: (col == -np.inf).sum())
        # Identify columns with more than the threshold number of -inf values
        columns_to_remove = inf_counts[inf_counts > threshold].index.tolist()
        # Remove the identified columns from the DataFrame
        heatmap_data = heatmap_data.drop(columns=columns_to_remove)
        mask_na = 0.000000666
        heatmap_data.replace([np.inf, -np.inf], np.nan, inplace=True)
        heatmap_data.fillna(mask_na, inplace=True)

    base_file_name = f"{analysis}_gene_median_important_genes_{all_genes}"
    plot_barchart_median_values_per_gene(heatmap_data=heatmap_data, fig_size=box_plt_figure, all_genes=all_genes,
                                         analysis_for_title=analysis, base_file_name=base_file_name,
                                         value_to_plot='log2ratio Median')
    # figure params
    params = {
        'legend.fontsize': 'x-large',
        'figure.figsize': plt_figure,
        'axes.labelsize': 'x-large',
        'axes.titlesize': 'x-large',
        'xtick.labelsize': 10,
        'ytick.labelsize': 10
    }
    cmap = get_heatmap_cmap()
    # plot cluster map
    base_file_name = f"{analysis}_cnv_clustermap_important_genes_{all_genes}"
    plot_cnv_cluster_or_heat_map(heatmap_data=heatmap_data,
                                 cmap=cmap, params=params,
                                 base_file_name=base_file_name,
                                 fig_size=plt_figure,
                                 cell_font_size=cell_font_size,
                                 dendrogram_ratio=dendrogram_ratio,
                                 analysis_for_title=analysis)
    # plot heatmap
    base_file_name = f"{analysis}_cnv_heatmap_important_genes_{all_genes}"
    plot_cnv_cluster_or_heat_map(heatmap_data=heatmap_data,
                                 cmap=cmap, params=params,
                                 base_file_name=base_file_name,
                                 fig_size=plt_figure,
                                 cell_font_size=cell_font_size,
                                 dendrogram_ratio=dendrogram_ratio,
                                 analysis_for_title=analysis,
                                 is_clustermap=False)


def process_cnr_metrics(directory: str, sample_col: str, type_tissue: str='tumor') -> pd.DataFrame:
    """
    Process the directory for the CNR values
    :param directory: dir where the data is found
    :param sample_col: Simple column name to use throughout
    :param type_tissue:  tumor or normal
    :return: NoneType
    """
    list_hsmetrics_files = open_directory_get_list_files_recursively(directory=directory,
                                                                     file_glob=f"*chroms.{type_tissue}.gene.cnr.txt")
    all_data = []
    for file in list_hsmetrics_files:
        df = read_dataset_and_get_pandas_df( col_vals=get_columns_from_cider_gene_cnv_file(),
                                             file_name=file)
        df[sample_col] = get_cid_from_string(file)
        all_data.append(df)
    df = pd.concat(all_data).reset_index(drop=True)
    df.sort_values(by="gene", inplace=True)
    return df


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Get the Gene-level plotting of CNR from our pipeline"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--analysis', dest='analysis',
                        help="analysis to perform", required=True,
                        choices=[
                            'agilent_run_1_2_before_cnv_model_update',
                            'agilent_run_1_2_after_cnv_model_update',
                            'agilent_run_pon_before_cnv_model_update',
                            'agilent_run_pon_after_cnv_model_update_1',
                            'agilent_run_pon_after_cnv_model_update_1_removed_high_variance_samples',
                            'gs180_gs395_samples_used_in_validation',
                            'gs395_run_pon_after_cnv_model_update',
                        ])
    feature_parser = parser.add_mutually_exclusive_group(required=True)
    feature_parser.add_argument('--all_genes', dest='all_genes', action='store_true')
    feature_parser.add_argument('--only_important_genes', dest='all_genes', action='store_false')
    parser.set_defaults(all_genes=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()