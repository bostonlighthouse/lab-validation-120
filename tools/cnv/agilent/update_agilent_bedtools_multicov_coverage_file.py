"""
The Agilent coveage file from bedtools mulitcov does not follow the CNV input files we've been using to create
the coefficient file.  This program will update so it does
"""
import argparse
import pandas as pd
import os

PRIMER_INDEX = 0
TMP_GENE = ''
def main():
    """Business logic"""
    args = get_cli_args()

    # get the bam file names
    with open(args.input_bam_file_names) as infh:
        bams = [os.path.basename(file.rstrip()) for file in infh]
    # column names for the input_coverage_file
    cols = ['Chr', 'Start', 'End', 'chr2', 'start2', 'end2', 'annotation', 'ignore'] + bams
    input_coverage_file_df = pd.read_csv(args.input_coverage_file, sep="\t", header=None,
                                         names=cols)
    # drop duplicates that occurred b/c of the bedtools overlap
    input_coverage_file_df = input_coverage_file_df.drop_duplicates(inplace=False, subset=['Chr', 'Start', 'End'])
    # add new column so it can be merged with depth_of_coverage_df
    input_coverage_file_df['Target'] = input_coverage_file_df.apply(lambda row: str(row['Chr']) + ':'
                                                                                + str(row['Start'] + 1) + '-' +
                                                                                str(row['End']), axis=1)
    depth_of_coverage_df = pd.read_csv(args.gene_level_data, sep="\t")

    final_df = pd.merge(input_coverage_file_df, depth_of_coverage_df[['Target', 'gene']], how='left', on='Target')

    final_df['Primer'] = final_df.apply(lambda row: primer_value(row, row['gene']), axis=1)
    #input_coverage_file_df.to_csv('test.tsv', sep="\t")
    final_df.to_csv('test2.tsv', sep="\t")
    #print(input_coverage_file_df.head())
    #print(depth_of_coverage_df.head())
    cols = ['Chr', 'Start', 'End', 'Primer'] + bams
    cov_filname = f"{os.path.splitext(args.input_coverage_file)[0]}_updated_gene_names.txt"

    final_df.to_csv(cov_filname, sep="\t", columns=cols, index=False, header=False)

def primer_value(row: pd.Series, gene: str):
    global PRIMER_INDEX
    global TMP_GENE

    if gene == 'SERPINB4;SERPINB3':
        # Agilent has the following, just update b/c the SERPINB4 if found right after this in the BED file
        # The final coverage file cannot have duplicate intervals, and since SERPINB4 comes before SERPINB3 on the
        # genome SERPINB4 gene was chosen.  Furthermore, the overlap is not seen in Esnembl or the RefSeq gene models
        # confirmed via IGV
        gene = 'SERPINB4'

    if TMP_GENE == gene:
        PRIMER_INDEX = PRIMER_INDEX + 1
    else:
        PRIMER_INDEX = 1
    TMP_GENE = gene

    return '_'.join([str(gene), 'chr' + str(row['Chr']), str(row['Start']), str(PRIMER_INDEX), '+', 'CNV'])


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Update the coverage file with gene level information"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--input_coverage_file', dest='input_coverage_file',
                        help="input coverage file from bedtools multicov", required=True)

    parser.add_argument('--gene_level_data', dest='gene_level_data',
                        help="The depth_of_coverage_df.txt from analyze_hsmetrcis_outputs.py", required=True)

    parser.add_argument('--input_bam_file_names', dest='input_bam_file_names',
                        help="input file with one bam per line used to create the input_coverage_file. ",
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()
