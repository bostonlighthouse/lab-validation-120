import argparse
import decimal
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from math import log2
import numpy as np

def main():

    # control overall size of axes labels
    plt.rcParams["axes.labelsize"] = 9
    plt.rcParams['figure.figsize'] = (10.0, 32.0)
    rc = {'font.size': 6, 'axes.labelsize': 10, 'legend.fontsize': 8,
          'axes.titlesize': 12, 'xtick.labelsize': 12, 'ytick.labelsize': 12,
          'axes.grid': True,
          'axes.edgecolor': 'dimgray',  # the border around the plot
          'grid.color': 'lightgray',
          'axes.facecolor': 'white'
    }
    sns.set(rc=rc)
    fig = plt.figure()
    gs = gridspec.GridSpec(4, 1)  # 3 rows, 2 columns
    # create the axes subplots
    ax0_0 = fig.add_subplot(gs[0])
    ax0_1 = fig.add_subplot(gs[1])
    ax0_2 = fig.add_subplot(gs[2])
    ax0_3 = fig.add_subplot(gs[3])

    plot_tumor_purity_plots(ax1=ax0_0, ax2=ax0_1, fig=fig)

    plot_log2_vs_copy_number_state(ax=ax0_2, copy_state_max=17, yaxis_set_major_locator=10)
    plot_log2_vs_copy_number_state(ax=ax0_3, plot_point_values=False, copy_state_max=101, yaxis_set_major_locator=25)

    fig.tight_layout()
    image_filename = "_".join((("test", ".svg")))
    plt.savefig(image_filename)
    plt.close(fig)  # have to close


def plot_tumor_purity_plots(ax1=None, ax2=None, plot_point_values=True, fig=None):
    # plot the first two plots
    copy_state_gains_list = []
    copy_state_losses_list = []
    for copy_state in range(0, 13):
        copy_state_label = f"copy_state = {copy_state}"
        purity_level_list, cnr_list = calculate_cnv(copy_state=copy_state)
        if copy_state == 2: # skip copy neutral
            continue
        # separating out copy state of 0 and 1
        if copy_state == 0 or copy_state == 1:
            copy_state_losses_list.append(copy_state)
            plot = sns.lineplot(x=purity_level_list, y=cnr_list, ax=ax2, label=copy_state_label, marker='o')
            if plot_point_values is True:
                add_point_values(list1=purity_level_list, list2=cnr_list, y_offset=.05, plot=plot)

        if copy_state >= 2:
            copy_state_gains_list.append(copy_state)
            plot = sns.lineplot(x=purity_level_list, y=cnr_list, ax=ax1, label=copy_state_label, marker='o')
            if plot_point_values is True:
                add_point_values(list1=purity_level_list, list2=cnr_list, y_offset=.05, plot=plot)

        plot.xaxis.set_major_locator(plt.MaxNLocator(20))

    for i, ax in enumerate((ax1, ax2)):
        if i == 0:
            ax.set_title("GAINS - Log2 Values for Copy Number States with Tumor Purity")
            add_label_to_end_line(ax=ax, line_name_list=copy_state_gains_list, fig=fig)
        else:
            ax.set_title("LOSSES - Log2 Values for Copy Number States with Tumor Purity")
            add_label_to_end_line(ax=ax, line_name_list=copy_state_losses_list, fig=fig)
        ax.set_xlabel("Tumor Purity")
        ax.set_ylabel("Log2 Ratio")


def add_label_to_end_line(ax=None, line_name_list=None, fig=None):
    # Add the text--for each line, find the end, annotate it with a label, and
    # adjust the chart axes so that everything fits on.
    for line, name in zip(ax.lines, line_name_list):
        y = line.get_ydata()[-1]
        x = line.get_xdata()[-1]
        if not np.isfinite(y):
            y = next(reversed(line.get_ydata()[~line.get_ydata().mask]), float("nan"))
        if not np.isfinite(y) or not np.isfinite(x):
            continue
        text = ax.annotate(name,
                           xy=(x + .025, y),
                           xytext=(0, 0),
                           color=line.get_color(),
                           xycoords=(ax.get_xaxis_transform(),
                                     ax.get_yaxis_transform()),
                           textcoords="offset points",
                           size=10)
        text_width = (text.get_window_extent(
            fig.canvas.get_renderer()).transformed(ax.transData.inverted()).width)
        if np.isfinite(text_width):
            ax.set_xlim(ax.get_xlim()[0], text.xy[0] + text_width * 1.05)

def plot_log2_vs_copy_number_state(ax=None, copy_state_max=51, plot_point_values=True, yaxis_set_major_locator=None):
    # plot the normal plot with total copies on the x-axis and y-axis log2 ratio
    copy_state_list = []
    cnr_list = []
    for copy_state in range(0, copy_state_max):
        copy_state_list.append(copy_state)
        if copy_state == 0:
            cnr_list.append(-5.0)
        else:
            cnr_list.append(log2(copy_state/2))
    print(cnr_list)
    plot = sns.lineplot(x=copy_state_list, y=cnr_list, ax=ax, label=f"log2 Ratio vs Copy State", marker='o')
    if plot_point_values is True:
        add_point_values(list1=copy_state_list, list2=cnr_list, y_offset=.17, plot=plot, font_size=10)

    ax.set_xlabel("Copy State")
    ax.set_ylabel("Log2 Ratio")
    ax.set_title("Log2 Values for Copy Number States in 100% Tumor Content (Assuming Normal Diploid Genome)")
    ax.legend(loc='upper left')
    ax.set_ylim([-5.5, None])
    plot.yaxis.set_major_locator(plt.MaxNLocator(yaxis_set_major_locator))


def add_point_values(list1=None, list2=None, x_offset=None, y_offset=None, plot=None, font_size=6):
    # label points on the plot
    for x, y in zip(list1, list2):
        # the position of the data label relative to the data point can be adjusted by adding/subtracting a value from the x &/ y coordinates
        plot.text(x=x,  # x-coordinate position of data label
                  y=y + y_offset,  # y-coordinate position of data label, adjusted to be 150 below the data point
                  s='{:.3f}'.format(y),  # data label, formatted to ignore decimals'
                  color='purple', size=font_size)  # set colour of line


def calculate_cnv(copy_state=None):

    purity_level_list = [i / 10 / 2 for i in range(2, 21)]
    cnr_list = []
    print(purity_level_list)
    for purity_level in purity_level_list:
        tumor_alleles = round(100 * purity_level, 2)  # number of tumor alleles
        normal_alleles = round(100 * (1 - purity_level), 2) # number of normal alleles
        tumor_allele_gain = (tumor_alleles / 2) * copy_state  # how many tumor alleles gained given the copy_sate
        if copy_state == 0:
            numerator = normal_alleles + 5
        else:
            numerator = normal_alleles + tumor_allele_gain  # what's the numerator in the log2 ratio
        print(f"tumor_purity={purity_level}, normal_alleles={normal_alleles}, tumor_alleles{tumor_alleles}, tumor_allele_gain={tumor_allele_gain}")
        print(f"log2({numerator}/100)")

        if purity_level == 1.0 and copy_state == 0:  # lost both alleles here have been lost
            cnr = -5.0
        else:
            cnr = round(log2(numerator/100), 3)

        print(cnr)
        cnr_list.append(cnr)
    return purity_level_list, cnr_list


if __name__ == '__main__':
    main()