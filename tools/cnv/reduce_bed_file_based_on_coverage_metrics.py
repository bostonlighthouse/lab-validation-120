"""
Module to take the coverage output from bedtools multicov and reduce the number of intervals

GS395 has 17K+ intervals which are too many to process with the CNV script cnv_create_pon_coefs.R
and too many to show in VV

The idea here is to take the input coverage file, remove intervals that had a mean lower than 100MCC
and then reduce down the number of intervals for a gene

"""
import argparse
import os
from matplotlib import pyplot as plt
import statistics
from typing import Optional

from lab_validation_120.utils.assay_utils import get_assay_names
from lab_validation_120.utils.plots.distribution_plot_utilities import plot_distribution_of_data
from lab_validation_120.utils.plots.qq_plot_utilities import plot_qq_plot
from tools.cnv.utils import get_list_important_cnv_genes


def main():
    """Main driver for the program"""
    args = get_cli_args()
    lower_mean_mcc_threshold = args.lower_mean_mcc_threshold
    upper_mean_mcc_threshold = args.upper_mean_mcc_threshold
    print(f"lower mcc threshold {lower_mean_mcc_threshold} upper mcc threshold {upper_mean_mcc_threshold}")
    input_bam_file_names = args.input_bam_file_names
    list_bam_files = get_list_bam_files(input_bam_file_names=input_bam_file_names)
    assay_name = args.assay_name
    # get all data, and the total number of original intervals (total_gene_intervals) and the number of removed gene
    # intervals total_removed_gene_intervals
    data_from_infile = \
        process_coverage_file_by_mcc_thresholds(input_coverage_file=args.input_coverage_file,
                                                lower_mean_mcc_threshold=lower_mean_mcc_threshold,
                                                upper_mean_mcc_threshold=upper_mean_mcc_threshold)

    # Reduce the number of intervals based on a scale
    data_from_infile_reduced = reduce_number_of_gsp_primer_binding_site(data_from_infile=data_from_infile,
                                                                        assay_name=assay_name)

    # if there was not 3 primers then it can't be used as CNV
    # total_num_gsp2_primers has the final number of primers that passed thresholds and filtering
    data_from_infile_reduced = {k: v for k, v in data_from_infile_reduced.items()
                                if v['total_num_gsp2_primers'] > 2}
    print(f"final number of genes in the data structure {len(data_from_infile_reduced)}")

    base_file_name = f"mean_mcc_lower_{lower_mean_mcc_threshold}_upper_mean_mcc_{upper_mean_mcc_threshold}_{assay_name}"

    # print out the new coverage file and BED file to be used with cnv_create_pon_coefs.R
    print_out_new_coverage_file(data_from_infile_reduced=data_from_infile_reduced,
                                output_coverage_file_name=args.output_coverage_file_name,
                                output_bed_file_name=args.output_bed_file_name,
                                base_file=base_file_name,
                                list_bam_files=list_bam_files)
    # plot the final figures
    process_plots(data_from_infile=data_from_infile, data_from_infile_reduced=data_from_infile_reduced,
                  base_file_name=base_file_name,
                  lower_mean_mcc_threshold=lower_mean_mcc_threshold,
                  upper_mean_mcc_threshold=upper_mean_mcc_threshold)

def get_list_bam_files(input_bam_file_names: str) -> list:
    """
    Open the file, and get one bam file per line
    @param input_bam_file_names:  The name of the file to open
    """
    list_files = []
    with open(input_bam_file_names) as infh:
        for line in infh:
            list_files.append(line.rstrip())
    return list_files

def process_plots(data_from_infile: dict, data_from_infile_reduced: dict,
                  base_file_name: str,
                  lower_mean_mcc_threshold: int,
                  upper_mean_mcc_threshold: int) -> None:
    """
    Function used to print the four subplots in one plot
    @param data_from_infile: Dictionary of all intervals that passed the MCC thresholds
    @param data_from_infile_reduced: Dictionary of all intervals that passed the MCC thresholds and final filtering
    @param base_file_name: Base file name to use with the final image
    @param lower_mean_mcc_threshold: What's the lower mean threshold were values dropped from
    @param upper_mean_mcc_threshold: What's the upper mean threshold were values dropped from
    """

    plt.rcParams["figure.figsize"] = [21, 14]
    fig, ax = plt.subplots(2, 3)  # rows x cols = Create 4 subplots
    _, _ = plot_distribution_of_all_coverage_values(ax=ax[0][0], data=data_from_infile,
                                                    lower_mean_mcc_threshold=lower_mean_mcc_threshold,
                                                    upper_mean_mcc_threshold=upper_mean_mcc_threshold)

    list_coverage_data, list_normal_data = plot_distribution_of_mean_coverage_values(ax=ax[0][1], data=data_from_infile,
                                              lower_mean_mcc_threshold=lower_mean_mcc_threshold,
                                              upper_mean_mcc_threshold=upper_mean_mcc_threshold,
                                              plot_normal_distribution=True)

    plot_qq_plot(ax=ax[0][2], sample1=list_coverage_data, sample2=list_normal_data,
                 x_axis_label="All values", y_axis_label="Normal",
                 sample1_distribution_str="All Values",
                 sample2_distribution_str="Normal Values")

    _, _ = plot_distribution_of_all_coverage_values(ax=ax[1][0], data=data_from_infile_reduced)

    list_coverage_data, list_normal_data = (
        plot_distribution_of_mean_coverage_values(ax=ax[1][1], data=data_from_infile_reduced,
                                                  plot_normal_distribution=True))

    plot_qq_plot(ax=ax[1][2], sample1=list_coverage_data, sample2=list_normal_data,
                 x_axis_label="Filtered values", y_axis_label="Normal",
                 sample1_distribution_str="Filtered Values",
                 sample2_distribution_str="Normal Values")

    # name the output file
    base_image_file_name = f"distributions_upper_{base_file_name}"
    plt.tight_layout()
    plt.savefig(f"{base_image_file_name}.svg")
    plt.close(fig)  # have to close


def plot_distribution_of_mean_coverage_values(ax: plt.Axes, data: dict,
                                              lower_mean_mcc_threshold: Optional[int] = None,
                                              upper_mean_mcc_threshold: Optional[int] = None,
                                              plot_normal_distribution: Optional[bool] = False) -> (list, list):
    """
    Plot all coverage values
    @param ax: Subplot to plot to
    @param data: The data structure to get the data from
    @param lower_mean_mcc_threshold: What's the lower mean threshold were values dropped from
    @param upper_mean_mcc_threshold: What's the upper mean threshold were values dropped from
    @param plot_normal_distribution: Plot a normal distribution on the historgram
    """
    list_coverage_data = []  # all values for each interval X number samples
    # create on large list
    for _, dict_data in data.items():
        for element in dict_data['data']:  # returns a list of dictionaries
            list_coverage_data.append(element['coverage_values_mean'])
    # some some stats from the coverage values
    pop_mean = statistics.mean(list_coverage_data)
    pop_std = statistics.stdev(list_coverage_data)

    normal_values = plot_distribution_of_data(ax=ax,
                                              values=list_coverage_data,
                                              plot_normal_distribution=plot_normal_distribution,
                                              x_axis_label='Mean Coverage of each GSP2 interval',
                                              value_plotted="bedtools multicov mean value for each interval")

    title = f'Distribution of {len(list_coverage_data)} mean values per interval w/ thresholds ' \
            f'(mu {pop_mean:.1f} std. dev. {pop_std:.1f})'
    # only used when this is called with thresholds
    if lower_mean_mcc_threshold is not None:
        title = f'{title}\n'\
                f'Lower Mean MCC {lower_mean_mcc_threshold} '\
                f'Upper Mean MCC {upper_mean_mcc_threshold}'

    ax.set_title(title, fontsize=9)
    if plot_normal_distribution is True:
        ax.legend(loc='upper right', frameon=True)

    return list_coverage_data, normal_values


def plot_distribution_of_all_coverage_values(ax: plt.Axes, data: dict,
                                             lower_mean_mcc_threshold: Optional[int] = None,
                                             upper_mean_mcc_threshold: Optional[int] = None,
                                             plot_normal_distribution: Optional[bool] = False) -> (list, list):
    """
    Plot all coverage values
    @param ax: Subplot to plot to
    @param data: The data structure to get the data from
    @param lower_mean_mcc_threshold: What lower mean threshold were values dropped from
    @param upper_mean_mcc_threshold: What's the upper mean threshold were values dropped from
    @param plot_normal_distribution: Plot a normal distribution on the histogram
    """
    list_coverage_data = []  # all values for each interval X number samples
    num_total_intervals = 0  # how many total intervals
    # create one large list of all coverage values
    for gene_name, dict_data in data.items():
        for i in [x['coverage_values'] for x in dict_data['data']]:
            num_total_intervals += 1
            for ii in i:
                list_coverage_data.append(ii)
    # some some stats from the coverage values
    pop_mean = statistics.mean(list_coverage_data)
    pop_std = statistics.stdev(list_coverage_data)
    # call to plotting utility
    normal_values = plot_distribution_of_data(ax=ax,
                                              values=list_coverage_data,
                                              plot_normal_distribution=plot_normal_distribution,
                                              x_axis_label='Coverage of each GSP2 primer',
                                              value_plotted="bedtools multicov values")
    # set the title
    title = f'Distribution of {len(list_coverage_data)} coverage values in all interval w/ thresholds ' \
            f'(mu {pop_mean:.1f} std. dev. {pop_std:.1f})'
    # only used when this is called with thresholds
    if lower_mean_mcc_threshold is not None:
        title = f'{title}\n'\
                f'Lower Mean MCC {lower_mean_mcc_threshold} '\
                f'Upper Mean MCC {upper_mean_mcc_threshold}'

    ax.set_title(title, fontsize=9)
    if plot_normal_distribution is True:
        ax.legend(loc='upper right', frameon=True)

    return list_coverage_data, normal_values


def print_out_new_coverage_file(data_from_infile_reduced: dict, output_coverage_file_name: str,
                                output_bed_file_name: str, base_file: str, list_bam_files: list) -> None:
    """
    Simple function to print out the updated coverage file and  the BED file, based on the coverage file
    @param data_from_infile_reduced: Dictionary of all intervals that passed the MCC thresholds and final filtering
    @param output_bed_file_name: Bed file name to be written too
    @param output_coverage_file_name: Updated coverage file name to be written too
    @param base_file: This is the base file that will be prepended to the names of the bed and cov files
    @param list_bam_files: List of bam files used in the analysis
    """
    output_coverage_file_name, cov_ext = os.path.splitext(output_coverage_file_name)
    output_bed_file_name, bed_ext = os.path.splitext(output_bed_file_name)
    # Add in the base_file name
    output_coverage_file_name = f"{output_coverage_file_name}_{base_file}{cov_ext}"
    output_bed_file_name = f"{output_bed_file_name}_{base_file}{bed_ext}"

    with open(output_coverage_file_name, 'w') as out_cov_fh, open(output_bed_file_name, 'w') as out_bed_fh:
        # coverage file needs a header line
        header = ['Chr', 'Start', 'End', 'Primer'] + list_bam_files
        print("\t".join(header), file=out_cov_fh)
        for gene, dict_data in data_from_infile_reduced.items():
            for element in dict_data['data']:
                print(f"{element['bed_line']}", file=out_bed_fh)
                line_coverage_data = element['entire_line']
                size_coverage_data = len(line_coverage_data.split('\t'))
                # - 4 b/c we have chr, start, end, and primer, so subtract and check to make sure these are the same
                size_to_check = size_coverage_data - 4
                size_bam_list = len(list_bam_files)
                assert size_to_check == size_bam_list, f"Sizes are not the same: {size_to_check} {size_bam_list}"
                print(f"{line_coverage_data}", file=out_cov_fh)




def reduce_number_of_gsp_primer_binding_site(data_from_infile: dict, assay_name: str) -> dict:
    """
    Function to go over the current dictionary and create a new reduced data structure
    @param data_from_infile: Dictionary of all intervals that passed the MCC thresholds
    @param assay_name: Assay name passed in

    {'ABL1': {'data': [{'annotation': 'ABL1_chr9_133589673_24_+',
                    'bed_line': '9\t133589698\t133589707\tABL1_chr9_133589673_24_+',
                    'chromosome': '9',
                    'coverage_values': [343,
                                        272,
                                        ..
                                        ..
                                        ..
                                        ..
                     ]
                    'coverage_values_mean': 237.41379310344828,,
                    'end': '133589707',
                     'entire_line': '9\t133589698\t133589707\t'
                                   'ABL1_chr9_133589673_24_+\t343\t272\t91\t'
                                   '275\t272\t200\t228\t236\t113\t268\t290\t'
                                   '142\t249\t184\t208\t292\t222\t156\t284\t'
                                   '232\t378\t182\t282\t306\t320\t242\t178\t'
                                   '246\t194',
                    'start': '133589698'},
                   {'annotation': 'ABL1_chr9_133589865_30_-',
                    'bed_line': '9\t133589856\t133589865\t'
                                'ABL1_chr9_133589865_30_-',
                    'chromosome': '9',
                    'coverage_values': [343,
                                        272,
                                        ..
                                        ..
                                        ..
                                        ..
                     ]
                    'coverage_values_mean': 406.86206896551727,,
                    'end': '133589865',
                    'entire_line': '9\t133589856\t133589865\t'
                                   'ABL1_chr9_133589865_30_-\t494\t457\t259\t'
                                   '464\t456\t427\t391\t403\t349\t435\t449\t'
                                   '304\t389\t349\t350\t472\t398\t305\t434\t'
                                   '374\t531\t386\t454\t442\t500\t395\t337\t'

                    'start': '133589856'},
                    ..
                    ..
                    ..]
          'total_num_gsp2_primers': 30,
          'total_num_intervals_per_gene': 39,
          'total_num_threshold_removed_intervals_per_gene': 9},


    """
    assumed_total_primers = 0
    actual_total_primers = 0
    values_to_return = {}
    for gene_name, dict_data in data_from_infile.items():
        num_gsp2_primers = data_from_infile[gene_name]['total_num_gsp2_primers']
        # how many primers should we attempt to reduce down to?
        num_reduced_gsp2_primers = num_primers_to_reduce_down_to(num_gsp2_primers, assay_name=assay_name)
        # get the nth_element number
        every_nth_element = int(round((num_gsp2_primers/num_reduced_gsp2_primers), 0))

        # get very nth element in the list, i.e. use the nth_element number
        if gene_name in get_list_important_cnv_genes():  # genes in this list, we'll just keep all intervals
            data_to_keep = dict_data['data'][:]
        else:
            data_to_keep = dict_data['data'][::every_nth_element]

        # store this total to compare
        assumed_total_primers += num_reduced_gsp2_primers
        # store the actual reduction to compare
        actual_total_primers += len(data_to_keep)

        # update with new data
        values_to_return[gene_name] = {}  # create a new entry
        values_to_return[gene_name]['data'] = data_to_keep  # store the data
        values_to_return[gene_name]['total_num_gsp2_primers'] = len(data_to_keep)  # update this new number
        values_to_return[gene_name]['total_num_intervals_per_gene'] = \
            data_from_infile[gene_name]['total_num_intervals_per_gene']
        values_to_return[gene_name]['total_num_threshold_removed_intervals_per_gene'] = \
            data_from_infile[gene_name]['total_num_threshold_removed_intervals_per_gene']
        values_to_return[gene_name]['total_num_filter_removed_intervals_per_gene'] = \
            len(dict_data['data']) - len(data_to_keep)

        # output some lines to stdout
        print(f"gene {gene_name}")
        print(f"original number of intervals {data_from_infile[gene_name]['total_num_intervals_per_gene']} "
              f"after threshold removal "
              f"{data_from_infile[gene_name]['total_num_intervals_per_gene'] - data_from_infile[gene_name]['total_num_threshold_removed_intervals_per_gene']}")
        print(f"assumed number_gsp2_primers {num_gsp2_primers} reduce down to "
              f"{num_reduced_gsp2_primers} by nth {every_nth_element}")
        print(f"attempted to reduce down to {num_reduced_gsp2_primers}, actual size was {len(data_to_keep)}\n\n")

    # output some final numbers to stdout
    print(f"final number of assumed primers {assumed_total_primers}")
    print(f"final number of actual primers {actual_total_primers}")
    return values_to_return

def num_primers_to_reduce_down_to(num_primers: int, assay_name: str) -> int:
    """
    @param num_primers:  The number of primers per gene
    @param assay_name: Assay name passed in
    # here's what we used for oc395
    12	number primers
    25	12
    50	14
    75	16
    100	18
    125	20
    150	22
    175	24
    200	26
    225	28
    250	30

    The above was updated for Agilent to reduce the number of primers

    12	number primers
    25	10
    50	10
    75	12
    100	14
    125	16
    150	18
    175	20
    200	22
    225	24
    250	36

    """

    if num_primers <= 12 or assay_name == 'gs180_snp':  # just keep the original number
        return num_primers
    elif 12 < num_primers <= 25:
        return 10
    elif 25 < num_primers <= 50:
        return 10
    elif 50 < num_primers <= 75:
        return 12
    elif 75 < num_primers <= 100:
        return 14
    elif 100 < num_primers <= 125:
        return 16
    elif 125 < num_primers <= 150:
        return 18
    elif 150 < num_primers <= 175:
        return 20
    elif 175 < num_primers <= 200:
        return 22
    elif 200 < num_primers <= 225:
        return 24
    elif 225 < num_primers <= 250:
        return 26
    else:
        raise ValueError(f"did not implement value num_primers: {num_primers}")


def process_coverage_file_by_mcc_thresholds(input_coverage_file: str,
                                            lower_mean_mcc_threshold: int,
                                            upper_mean_mcc_threshold: int) -> dict:
    """
    Go over the input coverage file and return the data structure for processing.  Removing intervals that do not meet
    the required MCC thresholds passed in.
    @param input_coverage_file: File to open
    @param lower_mean_mcc_threshold: What's the lower mean threshold should we drop?
    @param upper_mean_mcc_threshold: What's the upper mean threshold should we drop?
    """
    data_dict_to_return = {}  # this is the final DS to return
    total_primers_found = 0  # how many total GSP2 primer were found, regardless of filtering (does not include None_)
    num_less_lower_mean_mcc_threshold = 0
    num_greater_upper_mean_mcc_threshold = 0
    total_num_intervals_per_gene = {}
    total_num_threshold_removed_intervals_per_gene = {}
    with open(input_coverage_file) as infh:
        for line in infh:
            line = line.rstrip()
            if 'None_' in line:  # skip these regions b/c they were SNP regions, and we do not need them in CNV
                continue
            elif 'nan_' in line: # skip these b/c they had no gene content from Agilent and coverage was low
                continue
            # call function to get the dictionary for an individual interval
            gene_name, dict_to_store = get_ds_to_store(line=line)
            # just keep this for later
            total_num_intervals_per_gene[gene_name] = total_num_intervals_per_gene.get(gene_name, 0) + 1
            if gene_name in get_list_important_cnv_genes():  # genes in this list, we'll just keep all intervals
                pass
            elif dict_to_store['coverage_values_mean'] < lower_mean_mcc_threshold:  # if MCC was < than thres. skip
                num_less_lower_mean_mcc_threshold += 1
                total_num_threshold_removed_intervals_per_gene[gene_name] = \
                    total_num_threshold_removed_intervals_per_gene.get(gene_name, 0) + 1
                continue
            elif dict_to_store['coverage_values_mean'] > upper_mean_mcc_threshold:  # elif MCC was > than thres. skip
                num_greater_upper_mean_mcc_threshold += 1
                total_num_threshold_removed_intervals_per_gene[gene_name] = \
                    total_num_threshold_removed_intervals_per_gene.get(gene_name, 0) + 1
                continue
            update_dictionary(dict_to_update=data_dict_to_return, dict_to_store=dict_to_store, gene_name=gene_name)
            total_primers_found += 1
    # print some preliminary data out to stdout
    print(f"Found a total of {total_primers_found} gsp2 primers")
    print(f"Found {num_less_lower_mean_mcc_threshold} gsp2 primers that had less than {lower_mean_mcc_threshold} "
          f"mean MCC")
    print(f"Found {num_greater_upper_mean_mcc_threshold} gsp2 primers that had greater than {upper_mean_mcc_threshold} "
          f"mean MCC")

    for key in data_dict_to_return.keys():
        data_dict_to_return[key]['total_num_intervals_per_gene'] = total_num_intervals_per_gene[key]
        data_dict_to_return[key]['total_num_threshold_removed_intervals_per_gene'] = \
            total_num_threshold_removed_intervals_per_gene.get(key, 0)  # this could be empty
    return data_dict_to_return


def update_dictionary(dict_to_update: dict, dict_to_store: dict,  gene_name: str):
    """
    Update the dictionary passed in with the gene name and dict_to_store
    @param dict_to_update: The actual dictionary that needs to be updated
    @param dict_to_store: What dictionary should be stored
    @param gene_name: The key of the dictionary in dict_to_update
    """
    if gene_name in dict_to_update:  # update the gene dictionary's list and number_gsp2_primers
        dict_to_update[gene_name]['data'].append(dict_to_store)
        dict_to_update[gene_name]['total_num_gsp2_primers'] += 1
    else:  # new gene dictionary
        dict_to_update[gene_name] = {}
        dict_to_update[gene_name]['data'] = []
        dict_to_update[gene_name]['total_num_gsp2_primers'] = 1
        dict_to_update[gene_name]['data'].append(dict_to_store)

def get_ds_to_store(line: str) -> [str, dict]:
    """
    Simple function to return the data structure to be store from the line passed in
    @param line: The line from the input file
    """
    values = line.split("\t")
    entire_line = line   # store this for later if we are writing this coverage file out
    bed_line = "\t".join(values[0:4])  # store this out for the BED file if we write this entry out
    chromosome = values.pop(0)
    start = values.pop(0)
    end = values.pop(0)
    annotation = values.pop(0)
    gene_name = annotation.split('_')[0]  # get the gene name
    coverage_values = [int(x) for x in values]  # covert the str to ints
    mean = statistics.mean(coverage_values)  # find the mean of all the coverage values
    dict_to_store = {
        'bed_line': bed_line,
        'entire_line': entire_line,
        'chromosome': chromosome,
        'start': start,
        'end': end,
        'annotation': annotation,
        'coverage_values': coverage_values,
        'coverage_values_mean': mean
    }
    return gene_name, dict_to_store

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Module to take the coverage output from bedtools multicov and reduce the number of intervals"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--input_coverage_file', dest='input_coverage_file',
                        help="input coverage file from bedtools multicov", required=True)
    parser.add_argument('--input_bam_file_names', dest='input_bam_file_names',
                        help="input file with one bam per line used to create the input_coverage_file. "
                             "List of names must be the same size used by bedtools multicov", required=True)
    parser.add_argument('--output_coverage_file_name', dest='output_coverage_file_name',
                        help="reduced coverage file calculated from --input_coverage_file", required=True)
    parser.add_argument('--output_bed_file_name', dest='output_bed_file_name',
                        help="reduced coverage BED file from newly calculated coverage file.  This will be needed by "
                             "cnv_create_pon_coefs.R", required=True)
    parser.add_argument('--lower_mean_mcc_threshold', dest='lower_mean_mcc_threshold', required=True,
                        help="What's lower mean mcc threshold should be used to remove gsp2 intervals",
                        type=int)
    parser.add_argument('--upper_mean_mcc_threshold', dest='upper_mean_mcc_threshold', required=True,
                        help="What's upper mean mcc threshold should be used to remove gsp2 intervals",
                        type=int)
    parser.add_argument('--assay_name', dest='assay_name', required=True,
                        help="Assay name.  Used for final naming of files",
                        type=str, choices=get_assay_names())

    return parser.parse_args()


if __name__ == "__main__":
    main()


