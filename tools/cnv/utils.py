import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator

from lab_validation_120.utils.plots.plot_utilities import get_subplot_bounding_box_parameters

def get_list_important_cnv_genes_with_gc_content():
    # this came from running the too:
    # caffeinate pipenv run python -m tools.genes.gene_exon_to_gc >&gc_output_dict
    # and then taking the output of the dictionary and having chatGPT pretty print it
    gene_gc_dict = {
        'APC': {'gene_gc_content': '52.50%', 'exon_gc_content': '37.00%'},
        'AR': {'gene_gc_content': '39.19%', 'exon_gc_content': '39.68%'},
        'ATM': {'gene_gc_content': '62.48%', 'exon_gc_content': '37.42%'},
        'ATR': {'gene_gc_content': '56.79%', 'exon_gc_content': '37.12%'},
        'ATRX': {'gene_gc_content': '48.21%', 'exon_gc_content': '37.72%'},
        'BAP1': {'gene_gc_content': '58.16%', 'exon_gc_content': '55.75%'},
        'BARD1': {'gene_gc_content': '55.75%', 'exon_gc_content': '36.37%'},
        'BRCA1': {'gene_gc_content': '44.35%', 'exon_gc_content': '42.92%'},
        'BRCA2': {'gene_gc_content': '53.20%', 'exon_gc_content': '38.10%'},
        'BRIP1': {'gene_gc_content': '52.88%', 'exon_gc_content': '35.96%'},
        'CDK12': {'gene_gc_content': '36.29%', 'exon_gc_content': '43.30%'},
        'CD274': {'gene_gc_content': '40.25%', 'exon_gc_content': '38.07%'},
        'CDK4': {'gene_gc_content': '47.62%', 'exon_gc_content': '49.98%'},
        'CDK6': {'gene_gc_content': '60.17%', 'exon_gc_content': '38.19%'},
        'CCND1': {'gene_gc_content': '46.94%', 'exon_gc_content': '58.63%'},
        'CCND2': {'gene_gc_content': None, 'exon_gc_content': '49.13%'},
        'CCNE1': {'gene_gc_content': '50.05%', 'exon_gc_content': '47.23%'},
        'CDH1': {'gene_gc_content': None, 'exon_gc_content': '46.88%'},
        'CDKN2A': {'gene_gc_content': '41.79%', 'exon_gc_content': '46.00%'},
        'CDKN2B': {'gene_gc_content': '57.15%', 'exon_gc_content': '41.04%'},
        'CHEK1': {'gene_gc_content': '50.74%', 'exon_gc_content': '38.06%'},
        'CHEK2': {'gene_gc_content': '52.81%', 'exon_gc_content': '43.67%'},
        'EGFR': {'gene_gc_content': '51.35%', 'exon_gc_content': '44.57%'},
        'ERBB2': {'gene_gc_content': '61.47%', 'exon_gc_content': '53.86%'},
        'ESR1': {'gene_gc_content': '58.48%', 'exon_gc_content': '39.22%'},
        'FANCA': {'gene_gc_content': '56.73%', 'exon_gc_content': '50.45%'},
        'FANCC': {'gene_gc_content': '59.76%', 'exon_gc_content': '41.38%'},
        'FANCL': {'gene_gc_content': '29.82%', 'exon_gc_content': '34.75%'},
        'FGF19': {'gene_gc_content': '54.60%', 'exon_gc_content': '53.54%'},
        'FGFR1': {'gene_gc_content': '53.31%', 'exon_gc_content': '50.47%'},
        'FGFR2': {'gene_gc_content': '50.04%', 'exon_gc_content': '45.41%'},
        'FGFR3': {'gene_gc_content': '53.80%', 'exon_gc_content': '65.83%'},
        'FH': {'gene_gc_content': '39.42%', 'exon_gc_content': '36.86%'},
        'FLCN': {'gene_gc_content': '57.06%', 'exon_gc_content': '53.28%'},
        'KEAP1': {'gene_gc_content': '33.96%', 'exon_gc_content': '53.85%'},
        'KRAS': {'gene_gc_content': '45.93%', 'exon_gc_content': '36.42%'},
        'MDM2': {'gene_gc_content': '53.56%', 'exon_gc_content': '39.04%'},
        'MET': {'gene_gc_content': '51.21%', 'exon_gc_content': '39.10%'},
        'MLH1': {'gene_gc_content': '34.51%', 'exon_gc_content': '41.87%'},
        'MRE11': {'gene_gc_content': None, 'exon_gc_content': '37.30%'},
        'MSH2': {'gene_gc_content': '55.29%', 'exon_gc_content': '41.59%'},
        'MSH6': {'gene_gc_content': '33.70%', 'exon_gc_content': '43.75%'},
        'MTAP': {'gene_gc_content': '33.27%', 'exon_gc_content': '41.55%'},
        'MUTYH': {'gene_gc_content': '48.05%', 'exon_gc_content': '50.36%'},
        'MYC': {'gene_gc_content': '48.35%', 'exon_gc_content': '50.75%'},
        'MYCN': {'gene_gc_content': '52.85%', 'exon_gc_content': '53.77%'},
        'NBN': {'gene_gc_content': '52.24%', 'exon_gc_content': '36.21%'},
        'NF1': {'gene_gc_content': '33.65%', 'exon_gc_content': '37.54%'},
        'NOTCH1': {'gene_gc_content': '57.07%', 'exon_gc_content': '63.46%'},
        'NOTCH2': {'gene_gc_content': '50.99%', 'exon_gc_content': '39.74%'},
        'PALB2': {'gene_gc_content': '59.01%', 'exon_gc_content': '43.54%'},
        'PDGFRA': {'gene_gc_content': '55.94%', 'exon_gc_content': '43.66%'},
        'PDGFRB': {'gene_gc_content': '51.18%', 'exon_gc_content': '54.34%'},
        'PIK3CA': {'gene_gc_content': None, 'exon_gc_content': '35.72%'},
        'PMS2': {'gene_gc_content': '28.38%', 'exon_gc_content': '45.25%'},
        'POLE': {'gene_gc_content': '58.35%', 'exon_gc_content': '50.84%'},
        'PTEN': {'gene_gc_content': '40.43%', 'exon_gc_content': '35.61%'},
        'RAD51B': {'gene_gc_content': '46.67%', 'exon_gc_content': '38.93%'},
        'RAD51C': {'gene_gc_content': '48.99%', 'exon_gc_content': '41.68%'},
        'RAD51D': {'gene_gc_content': '42.89%', 'exon_gc_content': '46.88%'},
        'RAD54L': {'gene_gc_content': '56.21%', 'exon_gc_content': '46.45%'},
        'RB1': {'gene_gc_content': '55.40%', 'exon_gc_content': '36.91%'},
        'RET': {'gene_gc_content': '47.01%', 'exon_gc_content': '55.71%'},
        'SDHA': {'gene_gc_content': None, 'exon_gc_content': '45.95%'},
        'SDHAF2': {'gene_gc_content': '44.15%', 'exon_gc_content': '45.12%'},
        'SDHB': {'gene_gc_content': None, 'exon_gc_content': '43.42%'},
        'SDHC': {'gene_gc_content': None, 'exon_gc_content': '42.31%'},
        'SDHD': {'gene_gc_content': None, 'exon_gc_content': '42.17%'},
        'SMARCA4': {'gene_gc_content': '60.26%', 'exon_gc_content': '52.74%'},
        'SMARCB1': {'gene_gc_content': '59.65%', 'exon_gc_content': '51.41%'},
        'STK11': {'gene_gc_content': '53.02%', 'exon_gc_content': '62.19%'},
        'TP53': {'gene_gc_content': '45.78%', 'exon_gc_content': '49.38%'},
        'TSC1': {'gene_gc_content': '35.69%', 'exon_gc_content': '42.77%'},
        'TSC2': {'gene_gc_content': '61.83%', 'exon_gc_content': '59.92%'},
        'VHL': {'gene_gc_content': '41.00%', 'exon_gc_content': '47.17%'}
    }
    # Convert exon_gc_content to float by removing '%' sign
    for gene, gc_content in gene_gc_dict.items():
        exon_gc_str = gc_content['exon_gc_content']
        if exon_gc_str is not None:
            exon_gc_float = float(exon_gc_str.strip('%'))
            gc_content['exon_gc_content'] = exon_gc_float

    return gene_gc_dict

def get_list_important_cnv_genes() -> list:
    """
    Simple list of important CNV genes from Rodrigo and Fernanda
    """

    return [
        'APC',
        'AR',
        'ATM',
        'ATR',
        'ATRX',
        'BAP1',
        'BARD1',
        'BRCA1',
        'BRCA2',
        'BRIP1',
        'CDK12',
        'CD274',  # Added
        'CDK4',  # Added
        'CDK6',
        'CCND1',
        'CCND2',  # Agilent list
        'CCNE1',  # Added
        'CDH1',
        'CDKN2A',
        'CDKN2B',  # Added
        'CHEK1',
        'CHEK2',
        'EGFR',
        'ERBB2',
        'ESR1',
        'FANCA',
        'FANCC',
        'FANCL',
        'FGF19',
        'FGFR1',
        'FGFR2',
        'FGFR3',
        'FH',
        'FLCN',
        'KEAP1',
        'KRAS',
        'MDM2',  # Added
        'MET',
        'MLH1',
        'MRE11A',  # this new gene name is MRE11
        'MSH2',
        'MSH6',
        'MTAP',
        'MUTYH',
        'MYC',
        'MYCN',
        'NBN',
        'NF1',
        'NOTCH1',
        'NOTCH2',
        'PALB2',
        'PDGFRA',
        'PDGFRB',
        'PIK3CA',
        'PMS2',
        'POLE',
        'PTEN',
        'RAD51B',
        'RAD51C',
        'RAD51D',
        'RAD54L',
        'RB1',
        'RET',
        'SDHA',
        'SDHAF2',
        'SDHB',
        'SDHC',
        'SDHD',
        'SMARCA4',
        'SMARCB1',
        'STK11',
        'TP53',
        'TSC1',
        'TSC2',
        'VHL'
    ]

def get_list_important_cnv_genes_with_oncokb() -> dict:
    """
    Simple list of important CNV genes from Rodrigo and Fernanda
    """

    return {
        "APC": "del",
        "ATM": "del",
        "BARD1": "del",
        "BRCA1": "del",
        "BRCA2": "del",
        "BRIP1": "del",
        "CDK12": "amp",
        "CDK4": "amp",
        "CD274": "amp",
        "CCNE1": "amp",
        "CDKN2A": "del",
        "EGFR": "amp",
        "ERBB2": "amp",
        "FANCA": "del",
        "FGFR1": "amp",
        "FGFR2": "amp",
        "MDM2": "amp",
        "MET": "amp",
        "MLH1": "del",
        "MRE11A": "del",
        "MTAP": "del",
        "NF1": "del",
        "NF2": "del",
        "NOTCH1": "del",
        "PIK3CA": "amp",
        "PTEN": "del",
        "RAD51B": "del",
        "TSC2": "del",
        "TP53": "del",
    }


def get_heatmap_cmap():
    # Define the custom colormap
    colors = [(0, 0, 1), (1, 1, 1), (1, 0, 0)]  # Blue, White, Red
    # Create a colormap
    cmap = sns.blend_palette(colors, as_cmap=True)
    return cmap


def plot_cnv_cluster_or_heat_map(heatmap_data: pd.DataFrame,
                                 cmap: sns.blend_palette=None,
                                 base_file_name: str=None,
                                 params: dict=None,
                                 fig_size: tuple=None,
                                 cell_font_size: int=8,
                                 dendrogram_ratio: tuple=(0.002, 0.05),
                                 value_plotted: str= "log2cnr",
                                 is_clustermap: bool=True,
                                 method='average',
                                 center_value: int=0,
                                 analysis_for_title: str="",
                                 vmin: int = -2,
                                 vmax: int = 4) -> None:
    """
    Plot out the heatmap or cluster map for the values passed in
    :param heatmap_data: Dataframe for heatmap
    :param cmap: camp for coloring
    :param base_file_name: base filename to print
    :param params: plt params to use in plt.rcParams.update(params)
    :param fig_size: Size of the file (tuple)
    :param cell_font_size: integer for the cell font szie
    :param dendrogram_ratio: how large to make the dendrogram on the top and side for the cluster map
    :param value_plotted: string of the type of value that was plotted
    :param is_clustermap:  if True, then plot clustermap else plot heatmap
    :param method: Method to cluster on
    :param center_value what should the white be centered on?
    :return: None type
    """
    # for the scalre
    plt.rcParams.update(params)
    plot_type = 'Clustermap'
    if is_clustermap is True:
        sns.clustermap(
            heatmap_data,
            method=method,
            linewidths=0.05,
            col_cluster=True,  # Cluster columns (genes)
            row_cluster=True,  # Cluster rows (samples)
            figsize=fig_size,
            dendrogram_ratio=dendrogram_ratio,
            fmt=".2f",  # Format for the annotations
            cmap=cmap,
            cbar_pos=None,
            cbar=False,
            center=center_value,
            annot=True,  # Annotate with values
            annot_kws={"fontsize": cell_font_size},  # Set the font size for annotations
            vmin=vmin,
            vmax=vmax,
        )
    else:
        plot_type = 'Heatmap'
        cm = sns.heatmap(
            heatmap_data,
            fmt=".2f",  # Format for the annotations
            cmap=cmap,
            center=0,
            annot=True,  # Annotate with values
            annot_kws={"fontsize": cell_font_size},  # Set the font size for annotations
            vmin=vmin,
            vmax=vmax,
        )

    plt.xlabel('Gene')
    plt.ylabel('Sample')
    plt.title(f'{plot_type} of Gene {value_plotted} Values {analysis_for_title}')
    plt.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(f"{base_file_name}.svg")
    plt.close()


def plot_barchart_median_values_per_gene(heatmap_data: pd.DataFrame=None,
                                         value_to_plot: str=None,
                                         all_genes: bool=None,
                                         base_file_name: str=None,
                                         analysis_for_title: str="",
                                         fig_size: tuple=None,
                                         xlim: tuple=(-0.3, 0.7)) -> None:

    """
    Bar chart of the  median values in the heatmap data
    :param heatmap_data:  matrix of values with samples
    :param base_file_name: base filename to print
    :param analysis_for_title: Just a string to add for the title
    :param fig_size: Size of the file (tuple)
    :return: None
    """

    plt.figure(figsize=fig_size)
    plt.grid(axis='x')
    # Calculate the median of each column
    medians = heatmap_data.median().sort_values()
    # Create a bar plot sorted by median values
    sns.barplot(x=medians.values, y=medians.index, orient='h')

    plt.title(f'Median {value_to_plot} Values of Each Gene {analysis_for_title}')
    plt.xlabel(f'Median {value_to_plot} Value')
    plt.ylabel('Genes')
    plt.xlim(xlim)
    if value_to_plot in ('CNLR_MEDIAN', 'log2ratio Median'):
        # Set x-axis major tick marks at multiples of 0.05
        plt.gca().xaxis.set_major_locator(MultipleLocator(0.05))
        plt.axvline(x=0.1, color='red', linestyle='--')
        plt.axvline(x=-0.1, color='red', linestyle='--')
    elif value_to_plot == 'TCN_EM':
        plt.axvline(x=0.1, color='red', linestyle='--')
    else:
        raise ValueError(f"Unknown value_to_plot: {value_to_plot}")
    plt.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(f"{base_file_name}.svg")
    plt.close()
