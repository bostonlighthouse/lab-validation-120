import pandas as pd
import argparse
import os

def main():
    args = get_cli_args()
    coverage_file = args.input_coverage_file
    df = pd.read_csv(coverage_file, sep="\t")
    with open(args.input_cids_file_names_to_remove) as infh:
        bams = [line.strip().lower() for line in infh]

    cols_to_drop = [col for col in df.columns if any(sub in col for sub in bams)]
    print(f"len columns before dropping {len(df.columns)}")
    df = df.drop(columns=cols_to_drop)
    print(f"len columns before dropping {len(df.columns)}")

    file = os.path.splitext(os.path.basename(coverage_file))[0]
    df.to_csv(f"{file}_removed_cids.cov", sep="\t", index=False)


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Module to take the coverage output from bedtools multicov and reduce cids"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?

    parser.add_argument('--input_coverage_file', dest='input_coverage_file',
                        help="input coverage file from bedtools multicov", required=True)

    parser.add_argument('--input_cids_file_names_to_remove', dest='input_cids_file_names_to_remove',
                        help="input file with one cid per line used to remove from the coverage file",
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()