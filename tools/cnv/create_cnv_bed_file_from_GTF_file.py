"""Module used to convert the Assay's GTF into the BED file for CNV script cnv_create_pon_coefs.R"""
import argparse


def main():
    """Main driver for the program"""
    args = get_cli_args()
    # process all lines in the GTF file so they follow the upstream and downstream (10bp) of the GSP2
    # This pattern was found by analyzing nfs2:
    # /export/resources/snapshot-onco/onco-snapshot.variantplex.cnv.20190925.bed
    with open(args.gtf_file) as infh, open(args.bed_file, 'w') as outfh:
        for line in infh:
            chromosome, start, end, strand_sense, name = get_values_from_line(line=line)
            # update the start and stop based on Archer-GSP2-Primers-v10-20211214.pptx + / - Primer
            overlap = 9  # we use 9 b/c we are looking a 10bp inclusive of the start or end positions from below
            if strand_sense == '+':
                start = end + 1
                end = start + overlap
            elif strand_sense == '-':
                end = start - 1
                start = end - overlap
            else:
                raise ValueError(f"strand_sense is not + or -  Here it's {strand_sense}")
            print("\t".join((chromosome, str(start), str(end), fix_name(name=name))), file=outfh)


def fix_name(name=None):
    """
    :param name: The name to fix
    Before: name "SDHB_chr1_17345274_36_+_A1_GSP2"; transcript_id ""; function "SNV"
    After: SDHB_chr1_17345274_36_+

    Before: name "SDHB_chr1_17345422_26_-_A1_GSP2"; transcript_id ""; function "SNV,CNV"
    After: SDHB_chr1_17345422_26_-_CNV
    """
    name = name.replace('"', '')  # get rid of quotes since they are not needed
    values = name.split("; ")  # split on '; '
    values_dict = {}  # store the values in a dictionary
    for value in values:
        key, val = value.split(' ')  # split on space
        values_dict[key] = val
    # fix the name
    values_dict['name'] = '_'.join(values_dict['name'].split('_')[0:5])
    # did the function have a CNV in it, if so append on CNV to the name
    if 'CNV' in values_dict['function']:
        values_dict['name'] = '_'.join((values_dict['name'], 'CNV'))

    return values_dict['name']


def get_values_from_line(line=None):
    """
    :param line: The line of data
    """
    values = line.rstrip().split("\t")
    chromosome = values[0].replace("chr", "")  # some have chr1 or chr2
    _ = values[1]  # e.g. ArcherAssay
    _ = values[2]  # e.g. GSP2 or primer_bind
    start = int(values[3])  # e.g. 6846810
    end = int(values[4])  # e.g. 6846836
    _ = values[5]  # e.g. 0 or 1, no idea what it is, but not needed here
    strand_sense = values[6]  # + or -
    _ = values[7]  # e.g. .
    name = values[8]
    return chromosome, start, end, strand_sense, name


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Create the BED file for the cnv_create_pon_coefs.R R script following pattern from: " \
               "Archer-GSP2-Primers-v10-20211214.pptx"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--gtf_file', dest='gtf_file', help="Pass in the INPUT GTF file", required=True)
    parser.add_argument('--bed_file', dest='bed_file', help="Pass in the OUTPUT BED file", required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()
