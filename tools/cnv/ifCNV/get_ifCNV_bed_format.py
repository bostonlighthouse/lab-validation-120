"""
Module to create the BED file needed for ifCNV.  This program can create a simple BED file with Exon information:
--print_simple_bed_file

Or it can create a BED file with the cytoband information:
--print_cytoband_bed_file
"""
import argparse
import logging
import pandas as pd

from lab_validation_120.utils.pandas_dataframes_utilities import get_columns_from_bed_with_no_header, \
    read_dataset_with_no_header_and_get_pandas_df
from lab_validation_120.utils.configs.lab_validation_config import get_oc395_targets_bed_file, \
    get_agilent_hg19_hybrid_capture_dna_targets_bed_file_for_ifcnv_bed_file_generation
from tools.ensembl.rest.ensembl_rest_client import EnsemblRestClient
from lab_validation_120.utils.assay_utils import get_assay_names

logging.basicConfig(level=logging.DEBUG)
# pylint: disable=logging-fstring-interpolation

PROBE_NUMBER = 0  # need this b/c this is called via and lambda and apply


def main():
    """Main business Logic"""
    args = get_cli_args()
    assay_name = args.assay_name
    df_assay_bed = None

    if assay_name == 'gs395':
        df_assay_bed = read_dataset_with_no_header_and_get_pandas_df(col_vals=get_columns_from_bed_with_no_header(),
                                                                     file_name=get_oc395_targets_bed_file())
    elif assay_name == 'gs700':
        df_assay_bed = \
            read_dataset_with_no_header_and_get_pandas_df(
                col_vals=get_columns_from_bed_with_no_header(),
                file_name=get_agilent_hg19_hybrid_capture_dna_targets_bed_file_for_ifcnv_bed_file_generation())

    # pylint: disable=unnecessary-lambda
    df_assay_bed['gene'] = df_assay_bed.apply(lambda x: get_gene(x), axis=1)
    df_assay_bed['ifcnv_name'] = df_assay_bed.apply(lambda x: get_ifcnv_name(x), axis=1)

    # ifCNV will have exon level information so ifCNV can use it, e.g.:
    # 1       2488055 2488221 TNFRSF14_E1_pr1 0       +
    # https://github.com/SimCab-CHU/ifCNV#changing-the-resolution
    if args.print_simple_bed_file:
        df_assay_bed = df_assay_bed[df_assay_bed['ifcnv_name'] != 'snp']
        for _, row in df_assay_bed.iterrows():
            print_bed_format(row)

    # in this case the BED file will have cytoband data added to the BED file by using Ensemble API
    # and the final bed file will be set so ifCNV can use cytoband level clustering, e.g.:
    # 1       2488055 2488221 1p36_TNFRSF14_E1_pr1    0       +
    # https://github.com/SimCab-CHU/ifCNV#changing-the-resolution
    elif args.print_cytoband_bed_file:
        ensembl_client = EnsemblRestClient()  # used to download Ensembl data from REST endpoint
        df_assay_bed = df_assay_bed[df_assay_bed['ifcnv_name'] != 'snp']
        gene_to_band_map = {}
        # go over each gene and get the cytoband data and store in the gene_to_band_map
        for gene in set(df_assay_bed['gene'].to_list()):
            gene_df = df_assay_bed[df_assay_bed['gene'] == gene].copy()
            start = gene_df['START_ZERO_BASED'].min()
            end = gene_df['END'].max()
            chromosome = gene_df['CHROM'].to_list()[0]
            # call off to ensembl and get the cytoband data
            band = _get_cytoband_data(gene, chromosome, start, end, ensembl_client)
            gene_to_band_map[gene] = band  # store for later

        undef = 'UNDEF'
        for _, row in df_assay_bed.iterrows():
            gene = row['gene']
            chromosome = row['CHROM']
            update = gene_to_band_map.get(gene, undef)
            if update == undef:
                logging.debug(f"Skipping this gene {gene}")
                continue
            update = update.split('.')[0]  # split off the p22.1 so we only have p22
            # update the row
            row['ifcnv_name'] = '_'.join((chromosome + update, row['ifcnv_name']))
            print_bed_format(row)
    else:
        raise ValueError("No option given for BED file type to print")

def print_bed_format(row: pd.Series = None) -> None:
    """
    Print out the row in BED format
    :param row: pd.Series
    :return: None
    """
    print("\t".join((row['CHROM'], str(row['START_ZERO_BASED']), str(row['END']), row['ifcnv_name'],
                     str(row['SCORE']), row['STRAND'])))


def _get_cytoband_data(gene: str = None, chromosome: str = None, start: int = None, end: int = None,
                       ensembl_client: EnsemblRestClient = None) -> str:
    """
    Query Ensembl and get the cytoband information

    :param gene: gene under review
    :param chromosome: chromosome of the interval
    :param start: start of the interval
    :param end: end of the interval
    :param ensembl_client: Instance of the EnsemblRestClient
    :return: String of the cytoband
    """
    endpoint = f"/info/assembly/homo_sapiens/{chromosome}:{start}-{end}"  # ensembl endpoint
    endpoint = '?'.join((endpoint, 'bands=1'))  # add in some features to the query
    logging.debug(f"NEW EXON ENDPOINT QUERY: {endpoint}")
    # call on custom class to get a list of the ensembl transcripts
    ensembl_objects = ensembl_client.perform_rest_action(endpoint=endpoint)
    logging.debug(ensembl_objects)
    if len(ensembl_objects['karyotype_band']) > 1:
        logging.debug(f"The length is greater than 1 {gene}: {ensembl_objects['karyotype_band']}")
        # there's more than one cytoband, this can happened b/c a gene can overlap multiple bands
        # for this BED file we'll use the same band for the gene so find the band (interval) that overlaps
        # the BED interval by the most
        optimal_list = sort_bands_by_interval_overlap(band_list=ensembl_objects['karyotype_band'], start=start, end=end)
        ensembl_objects['karyotype_band'] = optimal_list
    # take the last element off, if its one element like most or if it was sorted by overlap in
    # sort_bands_by_interval_overlap
    return ensembl_objects['karyotype_band'].pop()['id']


def sort_bands_by_interval_overlap(band_list: list = None, start: int = None, end: int = None) -> list:
    """
    Overlap = min(A2, B2) - max(A1, B1) + 1
    :param band_list: The list of dict of karyotype_band from ensemble
    :param start: start of the bed interval
    :param end: end of the bed interval
    :return: the optimal
    """
    overlap_key = 'overlap'
    assert len(band_list) <= 2, "The length is greater than 2"
    # go over each band and find the overlap with the bed interval
    # the highest bed interval will be the optimal and last in the final list returend
    for band in band_list:
        band[overlap_key] = min((end, band['end'])) - max(start, band['start'])

    logging.debug(f"not sorted {band_list}")
    # always sort ascending here so the final element is the element with the most overlap
    # this is the optimal band and will be use for the cytoband since I want one gene = one cytoband for copy number
    # purposes
    sorted_band_list = sorted(band_list, key=lambda x: x[overlap_key])
    logging.debug(f"sorted {sorted_band_list}")
    return sorted_band_list


def get_ifcnv_name(row: pd.Series) -> str:
    """
    Go through and find the name to be used in the BED file for ifCNV format
    :param row: pd.Series
    :return:
    """
    # pylint: disable=global-statement
    global PROBE_NUMBER
    # g.139390145T>C;c.*7668+378A>G;g.139390152T>C;c.*7668+371A>G;NM_017617.4_Exon_34 0
    if "g.139390145T>C" in row['NAME']:
        PROBE_NUMBER += 1
        return f'NOTCH1_E34_pr{PROBE_NUMBER}'
    if "KLF4;NM_004235_exon_003_TILE_arBB3gLT" in row['NAME']:  # strange entry from Archer
        PROBE_NUMBER += 1
        return f'KLF4_E3_pr{PROBE_NUMBER}'
    if ";" in row['NAME']:
        PROBE_NUMBER += 1
        values = row['NAME'].split(";")
        gene_name = values.pop(0)
        # NM_004431.4_Exon_17,NM_001329090.1_Exon_16
        values[0] = values[0].replace('Exon_', 'E')  # update all NM_000983.3_Exon_4 - NM_000983.3_Exon4
        values[0] = values[0].replace('NM_', 'NM')  # update all NM_000983.3_E4 - NM000983.3_E4
        values[0] = values[0].replace('XM_', 'XM')  # update all XM_005245778.1_E1 - XM005245778.1_E4
        values[0] = values[0].replace('NR_', 'NR')  # update all NR_001566.1_E1 - NR001566.1_E1
        # just sort the accessions since there could be more than one and they are out of order
        values = sorted(values[0].split(','))
        _, exon = values[0].split('_')
        return "_".join((gene_name, exon, 'pr' + str(PROBE_NUMBER)))
    # just return SNP
    return 'snp'


def get_gene(row: pd.Series) -> str:
    """
    Get the gene name
    :param row: pd.Series
    :return: String of the gene name, or an 'snp'.
    """
    # RPL22;NM_000983.3_Exon_4
    if ";" in row['NAME']:
        values = row['NAME'].split(";")
        return values[0]
    return 'snp'


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open the GS395 BED file and produce a BED file format that can be '
                                                 'used with ifCNV CNV caller')

    parser.add_argument('--print_simple_bed_file', dest='print_simple_bed_file',
                        type=bool, help='Print the simple BED file with Gene exon ',
                        required=False)

    parser.add_argument('--print_cytoband_bed_file', dest='print_cytoband_bed_file',
                        type=bool, help='Print the more complete BED that requires cyto band along with Gene exon ',
                        required=False)

    parser.add_argument('--assay_name', dest='assay_name', required=True,
                        help="Assay name.  Used for final naming of files",
                        type=str, choices=get_assay_names())

    return parser.parse_args()


if __name__ == "__main__":
    main()
