"""Module to compare separate runs of ifCNV"""
import argparse
import pandas as pd
from tools.utils.tools_utils import get_cid_from_string


def main():
    """Maine business logic"""
    args = get_cli_args()
    ifcnv_df1 = pd.read_csv(args.ifconf_output_file1, sep="\t")
    ifcnv_df1['cid'] = ifcnv_df1.apply(lambda row: get_cid_from_string(string=row['Sample name']), axis=1)
    ifcnv_df2 = pd.read_csv(args.ifconf_output_file2, sep="\t")
    ifcnv_df2['cid'] = ifcnv_df2.apply(lambda row: get_cid_from_string(string=row['Sample name']), axis=1)
    ifcnv_final = ifcnv_df1.merge(ifcnv_df2, how="outer", on=["Sample name", "Region"])

    if args.ifconf_output_file3:
        ifcnv_df3 = pd.read_csv(args.ifconf_output_file3, sep="\t")
        ifcnv_df3['cid'] = ifcnv_df3.apply(lambda row: get_cid_from_string(string=row['Sample name']), axis=1)
        ifcnv_final = ifcnv_final.merge(ifcnv_df3, how="outer", on=["Sample name", "Region"])

    if args.ifconf_output_file4:
        ifcnv_df4 = pd.read_csv(args.ifconf_output_file4, sep="\t")
        ifcnv_df4['cid'] = ifcnv_df4.apply(lambda row: get_cid_from_string(string=row['Sample name']), axis=1)
        ifcnv_final = ifcnv_final.merge(ifcnv_df4, how="outer", on=["Sample name", "Region"],
                                        suffixes=('_a', '_b'))

    ifcnv_final.to_excel("b21-116-sT_0_ct_0.01-final.xlsx")


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Compare the ifCNV outputs')

    parser.add_argument('--ifconf_output_file1', dest='ifconf_output_file1',
                        type=str, help='Output file 1 from ifCNV ',
                        required=True)

    parser.add_argument('--ifconf_output_file2', dest='ifconf_output_file2',
                        type=str, help='Output file 2 from ifCNV ',
                        required=True)

    parser.add_argument('--ifconf_output_file3', dest='ifconf_output_file3',
                        type=str, help='Output file 3 from ifCNV ',
                        required=False)

    parser.add_argument('--ifconf_output_file4', dest='ifconf_output_file4',
                        type=str, help='Output file 4 from ifCNV ',
                        required=False)

    return parser.parse_args()


if __name__ == "__main__":
    main()
