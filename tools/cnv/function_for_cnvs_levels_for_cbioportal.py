"""
Module used to calculate  thresholds for CNV values for cBioPortal - Discrete Copy Number Data

Example run of the program:
pipenv run python -m tools.cnv.function_for_cnvs_levels_for_cbioportal \
--tumor_purity 1.1 \
--copy_state_for_high_level_amplification 6

https://docs.cbioportal.org/5.1-data-loading/data-loading/file-formats#discrete-copy-number-data
For each gene-sample combination, a copy number level is specified:
"-2" is a deep loss, possibly a homozygous deletion
"-1" is a single-copy loss (heterozygous deletion)
"0" is diploid
"1" indicates a low-level gain
"2" is a high-level amplification.

Genes we care about the most.  From Rodrigo

ATM
BRCA1
BRCA2
CCND1
CDKN2A
EGFR
ERBB2
ESR1
FGF19
FGFR1
FGFR2
FGFR3
MET
MYC
MYCN
NOTCH1
NOTCH2
PIK3CA
PTEN
TP53


"""
import argparse
from math import log2
import pprint as pp


class Range:
    """ Class to help with range for argsparse"""
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __eq__(self, other):
        return self.start <= other <= self.end

    def __contains__(self, item):
        return self.__eq__(item)

    def __iter__(self):
        yield self

    def __repr__(self):
        return '[{0}, {1}]'.format(self.start, self.end)


def main():
    """Main driver for the program"""
    args = get_cli_args()
    # get the thresholds
    thresholds = get_copy_number_thresholds(args.copy_state_for_high_level_amplification, args.tumor_purity)

    # Just showing you the data structure
    print("\nJust showing you the data structure")
    pp.pprint(thresholds, depth=4)

    # just showing access of the dictionary returned
    print("\n# just showing access of the dictionary returned")
    for copy_state, cbio_dict in thresholds.items():
        print(f"copy_state {copy_state} cnr_threshold {cbio_dict['cnr_threshold']} "
              f"cbio_value {cbio_dict['cbio_value']}")
    print("")

    # for Alexis import statements

    # from function_for_cnvs_levels_for_cbioportal import get_cbio_value, get_copy_number_thresholds, calculate_cnv

    # just call get_cbio_value with the cnr, the threshold dict, and the copy_state_for_high_level_amplification = 6
    # and apply across the pandas df
    thresholds = get_copy_number_thresholds(args.copy_state_for_high_level_amplification, args.tumor_purity)
    val = get_cbio_value(cnr=-4.0, thresholds=thresholds,
                         copy_state_for_high_level_amplification=6)
    print(val)


def test_code():
    """
    Simple test function
    """

    copy_state_for_high_level_amplification = 6
    tumor_purity = .60
    thresholds = get_copy_number_thresholds(copy_state_for_high_level_amplification, tumor_purity)
    _run_asserts(thresholds=thresholds,
                 copy_state_for_high_level_amplification=copy_state_for_high_level_amplification)

    # Use the above
    copy_state_for_high_level_amplification = 4
    tumor_purity = .50
    thresholds = get_copy_number_thresholds(copy_state_for_high_level_amplification, tumor_purity)

    # simple test to show you how to call this
    assert get_cbio_value(cnr=-4.0, thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == -2,\
        "2 copy loss at threshold is not -2"

    _run_asserts(thresholds=thresholds,
                 copy_state_for_high_level_amplification=copy_state_for_high_level_amplification)


def _run_asserts(thresholds: dict, copy_state_for_high_level_amplification: int):
    """
    Run asserts for tests
    :param thresholds: Dictionary of Dictionaries for of copy-state(s)
    :param copy_state_for_high_level_amplification: Copy state for high-level amplification
    """

    # 2 copy loss
    # test copy state 2 threshold, should return -2
    assert get_cbio_value(cnr=thresholds[0]['cnr_threshold'], thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == -2,\
        "2 copy loss at threshold is not -2"
    # test copy state 2 threshold - 0.1, should be -2
    assert get_cbio_value(cnr=thresholds[0]['cnr_threshold'] - 0.1, thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == -2,\
        "2 copy loss at threshold - 0.1 is not -2"
    # test copy state 2 threshold + 0.1, should be -1
    assert get_cbio_value(cnr=thresholds[0]['cnr_threshold'] + 0.1, thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == -1,\
        "2 copy loss at threshold + 01 is not -1"

    # 1 copy loss
    # test copy state 1 threshold, should return -1
    assert get_cbio_value(cnr=thresholds[1]['cnr_threshold'], thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == -1,\
        "1 copy loss is not -1"

    # test copy state 1 threshold - 0.1, should return -1
    assert get_cbio_value(cnr=thresholds[1]['cnr_threshold'] - 0.1, thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == -1,\
        "1 copy loss at threshold - 0.1 is not -1"

    # test copy state 1 threshold + 0.1, should return -1
    assert get_cbio_value(cnr=thresholds[1]['cnr_threshold'] + 0.1, thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 0,\
        "1 copy loss at threshold + 0.1 is not 0"

    # neutral tests
    # test copy neutral at threshold, should return 0
    assert get_cbio_value(cnr=thresholds[2]['cnr_threshold'], thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 0,\
        "copy neutral at threshold is is not 0"
    # test copy neutral at threshold + 0.1, should return 0
    assert get_cbio_value(cnr=thresholds[2]['cnr_threshold'] + 0.1, thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 0,\
        "copy neutral at threshold + 0.1 is  0"
    # test copy neutral at threshold - 0.1, should return 0
    assert get_cbio_value(cnr=thresholds[2]['cnr_threshold'] - 0.1, thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 0,\
        "copy neutral at threshold - 0.1 is  0"

    # 1 copy gain tests
    # test low-level copy gain at threshold, should return 1
    assert get_cbio_value(cnr=thresholds[3]['cnr_threshold'], thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 1,\
        "low level copy gain at threshold is not 1"
    # test low-level copy gain at threshold + 0.1, should return 1
    assert get_cbio_value(cnr=thresholds[3]['cnr_threshold'] + 0.1, thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 1,\
        "low level copy gain at threshold + 0.1 is not 1"
    # test low-level copy gain at threshold - 0.1, should return 1
    assert get_cbio_value(cnr=thresholds[3]['cnr_threshold'] - 0.1, thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 0,\
        "low level copy gain at threshold - 0.1 is not 0"

    # 1 copy gain tests
    # test high-level copy gain at threshold, should return 2
    assert get_cbio_value(cnr=thresholds[copy_state_for_high_level_amplification]['cnr_threshold'],
                          thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 2,\
        f"high-level copy gain at threshold, copy state: {copy_state_for_high_level_amplification} is not 2"

    # test high-level copy gain at threshold + 0.1, should return 2
    assert get_cbio_value(cnr=thresholds[copy_state_for_high_level_amplification]['cnr_threshold'] + 0.1,
                          thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 2,\
        f"high-level copy gain at threshold + 0.1, copy state: {copy_state_for_high_level_amplification} is not 2"

    # test high-level copy gain at threshold + 0.1, should return 2
    assert get_cbio_value(cnr=thresholds[copy_state_for_high_level_amplification]['cnr_threshold'] - 0.1,
                          thresholds=thresholds,
                          copy_state_for_high_level_amplification=copy_state_for_high_level_amplification) == 1,\
        f"high-level copy gain at threshold - 0.1, copy state: {copy_state_for_high_level_amplification} is not 1"


def get_cbio_value(cnr: float, thresholds: dict, copy_state_for_high_level_amplification: int) -> int:
    """
    Find the CbioPortal value values for the discrete CNV values
    https://docs.cbioportal.org/5.1-data-loading/data-loading/file-formats#discrete-copy-number-data
    For each gene-sample combination, a copy number level is specified:
    "-2" is a deep loss, possibly a homozygous deletion
    "-1" is a single-copy loss (heterozygous deletion)
    "0" is diploid
    "1" indicates a low-level gain
    "2" is a high-level amplification.
    :param cnr: log2 Copy number ratio
    :param thresholds: Dictionary of Dictionaries for of copy-state(s)
    :param copy_state_for_high_level_amplification: Copy state for high-level amplification
    """
    # 0 copy state should return -2
    ret_val = None
    if cnr <= thresholds[0]['cnr_threshold']:
        ret_val = thresholds[0]['cbio_value']
    # 1 copy loss should return -1
    elif thresholds[0]['cnr_threshold'] < cnr <= thresholds[1]['cnr_threshold']:
        ret_val = thresholds[1]['cbio_value']
    # 2 copy state should return 0
    elif thresholds[1]['cnr_threshold'] < cnr < thresholds[3]['cnr_threshold']:
        ret_val = thresholds[2]['cbio_value']
    # low-level should return 1, this is above neutral and below copy_state_for_high_level_amplification
    elif thresholds[3]['cnr_threshold'] <= cnr < thresholds[copy_state_for_high_level_amplification]['cnr_threshold']:
        ret_val = thresholds[3]['cbio_value']
    # high-level should return 2
    elif cnr >= thresholds[copy_state_for_high_level_amplification]['cnr_threshold']:
        ret_val = thresholds[copy_state_for_high_level_amplification]['cbio_value']
    else:
        raise ValueError(f"CNR of {cnr} not found....")

    return ret_val


def get_copy_number_thresholds(copy_state_for_high_level_amplification: int, tumor_purity: float) -> dict:
    """
    Function to return a dictionary of dictionaries
    key: copy number state
    value: dictionary with cbio value for the discrete states (cbio_value) and cnr threshold (cnr_threshold)
    :param copy_state_for_high_level_amplification: The high level copy number state
    :param tumor_purity: Tumor purity evaluated, float like 0.6 for 60%
    :return dictionary
    """

    cbio_dict = {}
    # go over the copy states we care about 0, 1, 2, 3 and copy_state_for_high_level_amplification
    # create the dictionary to be used later
    for copy_state, cbio_val in {0: -2,  # key, copy state 0: value, homozygous deletion is -2
                                 1: -1,  # key, copy state 1: value, single-copy loss (heterozygous deletion) -1
                                 2: 0,  # key, copy stat2: value diploid is 0
                                 3: 1,  # key, low-level gain: value for low-level gain (1)
                                 copy_state_for_high_level_amplification: 2  # key, high-level gain: value 2
                                 }.items():
        # fill the dict of dict
        cbio_dict[copy_state] = {
            'cbio_value': cbio_val,
            'cnr_threshold': calculate_cnv(copy_state=copy_state, purity_level=tumor_purity),
        }

    return cbio_dict


def calculate_cnv(copy_state: int, purity_level: float) -> float:
    """
    Calculate the Copy Number Ratio from the copy state and purity level passed in
    :param copy_state: Int for the copy state
    :param purity_level: The neoplastic content of the sample
    """
    tumor_alleles = round(100 * purity_level, 2)  # number of tumor alleles
    normal_alleles = round(100 * (1 - purity_level), 2)  # number of normal alleles
    tumor_allele_gain = (tumor_alleles / 2) * copy_state  # how many tumor alleles gained given the copy_sate
    if copy_state == 0:
        numerator = normal_alleles + 5
    else:
        numerator = normal_alleles + tumor_allele_gain  # what's the numerator in the log2 ratio
    cnr = round(log2(numerator / 100), 4)
    return cnr


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Create the BED file for the cnv_create_pon_coefs.R R script following pattern from: " \
               "Archer-GSP2-Primers-v10-20211214.pptx"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--tumor_purity', dest='tumor_purity', help="Tumor Purity used for calculation", required=True,
                        type=float, choices=Range(0.0, 1.0))
    parser.add_argument('--copy_state_for_high_level_amplification', dest='copy_state_for_high_level_amplification',
                        help="Copy state Threshold for high-level amplification", required=True, type=int)

    return parser.parse_args()


if __name__ == "__main__":
    main()
