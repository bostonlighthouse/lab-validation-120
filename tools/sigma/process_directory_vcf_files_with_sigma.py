"""
Module used to:
    1. Take an input directory of VCF files
    2. Parse and Filter each VCF file for SigMA and place the VCF file in a directory based on tumor type
    3. Run SigMA on each tumor type directory
    4. Open final outputs from each run of SigMA and combine in final DF and include tumor type
    5. Also print out an excel file of potential variants that were consequential and could lead to HRD

"""
import argparse
import os
from typing import List, Tuple, Set
import pandas as pd

from biomodels.models.vcf import vcf_container as vc
from tools.sigma.sigma_utils import run_sigma_on_vcfs, get_project_file_name, print_filtered_sigma_vcf_file, \
    filter_vc_container
from tools.utils.tools_utils import open_directory_get_list_files_recursively, return_oc395_pon_counts_json, \
    get_pon_counts_data
from tools.tmb.utils.tmb_utils import get_lims_data
from tools.utils.tools_utils import test_absolute_path
from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame, get_cid_from_file_name


def main():
    """Business Logic"""
    args = get_cli_args()
    # get the CLI options
    cider_vcf_directory = args.cider_vcf_directory
    final_vcf_directory = args.final_vcf_directory
    sigma_output_file_name = args.sigma_output_file_name
    depth_threshold = args.depth_threshold
    vaf_threshold = args.vaf_threshold
    consequential_impacts_only = args.consequential_impacts_only
    filter_germline_variants = args.filter_germline_variants
    filter_indel_variants = args.filter_indel_variants
    test_absolute_path(cider_vcf_directory, final_vcf_directory, sigma_output_file_name)
    # get the list of VCF files to process
    list_vcf_files = open_directory_get_list_files_recursively(directory=cider_vcf_directory)

    # get the lims data
    lims_df = get_lims_data(assay_name='gs395', get_pon_data=False)

    # get the PoN count data
    oc395_pon_data = get_pon_counts_data(json_file=return_oc395_pon_counts_json())

    # Go over the list of VCF files, generate VC containers via Biomodels, and then filter accordingly
    # and print out the new VCF files

    final_vcf_files, filter_df, final_vcf_directories, final_variants_associated_with_hrd_df = \
        process_vcf_files_by_tumor_type(list_vcf_files=list_vcf_files,
                                        final_vcf_directory=final_vcf_directory,
                                        vaf_threshold=vaf_threshold,
                                        depth_threshold=depth_threshold,
                                        consequential_impacts_only=consequential_impacts_only,
                                        filter_germline_variants=filter_germline_variants,
                                        filter_indel_variants=filter_indel_variants,
                                        lims_df=lims_df,
                                        oc395_pon_data=oc395_pon_data)

    sigma_lit_data_frames = []  # get a list of sigma lite data frames
    tumor_types = tumor_type_to_model()
    do_mva, do_assign, tumor_type_for_sigma, model_to_use = None, None, None, None
    for directory in sorted(final_vcf_directories):  # just make output the same
        tumor_type = directory.split("_")[-1]
        if tumor_type in tumor_types:
            do_mva = tumor_types[tumor_type]['do_mva']
            do_assign = tumor_types[tumor_type]['do_assign']
            tumor_type_for_sigma = tumor_types[tumor_type]['tumor_type']
            model_to_use = tumor_types[tumor_type]['model_to_use']

        # Call SigMA and get a data frame from the lite output
        sigma_lite_df = run_sigma_on_vcfs(input_dir=directory,
                                          consequential_impacts_only=consequential_impacts_only,
                                          vaf_threshold=vaf_threshold,
                                          depth_threshold=depth_threshold,
                                          filter_germline_variants=filter_germline_variants,
                                          sigma_output_file_name=sigma_output_file_name,
                                          tumor_type=tumor_type_for_sigma,
                                          filter_indel_variants=filter_indel_variants,
                                          do_mva=do_mva,
                                          do_assign=do_assign,
                                          model_to_use=model_to_use)
        sigma_lit_data_frames.append(sigma_lite_df)  # store for later merge

    # combine them all
    sigma_lite_df = pd.concat(sigma_lit_data_frames).reset_index(drop=True)

    # get the lims data
    lims_df = get_lims_data(assay_name='gs395', get_pon_data=False)
    # get the tmb dat
    tmb_df = get_tmb_df_for_sigma_validation_samples()

    # merge the clinical LIMS data with the sigma data
    final_df = pd.merge(sigma_lite_df, lims_df, how='left', on='cid')
    final_df = pd.merge(final_df, filter_df, how='left', on='cid')
    final_df = pd.merge(final_df, tmb_df, how='left', on='cid')

    # get final output filename
    final_output = get_project_file_name(base="final_sigma_output",
                                         vaf_threshold=vaf_threshold,
                                         depth_threshold=depth_threshold,
                                         consequential_impacts_only=consequential_impacts_only,
                                         filter_germline_variants=filter_germline_variants,
                                         tumor_type="combined_sigma_tumor_types",
                                         filter_indel_variants=filter_indel_variants)
    # print out the final data frame
    print_data_frame(df=final_df, tabular_output_file=final_output)
    print_data_frame(df=final_variants_associated_with_hrd_df,
                     tabular_output_file=final_output.replace("final_sigma_output", "final_variants_sigma_output"))

    # print out the final dataframes to excel
    final_output = os.path.splitext(final_output)[0]
    with pd.ExcelWriter(f'{final_output}.xlsx') as writer:
        final_variants_associated_with_hrd_df.to_excel(writer, sheet_name="variants")
        final_df.to_excel(writer, sheet_name="final_output")


def process_vcf_files_by_tumor_type(list_vcf_files: List[str],
                                    final_vcf_directory: str,
                                    vaf_threshold: float,
                                    depth_threshold: int,
                                    consequential_impacts_only: bool,
                                    filter_germline_variants: bool,
                                    lims_df: pd.DataFrame,
                                    oc395_pon_data: dict = None,
                                    filter_indel_variants: bool = False
                                    ) -> Tuple[List[str], pd.DataFrame, Set[str], pd.DataFrame]:
    """
    # Go over the list of VCF files, generate VC containers via Biomodels, and then filter accordingly and print out
    # the new VCF files
    :param list_vcf_files:  List of VCF files to process for sigMA
    :param final_vcf_directory: Where to store the updated VCF file
    :param vaf_threshold: VAF threshold that was used for filtering
    :param depth_threshold: Depth threshold that was used for filtering
    :param consequential_impacts_only:  Filter only consequential impact?
    :param filter_germline_variants:  Filter out germline variants?
    :param lims_df: LIMS data frame that has tumor type for each sample
    :param oc395_pon_data: dictionary of counts from the PoN Json
    :param filter_indel_variants: Filter out the indels from the analysis
    :return: NoneType
    """
    final_vcf_directory = get_project_file_name(base=final_vcf_directory.rstrip("/"),
                                                consequential_impacts_only=consequential_impacts_only,
                                                filter_germline_variants=filter_germline_variants,
                                                vaf_threshold=vaf_threshold,
                                                depth_threshold=depth_threshold,
                                                filter_indel_variants=filter_indel_variants)
    final_vcf_directory, _ = os.path.splitext(final_vcf_directory)
    # tumor types from sigma
    sigma_tumor_types = tumor_type_to_model()
    # go over an process each VCF file
    updated_vcf_files = []
    filtering_data = []   # store list data frames from filtering data, i.e. how many calls were filtered,
    final_vcf_directories = set()  # store all the directories
    final_variants_associated_with_hrd = []  # sore all the data frames for variants associated with HRD
    # how many were SNV were filtered, and how many INDEL were filtered
    for i, vcf_file in enumerate(list_vcf_files):
        if i > 20_000_000:  # just used to run the program faster...
            break
        cid = get_cid_from_file_name(vcf_file)
        # get the primary site from the LIMS data
        primary_site = lims_df.loc[lims_df['cid'] == cid, "primary_site"].iloc[0]
        if primary_site not in sigma_tumor_types:
            continue

        print(f"process vcf file {vcf_file}")
        vc_container = vc.VcfContainer(vcf_file)

        # get a list of VcfEntry objects passing filtering.  These will be used later to print out VCF records for
        # sigMA in tumor specific directories
        variant_entry_objects, filtered_df, variants_associated_with_hrd = \
            filter_vc_container(cid=cid,
                                vc_container=vc_container,
                                consequential_impacts_only=consequential_impacts_only,
                                vaf_threshold=vaf_threshold,
                                depth_threshold=depth_threshold,
                                filter_germline_variants=filter_germline_variants,
                                filter_indel_variants=filter_indel_variants,
                                oc395_pon_data=oc395_pon_data)
        if len(variant_entry_objects) < 5:  # filtered out all the variants so skip...
            continue

        # update the directory with primary site.  Will be used later on when calling SigMA
        final_vcf_directory_w_ps = "_".join((final_vcf_directory, primary_site))
        # just create a final output directory if it doesn't exist
        if os.path.exists(final_vcf_directory_w_ps) is False:
            os.mkdir(final_vcf_directory_w_ps)
        # store the final directory in a set, so we can use thes later to call SigMA on each directory
        final_vcf_directories.add(final_vcf_directory_w_ps)

        # get an updated VCF file name for sigMA updated and filtered VCF file
        sigma_filtered_vcf_file = get_project_file_name(base="sigma_filtered",
                                                        vaf_threshold=vaf_threshold,
                                                        depth_threshold=depth_threshold,
                                                        consequential_impacts_only=consequential_impacts_only,
                                                        filter_germline_variants=filter_germline_variants,
                                                        filter_indel_variants=filter_indel_variants,
                                                        tumor_type=primary_site,
                                                        ext='vcf')
        # print out the new filtered VCF file for SigMA in a tumor specific directory
        updated_vcf_file = print_filtered_sigma_vcf_file(variant_entry_objects=variant_entry_objects,
                                                         original_vcf_file=vcf_file,
                                                         final_vcf_directory=final_vcf_directory_w_ps,
                                                         sigma_filtered_file=sigma_filtered_vcf_file)
        # print out some data on filtering statistics
        print(f"found {len(vc_container.dictionary_vcf_entries.keys())} originally, "
              f"after filtering found {len(variant_entry_objects)} "
              f"for {updated_vcf_file}")
        updated_vcf_files.append(updated_vcf_file)  # store a list of all VCF files
        filtering_data.append(filtered_df)  # store all the filtered data frames
        variants_associated_with_hrd_df = pd.DataFrame.from_records(variants_associated_with_hrd)  # to a data frame
        variants_associated_with_hrd_df['cid'] = cid  # add in the CID
        final_variants_associated_with_hrd.append(variants_associated_with_hrd_df)  # store all
    # combine into these lists into individual dataframes
    final_filtering_df = pd.concat(filtering_data).reset_index(drop=True)
    final_variants_associated_with_hrd_df = pd.concat(final_variants_associated_with_hrd).reset_index(drop=True)
    return updated_vcf_files, final_filtering_df, final_vcf_directories, final_variants_associated_with_hrd_df


def tumor_type_to_model() -> dict:
    """
    By running list_tumor_types() in Rstudio you can see the models used.
    :return: dict of the tumor types

    https://github.com/xtmgah/SigMA-1
    Tags and descriptions of performance of SigMA for signature 3 detection. The exploratory tumor types should be
    used with do_mva = F and do_assign = F settings for the run() function.

    """
    return {
        'Bladder': {
            'cancer_type': 'Urothelial Bladder Cancer',
            'models': ['wgs', 'seqcap', 'seqcap_probe', 'tcga_mc3', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'bladder',
            'do_mva': 'F',
            'do_assign': 'F',
            'details': 'exploratory'
        },
        'Breast': {
            'cancer_type': 'Breast Cancer',
            'models': ['wgs', 'wgs_pancan', 'seqcap', 'seqcap_probe', 'tcga_mc3', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'breast',
            'do_mva': 'T',
            'do_assign': 'T',
            'details': 'tested'
        },
        'Colon': {
            'cancer_type': 'Colorectal Adenocarcinoma',
            'models': ['wgs', 'tcga_mc3', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'crc',
            'do_mva': 'F',
            'do_assign': 'F',
            'details': 'exploratory'
        },
        '_Brain': {
            'cancer_type': 'Glioblastoma',
            'models': ['tcga_mc3'],
            'model_to_use': 'tcga_mc3',
            'tumor_type': 'gbm',
            'do_mva': 'F',
            'do_assign': 'F',
            'details': 'exploratory'
        },
        'Esophagus': {
            'cancer_type': 'Oesophageal carcinoma',
            'models': ['wgs', 'seqcap', 'seqcap_probe', 'tcga_mc3', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'eso',
            'do_mva': 'T',
            'do_assign': 'T',
            'details': 'tested'
        },
        'Lung': {
            'cancer_type': 'Lung Adenocarcinoma',
            'models': ['wgs', 'tcga_mc3'],
            'model_to_use': 'msk',
            'tumor_type': 'lung',
            'do_mva': 'F',
            'do_assign': 'F',
            'details': 'exploratory'
        },
        'Osteo': {  # not a OCPM
            'cancer_type': 'Osteosarcoma',
            'models': ['wgs', 'seqcap', 'seqcap_probe', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'osteo',
            'do_mva': 'T',
            'do_assign': 'T',
            'details': 'tested'
        },
        'Ovary': {
            'cancer_type': 'Ovarian Cancer',
            'models': ['wgs', 'seqcap', 'seqcap_probe', 'tcga_mc3', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'ovary',
            'do_mva': 'T',
            'do_assign': 'T',
            'details': 'tested'
        },
        'Pancreas': {
            'cancer_type': 'Pancreas Adenocarcinoma',
            'models': ['wgs', 'seqcap', 'seqcap_probe', 'tcga_mc3', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'panc_ad',
            'do_mva': 'T',
            'do_assign': 'T',
            'details': 'tested'
        },
        'Prostate': {
            'cancer_type': 'Prostate Cancer',
            'models': ['wgs', 'seqcap', 'seqcap_probe', 'tcga_mc3', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'prost',
            'do_mva': 'T',
            'do_assign': 'T',
            'details': 'tested'
        },
        'Stomach': {
            'cancer_type': 'Stomach Adenocarcinoma',
            'models': ['wgs', 'seqcap', 'seqcap_probe', 'tcga_mc3', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'stomach',
            'do_mva': 'T',
            'do_assign': 'T',
            'details': 'tested'
        },
        'Uterus': {
            'cancer_type': 'Uterine Corpus Endometrial Carcinoma',
            'models': ['wgs', 'seqcap', 'seqcap_probe', 'tcga_mc3', 'msk'],
            'model_to_use': 'msk',
            'tumor_type': 'uterus',
            'do_mva': 'T',
            'do_assign': 'T',
            'details': 'tested'
        },
    }


def get_tmb_df_for_sigma_validation_samples():
    file = "lab_validation_120/internal_data_sources/oc395/primer_concentration_boost_for_sigma/all_samples_tmb_values.txt"
    return pd.read_csv(file, sep="\t")

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Prepare CIDER VCF files for sigMA')

    parser.add_argument('--cider_vcf_directory',
                        dest='cider_vcf_directory',
                        type=str,
                        help='Directory where to find the VCF files from CIDER pipeline')

    parser.add_argument('--final_vcf_directory',
                        dest='final_vcf_directory',
                        type=str,
                        required=True,
                        help='Directory where print the sigMA VCF files')

    parser.add_argument('--consequential_impacts_only',
                        dest='consequential_impacts_only',
                        action='store_true',
                        help='Only consider consequential impacts when filtering the VCF file',
                        default=False)

    parser.add_argument('--filter_germline_variants',
                        dest='filter_germline_variants',
                        action='store_true',
                        help='Filter out germline variants when filtering the VCF file',
                        default=False)

    parser.add_argument('--filter_indel_variants',
                        dest='filter_indel_variants',
                        action='store_true',
                        help='Filter out all indels when filtering the VCF file, i.e. only use SNVs',
                        default=False)

    parser.add_argument('--remove_final_output',
                        dest='remove_final_output',
                        action='store_true',
                        help='Remove the final filtered VCF files after running',
                        default=False)

    parser.add_argument('--vaf_threshold', dest='vaf_threshold',
                        type=float, help='Variant allele fraction threshold, e.g. 5.0 = 5.0 Percentage',
                        required=True, default=None)

    parser.add_argument('--depth_threshold', dest='depth_threshold',
                        type=int, help='Depth of coverage threshold',
                        required=True, default=None)

    parser.add_argument('--sigma_output_file_name', dest='sigma_output_file_name',
                        type=str, help='The SigMa output file(s)',
                        required=True, default=None)

    return parser.parse_args()


if __name__ == "__main__":
    main()