"""
Module used to prepare the CIDER vcf files and the run with sigMA
This module did not take into account the tumor type, and was used to develop the code
for processing the VCF files (including filtering) and then called SigMA (R program)
"""
import argparse
import os
import shutil
from typing import List, Tuple
import pandas as pd

from biomodels.models.vcf import vcf_container as vc
from tools.sigma.sigma_utils import run_sigma_on_vcfs, get_project_file_name, print_filtered_sigma_vcf_file, \
    filter_vc_container
from tools.utils.tools_utils import open_directory_get_list_files_recursively
from tools.tmb.utils.tmb_utils import get_lims_data
from lab_validation_120.utils.pandas_dataframes_utilities import print_data_frame, get_cid_from_file_name


def main():
    """Business Logic"""
    args = get_cli_args()
    # get the CLI options
    cider_vcf_directory = args.cider_vcf_directory
    depth_threshold = args.depth_threshold
    vaf_threshold = args.vaf_threshold
    consequential_impacts_only = args.consequential_impacts_only
    sigma_output_file_name = args.sigma_output_file_name
    filter_germline_variants = args.filter_germline_variants

    # update the final directory based on consequential impacts
    final_vcf_directory = "_".join((args.final_vcf_directory.rstrip("/"),
                                    "consequential_impacts_only",
                                    str(consequential_impacts_only).lower(),
                                    "filter_germline_variants",
                                    str(filter_germline_variants).lower()))

    # just create a final output directory if it doesn't exist
    if os.path.exists(final_vcf_directory) is False:
        os.mkdir(final_vcf_directory)

    # get the list of VCF files to process
    list_vcf_files = open_directory_get_list_files_recursively(directory=cider_vcf_directory)

    # Go over the list of VCF files, generate VC containers via Biomodels, and then filter accordingly
    # and print out the new VCF files
    final_vcf_files, filter_df = process_vcf_files(list_vcf_files=list_vcf_files,
                                                       final_vcf_directory=final_vcf_directory,
                                                       vaf_threshold=vaf_threshold,
                                                       depth_threshold=depth_threshold,
                                                       consequential_impacts_only=consequential_impacts_only,
                                                       filter_germline_variants=filter_germline_variants)


    # Call SigMA and get a data frame from the lite output
    sigma_lite_df = run_sigma_on_vcfs(input_dir=final_vcf_directory,
                                      consequential_impacts_only=consequential_impacts_only,
                                      vaf_threshold=vaf_threshold,
                                      depth_threshold=depth_threshold,
                                      filter_germline_variants=filter_germline_variants,
                                      sigma_output_file_name=sigma_output_file_name)
    # get the lims data
    lims_df = get_lims_data(assay_name='gs395', get_pon_data=False)
    # merge the clinical data with the sigma data
    final_df = pd.merge(sigma_lite_df, lims_df, how='left', on='cid')
    final_df = pd.merge(final_df, filter_df, how='left', on='cid')
    # print out the final data frame
    print_data_frame(df=final_df,
                     tabular_output_file=get_project_file_name(base="final",
                                                               vaf_threshold=vaf_threshold,
                                                               depth_threshold=depth_threshold,
                                                               consequential_impacts_only=consequential_impacts_only,
                                                               filter_germline_variants=filter_germline_variants))

    print(f"Analyzed {len(final_vcf_files)} VCF files")
    print(f"Final dataframe is {len(final_df)} rows")

    if args.remove_final_output is True:
        shutil.rmtree(final_vcf_directory)


def process_vcf_files(list_vcf_files: List[str],
                      final_vcf_directory: str,
                      vaf_threshold: int,
                      depth_threshold: int,
                      consequential_impacts_only: bool,
                      filter_germline_variants: bool,
                      ) -> Tuple[List[str], pd.DataFrame]:
    """
    # Go over the list of VCF files, generate VC containers via Biomodels, and then filter accordingly and print out
    # the new VCF files
    :param list_vcf_files:  List of VCF files to process for sigMA
    :param final_vcf_directory: Where to store the updated VCF file
    :param vaf_threshold: VAF threshold that was used for filtering
    :param depth_threshold: Depth threshold that was used for filtering
    :param consequential_impacts_only:  Filter only consequential impact?
    :param filter_germline_variants:  Filter out germline variants?
    :return: NoneType
    """
    # go over an process each VCF file
    updated_vcf_files = []
    filter_data = []  # store the overall  filtering data, i.e. how many calls were made by HaplotypeCaller,
    # how many were SNV were filtered, and how many INDEL were filtered
    variants_associated_with_hrd = []
    for i, vcf_file in enumerate(list_vcf_files):
        print(f"process vcf file {vcf_file}")
        if i > 100000000:
            break
        vc_container = vc.VcfContainer(vcf_file)
        cid = get_cid_from_file_name(vcf_file)
        # get a list of VcfEntry objects passing filtering
        variant_entry_objects, filter_df = variants_associated_with_hrd \
                filter_vc_container(cid=cid,
                                    vc_container=vc_container,
                                    consequential_impacts_only=consequential_impacts_only,
                                    vaf_threshold=vaf_threshold,
                                    depth_threshold=depth_threshold,
                                    filter_germline_variants=filter_germline_variants)

        # print out a new VCF file for sigMA
        sigma_filtered_vcf_file = get_project_file_name(base="sigma_filtered",
                                                    vaf_threshold=vaf_threshold,
                                                    depth_threshold=depth_threshold,
                                                    consequential_impacts_only=consequential_impacts_only,
                                                    filter_germline_variants=filter_germline_variants,
                                                    ext='vcf')
        # print out the new updated VCF file
        updated_vcf_file = print_filtered_sigma_vcf_file(variant_entry_objects=variant_entry_objects,
                                                         original_vcf_file=vcf_file,
                                                         final_vcf_directory=final_vcf_directory,
                                                         sigma_filtered_file=sigma_filtered_vcf_file)
        # print out some data on filtering statistics
        print(f"found {len(vc_container.dictionary_vcf_entries.keys())} originally, "
              f"after filtering found {len(variant_entry_objects)} "
              f"for {vcf_file}")
        updated_vcf_files.append(updated_vcf_file)
        filter_data.append(filter_df)
    filter_df = pd.concat(filter_data).reset_index(drop=True)
    return updated_vcf_files, filter_df


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Prepare CIDER VCF files for sigMA')

    parser.add_argument('--cider_vcf_directory',
                        dest='cider_vcf_directory',
                        type=str,
                        help='Directory where to find the VCF files from CIDER pipeline')

    parser.add_argument('--final_vcf_directory',
                        dest='final_vcf_directory',
                        type=str,
                        required=True,
                        help='Directory where print the sigMA VCF files')

    parser.add_argument('--consequential_impacts_only',
                        dest='consequential_impacts_only',
                        action='store_true',
                        help='Only consider consequential impacts when filtering the VCF file',
                        default=False)

    parser.add_argument('--filter_germline_variants',
                        dest='filter_germline_variants',
                        action='store_true',
                        help='Filter out germline variants when filtering the VCF file',
                        default=False)

    parser.add_argument('--remove_final_output',
                        dest='remove_final_output',
                        action='store_true',
                        help='Remove the final filtered VCF files after running',
                        default=False)

    parser.add_argument('--vaf_threshold', dest='vaf_threshold',
                        type=float, help='Variant allele fraction threshold, e.g. 5.0 = 5.0 Percentage',
                        required=True, default=None)

    parser.add_argument('--depth_threshold', dest='depth_threshold',
                        type=int, help='Depth of coverage threshold',
                        required=True, default=None)

    parser.add_argument('--sigma_output_file_name', dest='sigma_output_file_name',
                        type=str, help='The SigMa output file(s)',
                        required=True, default=None)

    return parser.parse_args()


if __name__ == "__main__":
    main()
