import argparse
import sys

import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
import seaborn as sns
from typing import Tuple

from lab_validation_120.utils.genomic_metadata_utilities import get_df_bed_intervals_by_assay, \
    variant_position_overlapped_bed_file
from lab_validation_120.utils.plots.plot_utilities import get_subplot_bounding_box_parameters

from tools.sigma.sigma_utils import run_sigma_on_vcfs
from tools.utils.tools_utils import test_absolute_path
from tools.utils.sensitivity_specificity_confidence_intervals import \
    sensitivity_and_specificity_with_confidence_intervals

def main() -> None:
    args = get_cli_args()
    updated_vcf_directory = args.vcf_out_directory
    # where to store the output from Sigma
    sigma_output_file_name = args.sigma_output_file_name
    # dev version is a smaller set for development
    dev_version = args.dev_version
    # padding to add when finding overlap to the Breast cancer variants and the GS395 interval
    interval_padding = args.interval_padding
    # The minium number of variants a sample can have in order to be analyzed and included for Sn/Sp analysis
    min_num_variants_per_sample = args.min_num_variants_per_sample

    # are the directories absolute
    test_absolute_path(updated_vcf_directory, sigma_output_file_name)
    # two directories will be generated when creating the new VCF files.  One with overlap
    overlap_true_directory = f"{updated_vcf_directory}_overlap_True_dev_version_{dev_version}_" \
                             f"min_num_variants_per_sample_{min_num_variants_per_sample}_" \
                             f"interval_padding_{interval_padding}"
    os.makedirs(overlap_true_directory, exist_ok=True)
    # the other directory w/o overlap
    overlap_false_directory = f"{updated_vcf_directory}_overlap_False_dev_version_{dev_version}_" \
                              f"min_num_variants_per_sample_{min_num_variants_per_sample}_" \
                              f"interval_padding_{interval_padding}"
    os.makedirs(overlap_false_directory, exist_ok=True)
    # test they are absolute
    test_absolute_path(overlap_true_directory, overlap_false_directory)

    print("Generating Updated VCF files....")
    # generate the VCF files and get a df with the number of variants for each file
    num_variants_per_vcf_df = \
        convert_breast_cancer_output_to_vcf(directory_overlap_true=overlap_true_directory,
                                            directory_overlap_false=overlap_false_directory,
                                            dev_version=dev_version,
                                            min_num_variants_per_sample=min_num_variants_per_sample,
                                            interval_padding=interval_padding)
    # get the HRD study data
    patient_data_df = get_data_frame_from_patient_data_table_excel_file()
    sigma_base = os.path.splitext(sigma_output_file_name)[0]
    sigma_base = f"{sigma_base}_min_num_variants_per_sample_{min_num_variants_per_sample}_" \
                 f"interval_padding_{interval_padding}"
    final_dfs = []
    # go over the overlap and the all variants df
    for vcf_directory_for_sigma, overlap in ((overlap_true_directory, True), (overlap_false_directory, False)):
        # go over and print out the ROC analysis and get back a df for final printing
        final_dfs.append(run_sigma_produce_roc_analysis(sigma_base=sigma_base,
                                                        dev_version=dev_version,
                                                        vcf_directory_for_sigma=vcf_directory_for_sigma,
                                                        find_overlap=overlap,
                                                        patient_data_df=patient_data_df,
                                                        num_variants_per_vcf_df=num_variants_per_vcf_df,
                                                        min_num_variants_per_sample=min_num_variants_per_sample,
                                                        interval_padding=interval_padding)
                         )
    # print out the final dataframe
    final_sn_sp_df = pd.concat(final_dfs).reset_index(drop=True)
    final_sn_sp_df.to_csv(f"final_sn_sp_min_num_variants_per_sample_{min_num_variants_per_sample}_"
                          f"interval_padding_{interval_padding}.txt", sep="\t")

def run_sigma_produce_roc_analysis(sigma_base: str,
                                   dev_version: bool,
                                   vcf_directory_for_sigma: str,
                                   find_overlap: bool,
                                   patient_data_df: pd.DataFrame,
                                   num_variants_per_vcf_df: pd.DataFrame,
                                   min_num_variants_per_sample: int,
                                   interval_padding: int) -> pd.DataFrame:
    """
    Run SigMA on the non-overlap and overlap data, and complete a ROC analysis
    :param sigma_base: This will be used to generate SigMA output files
    :param dev_version: boolean on whether to run the smaller dev version
    :param vcf_directory_for_sigma: Directory of VCF files to run Sigma on
    :param find_overlap: boolean on whether or not overlap was found
    :param patient_data_df: Patient data frome from the study
    :param num_variants_per_vcf_df: dataframe with the vcf file and the number of variants that were analayzed based on
    the overlap to the assay
    :param min_num_variants_per_sample: Minium number of variants >= to print to VCF files
    :param interval_padding:  What padding to add to our BED intervals in order to find overlap with the Breast Cancer
    Study
    :return: sn / sp dataframe
    """
    # two types of output will be created.  One with overlap, one w/p
    sigma_file = f"{sigma_base}_overlap_{find_overlap}_dev_version_{dev_version}.txt"
    find_overlap_str = f"overlap_{find_overlap}"

    final_sn_sp_value = []  # a list of the sn and sp values of the model
    # call sigMA, get the SigMA df, combine with the breast cancer data, and store for final ROC plotting

    # Run sigma on the directory passed in
    sigma_lite_df = run_sigma_on_vcfs(input_dir=vcf_directory_for_sigma,
                                      sigma_output_file_name=sigma_file,
                                      is_cid_samples=False,
                                      run_sigma_cmd=True)
    # updated the colum so it can be matched with the patient_data_df
    sigma_lite_df['TumorAssay'] = sigma_lite_df.apply(lambda row: get_sample_column(row), axis=1)
    hrd_detect_threshold = 0.7
    for sigma_signature in get_sigma_signature3():  # we will produce ROC for multiple signatures
        for sigma_thresh in np.arange(0, 1.02, 0.02):  # the values for each sig are 0 - 1
            sigma_thresh = round(sigma_thresh, 2)  # need to round b/c of the np rounding
            # merge the patient data and the sigma lite dfs
            final_df = pd.merge(sigma_lite_df, patient_data_df, on="TumorAssay")
            # add a column so we can merge directly below
            final_df['find_overlap'] = find_overlap
            # merge with data about the number of variants, this will also contain rows where Sigma was not called
            # due to not enough variants in the VCF file
            del final_df['tumor']  # remove these columns b/c it will be added back by the merge below
            final_df = pd.merge(
                final_df,
                num_variants_per_vcf_df.loc[num_variants_per_vcf_df['find_overlap'] == find_overlap],
                how="outer", on=('TumorAssay', 'find_overlap'))
            del final_df['HRDetect.prob']  # remove these columns b/c it will be added back by the merge below
            # Add in the patient data (HRD score) for those rows where SigMA was not run
            final_df = pd.merge(final_df, patient_data_df, on="TumorAssay")
            # Need to get the correlations data so se can calculate TP, FP, TN, and FN in get_diagnostic_values
            final_df['correlation'] = \
                final_df.apply(lambda row: get_correlation_column(row,
                                                                  sigma_threshold=sigma_thresh,
                                                                  sigma_signature=sigma_signature,
                                                                  hrd_detect_threshold=hrd_detect_threshold),
                               axis=1)
            # get the TP, FP, TN, and FN
            diagnostic_vals = get_diagnostic_values(df=final_df)
            # create a sn and sp data frame
            sn_sp_df = pd.DataFrame(data=[list(diagnostic_vals)],
                                    columns=[
                                        'tp', 'fp', 'tn', 'fn',
                                        'sensitivity', 'specificity', 'fdr', 'fpr',
                                        'sensitivity_confidence_interval', 'specificity_confidence_interval'])
            # add these values to the df
            sn_sp_df['sigma_threshold'] = sigma_thresh
            sn_sp_df['overlap'] = find_overlap_str
            sn_sp_df['signature'] = sigma_signature
            # store them all for later so they can be merged and then printed
            final_sn_sp_value.append(sn_sp_df)
            final_df.to_csv(f"{os.path.splitext(sigma_file)[0]}_merged_sigma_thres_{sigma_thresh}_"
                            f"sig_{sigma_signature}.txt", sep="\t")

    # plot the data using the 2-D arrays of tpr and fpr for each signature
    plot_roc_curve(final_sn_sp_value=final_sn_sp_value, overlap=find_overlap_str, num_samples=len(final_df),
                   min_num_variants_per_sample=min_num_variants_per_sample, interval_padding=interval_padding)
    # print out the final data
    final_sn_sp_df = pd.concat(final_sn_sp_value).reset_index(drop=True)
    return final_sn_sp_df


def get_sigma_signature3():
    """Simple function to return the Signature 3 sig we are want to compare"""
    return ['Signature_3_ml', 'Signature_3_mva', 'Signature_3_l_rat']


def plot_roc_curve(final_sn_sp_value: list, overlap: str, num_samples: int, min_num_variants_per_sample: int,
                   interval_padding: int) -> None:
    """
    Plot the ROC
    :param final_sn_sp_value: list of all values needed to produce tpr and fpr for a given signature
    :param overlap: Just a string on whether this was the BED overlap VCF generated data
    :param num_samples: How man samples were used
    :param min_num_variants_per_sample: Minium number of variants >= to print to VCF files
    :param interval_padding:  What padding to add to our BED intervals in order to find overlap with the Breast Cancer
    Study
    :return:
    """

    plt.rcParams["figure.figsize"] = (10, 10)
    fig = plt.figure()
    sns.set_theme()
    auc_values = [] # store the auc for the different signatures
    signatures = get_sigma_signature3()  # get the signatures
    # loop over the signatures
    ax = None
    for i, sigma_signature in enumerate(signatures):
        # draw the ROC curve, by getting a list of the tpr and fpr values stored for a given signature and overlap
        temp_df = pd.concat(final_sn_sp_value).reset_index()
        # true positive rate is the same as sensitivity, store in the 2-D array
        tpr_vals = temp_df[(temp_df['overlap'] == overlap) &
                           (temp_df['signature'] == sigma_signature)]['sensitivity'].to_list()
        # false positive rate is 1 - specificity, store in the 2-D array
        fpr_vals = [1 - rate for rate in
                    temp_df[(temp_df['overlap'] == overlap) &
                            (temp_df['signature'] == sigma_signature)]['specificity'].to_list()]

        auc_values.append(auc(fpr_vals, tpr_vals))  # store each AUC for the legend

        seaborn_df = pd.DataFrame({'fpr': fpr_vals, 'tpr': tpr_vals})  # get df for plotting
        # do the plotting
        ax = sns.lineplot(seaborn_df, x='fpr', y='tpr', errorbar=None, marker='o',
                          markersize=7, estimator=None)

    ax.plot([0, 1], [0, 1], 'k--')  # draw the line for random predictions curve
    # Some plot formatting to extend out the x and y axis
    ax.set_xlim([-0.01, 1.01])
    ax.set_xlim([-0.01, 1.01])
    # increase tick marks
    ax.set_xticks(np.arange(0, 1.01, 0.05))
    ax.set_yticks(np.arange(0, 1.01, 0.05))
    ax.set_xlabel('False Positive Rate or (1 - Specificity)')
    ax.set_ylabel('True Positive Rate or (Sensitivity)')
    ax.set_title(f'ROC (Padding {interval_padding} | ({min_num_variants_per_sample} min. # variants per sample | '
                 f'{num_samples} samples)')
    # update the legend
    plt.legend(labels=(f"AUC {signatures[0]}= {auc_values[0]:0.2f}",
                       f"AUC {signatures[1]}= {auc_values[1]:0.2f}",
                       f"AUC {signatures[2]}= {auc_values[2]:0.2f}",
                       "Random"), loc="lower right")
    fig.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(f"HRD_ROC_{overlap}_min_num_variants_per_sample_{min_num_variants_per_sample}_"
                f"interval_padding_{interval_padding}.svg")
    plt.close(fig=fig)


def get_diagnostic_values(df: pd.DataFrame, column_name: str = 'correlation') -> Tuple[int, int, int, int,
                                                                                       float, float, float, float,
                                                                                       Tuple[float, float],
                                                                                       Tuple[float, float]]:
    """
    See https://gist.github.com/maidens/29939b3383a5e57935491303cf0d8e0b for:
    sensitivity_and_specificity_with_confidence_intervals
    :param df: Dataframe used to find: tp, fp, fn, tn
    :param column_name: string of the column name to use to find values
    :return: NoneType
    """
    # find the frequency of each: True Positives, False Positives, True
    correlation = (len(df[df[column_name] == value]) for value in ['TP', 'FP', 'TN', 'FN'])
    tp, fp, tn, fn = correlation
    # false discovery rate
    try:
        fdr = fp / (fp + tp)
    except ZeroDivisionError:
        fdr = -1
    try:
        fpr = fp / (fp + tn)
    except ZeroDivisionError:
        fpr = -1
    # get the values from the statistical package implemented below
    sensitivity, \
        specificity, \
        sensitivity_confidence_interval, \
        specificity_confidence_interval = \
        sensitivity_and_specificity_with_confidence_intervals(tp=tp, fp=fp, fn=fn, tn=tn)

    return tp, fp, tn, fn, \
           sensitivity, specificity, \
           fdr, fpr, \
           sensitivity_confidence_interval, specificity_confidence_interval


def update_rows_with_no_sigma_results(row: pd.Series,
                                      hrd_detect_threshold: float) -> str:
    """
    :param row: pandas series
    :param hrd_detect_threshold: Threshold used to determined breast cancer HRD positive
    :return:
    """
    if row['HRDetect.prob'] > hrd_detect_threshold:  # see comments above about HRDetect
        return 'FN'
    else:
        return 'TN'

def get_correlation_column(row: pd.Series, sigma_threshold: float, sigma_signature: str,
                           hrd_detect_threshold: float) -> str:
    """
    Find the Correlation, i.e., FP (False Positive), FN (False Negative, TN (True Negative), TP (True positive)
    :param row: pandas series
    :param sigma_threshold: value to use to decide
    :param sigma_signature: SigMA signature to use
    :param hrd_detect_threshold: Threshold used to determined breast cancer HRD positive
    :return:
    See https://www.nature.com/articles/s41591-019-0582-4
    To assess the additional benefits of WGS-based stratification, we applied a mutational-signature-based algorithm,
    HRDetect, designed to detect ‘BRCA’ness or homologous-recombination-repair deficiency (HRD), using default
    breast-cancer-specific parameters. More than half of TNBCs (58.6%) were classified as HRDetect-high
    (exceeding a predefined score of 0.7, predictive of BRCA1/BRCA2-deficiency).
    A total of 35.9% were classified as HRDetect-low (score <0.2) and 5.5% fell within an HRD-intermediate category
    (score 0.2–0.7) (Fig. 2a).

    We will not be able to detect HRD intermediate so threshold will be 0.7
    """
    # HRDetect HRD High
    if pd.isnull(row['HRDetect.prob']) and row['number_variants_in_vcf'] < 5:
        return update_rows_with_no_sigma_results(row=row, hrd_detect_threshold=hrd_detect_threshold)
    else:
        if row['HRDetect.prob'] > hrd_detect_threshold:  # see comments above about HRDetect
            if row[sigma_signature] >= sigma_threshold:
                return 'TP'
            else:
                return 'FN'
        else:  # HRD Low
            if row[sigma_signature] >= sigma_threshold:
                return 'FP'
            else:
                return 'TN'

def get_sample_column(row: pd.Series) -> str:
    """
    Get the breast cancer sample name from the file input
    :param row: pd.Series called by the lambda function
    :return:
    """
    # /Users/cleslin/BLI-repo/lab-validation-120/scratch/SigMA/breast_cancer_hrd_study/breast_cancer_study_vcf_overlap_True/PD31030a_overlap_True.txt
    tumor = row['tumor'].split('/')[-1].split("_")[0]
    return tumor


def convert_breast_cancer_output_to_vcf(directory_overlap_true: str,
                                        directory_overlap_false: str,
                                        dev_version: bool,
                                        min_num_variants_per_sample: int,
                                        interval_padding: int) -> pd.DataFrame:
    """
    Go over the breast cancer study and covert the data into VCF files
    One directory of VCF files with variant that overlap the assay's bed file, per sample
    One directory of VCF files with all variants in the variants_pub_df, per sample
    :param directory_overlap_true:
    :param directory_overlap_false:
    :param dev_version: Boolean on whether to run the smaller set for development
    :param min_num_variants_per_sample: Minium number of variants >= to print to VCF files
    :param interval_padding:  What padding to add to our BED intervals in order to find overlap with the Breast Cancer
    Study
    :return: data frame with the number of variants used per data frame

    """

    # get intervals for the assay for overlap analysis of variants
    df_assay_intervals = get_df_bed_intervals_by_assay(which_oc_assay="oc395boosted_primers")
    # I looked at 40-50 positions in a GS395 samples (CID23-10) and noticed we could do this...
    if interval_padding > 0:
        df_assay_intervals['START_ZERO_BASED'] -= interval_padding
        df_assay_intervals['END'] += interval_padding

    # get the list of dictionaries from the BED file
    list_of_bed_interval_dicts = df_assay_intervals.to_dict('records')
    variants_pub_df = get_data_frame_from_variants_pub_file(dev_version=dev_version)
    samples = variants_pub_df['Sample'].unique()  # get the samples from the breast cancer study
    number_variants_in_vcf_list = []
    for i, sample in enumerate(samples, 1):
        sample_df = variants_pub_df[variants_pub_df['Sample'] == sample].copy()
        # loop finding overlap with our assay BED file, and no overlap
        for directory, find_overlap in ((directory_overlap_true, True),(directory_overlap_false, False)):
            file_name = os.path.join(directory, f"{sample}_overlap_{str(find_overlap)}.vcf")
            number_variants_in_vcf = df_to_vcf(file_name=file_name, df=sample_df,
                                               list_of_bed_interval_dicts=list_of_bed_interval_dicts,
                                               find_overlap=find_overlap)
            if number_variants_in_vcf >= min_num_variants_per_sample:
                number_variants_in_vcf_list.append(pd.DataFrame(data=[[file_name, number_variants_in_vcf, find_overlap,
                                                                       sample]],
                                                                columns=['tumor', 'number_variants_in_vcf',
                                                                         'find_overlap', 'TumorAssay']
                                                                ))
        print(f"finished VCF file {i} of {len(samples)}", file=sys.stderr)
    number_variants_in_vcf_df = pd.concat(number_variants_in_vcf_list).reset_index(drop=True)
    return number_variants_in_vcf_df


def df_to_vcf(file_name: str, df: pd.DataFrame, list_of_bed_interval_dicts: list,
              find_overlap: bool = True) -> int:
    """
    Produce the VCF file
    :param file_name: File name to use for the VCF file
    :param df: data frame used to produce the VCF file
    :param list_of_bed_interval_dicts: Dictionary with intervals that will be used for overlaps
    :param find_overlap: Boolean on weather or not to use take into overalp with the intervlas
    :return:
    """
    header = """##fileformat=VCFv4.2
##fileDate=20090805
##source=myTestProgram
##reference=file:///home/CIDer_RESOURCES/hg19/Homo_sapiens_assembly19.fasta
#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO
"""

    #  get the df ready to print
    #  add some values
    df['Id'] = '.'
    df['Info'] = df['VD']  # store some variant annotation data
    df['Pos'] = df['Pos'].astype('int')

    # only keep the rows that have an overlap in our Bed file, set those rows to True
    if find_overlap is True:
        # go over all rows in the df (VCF file) and denote True/False if there was an overlap
        df['Overlap'] = df.apply(lambda row: is_overlap(row=row, list_of_bed_interval_dicts=list_of_bed_interval_dicts), axis=1)
        df = df.loc[df['Overlap'] == True]  # keep only the rows that had Overlap Ture

    if len(df) > 0:  # was there more than one hit in the df (VCF file) that had overalp
        with open(file_name, 'w') as vcf_file:  # print out the header
            print(header, file=vcf_file, end="")
        # get only the VCF columns
        df = df[['Chrom', 'Pos', 'Id', 'Ref', 'Alt', 'Qual', 'Filter', 'Info']]
        df.to_csv(file_name, sep="\t", mode='a', index=False, header=False)  # append
    return len(df)


def is_overlap(row: pd.Series, list_of_bed_interval_dicts: list) -> bool:
    """
    Return True or False if there was overlap in the position passed in via the series
    :param row: pandas series
    :param list_of_bed_interval_dicts: dictionary containing all the positions
    :return: bool
    """
    # use function to dtermine if there was overlap
    if variant_position_overlapped_bed_file(chrom=row['Chrom'],
                                            position=row['Pos'],
                                            list_of_dict_df_bed=list_of_bed_interval_dicts) is True:
        return True
    return False

def get_data_frame_from_variants_pub_file(dev_version: bool = False) -> pd.DataFrame:
    """
    Get the string where the variants are found.  Dev version is smaller can can be run faster
    :param dev_version: Boolean, Dev version is smaller can can be run faster
    :return: pd.DataFrame
    """
    file = "lab_validation_120/external_data_sources/" \
           "Whole-genome-sequencing-of-triple-negative-breast-cancers-a-population-study/" \
           "substitutions/"
    if dev_version is True:
        file += "subs.bulk.1639.1000gFiltered.small_dev_version.pub"
    else:
        file += "subs.bulk.1639.1000gFiltered.pub"
    return pd.read_csv(file, sep='\t', dtype=object)


def get_data_frame_from_patient_data_table_excel_file() -> pd.DataFrame:
    file = 'lab_validation_120/external_data_sources/' \
           'Whole-genome-sequencing-of-triple-negative-breast-cancers-a-population-study/cohort-summary/' \
           'SupplementaryDataTable.xlsx'
    df = pd.read_excel(file, sheet_name='PatientDataTable', usecols=['TumorAssay', 'HRDetect.prob'])
    return df


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Prepare CIDER VCF files for sigMA')

    parser.add_argument('--vcf_out_directory',
                        dest='vcf_out_directory',
                        type=str,
                        required=True,
                        help='Directory where to store the VCF files generated from the study')

    parser.add_argument('--min_num_variants_per_sample',
                        default=5,
                        type=int,
                        help='Only analyze sample with >= to this minium number of variants per sample.  Default is 5 '
                             'since this is the minium number of variants that are required by SigMa to run')

    parser.add_argument('--interval_padding',
                        type=int,
                        required=True,
                        help='Padding to add when finding overlap to the GS395 panel, e.g., 5 would be a padding of 5 '
                             'base pairs')

    parser.add_argument('--sigma_output_file_name', dest='sigma_output_file_name',
                        type=str, help='The SigMa output file(s)',
                        required=True, default=None)

    parser.add_argument('--dev_version',
                        action='store_true',
                        help='Run the smaller set of samples for development....')

    return parser.parse_args()


if __name__ == "__main__":
    main()
