"""Utility module for SigMA"""
import os
import shutil
import sys
from typing import List, Tuple

import pandas as pd
from biomodels.models.vcf import vcf_container as vc
from biomodels.models.vcf.utilities.vcf_utilities import return_most_severe_impact_transcript_obj, \
    is_a_consequential_impact, get_vaf_from_vf_field, ignore_caller_because_of_caller_thresholds
from biomodels.models.vcf.vcf_entry import VcfEntry
from biomodels.models.vcf.metadata.vep import InfoVep

from lab_validation_120.utils.assay_utils import get_root_repo_dir
from lab_validation_120.utils.pandas_dataframes_utilities import read_dataset_and_get_pandas_df, \
    get_columns_from_sigma_lite, get_cid_from_data_frame_row
from tools.utils.system_calls import run_system_cmd_capture_stderr_stdout_with_shell
from tools.utils.gene_lists import get_hrd_genes


def run_sigma_on_vcfs(input_dir: str,
                      sigma_output_file_name: str,
                      consequential_impacts_only: bool = False,
                      filter_germline_variants: bool = False,
                      vaf_threshold: float = 0.0,
                      depth_threshold: int = 0,
                      tumor_type: str = "breast",
                      do_mva: str = 'T',
                      do_assign: str = 'T',
                      model_to_use: str = 'msk',
                      filter_indel_variants: bool = False,
                      is_cid_samples: bool = True,
                      run_sigma_cmd: bool = True) -> pd.DataFrame:
    """
    Call the SigMA program and return the lite results in a pandas data frame
    :param input_dir: The input directroy where the VCF file is located
    :param consequential_impacts_only: Only use consequential impacts
    :param filter_germline_variants:  Filter out germline variants?
    :param vaf_threshold: VAF threshold to use for filtering
    :param depth_threshold: Depth threshold to use for filtering
    :param sigma_output_file_name: Base file name used to name the outputs
    :param tumor_type: Tumor type from SigMA
    :param do_mva: A R Boolean (T or F) boolean for whether multivariate analysis
    :param do_assign: A R Boolean (T or F) when true a cutoff is applied on MVA score
    :param model_to_use: determines the type of sequencing platform
    :param filter_indel_variants: Filter out the indels from the analysis
    :param is_cid_samples: Boolean on if these were cider samples
    :param run_sigma_cmd: Should actually call Sigma, can be turned off for faster development.  Note if false, the
    output from a previous run will be used, and should be there...
    :return: data frame from the SigMA output

    """
    if do_mva == 'F' and do_assign == 'T':
        raise ValueError(f"do_assign cannot be 'T' when do_mva is 'F")
    # Get outputs for the sigma command
    path = os.path.splitext(sigma_output_file_name)[0]
    sigma_lite_output = get_project_file_name(base=f"{path}_lite",
                                              depth_threshold=depth_threshold,
                                              vaf_threshold=vaf_threshold,
                                              consequential_impacts_only=consequential_impacts_only,
                                              filter_germline_variants=filter_germline_variants,
                                              filter_indel_variants=filter_indel_variants,
                                              tumor_type=tumor_type)
    sigma_genome_output = sigma_lite_output.replace("_lite", "_genome")
    sigma_merged_output = sigma_lite_output.replace("_lite", "_merged")

    r_program = "run_sigma.R"  # program to run SigMA

    # copy over to the R project
    src = f"tools/sigMA/{r_program}"  # here in my local repot
    dst = f"/Users/cleslin/BLI-repo/scratch/bioinformatics_software/SigMA/examples/{r_program}"
    shutil.copyfile(src, dst)
    if run_sigma_cmd is True:
        # create the command to run the program
        cmd = f'Rscript examples/{r_program}'
        cmd = command_line(cmd, input_dir=input_dir,
                           sigma_lite_output=sigma_lite_output,
                           sigma_genome_output=sigma_genome_output,
                           sigma_merged_output=sigma_merged_output,
                           tumor_type=tumor_type,
                           do_assign=do_assign,
                           do_mva=do_mva,
                           model_to_use=model_to_use)

        print(f"\nRunning command:\n{cmd}\n")

        # go to the R repo directory before running to get other libraries
        os.chdir('/Users/cleslin/BLI-repo/scratch/bioinformatics_software/SigMA')
        run_system_cmd_capture_stderr_stdout_with_shell(cmd=cmd)

    # get a data frame from the output
    sigma_lite_df = read_dataset_and_get_pandas_df(col_vals=get_columns_from_sigma_lite(tumor_type=tumor_type),
                                                   file_name=sigma_lite_output,
                                                   sep='\t')
    # store the tumor_type used when calling
    sigma_lite_df['tumor_type_used_in_sigma'] = tumor_type

    # go back to the root directory of this project
    os.chdir(get_root_repo_dir())

    if is_cid_samples is True:  # call this with the breast cancer study, so sample will not have CID
        # just creat a column with a cid so it can be joined later
        sigma_lite_df['cid'] = sigma_lite_df.apply(lambda row: get_cid_from_data_frame_row(row, col_name='tumor'),
                                                   axis=1)
        sigma_lite_df['cid'] = sigma_lite_df['cid'].str.lower()

    return sigma_lite_df


def command_line(program_name: str, **kwargs) -> str:
    """
    Function that accepts a variable number of arguments and returns a command line string that can be executed later on
    :param program_name: program name
    :param kwargs: keyword arguments for the options to be passed
    :return: string
    """
    line = ""
    for parname, parvalue in kwargs.items():
        line += "".join((" ", "--", parname, " ", parvalue))
    return program_name + line


def get_project_file_name(base: str,
                          consequential_impacts_only: bool,
                          filter_germline_variants: bool,
                          vaf_threshold: float,
                          depth_threshold: int,
                          tumor_type: str = None,
                          ext: str = "txt",
                          filter_indel_variants: bool = False):
    """
    Return a file name for this project with parameters used from the CLI
    :param base: Base name of the file
    :param consequential_impacts_only: Only use consequential impacts
    :param filter_germline_variants:  Filter out germline variants?
    :param vaf_threshold: VAF threshold to use for filtering
    :param depth_threshold: Depth threshold to use for filtering
    :param ext: Extension for the file, default "txt"
    :param tumor_type: Tumor type used for the analysis
    :param filter_indel_variants: Filter out the indels from the analysis
    :return:
    """
    # use base for all naming
    base = f"{base}_depth_{depth_threshold}_vaf_{vaf_threshold}_" \
           f"consequential_impacts_only_{consequential_impacts_only}_" \
           f"filter_germline_variants_{filter_germline_variants}_" \
           f"filter_indels_{filter_indel_variants}"

    if tumor_type is None:
        return f"{base}.{ext}"
    return f"{base}_sigma_tumor_type_{tumor_type}.{ext}"


def print_filtered_sigma_vcf_file(variant_entry_objects: List[VcfEntry],
                                  original_vcf_file: str,
                                  final_vcf_directory: str,
                                  sigma_filtered_file: str) -> str:
    """
    Print out the updated VCF file for sigMA, which will no contain only variants passing thresholds, and not rows with
    the INFO tag, b/c sigMA doesn't like them
    :param variant_entry_objects: List of VcfEntry objects that passed filtering
    :param original_vcf_file: Original name of the VCF file
    :param final_vcf_directory: Where to store the updated VCF file
    :param sigma_filtered_file: File name to use
    :return: NoneType
    """
    # strip off the directory, and remove the extension
    final_vcf_file = "_".join((os.path.splitext(os.path.split(original_vcf_file)[1])[0], sigma_filtered_file))
    # update to the output
    final_vcf_file = os.path.join(final_vcf_directory, final_vcf_file)
    # go over the original VCF file and only print out header lines w/o ##INFO
    # then go over the list of variant_entry_objects, and print out the original data stored in the input attribute
    with open(original_vcf_file, 'r', encoding='utf8') as in_fh, open(final_vcf_file, 'w', encoding='utf8') as out_fh:
        for line in in_fh:
            # print out the header, but avoid the ##INFO Rows b/c sigMA will not accept
            if line.startswith("#") and not line.startswith('##INFO'):
                print(line.rstrip(), file=out_fh)

        # print out the original lines from the variants that passed
        for variant_entry in variant_entry_objects:
            print(variant_entry.input, file=out_fh)
    return final_vcf_file


def variant_associated_with_hrd(variant_entry_obj: VcfEntry,
                                variant_transcript_obj: InfoVep,
                                variants_associated_with_hrd: list,
                                vaf: float, total_depth: int,
                                oc395_pon_data: dict) -> None:
    """
    Find the variants that are associated with HRD and update an ongoing dictionary of variants and assoicated meta data
    :param variant_entry_obj: Biomodels VcfEntry
    :param variant_transcript_obj:  Biomodels InfoVep
    :param variants_associated_with_hrd: List to be updated
    :param vaf: VAF threshold
    :param total_depth:  Depth threshold
    :param oc395_pon_data:  PoN data
    :return: NoneType
    """
    #print(variant_transcript_obj, "\n\n")
    gene = variant_transcript_obj.SYMBOL
    if gene not in get_hrd_genes():  # we can skip if not in this dictionary
        return None
    var_id = variant_entry_obj.vep_key
    pon_count = oc395_pon_data[var_id]['count'] if var_id in oc395_pon_data else 0
    hgvsc = variant_transcript_obj.HGVSc
    hgvsg = variant_transcript_obj.HGVSg
    hgvsp = None

    if 'regulatory_region_variant' in variant_transcript_obj.Consequence:
        return None
    if 'TF_binding_site_variant' in variant_transcript_obj.Consequence:
        if gene is None:  # this gene is listed as None, so we can return
            return None
        hgvsp = 'TF_binding_site_variant'
    for con_type in ['splice_donor_variant', 'splice_acceptor_variant', 'splice_region_variant',
                     'stop_retained_variant', 'inframe_deletion', 'inframe_insertion', 'stop_lost']:
        if con_type in variant_transcript_obj.Consequence:
            hgvsp = f"{con_type}:{hgvsc}"
    if hgvsp is None:
        hgvsp = variant_transcript_obj.HGVSp  # if we didn't find above...
    # does it pass very lenient
    if vaf > 4.5 and total_depth > 50:
        #print(variant_entry_obj, "\n\n")
        variant = {"gene": gene,
                   "hgvps": hgvsp,
                   "hgvpc": hgvsc,
                   "hgvpg": hgvsg,
                   "vaf": vaf,
                   "total_depth": total_depth,
                   "sift": variant_transcript_obj.SIFT,
                   "polyphen": variant_transcript_obj.PolyPhen,
                   "clinvar": variant_transcript_obj.CLIN_SIG,
                   "impact": variant_transcript_obj.IMPACT,
                   "consequence": variant_transcript_obj.Consequence,
                   "gnomad_af": variant_transcript_obj.gnomAD_AF,
                   "pon_count": pon_count,
                   "cadd_phred": variant_transcript_obj.CADD_PHRED,  # must update Biomodels vep.py line 243
                   "cadd_raw": variant_transcript_obj.CADD_RAW}  # must update Biomodels vep.py line 244
        variants_associated_with_hrd.append(variant)
    return None


def filter_vc_container(cid: str,
                        vc_container: vc,
                        vaf_threshold: float,
                        depth_threshold: int,
                        consequential_impacts_only: bool,
                        filter_germline_variants: bool,
                        oc395_pon_data: dict = None,
                        filter_indel_variants: bool = False) -> Tuple[List[VcfEntry], pd.DataFrame, list]:
    """
    Go over the container of VCF entries and filter them
    :param cid: Cid for the sample
    :param vc_container: Biomodels VCF container
    :param vaf_threshold: VAF threshold to use for filtering
    :param depth_threshold: Depth threshold to use for filtering
    :param consequential_impacts_only:  Filter only consequential impact?
    :param filter_germline_variants: Boolen on whether to filter out germline variants using the developed filtering
    :param filter_indel_variants: Filter out the indels from the analysis
    :param oc395_pon_data: dictionary of counts from the PoN Json
    :return: List of VcfEntry objects passing filtering below
    """
    # pylint: disable=too-many-branches
    vcf_entries: List[VcfEntry] = []
    gatk_called = 0  # all variants called by gatk, before filtering
    gatk_called_snv_filtered = 0  # gatk called snv variants that were filtered
    gatk_called_indel_filtered = 0  # gatk called indels variants that were filtered
    indels_filtered = 0  # number of indels removed before filtering
    gatk_called_variants_passing = 0  # gatk called variants that passed the filtering
    variants_associated_with_hrd = [] # store all variants in certain genes that are associated with HRD
    for key in vc_container.dictionary_vcf_entries.keys():
        variant_entry_obj: VcfEntry = vc_container.get_key(key)
        # get get rid of pindel calls
        if variant_entry_obj is not None and variant_entry_obj.info_internal_2_vcf.PRIMARY_CALLER in {"pindel"}:
            continue

        # get the most severe impact
        variant_transcript_obj = return_most_severe_impact_transcript_obj(variant_entry_obj=variant_entry_obj)

        # was it a consequential variant?
        is_consequential = is_a_consequential_impact(variant_consequence_list=variant_transcript_obj.Consequence)

        # get data on VAF and DEPTH
        try:
            total_depth = variant_entry_obj.info_internal_2_vcf.REF_DP + variant_entry_obj.info_internal_2_vcf.ALT_DP
            vaf = round(get_vaf_from_vf_field(variant_entry_obj=variant_entry_obj) * 100, 3)
        except TypeError:
            print("Issue with variant_entry_obj.info_internal_2_vcf", variant_entry_obj.info_internal_2_vcf,
                  file=sys.stderr)
            print("Entire variant_entry_obj", variant_entry_obj, file=sys.stderr)
            continue

        snv = True  # either an SNV or not
        ignore_call = False
        if len(variant_entry_obj.REF) == 1 and len(variant_entry_obj.ALT) == 1:
            pass
        else:
            snv = False
        # Sigma only uses SNVs
        if filter_indel_variants is True and snv is False:
            indels_filtered += 1
            ignore_call = True

        # only look at consequential transcript impacts?
        if consequential_impacts_only:
            # do not look at non-consequential impact, then skip
            if is_consequential is False:
                ignore_call = True
            # added this additional consequence to avoid 'regulatory_region_variant'
            if 'regulatory_region_variant' in variant_transcript_obj.Consequence:
                ignore_call = True

        # ignore a certain type of call from the analysis b/c caller data suggested it was artifact
        # keep germline calls made by gatk, keep_gatk_calls=True, and filter below
        ignore_call_temp, _ = ignore_caller_because_of_caller_thresholds(variant_entry_obj=variant_entry_obj,
                                                                         variant_transcript_obj=variant_transcript_obj,
                                                                         lofreq_SB=20,
                                                                         lofreq_HRUN=4,
                                                                         keep_gatk_calls=True)
        if ignore_call_temp is True:
            ignore_call = True
        # VAF and depth thresholds
        if vaf < vaf_threshold:
            ignore_call = True
        if total_depth < depth_threshold:
            ignore_call = True

        #if variant_entry_obj.info_gatk.gatk_DETECT == 'Y' and filter_germline_variants is True:
        if filter_germline_variants is True:
            var_id = variant_entry_obj.vep_key
            pon_count = oc395_pon_data[var_id]['count'] if var_id in oc395_pon_data else 0
            if pon_count > 3:
                ignore_call = True

        if variant_entry_obj.info_gatk.gatk_DETECT == 'Y' and filter_germline_variants is True:
            ignore_call = True

        """
        # Primary GATK calls need further processing
        if variant_entry_obj.info_gatk.gatk_DETECT == 'Y' and filter_germline_variants is True:
            # print(f"{variant_entry_obj.CHROM} {variant_entry_obj.POS} ", end='')
            # print(f"{variant_entry_obj.info_internal_2_vcf.REF_DP} {variant_entry_obj.info_internal_2_vcf.ALT_DP}")
            gatk_called += 1
            if snv is True:  # is a SNP
                if is_filterable_gatk_snv_call(variant_entry_obj=variant_entry_obj):
                    gatk_called_snv_filtered += 1
                    ignore_call = True
                else:
                    gatk_called_variants_passing += 1

            else:  # is in INDEL event
                if is_filterable_gatk_indel_call(variant_entry_obj=variant_entry_obj):
                    gatk_called_indel_filtered += 1
                    ignore_call = True
                else:
                    gatk_called_variants_passing += 1
        """
        # Was this a call to ignore?  If so it will not be in the VCF sent to sigMA
        if ignore_call is True:
            continue

        if is_consequential:
            # go over each row and use relaxed thresholds to see if the variant was consequential in a gene of interest
            # update the list variants_associated_with_hrd with variants that could lead to HRD.  This list will be
            # all variants associated with the CID so it can be printed later on...
            variant_associated_with_hrd(variant_entry_obj=variant_entry_obj,
                                        variant_transcript_obj=variant_transcript_obj,
                                        variants_associated_with_hrd=variants_associated_with_hrd,
                                        vaf=vaf,
                                        total_depth=total_depth,
                                        oc395_pon_data=oc395_pon_data)

        # VcfEntry passed, so store for later return
        vcf_entries.append(variant_entry_obj)

    print("variants_associated_with_hrd", variants_associated_with_hrd)
    print(f"Found a total of {gatk_called} GATK called")
    print(f"Found a total of {gatk_called_snv_filtered} GATK called - snv filtered")
    print(f"Found a total of {gatk_called_indel_filtered} GATK called - indel filtered\n")
    print(f"Found a total of {gatk_called_variants_passing} GATK called - passed filtering\n")
    print(f"Found a total of {indels_filtered} indels filtered\n")
    # get a data frame for filtering, e.g.:
    #        cid  gatk_called_Y  gatk_snv_filtered  gatk_indel_filtered  indels_filtered  gatk_called_variants_passing
    # cid21-1743           1024                 89                    0              206                           887
    filtering_df = pd.DataFrame([[cid,
                                  gatk_called,
                                  gatk_called_snv_filtered,
                                  gatk_called_indel_filtered,
                                  indels_filtered,
                                  gatk_called_variants_passing]],
                                columns=["cid",
                                         "gatk_called_Y",
                                         "gatk_snv_filtered",
                                         "gatk_indel_filtered",
                                         "indels_filtered",
                                         "gatk_called_variants_passing"])
    return vcf_entries, filtering_df, variants_associated_with_hrd


def is_filterable_gatk_snv_call(variant_entry_obj: VcfEntry) -> bool:
    """
    Determine parameters for filtering SNPs
    Germline filtering, i.e. when HaplotypeCaller is primary, i.e. GATK
    See: https://gatk.broadinstitute.org/hc/en-us/articles/360035531112--How-to-Filter-variants-either-with-VQSR-or-by-hard-filtering # noqa: E501
    SNPs matching any of these conditions will be considered bad and filtered out
    SNPs that do not match any of these conditions will be considered good and placed in the final output VCF file.
    :param variant_entry_obj: Biomodel VcfEntry object
    :return: Bool
    """
    # pylint: disable=too-many-return-statements
    # This is the variant confidence (from the QUAL field) divided by the unfiltered depth of non-reference samples.
    # when the ALT is almost exclusively found and few REF alleles this can be None
    if variant_entry_obj.info_gatk.gatk_QD is not None and variant_entry_obj.info_gatk.gatk_QD < 2.0:
        return True
    # from the QUAL field
    # when the ALT is almost exclusively found and few REF alleles this can be None
    if variant_entry_obj.info_gatk.gatk_QUAL is not None and variant_entry_obj.info_gatk.gatk_QUAL < 30.0:
        return True
    # Phred-scaled p-value using Fisher’s Exact Test to detect strand bias (the variation being seen on only the forward
    # or only the reverse strand) in the reads. More bias is indicative of false positive calls.
    if variant_entry_obj.info_gatk.gatk_FS > 60:
        return True
    # This is the Root Mean Square of the mapping quality of the reads across all samples.
    if variant_entry_obj.info_gatk.gatk_MQ < 40:
        return True
    # This is the u-based z-approximation from the Mann-Whitney Rank Sum Test for mapping qualities
    # (reads with ref bases vs. those with the alternate allele).
    # Note that the mapping quality rank sum test can not be calculated for sites without a mixture of reads showing
    # both the reference and alternate alleles, i.e. this will only be applied to heterozygous calls.
    if variant_entry_obj.info_gatk.gatk_MQRankSum is not None and variant_entry_obj.info_gatk.gatk_MQRankSum < -12.5:
        return True
    # This is the u-based z-approximation from the Mann-Whitney Rank Sum Test for the distance from the end of the read
    # for reads with the alternate allele. If the alternate allele is only seen near the ends of reads, this is
    # indicative of error. Note that the read position rank sum test can not be calculated for sites without a mixture
    # of reads showing both the reference and alternate alleles, i.e. this will only be applied to heterozygous calls.
    if variant_entry_obj.info_gatk.gatk_ReadPosRankSum is not None \
            and variant_entry_obj.info_gatk.gatk_ReadPosRankSum < -8.0:
        return True
    # The StrandOddsRatio annotation is one of several methods that aims to evaluate whether there is strand bias in
    # the data. Higher values indicate more strand bias.
    if variant_entry_obj.info_gatk.gatk_SOR > 3.0:
        return True
    return False


def is_filterable_gatk_indel_call(variant_entry_obj: VcfEntry) -> bool:
    """
    Determine parameters for filtering INDELs
    Germline filtering, i.e. when HaplotypeCaller is primary, i.e. GATK
    See: https://gatk.broadinstitute.org/hc/en-us/articles/360035531112--How-to-Filter-variants-either-with-VQSR-or-by-hard-filtering # noqa: E501
    INDELs matching any of these conditions will be considered bad and filtered out
    INDELs that do not match any of these conditions will be considered good and placed in the final output VCF file.
    :param variant_entry_obj: Biomodel VcfEntry object
    :return: Bool
    """

    # This is the variant confidence (from the QUAL field) divided by the unfiltered depth of non-reference samples.
    if variant_entry_obj.info_gatk.gatk_QD < 2.0:
        return True
    # from the QUAL field
    if variant_entry_obj.info_gatk.gatk_QUAL < 30.0:
        return True
    # Phred-scaled p-value using Fisher’s Exact Test to detect strand bias (the variation being seen on only the forward
    # or only the reverse strand) in the reads. More bias is indicative of false positive calls.
    if variant_entry_obj.info_gatk.gatk_FS > 200:
        return True
    # This is the u-based z-approximation from the Mann-Whitney Rank Sum Test for the distance from the end of the read
    # for reads with the alternate allele. If the alternate allele is only seen near the ends of reads, this is
    # indicative of error. Note that the read position rank sum test can not be calculated for sites without a mixture
    # of reads showing both the reference and alternate alleles, i.e. this will only be applied to heterozygous calls.
    if variant_entry_obj.info_gatk.gatk_ReadPosRankSum is not None \
            and variant_entry_obj.info_gatk.gatk_ReadPosRankSum < -20.0:
        return True
    # The StrandOddsRatio annotation is one of several methods that aims to evaluate whether there is strand bias in
    # the data. Higher values indicate more strand bias.
    if variant_entry_obj.info_gatk.gatk_SOR > 10.0:
        return True
    return False
