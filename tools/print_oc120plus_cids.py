

data = {
    "concordance_files": {
        "inter_Sample1": [
            {
                # CID20-435
                "sample_id": "1a-cid20-435",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-07-13-oc120-run-b20-60-cid20-435-snapshot/2020-07-13-oc120-run-b20-60-cid20-435-snapshot.B20-60.CID20-435.C503_N703.merged_concatenated.vcf",
            },
            {
                # CID20-1081
                "sample_id": "1b-cid20-1081",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out23-oc120-run6-b20-113-cid20-1081-snapshot/2020out23-oc120-run6-b20-113-cid20-1081-snapshot.B20-113.CID20-1081.C528_N704.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        "inter_Sample2": [
            {
                # CID20-440
                "sample_id": "2a-cid20-440",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-07-13-oc120-run-b20-60-cid20-440-snapshot/2020-07-13-oc120-run-b20-60-cid20-440-snapshot.B20-60.CID20-440.C509_N707.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1082
                "sample_id": "2b-cid20-1082",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out23-oc120-run6-b20-113-cid20-1082-snapshot/2020out23-oc120-run6-b20-113-cid20-1082-snapshot.B20-113.CID20-1082.C529_N705.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        "inter_Sample3": [
            {
                # CID20-594
                "sample_id": "3a-cid20-594",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020ju27-oc120-run53-b20-68-cid20-594-snapshot/2020ju27-oc120-run53-b20-68-cid20-594-snapshot.B20-68.CID20-594.B526_N708.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1083
                "sample_id": "3b-cid20-1083",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out23-oc120-run6-b20-113-cid20-1083-snapshot/2020out23-oc120-run6-b20-113-cid20-1083-snapshot.B20-113.CID20-1083.C530_N706.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }

        ],
        "inter_Sample4": [
            {
                # CID19-4257
                "sample_id": "4a-cid19-4257",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/20201026-cid19-4257/20201026-cid19-4257.B19-15.CID19-4257.B543_N703.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1079
                "sample_id": "4b-cid20-1079",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out23-oc120-run6-b20-113-cid20-1079-snapshot/2020out23-oc120-run6-b20-113-cid20-1079-snapshot.B20-113.CID20-1079.C526_N702.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        # MCC was too low in original, keeping for data provenance
        # "inter_Sample5": [
        #    {
        #        # CID19-516
        #        "sample_id": "5a-cid19-516",
        #        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020jul29-oc120-run5-b20-69-cid20-516-snapshot/2020jul29-oc120-run5-b20-69-cid20-516-snapshot.B20-69.CID20-516.C506_N706.merged_concatenated.vcf",
        #        "variant_type": "snvs"
        #    },
        #    {
        #        # CID20-1098
        #        "sample_id": "5b-cid20-1098",
        #        "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1098-snapshot/2020out29-oc120-run6-b20-117-cid20-1098-snapshot.B20-117.CID20-1098.C501_N701.merged_concatenated.vcf",
        #        "variant_type": "snvs"
        #    },
        #    # must have this last element
        #    {
        #        "experiment_type": "inter-run"
        #    }
        # ],
        "inter_Sample6": [
            {
                # CID19-576
                "sample_id": "6a-cid19-576",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-09-04-oc120-run-b20-88-cid20-576-snapshot/2020-09-04-oc120-run-b20-88-cid20-576-snapshot.B20-88.CID20-576.B514_N706.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1100
                "sample_id": "6b-cid20-1100",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1100-snapshot/2020out29-oc120-run6-b20-117-cid20-1100-snapshot.B20-117.CID20-1100.C503_N703.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        "inter_Sample7": [
            {
                # CID19-334
                "sample_id": "7a-cid19-334",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020jul04-oc120-run5-b20-53-cid20-334-snapshot/2020jul04-oc120-run5-b20-53-cid20-334-snapshot.B20-53.CID20-334.C513_N705.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1102
                "sample_id": "7b-cid20-1102",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1102-snapshot/2020out29-oc120-run6-b20-117-cid20-1102-snapshot.B20-117.CID20-1102.C505_N705.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        "inter_Sample8": [
            {
                # CID19-640
                "sample_id": "8a-cid19-640",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-09-03-oc120-run-b20-86-cid20-640-snapshot/2020-09-03-oc120-run-b20-86-cid20-640-snapshot.B20-86.CID20-640.B505_N705.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1103
                "sample_id": "8b-cid20-1103",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1103-snapshot/2020out29-oc120-run6-b20-117-cid20-1103-snapshot.B20-117.CID20-1103.C506_N706.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        # MCC was too low in original, keeping for data provenance
        # "inter_Sample9": [
        #     {
        #         # CID19-632
        #         "sample_id": "9a-cid19-632",
        #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-09-03-oc120-run-b20-86-cid20-632-snapshot/2020-09-03-oc120-run-b20-86-cid20-632-snapshot.B20-86.CID20-632.B510_N702.merged_concatenated.vcf",
        #         "variant_type": "snvs"
        #     },
        #     {
        #         # CID20-1104
        #         "sample_id": "9b-cid20-1104",
        #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1104-snapshot/2020out29-oc120-run6-b20-117-cid20-1104-snapshot.B20-117.CID20-1104.C507_N707.merged_concatenated.vcf",
        #         "variant_type": "snvs"
        #     },
        #     # must have this last element
        #     {
        #         "experiment_type": "inter-run"
        #     }
        # ],
        "inter_Sample10": [
            {
                # CID19-620
                "sample_id": "10a-cid19-620",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020ago28-oc120-run5-b20-84-cid20-620-snapshot/2020ago28-oc120-run5-b20-84-cid20-620-snapshot.B20-84.CID20-620.C513_N705.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1105
                "sample_id": "10b-cid20-1105",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1105-snapshot/2020out29-oc120-run6-b20-117-cid20-1105-snapshot.B20-117.CID20-1105.C508_N708.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        "inter_Sample11": [
            {
                # CID19-692
                "sample_id": "11a-cid19-692",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-09-04-oc120-run-b20-88-cid20-692-snapshot/2020-09-04-oc120-run-b20-88-cid20-692-snapshot.B20-88.CID20-692.B512_N704.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1106
                "sample_id": "11b-cid20-1106",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1106-snapshot/2020out29-oc120-run6-b20-117-cid20-1106-snapshot.B20-117.CID20-1106.C509_N701.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        "inter_Sample12": [
            {
                # CID19-829
                "sample_id": "12a-cid19-829",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020set15-oc120-run5-b20-92-cid20-829-snapshot/2020set15-oc120-run5-b20-92-cid20-829-snapshot.B20-92.CID20-829.C503_N703.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1107
                "sample_id": "12b-cid20-1107",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1107-snapshot/2020out29-oc120-run6-b20-117-cid20-1107-snapshot.B20-117.CID20-1107.C510_N702.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        # MCC was too low in original, keeping for data provenance
        # "inter_Sample13": [
        #     {
        #         # CID19-819
        #         "sample_id": "13a-cid19-819",
        #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020set15-oc120-run5-b20-92-cid20-819-snapshot/2020set15-oc120-run5-b20-92-cid20-819-snapshot.B20-92.CID20-819.C504_N704.merged_concatenated.vcf",
        #         "variant_type": "snvs"
        #     },
        #     {
        #         # CID20-1108
        #         "sample_id": "13b-cid20-1108",
        #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1108-snapshot/2020out29-oc120-run6-b20-117-cid20-1108-snapshot.B20-117.CID20-1108.C511_N703.merged_concatenated.vcf",
        #         "variant_type": "snvs"
        #     },
        #     # must have this last element
        #     {
        #         "experiment_type": "inter-run"
        #     }
        # ],
        # "inter_Sample14": [
        #     {
        #         # CID19-893
        #         "sample_id": "14a-cid19-893",
        #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020set15-oc120-run5-b20-92-cid20-893-snapshot/2020set15-oc120-run5-b20-92-cid20-893-snapshot.B20-92.CID20-893.C507_N707.merged_concatenated.vcf",
        #         "variant_type": "snvs"
        #     },
        #     {
        #         # CID20-1109
        #         "sample_id": "14b-cid20-1109",
        #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1109-snapshot/2020out29-oc120-run6-b20-117-cid20-1109-snapshot.B20-117.CID20-1109.C512_N707.merged_concatenated.vcf",
        #         "variant_type": "snvs"
        #     },
        #     # must have this last element
        #     {
        #         "experiment_type": "inter-run"
        #     }
        # ],
        "inter_Sample15": [
            {
                # CID19-841
                "sample_id": "15a-cid19-841",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020set15-oc120-run5-b20-92-cid20-841-snapshot/2020set15-oc120-run5-b20-92-cid20-841-snapshot.B20-92.CID20-841.C508_N708.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            {
                # CID20-1110
                "sample_id": "15b-cid20-1110",
                "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1110-snapshot/2020out29-oc120-run6-b20-117-cid20-1110-snapshot.B20-117.CID20-1110.C513_N708.merged_concatenated.vcf",
                "variant_type": "snvs"
            },
            # must have this last element
            {
                "experiment_type": "inter-run"
            }
        ],
        # MCC was too low in original, keeping for data provenance
        # "inter_Sample16": [
        #     {
        #         # CID19-843
        #         "sample_id": "16a-cid19-843",
        #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020set15-oc120-run5-b20-94-cid20-843-snapshot/2020set15-oc120-run5-b20-94-cid20-843-snapshot.B20-94.CID20-843.C509_N701.merged_concatenated.vcf",
        #         "variant_type": "snvs"
        #     },
        #     {
        #         # CID20-1111
        #         "sample_id": "16b-cid20-1111",
        #         "vcf_file": "lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out29-oc120-run6-b20-117-cid20-1111-snapshot/2020out29-oc120-run6-b20-117-cid20-1111-snapshot.B20-117.CID20-1111.C514_N706.merged_concatenated.vcf",
        #         "variant_type": "snvs"
        #     },
        #     # must have this last element
        #     {
        #         "experiment_type": "inter-run"
        #     }
        # ],

        #### NEW FILES Created by tools/create_oc120_vs_oc120plus_reproducibility_config_ds.py
        'inter_Sample17': [{'sample_id': '17a-cid19-4524',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2019out10-archer-run-b19-29-cid19-4524-snapshot/2019out10-archer-run-b19-29-cid19-4524-snapshot.B19-29.CID19-4524.C529_N705.merged_concatenated.vcf'},
                           {'sample_id': '17b-cid20-1203',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot/2020nov10-oc120-run6-b20-125-cid20-1203-snapshot.B20-125.CID20-1203.B501_N701.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample18': [{'sample_id': '18a-cid20-541',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020ju27-oc120-run53-b20-68-cid20-541-snapshot/2020ju27-oc120-run53-b20-68-cid20-541-snapshot.B20-68.CID20-541.C512_N702.merged_concatenated.vcf'},
                           {'sample_id': '18b-cid20-1204',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot/2020nov10-oc120-run6-b20-125-cid20-1204-snapshot.B20-125.CID20-1204.B502_N702.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample19': [{'sample_id': '19a-cid20-815',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020ago28-oc120-run5-b20-82-cid20-815-snapshot/2020ago28-oc120-run5-b20-82-cid20-815-snapshot.B20-82.CID20-815.C505_N705.merged_concatenated.vcf'},
                           {'sample_id': '19b-cid20-1213',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot/2020nov10-oc120-run6-b20-125-cid20-1213-snapshot.B20-125.CID20-1213.B511_N703.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample20': [{'sample_id': '20a-cid20-754',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-09-03-oc120-run-b20-86-cid20-754-snapshot/2020-09-03-oc120-run-b20-86-cid20-754-snapshot.B20-86.CID20-754.B507_N707.merged_concatenated.vcf'},
                           {'sample_id': '20b-cid20-1214',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot/2020nov10-oc120-run6-b20-125-cid20-1214-snapshot.B20-125.CID20-1214.B512_N704.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample21': [{'sample_id': '21a-cid20-994',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out13-oc120-run6-b20-107-cid20-994-snapshot/2020out13-oc120-run6-b20-107-cid20-994-snapshot.B20-107.CID20-994.A528_N701.merged_concatenated.vcf'},
                           {'sample_id': '21b-cid20-1215',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot/2020nov10-oc120-run6-b20-125-cid20-1215-snapshot.B20-125.CID20-1215.B513_N705.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample22': [{'sample_id': '22a-cid20-624',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020ago15-run55-oc12-b20-73-cid20-624-snapshot/2020ago15-run55-oc12-b20-73-cid20-624-snapshot.B20-73.CID20-624.C547_N707.merged_concatenated.vcf'},
                           {'sample_id': '22b-cid20-1216',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot/2020nov10-oc120-run6-b20-125-cid20-1216-snapshot.B20-125.CID20-1216.B514_N706.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample23': [{'sample_id': '23a-cid20-1022',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out17-oc120-run6-b20-109-cid20-1022-snapshot/2020out17-oc120-run6-b20-109-cid20-1022-snapshot.B20-109.CID20-1022.B541_N701.merged_concatenated.vcf'},
                           {'sample_id': '23b-cid20-1217',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot/2020nov10-oc120-run6-b20-125-cid20-1217-snapshot.B20-125.CID20-1217.B515_N707.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample24': [{'sample_id': '24a-cid20-395',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-07-13-oc120-run-b20-60-cid20-395-snapshot/2020-07-13-oc120-run-b20-60-cid20-395-snapshot.B20-60.CID20-395.C502_N702.merged_concatenated.vcf'},
                           {'sample_id': '24b-cid20-1206',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot/2020nov10-oc120-run6-b20-125-cid20-1206-snapshot.B20-125.CID20-1206.B503_N703.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample25': [{'sample_id': '25a-cid20-469',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020jul20-oc120-run5-b20-64-cid20-469-snapshot/2020jul20-oc120-run5-b20-64-cid20-469-snapshot.B20-64.CID20-469.C519_N701.merged_concatenated.vcf'},
                           {'sample_id': '25b-cid20-1205',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot/2020nov10-oc120-run6-b20-125-cid20-1205-snapshot.B20-125.CID20-1205.B504_N704.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample26': [{'sample_id': '26a-cid20-481',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020jul29-oc120-run5-b20-69-cid20-481-snapshot/2020jul29-oc120-run5-b20-69-cid20-481-snapshot.B20-69.CID20-481.C508_N708.merged_concatenated.vcf'},
                           {'sample_id': '26b-cid20-1207',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot/2020nov10-oc120-run6-b20-125-cid20-1207-snapshot.B20-125.CID20-1207.B505_N705.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample27': [{'sample_id': '27a-cid20-524',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020jul29-oc120-run5-b20-69-cid20-524-snapshot/2020jul29-oc120-run5-b20-69-cid20-524-snapshot.B20-69.CID20-524.C507_N707.merged_concatenated.vcf'},
                           {'sample_id': '27b-cid20-1208',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot/2020nov10-oc120-run6-b20-125-cid20-1208-snapshot.B20-125.CID20-1208.B506_N706.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample28': [{'sample_id': '28a-cid20-653',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020ago28-oc120-run5-b20-84-cid20-653-snapshot/2020ago28-oc120-run5-b20-84-cid20-653-snapshot.B20-84.CID20-653.C514_N706.merged_concatenated.vcf'},
                           {'sample_id': '28b-cid20-1210',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot/2020nov10-oc120-run6-b20-125-cid20-1210-snapshot.B20-125.CID20-1210.B508_N708.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample29': [{'sample_id': '29a-cid20-657',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-09-03-oc120-run-b20-86-cid20-657-snapshot/2020-09-03-oc120-run-b20-86-cid20-657-snapshot.B20-86.CID20-657.B503_N703.merged_concatenated.vcf'},
                           {'sample_id': '29b-cid20-1211',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot/2020nov10-oc120-run6-b20-125-cid20-1211-snapshot.B20-125.CID20-1211.B509_N701.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample30': [{'sample_id': '30a-cid20-705',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020-09-04-oc120-run-b20-88-cid20-705-snapshot/2020-09-04-oc120-run-b20-88-cid20-705-snapshot.B20-88.CID20-705.B515_N707.merged_concatenated.vcf'},
                           {'sample_id': '30b-cid20-1212',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot/2020nov10-oc120-run6-b20-125-cid20-1212-snapshot.B20-125.CID20-1212.B510_N702.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample31': [{'sample_id': '31a-cid20-1024',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out17-oc120-run6-b20-109-cid20-1024-snapshot/2020out17-oc120-run6-b20-109-cid20-1024-snapshot.B20-109.CID20-1024.B542_N702.merged_concatenated.vcf'},
                           {'sample_id': '31b-cid20-1163',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot/2020nov12-oc120-run6-b20-126-cid20-1163-snapshot.B20-126.CID20-1163.B516_N708.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample32': [{'sample_id': '32a-cid20-936',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out06-oc120-run6-b20-103-cid20-936-snapshot/2020out06-oc120-run6-b20-103-cid20-936-snapshot.B20-103.CID20-936.A501_N701.merged_concatenated.vcf'},
                           {'sample_id': '32b-cid20-1165',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot/2020nov12-oc120-run6-b20-126-cid20-1165-snapshot.B20-126.CID20-1165.B518_N702.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample33': [{'sample_id': '33a-cid20-946',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out06-oc120-run6-b20-103-cid20-946-snapshot/2020out06-oc120-run6-b20-103-cid20-946-snapshot.B20-103.CID20-946.A506_N706.merged_concatenated.vcf'},
                           {'sample_id': '33b-cid20-1168',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot/2020nov12-oc120-run6-b20-126-cid20-1168-snapshot.B20-126.CID20-1168.B521_N705.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample34': [{'sample_id': '34a-cid20-1015',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out17-oc120-run6-b20-109-cid20-1015-snapshot/2020out17-oc120-run6-b20-109-cid20-1015-snapshot.B20-109.CID20-1015.B544_N704.merged_concatenated.vcf'},
                           {'sample_id': '34b-cid20-1172',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot/2020nov12-oc120-run6-b20-126-cid20-1172-snapshot.B20-126.CID20-1172.B525_N701.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample35': [{'sample_id': '35a-cid20-1000',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out09-oc120-run6-b20-105-cid20-1000-snapshot/2020out09-oc120-run6-b20-105-cid20-1000-snapshot.B20-105.CID20-1000.A517_N701.merged_concatenated.vcf'},
                           {'sample_id': '35b-cid20-1174',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot/2020nov12-oc120-run6-b20-126-cid20-1174-snapshot.B20-126.CID20-1174.B527_N703.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample36': [{'sample_id': '36a-cid20-972',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out13-oc120-run6-b20-107-cid20-972-snapshot/2020out13-oc120-run6-b20-107-cid20-972-snapshot.B20-107.CID20-972.A519_N703.merged_concatenated.vcf'},
                           {'sample_id': '36b-cid20-1241',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot/2020nov12-oc120-run6-b20-129-cid20-1241-snapshot.B20-129.CID20-1241.C515_N705.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}],
        'inter_Sample37': [{'sample_id': '37a-cid20-974',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020out13-oc120-run6-b20-107-cid20-974-snapshot/2020out13-oc120-run6-b20-107-cid20-974-snapshot.B20-107.CID20-974.A521_N705.merged_concatenated.vcf'},
                           {'sample_id': '37b-cid20-1240',
                            'variant_type': 'snvs',
                            'vcf_file': 'lab_validation_120/internal_data_sources/equivalency_oc120_vs_oc120plus/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot/2020nov12-oc120-run6-b20-129-cid20-1240-snapshot.B20-129.CID20-1240.C516_N706.merged_concatenated.vcf'},
                           {'experiment_type': 'inter-run'}]
    }
}

# For Ben's spreadsheet
#for key, list_ in data['concordance_files'].items():
#    print(list_[1]['vcf_file'])
import re

for key, list_ in data['concordance_files'].items():
    cid1 = (list_[0]['vcf_file'].split('/')[3])
    cid2 = (list_[1]['vcf_file'].split('/')[3])
    #print(f"{cid1}\t{cid2}")
    match1 = re.search(r'(cid\d+-\d+)', cid1)
    match2 = re.search(r'(cid\d+-\d+)', cid2)
    print(f"{match1.groups()[0]}\t{match2.groups()[0]}")

