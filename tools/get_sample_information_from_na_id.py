"""Get the sample level information from the NA"""
import argparse
import json
from os import path
import re
import sys
from tools.get_cid_results import get_json_result


def main():
    """Business Logic"""
    args = get_cli_args()
    json_dir = 'tools/json_outputs/Test/'
    url_base = "http://api.lims.demo.bostonlighthouse.us/"
    result_type = 'properties/Test'
    headers = {'Accept': 'application/json'}
    # get the dictionary where the the key is the cid, and the value is the nae
    cids_2_nas = get_dictionary_cids_2_naes(file=args.na_2_cids)
    # Download the JSON data from the limsa and store as a JSON file, and get back a list of JSON files to process
    cid_files = download_test_object_by_cid(cid_file=args.cid_file, json_dir=json_dir, url_base=url_base,
                                            result_type=result_type, headers=headers)
    # go over the list and pull out the data that's needed

    json_dir = 'tools/json_outputs/OrderEntry/'
    result_type = 'properties/OrderEntry'
    process_cid_files(cid_files=cid_files, cids_2_nas=cids_2_nas, json_dir=json_dir, url_base=url_base,
                                            result_type=result_type, headers=headers)


def get_dictionary_cids_2_naes(file=None):
    """
    Open the data file from ../tmb/tools/result_na_2_cids_20210508.csv where we queried data from the LIMS
    Looked like this once I saved to csv

    NA19-10,CID19-4177
    NA19-100,"CID19-4259,CID19-4260,CID19-4763,CID19-4764,CID19-4830,CID19-4831,CID20-37"
    NA19-101,CID19-4261

    Take this and store into a dictionary where cid: nae
    @param file: The file to open
    @return: The dictionary where the the key is the cid, and the value is the nae
    """
    # get the dictionary of cid 2 NA's
    cids_2_nas = {}
    with open(file) as in_fh:
        _ = in_fh.readline()  # ignore the header
        for line in in_fh:
            all_elements = line.rstrip().replace('"', '').split(",")  # remove " and then split
            na_id = all_elements.pop(0)  # first element is the NA id
            #  all cids on this line have the same NA
            cids_2_nas.update({cid: na_id for cid in all_elements if cid != ''})

    # Keep for debugging purposes
    # [print(k, v) for k, v in cids_2_nas.items()]
    # print("done")
    return cids_2_nas

def process_poe_file(poe_json_file=None):
    """
    Open the Json file and get the patient information
    @param poe_json_file:  The JSON file to open
    @retrun patient_dict: A dictionary of file patient data
    """
    # open the JSON file and load into data
    with open(poe_json_file) as in_fh:
        poe_data = json.load(in_fh)
    return poe_data


def process_cid_files(cid_files=None, cids_2_nas=None, json_dir=None, url_base=None, result_type=None, headers=None):
    """Go through and print out the data that is needed
    @param cid_files: The list of cid files. cid: cid_file
    @param cids_2_nas: The dictionary where the the key is the cid, and the value is the nae
    @param url_base: Base url to query
    @param result_type: e.g. json
    @param headers: Dictionary of headers to pass
    """
    # print out the header
    print("\t".join(("poe", "first_name", "middle_name", "last_name", "date_of_birth", "gender", "mrn",
                     "new_or_old_cid", "cid", "primary_site", "primary_site_diagnosis", "tumor_percentage",
                     "test_category")))

    final_data = []  # store the final data per CID
    no_match_dict = get_no_match_dict()
    for index, element in enumerate(cid_files):
        # this was a tuple so just get the values needed
        cid, cid_file, new_or_old_cid = element[0], element[1], element[2]
        # open the JSON file and load into data
        with open(cid_file) as in_fh:
            data = json.load(in_fh)

        # get some values that are always there...
        list_tumor_naes = data['tumor_nae']
        test_category = data['test_category']
        poe = data['POE_link']
        # get the POE JSON file
        poe_json_file = download_order_entry_object_by_poe(poe=poe, json_dir=json_dir, url_base=url_base,
                                                           result_type=result_type, headers=headers)
        # get the JSON data structure for the POE
        poe_data = process_poe_file(poe_json_file=poe_json_file)
        # append on the first part of the tuple
        final_data.append((poe, poe_data['first_name'], poe_data['middle_name'], poe_data['last_name'],
                      poe_data['date_of_birth'], poe_data['gender'], poe_data['mrn'], new_or_old_cid))

        # if the list was size 1 then you can just use the data as is...
        if len(list_tumor_naes) == 1:
            derivative = data['tumor_nae'][0]['derivative']
            primary_site_diagnosis = derivative['primary_site_diagnosis']
            primary_site = derivative['primary_site']
            tumor_percentage = derivative['tumor_percentage'] if derivative['tumor_percentage'] is not None else -1
            # add on the second part of tuple
            final_data[index] = final_data[index] + (cid, primary_site, primary_site_diagnosis, tumor_percentage,
                                                     test_category)
        # this was a manually check entry
        elif cid in no_match_dict:
            final_data[index] = final_data[index] + no_match_dict[cid]
        else:  # more complicated logic here, b/c there is more than one tumor_nae
            tumor_nae_vals = []  # store all the entries so we can compare
            cid_2_nas_match = None  # if there was a match in the cid_2_nas just store the index
            for i, tumor_nae in enumerate(list_tumor_naes):
                # store values to use
                na_id = tumor_nae['name']
                derivative = tumor_nae['derivative']
                primary_site_diagnosis = derivative['primary_site_diagnosis']
                primary_site = derivative['primary_site']
                tumor_percentage = derivative['tumor_percentage'] if derivative['tumor_percentage'] is not None else -1

                # add to the list for checking later if needed
                tumor_nae_vals.append((cid, primary_site, primary_site_diagnosis, tumor_percentage, test_category))

                # was there a match here, if so keep the index for use below
                if cid in cids_2_nas and cids_2_nas[cid] == na_id:
                    cid_2_nas_match = i

            # now test what to use in final_data
            if cid_2_nas_match is not None:  # there was a match to the cid_2_nas_match
                # add on the second part of tuple
                final_data[index] = final_data[index] + (tumor_nae_vals[cid_2_nas_match])
            # all elements are the same, so you can use 0
            elif all(elem == tumor_nae_vals[0] for elem in tumor_nae_vals):
                # add on the second part of tuple
                final_data[index] = final_data[index] + (tumor_nae_vals[0])
            # We didn't have a match in the cid_2_nas_match, and the data is different
            else:
                # add on the second part of tuple
                final_data[index] = final_data[index] + (cid, "Unk", "Unk", "-1", test_category)
                print(f"no match here {cid_file}", file=sys.stderr)
                _ = [print(i, file=sys.stderr) for i in tumor_nae_vals]
                print("\n", file=sys.stderr)

    # print the final output
    _ = [print("\t".join([str(ii) for ii in i])) for i in final_data]

def get_no_match_dict():
    """
    There are examples that come from this program when it cannot make a decision and it prints out things like:
    no match here tools/json_outputs/Test//CID21-1146.json
    ('CID21-1146', 'Prostate', 'prostate carcinoma', '80', 'CLINICAL')
    ('CID21-1146', 'Prostate', 'prostate adenocarcinoma', '70', 'CLINICAL')

    The function returns the correct decision where I went through and determined the correct entry in VV
    """
    return {
        'CID21-396': ('CID21-396', 'Uterus', 'endometrial adenocarcinoma', '70', 'VALIDATION'),
        'CID21-635': ('CID21-635', 'Lung', 'lung adenocarcinoma', '30', 'CLINICAL'),
        'CID21-765': ('CID21-765', 'Lung', 'lung adenocarcinoma', '40', 'CLINICAL'),
        'CID21-939': ('CID21-939', 'Colon', 'adenocarcinoma', '60', 'CLINICAL'),
        'CID21-1116': ('CID21-1116', 'Head and Neck', 'head and neck squamous cell carcinoma', '40', 'CLINICAL'),
        'CID21-1146': ('CID21-1146', 'Prostate', 'prostate carcinoma', '80', 'CLINICAL'),
        'CID21-1161': ('CID21-1161', 'Lung', 'mucinous adenocarcinoma', '30', 'CLINICAL'),
        'CID21-1222': ('CID21-1222', 'Breast', 'invasive ductal carcinoma', '80', 'CLINICAL'),
        'CID21-1316': ('CID21-1316', 'Lung', 'adenocarcinoma', '30', 'CLINICAL'),
        'CID21-477': ('CID21-477', 'Other', 'melanoma', '80', 'CLINICAL'),
        'CID21-1772': ('CID21-1772', 'Lung', 'lung adenocarcinoma', '50', 'CLINICAL'),
        'CID21-1076': ('CID21-1076', 'Prostate', 'prostate adenocarcinoma', '70', 'CLINICAL'),
    }

def download_order_entry_object_by_poe(poe=None, json_dir=None, url_base=None, result_type=None, headers=None):
    """
    Get all the cid Test objects
    @param poe: POE to download
    @param json_dir: The json directory where things are stored
    @param url_base: Base url to query
    @param result_type: e.g. json
    @param headers: Dictionary of headers to pass
    @return: A dictionary of patient information
    """
    patient = {}
    poe_json_file = '/'.join((json_dir, '.'.join((poe, 'json'))))
    download_json_file(id=poe, json_file=poe_json_file, url_base=url_base, result_type=result_type,
                       headers=headers)
    return poe_json_file


def download_test_object_by_cid(cid_file=None, json_dir=None, url_base=None, result_type=None, headers=None):
    """
    Get all the cid Test objects
    @param cid_file: the input file
    @param json_dir: The json directory where things are stored
    @param url_base: Base url to query
    @param result_type: e.g. json
    @param headers: Dictionary of headers to pass
    @return: A list of cid json file locations
    """

    cid_files = []  # store a list of tuples of the cid, and cid_file name
    # go over the list of CIDs and get the results object from the LIMS, and then print what's needed in a tab delimited
    with open(cid_file, 'r') as in_fh:
        for line in in_fh:
            # extract the cid and turn into CID
            lines = line.strip("\n").split("\t")
            cid = re.search(r'(cid\d+-\d+)', lines[0])[0].replace('cid', 'CID')
            cid_json_file = '/'.join((json_dir, '.'.join((cid, 'json'))))
            new_or_old_cid = lines[1]
            assert new_or_old_cid in ("NEW", "OLD"), "the value for new_or_old_cid should be 'NEW' or 'OLD, " \
                                                     "did you enter the correct value?"
            cid_files.append((cid, cid_json_file, new_or_old_cid))
            download_json_file(id=cid, json_file=cid_json_file, url_base=url_base, result_type=result_type,
                               headers=headers)
    return cid_files

def download_json_file(id=None, json_file=None, url_base=None, result_type=None, headers=None):
    """
    @param id: the CID or POE
    @param json_file: Where to store the json file
    @param url_base: Base url to query
    @param result_type: e.g. json
    @param headers: Dictionary of headers to pass
    @return: NoneType
    """
    # if it exists not need to download again
    if path.exists(json_file):
        return
    else:
        print(f"Downloading {id}", file=sys.stderr)
        results = get_json_result(id, "".join((url_base, result_type)), headers)
        # store the data as JSON so it can be opened next time and not downloaded
        with open(json_file, 'w') as out_fh:
            print(json.dumps(results, indent=2), file=out_fh)
    return

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Go off and query the LIMS for a CID Test object.  One cid per line in the form " \
               "2021-04-08-oc120-run-b21-45-cid21-472-snapshot"

    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--cid_file', dest='cid_file', required=True,
                        help="List of CIDs to get, one per line in the form "
                        "2021-04-08-oc120-run-b21-45-cid21-472-snapshot")

    parser.add_argument('--na_2_cids', dest='na_2_cids', required=True,
                        help="The list from the following query, that was copied into a text file (input here):\n"
                        "http://lims.demo.bostonlighthouse.us/index.php?title=Special:Ask&q=%5B%5BCategory%3A"
                        "NucleicAcidExtraction%5D%5D&p=format%3Dbroadtable%2Flink%3Dall%2Fheaders%3Dshow%2F"
                        "searchlabel%3D...-20further-20results%2Fclass%3Dsortable-20wikitable-20smwtable&po=%3F"
                        "AccessionNumber%0A&sort=&order=asc&eq=yes&offset=0&limit=2000#search")

    return parser.parse_args()


def json_printer():
    """hard-coded in a test file, so I could implement print_data() w/o calling many times.. leave for now in case
       I need to update print_data()
    """
    file = "CID21-234.json"
    with open(file) as in_fh:
        return json.load(in_fh)


if __name__ == "__main__":
    # results came from http://api.lims.demo.bostonlighthouse.us/properties/Test/$CID
    main()
    # json_data = json_printer()
    # print_data(json_data)
