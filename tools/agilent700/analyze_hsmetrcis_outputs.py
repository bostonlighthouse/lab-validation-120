import argparse
import os.path

import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
from lab_validation_120.utils.plots.plot_utilities import get_subplot_bounding_box_parameters, global_rc_set
from lab_validation_120.utils.configs.lab_validation_config import get_agilent700_interval_annotation_excel, \
    get_agilent_700_interval_100bp_gc_content
from tempfile import NamedTemporaryFile
import pandas as pd
import numpy as np
from typing import Union
from tools.utils.tools_utils import open_directory_get_list_files_recursively, get_cid_from_string
from tools.cnv.utils import get_list_important_cnv_genes_with_oncokb
from sklearn.linear_model import LinearRegression
from statsmodels.nonparametric.smoothers_lowess import lowess
import statsmodels.api as sm


SAMPLE_COL = 'sample'
ALL_SAMPLE_COL = 'all_samples'
INTERVAL_NAME_COL = 'interval_name'
NORMALIZED_COVERAGE_COL = 'normalized_coverage'
PCT_GC_COL = '%gc'
EXON_COL = 'exon'
GENE_COL = 'gene'
ASSAY_TYPE = ''
FILE_TYPE = 'pdf'
LOG10_COVERAGE_COL = 'log10_coverage'
LOG10_NORMALIZED_COVERAGE_COL = 'log10_normalized'

def main():
    """Business logic"""
    args = get_cli_args()
    global ASSAY_TYPE # not a fan of the global, but did not want to pass this arround to functions
    ASSAY_TYPE = args.assay_type
    hsmetrics_directory = args.hsmetrics_directory
    depthofcoverage_directory = args.depthofcoverage_directory

    # get the annotation level data (gene exon transcript)
    agilent_dna_rna_interval_annotation_df = pd.read_excel(get_agilent700_interval_annotation_excel())
    # bed files need to be updated to interval
    agilent_dna_rna_interval_annotation_df[INTERVAL_NAME_COL] = \
        agilent_dna_rna_interval_annotation_df.apply(lambda row: add_interval_name(row=row, update_from_bed=True),
                                                     axis=1)
    # Depth of coverage
    #process_depth_of_coverage(directory=depthofcoverage_directory,
    #                          agilent_dna_rna_interval_annotation_df=agilent_dna_rna_interval_annotation_df)

    ### HSmetrics
    process_hsmetrics_summary(hsmetrics_directory=hsmetrics_directory)

    hsmetrics_per_target_cov_df = process_per_target_coverage(directory=hsmetrics_directory,
                                                              file_glob="*per_target_cov.txt",
                                                              picard_tool='hsmetrics')
    agilent_dna_rna_interval_annotation_df['exon'] = \
        agilent_dna_rna_interval_annotation_df.apply(lambda row: get_exon(row), axis=1)

    hsmetrics_per_target_cov_df = \
        hsmetrics_per_target_cov_df.merge(agilent_dna_rna_interval_annotation_df[[
            INTERVAL_NAME_COL, 'gene', 'ds_data', EXON_COL]],
                                          on=INTERVAL_NAME_COL, how='left')

    hsmetrics_per_target_cov_vs_gc_distribution_per_sample_cnv_gene(df=hsmetrics_per_target_cov_df)

    cov_vs_gc_distribution_per_sample(df=hsmetrics_per_target_cov_df,
                                      yaxis_list=[LOG10_COVERAGE_COL],
                                      xaxis=PCT_GC_COL,
                                      picard_tool='hsmetrics',
                                      sample_column=SAMPLE_COL)

    cnv_gene_df = \
        hsmetrics_per_target_cov_df[hsmetrics_per_target_cov_df['gene'].isin(get_list_important_cnv_genes_with_oncokb())].copy()

    cov_vs_gc_distribution_per_sample(df=cnv_gene_df,
                                      yaxis_list=[LOG10_COVERAGE_COL],
                                      xaxis=PCT_GC_COL,
                                      picard_tool='hsmetrics',
                                      sample_column=SAMPLE_COL)

    #cov_vs_gc_distribution_cnv_genes(df=hsmetrics_per_target_cov_df,
    #                                 picard_tool="hsmetrics", xaxis=PCT_GC_COL,
    #                                 yaxis=LOG10_NORMALIZED_COVERAGE_COL)

    quit()
    plot_heatmap_over_intervals(df=hsmetrics_per_target_cov_df, filter_name="no_padding")

    #hsmetrics_per_target_cov_padded = \
    #    process_hsmetrics_per_target_cov(hsmetrics_directory=hsmetrics_directory,
    #                                     file_glob="*per_target_cov.50bppadded_intervname.txt")

    #hsmetrics_per_target_cov_df.to_excel(f"hsmetrics_per_target_cov_from_{ASSAY_TYPE}.xlsx")
    #hsmetrics_per_target_cov_padded.to_excel(f"hsmetrics_per_target_cov_padded_from_{ASSAY_TYPE}.xlsx")


def process_depth_of_coverage(directory: str, agilent_dna_rna_interval_annotation_df: pd.DataFrame):
    # get the data frame from GATK depth of coverage
    depth_of_coverage_df = process_per_target_coverage(directory=directory,
                                                       file_glob="*sample_interval_summary", sep=',',
                                                       picard_tool='depth_of_coverage')
    # add in the gene name for each of the intervals
    depth_of_coverage_df = \
        depth_of_coverage_df.merge(agilent_dna_rna_interval_annotation_df[[INTERVAL_NAME_COL, 'gene', 'shared_mapping']],
                                   on="shared_mapping", how="left")
    depth_of_coverage_df.to_csv("depth_of_coverage_df.txt", sep="\t")
    #quit()
    # draw out the coverage vs gc figures
    depth_of_coverage_corrected_df = cov_vs_gc_distribution_per_sample(df=depth_of_coverage_df,
                                                                       picard_tool="depth_of_coverage",
                                                                       xaxis=PCT_GC_COL,
                                                                       yaxis_list=[LOG10_COVERAGE_COL],
                                                                       sample_column=SAMPLE_COL)
    # send in these two genes for plotting
    #for gene, chrom, start, end in [('ERBB2', '17', 8108101, 40526001), ('EGFR', '7', 6013001, 92833323)]:
    #    plot_gene_cnv_plot(df=depth_of_coverage_corrected_df, sample_column=SAMPLE_COL, picard_tool="depth_of_coverage",
    #                       chrom=chrom, start=start, end=end, gene=gene)

    #quit()
    cnv_gene_df = \
        depth_of_coverage_df[depth_of_coverage_df['gene'].isin(get_list_important_cnv_genes_with_oncokb())].copy()

    cnv_gene_df[ALL_SAMPLE_COL] = ALL_SAMPLE_COL
    cov_vs_gc_distribution_per_sample(df=cnv_gene_df,
                                      picard_tool="depth_of_coverage",
                                      xaxis=PCT_GC_COL,
                                      yaxis_list=[LOG10_COVERAGE_COL],
                                      sample_column=ALL_SAMPLE_COL)

def get_exon(row: pd.Series) -> str:
    exon = 'n/a'
    ds_data = eval(row['ds_data'])
    for gene, gene_ds in ds_data.items():
        for transcript, dict_ in gene_ds.items():
            if dict_['is_canonical'] == 1:
                exons = dict_[EXON_COL]
                # exons can be size 0 here if the interval is covering a gene in an intron region of a gene,
                if len(exons) == 0:
                    return exon
                return str(exons[0])
    return exon


def get_slope_intercept(x: list, y: list) -> tuple:
    # get the slope and intercept of the regression line
    x = np.array(x).reshape((-1, 1))
    y = np.array(y)
    model = LinearRegression()
    model.fit(x, y)
    slope = model.coef_[0]
    intercept = model.intercept_
    return slope, intercept


def hsmetrics_per_target_cov_vs_gc_distribution_per_sample_cnv_gene(df: pd.DataFrame) -> None:
    """
    :param df:
    :return:
    """
    # Plotting the histograms
    global_rc_set()
    samples = df[SAMPLE_COL].unique()

    xval = "%gc"
    yvals = ["mean_coverage", NORMALIZED_COVERAGE_COL, "read_count"]
    genes = get_list_important_cnv_genes_with_oncokb()
    samples = {sample: get_mcc_from_cid(sample) for sample in samples} # get mcc so I can sort by it
    gene_df_list = []
    point_color = 'red'
    for ii, gene in enumerate(genes):
        plt.rcParams['figure.figsize'] = (len(yvals) * 5, len(samples) * 5)
        fig, ax = plt.subplots(len(samples), 3)
        temp_df_gene = df[df[GENE_COL] == gene]
        for i, cid in enumerate(sorted(samples.items(), key=lambda x: x[1], reverse=True)):
            cid = cid[0]  # was a tuple
            for ii, yval in enumerate(yvals):
                ax_tmp = ax[i][ii]
                print(f"processing cid {cid} gc bias plot gene {gene}")
                temp_df = temp_df_gene[temp_df_gene[SAMPLE_COL] == cid]
                # Re-plotting the scatter plot for the filtered data with the best fit line
                # and showing the Pearson Correlation value
                sns.regplot(data=temp_df, x=xval, y=yval, scatter_kws={'s': 20}, ax=ax_tmp, line_kws={"color": "red"})
                # get the slope and intercept of the regression line
                x = temp_df[xval].to_list()
                y = temp_df[yval].to_list()
                slope, intercept = get_slope_intercept(x, y)

                sample_text = f"{get_primary_site_from_cid(cid)} " \
                              f"{get_diagnosis_from_cid(cid)} " \
                              f"{cid} MCC {get_mcc_from_cid(cid)}"
                gc_bias_text = f"GC% Bias: {yval} vs. {xval}\n"
                ax_tmp.set_title(
                    f"{gc_bias_text}"
                    f"y = {slope:.2f}x + {intercept:.2f}, ({gene} in {point_color})\n"
                    f"{sample_text}", fontsize=8
                )
                ax_tmp.set_xlim(.20, .80)  # set same y-axis
                ax_tmp.set_ylim(0, round(temp_df[yval].max(), 0) + 1)
                df_tmp = pd.DataFrame(data=[[cid, gene, slope, intercept, yval]], columns=["cid", "gene", "slope", "intercept", "metric"])
                gene_df_list.append(df_tmp)

        plt.tight_layout()
        plt.savefig(f"samples_mean_coverage_vs_gc_{ASSAY_TYPE}_{gene}.{FILE_TYPE}")
        plt.close()
        if ii == 22:
            break

    final_df = pd.concat(gene_df_list).reset_index(drop=True)
    final_df.to_excel("hsmetrics_per_target_cov_vs_gc_distribution_per_sample_cnv_gene.xlsx")


def loess_correct_value(xaxis, yaxis, frac=.5):
    smoothed = lowess(yaxis, xaxis, frac=frac)
    corrected = np.interp(xaxis, smoothed[:, 0], smoothed[:, 1])
    #return yaxis - corrected
    # Exponentiate the corrected value of matrices (yaxis - corrected) since values can be negatice without exp
    return 10**(yaxis - corrected)

def return_gc_bias_title(xaxis: str, yaxis: str, cid: str, slope: float, intercept: float,
                         bias_text: str) -> str:
    gc_bias_text = f"GC% {bias_text}: {yaxis} vs. {xaxis}\n"
    sample_text = f"{get_primary_site_from_cid(cid)} " \
                  f"{get_diagnosis_from_cid(cid)} " \
                  f"{cid} MCC {get_mcc_from_cid(cid)}, ERBB2 Status {get_cnv_status(cid)}"
    equation_text = f"y = {slope:.2f}x + {intercept:.2f}\n"
    return f"{gc_bias_text}{equation_text}{sample_text}"


def plot_loess_lines(df: pd.DataFrame, xaxis: str, yaxis: str, lowess_correction_label: str, fracs: list,
                     lowess_linestyle: str, line_width: int, ax: mpl.axes) -> list:
    lowess_vals = []
    for frac, color in fracs:
        # Apply loess smoothing
        loess = sm.nonparametric.lowess(df[yaxis], df[xaxis], frac=frac)
        loess_x = list(zip(*loess))[0]
        loess_y = list(zip(*loess))[1]
        # Overlay LOESS regression line
        label = f"{lowess_correction_label}_{frac}"
        ax.plot(loess_x, loess_y, linewidth=line_width, linestyle=lowess_linestyle, color=color,
                label=label)
        lowess_vals.append([loess_x, loess_y, label, color])
    return lowess_vals


def plot_gene_cnv_plot(df: pd.DataFrame,
                       picard_tool: str,
                       sample_column: str,
                       gene: str,
                       chrom: str, start: int, end: int) -> list:

    # Plotting the histograms
    global_rc_set()
    samples = df[sample_column].unique()  # get the samples
    plt.rcParams['figure.figsize'] = (10, len(samples) * 3)  # create a large subplot based inputs
    fig, ax = plt.subplots(len(samples), 2)

    samples = {sample: get_cnv_status(sample) for sample in samples}  # get mcc so I can sort by it
    for i, cid in enumerate(sorted(samples.items(), key=lambda x: x[1], reverse=False)):
        cid = cid[0]  # was a tuple
        cid_df = df[(df[sample_column] == cid) & (df['chrom'] == chrom) &
                    (df['start'] > start) & (df['start'] < end)]
        cid_df_temp = cid_df[cid_df['gene'] == gene]
        ii = 0
        for value in ["log10_coverage", "log10_loess_coverage"]:
            ax_tmp = ax[i][ii]
            print(f"processing cid {cid} gc bias plot for tool {picard_tool}")
            sns.stripplot(data=cid_df, x='Target', y=value, jitter=True, marker='o', alpha=0.7, ax=ax_tmp, color='red')
            sns.stripplot(data=cid_df_temp, x='Target', y=value, jitter=True, marker='o', alpha=0.7, ax=ax_tmp, color='blue')
            ax_tmp.set_title(f"Chrom {chrom} {start} - {end} CID {cid}, {get_primary_site_from_cid(cid)} "
                             f"{get_diagnosis_from_cid(cid)}\n"
                             f"{gene} CNV {get_cnv_status(cid)} (blue n={len(cid_df_temp)})", fontsize=8)
            ax_tmp.set_ylim(-4, 13)  # set same x-axis
            # Remove x-axis labels
            ax_tmp.set_xticks([])
            ii += 1

    plt.tight_layout()
    plt.savefig(f"samples_{gene}_plot.{FILE_TYPE}")
    plt.close()


def cov_vs_gc_distribution_per_sample(df: pd.DataFrame,
                                      picard_tool: str,
                                      yaxis_list: list,
                                      xaxis: str,
                                      sample_column: str) -> pd.DataFrame:
    """
    :param df: data frame with information needed for this function
    :param picard_tool: Tool used
    :param yaxis_list: This contains the yaxis columns to parse
    :param xaxis: The gc-conent
    :return:
    """
    # Plotting the histograms
    global_rc_set()
    samples = df[sample_column].unique()  # get the samples
    plt.rcParams['figure.figsize'] = (len(yaxis_list) * 15, len(samples)*5)  # create a large subplot based inputs
    fig, ax = plt.subplots(len(samples), len(yaxis_list) * 2)

    if sample_column == ALL_SAMPLE_COL:  # this will create a len(samples) == 1 so ax will not a 2-D list, so just
        # force it to be for the all samples df
        ax_tmp = [[]]
        for i in enumerate(len(yaxis_list)):
            ax_tmp[0].append(ax[i])
        ax = ax_tmp

    samples = {sample: get_mcc_from_cid(sample) for sample in samples} # get mcc so I can sort by it
    # frac value to run in loess and plot, second value is a color eg.. 'k' = black
    fracs = [(0.05, 'b'), (0.1, 'g'), (0.5, 'y'), (0.75, 'k')]
    lowess_correction_label = 'lowess_correction'  # label to add
    best_fit_label = 'best_fit_line'
    lowess_linestyle = '-'
    line_width = 1
    sample_df_list = []
    for i, cid in enumerate(sorted(samples.items(), key=lambda x: x[1], reverse=True)):
        next_time_through = 0
        cid = cid[0]  # was a tuple
        print(f"processing cid {cid} gc bias plot for tool {picard_tool}")
        cid_df = df[df[sample_column] == cid].copy()
        for yaxis in yaxis_list:
            if yaxis == LOG10_COVERAGE_COL:  # this is the log10 of the coverage column
                yaxis_max = 6  # empirically determined
                yaxis_min = 3
                yaxis_max_corrected = 3
                yaxis_min_corrected = -1
                updated_cov_col = 'log10_loess_coverage'
            elif yaxis == LOG10_NORMALIZED_COVERAGE_COL:  # this is the log10 of the normalized coverage column
                yaxis_max = 2  # normalized coverage, # empirically determined
                yaxis_min = -2
                yaxis_max_corrected = 2
                yaxis_min_corrected = -2
                updated_cov_col = 'log10_loess_normalized_coverage'
            elif yaxis == NORMALIZED_COVERAGE_COL:  # this is the normalized coverage column
                yaxis_max = 4  # normalized coverage, # empirically determined
                yaxis_min = -2.0
                yaxis_max_corrected = 8
                yaxis_min_corrected = -4.0
                updated_cov_col = 'normalized_loess_coverage'
            else:
                raise ValueError(f"Unknown yaxis {yaxis}")
            # get the slope and intercept of the regression line
            slope, intercept = get_slope_intercept(cid_df[xaxis].to_list(), cid_df[yaxis].to_list())
            title = return_gc_bias_title(xaxis=xaxis, yaxis=yaxis, cid=cid, slope=slope, intercept=intercept,
                                         bias_text='Biased')
            # First PLOT - KDE
            ax_tmp = ax[i][0 + next_time_through]
            sns.kdeplot(data=cid_df, x=xaxis, y=yaxis, fill=True, cbar=True, ax=ax_tmp)
            sns.regplot(data=cid_df, x=xaxis, y=yaxis, scatter=False,
                        line_kws={"color": "red", "linewidth": line_width}, ax=ax_tmp, label=best_fit_label)
            lowess_vals = plot_loess_lines(df=cid_df, xaxis=xaxis, yaxis=yaxis,
                                           lowess_correction_label=lowess_correction_label,
                                           fracs=fracs, lowess_linestyle=lowess_linestyle, ax=ax_tmp,
                                           line_width=line_width)

            ax_tmp.set_title(f"{title}\nNo Sub-sampling N={len(cid_df)}", fontsize=8)
            ax_tmp.set_xlim(0, 1)  # set same x-axis
            ax_tmp.set_ylim(yaxis_min, yaxis_max)  # set same y-axis
            ax_tmp.legend()

            # Second PLOT - scatter of corrected by loess
            cid_df[updated_cov_col] = loess_correct_value(cid_df[xaxis], cid_df[yaxis], frac=fracs[0][0])
            yaxis = updated_cov_col
            # get the slope and intercept of the regression line
            slope, intercept = get_slope_intercept(cid_df[xaxis].to_list(), cid_df[yaxis].to_list())
            title = return_gc_bias_title(xaxis=xaxis, yaxis=yaxis, cid=cid, slope=slope, intercept=intercept,
                                         bias_text='Bias Corrected')
            ax_tmp = ax[i][1 + next_time_through]
            sns.kdeplot(data=cid_df, x=xaxis, y=yaxis, fill=True, cbar=True, ax=ax_tmp)
            sns.regplot(data=cid_df, x=xaxis, y=yaxis, scatter=False,
                        line_kws={"color": "red", "linewidth": line_width}, ax=ax_tmp, label=best_fit_label)

            lowess_vals = plot_loess_lines(df=cid_df, xaxis=xaxis, yaxis=yaxis,
                                           lowess_correction_label=lowess_correction_label,
                                           fracs=fracs, lowess_linestyle=lowess_linestyle, ax=ax_tmp,
                                           line_width=line_width)

            ax_tmp.set_title(f"{title}\nNo Sub-sampling N={len(cid_df)}", fontsize=8)
            ax_tmp.set_xlim(0, 1)  # set same x-axis
            ax_tmp.set_ylim(yaxis_min_corrected, yaxis_max_corrected)  # set same y-axis
            ax_tmp.legend()
            sample_df_list.append(cid_df)
            # update if there another plot in the yaxis_list
            next_time_through += 2

        #if i == 2:
        #    break
    plt.tight_layout()
    plt.savefig(f"samples_{yaxis}_vs_gc_{ASSAY_TYPE}_{picard_tool}_{sample_column}_exp10_no_mean_{get_cnv_status(cid)}.{FILE_TYPE}")
    plt.close()
    return pd.concat(sample_df_list).reset_index(drop=True)

def cov_vs_gc_distribution_cnv_genes(df: pd.DataFrame, picard_tool: str,
                                     yaxis: str = NORMALIZED_COVERAGE_COL,
                                     xaxis: str = '%gc') -> None:
    """

    :param df:
    :return:
    """
    # Plotting the histograms
    global_rc_set()
    plt.rcParams['figure.figsize'] = (10, 5)
    fig, ax = plt.subplots(1, 2)
    print("Beginning to filter")
    filtered_df = df[df['gene'].isin(get_list_important_cnv_genes_with_oncokb())]
    print("Finished filtering")
    ax_tmp = ax[0]
    sns.regplot(data=filtered_df, x=xaxis, y=yaxis, scatter_kws={'s': 5}, line_kws={"color": "red"}, ax=ax_tmp)
    sns.scatterplot(data=filtered_df, x=xaxis, y=yaxis, s=10, ax=ax_tmp, color='red')

    # get the slope and intercept of the regression line
    slope, intercept = get_slope_intercept(filtered_df[xaxis].to_list(), filtered_df[yaxis].to_list())
    gc_bias_text = f"GC% Bias: {yaxis} vs. {xaxis}\n"
    ax_tmp.set_title(
        f"{gc_bias_text}"
        f"y = {slope:.2f}x + {intercept:.2f}", fontsize=8
    )
    ax_tmp = ax[1]
    sns.kdeplot(data=filtered_df, x=xaxis, y=yaxis, fill=True, cbar=True, ax=ax_tmp)
    ax_tmp.set_xlabel(f'{xaxis}')
    ax_tmp.set_ylabel(f'{yaxis}')
    ax_tmp.set_ylim(0, 5)
    plt.tight_layout()
    plt.savefig(f"{xaxis}_vs_gc_{ASSAY_TYPE}_{picard_tool}.{FILE_TYPE}")
    plt.close()


def plot_heatmap_over_intervals(df: pd.DataFrame, filter_name: str) -> None:
    """
    Plot out the heat maps
    :param df: The hsmetrics per target coverage df
    :param filter_name: Filter name on padding vs no padding
    :return:
    """
    chromosomes = df['chrom'].unique().tolist()
    chromosomes.remove('Y')
    # Plotting the histograms
    global_rc_set()
    plt.rcParams['figure.figsize'] = (30, 240)
    fig, ax = plt.subplots(len(chromosomes), 1)
    num_top_rows = 2  # show top median rows
    final_corr_df = []
    for i, chrom in enumerate(chromosomes):
        ax_tmp = ax[i]
        # only plot chromosome
        chrom_df = df[df['chrom'] == chrom].copy()
        # update the interval name to contain the gene name for visualization
        chrom_df[INTERVAL_NAME_COL] = \
            chrom_df.apply(lambda row: ' '.join((row[INTERVAL_NAME_COL] + "|",  # pipe here so I can split later on
                                                 row['gene'],
                                                 f"exon {row[EXON_COL]}",
                                                 f"\n:gc:{round(row['%gc'], 2)}")), axis=1)
        # need to get a matrix with interval as rows and CID values of mean coverage as columns
        cov_matrix_df = chrom_df.pivot_table(
            index=INTERVAL_NAME_COL,
            columns=SAMPLE_COL,
            values="mean_coverage")

        # add mcc to column names for visualization
        column_name = cov_matrix_df.columns.values
        column_mcc = list(map(get_mcc_from_cid, column_name))
        column_diagnosis = list(map(get_primary_site_from_cid, column_name))
        column_name_mcc = tuple(zip(column_name, column_mcc, column_diagnosis))
        # update column names to new cid + mcc column name
        cov_matrix_df.columns.values[0:] = column_name_mcc
        # sort by the MCC of the tuple that is the column name
        cov_matrix_df = cov_matrix_df[sorted(cov_matrix_df.columns, key=lambda x: (x[2], x[1]),
                                             reverse=True)]
        # rename the columns so they are easier to understand in the heatmap
        cov_matrix_df.columns.values[0:] = ["\n".join((cid, "MCC " + str(mcc), diagnosis))
                                            for cid, mcc, diagnosis in cov_matrix_df.columns.values]

        # filter the data frame based on the median of mean_coverage
        # this is a good metric since we have close to 50 samples, so the median is a good measure of whether the
        # sequencing is working at this intervals
        cov_matrix_df['median'] = cov_matrix_df.median(axis=1)  # add the median column for filtering
        # add the median to the index
        cov_matrix_df.index = \
            cov_matrix_df.apply(lambda row: ' '.join((row.name, f":median MCC:{round(row['median'], 1)}")), axis=1)

        # sort so the lowest intervals (based on the median) are at the bottom of the heatmap
        # sort here b/c I want to add back top couple rows defined by num_top_rows
        cov_matrix_df.sort_values(by=['median'], inplace=True, ascending=False)
        # find the intervals where the median "mean_coverage" is <= to the coverage_threshold
        coverage_threshold = 200
        # filter it down by the coverage_threshold
        cov_matrix_filtered_df = cov_matrix_df[cov_matrix_df['median'] <= coverage_threshold].copy()

        if len(cov_matrix_filtered_df) == 0:  # no filtered data, only top rows, so do not plot
            ax_tmp.set_axis_off()
            ax_tmp.text(0.2, 0.5, f"No intervals with median of mean_coverage <= {coverage_threshold}x "
                                  f"for Chromosome {chrom}",
                        fontsize=30, verticalalignment='center', transform=ax_tmp.transAxes)
            continue

        # write out the title
        len_cov_matrix = len(cov_matrix_df)
        len_cov_matrix_filtered = len(cov_matrix_filtered_df)
        ax_tmp.title.set_text(
            f'Chromosome {chrom} : Original # Intervals {len_cov_matrix}, '
            f'filtered # {len_cov_matrix_filtered}')  # minus because we added that number on

        # add back on the top num_top_rows for visualization
        cov_matrix_filtered_df = pd.concat((cov_matrix_df.head(num_top_rows), cov_matrix_filtered_df))
        final_corr_df.append(cov_matrix_filtered_df)
        # remove b/c it's no longer needed  and would mess the heatmap up
        cov_matrix_filtered_df.drop(columns=['median'], inplace=True)

        # normalize values to threshold (This way actual coverages are still left in the cell value, but extreme values
        # do not skew the overall heatmap coloring
        norm = plt.Normalize(cov_matrix_filtered_df.values.min(), coverage_threshold)

        cmap = plt.get_cmap('jet')  # define the colormap
        cmap_list = [cmap(i) for i in range(cmap.N)]  # extract all colors from the .jet map
        cmap_list[0] = (.5, .5, .5, 1.0)  # force the first color entry to be grey
        # create the new map
        cmap = mpl.colors.LinearSegmentedColormap.from_list('Custom cmap', cmap_list, cmap.N)
        # plot the heatmap
        sns.heatmap(cov_matrix_filtered_df,
                    cmap=cmap,
                    norm=norm,
                    annot=True,  # annotate each cell
                    annot_kws={"size": 150 / np.sqrt(len(cov_matrix_df))},  # way to auto size the text for the values
                    fmt='.3g',  # round the values in each cell
                    ax=ax_tmp)

    plt.tight_layout()
    plt.savefig(f"heatmap_{filter_name}_{ASSAY_TYPE}_chr_all.{FILE_TYPE}")
    df = pd.concat(final_corr_df) # no need to drop index here b/c intervals are unique
    df_temp = pd.DataFrame()
    # add some additional data before printing
    df_temp['interval'] = df.apply(lambda row: row.name.split("|")[0], axis=1)
    df_temp['min'] = df.min(axis=1)
    df_temp['max'] = df.max(axis=1)
    df_temp['mean'] = df.mean(axis=1)
    df_temp['median'] = df.median(axis=1)
    # had to do separately so values of min, or max did not get treated in mean or median
    df = pd.concat([df_temp, df], axis=1)
    df.to_excel("final_correlation_matrix.xlsx")
    plt.close()


def process_per_target_coverage(directory: str, file_glob: str,  picard_tool: str, sep: str = "\t") -> pd.DataFrame:
    """
    Process the directory for CollectHsMetrics per target or DepthOfCoverage
    :param directory: dir where the data is found
    :param file_glob: the files to gather with the glob, e.g. "*per_target_cov.txt"
    :param picard_tool: What tool was used?
    :param sep: seperator for pandas
    :return: NoneType
    """
    list_cov_files = open_directory_get_list_files_recursively(directory=directory,
                                                               file_glob=file_glob)
    list_cov_files = [file for file in list_cov_files if 'b23-134' not in file]  # filter out LOD study
    list_df = []  # store the list of data frames
    #list_cov_files = list_cov_files[0:3]
    for _, filename in enumerate(list_cov_files):
        df = pd.read_csv(filename, sep=sep)
        if picard_tool == 'depth_of_coverage':  # this is from depthofcoverage
            # update the column names, b/c there were CID appended onto them
            df.columns = ["Target", "total_coverage", "average_coverage",
                          "total_cvg", "mean_cvg", "granular_Q1", "granular_median",
                          "granular_Q3", "%_above_15"]
            df[['chrom', 'start-end']] = df['Target'].str.split(':', expand=True)
            df[['start', 'end']] = df['start-end'].str.split('-', expand=True).astype(int)
            df.drop(columns=['start-end'], inplace=True)
            mean_target_coverage = df['total_coverage'].mean()
            df[NORMALIZED_COVERAGE_COL] = df['total_coverage'] / mean_target_coverage

        # Add in these columns
        cid = get_cid_from_string(filename)
        df[SAMPLE_COL] = cid
        df[INTERVAL_NAME_COL] = df.apply(lambda row: add_interval_name(row), axis=1)
        df = df[~df['chrom'].astype(str).str.startswith('Y')]  # remove Y from the analysis

        # if it was depth of coverage, we need to add in the gc
        if picard_tool == 'depth_of_coverage':
            # get the gc content per interval for depthofcoverage.  Note hsmetrics already contains the %gc (PCT_GC_COL)
            gc_content_df = pd.read_csv(get_agilent_700_interval_100bp_gc_content(), sep="\t")
            # update some column names
            gc_content_df.rename(columns={'#1_usercol': 'chrom', '2_usercol': 'start', '3_usercol': 'end',
                                          "7_usercol": "shared_mapping", "9_pct_at": "%at", "10_pct_gc": PCT_GC_COL},
                                 inplace=True)
            # bed file so start + 1
            gc_content_df['start'] = gc_content_df['start'] + 1
            # update gc df with an interval so it can be merged
            gc_content_df[INTERVAL_NAME_COL] = \
                gc_content_df.apply(lambda row: add_interval_name(row, update_from_bed=False), axis=1)
            # merge so I have the gc content in the depth of coverage data frame
            df = df.merge(gc_content_df[[INTERVAL_NAME_COL, PCT_GC_COL, 'shared_mapping']],
                          on=INTERVAL_NAME_COL, how="left")
        # remove 0 coverage b/c it's not helpful
        read_col = 'total_coverage' if picard_tool == 'depth_of_coverage' else 'read_count'
        print(f"size before 0x removal {len(df)} - {read_col}")
        df = df[df[read_col] > 0].copy()  # remove data with 0
        print(f"after before 0x removal {len(df)}- {read_col}")

        df[LOG10_COVERAGE_COL] = np.log10(df[read_col])
        df[LOG10_NORMALIZED_COVERAGE_COL] = np.log10(df[NORMALIZED_COVERAGE_COL])
        """
        read_col = 'total_coverage' if picard_tool == 'depth_of_coverage' else 'read_count'
        # Calculate boundaries for outliers
        # Calculate the upper and lower percentiles
        lower_bound = np.percentile(df[read_col], 0.5)
        upper_bound = np.percentile(df[read_col], 99.5)
        # Filter the DataFrame based on the bounds
        df = df[(df[read_col] >= lower_bound) & (df[read_col] <= upper_bound)]
        df.to_excel("junk2.xlsx")

        # Fit a LOESS curve
        # Create a scatter plot of read coverage vs. GC percentage
        plt.scatter(df[PCT_GC_COL], df[read_col])
        # Fit a LOESS curve to the data
        loess_fit = sm.nonparametric.lowess(df[read_col], df[PCT_GC_COL], frac=.05)
        # Plot the LOESS curve on the scatter plot
        plt.plot(loess_fit[:, 0], loess_fit[:, 1], color='red')

        #df['predicted_cov'] = loess_fit[:, 1]
        #df['correction_factor'] = df[read_col] / df['predicted_cov']
        #df['correction_factor'] = df['predicted_cov'] / df[read_col]
        # Use the LOESS curve to correct the read coverage and GC percentage values
        #df['corrected_reads'] = df['correction_factor'] * df[read_col].mean()
        df['corrected_coverage'] = df[read_col] - loess_fit[:, 1]
        # Set negative corrected_coverage values to a small positive value (or zero)
        df['corrected_coverage'] = df['corrected_coverage'].apply(lambda x: max(x, 1))  # or max(x, 0) for zero
        #plt.scatter(df[PCT_GC_COL], df['corrected_coverage'], color='blue')
        plt.savefig("test4.png")
        """

        list_df.append(df)
    final_df = pd.concat(list_df).reset_index(drop=True)
    return final_df


def process_hsmetrics_summary(hsmetrics_directory: str) -> None:
    """
    Process the directory for HS Metrics summary
    :param hsmetrics_directory: dir where the data is found
    :return: NoneType
    """
    list_hsmetrics_files = open_directory_get_list_files_recursively(directory=hsmetrics_directory,
                                                                     file_glob="*hsmetrics*.txt")
    hsmetrics_summary_df = get_final_picard_summary_metrics_df(input_files=list_hsmetrics_files)

    plot_barchart(df=hsmetrics_summary_df)
    interval_name, interval_padded_name = None, None
    if ASSAY_TYPE == 'dna':
        interval_name = 'A3416642_Regions_hg19lft_intervname'
        interval_padded_name = 'A3416642_Regions_hg19lft_50bppadded_intervname'
    else:
        interval_name = 'hg19_A3416642_Covered_RNA'

    # plot the distributions using the non-padded and padded
    plot_distributions(df=hsmetrics_summary_df, filter_name=interval_name)
    # plot the correlation heat maps using the non-padded and padded
    plot_correlation(df=hsmetrics_summary_df, filter_name=interval_name)
    # plot the regressions using the non-padded
    plot_regressions(df=hsmetrics_summary_df, filter_name=interval_name)

    # re-do them for the padded interval for the DNA assay (RNA did not have a padded interval
    if ASSAY_TYPE == 'dna':
        plot_distributions(df=hsmetrics_summary_df, filter_name=interval_padded_name)
        plot_correlation(df=hsmetrics_summary_df, filter_name=interval_padded_name)
        plot_regressions(df=hsmetrics_summary_df, filter_name=interval_padded_name)

def correlation_matrix_filter(corr_matrix: pd.DataFrame, bound: float) -> pd.Series:
    """
    Take in a correlation matrix and flatten it to a series of pairs that are above/below the bound
    Remember this is a correlation (R2) so there can be negatives numbers
    :param corr_matrix: Correlation matrix to flatten in a Series
    :param bound: the value to filter on
    :return: Series
    """
    filtered_corr_matrix = corr_matrix[((corr_matrix >= bound) | (corr_matrix <= -bound)) & (corr_matrix != 1.000)]
    flattened_matrix = filtered_corr_matrix.unstack().sort_values().drop_duplicates().dropna()
    return flattened_matrix


def plot_regressions(df: pd.DataFrame, filter_name: str) -> None:
    """
    Plot the Regressions in the columns
    :param df: data frame of all samples hsmetrics
    :param filter_name: filter the dataframe with this value
    :return: None
    """
    # Filtering data where BAIT_SET equals the specified value
    temp_df = df[df['BAIT_SET'] == filter_name]

    corr_matrix = temp_df[key_hsmetrics_cols()].corr()
    flat_mat = correlation_matrix_filter(corr_matrix=corr_matrix, bound=.7)
    print(flat_mat)
    regressions = list(flat_mat.index)
    regressions.append(('HS_LIBRARY_SIZE', 'TOTAL_READS'))
    # regressions is a list of tuples that passed the bound above
    # regressions = [
    #   ('PCT_PF_UQ_READS', 'PCT_EXC_DUPE')
    #   ('PCT_EXC_DUPE', 'HS_LIBRARY_SIZE')
    # ..
    # ..
    # ]

    # Plotting the histograms
    global_rc_set()
    plt.rcParams['figure.figsize'] = (8, 70)
    fig, ax = plt.subplots(len(regressions), 1)
    for i, values in enumerate(regressions):
        xval, yval = values
        if yval == 'HS_LIBRARY_SIZE':  # just swap so HS_LIBRARY_SIZE on x-axis
            yval, xval = xval, yval
        ax_tmp = ax[i]
        # Recalculating the Pearson Correlation value for the filtered data
        pearson_corr_filtered = temp_df[xval].corr(temp_df[yval])

        # Re-plotting the scatter plot for the filtered data with the best fit line
        # and showing the Pearson Correlation value
        sns.regplot(data=temp_df, x=xval, y=yval, ci=95, scatter_kws={'s': 50},
                    line_kws={"color": "red"}, ax=ax_tmp)

        ax_tmp.title.set_text(
            f'Regression: {yval} vs. {xval} : Pearson R2: {pearson_corr_filtered:.2f}')

    plt.tight_layout()
    plt.savefig(f"regression_{filter_name}_{ASSAY_TYPE}.{FILE_TYPE}")
    plt.close()

def key_hsmetrics_cols() -> list:
    values = [
        'TOTAL_READS', 'PF_UNIQUE_READS', 'ON_TARGET_BASES',
        'PCT_PF_UQ_READS', 'PCT_SELECTED_BASES', 'PCT_OFF_BAIT',
        'PCT_PF_UQ_READS_ALIGNED', 'MEAN_TARGET_COVERAGE', 'PCT_EXC_DUPE',
        'FOLD_ENRICHMENT', 'ZERO_CVG_TARGETS_PCT', 'FOLD_80_BASE_PENALTY',
        'PCT_TARGET_BASES_50X', 'PCT_TARGET_BASES_100X', 'HS_LIBRARY_SIZE', 'GC_DROPOUT',
        'HS_PENALTY_100X'
    ]
    if ASSAY_TYPE == 'dna':  # only dna has MCC
        values.append('MCC')
    return values


def plot_correlation(df: pd.DataFrame, filter_name: str) -> None:
    """
    Plot the correlation matrix of the columns
    :param df: data frame of all samples hsmetrics
    :param filter_name: filter the dataframe with this value
    :return: None
    """
    # Selecting columns for correlation
    global_rc_set()
    columns_to_plot = key_hsmetrics_cols()
    df_temp = df[df['BAIT_SET'] == filter_name].reset_index(drop=True)
    # Visualizing the correlation matrix using a heatmap
    plt.figure(figsize=(15, 10))
    plt.title("Correlation Heatmap")
    # Calculating correlation matrix
    correlation_matrix = df_temp[columns_to_plot].corr()
    print(correlation_matrix)
    # Generate a mask for the upper triangle
    mask = np.triu(np.ones_like(correlation_matrix, dtype=bool))
    # plot the correlation matrix
    sns.heatmap(correlation_matrix, cmap="coolwarm", annot=True, mask=mask)
    plt.grid(False)
    plt.tight_layout()
    plt.savefig(f"correlation_{filter_name}_{ASSAY_TYPE}.{FILE_TYPE}")
    plt.close()


def plot_distributions(df: pd.DataFrame, filter_name: str) -> None:
    """
    Plot the distributions of the columns
    :param df: data frame of all samples hsmetrics
    :param filter_name: filter the dataframe with this value
    :return: None
    """
    # List of columns to visualize
    columns_to_plot = key_hsmetrics_cols()

    # Plotting the histograms
    global_rc_set()
    plt.rcParams['figure.figsize'] = (10, 40)
    df_temp = df[df['BAIT_SET'] == filter_name].reset_index(drop=True)
    fig, ax = plt.subplots(len(columns_to_plot), 2)
    for i, col in enumerate(columns_to_plot):
        print(col)
        ax_tmp = ax[i][0]
        sns.histplot(df_temp[col], kde=True, color='skyblue', bins=20, ax=ax_tmp)
        ax_tmp.title.set_text(col)
        ax_tmp.set_xlabel(f'Value {col}')
        ax_tmp.set_ylabel('Frequency')

        ax_tmp = ax[i][1]
        sns.boxplot(df_temp[col], color='skyblue', showfliers=True, ax=ax_tmp)
        sns.swarmplot(df_temp[col], color='red', ax=ax_tmp, size=3)

    plt.tight_layout()
    plt.savefig(f"distributions_{filter_name}_{ASSAY_TYPE}.{FILE_TYPE}")
    plt.close()


def plot_barchart(df: pd.DataFrame) -> None:
    global_rc_set()
    plt.rcParams["figure.figsize"] = [15, 10]
    fig, ax = plt.subplots(1, 1)  # rows x cols = Create 4 subplots
    sns.barplot(data=df, x=SAMPLE_COL, y="MEAN_TARGET_COVERAGE")
    ax.set_xlabel("MTC targets and 50bp padding targets")
    plt.xticks(rotation=90)
    ax.set_title("MTC plot")
    plt.tight_layout()
    plt.savefig(f'mtc_{ASSAY_TYPE}.{FILE_TYPE}')
    plt.close()


def hsmetrics_meaning(name: str) -> str:
    values = {
        "BAIT_SET": "Name bait set used in the hybrid selection.",
        "GENOME_SIZE": "# bases in the reference genome",
        "BAIT_TERRITORY": "# bases localized to one or more baits.",
        "TARGET_TERRITORY": "Unique # of target bases in the experiment",
        "BAIT_DESIGN_EFFICIENCY": "Ratio: TARGET_TERRITORY/BAIT_TERRITORY. Value of 1 indicates a perfect design",
        "TOTAL_READS": "# of reads in the BAM file examined.",
        "PF_READS": "# number of reads that pass filter.",
        "PF_UNIQUE_READS": "# PF reads not marked as duplicates.",
        "PCT_PF_READS": "Frac. reads passing the filter, PF_READS/TOTAL_READS.",
        "PCT_PF_UQ_READS": "Frac. PF_UNIQUE_READS/TOTAL_READS.",
        "PF_UQ_READS_ALIGNED": "# PF_UNIQUE_READS aln. to the ref. genome w/ a map score > 0.",
        "PCT_PF_UQ_READS_ALIGNED": "Frac. PF_UQ_READS_ALIGNED from total # of PF reads.",
        "PF_BASES_ALIGNED": "# PF unique bases aln. to the ref. genome with mapping scores > 0.",
        "PF_UQ_BASES_ALIGNED": "# bases in the PF_UQ_READS_ALIGNED reads. Accounts for clipping and gaps.",
        "ON_BAIT_BASES": "# PF_BASES_ALIGNED gmapped to the baited regions of the genome.",
        "NEAR_BAIT_BASES": "# PF_BASES_ALIGNED gmapped to w/in a fixed interval containing a baited region",
        "OFF_BAIT_BASES": "# PF_BASES_ALIGNED gmapped away from any baited region.",
        "ON_TARGET_BASES": "# PF_BASES_ALIGNED gmapped to a targeted region of the genome.",
        "PCT_SELECTED_BASES": "Frac. PF_BASES_ALIGNED located on or near a baited region\n(ON_BAIT_BASES + NEAR_BAIT_BASES)/PF_BASES_ALIGNED.",
        "PCT_OFF_BAIT": "Frac. PF_BASES_ALIGNED gmapped away from any baited region, \nOFF_BAIT_BASES/PF_BASES_ALIGNED.",
        "ON_BAIT_VS_SELECTED": "Frac. bases on or near baits gcovered by baits, \nON_BAIT_BASES/(ON_BAIT_BASES + NEAR_BAIT_BASES).",
        "MEAN_BAIT_COVERAGE": "The mean cov. of all baits in the experiment.",
        "MEAN_TARGET_COVERAGE": "The mean cov. of a target region.",
        "MEDIAN_TARGET_COVERAGE": "The median cov. of a target region.",
        "MAX_TARGET_COVERAGE": "The max cov. of reads that mapped to target regions. ",
        "PCT_USABLE_BASES_ON_BAIT": "# aln., de-duped, on-bait bases out of the PF bases available.",
        "PCT_USABLE_BASES_ON_TARGET": "# aln., de-duped, on-target bases out of all of the PF bases available.",
        "FOLD_ENRICHMENT": "The fold by which the baited region has been amplified above genomic background.",
        "ZERO_CVG_TARGETS_PCT": "Frac. targets that did not reach cov.=1 over any base.",
        "PCT_EXC_DUPE": "Frac. aln. bases filtered b/c they were in reads marked as duplicates.",
        "PCT_EXC_MAPQ": "Frac. aln. bases filtered b/c they were in reads with low mapping quality.",
        "PCT_EXC_BASEQ": "Frac. aln. bases filtered b/c they were of low base quality.",
        "PCT_EXC_OVERLAP": "Frac. aln. bases filtered b/c they were the second observation\nfrom an insert with overlapping reads.",
        "PCT_EXC_OFF_TARGET": "Frac. aln. bases filtered b/c they did not align over a target base.",
        "FOLD_80_BASE_PENALTY": "The fold over-cov. necessary to raise 80% of bases in 'non-zero-cvg' targets \mto the mean cov. level in those targets.",
        "PCT_TARGET_BASES_1X": "Frac. all target bases achieving 1X or greater cov..",
        "PCT_TARGET_BASES_2X": "Frac. all target bases achieving 2X or greater cov..",
        "PCT_TARGET_BASES_10X": "Frac. all target bases achieving 10X or greater cov..",
        "PCT_TARGET_BASES_20X": "Frac. all target bases achieving 20X or greater cov..",
        "PCT_TARGET_BASES_30X": "Frac. all target bases achieving 30X or greater cov..",
        "PCT_TARGET_BASES_40X": "Frac. all target bases achieving 40X or greater cov..",
        "PCT_TARGET_BASES_50X": "Frac. all target bases achieving 50X or greater cov..",
        "PCT_TARGET_BASES_100X": "Frac. all target bases achieving 100X or greater cov..",
        "HS_LIBRARY_SIZE": "The estimated number of unique molecules in the selected part of the library.",
        "HS_PENALTY_10X": "The 'hybrid selection penalty' incurred to get 80% of target bases to 10X. This metric should be interpreted as: if I have a design with 10 megabases of target, and want to get 10X cov. I need to sequence until PF_ALIGNED_BASES = 10^7 * 10 * HS_PENALTY_10X.",
        "HS_PENALTY_20X": "The 'hybrid selection penalty' incurred to get 80% of target bases to 20X. This metric should be interpreted as: if I have a design with 10 megabases of target, and want to get 20X cov. I need to sequence until PF_ALIGNED_BASES = 10^7 * 20 * HS_PENALTY_20X.",
        "HS_PENALTY_30X": "The 'hybrid selection penalty' incurred to get 80% of target bases to 30X. This metric should be interpreted as: if I have a design with 10 megabases of target, and want to get 30X cov. I need to sequence until PF_ALIGNED_BASES = 10^7 * 30 * HS_PENALTY_30X.",
        "HS_PENALTY_40X": "The 'hybrid selection penalty' incurred to get 80% of target bases to 40X. This metric should be interpreted as: if I have a design with 10 megabases of target, and want to get 40X cov. I need to sequence until PF_ALIGNED_BASES = 10^7 * 40 * HS_PENALTY_40X.",
        "HS_PENALTY_50X": "The 'hybrid selection penalty' incurred to get 80% of target bases to 50X. This metric should be interpreted as: if I have a design with 10 megabases of target, and want to get 50X cov. I need to sequence until PF_ALIGNED_BASES = 10^7 * 50 * HS_PENALTY_50X.",
        "HS_PENALTY_100X": "The 'hybrid selection penalty' incurred to get 80% of target bases to 100X. This metric should be interpreted as: if I have a design with 10 megabases of target, and want to get 100X cov. I need to sequence until PF_ALIGNED_BASES = 10^7 * 100 * HS_PENALTY_100X.",
        "AT_DROPOUT": "A measure of how undercovered <= 50% GC regions are relative to the mean. For each GC bin [0..50] we calculate a = % of target territory, and b = % of aln. reads aln. to these targets. AT DROPOUT is then abs(sum(a-b when a-b < 0)). E.g. if the value is 5% this implies that 5% of total reads that should have mapped to GC<=50% regions mapped elsewhere.",
        "GC_DROPOUT": "A measure of how undercovered >= 50% GC regions are relative to the mean. For each GC bin [50..100] we calculate a = % of target territory, and b = % of aln. reads aln. to these targets. GC DROPOUT is then abs(sum(a-b when a-b < 0)). E.g. if the value is 5% this implies that 5% of total reads that should have mapped to GC>=50% regions mapped elsewhere.",
        "HET_SNP_SENSITIVITY": "The theoretical HET SNP sensitivity.",
        "HET_SNP_Q": "The Phred Scaled Q Score of the theoretical HET SNP sensitivity.",
    }
    return values[name]


def add_interval_name(row: pd.Series, update_from_bed: bool = False) -> str:
    """
    Return a key of chrom start end
    :param row: pd.Series
    :param update_from_bed: Bool on whether +1 should be added to the start
    :return: string
    """
    row['chrom'] = row['chrom'].replace('chr', '')  # just update any chr coming in for this key
    # create intervals like: 1:120572529_120572610 so they can be loaded into IGV
    if update_from_bed:
        # BED is -1 so add one to be gatk interval based to map
        return '-'.join((row['chrom'] + ":" + str(row['start'] + 1), str(row['end'])))
    return '-'.join((row['chrom'] + ":" + str(row['start']), str(row['end'])))


def get_final_picard_summary_metrics_df(input_files: list, get_cid: bool=True, metric_type: str= 'hsmetrics') -> pd.DataFrame:
    """
    Get the dataframe from all Summary Picard Metrics outputs (tested with hsmetrics and wgsmetrics)
    :param input_files: List of files to parse
    :param get_cid: pull the cid out for the name column, if not true, then use the file
    :param metric_type: Type of metric to gather
    :return: pd.Dataframe
    """
    list_df = []  # store the list of hsmetrics data frames
    for _, filename in enumerate(input_files):
        with open(filename, "r", encoding="utf-8") as f:
            lines = []
            for line in f.readlines():
                line = line.rstrip()
                if line.startswith("## HISTOGRAM"):
                    # Optimization to avoid parsing the full file.
                    # If the line starts with FFQ, we know that we have finished with the SN lines.
                    break
                elif line.startswith("#"):
                    continue
                elif len(line.strip()) == 0:  # skip blank lines
                    continue

                lines.append(line.split("\t"))
        # three columns were blank so ignore them
        df = pd.DataFrame([lines[1]], columns=lines[0][0:66])
        cid = None
        if get_cid is True:
            cid = get_cid_from_string(filename)
            df[SAMPLE_COL] = cid
        else:
            df[SAMPLE_COL] = os.path.basename(filename)

        if cid is not None:
            if ASSAY_TYPE == 'dna':  # no MCC for rna (fusions)
                df['MCC'] = get_mcc_from_cid(cid)
        list_df.append(df)

    final_df = pd.concat(list_df).reset_index(drop=True)
    if metric_type == 'hsmetrics':
        # put the non padded interval first
        final_df.sort_values(by=[SAMPLE_COL, 'BAIT_SET'], inplace=True, ascending=False)

    if ASSAY_TYPE == 'dna':  # no padding on RNA
        final_df[SAMPLE_COL] = final_df.apply(lambda row: update_sample_name(row), axis=1)

    f = NamedTemporaryFile()
    # Change the file name to something
    f.name = 'test.tsv'

    # all values were strings, so dump to csv and then read back so pandas will guess types
    final_df.to_csv(f.name, sep="\t")
    final_df = pd.read_csv(f.name, sep="\t")
    final_df.to_excel(f"{metric_type}_summary_from_{ASSAY_TYPE}.xlsx")
    return final_df


def update_sample_name(row: pd.Series) -> str:
    if 'padded' in row['BAIT_SET']:
        return row[SAMPLE_COL] + '_50b_pad'
    return row[SAMPLE_COL]


def get_primary_site_from_cid(cid: str) -> Union[str, dict]:
    """
    Get the Primary Site.  Generated form a query sequencing_table.sql, then just used VIM to create the dictionary
    :param cid: CID to retrieve
    :return: string
    """
    values = {
        "cid23-1822": "Biliary Brush",
        "cid23-1836": "Brain",
        "cid23-1833": "Breast",
        "cid23-1695": "Breast",
        "cid23-1839": "Breast",
        "cid23-1837": "Breast",
        "cid23-1684": "Breast",
        "cid23-1696": "Breast",
        "cid23-1682": "Breast",
        "cid23-1691": "Breast",
        "cid23-1834": "Breast",
        "cid23-1832": "Breast",
        "cid23-1824": "Central Nervous System",
        "cid23-1825": "Colon",
        "cid23-1821": "Colon",
        "cid23-1709": "Colon",
        "cid23-1690": "Lung",
        "cid23-1838": "Lung",
        "cid23-1706": "Lung",
        "cid23-1827": "Lung",
        "cid23-1830": "Lung",
        "cid23-1705": "Lung",
        "cid23-1683": "Lung",
        "cid23-1685": "Lung",
        "cid23-1694": "Lung",
        "cid23-1692": "Lung",
        "cid23-1687": "Lung",
        "cid23-1835": "Lung",
        "cid23-1686": "Lung",
        "cid23-1680": "Lung",
        "cid23-1708": "Lung",
        "cid23-1704": "Lung",
        "cid23-1689": "Lung",
        "cid23-1688": "Pancreas",
        "cid23-1707": "Rectum",
        "cid23-1823": "Skin",
        "cid23-1829": "Unknown",
        "cid23-1681": "Unknown",
        "cid23-1693": "Unknown",
        "cid23-1820": "Unknown",
        "cid23-1826": "Unknown",
        "cid23-1828": "Unknown",
        "cid23-1864": "Unknown",
    }
    if not cid:
        return values
    elif cid in values:
        return values[cid]
    return 'N/A'


def get_diagnosis_from_cid(cid: str) -> Union[str, dict]:
    """
    Get the diagnosis.  Generated form a query sequencing_table.sql, then just used VIM to create the dictionary
    :param cid: CID to retrieve
    :return: string
    """
    values = {
        "cid23-1822": "intrahepatic cholangiocarcinoma",
        "cid23-1836": "craniopharyngioma",
        "cid23-1833": "breast cancer",
        "cid23-1695": "breast carcinoma",
        "cid23-1839": "breast carcinoma",
        "cid23-1837": "breast carcinoma",
        "cid23-1684": "breast carcinoma",
        "cid23-1696": "breast carcinoma",
        "cid23-1682": "her2-receptor positive breast cancer",
        "cid23-1691": "invasive ductal carcinoma",
        "cid23-1834": "invasive ductal carcinoma",
        "cid23-1832": "invasive ductal carcinoma",
        "cid23-1824": "brain glioma",
        "cid23-1825": "colon adenocarcinoma",
        "cid23-1821": "colon adenocarcinoma",
        "cid23-1709": "colon adenocarcinoma",
        "cid23-1690": "adenocarcinoma",
        "cid23-1838": "adenocarcinoma",
        "cid23-1706": "carcinoma",
        "cid23-1827": "lung adenocarcinoma",
        "cid23-1830": "lung adenocarcinoma",
        "cid23-1705": "lung adenocarcinoma",
        "cid23-1683": "lung adenocarcinoma",
        "cid23-1685": "lung adenocarcinoma",
        "cid23-1694": "lung adenocarcinoma",
        "cid23-1692": "lung adenocarcinoma",
        "cid23-1687": "lung adenocarcinoma",
        "cid23-1835": "lung adenocarcinoma",
        "cid23-1686": "lung adenocarcinoma",
        "cid23-1680": "lung adenocarcinoma",
        "cid23-1708": "lung adenocarcinoma",
        "cid23-1704": "lung adenocarcinoma",
        "cid23-1689": "lung adenocarcinoma",
        "cid23-1688": "pancreatic adenocarcinoma",
        "cid23-1707": "colon adenocarcinoma",
        "cid23-1823": "skin melanoma",
        "cid23-1829": "adenocarcinoma",
        "cid23-1681": "adenocarcinoma of unknown primary",
        "cid23-1693": "adenocarcinoma of unknown primary",
        "cid23-1820": "carcinoma",
        "cid23-1826": "endometrial stromal sarcoma",
        "cid23-1828": "squamous cell carcinoma of unknown primary",
        "cid23-1864": "squamous cell carcinoma of unknown primary",
    }
    if not cid:
        return values
    elif cid in values:
        return values[cid]
    return 'N/A'


def get_mcc_from_cid(cid: str) -> Union[int, dict]:
    values = {
        "cid23-1820": 330,
        "cid23-1821": 272,
        "cid23-1864": 303,
        "cid23-1822": 540,
        "cid23-1823": 496,
        "cid23-1865": 387,   # had MFC orginally
        "cid23-1824": 471,
        "cid23-1825": 245,
        "cid23-1826": 486,
        "cid23-1827": 404,
        "cid23-1828": 782,
        "cid23-1829": 434,
        "cid23-1830": 340,
        "cid23-1833": 677,
        "cid23-1832": 610,
        "cid23-1834": 690,
        "cid23-1835": 203,
        "cid23-1836": 71,
        "cid23-1838": 177,
        "cid23-1837": 91,
        "cid23-1839": 237,
        "cid23-1863": 698,  # need this for sorting
        "cid23-1680": 242,
        "cid23-1704": 573,
        "cid23-1705": 287,
        "cid23-1706": 262,
        "cid23-1681": 347,
        "cid23-1707": 524,
        "cid23-1683": 430,
        "cid23-1685": 313,
        "cid23-1686": 499,
        "cid23-1687": 720,
        "cid23-1688": 486,
        "cid23-1689": 311,
        "cid23-1690": 350,
        "cid23-1684": 422,
        "cid23-1691": 287,
        "cid23-1692": 510,
        "cid23-1708": 525,
        "cid23-1709": 217,
        "cid23-1693": 437,
        "cid23-1694": 134,
        "cid23-1696": 328,
        "cid23-1695": 393,
        "cid23-1682": 456
    }
    if not cid:
        return values
    elif cid in values:
        return values[cid]
    return -1


def get_cnv_status(cid: str) -> Union[int, dict]:
    values = {
        "cid23-1822": "neutral",
        "cid23-1865": "neutral",
        "cid23-1836": "neutral",
        "cid23-1833": "ERBB2 amp",
        "cid23-1695": "ERBB2 amp",
        "cid23-1839": "ERBB2 amp",
        "cid23-1837": "ERBB2 amp",
        "cid23-1684": "ERBB2 amp",
        "cid23-1696": "ERBB2 amp gs180, orthogonally confirmed IHC negative",
        "cid23-1682": "ERBB2 amp",
        "cid23-1691": "neutral",
        "cid23-1834": "ERBB2 amp",
        "cid23-1832": "ERBB2 amp",
        "cid23-1824": "neutral",
        "cid23-1825": "neutral",
        "cid23-1821": "neutral",
        "cid23-1709": "neutral",
        "cid23-1690": "neutral",
        "cid23-1838": "neutral",
        "cid23-1706": "neutral",
        "cid23-1827": "neutral",
        "cid23-1830": "EGFR amp",
        "cid23-1705": "neutral",
        "cid23-1683": "neutral",
        "cid23-1685": "neutral",
        "cid23-1694": "ERBB2 amp",
        "cid23-1692": "neutral",
        "cid23-1687": "neutral",
        "cid23-1835": "neutral",
        "cid23-1686": "neutral",
        "cid23-1680": "neutral",
        "cid23-1708": "neutral",
        "cid23-1704": "neutral",
        "cid23-1689": "neutral",
        "cid23-1688": "neutral",
        "cid23-1707": "neutral",
        "cid23-1823": "neutral",
        "cid23-1829": "neutral",
        "cid23-1681": "neutral",
        "cid23-1693": "ERBB2 amp",
        "cid23-1820": "neutral",
        "cid23-1826": "neutral",
        "cid23-1828": "neutral",
        "cid23-1864": "neutral",
        "cid23-1863": "neutral"

    }
    if not cid:
        return values
    elif cid in values:
        return values[cid]
    return -1

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Parse out the FACETS VCF file')

    parser.add_argument('--hsmetrics_directory',
                        dest='hsmetrics_directory',
                        type=str,
                        help='Directory where find the hsmetrics files are located',
                        required=True)
    parser.add_argument('--depthofcoverage_directory',
                        dest='depthofcoverage_directory',
                        type=str,
                        help='Directory where find the depthofcoverage files are located',
                        required=True)

    parser.add_argument('--assay_type',
                        dest='assay_type',
                        type=str,
                        help='dna or rna (fusion) assay type',
                        choices=['dna', 'rna'],
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()