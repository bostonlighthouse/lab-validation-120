import argparse
import json
from pathlib import Path
import os
from matplotlib_venn import venn2
import matplotlib.pyplot as plt
from typing import Tuple

import pandas as pd
import requests
from io import StringIO
import re
import pprint as pp
from goatools.obo_parser import GODag
from goatools.gosubdag.gosubdag import GoSubDag


def main():
    args = get_cli_args()
    assay_name2_cli = args.assay_name2

    assay_name1 = 'GS Infinity v2'
    json_gene_map2 = ''
    if 'gs180' ==  assay_name2_cli:
        assay_name2 = 'GS 180'
        json_gene_map2 = 'scratch/gene_maps/gs-v2.0-gene-list-v23-20220624.json'
    else:
        assay_name2 = 'GS Infinity v1'
        json_gene_map2 = 'scratch/gene_maps/gs395-gene-list-v13-20220610.json'

    with open(args.json_gene_map1, encoding='utf-8') as infh:
        ds1 = json.load(infh)

    with open(json_gene_map2, encoding='utf-8') as infh:
        ds2 = json.load(infh)

    # Load the GO DAG
    obo_file = "go-basic.obo"  # Update with the path to your OBO file
    go_dag = GODag(obo_file)
    go_terms1, genes1 = get_goterms(ds1, go_dag)
    go_terms2, genes2 = get_goterms(ds2, go_dag)

    update_set_of_genes_names(set_of_genes_names=genes2)
    print(f"Variant found in {assay_name2_cli} not found in gs700 {len(genes2.difference(genes1))}")

    draw_venn_diagram(set(go_terms1), set(go_terms2), filename=f"go_terms_{assay_name2_cli}.svg", term='GO Terms',
                      assay_name1 = assay_name1, assay_name2 = assay_name2)
    draw_venn_diagram(set(genes1), set(genes2), filename=f"gene_terms_{assay_name2_cli}.svg", term='Genes',
                      assay_name1 = assay_name1, assay_name2 = assay_name2)



    #[print(x) for x in genes2.difference(genes1)]

def update_set_of_genes_names(set_of_genes_names: set):
    """
    Some gene names in GS180 or GS395 need updating, just do that here
    :param set_of_genes_names: set of gene names
    :return: updates
    """

    for gene_to_remove, gene_to_add in [('H3-3A','H3F3A'),('H3-5','H3F3C'),
                                        ('MRE11', 'MRE11A')]:
        if gene_to_remove in set_of_genes_names:
            set_of_genes_names.remove(gene_to_remove)
            set_of_genes_names.add(gene_to_add)

def draw_venn_diagram(set1: set, set2: set, filename: str, term: str,
                      assay_name1: str,
                      assay_name2: str) -> None:

    plt.figure(figsize=(8, 6))
    venn_diagram = venn2([set1, set2],
                         (f'{assay_name1} n={len(set1)}',
                          f'{assay_name2} n={len(set2)}'))
    plt.title(f'{term} Overlapping Between {assay_name1} & {assay_name2}')
    # Customizing colors: (R, G, B, Alpha)
    venn_diagram.get_patch_by_id('10').set_color('red')
    venn_diagram.get_patch_by_id('10').set_alpha(0.4)  # Set 1 only
    venn_diagram.get_patch_by_id('01').set_color('blue')
    venn_diagram.get_patch_by_id('01').set_alpha(0.4)  # Set 2 only
    venn_diagram.get_patch_by_id('11').set_color('purple')
    venn_diagram.get_patch_by_id('11').set_alpha(0.4)  # Intersection
    # Control label size
    # Control label size
    venn_diagram.get_label_by_id('10').set_fontsize(14)  # Set font size for label '10'
    venn_diagram.get_label_by_id('01').set_fontsize(14)  # Set font size for label '01'
    venn_diagram.get_label_by_id('11').set_fontsize(14)  # Set font size for label '11'
    plt.savefig(filename)
    plt.close()


def get_goterms(ds, go_dag) -> Tuple[set, set]:

    #print(f"getting {len(ds['data'].items())} genes")
    go_term_list = []
    genes = []
    for i, (gene, gene_dict) in enumerate(ds['data'].items()):
        if gene_dict['cnv_meta_data']['is_technical'] is True or \
            gene_dict['fusion_meta_data']['is_technical'] is True or \
            gene_dict['snv_meta_data']['is_technical'] is True:
            pass
        else:
            continue
        #print(gene)
        genes.append(gene)
        go_terms_df = fetch_uniprot_data(gene)
        #print(go_terms_df)
        # AttributeError: 'numpy.float64' object has no attribute 'split' go_terms_df['Gene Ontology (GO)'][0] is NaN
        if len(go_terms_df) > 0 and not pd.isna(go_terms_df['Gene Ontology (GO)'][0]):
            go_terms = go_terms_df['Gene Ontology (GO)'][0]
            go_string_val = go_terms.split(";")
            if len(go_string_val) > 2:
                go_string = go_string_val[2]
            else:
                continue
            go_term = re.search(r"(GO:\d+)", go_string).group(0)
            go_term_annotation = re.search(r"(.*)?\s+\[", go_string).group(1)
            #go_term_classification = classify_go_term(go_term, go_dag)
            go_term_list.append(go_term_annotation)
        #if i == 100:
        #    break
    return set(go_term_list), set(genes)


    # GET the Descendants Count and Information Content
    # https://github.com/tanghaibao/goatools/blob/main/notebooks/dcnt_and_tinfo.ipynb
    # Get the GO-DAG subset for your GO IDs
    gosubdag = GoSubDag(go_term_list, go_dag)
    # Get the deepest GO ID in the GO DAG subset
    go_id, go_term = max(gosubdag.go2obj.items(), key=lambda t: t[1].depth)
    # Print GO ID, using print format in gosubdag
    print(go_id, go_term.name)
    # Get all parents of the deepest GO ID
    go_ids_chosen = go_term.get_all_parents()
    print(go_ids_chosen)
    print('{N} ancestors for {GO} "{name}"'.format(
        N=len(go_ids_chosen), GO=go_term.item_id, name=go_term.name))

    #quit()
    # Add the deep GO ID to its list of ancestors for printing
    #go_ids_chosen.add(go_id)
    go_ids_chosen = list(set(go_term_list))
    nts = [gosubdag.go2nt[go] for go in go_ids_chosen]

    # Print the descendants count (dcnt)
    fmt_str = '{I:2}) {NS} {GO:10} {dcnt:11}        D{depth:02}  {GO_name}'
    # Print selected GO information
    print("\t".join(('IDX', 'NS', 'GO', 'ID', 'Descendants', 'Count', 'Depth Name')))
    for idx, nt_go in enumerate(sorted(nts, key=lambda nt: nt.depth), 1):
        vals = nt_go._asdict()
        print("\t".join([str(idx)] + [str(x) for x in vals.values()]))


def classify_go_term(go_id, go_dag):
    go_term = go_dag[go_id]

    if go_term.namespace == 'biological_process':
        return 'Biological Process (BP)'
    elif go_term.namespace == 'molecular_function':
        return 'Molecular Function (MF)'
    elif go_term.namespace == 'cellular_component':
        return 'Cellular Component (CC)'
    else:
        return 'Unknown'



def fetch_uniprot_data(gene_name):
    """
    Fetch data from UniProt for a given gene name and return GO terms.

    :param gene_name: The gene name for which to fetch GO terms.
    :return: A DataFrame with GO terms and their descriptions.
    """
    uniprot_gene_dir = 'scratch/data/uniprot_genes'
    uniprot_gene_file = f'{uniprot_gene_dir}/{gene_name}.tsv'
    if os.path.exists(uniprot_gene_file):
        #print(f"gene {gene_name} exists, loading")
        return pd.read_csv(uniprot_gene_file, sep='\t')
    else:
        base_url = "https://rest.uniprot.org/uniprotkb/stream?query=gene_exact:{encoded_gene_name}+AND+organism_id:9606&fields=accession,protein_name,go&format=tsv"
        # Encode the gene name for URL inclusion
        encoded_gene_name = requests.utils.quote(gene_name)
        # Format the URL with the current gene name
        formatted_url = base_url.format(encoded_gene_name=encoded_gene_name)
        #print(formatted_url)
        # Make the HTTP GET request
        response = requests.get(formatted_url)
        if response.status_code == 200:
            # Read the TSV content into a pandas DataFrame
            df = pd.read_csv(StringIO(response.text), sep='\t')
            df.to_csv(uniprot_gene_file, index=False, sep='\t')
            return df
        else:
            print(f"Failed to fetch data for {gene_name}")


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open a gene maps and get the go terms and compare them')

    parser.add_argument('--json_gene_map1', dest='json_gene_map1',
                        type=str, help='JSON gene map 1 ', default=None, required=True)

    parser.add_argument('--assay_name2', dest='assay_name2',
                        type=str, help='Second assay name ', default=None, required=True, choices=['gs395', 'gs180'])

    return parser.parse_args()


if __name__ == "__main__":
    main()