import argparse
import sys
from typing import Tuple
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from tools.bam_file_analysis.samtools.utils import get_samtools_stats_values
from tools.utils.tools_utils import open_directory_get_list_files_recursively, get_cid_from_string
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FixedLocator
from scipy import stats
import locale
locale.setlocale(locale.LC_ALL, 'en_US')



def main():
    args = get_cli_args()
    fusion_directory = args.fusion_directory
    batch = args.batch
    min_insert_size = args.min_insert_size
    max_insert_size = args.max_insert_size
    # get the list of input files to open
    fusion_directory_list = open_directory_get_list_files_recursively(directory=fusion_directory, file_glob='*.rnqc.txt')
    list_df = []
    cid_stats = {}
    column = 'NON-DUP%'
    for i, file in enumerate(fusion_directory_list):

        #if 'b24-66' in file:  # first clinical batch
        #    continue
        #if 'b24-69' in file:  # this batch was way over sequenced, but second clinical batch
        #    continue
        #if 'b24-72' in file:  # Third clinical batch
        #    continue
        #if 'b24-75' in file:  # Fourth clinical batch
        #    continue

        #if 'cid24-1846' in file:  # this sample was over sequenced 33.5 M reads
        #    continue


        if 'b23-133' in file:  # was a re-run of very difficult samples, so remove - Validation run
            continue
        if 'b23-158' in file: # Validation run
            continue
        if 'b23-114' in file: # validation run used in final document
            continue
        if 'b23-120' in file: # validation run used in final document
            continue

        samtools_file_name = file.replace('.rnqc.txt', '.samtools_stats.with_dups.txt')
        detango_file_name = file.replace('.rnqc.txt', '.detango.txt')
        intragenic_fc, num_intragenic_fusions = get_intragenic_detango_values(detango_file_name)
        dict_stats = get_samtools_stats_values(samtools_file_name)
        # Format the integer with commas as thousands separators
        if min_insert_size <=  dict_stats['insert_size_average'] < max_insert_size:
            pass
        else:
            continue

        cid = get_cid_from_string(file)
        #if get_signed_out_fusion(cid) == '':
        #    continue

        df = pd.read_csv(file, sep='\t', skiprows=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 29])
        df['CID'] = cid
        # Filter the data for the specified groups
        #groups_of_interest = ['rna', 'dna', 'u-u', 'x-x', 'e-e', 'e-i', 'e-j', 'i-e', 'i-i', 'i-j', 'j-e', 'j-i', 'j-j']
        #groups_of_interest = ['u-u', 'x-x', 'e-e', 'e-i', 'e-j', 'i-e', 'i-i', 'i-j', 'j-e', 'j-i', 'j-j']
        groups_of_interest = ['e-e', 'e-i', 'e-j', 'i-e', 'i-j', 'j-e', 'j-i', 'j-j']
        filtered_data = df[df['#GROUP'].isin(groups_of_interest)]
        actual_groups = filtered_data['#GROUP'].unique()
        filtered_data = filtered_data[filtered_data['#GROUP'].isin(actual_groups)]
        # Remove any non-numeric characters from 'NON-DUP' for plotting
        filtered_data[column] = filtered_data[column].replace({'%': '', ',': ''}, regex=True).astype(float)
        if cid in cid_stats:
            print(f"cid {cid} exists...{file}", file=sys.stderr)
            quit()
        cid_stats[cid] = {
            'insert_size': dict_stats['insert_size_average'],
            'insert_size_std': dict_stats['insert_size_std'],
            'intragenic_fc': intragenic_fc,
            'num_intragenic_fusions': num_intragenic_fusions,
            'num_sequences': dict_stats['num_sequences'],
        }
        # add in the other values from the e-e, e-i, etc
        for index, row in filtered_data.iterrows():
            cid_stats[cid][row['#GROUP']] = row['NON-DUP%']

        # Selects only numeric columns to sum
        cid_stats[cid]['total'] = round(filtered_data.select_dtypes(include=[np.number]).sum()[column], 2)
        list_df.append(filtered_data)

    assert len(list_df) == len(cid_stats), "Sizes not the same, likely redundant CID in the directories"
    filtered_data = pd.concat(list_df).reset_index(drop=True)

    # Plot individual line plots for each CID
    unique_cids = filtered_data['CID'].unique()
    # Calculate number of rows needed for subplots based on number of unique CIDs
    num_cids = len(unique_cids)
    num_rows = int(np.ceil(num_cids / 3)) + 3 # Add an extra row for the boxplot and 2 rows regressions lines
    # get the list of sorted CIDs by total from the column
    sorted_cids = sorted(cid_stats.keys(), key=lambda cid: cid_stats[cid]['total'], reverse=False)

    # Create a figure with subplots
    # Create a figure with gridspec for flexible subplot management
    fig = plt.figure(figsize=(21, 3 * num_rows))  # Adjust size as needed
    #fig = plt.figure(figsize=(30, 3 * num_rows))  # Adjust size as needed
    gs = gridspec.GridSpec(num_rows, 3, figure=fig)  # Defines a grid of num_rows rows and 3 columns

    filtered_dfs = []
    for i, cid in enumerate(sorted_cids):
        ax = plt.subplot(gs[i // 3, i % 3])  # Use gridspec to place each subplot
        # filter down by the cid
        subset = filtered_data[filtered_data['CID'] == cid]
        # Calculate sum for relevant columns
        total_row = subset.select_dtypes(include=[np.number]).sum()  # Selects only numeric columns to sum
        total_row['#GROUP'] = 'total'  # Adding back the non-numeric data
        total_row['CID'] = subset['CID'].iloc[0]  # Assuming all CIDs are the same for this example
        # Create a DataFrame from the total row dictionary
        total_df = pd.DataFrame([total_row])

        # Concatenate the original DataFrame with the new DataFrame containing the total row
        subset = pd.concat([subset, total_df], ignore_index=True)
        filtered_dfs.append(subset)
        # only box plot all except total
        subset2 = subset[subset['#GROUP'] != 'total']
        sns.barplot(data=subset2, x='#GROUP', y=column, ax=ax)

        # Loop through the points to annotate each one
        for line in range(0, subset2.shape[0]):
            ax.text(subset2['#GROUP'].iloc[line], subset2[column].iloc[line] + .5,
                    f'{subset2[column].iloc[line]:.2f}',  # Formatting the number
                    horizontalalignment='left', color='black', weight='semibold', fontsize=7)
        title = cid_stats[cid]
        title2 = {k: v for k, v in title.items() if k in ['insert_size', 'insert_size_std', 'total']}
        ax.set_title(f'CID: {cid} {title2}', fontsize=10, fontweight='bold')
        ax.set_ylabel(column)
        ax.set_xlabel('Group')
        ax.tick_params(axis='x', rotation=45)
        ax.grid(True, axis='y')
        num_sequences = title['num_sequences']
        num_sequences = locale.format_string("%d", num_sequences, grouping=True)
        ax.annotate(f"intragenic_fc: {title['intragenic_fc']} num_intragenic_fusions: {title['num_intragenic_fusions']}",
                    xy=(0.05, 0.90), xycoords='axes fraction', fontsize=12)
        ax.annotate(f"num_sequences: {num_sequences} QC: {sample_quality(title['intragenic_fc'], title['num_intragenic_fusions'])}",
                    xy=(0.10, 0.80), xycoords='axes fraction', fontsize=12)
        fusion = get_signed_out_fusion(cid)
        if fusion:
            ax.annotate(f"Fusion Found {fusion}",
                        xy=(0.15, 0.70), xycoords='axes fraction', fontsize=12)
        #ax.set_ylim([0, 60])
        ax.set_ylim([0, 20])

    filtered_data = pd.concat(filtered_dfs).reset_index(drop=True)
    # Remove column 'total'
    filtered_data2 = filtered_data[filtered_data['#GROUP'] != 'total']
    # Adding the boxplot as the last row spanning all columns
    ax_box = plt.subplot(gs[-3, 0])  # This uses the last row and two columns
    sns.boxplot(data=filtered_data2, x='#GROUP', y=column, ax=ax_box, showfliers=False)
    sns.stripplot(data=filtered_data2, x='#GROUP', y=column, color='black', size=4, jitter=True, ax=ax_box)
    temp_df = filtered_data2[filtered_data2['#GROUP'] == "e-e"]
    ax_box.set_title(f'{batch} Boxplot of "{column}" by Group Across All CIDs n = {len(temp_df)}')
    tick_positions = ax_box.get_xticks()
    ax_box.xaxis.set_major_locator(FixedLocator(tick_positions))
    ax_box.set_xticklabels(ax_box.get_xticklabels(), rotation=45)
    ax_box.set_ylabel(column)
    ax_box.set_xlabel('Group')
    ax_box.grid(True)
    ax_box.set_ylim([0, 15])

    filtered_data2 = filtered_data[filtered_data['#GROUP'] == 'total']
    # Adding the boxplot as the last row spanning all columns
    ax_box = plt.subplot(gs[-3, 1])  # This uses the last row and two columns
    sns.boxplot(data=filtered_data2, x='#GROUP', y=column, ax=ax_box, showfliers=False)
    sns.stripplot(data=filtered_data2, x='#GROUP', y=column, color='black', size=4, jitter=True, ax=ax_box)
    ax_box.set_title(f'{batch} Boxplot of "{column}" by Group Across All CIDs n = {len(temp_df)}')
    tick_positions = ax_box.get_xticks()
    ax_box.xaxis.set_major_locator(FixedLocator(tick_positions))
    ax_box.set_xticklabels(ax_box.get_xticklabels(), rotation=45)
    ax_box.set_ylabel(column)
    ax_box.set_xlabel('Group')
    ax_box.axhline(y=1.5, color='red', linestyle='--')
    ax_box.grid(True)
    #ax_box.set_ylim([0, 10])

    # Converting dictionary to DataFrame
    df = pd.DataFrame.from_dict(cid_stats, orient='index')
    ax_reg = plt.subplot(gs[-3, 2])  # This uses the last row and two columns
    # Ensure numeric columns are treated as such (they are currently strings)
    df = df.apply(pd.to_numeric)
    sns.regplot(x='insert_size', y='total', data=df, ax=ax_reg)
    ax_reg.set_title(f'{batch} Linear Regression of Total vs. Insert Size  n = {len(df)}')
    ax_reg.set_xlabel('Insert Size')
    ax_reg.set_ylabel('Total')
    ax_reg.grid(True)
    # Calculate R2 value and annotate
    x = df['insert_size']
    y = df['total']
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    r2 = r_value ** 2
    ax_reg.annotate(f'R\u00b2 = {r2:.3f}', xy=(0.15, 0.65), xycoords='axes fraction', fontsize=12)

    for i, (xval, yval, xlim, ylim) in enumerate([['intragenic_fc', 'num_intragenic_fusions', None, None],
                                      ['total', 'intragenic_fc', None, None],
                                      ['total', 'num_intragenic_fusions', None, None]]):
        ax_reg = plt.subplot(gs[-2, i])  # This uses the last row and two columns
        sns.regplot(x=xval, y=yval, data=df, ax=ax_reg)
        ax_reg.set_title(f'{batch} Linear Regression of {yval} vs {xval} n = {len(df)}')
        ax_reg.set_xlabel(xval)
        ax_reg.set_ylabel(yval)
        ax_reg.set_xlim([0, xlim])
        ax_reg.set_ylim([0, ylim])
        ax_reg.grid(True)
        # Calculate R2 value and annotate
        x = df[xval]
        y = df[yval]
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
        r2 = r_value ** 2
        ax_reg.annotate(f'R\u00b2 = {r2:.3f}', xy=(0.15, 0.65), xycoords='axes fraction', fontsize=12)


    for i, (xval, yval) in enumerate([['num_sequences', 'num_intragenic_fusions'],
                                      ['num_sequences', 'intragenic_fc'],
                                      ['num_sequences', 'total']]):
        ax_reg = plt.subplot(gs[-1, i])  # This uses the last row and two columns
        sns.regplot(x=xval, y=yval, data=df, ax=ax_reg)
        ax_reg.set_title(f'{batch} Linear Regression of {yval} vs {xval} n = {len(df)}')
        ax_reg.set_xlabel(xval)
        ax_reg.set_ylabel(yval)
        ax_reg.grid(True)
        # Calculate R2 value and annotate
        x = df[xval]
        y = df[yval]
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
        r2 = r_value ** 2
        ax_reg.annotate(f'R\u00b2 = {r2:.3f}', xy=(0.15, 0.65), xycoords='axes fraction', fontsize=12)

    # Move index to a column named "CID"
    df.reset_index(inplace=True)
    df.rename(columns={'index': 'CID'}, inplace=True)
    df['Heuristic_QC'] = df.apply(lambda row: sample_quality(row['intragenic_fc'], row['num_intragenic_fusions']),
                                  axis=1)
    df['Signed_out_fusion'] = df.apply(lambda row: get_signed_out_fusion(row['CID']),
                                       axis=1)
    df.sort_values(by='total', inplace=True)
    df.to_excel(f'{batch}-fusion-metrics.xlsx')
    # Adjust layout to prevent overlap
    plt.tight_layout()
    plt.savefig(f'{batch}_fusion_min_insert_{min_insert_size}_max_insert_{max_insert_size}.pdf')


    plt.figure(figsize=(8, 8))  # Adjust size as needed
    xval = 'total'
    yval = 'intragenic_fc'
    fig, ax_reg = plt.subplots()
    sns.regplot(x=xval, y=yval, data=df, ax=ax_reg)
    ax_reg.set_title(f'{batch} Linear Regression of {yval} vs {xval} n = {len(df)}', fontsize = 8)
    ax_reg.set_xlabel(xval)
    ax_reg.set_ylabel(yval)
    ax_reg.grid(True)
    max = 1300
    ax_reg.set_xlim(0, 60)
    ax_reg.set_ylim(0, max)
    # threhsolds
    plt.axvline(x=1.5, color='r', linestyle='--')  # Adds a vertical dashed red line at x=5
    plt.axhline(y=100, color='r', linestyle='-.')  # Adds a horizontal dash-dot green line at y=5

    # Draw a diagonal line
    plt.plot([0, 60], [0, 1300], 'k--', lw=.75)  # 'k-' means black solid line, lw is line width

    # Calculate R2 value and annotate
    x = df[xval]
    y = df[yval]
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    r2 = r_value ** 2
    ax_reg.annotate(f'R\u00b2 = {r2:.3f}', xy=(0.15, 0.65), xycoords='axes fraction', fontsize=12)
    # Adjust layout to prevent overlap
    plt.tight_layout()
    plt.savefig(f'{batch}_small_regression_{min_insert_size}_max_insert_{max_insert_size}.pdf')


def get_signed_out_fusion(cid) -> str:
    fusion = ''
    fusion_data = {
        'cid24-2030': 'EML4_Exon5--ALK_Exon20',
        'cid24-2044': 'EGFR_Exon1--EGFR_Exon8',
        'cid24-1809': 'EML4_Exon13--ALK_Exon20',
        'cid24-2131': 'EML4_Exon13--ALK_Exon20',
        'cid24-1725': 'EWSR1_Exon8--ATF1_Exon5',
        'cid24-1727': 'BRD4_Exon11--NUTM1_Exon3',
        'cid24-2300': 'EML4_Exon2--ALK_Exon20',
        'cid24-2404': 'EML4_Exon13--ALK_Exon20',  # b24-81
        'cid24-2414': 'EML4_Exon2--NTRK3_Exon14', # b24-81
        'cid24-2412': 'MET_Exon13--MET_Exon15', # b24-81
        'cid24-2429': 'EML4_Exon5--ALK_Exon20', # b24-81
        'cid24-2535': 'CRTC1_Exon1--MAML2_Exon2', # b24-81
        'cid24-2715': 'MET_Exon13--MET_Exon15', # b24-87
        'cid24-2713': 'MET_Exon13--MET_Exon15', # b24-87
        'cid24-2889': 'EIF3E_Intron1~2--RSPO2_Exon2', # b24-90
        'cid24-2840': 'MET_Exon13--MET_Exon15', # b24-90
        ## validation data
        'cid23-1701': 'FGFR2_Exon17--BICC1_Exon3\nPLG_Exon18--LPA_Exon40',
        'cid23-1668': 'TPM3_Exon7--NTRK1_Exon10',
        'cid23-1664': 'EML4_Exon2--NTRK3_Exon14\nRAB40C_Exon2--ALK_Exon2',
        'cid23-1697': 'EML4_Exon5--ALK_Exon20',
        'cid23-1698': 'EML4_Exon2--ALK_Exon20',
        'cid23-1665': 'EML4_Exon19--RET_Exon12',
        'cid23-1666': 'KIF5B_Exon15--RET_Exon12',
        'cid23-1699': 'CD74_Exon6--ROS1_Exon34',
        'cid23-1700': 'EZR_Exon10--ROS1_Exon34',
        'cid23-1667': 'MET_Exon13--MET_Exon15',
        'cid23-1702': 'PAX3_Exon7--FOXO1_Exon2',
        'cid23-1669': 'MET_Exon13--MET_Exon15',
        'cid23-1671': 'MET_Exon13--MET_Exon15',
        'cid23-1672': 'MET_Exon13--MET_Exon15',
        'cid23-1673': 'MET_Exon13--MET_Exon15',
        'cid23-1676': 'EGFR_Exon1--EGFR_Exon8',
        'cid23-1679': 'NCOA4_Exon7--RET_Exon12\nPLG_Exon18--LPA_Exon40',
        'cid23-1677': 'EGFR_Exon1--EGFR_Exon8',
        'cid23-1841': 'CCDC82_Exon9--MAML2_Exon3',
        'cid23-1840': 'NCOA4_Exon7--RET_Exon12',
        'cid23-1842': 'EML4_Exon13--ALK_Exon20',
        'cid23-1843': 'PAX3_Exon7--FOXO1_Exon2',
        'cid23-1845': 'MET_Exon13--MET_Exon15',
        'cid23-1847': 'KIF5B_Exon16--RET_Exon12',
        'cid23-1846': 'EML4_Exon20--ALK_Exon20',
        'cid23-1848': 'EML4_Exon13--ALK_Exon20',
        'cid23-1849': 'MET_Exon13--MET_Exon15',
        'cid23-1854': 'FOXO1_Exon2--PAX3_Exon7',
        'cid23-1856': 'EZR_Exon10--ROS1_Exon34',
        'cid23-1858': 'EML4_Exon5--ALK_Exon20',
        'cid23-1861': 'ST7_Exon1--MET_Exon2',
        'cid23-1851': 'ERC1_Exon17--RET_Exon12',
        'cid23-1860': 'EML4_Exon6--ALK_Exon20',
        'cid23-1855': 'TPM3_Exon7--NTRK1_Exon10',
        'cid23-1857': 'EML4_Exon5--ALK_Exon20',

    }
    if cid in fusion_data:
        fusion = fusion_data[cid]
    return fusion

def sample_quality(intragenic_fc: int, num_intragenic_fusions: int) -> str:

    if intragenic_fc >= 300 and num_intragenic_fusions >= 100:
        qc = 'Pass'
    elif intragenic_fc >= 150 and num_intragenic_fusions >= 80:
        qc = 'Conditional'
    else:
        qc = 'Fail'
    return qc
def get_intragenic_detango_values(file: str) -> Tuple[int, int]:
    """
    Opne the detango file and get the intragenic values of the metrics (intragenic fusion with the highest coverage and
    the total number of intragenic fusions
    :param file: Detango file from Cider
    :return: int (intragenic_fusion_cov), int (total_intragenic_fusions)
    """
    intragenic_fc, num_intragenic_fusions = None, None

    df = pd.read_csv(file, sep='\t')
    df = df[df['Isoform_fusionType'] == 'Intragenic']
    df = df.sort_values(by='Fusion_Coverage', ascending=False)
    intragenic_fc = df['Fusion_Coverage'].iloc[0]
    num_intragenic_fusions = len(df)

    return intragenic_fc, num_intragenic_fusions


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open a directory of CIDER output files for fusion and plot stats')
    parser.add_argument('--fusion_directory', dest='fusion_directory',
                        type=str, help='Open a directory of CIDER output files for fusion',
                        required=True)
    parser.add_argument('--batch', dest='batch',
                        type=str, help='Batch from the run',
                        required=True)

    parser.add_argument('--min_insert_size', dest='min_insert_size',
                        type=float, help='The minimum size of of the insert to look at',
                        required=True)

    parser.add_argument('--max_insert_size', dest='max_insert_size',
                        type=float, help='The max size of of the insert to look at',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()