import argparse

import pandas as pd


def main():
    args = get_cli_args()
    input_bam = args.fusion_bam_header
    gtf_file = args.quiver_gtf_file

    gtf_df = pd.read_csv(gtf_file, sep='\t')

    with open(input_bam) as infh:
        for line in infh:
            values = line.strip().split('\t')
            if line.startswith('@SQ'):
                # @SQ     SN:1    LN:2009
                chrom = values[1].split(":")[1]
                end = values[2].split(":")[1]
                annotation = gtf_df[gtf_df['id'] == chrom].iloc[0]
                annotation = ";".join((annotation['transcript'], annotation['exon_junct']))
                print("\t".join((chrom, '0', end, annotation)))


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open the output from a "samtools view --header-only" call and produce'
                                                 ' a bed file')
    parser.add_argument('--fusion_bam_header', dest='fusion_bam_header',
                        type=str, help='Open the output from a samtools view --header-only call ',
                        required=True)

    parser.add_argument('--quiver_gtf_file', dest='quiver_gtf_file',
                        type=str, help='GTF file from Quiver ',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()