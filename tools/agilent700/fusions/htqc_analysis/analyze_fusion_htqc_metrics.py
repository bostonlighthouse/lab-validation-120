import argparse
import os.path
import sys
from typing import Tuple
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from lab_validation_120.utils.pandas_dataframes_utilities import \
    read_dataset_and_get_pandas_df_from_key_value_each_line, get_column_types_summary_htqc
from tools.bam_file_analysis.samtools.utils import get_samtools_stats_values
from tools.utils.tools_utils import open_directory_get_list_files_recursively, get_cid_from_string
from tools.agilent700.fusions.analyze_fusion_metrics import get_signed_out_fusion, sample_quality, \
    get_intragenic_detango_values
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FixedLocator
from scipy import stats


"""
Step 1: Within-sample normalization
    -Group by CID.
    -Compute the median for each metric within each group.
    -Divide each metric value by its respective median within the sample.

Step 2: Across-sample normalization
    -Compute the median for each contig across all samples for each metric.
    -Divide the within-sample normalized values by these across-sample medians.
"""


def main():
    args = get_cli_args()
    fusion_htqc_directory = args.fusion_htqc_directory
    batch =  args.batch
    # get the list of input files to open
    fusion_directory_list = (
        open_directory_get_list_files_recursively(directory=fusion_htqc_directory, file_glob='*.marked_dup_htqc_output.txt'))
    list_df = []
    cids_found = {}
    #vals = ('MEAN_ABS_COVERAGE', 'MEAN_FILTERED_COVERAGE', 'MEAN_COLLAPSED_COVERAGE')
    vals = ('RPKM_ABS_COVERAGE', 'RPKM_FILTERED_COVERAGE', 'RPKM_COLLAPSED_COVERAGE')
    for i, file in enumerate(fusion_directory_list):
        htqc_summary_file_name = file.replace('output.txt', 'output_summary.txt')
        if os.path.exists(htqc_summary_file_name):
            htqc_summary_df = read_dataset_and_get_pandas_df_from_key_value_each_line(
                col_vals=get_column_types_summary_htqc(),
                file_name=htqc_summary_file_name)
            print(htqc_summary_df['MEAN_ABS_COVERAGE'][0])
        cid = get_cid_from_string(file)
        if cid in cids_found:
            print("Skipping duplicate")

        cids_found[cid] = 1
        df = pd.read_csv(file, sep='\t')
        df['signed_out_fusion'] = get_signed_out_fusion(cid)
        df['CID'] = cid
        list_df.append(df)
    # one big data frame
    df = pd.concat(list_df).reset_index(drop=True)
    #df = filter_df(df)

    # Normalizing based on contig length is required to ensure a fair comparisons, as longer contigs are expected to
    # have more reads simply due to their length. This process adjusts for both the sequencing depth and the length
    # of the contigs.
    df['RPKM_ABS_COVERAGE'] = df['MEAN_ABS_COVERAGE'] / (df['SIZE (bp)'] / 1000)
    df['RPKM_FILTERED_COVERAGE'] = df['MEAN_FILTERED_COVERAGE'] / (df['SIZE (bp)'] / 1000)
    df['RPKM_COLLAPSED_COVERAGE'] = df['MEAN_COLLAPSED_COVERAGE'] / (df['SIZE (bp)'] / 1000)

    # remove these, since the BED file was generated using Archer GTF, and there are areas with 0 coverage
    df = df[(df['MEAN_ABS_COVERAGE'] > 0) & (df['MEAN_COLLAPSED_COVERAGE'] > 0)]

    # First normazliation technique using medians and w/in sample and across sample normalization
    for col in vals:
        # Step 1: Within-sample normalization
        # Purpose: To adjust for variability within each sample (CID) due to differences in sequencing depth or
        # other sample-specific factors.
        # Method: By dividing each contig's coverage metrics by the median coverage metric for that sample,
        # we standardize the values, making them relative to the typical coverage level within that sample.
        df[f'MEDIAN_{col}'] = df.groupby('CID')[col].transform('median')
        df[f'NORM_{col}'] = df[col] / df[f'MEDIAN_{col}']
        # Step 2: Across-sample normalization using raw values
        # Purpose: To adjust for differences in contig abundance across all samples, allowing for meaningful comparison
        # of specific contigs across different samples.
        # Method: By dividing the within-sample normalized values by the median of these values for each contig across
        # all samples, we standardize the contig's coverage relative to its typical level across the entire dataset.
        df[f'ACROSS_MEDIAN_{col}'] = df.groupby('NAME')[col].transform('median')
        df[f'FINAL_NORM_{col}'] = df[f'NORM_{col}'] / df[f'ACROSS_MEDIAN_{col}']

    # Implementing DESeq's Package Normalization ("Median of Ratios" normalization method)
    # Calculate the geometric mean for each contig across all samples.
    # Compute the ratio of each contig’s coverage to the geometric mean.
    # Calculate the median of these ratios for each sample.
    # Normalize each contig’s coverage by this median ratio.
    for col in vals:
        # Step 1: Calculate the geometric mean for each contig across all samples
        df[f'GEOMETRIC_{col}'] = df.groupby('NAME')[col].transform(
            lambda x: np.exp(np.mean(np.log(x[x > 0]))))
        # Step 2: Compute the ratio of each contig’s coverage to the geometric mean
        df[f'RATIO_{col}'] = df[col] / df[f'GEOMETRIC_{col}']
        # Step 3: Calculate the median of these ratios for each sample
        col2 = col.replace('RPKM_', '')
        df[f'MEDIAN_RATIO_{col2}'] = df.groupby('CID')[f'RATIO_{col}'].transform('median')
        # Step 4: Normalize each contig’s coverage by this median ratio
        df[f'DESEQ_NORM_MEAN_{col2}'] = df[col] / df[f'MEDIAN_RATIO_{col2}']

    #column_to_use = 'DESEQ_NORM_MEAN_FILTERED_COVERAGE'
    #column_to_use = 'DESEQ_NORM_MEAN_COLLAPSED_COVERAGE'
    column_to_use = 'FINAL_NORM_RPKM_COLLAPSED_COVERAGE'

    # Find Outliers
    # Step 1: Calculate the Z-score for each contig group
    df[f'Z_SCORE_{column_to_use}'] = df.groupby('NAME')[column_to_use].transform(lambda x:
                                                                                 stats.zscore(x, nan_policy='omit'))

    # Calculate p-values from Z-scores
    df[f'P_VALUE_{column_to_use}'] = df[f'Z_SCORE_{column_to_use}'].apply(lambda x: stats.norm.sf(abs(x)) * 2)  # two-tailed test

    # Step 2: Identify outliers based on zscore (e.g., Z-score > 2)
    outliers_zs = df[df[f'Z_SCORE_{column_to_use}'] > 2]

    # Step 2: Calculate the IQR for each contig group
    def _identify_outliers(group):
        q1 = group[column_to_use].quantile(0.25)
        q3 = group[column_to_use].quantile(0.75)
        iqr = q3 - q1
        outlier_condition = group[column_to_use] > (q3 + 1.5 * iqr)
        group['q3_threshold'] = (q3 + 1.5 * iqr)
        return group[outlier_condition]

    # Step 4: Find outliers based on IQR
    outliers_iqr = df.groupby('NAME').apply(_identify_outliers).reset_index(drop=True)

    ## Plotting
    # Define the list of NAMEs you want to plot
    # Fusions NOT FOUND via the GTF
    # EML4_Exon2--NTRK3_Exon14
    # RAB40C_Exon2--ALK_Exon2
    # EML4_Exon19--RET_Exon12
    # EML4_Exon5--ALK_Exon20
    # ERC1_Exon17--RET_Exon12
    # PLG_Exon18--LPA_Exon40
    # ST7_Exon1--MET_Exon2

    # plot these fusions
    name_list = [
        "MET[NM_000245]:13.?_MET[NM_000245]:15.?;13|15",
        "WIPF2[NM_133264.4]:1.?_ERBB2[NM_004448.3]:4.?;1|4",  # CNV
        "EGFRvIII[NM_005228]:1.?_EGFRvIII[NM_005228]:8.?;1|8",
        "EML4{ENST00000318522}:r.1_2504_ALK{ENST00000389048}:r.4080_6220;20|20",

        "FGFR2[NM_000141.4]:17.?_BICC1[NM_001080512.2]:2.?;17|2",  # -->  FGFR2_Exon17--BICC1_Exon3

        "VCL[NM_003373.3]:16.?_NTRK2[NM_006180.4]:12.?;16|12",
        "EML4{ENST00000318522}:r.1_1751_ALK{ENST00000389048}:r.4080_6220;13|20",
        "EML4{ENST00000318522}:r.1_470_ALK{ENST00000389048}:r.4080_6220;2|20",
        "EZR[NM_003379.4]:10.?_ROS1[NM_002944.2]:34.?;10|34",
        "CD74[NM_004355.3]:6.?_ROS1[NM_002944.2]:35.?;6|35",
        "EWSR1{ENST00000397938}:r.1_1112_ETV4{ENST00000319349}:r.1073_2391;7|8",
        "EML4{ENST00000318522}:r.1_929_ALK{ENST00000389048}:r.4080_6220;6|20",
        "EZR{ENST00000367075}:r.1_1259_ROS1{NM_002944}:r.5757_7368;10|34",

        "KIF5B[NM_004521.2]:16.?_RET[NM_020630.4]:12.?;16|12",
        "NCOA4[NM_005437.3]:7.?_RET[NM_020630.4]:12.?;7|12",   #  --> Only 1 row found
        "TPM3[NM_152263.3]:7.?_NTRK1[NM_002529.3]:10.?;7|10"
    ]
    if batch == 'all':
       name_list = name_list + [
           "PAX3{ENST00000392069}:r.1_1539_FOXO1{ENST00000379561}:r.1016_5722;7|2",  # --> FOXO1_Exon2--PAX3_Exon7
           "KIF5B[NM_004521.2]:15.?_RET[NM_020630.4]:12.?;15|12",
           "PAX3{ENST00000392069}:r.1_1539_FOXO1{ENST00000379561}:r.1016_5722;7|2",  # --> only 3 rows
       ]

    n_rows = len(name_list)  # Number of rows for the subplot grid

    # Subplots with three columns (line plot, boxplot, and histogram)
    fig, axes = plt.subplots(nrows=n_rows, ncols=3, figsize=(21, 5 * n_rows))

    for idx, name in enumerate(name_list):
        # Filter the dataframe for the current NAME
        df_name = df[df['NAME'] == name]
        # Sort the data for line plot
        df_sorted = df_name.sort_values(by=column_to_use)

        # Calculate quartile 1 and quartile 3
        q1 = df_sorted[column_to_use].quantile(0.25)
        q3 = df_sorted[column_to_use].quantile(0.75)
        iqr = q3 - q1  # inter-quartile range

        # Calculate the number of samples
        num_samples = df_sorted.shape[0]
        print(f"num samples {num_samples} {name}")
        if not num_samples > 0:
            continue

        # Draw Q1 and Q3 lines
        # Draw lines for Q3 + 1.5 * IQR (outlier threshold)
        upper_threshold = q3 + 1.5 * iqr
        axes[idx, 0].axhline(upper_threshold, color='green', linestyle='--', label='Q3 + 1.5 * IQR')
        axes[idx, 0].axhline(q1, color='red', linestyle='--', label='Q1 (25th percentile)')
        axes[idx, 0].axhline(q3, color='blue', linestyle='--', label='Q3 (75th percentile)')
        # Add a dummy line for the number of samples
        axes[idx, 0].plot([], [], ' ', label=f'# samples: {num_samples}')
        axes[idx, 0].legend()

        # Line plot
        sns.lineplot(data=df_sorted, x=range(len(df_sorted)), y=column_to_use, ax=axes[idx, 0], marker='o')
        axes[idx, 0].set_title(f'Line Plot for {name}', fontsize=10)
        axes[idx, 0].set_xlabel('Sample Index', fontsize=9)
        axes[idx, 0].set_ylabel(column_to_use, fontsize=9)
        # Boxplot
        sns.boxplot(data=df_name, y=column_to_use, ax=axes[idx, 1], showfliers=False)
        #sns.swarmplot(data=df_name, y=column_to_use, ax=axes[idx, 1], color=".25", size=3)
        sns.stripplot(data=df_name, y=column_to_use, ax=axes[idx, 1],
                      color=".25")  # Stripplot instead of swarmplot
        axes[idx, 1].set_title(f'Boxplot for {name}', fontsize=10)
        axes[idx, 1].set_ylabel(column_to_use, fontsize=9)

        # Histogram
        sns.histplot(data=df_name, x=column_to_use, ax=axes[idx, 2], kde=True)
        axes[idx, 2].set_title(f'Histogram for {name}', fontsize=10)
        axes[idx, 2].set_xlabel(column_to_use, fontsize=9)
        axes[idx, 2].set_ylabel('Frequency', fontsize=9)

    # Adjust layout for better readability
    plt.tight_layout()
    plt.savefig(f"2final_{batch}_{column_to_use}.pdf", bbox_inches='tight')

    # Write to excel
    with pd.ExcelWriter(f'2fusion_htqc_{batch}_{column_to_use}.xlsx') as writer:
        # Write each DataFrame to a different worksheet
        # All data
        df.sort_values(by=['NAME', column_to_use], ascending=False, inplace=True)
        df.to_excel(writer, sheet_name="all-data", index=False)
        # Outliers by Z-score
        outliers_zs = outliers_zs.sort_values(by=['NAME', f'Z_SCORE_{column_to_use}'], ascending=False)
        outliers_zs.to_excel(writer, sheet_name="z-score-outliers", index=False)
        # Outliers by IQR
        outliers_iqr = outliers_iqr.sort_values(by=['NAME', column_to_use], ascending=False)
        outliers_iqr.to_excel(writer, sheet_name="iqr-based-outliers", index=False)

def filter_df(df: pd.DataFrame) -> pd.DataFrame:
    list_to_filter = [
        'EML4{ENST00000318522}:r.1_1903_ALK{ENST00000389048}:r.4094_6220;14|20',
        'EML4{ENST00000318522}:r.1_1903_ALK{ENST00000389048}:r.4118_6220;14|20',
        'EML4{ENST00000318522}:r.1_2229+2522_ALK{ENST00000389048}:r.4126_6220;17|20',
        'EML4{ENST00000318522}:r.1_2229_EML4{ENST00000318522}:r.2229+2517_2229+2522;17|-1',
        'EML4{ENST00000318522}:r.2229+2517_2229+2522_ALK{ENST00000389048}:r.4126_6220;-1|20',
        'EML4[NM_019063.4]:21.?_ALK[NM_004304.4]:20.?;21|20',
        'EML4{ENST00000318522}:r.1_2318_ALK{ENST00000389048}:r.4080_6220;18|20',
        'EML4{ENST00000318522}:r.1_2318+654_insU_ALK{ENST00000389048}:r.4080-172_6220;18|20',
        'EML4{ENST00000318522}:r.1_2229_ins68_ALK{ENST00000389048}:r.4080_6220;17|20',
        'EML4{ENST00000318522}:r.1_2029_ALK{ENST00000389048}:r.4080_6220;15|20',
        'EML4{ENST00000318522}:r.1_1969_ALK{ENST00000389048}:r.4151_6220;15|20',
        'EML4{ENST00000318522}:r.1_1903_ALK{ENST00000389048}:r.4080_6220;14|20',
        'EML4{ENST00000318522}:r.1_1751_ALK{ENST00000389048}:r.4080_6220;13|20',
        'EML4{ENST00000318522}:r.1_929+805_insAA_ALK{ENST00000389048}:r.4080-115_6220;6|20',
        'EML4[NM_019063.4]:6.?_ALK[NM_004304.4]:17.?;6|17',
        'EML4{ENST00000318522}:r.1_929_ALK{ENST00000389048}:r.4080_6220;6|20',
        'EML4{ENST00000318522}:r.1_929_ALK{ENST00000389048}:r.3975_6220;6|19',
        'EML4{ENST00000318522}:r.903+188_903+220_ALK{ENST00000389048}:r.4080_6220;6|-1',
        'EML4{ENST00000318522}:r.1_929_EML4{ENST00000318522}:r.903+188_903+220;-1|20',
        'EML4[NM_019063.4]:3.?_ALK[NM_004304.4]:20.?;3|20',
        'EML4{ENST00000318522}:r.1_470_ALK{ENST00000389048}:r.4080_6220;2|20',
        'EML4[NM_019063.4]:1.?_CASK[NM_003688.3]:17.?;1|17',
        'EML4{ENST00000318522}:r.1_1903_insAUAUGCUGGAU_ALK{ENST00000389048}:r.4129_6220;14|20',
    ]
    """
    Keeping only the first:
    'EML4{ENST00000318522}:r.1_2504_ALK{ENST00000389048}:r.4080_6220;20|20',
    """
    return df[~df['NAME'].isin(list_to_filter)]

def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Open a directory of htqc output')
    parser.add_argument('--fusion_htqc_directory', dest='fusion_htqc_directory',
                        type=str, help='Open a directory of CIDER output files for fusion',
                        required=True)

    parser.add_argument('--batch', dest='batch',
                        type=str, help='Batch from the run',
                        required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()