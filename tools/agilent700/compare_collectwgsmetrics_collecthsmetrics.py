import argparse

from lab_validation_120.utils.htqc_utilites import get_htqc_summary_df_from_directory_files
from tools.utils.tools_utils import open_directory_get_list_files_recursively
from tools.agilent700.analyze_hsmetrcis_outputs import get_final_picard_summary_metrics_df, SAMPLE_COL


def main():
    """Business Logic"""
    args = get_cli_args()
    process_metrics_summary(directory=args.directory, sample_col=SAMPLE_COL)


def process_metrics_summary(directory: str, sample_col: str) -> None:
    """
    Process the directory for HS Metrics summary
    :param directory: dir where the data is found
    :param sample_col: Simple column name to use throughout
    :return: NoneType
    """
    list_hsmetrics_files = open_directory_get_list_files_recursively(directory=directory,
                                                                     file_glob="*HsMetrics.txt")
    # get the hsmetrics df from the list of files
    hsmetrics_summary_df = get_final_picard_summary_metrics_df(input_files=list_hsmetrics_files,
                                                               get_cid=False)
    # update sample so it can be merged
    hsmetrics_summary_df[sample_col] = hsmetrics_summary_df.apply(lambda row: row[sample_col].split(".")[0], axis=1)

    # get the wgsMetrics df from the list of files
    list_hsmetrics_files = open_directory_get_list_files_recursively(directory=directory,
                                                                     file_glob="*wgsMetrics.txt")
    wgsmetric_summary_df = get_final_picard_summary_metrics_df(input_files=list_hsmetrics_files,
                                                               get_cid=False, metric_type='wgsmetrics')
    # update sample so it can be merged
    wgsmetric_summary_df[sample_col] = wgsmetric_summary_df.apply(lambda row: row[sample_col].split(".")[0], axis=1)

    # get the htqc df from the list of files
    htqc_df = get_htqc_summary_df_from_directory_files(directory=directory, file_glob="*htqc_output_summary.txt",
                                                       sample_col=sample_col)
    # update sample so it can be merged
    htqc_df[sample_col] = htqc_df.apply(lambda row: row[sample_col].split(".")[0], axis=1)
    # merge the results
    final_df = hsmetrics_summary_df.merge(wgsmetric_summary_df, on=sample_col).merge(htqc_df, on=sample_col)
    # output to an excel file
    final_df.to_excel('merged_results_wgsMetrics_hsMetrics_htqc.xlsx')


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Compare the Mean Coverage and Mean Target Coverage from CollectWgsMetrics and CollectHsMetrics"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--directory', dest='directory',
                        help="Directory where outputs are located", required=True)


    return parser.parse_args()


if __name__ == "__main__":
    main()