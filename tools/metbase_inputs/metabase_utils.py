import os
import sqlalchemy
from dotenv import load_dotenv


def get_engine_with_password(password: str) -> sqlalchemy.engine.base.Engine:
    if password is None:
        # Load the .env file
        load_dotenv()
        # Retrieve the password from the environment variable
        password = os.getenv('METABASE_RO_DATABASE_PASSWORD')
    engine = _get_engine(password=password)
    return engine


def _get_engine(password: str = None) -> sqlalchemy.engine.base.Engine:
    user = "bliro"  # read only user
    db_name = "bli_data_lake"
    host = "10.240.0.51"
    port = 31372
    engine = sqlalchemy.create_engine(
        "postgresql://{user}:{password}@{host}:{port}/{db_name}".format(user=user, password=password, host=host,
                                                                        port=port, db_name=db_name))
    return engine
