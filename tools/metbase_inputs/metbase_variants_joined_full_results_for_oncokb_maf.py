import argparse
import re
import sys

from tools.utils.tools_utils import get_onco_tree_data


def main():
    args = get_cli_args()
    onco_tree_data = get_onco_tree_data(json_file=args.onco_json_file)
    cut_value = 19  # values to take up till... rest ignore

    cid_to_tumor_type = get_cid_to_tumor_type(file=args.joined_metabase_file)
    with open(args.joined_metabase_file) as infh:
        line = infh.readline().rstrip()
        values = line.split("\t")[0:cut_value]
        values.append("ONCOTREE_CODE")  # add this to the headder
        values.append("CID")  # add this to the headder
        print("\t".join(values))
        for line in infh:
            line = line.rstrip()
            values = line.split("\t")
            if args.keep_only_research_variants is True:  # is this only "RESEARCH" variant we care about
                # column 19 is the VET status
                if values[18] == 'RESEARCH':
                    url = values[9]
                    match = re.search(r'(CID\d+-\d+)', url)  # pull out the CID to get the tumor types
                    cid = None
                    if match:
                        cid = match.group(1).lower()
                        if cid in cid_to_tumor_type:
                            tumor_type = cid_to_tumor_type[cid]  # this should not fail
                        else:
                            print(f"No tumor type for this cid: {cid}")
                    values = values[0:cut_value]  # we only care about these values in the printed MAF
                    values.append(onco_tree_data[tumor_type]["ONCOTREE_CODE"])  # append on the oncoTree code
                    values.append(cid)
                    print("\t".join(values))
            else:
                print(line)

def get_cid_to_tumor_type(file=None):
    cid_to_tumor = {}
    with open(file) as infh:
        line = infh.readline().rstrip()  # remove the header
        for line in infh:
            values = line.split("\t")
            url = values[9]
            type_vet = values[18]
            match = re.search(r'(CID\d+-\d+)', url)
            cid = None
            if match:
                cid = match.group(1).lower()
                tumor_type = values[35].lower()
                cid_to_tumor[cid] = tumor_type

            else:  # this was missing a cid, so just print it out
                print(f"CID missing here: {url} type of vet: {type_vet}", file=sys.stderr)

    return  cid_to_tumor


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """

    help_str = "Take the Metbase file (joined from the Variants table and the Full Results table and produce " \
               "a MAF file with OncoTree Codes"
    parser = argparse.ArgumentParser(description=help_str)
    # only analyzed consequential impacts in regression?
    parser.add_argument('--joined_metabase_file', dest='joined_metabase_file', help="MAF file from metabase", default=None, required=True)
    parser.add_argument('--onco_json_file', dest='onco_json_file', help="OncoTree JSON file from Cider",
                        default=None, required=True)
    parser.add_argument('--keep_only_research_variants', dest='keep_only_research_variants', action='store_true')
    parser.add_argument('--keep_all_variants', dest='keep_only_research_variants', action='store_false')


    return parser.parse_args()


if __name__ == "__main__":
    main()