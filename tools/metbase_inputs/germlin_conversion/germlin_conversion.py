import pandas as pd
import matplotlib.pyplot as plt
import argparse
import sqlalchemy
import seaborn as sns

from tools.metbase_inputs.metabase_utils import get_engine_with_password

sns.set(style="whitegrid")
sns.set_palette("Set2")
from typing import Tuple

GERMLINE_TEXT = 'germline_conversion'

# More on Germline Conversion
# https://www.nature.com/articles/s41698-023-00429-1

# also abour tumor purity see:
# https://onlinelibrary.wiley.com/doi/full/10.1111/cas.16043

def main():
    args = get_cli_args()
    password = args.password
    engine = get_engine_with_password(password)
    #final_variants_df = process_tables(engine=engine)
    df_file = f'{GERMLINE_TEXT}_final_variants.xlsx'
    #final_variants_df.to_excel(df_file)
    final_variants_df = pd.read_excel(df_file)

    #samples_df = get_samples_sequenced(engine=engine)
    # in metabase for SNAPSHOT-NGS-V2
    #samples_df = samples_df[samples_df['Test'] != 'SNAPSHOT-NGS-V2']  # did not have all data in meta base for GS120
    #samples_df.to_excel("final_samples.xlsx", index=False)
    samples_df = pd.read_excel("final_samples.xlsx")

    (all_percentages, all_numerators, all_denominators,
     list_df_all_variants_found) = analyze_variants_df(df=final_variants_df, samples_df=samples_df)
    draw_final_plot(all_percentages=all_percentages, all_numerators=all_numerators, all_denominators=all_denominators)
    plot_list_df_all_variants_found(list_df_all_variants_found)


def plot_list_df_all_variants_found(list_df_all_variants_found: list = None):
    df = pd.concat(list_df_all_variants_found).reset_index(drop=True)
    # biomarkers could be added again, so remove them
    df = df.drop_duplicates()
    df.to_excel(f"{GERMLINE_TEXT}_gene_transcript_consequence_data.xlsx")
    # Create the new column 'plot_hgvsp' by combining the 'transcript_consequences_gene_symbol' and 'transcript_consequences_hgvsp' columns
    df['plot_hgvsp'] = df['transcript_consequences_gene_symbol'] + ':' + df['transcript_consequences_hgvsp']
    df['most_severe_consequence'] = df['most_severe_consequence'].str.replace("_", "\n")
    df.to_excel(f"{GERMLINE_TEXT}_gene_transcript_consequence_data.xlsx")

    # Define the filtered dataframes
    df_esmo_conservative = df[
        (df['list_type'] == 'gene_list_esmo_conservative') | (df['list_type'] == 'gene_biomarker')]
    df_esmo_relaxed = df[(df['list_type'] == 'gene_list_esmo_relaxed') | (df['list_type'] == 'gene_biomarker')]
    df_asco_relaxed = df[(df['list_type'] == 'gene_list_asco_relaxed') | (df['list_type'] == 'gene_biomarker')]

    for value in ['transcript_consequences_gene_symbol', 'most_severe_consequence']:
        plot_value = 'Gene' if value == 'transcript_consequences_gene_symbol' else 'Most Severe Consequence'
        # Count the occurrences of 'transcript_consequences_gene_symbol'
        esmo_conservative_counts = df_esmo_conservative[value].value_counts()
        esmo_relaxed_counts = df_esmo_relaxed[value].value_counts()
        asco_relaxed_counts = df_asco_relaxed[value].value_counts()

        # Plotting the bar charts with subplots
        add_to_yaxis = 10
        if value == 'most_severe_consequence':
            fig, axs = plt.subplots(3, 1, figsize=(7, 8))
        else:
            fig, axs = plt.subplots(3, 1, figsize=(7, 8))

        # Plot for gene_list_esmo_conservative
        sns.barplot(x=esmo_conservative_counts.index, y=esmo_conservative_counts.values, ax=axs[0],
                    hue=esmo_conservative_counts.index, palette="muted", width=0.6, dodge=False)
        axs[0].set_title(f'ESMO Conservative | Non-squamous NSCLC | {plot_value} | N={len(df_esmo_conservative)}')
        axs[0].set_ylabel('Count')
        #axs[0].set_xlabel(plot_value)
        axs[0].set_xlabel('')
        #axs[0].set_xticklabels(axs[0].get_xticklabels(), rotation=45)
        plt.setp(axs[0].get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
        axs[0].set_ylim(axs[0].get_ylim()[0], axs[0].get_ylim()[1] + add_to_yaxis)
        for p in axs[0].patches:
            axs[0].annotate(f'{p.get_height():.0f}',  (p.get_x() + p.get_width() / 2., p.get_height()),
                            ha='center', va='center', xytext=(0, 10), textcoords='offset points')

        # Plot for gene_list_esmo_relaxed
        sns.barplot(x=esmo_relaxed_counts.index, y=esmo_relaxed_counts.values, ax=axs[1],
                    hue=esmo_relaxed_counts.index, palette="muted", width=0.6, dodge=False)
        axs[1].set_title(f'ESMO Relaxed | Non-squamous NSCLC | {plot_value} | N={len(df_esmo_relaxed)}')
        axs[1].set_ylabel('Count')
        #axs[1].set_xlabel(plot_value)
        axs[1].set_xlabel('')
        #axs[1].set_xticklabels(axs[1].get_xticklabels(), rotation=45)
        plt.setp(axs[1].get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
        axs[1].set_ylim(axs[1].get_ylim()[0], axs[1].get_ylim()[1] + add_to_yaxis)
        for p in axs[1].patches:
            axs[1].annotate(f'{p.get_height():.0f}', (p.get_x() + p.get_width() / 2., p.get_height()),
                            ha='center', va='center', xytext=(0, 10), textcoords='offset points')

        # Plot for gene_list_asco_relaxed
        sns.barplot(x=asco_relaxed_counts.index, y=asco_relaxed_counts.values, ax=axs[2],
                    hue=asco_relaxed_counts.index, palette="muted", width=0.6, dodge=False)
        axs[2].set_title(f'ASCO Relaxed | Non-squamous NSCLC | {plot_value} | N={len(df_asco_relaxed)}')
        axs[2].set_ylabel('Count')
        axs[2].set_xlabel(plot_value)
        #axs[2].set_xticklabels(axs[2].get_xticklabels(), rotation=45)
        plt.setp(axs[2].get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
        axs[2].set_ylim(axs[2].get_ylim()[0], axs[2].get_ylim()[1] + add_to_yaxis + 10)
        for p in axs[2].patches:
            axs[2].annotate(f'{p.get_height():.0f}', (p.get_x() + p.get_width() / 2., p.get_height()),
                            ha='center', va='center', xytext=(0, 10), textcoords='offset points')

        plt.tight_layout()
        plt.savefig(f'{GERMLINE_TEXT}_{plot_value.lower()}_counts.svg')
        plt.close(fig)


def draw_final_plot(all_percentages: list = None, all_numerators: list = None, all_denominators: list = None):


    # Data preparation
    data = {
        'Gene List': [
            'ESMO Conservative', 'ESMO Conservative', 'ESMO Conservative',
            'ESMO Relaxed', 'ESMO Relaxed', 'ESMO Relaxed',
            'ASCO Relaxed', 'ASCO Relaxed', 'ASCO Relaxed'
        ],
        'Category': [
            'All Samples', 'Non-squamous NSCLC', 'Additional Lung Samples',
            'All Samples', 'Non-squamous NSCLC', 'Additional Lung Samples',
            'All Samples', 'Non-squamous NSCLC', 'Additional Lung Samples'
        ],
        'Percentage': all_percentages,
        'Numerator (# samples)': all_numerators,
        'Denominator (# samples)': all_denominators,
    }

    # Create a DataFrame
    df = pd.DataFrame(data)
    df.to_excel(f'{GERMLINE_TEXT}_output.xlsx')
    # Plotting using Seaborn
    plt.figure(figsize=(10, 6))
    ax = sns.barplot(x='Category', y='Percentage', hue='Gene List', data=df)
    plt.title('Comparison of Germline Conversion Rate (GCR) by Gene List and Category')
    plt.ylabel('Percentage')
    plt.xlabel('Category')
    plt.ylim(0, 15)  # Set the y-axis limit to make the differences more visible
    plt.legend(title='Gene List')

    # Adding value labels on top of each bar
    for p in ax.patches:
        height = p.get_height()
        if height > 0:  # Skip bars with height 0
            ax.annotate(f'{height:.2f}%',  # Adding the '%' symbol with three decimal places
                        (p.get_x() + p.get_width() / 2., height),
                        ha='center', va='center',
                        xytext=(0, 9),  # 9 points vertical offset
                        textcoords='offset points')
    plt.savefig(f'{GERMLINE_TEXT}_percentage_plot.svg')


def analyze_variants_df(df: pd.DataFrame, samples_df: pd.DataFrame = None) -> Tuple[list, list, list, list]:

    num_sample_cids = len(samples_df['snapshot_cid'].unique())


    samples_df_filtered_lung = samples_df[samples_df['PrimarySite'] == 'Lung']
    samples_df_gs700 = samples_df_filtered_lung[samples_df_filtered_lung['Test'] == 'OC-700-Snapshot']
    print(f"Sample count GS700: {len(samples_df_gs700['snapshot_cid'].unique())}")
    samples_df_gs395 = samples_df_filtered_lung[samples_df_filtered_lung['Test'] == 'OC-395-Snapshot']
    print(f"Sample count GS395: {len(samples_df_gs395['snapshot_cid'].unique())}")
    samples_df_gsv2 = samples_df_filtered_lung[samples_df_filtered_lung['Test'] == 'SNAPSHOT-GSV2']
    print(f"Sample count GSV2: {len(samples_df_gsv2['snapshot_cid'].unique())}")

    num_sample_cids_filtered_lung = len(samples_df_filtered_lung['snapshot_cid'].unique())
    primary_site_dx = ['adenocarcinoma', 'lung adenocarcinoma', 'adenocarcinoma of unknown primary']
    samples_df_filtered_lung_adenocarcinoma = samples_df[(samples_df['PrimarySite'] == 'Lung') &
                                          (samples_df['PrimarySiteDiagnosis'].isin(primary_site_dx))]
    num_sample_cids_filtered_lung_adenocarcinoma = len(samples_df_filtered_lung_adenocarcinoma['snapshot_cid'].unique())

    # list_type = ['gene_list_esmo_conservative', 'gene_list_esmo_relaxed', 'gene_list_asco_relaxed', 'gene_biomarker']
    # Test = ['SNAPSHOT-GSV2', 'OC-395-Snapshot', 'OC-700-Snapshot']

    print(f"Number of All samples: {num_sample_cids}")
    print(f"Number of Only PrimarySite: Lung: {num_sample_cids_filtered_lung}")
    print(f"Primary Site Lung Diagnosis: {primary_site_dx}")
    print(f"Number of PrimarySite Lung and Diagnosis: {num_sample_cids_filtered_lung_adenocarcinoma}")
    print("")
    all_percentages = []
    all_numerators = []
    all_denominators = []
    list_df_all_variants_found = []
    for list_type in df['list_type'].unique():
        if list_type == 'gene_biomarker':
            continue
        df_variant_filtered = df[(df['list_type'] == list_type) | (df['list_type'] == 'gene_biomarker')]
        num_rows = len(df_variant_filtered['AccessionNumber'].unique())
        print(f" {list_type} Number of samples found Lung Primary: ", num_rows)

        val1 = round(num_rows/num_sample_cids*100, 2)
        all_percentages.append(val1)
        all_numerators.append(num_rows)
        all_denominators.append(num_sample_cids)
        print(f" {list_type} All Samples %: {val1}")
        val2 = round(num_rows/num_sample_cids_filtered_lung*100, 2)
        all_percentages.append(val2)
        all_numerators.append(num_rows)
        all_denominators.append(num_sample_cids_filtered_lung)
        print(f" {list_type} Only PrimarySite: Lung %: {val2}")

        df_variant_filtered2 = df_variant_filtered[df_variant_filtered['PrimarySiteDiagnosis'].isin(primary_site_dx)]
        num_rows = len(df_variant_filtered2['AccessionNumber'].unique())
        print(f" {list_type} Number of samples found Diagnosis {primary_site_dx}: ", num_rows)
        val3 = round(num_rows / num_sample_cids_filtered_lung_adenocarcinoma * 100, 2)
        all_percentages.append(val3)
        all_numerators.append(num_rows)
        all_denominators.append(num_sample_cids_filtered_lung_adenocarcinoma)
        print(f" {list_type} PrimarySite: Lung and Diagnosis %: {val3}")

        print("")
        list_df_all_variants_found.append(df_variant_filtered)
        df_variant_filtered.to_excel(f"{GERMLINE_TEXT}_{list_type}.xlsx")
    return all_percentages, all_numerators, all_denominators, list_df_all_variants_found


def get_samples_sequenced(engine: sqlalchemy.engine = None) -> pd.DataFrame:

    query = """
    SELECT "public"."report_app_meta".*, "Results".*
    FROM "public"."report_app_meta"
    LEFT JOIN "public"."full_results" "Results" ON "public"."report_app_meta"."snapshot_cid" = "Results"."AccessionNumber"
    WHERE (
        "public"."report_app_meta"."mock" = FALSE
       AND "public"."report_app_meta"."report_state" = 'SIGNED_OUT' 
       AND ("public"."report_app_meta"."germline_assay" = '' OR "public"."report_app_meta"."germline_assay" IS NULL)
       AND "Results"."Diagnosis" !~ 'FAILURE'
    )
    """
    # Execute the query and load the results into a DataFrame
    df = pd.read_sql(query, engine)

    df = fix_timezone_in_df(df=df)
    return df

def fix_timezone_in_df(df: pd.DataFrame) -> pd.DataFrame:
    # fix ValueError: Excel does not support datetimes with timezones. Please ensure that datetimes are
    # timezone unaware before writing to Excel.

    # Example datetime format (modify according to your actual datetime string format)
    datetime_format = '%Y-%m-%d %H:%M:%S%z'

    # Ensure the datetime columns are properly recognized as datetime type
    for col in df.columns:
        if pd.api.types.is_object_dtype(df[col]):
            try:
                df[col] = pd.to_datetime(df[col], format=datetime_format)
            except (ValueError, TypeError):
                pass

    # Convert datetime columns with timezone to timezone-unaware format
    for col in df.columns:
        if pd.api.types.is_datetime64tz_dtype(df[col]):
            df[col] = df[col].dt.tz_localize(None)

    return df


def process_tables(engine: sqlalchemy.engine) -> pd.DataFrame:
    # Snapshot2 Variants Clinical Prod Tumor doesn't have much data, so removing
    tables_to_get = ("SnapshotGSV2 Variants Clinical Prod Tumor",
                       "Snapshot395 Variants Clinical Prod Tumor", "Snapshot700 Variants Clinical Prod Tumor")

    list_combined_tables = []
    list_tables_variants_samples = []
    for table_name in tables_to_get:
        # for table_name in ("Snapshot700 Variants Clinical Prod Tumor"):
        print(f"Analyzing {table_name}")
        data, table_name, total_variants, total_variants_passing_vaf, total_samples, total_samples_passing_vaf \
            = get_data(engine=engine, table_name=table_name)
        list_of_dfs = get_data_from_gene(df=data)
        combined_df = pd.concat(list_of_dfs).reset_index(drop=True)
        list_combined_tables.append(combined_df)
        combined_df.to_excel(f"{table_name}_final_matching_data.xlsx")
        list_tables_variants_samples.append((table_name, total_variants, total_variants_passing_vaf,
                                             total_samples, total_samples_passing_vaf))

    final_df = pd.concat(list_combined_tables).reset_index(drop=True)
    df = pd.DataFrame(list_tables_variants_samples, columns=["Table", "total_variants", "total_variants_passing_vaf",
                                                             "total_samples", "total_samples_passing_vaf"])
    df.to_excel(f"{GERMLINE_TEXT}_variants_per_table.xlsx", index=False)
    return final_df


def get_data(engine: sqlalchemy.engine.base.Engine = None, table_name: str = None) \
        -> Tuple[pd.DataFrame, str, int, int, int, int]:
    table_name2 = table_name.lower().replace(" ", "_")
    query = f"""
    SELECT 
        "public"."full_results".*,
        "{table_name}".*
    FROM 
        "public"."full_results"
    INNER JOIN 
       "public"."{table_name2}" AS "{table_name}"
    ON 
        "public"."full_results"."SAMPLE_URL" = CONCAT('http://vv.cgs.bostonlighthouse.us/', "{table_name}"."sample_url")
    WHERE 
        "{table_name}"."VETT" = 'YES' AND
         "public"."full_results"."PrimarySite" = 'Lung'
    """

    # Execute the query and load the results into a DataFrame
    data = pd.read_sql(query, engine)
    # Create a new column by splitting 'Gene_Mutation' and selecting the second part
    data['hgvsp'] = data['transcript_consequences_hgvsp'].str.split(':', expand=True)[1]

    # Correct approach to check each column for datetime type with timezone and convert
    for column in data.columns:
        if pd.api.types.is_datetime64_any_dtype(data[column]):
            if data[column].dt.tz is not None:  # Check if the column has timezone info
                data[column] = data[column].dt.tz_convert(None)  # Correct method to remove timezone

    data.to_excel(f'{table_name2}_all_data.xlsx')
    df_filtered = data.drop_duplicates(subset='AccessionNumber', keep='first')
    total_variants = len(data)
    total_samples = len(df_filtered)
    print(f"Total Variants found: {total_variants}")
    print(f"Total Samples Analyzed: {total_samples}")
    df_filtered.to_excel(f'{table_name2}_unique_ID_data.xlsx')
    data = data[data['QC_ALT_AF'] >= .20]
    df_filtered = data.drop_duplicates(subset='AccessionNumber', keep='first')
    total_variants_passing_vaf = len(data)
    total_samples_passing_vaf = len(df_filtered)
    print(f"Total Variants >= 20% VAF found: {total_variants_passing_vaf}")
    print(f"Total Samples Analyzed >= 20% VAF: {total_samples_passing_vaf}")

    return data, table_name2, total_variants, total_variants_passing_vaf, total_samples, total_samples_passing_vaf


def get_data_from_gene(df: pd.DataFrame = None) -> list:
    all_data = []
    gene_list_esmo_conservative = ["BRCA1", "BRCA2", "PALB2", "RET", "MLH1", "MSH2", "MSH6"]
    gene_list_esmo_relaxed = gene_list_esmo_conservative + ["BRIP1", "MUTYH", "PMS2", "RAD51C", "RAD51D", "SDHAF2",
                                                    "SDHB", "SDHC", "SDHD", "TMEM127", "TSC2", "VHL"]
    gene_list_asco_relaxed = (gene_list_esmo_conservative +
                              gene_list_esmo_relaxed +
                              ["ATM", "BAP1", "BARD1", "CHEK2", "DICER1", "FH", "FLCN", "NF1", "PTCH1",
                               "POLD1", "POLE", "SDHA", "POT1"])

    gene_biomarker = [('TP53', 'p.Arg337His'), ('EGFR', 'p.Thr790Met')]
    print(f"gene_list_esmo_conservative: {[gene_list_esmo_conservative] + [gene_biomarker]}")
    print("")
    print(f"gene_list_esmo_relaxed: {[gene_list_esmo_relaxed] + [gene_biomarker]}")
    print("")
    print(f"gene_list_asco_relaxed: {[gene_list_asco_relaxed] + [gene_biomarker]}")

    for ds_type, ds in (('gene_list_esmo_conservative', gene_list_esmo_conservative),
                        ('gene_list_esmo_relaxed', gene_list_esmo_relaxed),
                        ('gene_list_asco_relaxed', gene_list_asco_relaxed),
                        ('gene_biomarker', gene_biomarker)):

        if ds_type == 'gene_biomarker':
            gene_names = [item[0] for item in ds]
            mutations = [item[1] for item in ds]
            df2 = df[df['hgvsp'].isin(mutations)].copy()
        else:
            df2 = df[df['transcript_consequences_gene_symbol'].isin(ds)].copy()
        if len(df2) == 0:  # possible to have no results at this point
            continue
        # fill in some values when they are not there
        if 'transcript_consequences_cadd_phred' not in df2.columns:
            df2.loc[:, 'transcript_consequences_cadd_phred'] = 'N/A'
        if 'gnomad' not in df2.columns:
            df2.loc[:, 'gnomad'] = 'N/A'
        if 'ONCO_KB_HIGHEST_DX_LEVEL' not in df2.columns:
            df2.loc[:, 'ONCO_KB_HIGHEST_DX_LEVEL'] = 'N/A'

        df2.loc[:, 'list_type'] = ds_type
        df2 = df2[
            ['list_type', 'ID', 'Diagnosis', 'MSIScore', 'Test', 'TMBScore', 'AccessionNumber', 'Batch', 'PrimarySite',
             'PrimarySiteDiagnosis',
             'TumorPercentage', 'Patient__Gender', 'ID', 'most_severe_consequence',
             'transcript_consequences_gene_symbol', 'transcript_consequences_hgvsg',
             'QC_REF_DP', 'QC_ALT_DP', 'QC_ALT_AF',
             'transcript_consequences_hgvsc', 'transcript_consequences_hgvsp', 'gnomad',
             'transcript_consequences_polyphen_prediction', 'transcript_consequences_sift_prediction',
             'transcript_consequences_polyphen_score', 'transcript_consequences_sift_score',
             'transcript_consequences_cadd_phred', 'ONCO_KB_HIGHEST_DX_LEVEL'
             ]]
        # Display the first few rows of the DataFrame
        # df2.to_excel(f"{ds_type}_{table_name2}_final.xlsx")
        all_data.append(df2)
    return all_data



def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Create the gene map json for the HRR assay')

    parser.add_argument('--password',
                        dest='password',
                        type=str,
                        default=None,
                        help='The password to connect to the database',
                        required=False)

    return parser.parse_args()


if __name__ == '__main__':
    main()
