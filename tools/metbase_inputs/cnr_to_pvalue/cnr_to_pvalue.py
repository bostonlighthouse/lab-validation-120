import argparse
import sqlalchemy
import pandas as pd
import numpy as np
import scipy.stats as stats
from typing import Tuple

import matplotlib.pyplot as plt
import seaborn as sns
from tools.cnv.utils import get_list_important_cnv_genes
from lab_validation_120.utils.plots.plot_utilities import get_subplot_bounding_box_parameters
from tools.metbase_inputs.metabase_utils import get_engine_with_password
from lab_validation_120.utils.assay_utils import get_assay_names


PVALUE_TITLE_ROUND = 5
MIN_LOG2RATIO_TO_PLOT = -3  # was thinking -5
MAX_LOG2RATIO_TO_PLOT = 3

def main():
    args = get_cli_args()
    table = ''
    assay_name = args.assay_name
    if assay_name == 'gs700':
        table = 'snapshot700_cnv_clinical_prod_tumor'
    elif assay_name == 'gs395':
        table = 'snapshot395_cnv_clinical_prod_tumor'
    elif assay_name == 'gs180':
        table = 'snapshotGSV2_cnv_clinical_prod_tumor'
    elif assay_name == 'gs700_normals':
        table = 'snapshot700_cnv_clinical_prod_normal'
    elif assay_name == 'gs395_normals':
        table = 'snapshot395_cnv_clinical_prod_normal'


    assay_name = assay_name.upper()
    engine = get_engine_with_password(args.password)
    var_ids = sorted(get_var_ids_list(engine=engine, table_name=table))
    list_important_cnv_genes = get_list_important_cnv_genes()
    data_points = [2.0, -1.0]
    list_gene_pvalues = []
    for data_point in data_points:
        for var_id in var_ids:
            #var_id = 'ERBB2_17_37855800'
            #var_id = 'EGFR_7_55086900' # for gs700
            #var_id = 'EGFR_7_55086967' # for gs395
            #var_id = 'EGFR_7_55086949' # for gsv2
            #var_id = 'PTEN_10_89623700' # for gs700
            gene = var_id.split("_")[0]
            if gene in list_important_cnv_genes:
                print(f'gene {gene} is important')
                log2cnr = get_log2cnr_list(engine=engine, table_name=table, var_id=var_id)
                empirical_pvalue = get_empirical_pvalue(log2cnr=log2cnr, data_point=data_point)
                kde_pvalue, kde_mean, kde_median, kde_min, kde_max = get_kde_pvalue(log2cnr=log2cnr, data_point=data_point,
                                                                                    gene=gene, assay_name=assay_name)
                bootstrap_pvalue = get_bootstrap_pvalue(log2cnr=log2cnr, data_point=data_point, gene=gene,
                                                        assay_name=assay_name)
                permutation_p_value = get_permutation_p_value(log2cnr=log2cnr, data_point=data_point, gene=gene,
                assay_name=assay_name)
                list_gene_pvalues.append(
                    {'gene': gene, 'num_samples': len(log2cnr), 'data_point': data_point,
                     'empirical_pvalue': empirical_pvalue,
                     'kde_pvalue': kde_pvalue, 'kde_mean': kde_mean, 'kde_median': kde_median,
                     'kde_min': kde_min, 'kde_max': kde_max,
                     'kde_-1': find_number_defined_at_threshold(data=log2cnr, data_point=-1, greater_equal=False),
                     'kde_-2': find_number_defined_at_threshold(data=log2cnr, data_point=-2, greater_equal=False),
                     'kde_-3': find_number_defined_at_threshold(data=log2cnr, data_point=-3, greater_equal=False),
                     'kde_1': find_number_defined_at_threshold(data=log2cnr, data_point=1, greater_equal=True),
                     'kde_2': find_number_defined_at_threshold(data=log2cnr, data_point=2, greater_equal=True),
                     'kde_3': find_number_defined_at_threshold(data=log2cnr, data_point=3, greater_equal=True),
                     'bootstrap_pvalue': bootstrap_pvalue,
                     'permutation_p_value': permutation_p_value}
                )
                #break
    df = pd.DataFrame(list_gene_pvalues)
    df.to_excel(f"final_cnv_pvalue_{assay_name}.xlsx")

def find_number_defined_at_threshold(data: list = None, data_point: float = None,
                                         greater_equal: bool = True) -> int:

    # Find the number of values equal or above the limit using list comprehension
    if greater_equal is True:
        return len([num for num in data if num >= data_point])
    # Find the number of values equal or below the limit using list comprehension
    return len([num for num in data if num <= data_point])

def get_permutation_p_value(log2cnr: list, data_point: float, num_permutations: int = 10_000, gene: str = None,
                            assay_name: str = None)-> float:
    # During the permutation process, you're essentially testing whether the observed data_point is an extreme value
    # compared to what would happen if the data were randomly reordered. This often leads to a permutation distribution
    # that doesn't necessarily resemble the original data distribution but instead reflects the differences in medians
    # when the data is permuted.

    # Convert log2cnr to a NumPy array if it isn't one already
    log2cnr = np.array(log2cnr)
    data_point_file_name = data_point
    # Convert data_point to a NumPy array with the same type as log2cnr
    data_point = np.array([data_point], dtype=log2cnr.dtype)

    # Concatenate log2cnr and data_point
    combined = np.concatenate([log2cnr, data_point])
    perm_diffs = []

    # Determine the observed difference using the median
    observed_diff = np.median(log2cnr) - data_point

    for _ in range(num_permutations):
        np.random.shuffle(combined)
        perm_diff = np.median(combined[:-1]) - np.median(combined[-1:])
        perm_diffs.append(perm_diff)

    perm_diffs = np.array(perm_diffs)

    # Determine the direction of the test based on the observed value relative to the median
    if data_point > np.median(log2cnr):
        # Right-tail test (testing for gain)
        permutation_p_value = np.mean(perm_diffs <= observed_diff)
    else:
        # Left-tail test (testing for loss)
        permutation_p_value = np.mean(perm_diffs >= observed_diff)

    # Plot the distribution of permutation differences
    fig = plt.figure(figsize=(10, 6))
    sns.histplot(perm_diffs, bins=30, kde=True, color='green', label='Permutation Differences')
    permutation_mean = np.mean(perm_diffs)
    permutation_median = np.median(perm_diffs)
    plt.axvline(
    permutation_mean, color='black', linestyle='--', label=f'Mean Bootstraps: {permutation_mean:.2f}')
    plt.axvline(permutation_median, color='blue', linestyle='--', label=f'Median Bootstraps: {permutation_median:.2f}')

    plt.axvline(observed_diff, color='red', linestyle='--', label=f'Observed Difference: {observed_diff[0]:.2f}')
    plt.title(f'{assay_name}: Distribution of Permutation Differences (n={num_permutations}) for {gene} (p-value: {round(permutation_p_value, PVALUE_TITLE_ROUND)})')
    plt.xlabel('Difference in Medians')
    plt.ylabel('Frequency')
    plt.legend()
    plt.xlim(MIN_LOG2RATIO_TO_PLOT, MAX_LOG2RATIO_TO_PLOT)
    plt.tight_layout(rect=get_subplot_bounding_box_parameters())
    # The permutation distribution and the original log2cnr distribution are not expected to look the same because
    # they represent different things: the permutation distribution shows differences from permuted data, while the
    # log2cnr distribution shows the actual observed data. The permutation distribution helps you assess whether your
    # observed data_point is extreme compared to what you would expect by chance, while the original log2cnr
    # distribution tells you about the nature of your data.
    plt.savefig(f"scratch/log2cnr_plots/{assay_name}_{gene}_{data_point_file_name}_permutation_distribution.svg")
    plt.close(fig)

    print(f"Permutation P-value: {permutation_p_value}")

    return permutation_p_value


def get_bootstrap_pvalue(log2cnr: list, data_point: float, num_bootstrap: int = 10_000, gene: str = None,
                         assay_name: str =None) -> float:
    # Bootstrap resampling is a statistical technique used to estimate the distribution of a statistic
    # (e.g., mean, median) by repeatedly sampling with replacement from the observed data.
    # It allows you to create a large number of "bootstrap samples" that mimic the process of collecting new samples,
    # but from your existing data.

    # This method provides a direct comparison between the original and bootstrapped data distributions,
    # which can be valuable if you're interested in understanding how the data itself (rather than a summary statistic)
    # behaves under resampling.
    bootstraps = []
    for _ in range(num_bootstrap):
        resample = np.random.choice(log2cnr, size=len(log2cnr), replace=True)
        bootstraps.extend(resample)  # Collect all resampled values, rather than summarizing each resample with a
        # single statistic like the mean or median, i.e. do not do bootstraps.append(np.median(resample)), and do extend

    bootstraps = np.array(bootstraps)

    # Calculate the median, mean, and range of the bootstrap samples
    bootstrap_median = np.median(bootstraps)
    bootstrap_mean = np.mean(bootstraps)
    bootstrap_min = np.min(bootstraps)
    bootstrap_max = np.max(bootstraps)

    # Decide which tail to test based on the observed value
    # The key reason for using np.mean() here is to calculate the proportion of bootstrap samples that are as extreme
    # as or more extreme than the observed data_point. This proportion is what we interpret as the p-value in a
    # bootstrap test. e.g., if 5% of the bootstrap samples are greater than or equal to the observed data_point,
    # then np.mean(bootstraps >= data_point) will return 0.05, which is the p-value.
    if data_point >= bootstrap_median:
        # Right-tail test for gain
        # If you have 100,000 bootstrap samples and 5,000 of them are greater than or equal to your data_point, then:
        # # This would be 5000 / 100000 = 0.05
        bootstrap_p_value = np.mean(bootstraps >= data_point)
    else:
        # Left-tail test for loss
        bootstrap_p_value = np.mean(bootstraps <= data_point)

    # e.g. p-bootstrap_p_value is 0.05, indicating that 5% of the bootstrap samples are as extreme as or more extreme
    # than your observed data_point
    # Ensure p-value is not exactly zero (use small constant if necessary)
    bootstrap_p_value = max(bootstrap_p_value, 1e-10)

    # Plot the distribution of bootstrap samples
    sns.set(style="whitegrid")
    fig = plt.figure(figsize=(10, 6))
    # Draw the histogram (bar distribution)
    ax = sns.histplot(bootstraps, bins=30, kde=True, color='lightblue', label='Bootstrap Histogram')
    plt.axvline(data_point, color='red', linestyle='--', label=f'Data Point: {data_point}')

    plt.axvline(bootstrap_mean, color='black', linestyle='--', label=f'Mean Bootstraps: {bootstrap_mean:.2f}')
    plt.axvline(bootstrap_median, color='blue', linestyle='--', label=f'Median Bootstraps: {bootstrap_median:.2f}')

    ax.set_title(f'{assay_name}: Distribution of Bootstrap Samples (n={num_bootstrap}) for {gene} (p-value: {round(bootstrap_p_value, PVALUE_TITLE_ROUND)})')
    plt.xlabel('Bootstrap Sample Median')
    plt.ylabel('Frequency')
    plt.legend()
    plt.xlim(MIN_LOG2RATIO_TO_PLOT, MAX_LOG2RATIO_TO_PLOT)
    plt.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.savefig(f"scratch/log2cnr_plots/{assay_name}_{gene}_{data_point}_bootstrap_distribution.svg")
    plt.close(fig)

    # Debugging: Print statistics for insight
    print(f"Data Point: {data_point}")
    print(f"Bootstrap Median: {bootstrap_median}")
    print(f"Bootstrap Mean: {bootstrap_mean}")
    print(f"Bootstrap Range: {bootstrap_min} to {bootstrap_max}")
    # print final boostrap p-value
    print(f"Bootstrapped P-value: {bootstrap_p_value}")

    return bootstrap_p_value


def get_empirical_pvalue(log2cnr: list = None, data_point: float = None) -> float:
    # Ensure log2_ratios is a flat array
    log2cnr = np.array(log2cnr).flatten()

    # Step 1: Count how many data points are as extreme as or more extreme than the observed data point
    # For a one-tailed test (right-tail, i.e., testing for gains):
    if data_point >= 0:
        more_extreme = np.sum(log2cnr >= data_point)
    # Alternatively, for a one-tailed test (left-tail, i.e., testing for losses):
    else:
        more_extreme = np.sum(log2cnr <= data_point)

    # Step 2: Calculate empirical p-value
    empirical_p_value = more_extreme / len(log2cnr)

    print(f"Data Point: {data_point}")
    print(f"One-Tailed Empirical P-value: {empirical_p_value}")

    return empirical_p_value

def get_kde_pvalue(log2cnr: list = None, data_point: float = None, gene: str = None,
                   assay_name: str = None) -> Tuple[float, float, float, float, float]:
    # Step 1: Perform Kernel Density Estimation (KDE)
    kde = stats.gaussian_kde(log2cnr, bw_method='scott')  # bw_method controls the smoothness

    # Step 2: Define the range for evaluation
    x_vals = np.linspace(min(log2cnr), max(log2cnr), 1000)
    pdf_vals = kde(x_vals)

    # Step 3: Calculate the cumulative distribution function (CDF) from the KDE
    cdf_vals = np.cumsum(pdf_vals)
    cdf_vals /= cdf_vals[-1]  # Normalize to make it a proper CDF

    # Step 4: Calculate the p-value for a one-tailed test
    if data_point >= 0:
        kde_p_value = 1 - np.interp(data_point, x_vals, cdf_vals)
    else:
        kde_p_value = np.interp(data_point, x_vals, cdf_vals)
    kde_p_value = float(kde_p_value)

    # Step 5: Plot the KDE
    sns.set(style="whitegrid")
    fig, ax = plt.subplots(figsize=(10, 6))
    # Draw the histogram (bar distribution)
    sns.histplot(log2cnr, bins=30, kde=True, color='blue', label='Log2cnr Histogram', ax=ax)
    # Add a vertical line for the data_point
    ax.axvline(data_point, color='red', linestyle='--', label=f'Data Point: {data_point}')

    num_log2cnr = len(log2cnr)
    # Add a vertical line for the mean and median of the KDE
    kde_mean = np.mean(log2cnr)
    kde_median = np.median(log2cnr)
    kde_min = np.min(log2cnr)
    kde_max = np.max(log2cnr)



    # Add labels and legend using the Axes object
    negative_3_or_less = find_number_defined_at_threshold(data=log2cnr, data_point=-3, greater_equal=False)
    positive_3_or_more = find_number_defined_at_threshold(data=log2cnr, data_point=3, greater_equal=True)
    print_string = f'# <= -3, {negative_3_or_less} | # >= 3, {positive_3_or_more}'
    ax.set_xlabel(f'Log2 Ratio | {print_string}')
    ax.set_ylabel('Density')


    # Step 6: Create a secondary y-axis with a logarithmic scale to show things like frequency of 1 better
    ax2 = ax.twinx()
    sns.histplot(log2cnr, bins=30, kde=False, color='blue', linewidth=2, ax=ax2, alpha=0.10,
                 label='Log2cnr Histogram Scaled')
    # Set the y-axis to log scale with base 10
    ax2.set_yscale('log', base=10)
    ax2.set_ylabel('Log10-scaled Density')

    # do this with ax2 b/c it a secondary plot
    ax2.axvline(kde_mean, color='black', linestyle='--', label=f'Mean KDE: {kde_mean:.2f}')
    ax2.axvline(kde_median, color='blue', linestyle='--', label=f'Median KDE: {kde_median:.2f}')
    ax2.legend()

    # Add the title with p-value using the Axes object
    plt.title(f'{assay_name}: Distribution of Log2 Ratios Samples (n={num_log2cnr}) for {gene} (p-value: {round(kde_p_value, PVALUE_TITLE_ROUND)})')

    plt.tight_layout(rect=get_subplot_bounding_box_parameters())
    plt.xlim(MIN_LOG2RATIO_TO_PLOT, MAX_LOG2RATIO_TO_PLOT)

    plt.savefig(f"scratch/log2cnr_plots/{assay_name}_{gene}_{data_point}_kde_distribution.svg", bbox_inches='tight')
    plt.close(fig)

    print(f"Data Point: {data_point}")
    print(f"KDE P-value: {kde_p_value}")

    return kde_p_value, kde_mean, kde_median, kde_min, kde_max

def get_log2cnr_list(engine: sqlalchemy.engine.base.Engine = None, table_name: str = None,
                     var_id: str = None) -> list:
    table_name2 = table_name.lower().replace(" ", "_")
    query = f"""
SELECT
    "log2cnr"
FROM
    {table_name}
WHERE
    "VAR_ID" = '{var_id}'
    """

    # Execute the query and load the results into a DataFrame
    data = pd.read_sql(query, engine)
    # Replace infinite values with NaNs
    data = data.replace([np.inf, -np.inf], np.nan)
    data = data.dropna(how='all')

    data = data['log2cnr'].to_list()
    return data


def get_var_ids_list(engine: sqlalchemy.engine.base.Engine = None, table_name: str = None) -> list:
    table_name2 = table_name.lower().replace(" ", "_")
    # The query cannot be a straightforward query b/c gs395 table has multiple rows for a gene:
    # For example:
    #
    # EPAS1_2_46525040
    # EPCAM_2_47596200
    # EPCAM_2_47596631
    # EPHA2_1_16451688
    # EPHA2_1_1645170
    #
    # EPAS1_2_46525040 might have 10000 rows
    # EPCAM_2_47596200 might have 10 rows
    # EPCAM_2_47596631 might have 10000 rows
    # EPHA2_1_16451688 might have 10 rows
    # EPHA2_1_1645170 might have 100000 rows.
    #
    # The query should return the gene e.g. EPCAM with the most rows EPCAM_2_47596631.
    # It should return the gene EPHA2 with the most rows EPHA2_1_1645170


    query = f"""
-- Step 1: Extract the gene prefix and count rows for each VAR_ID
WITH PrefixCounts AS (
    SELECT
        -- Extracting the gene prefix
        substring("VAR_ID" FROM '^[^_]+') AS gene_prefix,
        "VAR_ID",
        COUNT(*) AS cnt
    FROM
        {table_name}
    GROUP BY
        gene_prefix, "VAR_ID"
),

-- Step 2: Find the maximum count for each gene prefix
MaxCounts AS (
    SELECT
        gene_prefix,
        "VAR_ID",
        cnt
    FROM (
        SELECT
            gene_prefix,
            "VAR_ID",
            cnt,
            ROW_NUMBER() OVER (PARTITION BY gene_prefix ORDER BY cnt DESC) AS rn
        FROM
            PrefixCounts
    ) AS Ranked
    WHERE
        rn = 1
)

-- Step 3: Return the VAR_ID with the maximum rows for each gene prefix
SELECT DISTINCT
    "VAR_ID"
FROM
    MaxCounts
ORDER BY
    "VAR_ID";
    """

    # Execute the query and load the results into a DataFrame
    data = pd.read_sql(query, engine)
    data = data['VAR_ID'].to_list()
    return data

def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Create the gene map json for the HRR assay')

    parser.add_argument('--password',
                        dest='password',
                        type=str,
                        default=None,
                        help='The password to connect to the database',
                        required=False)
    parser.add_argument('--assay_name', dest='assay_name', required=True,
                        help="Assay name.  Used for final naming of files",
                        type=str, choices=get_assay_names())


    return parser.parse_args()

if __name__ == '__main__':
    main()