import argparse
import pandas as pd
from functools import reduce
import os
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
from tools.cnv.utils import get_list_important_cnv_genes_with_oncokb, get_list_important_cnv_genes_with_gc_content
from adjustText import adjust_text
def main():
    args = get_cli_args()

    # Step 2: Extract the substring "GS700" from each file name
    dataframes = []
    for file in args.excel_files:
        base_name = os.path.basename(file)
        identifier = base_name.split('_')[-1].split('.')[0]
        df = pd.read_excel(file)
        update_df(df=df, identifier=identifier)
        # Add the identifier to each column name except 'gene' and 'data_point'
        df = df.rename(columns={col: f"{col}_{identifier}" if col not in ('gene', 'data_point') else col for col in df.columns})
        dataframes.append(df)

    # Step 3: Merge all DataFrames on the 'gene' column
    merged_df = reduce(lambda left, right: pd.merge(left, right, on=['gene', 'data_point']), dataframes)
    merged_df.to_excel("test.xlsx", index=False)

    # Step 4: Do some filtering before ploting (When i did this analysis I just calculated p-values at -1 and 2, so
    # the data is redundant expect for the pvalues, so filter down to just == 2
    merged_df_filtered = merged_df[merged_df['data_point'] == 2]
    # Step 5: Plot the median values of all genes
    gs395_log2_median = merged_df_filtered['kde_median_GS395'].to_list()
    gs700_log2_median = merged_df_filtered['kde_median_GS700'].to_list()
    gs700normals_log2_median = merged_df_filtered['kde_median_GS700normals'].to_list()
    gs395normals_log2_median = merged_df_filtered['kde_median_GS395normals'].to_list()
    # GS700 vs. GS395 median KDE
    plot_df(df=merged_df_filtered, xaxis='kde_median_GS395', yaxis='kde_median_GS700', title='KDE Median',
            print_gene=True, x=gs395_log2_median, y=gs700_log2_median)

    # GS700 vs. GS700 normals median KDE
    plot_df(df=merged_df_filtered, xaxis='kde_median_GS700normals', yaxis='kde_median_GS700', title='KDE Median',
            print_gene=True, x=gs700normals_log2_median, y=gs700_log2_median)

    # GS395 vs. GS395 normals median KDE
    plot_df(df=merged_df_filtered, xaxis='kde_median_GS395normals', yaxis='kde_median_GS395', title='KDE Median',
            print_gene=True, x=gs395normals_log2_median, y=gs395_log2_median)

    # GS700 normals vs GS395 normals
    plot_df(df=merged_df_filtered, xaxis='kde_median_GS395normals', yaxis='kde_median_GS700normals', title='KDE Median',
            print_gene=True, x=gs395normals_log2_median, y=gs700normals_log2_median)

    # Step 6: Plot the median values of just the important genes
    merged_df_filtered = merged_df_filtered[merged_df_filtered['gene'].isin(get_list_important_cnv_genes_with_oncokb())]
    gs395_log2_median = merged_df_filtered['kde_median_GS395'].to_list()
    gs700_log2_median = merged_df_filtered['kde_median_GS700'].to_list()
    plot_df(df=merged_df_filtered, xaxis='kde_median_GS395', yaxis='kde_median_GS700', title='Important Genes KDE Median',
            print_gene=True, x=gs395_log2_median, y=gs700_log2_median)

    # Step 7a: get the GC and plot
    gene_gc_content_dict = get_list_important_cnv_genes_with_gc_content()
    # Initialize the list to store exon GC content
    exon_gc_contents = []
    # Iterate over the gene list and get the exon GC content
    for gene in merged_df_filtered['gene'].to_list():
        if gene in gene_gc_content_dict:
            exon_gc_content = gene_gc_content_dict[gene]['exon_gc_content']
            exon_gc_contents.append(exon_gc_content)
        else:
            exon_gc_contents.append(None)  # or handle missing genes as needed
    # Step 7b: Plot the KDE vs GC content
    plot_df(df=merged_df_filtered, yaxis='kde_median_GS395', xaxis='Gene exon GC', title='Gene exon GC',
            print_gene=True, y=gs395_log2_median, x=exon_gc_contents, plot_range_x=(20, 80), text_adjust_y=.05,
            text_adjust_x=60)

    plot_df(df=merged_df_filtered, yaxis='kde_median_GS700', xaxis='Gene exon GC', title='Gene exon GC',
            print_gene=True, y=gs700_log2_median, x=exon_gc_contents, plot_range_x=(20, 80), text_adjust_y=.05,
            text_adjust_x=60)

    # Step 8. Plot the percentage
    for key1, key2 in [('percentage_change_1_GS700', 'percentage_change_1_GS395'),
                       ('percentage_change_2_GS700', 'percentage_change_2_GS395'),
                       ('percentage_change_-1_GS700', 'percentage_change_-1_GS395')]:
        plot_bar_chart_df(df=merged_df_filtered, key1=key1, key2=key2)

def plot_bar_chart_df(df: pd.DataFrame, key1: str = None, key2: str = None) -> None:
    # Melt the DataFrame for seaborn
    df_melted = pd.melt(df, id_vars=['gene'], value_vars=[key1, key2],
                        var_name='Assay', value_name='Percentage Change')

    # Map assay names for easier interpretation
    assay1 = key1.split('_')[3]
    assay2 = key2.split('_')[3]
    df_melted['Assay'] = df_melted['Assay'].replace({
        key1: assay1,
        key2: assay2
    })

    # Plotting the bar chart
    plt.figure(figsize=(15, 6))
    plt.xticks(rotation=45)
    sns.barplot(data=df_melted, x='gene', y='Percentage Change', hue='Assay')
    plt.title(f"{key1.lower()} vs.{key2.lower()}")
    plt.tight_layout()

    plt.savefig(f"{key1.lower()}_{key2.lower()}_bar_chart.svg")

def plot_df(df: pd.DataFrame = None, xaxis: str = None, yaxis: str = None, title: str = None, print_gene: bool = None,
            x: list = None, y: list = None, plot_range_x: tuple = (-1, 1), plot_range_y: tuple = (-1, 1),
            text_adjust_y: float = -0.5, text_adjust_x: float = 0.05
            ) -> None:
    # Create the seaborn plot
    sns.set(style="whitegrid")
    plt.figure(figsize=(12, 12))

    genes = df['gene']  # Assuming 'gene' column exists in the DataFrame
    data = pd.DataFrame({'x': x, 'y': y, 'gene': genes})
    # Regression plot with 95% CI
    sns.regplot(x='x', y='y', data=data, ci=95, line_kws={"color": "red"})

    # Fit the data to get the regression equation and R^2
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    line_eq = f'y = {slope:.2f}x + {intercept:.2f}'
    r_squared = r_value ** 2

    plt.xlim(plot_range_x)
    plt.ylim(plot_range_y)
    # Annotate the plot with the equation and R^2
    #plt.text(text_adjust_x, text_adjust_y, line_eq, fontsize=12, color='black')
    plt.text(text_adjust_x, text_adjust_y, f'R² = {r_squared:.2f}', fontsize=12, color='black')

    # Draw the dotted diagonal line for the perfect linear relationship
    plt.plot(plot_range_x, plot_range_y, 'k--', linewidth=2, label='Perfect Linear Relationship')

    if print_gene is True:
        # Add gene names
        texts = []
        for i in range(len(data)):
            texts.append(plt.text(data['x'].iloc[i], data['y'].iloc[i],
                                  data['gene'].iloc[i], fontsize=8, color='blue', alpha=0.7))
        adjust_text(texts, arrowprops=dict(arrowstyle="->", color='gray', lw=0.5))

    # Set labels and title
    plt.xlabel(xaxis)
    plt.ylabel(yaxis)
    plt.axhline(y=0, color='black')
    plt.axvline(x=0, color='black')

    plt.title(f' {title} | {yaxis} vs {xaxis} with 95% CI')
    plt.tight_layout()

    plt.savefig(f"{title.replace(' ', '_').lower()}_{xaxis}_{yaxis}.svg")

def update_df(df: pd.DataFrame = None, identifier: str = None) -> None:
    df['assay_name'] = identifier
    for index in range(1, 4):
        num = str(index)
        df[f'percentage_change_{num}'] = df[f'kde_{num}'] / df['num_samples'] * 100
        df[f'percentage_change_-{num}'] = df[f'kde_-{num}'] / df['num_samples'] * 100


def get_cli_args() -> argparse.Namespace:
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(description='Create the gene map json for the HRR assay')

    parser.add_argument('--excel_files', nargs='+', help='List of Excel files to process that were output from: '
                                                       'gene_level_cnr_plotting.py')

    return parser.parse_args()

if __name__ == '__main__':
    main()