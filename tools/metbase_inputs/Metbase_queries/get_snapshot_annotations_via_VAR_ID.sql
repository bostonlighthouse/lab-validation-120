SELECT
  "public"."snapshotgsv2_variants_clinical_prod_tumor"."VAR_ID" AS "VAR_ID",
  COUNT("public"."snapshotgsv2_variants_clinical_prod_tumor"."VAR_ID") AS "Occurrences",
  MAX("public"."snapshotgsv2_variants_clinical_prod_tumor"."updated_on") AS "Latest_Update",
  "public"."snapshotgsv2_variants_clinical_prod_tumor"."most_severe_consequence" AS "most_severe_consequence",
   "public"."snapshotgsv2_variants_clinical_prod_tumor"."transcript_consequences_gene_symbol" AS "transcript_consequences_gene_symbol",
       "public"."snapshotgsv2_variants_clinical_prod_tumor"."transcript_consequences_hgvsc" AS "transcript_consequences_hgvsc",
              "public"."snapshotgsv2_variants_clinical_prod_tumor"."transcript_consequences_hgvsp" AS "transcript_consequences_hgvsp",
                            "public"."snapshotgsv2_variants_clinical_prod_tumor"."transcript_consequences_hgvsg" AS "transcript_consequences_hgvsg",
                                                        "public"."snapshotgsv2_variants_clinical_prod_tumor"."transcript_consequences_impact" AS "transcript_consequences_impact",
                                                        "public"."snapshotgsv2_variants_clinical_prod_tumor"."variant_class" AS "variant_class",
                                                        "public"."snapshotgsv2_variants_clinical_prod_tumor"."QC_PRIMARY_CALLER" AS "QC_PRIMARY_CALLER"
FROM
  "public"."snapshotgsv2_variants_clinical_prod_tumor"
WHERE
  "public"."snapshotgsv2_variants_clinical_prod_tumor"."VAR_ID" IN (
'11_108172494_._CT_C',
'22_30057328_._G_A',
'13_32972892_._T_TA'
  )
GROUP BY
  "VAR_ID", "most_severe_consequence", "transcript_consequences_gene_symbol", "transcript_consequences_hgvsc", "transcript_consequences_hgvsp", "transcript_consequences_hgvsg",
"transcript_consequences_impact", "variant_class", "QC_PRIMARY_CALLER";


SELECT
  "public"."snapshot395_variants_clinical_prod_tumor"."VAR_ID" AS "VAR_ID",
  COUNT("public"."snapshot395_variants_clinical_prod_tumor"."VAR_ID") AS "Occurrences",
  MAX("public"."snapshot395_variants_clinical_prod_tumor"."updated_on") AS "Latest_Update",
  "public"."snapshot395_variants_clinical_prod_tumor"."most_severe_consequence" AS "most_severe_consequence",
   "public"."snapshot395_variants_clinical_prod_tumor"."transcript_consequences_gene_symbol" AS "transcript_consequences_gene_symbol",
       "public"."snapshot395_variants_clinical_prod_tumor"."transcript_consequences_hgvsc" AS "transcript_consequences_hgvsc",
              "public"."snapshot395_variants_clinical_prod_tumor"."transcript_consequences_hgvsp" AS "transcript_consequences_hgvsp",
                            "public"."snapshot395_variants_clinical_prod_tumor"."transcript_consequences_hgvsg" AS "transcript_consequences_hgvsg",
                                                        "public"."snapshot395_variants_clinical_prod_tumor"."transcript_consequences_impact" AS "transcript_consequences_impact",
                                                        "public"."snapshot395_variants_clinical_prod_tumor"."variant_class" AS "variant_class",
                                                        "public"."snapshot395_variants_clinical_prod_tumor"."QC_PRIMARY_CALLER" AS "QC_PRIMARY_CALLER"
FROM
  "public"."snapshot395_variants_clinical_prod_tumor"
WHERE
  "public"."snapshot395_variants_clinical_prod_tumor"."VAR_ID" IN (
'22_30057328_._G_A',
'13_32972892_._T_TA'
  )
GROUP BY
  "VAR_ID", "most_severe_consequence", "transcript_consequences_gene_symbol", "transcript_consequences_hgvsc", "transcript_consequences_hgvsp", "transcript_consequences_hgvsg",
"transcript_consequences_impact", "variant_class", "QC_PRIMARY_CALLER";


SELECT
  "public"."snapshot2_variants_clinical_prod_tumor"."VAR_ID" AS "VAR_ID",
  COUNT("public"."snapshot2_variants_clinical_prod_tumor"."VAR_ID") AS "Occurrences",
  MAX("public"."snapshot2_variants_clinical_prod_tumor"."updated_on") AS "Latest_Update",
  "public"."snapshot2_variants_clinical_prod_tumor"."most_severe_consequence" AS "most_severe_consequence",
   "public"."snapshot2_variants_clinical_prod_tumor"."transcript_consequences_gene_symbol" AS "transcript_consequences_gene_symbol",
       "public"."snapshot2_variants_clinical_prod_tumor"."transcript_consequences_hgvsc" AS "transcript_consequences_hgvsc",
              "public"."snapshot2_variants_clinical_prod_tumor"."transcript_consequences_hgvsp" AS "transcript_consequences_hgvsp",
                            "public"."snapshot2_variants_clinical_prod_tumor"."transcript_consequences_hgvsg" AS "transcript_consequences_hgvsg",
                                                        "public"."snapshot2_variants_clinical_prod_tumor"."transcript_consequences_impact" AS "transcript_consequences_impact",
                                                        "public"."snapshot2_variants_clinical_prod_tumor"."variant_class" AS "variant_class",
                                                        "public"."snapshot2_variants_clinical_prod_tumor"."QC_PRIMARY_CALLER" AS "QC_PRIMARY_CALLER"
FROM
  "public"."snapshot2_variants_clinical_prod_tumor"
WHERE
  "public"."snapshot2_variants_clinical_prod_tumor"."VAR_ID" IN (
'22_30057328_._G_A',
'13_32972892_._T_TA'
  )
GROUP BY
  "VAR_ID", "most_severe_consequence", "transcript_consequences_gene_symbol", "transcript_consequences_hgvsc", "transcript_consequences_hgvsp", "transcript_consequences_hgvsg",
"transcript_consequences_impact", "variant_class", "QC_PRIMARY_CALLER";



