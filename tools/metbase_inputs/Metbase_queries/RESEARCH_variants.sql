SELECT
    "transcript_consequences_gene_symbol" AS "Hugo_Symbol",
    /* "" AS "Entrez_Gene_Id", This is optional, so commented for now. */
    "CHROM" AS "Chromosome",
    "START_POSITION" AS "Start_Position",
    "END_POSITION" AS "End_Position",
    "most_severe_consequence" AS "Variant_Classification",
    "variant_class" AS "Variant_Type",
    SPLIT_PART("allele_string", '/', 1) AS "Reference_Allele",
    SPLIT_PART("allele_string", '/', 1) AS "Tumor_Seq_Allele1",
    SPLIT_PART("allele_string", '/', 2) AS "Tumor_Seq_Allele2",
    "sample_name" AS "Tumor_Sample_Barcode",
    SPLIT_PART("transcript_consequences_hgvsp", ':', 2) AS "HGVSp_Short",
    "QC_ALT_DP" + "QC_REF_DP" AS "t_depth",
    "QC_ALT_DP" AS "t_alt_count",
    "QC_REF_DP" AS "t_ref_count",
    '' AS "n_depth",
    '' AS "n_ref_count",
    '' AS "n_alt_count",
    "VETT" AS "FILTER"

FROM snapshotgsv2_variants_clinical_prod_tumor
-- FROM snapshot2_variants_clinical_prod_tumor
WHERE "VETT" = 'YES' OR "VETT" = 'RESEARCH';



SELECT * FROM (
    SELECT
        "transcript_consequences_gene_symbol" AS "Hugo_Symbol",
        /* "" AS "Entrez_Gene_Id", This is optional, so commented for now. */
        "CHROM" AS "Chromosome",
        "START_POSITION" AS "Start_Position",
        "END_POSITION" AS "End_Position",
        "most_severe_consequence" AS "Variant_Classification",
        "variant_class" AS "Variant_Type",
        SPLIT_PART("allele_string", '/', 1) AS "Reference_Allele",
        SPLIT_PART("allele_string", '/', 1) AS "Tumor_Seq_Allele1",
        SPLIT_PART("allele_string", '/', 2) AS "Tumor_Seq_Allele2",
        "sample_name" AS "Tumor_Sample_Barcode",
        SPLIT_PART("transcript_consequences_hgvsp", ':', 2) AS "HGVSp_Short",
        SPLIT_PART("transcript_consequences_hgvsc", ':', 2) AS "HGVSc_Short",
        "QC_ALT_DP" + "QC_REF_DP" AS "t_depth",
        "QC_ALT_DP" AS "t_alt_count",
        "QC_REF_DP" AS "t_ref_count",
        '' AS "n_depth",
        '' AS "n_ref_count",
        '' AS "n_alt_count",
        "VETT" AS "FILTER",
        CONCAT('http://vv.cgs.bostonlighthouse.us/', "sample_url") AS "VV_URL"


    FROM snapshotgsv2_variants_clinical_prod_tumor
    -- FROM snapshot2_variants_clinical_prod_tumor
    WHERE "VETT" = 'YES' OR "VETT" = 'RESEARCH'
) AS "temp"

LEFT JOIN "full_results"
ON "full_results"."VV_URL" = "temp"."VV_URL"

;