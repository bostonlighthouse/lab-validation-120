-- http://analytics3.cgs.bostonlighthouse.us/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjM1LCJvcmRlci1ieSI6W1siZGVzYyIsWyJmaWVsZCIsODI2LG51bGxdXV0sImZpbHRlciI6WyJhbmQiLFsiPSIsWyJmaWVsZCIsODE1LG51bGxdLCJ2Ni4xLjEiXV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e30sIm9yaWdpbmFsX2NhcmRfaWQiOm51bGx9


SELECT main_query."Batch",
       main_query."Test",
       main_query."Results",
       main_query."CID",
       main_query."Diagnosis",
       main_query."PrimarySite",
       main_query."PrimarySiteDiagnosis",
       main_query."TMBScore",
       main_query."TMBStatus",
       main_query."MSIScore",
       main_query."MSIStatus",
       sub_query."mcc",
       sub_query."fraction_gte_upper_dp"
FROM (
SELECT
    "Batch",
    "Test",
    "Diagnosis",
    "PrimarySite",
    "PrimarySiteDiagnosis",
    "TMBScore",
    "TMBStatus",
    "MSIScore",
    "MSIStatus",
    "ID" AS "Results",
    SUBSTRING("ID", 'CID\d*-\d*') as "CID"
    FROM "full_results"
) AS main_query
FULL OUTER JOIN (
    SELECT
        "fraction_gte_upper_dp",
        "mcc",
        UPPER(SUBSTRING("sample_url", 'cid\d*-\d*')) AS "CID"
    FROM snapshot2_meta_clinical_prod_tumor
    UNION ALL
    SELECT
        "fraction_gte_upper_dp",
        "mcc",
        UPPER(SUBSTRING("sample_url", 'cid\d*-\d*')) AS "CID"
    FROM snapshotgsv2_meta_clinical_prod_tumor
    UNION ALL
    SELECT
        "fraction_gte_upper_dp",
        "mcc",
        UPPER(SUBSTRING("sample_url", 'cid\d*-\d*')) AS "CID"
    FROM fusion_meta_clinical_prod_tumor
) AS sub_query
ON main_query."CID" = sub_query."CID"
WHERE main_query."Batch" in ('B23-130', 'B23-125', 'B23-119', 'B23-117', 'B23-113', 'B23-109', 'B23-107', 'B23-105', 'B23-101',
                             'B23-97', 'B23-95', 'B23-91', 'B23-87', 'B23-82', 'B23-78', 'B23-74', 'B23-70', 'B23-66', 'B23-58',
                            'B23-53', 'B23-45', 'B23-38', 'B23-28', 'B23-20',  'B23-9', 'B23-4', 'B23-2')


-- GS395 batches as of 20231015
( 'B23-166', 'B23-163', 'B23-161', 'B23-157', 'B23-154', 'B23-152', 'B23-149', 'B23-147',
                            'B23-132', 'B23-128', 'B23-123', 'B23-111', 'B23-103', 'B23-99', 'B23-93', 'B23-89',
                             'B23-84', 'B23-80', 'B23-76', 'B23-72', 'B23-64', 'B23-60', 'B23-56', 'B23-55', 'B23-49',
                            'B23-49', 'B23-47', 'B23-30', 'B23-7',
                            'B22-162', 'B22-156', 'B22-150', 'B22-144', 'B22-140', 'B22-134')


( 'B22-113', 'B22-112', 'B22-109', 'B22-108', 'B22-107', 'B22-106', 'B22-105', 'B22-102', 'B22-103', 'B22-104', 'B22-98', 'B22-99', 'B22-100', 'B22-101', 'B22-96', 'B22-97', 'B22-92', 'B22-93', 'B22-90', 'B22-91', 'B22-88', 'B22-87', 'B22-86', 'B22-85', 'B22-83','B22-82', 'B22-81','B22-80', 'B22-79', 'B22-78', 'B22-77', 'B22-76', 'B22-75', 'B22-74',
'B22-72', 'B22-73', 'B22-71', 'B22-70', 'B22-69', 'B22-68', 'B22-67', 'B22-66', 'B22-65', 'B22-64', 'B22-63', 'B22-62', 'B22-61', 'B22-60','B22-59', 'B22-58', 'B22-57', 'B22-56',
'B22-51', 'B22-50', 'B22-49', 'B22-48',  'B22-47', 'B22-46', 'B22-45', 'B22-41', 'B22-40', 'B22-38', 'B22-39', 'B22-35', 'B22-36',
 'B22-33', 'B22-34', 'B22-30', 'B22-29', 'B22-28', 'B22-27', 'B22-26', 'B22-25', 'B22-24', 'B22-23', 'B22-22', 'B22-21',
 'B22-20', 'B22-19', 'B22-18')

-- WHERE main_query."Batch" in ('B22-78', 'B22-77', 'B22-76', 'B22-75', 'B22-74');
-- ('B22-72', 'B22-73' );
-- 'B22-71', 'B22-70', OC395
--  'B22-69', 'B22-68' - not clinical
-- WHERE main_query."Batch" in ('B22-67', 'B22-66', 'B22-65', 'B22-64' );
-- WHERE main_query."Batch" in ('B22-63', 'B22-62', 'B22-61', 'B22-60','B22-59', 'B22-58', 'B22-57', 'B22-56' );
-- WHERE main_query."Batch" in ('B22-51', 'B22-50', 'B22-49', 'B22-48',  'B22-47', 'B22-46', 'B22-45', 'B22-41', 'B22-40');
-- WHERE main_query."Batch" in ('B22-38', 'B22-39');
-- LAST TIME WHERE main_query."Batch" in ('B22-35', 'B22-36');
-- LAST TIME WHERE main_query."Batch" in ('B22-33', 'B22-34');
-- LAST TIME ('B22-30', 'B22-29');
-- LAST TIME WHERE main_query."Batch" in ('B22-28', 'B22-27', 'B22-26', 'B22-25', 'B22-24', 'B22-23', 'B22-22', 'B22-21');
-- LAST TIME WHERE main_query."Batch" in ('B22-20', 'B22-19', 'B22-18');


-- for clustering
SELECT main_query."Batch", main_query."Test", main_query."Results", main_query."Diagnosis", main_query."Primary_Site",
main_query."Primary_Site_Diagnosis", main_query."TMB_Score", main_query."TMB_Status",
main_query."MSI_Score", main_query."MSI_Status", main_query."Batch_Mode",
main_query."MCC", sub_query."fraction_gte_upper_dp" FROM full_results as main_query
LEFT JOIN (
    SELECT "fraction_gte_upper_dp", CONCAT('http://vv.cgs.bostonlighthouse.us/', "sample_url") AS "VV_URL" FROM snapshot2_meta_clinical_prod_tumor
    UNION ALL
    SELECT "fraction_gte_upper_dp", CONCAT('http://vv.cgs.bostonlighthouse.us/', "sample_url") AS "VV_URL" FROM snapshotgsv2_meta_clinical_prod_tumor
) AS sub_query ON main_query."VV_URL"= sub_query."VV_URL"
WHERE main_query."Test" in ('SNAPSHOT-GSV2', 'SNAPSHOT-NGS-V2')



SELECT main_query."Batch", main_query."Test", main_query."Results", main_query."Diagnosis", main_query."Primary_Site",
main_query."Primary_Site_Diagnosis", main_query."TMB_Score", main_query."TMB_Status",
main_query."MSI_Score", main_query."MSI_Status", main_query."Batch_Mode",
main_query."MCC", sub_query."fraction_gte_upper_dp" FROM full_results as main_query
LEFT JOIN (
    SELECT "fraction_gte_upper_dp", CONCAT('http://vv.cgs.bostonlighthouse.us/', "sample_url") AS "VV_URL" FROM snapshot2_meta_clinical_prod_tumor
    UNION ALL
    SELECT "fraction_gte_upper_dp", CONCAT('http://vv.cgs.bostonlighthouse.us/', "sample_url") AS "VV_URL" FROM snapshotgsv2_meta_clinical_prod_tumor
) AS sub_query ON main_query."VV_URL"= sub_query."VV_URL"
WHERE main_query."Test" != 'Solid Fusion Assay V2' ORDER BY main_query."Diagnosis";
