SELECT
  "public"."fusion395_variants_clinical_prod_tumor"."VAR_ID" AS "VAR_ID",
  COUNT("public"."fusion395_variants_clinical_prod_tumor"."VAR_ID") AS "Occurrences",
  MAX("public"."fusion395_variants_clinical_prod_tumor"."updated_on") AS "Latest_Update",
   "public"."fusion395_variants_clinical_prod_tumor"."Isoform_fusionType" AS "Isoform_fusionType",
         "public"."fusion395_variants_clinical_prod_tumor"."LP_gene_name" AS "LP_gene_name",
      "public"."fusion395_variants_clinical_prod_tumor"."LP_break_point" AS "LP_break_point",
            "public"."fusion395_variants_clinical_prod_tumor"."LP_position" AS "LP_position",
            "public"."fusion395_variants_clinical_prod_tumor"."RP_break_point" AS "RP_break_point",
                     "public"."fusion395_variants_clinical_prod_tumor"."RP_gene_name" AS "RP_gene_name",
                                 "public"."fusion395_variants_clinical_prod_tumor"."RP_position" AS "RP_position"
 FROM
  "public"."fusion395_variants_clinical_prod_tumor"
WHERE
  "public"."fusion395_variants_clinical_prod_tumor"."VAR_ID" IN (
'7_116411708_7_116414934',
'17_78867665_17_64492318'
)
GROUP BY
  "VAR_ID", "Isoform_fusionType", "LP_gene_name", "LP_break_point", "LP_position", "RP_gene_name", "RP_break_point", "RP_position";
