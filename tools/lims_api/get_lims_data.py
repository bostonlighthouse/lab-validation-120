"""Simple module to get the data needed from the LIMS"""
import json
import re
import sys
import requests
import time as t
import urllib3


def get_json_result(cid=None, url_base=None, headers=None):
    """
    Deserialized JSON String : get_json_result(cid, url_base, headers)
    Takes: Three arguments, string task id,  the url base, and the other is the headers
    @param cid: THe cid to query
    @param url_base: The base to the LIMS
    @param headers: Headers to pass to the get request
    @return: Python object from response.json()
    """
    url_base = url_base + '/' + cid
    response = None
    try:
        print(f"Trying {url_base}", file=sys.stderr)
        response = requests.get(url=url_base, headers=headers, timeout=100)
    except requests.exceptions.RequestException as e:
        print(f"Error for cid {cid}: {e}\n{response.raise_for_status()}", file=sys.stderr)
    # https://stackoverflow.com/questions/59851207/connectionreseterror-with-requests-module-raises-exception-inside-of-exception
    except urllib3.exceptions.ProtocolError as e:
        print(f"failed on {cid} b/c of {e}, trying again after sleeping for 10 seconds....", file=sys.stderr)
        t.sleep(10)
        json = get_json_result(cid=cid, url_base=url_base, headers=headers)
        return json

    return response.json()


def json_printer(json_file: str = None) -> dict:
    """hard-coded in a test file, so I could implement print_data() w/o calling many times.. leave for now in case
       I need to update print_data()
    """
    file = "CID20-435.json"
    with open(file) as in_fh:
        return json.load(in_fh)


def return_batch_and_cid(input: str = None) -> tuple:
    """
    Take cosmos input or cider pipeline directory input and parse out the data
    :param input: The string to parse e.g.:
    2021fev09-01-oc395-nova-b21-15-cid21-74-snapshot
    2021jul08-oc395-run0-b21-90-cid21-1050-snapshot
    """

    match = re.findall(r"(b\d+-\d+)-(cid\d+-\d+)", input)  # match the batch and cid
    batch = match[0][0]
    cid = match[0][1]
    return batch, cid