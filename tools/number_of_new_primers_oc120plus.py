"""Open the GTF files from oc120 and oc120plus and find the number of new primers used in GS 120plus"""
import argparse
import sys

from lab_validation_120.utils.configs.lab_validation_config import get_oc120plus_gtf_file, get_oc120_gtf_file


def main():
    args = get_cli_args()
    set_primers_oc120plus = process_gtf_file(input_file=get_oc120plus_gtf_file())
    set_primers_oc120 = process_gtf_file(input_file=get_oc120_gtf_file())

    print(set_primers_oc120plus.intersection(set_primers_oc120))
    print(f"There are # {len(set_primers_oc120plus)} for pimers in OC120plus")
    print(f"There are # {len(set_primers_oc120)} for pimers in OC120p")
    print(f"There are # {len(set_primers_oc120plus.intersection(set_primers_oc120))} shared primers in OC120plus and "
          f"OC120")


def process_gtf_file(input_file=None):
    """
    Open the GTF file and get a dictionary of all values primer_bind (start + '_' + stop)
    @param input_file:  What file to open
    """
    set_values = set()
    with open(input_file) as infh:
        for line in infh:
            values = line.rstrip().split("\t")
            start = str(values[3])
            stop = str(values[4])
            strand = values[6]
            key = '_'.join((start, strand, stop))
            set_values.add(key)
    return set_values


def get_cli_args():
    """
    void: get_cli_args()
    Takes: no arguments
    Returns: instance of argparse arguments
    """
    parser = argparse.ArgumentParser(
        description='Open the GFT files from oc120 and oc120plus and see what primers are shared and what are primers '
                    'are Different')

    return parser.parse_args()


if __name__ == "__main__":
    main()