set -e
# BED files from polymorphisms
BED=$1  # reference/HGDP-938-632958/HGDP/HGDP_938.bed
# List of your bam files with full paths
bamlist=$2
REF="/export/resources/GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa"
# Output prefix
OUT=$3

# 1. Generate low-pass pileup files
for bam in `cat $bamlist`; do
         echo $bam
         #samtools mpileup -q 20 -Q 20 -l $BED -f $REF $bam > $bam.pileup
    done

# 2. Convert pileup to Laser input (.seq file)
var=''
for bam in `cat $bamlist`; do
         echo $bam
         var="$var $bam.pileup"
    done

echo $var
echo "LASER-2.04/pileup2seq/pileup2seq.py -f $REF -m reference/HGDP-938-632958/HGDP/HGDP_938.site -o $OUT $var"
python2 LASER-2.04/pileup2seq/pileup2seq.py -f $REF -m reference/HGDP-938-632958/HGDP/HGDP_938.site -o $OUT $var

#4. run LASER for both reference and cases
# Note:
# -- change SEQ_FILE to $OUT.seq
LASER-2.04/laser -p reference/Laser.example.conf -k 10
