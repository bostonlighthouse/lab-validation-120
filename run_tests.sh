#!/bin/bash
set -e

# Run tests
coverage run -m pytest biomodels/tests/ "$@"

# Generate report in command line
coverage report

# Generate HTML report
coverage html


